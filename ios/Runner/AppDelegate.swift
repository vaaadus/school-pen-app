import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        GeneratedPluginRegistrant.register(with: self)
    
        initializeLocalNotifications()
    
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func initializeLocalNotifications() {
        UNUserNotificationCenter.current().delegate = self
    }
    
    private func initializeBackgroundFetch() {
        // background fetch every 30 mins
        UIApplication.shared.setMinimumBackgroundFetchInterval(TimeInterval(30 * 60))
    }
}
