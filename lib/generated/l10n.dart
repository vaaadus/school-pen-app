// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `School Pen`
  String get app_name {
    return Intl.message(
      'School Pen',
      name: 'app_name',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get nav_home {
    return Intl.message(
      'Home',
      name: 'nav_home',
      desc: '',
      args: [],
    );
  }

  /// `Courses`
  String get nav_courses {
    return Intl.message(
      'Courses',
      name: 'nav_courses',
      desc: '',
      args: [],
    );
  }

  /// `Notes`
  String get nav_notes {
    return Intl.message(
      'Notes',
      name: 'nav_notes',
      desc: '',
      args: [],
    );
  }

  /// `Timetable`
  String get nav_timetable {
    return Intl.message(
      'Timetable',
      name: 'nav_timetable',
      desc: '',
      args: [],
    );
  }

  /// `Attendance`
  String get nav_attendance {
    return Intl.message(
      'Attendance',
      name: 'nav_attendance',
      desc: '',
      args: [],
    );
  }

  /// `People`
  String get nav_people {
    return Intl.message(
      'People',
      name: 'nav_people',
      desc: '',
      args: [],
    );
  }

  /// `Files`
  String get nav_files {
    return Intl.message(
      'Files',
      name: 'nav_files',
      desc: '',
      args: [],
    );
  }

  /// `Holidays`
  String get nav_holidays {
    return Intl.message(
      'Holidays',
      name: 'nav_holidays',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get nav_search {
    return Intl.message(
      'Search',
      name: 'nav_search',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get nav_settings {
    return Intl.message(
      'Settings',
      name: 'nav_settings',
      desc: '',
      args: [],
    );
  }

  /// `More`
  String get common_more {
    return Intl.message(
      'More',
      name: 'common_more',
      desc: '',
      args: [],
    );
  }

  /// `Copy`
  String get common_copy {
    return Intl.message(
      'Copy',
      name: 'common_copy',
      desc: '',
      args: [],
    );
  }

  /// `Options`
  String get common_options {
    return Intl.message(
      'Options',
      name: 'common_options',
      desc: '',
      args: [],
    );
  }

  /// `Overdue`
  String get common_overdue {
    return Intl.message(
      'Overdue',
      name: 'common_overdue',
      desc: '',
      args: [],
    );
  }

  /// `Less`
  String get common_less {
    return Intl.message(
      'Less',
      name: 'common_less',
      desc: '',
      args: [],
    );
  }

  /// `Avg`
  String get common_avg {
    return Intl.message(
      'Avg',
      name: 'common_avg',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get common_search {
    return Intl.message(
      'Search',
      name: 'common_search',
      desc: '',
      args: [],
    );
  }

  /// `Account`
  String get common_account {
    return Intl.message(
      'Account',
      name: 'common_account',
      desc: '',
      args: [],
    );
  }

  /// `Premium`
  String get common_premium {
    return Intl.message(
      'Premium',
      name: 'common_premium',
      desc: '',
      args: [],
    );
  }

  /// `Sync & Restore`
  String get common_syncAndRestore {
    return Intl.message(
      'Sync & Restore',
      name: 'common_syncAndRestore',
      desc: '',
      args: [],
    );
  }

  /// `PC/Web version`
  String get common_pcOrWebVersion {
    return Intl.message(
      'PC/Web version',
      name: 'common_pcOrWebVersion',
      desc: '',
      args: [],
    );
  }

  /// `Select your school`
  String get common_selectYourSchool {
    return Intl.message(
      'Select your school',
      name: 'common_selectYourSchool',
      desc: '',
      args: [],
    );
  }

  /// `Select your timetable`
  String get common_selectYourTimetable {
    return Intl.message(
      'Select your timetable',
      name: 'common_selectYourTimetable',
      desc: '',
      args: [],
    );
  }

  /// `School Years`
  String get common_schoolYears {
    return Intl.message(
      'School Years',
      name: 'common_schoolYears',
      desc: '',
      args: [],
    );
  }

  /// `New year`
  String get common_newSchoolYear {
    return Intl.message(
      'New year',
      name: 'common_newSchoolYear',
      desc: '',
      args: [],
    );
  }

  /// `Edit school year`
  String get common_editSchoolYear {
    return Intl.message(
      'Edit school year',
      name: 'common_editSchoolYear',
      desc: '',
      args: [],
    );
  }

  /// `Students`
  String get common_students {
    return Intl.message(
      'Students',
      name: 'common_students',
      desc: '',
      args: [],
    );
  }

  /// `Custom`
  String get common_custom {
    return Intl.message(
      'Custom',
      name: 'common_custom',
      desc: '',
      args: [],
    );
  }

  /// `Unknown`
  String get common_unknown {
    return Intl.message(
      'Unknown',
      name: 'common_unknown',
      desc: '',
      args: [],
    );
  }

  /// `Create`
  String get common_create {
    return Intl.message(
      'Create',
      name: 'common_create',
      desc: '',
      args: [],
    );
  }

  /// `Subscribe`
  String get common_subscribe {
    return Intl.message(
      'Subscribe',
      name: 'common_subscribe',
      desc: '',
      args: [],
    );
  }

  /// `Unsubscribe`
  String get common_unsubscribe {
    return Intl.message(
      'Unsubscribe',
      name: 'common_unsubscribe',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get common_name {
    return Intl.message(
      'Name',
      name: 'common_name',
      desc: '',
      args: [],
    );
  }

  /// `New name`
  String get common_newName {
    return Intl.message(
      'New name',
      name: 'common_newName',
      desc: '',
      args: [],
    );
  }

  /// `Duplicate`
  String get common_duplicate {
    return Intl.message(
      'Duplicate',
      name: 'common_duplicate',
      desc: '',
      args: [],
    );
  }

  /// `No category`
  String get common_noCategory {
    return Intl.message(
      'No category',
      name: 'common_noCategory',
      desc: '',
      args: [],
    );
  }

  /// `No lessons`
  String get common_noLessons {
    return Intl.message(
      'No lessons',
      name: 'common_noLessons',
      desc: '',
      args: [],
    );
  }

  /// `No grades added`
  String get common_noGradesAdded {
    return Intl.message(
      'No grades added',
      name: 'common_noGradesAdded',
      desc: '',
      args: [],
    );
  }

  /// `Appearance`
  String get common_appearance {
    return Intl.message(
      'Appearance',
      name: 'common_appearance',
      desc: '',
      args: [],
    );
  }

  /// `System (light)`
  String get common_theme_systemLight {
    return Intl.message(
      'System (light)',
      name: 'common_theme_systemLight',
      desc: '',
      args: [],
    );
  }

  /// `System (dark)`
  String get common_theme_systemDark {
    return Intl.message(
      'System (dark)',
      name: 'common_theme_systemDark',
      desc: '',
      args: [],
    );
  }

  /// `Dark`
  String get common_theme_dark {
    return Intl.message(
      'Dark',
      name: 'common_theme_dark',
      desc: '',
      args: [],
    );
  }

  /// `Light`
  String get common_theme_light {
    return Intl.message(
      'Light',
      name: 'common_theme_light',
      desc: '',
      args: [],
    );
  }

  /// `Black`
  String get common_theme_black {
    return Intl.message(
      'Black',
      name: 'common_theme_black',
      desc: '',
      args: [],
    );
  }

  /// `Amber`
  String get common_theme_amber {
    return Intl.message(
      'Amber',
      name: 'common_theme_amber',
      desc: '',
      args: [],
    );
  }

  /// `Navy blue`
  String get common_theme_navyBlue {
    return Intl.message(
      'Navy blue',
      name: 'common_theme_navyBlue',
      desc: '',
      args: [],
    );
  }

  /// `Red`
  String get common_theme_red {
    return Intl.message(
      'Red',
      name: 'common_theme_red',
      desc: '',
      args: [],
    );
  }

  /// `Green`
  String get common_theme_green {
    return Intl.message(
      'Green',
      name: 'common_theme_green',
      desc: '',
      args: [],
    );
  }

  /// `Privacy`
  String get common_privacy {
    return Intl.message(
      'Privacy',
      name: 'common_privacy',
      desc: '',
      args: [],
    );
  }

  /// `Support`
  String get common_support {
    return Intl.message(
      'Support',
      name: 'common_support',
      desc: '',
      args: [],
    );
  }

  /// `Integrations`
  String get common_integrations {
    return Intl.message(
      'Integrations',
      name: 'common_integrations',
      desc: '',
      args: [],
    );
  }

  /// `Rate us`
  String get common_rateUs {
    return Intl.message(
      'Rate us',
      name: 'common_rateUs',
      desc: '',
      args: [],
    );
  }

  /// `Share`
  String get common_share {
    return Intl.message(
      'Share',
      name: 'common_share',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get common_aboutApp {
    return Intl.message(
      'About',
      name: 'common_aboutApp',
      desc: '',
      args: [],
    );
  }

  /// `Anonymous user`
  String get common_anonymousUser {
    return Intl.message(
      'Anonymous user',
      name: 'common_anonymousUser',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get common_register {
    return Intl.message(
      'Register',
      name: 'common_register',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get common_login {
    return Intl.message(
      'Login',
      name: 'common_login',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get common_username {
    return Intl.message(
      'Username',
      name: 'common_username',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get common_email {
    return Intl.message(
      'Email',
      name: 'common_email',
      desc: '',
      args: [],
    );
  }

  /// `Phone`
  String get common_phone {
    return Intl.message(
      'Phone',
      name: 'common_phone',
      desc: '',
      args: [],
    );
  }

  /// `Website`
  String get common_website {
    return Intl.message(
      'Website',
      name: 'common_website',
      desc: '',
      args: [],
    );
  }

  /// `Address`
  String get common_address {
    return Intl.message(
      'Address',
      name: 'common_address',
      desc: '',
      args: [],
    );
  }

  /// `Office hours`
  String get common_officeHours {
    return Intl.message(
      'Office hours',
      name: 'common_officeHours',
      desc: '',
      args: [],
    );
  }

  /// `Add office hours`
  String get common_addOfficeHours {
    return Intl.message(
      'Add office hours',
      name: 'common_addOfficeHours',
      desc: '',
      args: [],
    );
  }

  /// `Add course`
  String get common_addCourse {
    return Intl.message(
      'Add course',
      name: 'common_addCourse',
      desc: '',
      args: [],
    );
  }

  /// `Course`
  String get common_course {
    return Intl.message(
      'Course',
      name: 'common_course',
      desc: '',
      args: [],
    );
  }

  /// `Courses`
  String get common_courses {
    return Intl.message(
      'Courses',
      name: 'common_courses',
      desc: '',
      args: [],
    );
  }

  /// `No courses`
  String get common_noCourses {
    return Intl.message(
      'No courses',
      name: 'common_noCourses',
      desc: '',
      args: [],
    );
  }

  /// `Go to date`
  String get common_goToDate {
    return Intl.message(
      'Go to date',
      name: 'common_goToDate',
      desc: '',
      args: [],
    );
  }

  /// `Weekday`
  String get common_weekday {
    return Intl.message(
      'Weekday',
      name: 'common_weekday',
      desc: '',
      args: [],
    );
  }

  /// `Previous page`
  String get common_previousPage {
    return Intl.message(
      'Previous page',
      name: 'common_previousPage',
      desc: '',
      args: [],
    );
  }

  /// `Next page`
  String get common_nextPage {
    return Intl.message(
      'Next page',
      name: 'common_nextPage',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get common_password {
    return Intl.message(
      'Password',
      name: 'common_password',
      desc: '',
      args: [],
    );
  }

  /// `Old password`
  String get common_oldPassword {
    return Intl.message(
      'Old password',
      name: 'common_oldPassword',
      desc: '',
      args: [],
    );
  }

  /// `New password`
  String get common_newPassword {
    return Intl.message(
      'New password',
      name: 'common_newPassword',
      desc: '',
      args: [],
    );
  }

  /// `Confirm password`
  String get common_repeatPassword {
    return Intl.message(
      'Confirm password',
      name: 'common_repeatPassword',
      desc: '',
      args: [],
    );
  }

  /// `Create account`
  String get common_createAccount {
    return Intl.message(
      'Create account',
      name: 'common_createAccount',
      desc: '',
      args: [],
    );
  }

  /// `Forgot password?`
  String get common_forgotPassword {
    return Intl.message(
      'Forgot password?',
      name: 'common_forgotPassword',
      desc: '',
      args: [],
    );
  }

  /// `Create timetable`
  String get common_createTimetable {
    return Intl.message(
      'Create timetable',
      name: 'common_createTimetable',
      desc: '',
      args: [],
    );
  }

  /// `Duplicate timetable`
  String get common_duplicateTimetable {
    return Intl.message(
      'Duplicate timetable',
      name: 'common_duplicateTimetable',
      desc: '',
      args: [],
    );
  }

  /// `Rename timetable`
  String get common_renameTimetable {
    return Intl.message(
      'Rename timetable',
      name: 'common_renameTimetable',
      desc: '',
      args: [],
    );
  }

  /// `Delete timetable`
  String get common_deleteTimetable {
    return Intl.message(
      'Delete timetable',
      name: 'common_deleteTimetable',
      desc: '',
      args: [],
    );
  }

  /// `Subscribe to other timetable`
  String get common_subscribeOtherTimetable {
    return Intl.message(
      'Subscribe to other timetable',
      name: 'common_subscribeOtherTimetable',
      desc: '',
      args: [],
    );
  }

  /// `Unsubscribe from this timetable`
  String get common_unsubscribeFromTimetable {
    return Intl.message(
      'Unsubscribe from this timetable',
      name: 'common_unsubscribeFromTimetable',
      desc: '',
      args: [],
    );
  }

  /// `Never`
  String get common_never {
    return Intl.message(
      'Never',
      name: 'common_never',
      desc: '',
      args: [],
    );
  }

  /// `Send`
  String get common_send {
    return Intl.message(
      'Send',
      name: 'common_send',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get common_cancel {
    return Intl.message(
      'Cancel',
      name: 'common_cancel',
      desc: '',
      args: [],
    );
  }

  /// `Ok`
  String get common_ok {
    return Intl.message(
      'Ok',
      name: 'common_ok',
      desc: '',
      args: [],
    );
  }

  /// `Apply`
  String get common_apply {
    return Intl.message(
      'Apply',
      name: 'common_apply',
      desc: '',
      args: [],
    );
  }

  /// `Upload file`
  String get common_uploadFile {
    return Intl.message(
      'Upload file',
      name: 'common_uploadFile',
      desc: '',
      args: [],
    );
  }

  /// `Email is not set`
  String get common_emailNotSet {
    return Intl.message(
      'Email is not set',
      name: 'common_emailNotSet',
      desc: '',
      args: [],
    );
  }

  /// `Change`
  String get common_change {
    return Intl.message(
      'Change',
      name: 'common_change',
      desc: '',
      args: [],
    );
  }

  /// `Change avatar`
  String get common_changeAvatar {
    return Intl.message(
      'Change avatar',
      name: 'common_changeAvatar',
      desc: '',
      args: [],
    );
  }

  /// `Change password`
  String get common_changePassword {
    return Intl.message(
      'Change password',
      name: 'common_changePassword',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get common_profile {
    return Intl.message(
      'Profile',
      name: 'common_profile',
      desc: '',
      args: [],
    );
  }

  /// `Connections`
  String get common_account_connections {
    return Intl.message(
      'Connections',
      name: 'common_account_connections',
      desc: '',
      args: [],
    );
  }

  /// `Apple ID`
  String get common_account_apple {
    return Intl.message(
      'Apple ID',
      name: 'common_account_apple',
      desc: '',
      args: [],
    );
  }

  /// `Facebook`
  String get common_account_facebook {
    return Intl.message(
      'Facebook',
      name: 'common_account_facebook',
      desc: '',
      args: [],
    );
  }

  /// `Google+`
  String get common_account_google {
    return Intl.message(
      'Google+',
      name: 'common_account_google',
      desc: '',
      args: [],
    );
  }

  /// `Sign in with Apple`
  String get common_account_signInWith_apple {
    return Intl.message(
      'Sign in with Apple',
      name: 'common_account_signInWith_apple',
      desc: '',
      args: [],
    );
  }

  /// `Sign in with Facebook`
  String get common_account_signInWith_facebook {
    return Intl.message(
      'Sign in with Facebook',
      name: 'common_account_signInWith_facebook',
      desc: '',
      args: [],
    );
  }

  /// `Sign in with Google`
  String get common_account_signInWith_google {
    return Intl.message(
      'Sign in with Google',
      name: 'common_account_signInWith_google',
      desc: '',
      args: [],
    );
  }

  /// `Link`
  String get common_account_link {
    return Intl.message(
      'Link',
      name: 'common_account_link',
      desc: '',
      args: [],
    );
  }

  /// `Unlink`
  String get common_account_unlink {
    return Intl.message(
      'Unlink',
      name: 'common_account_unlink',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get common_save {
    return Intl.message(
      'Save',
      name: 'common_save',
      desc: '',
      args: [],
    );
  }

  /// `Discard changes?`
  String get common_discardChangesQuestion {
    return Intl.message(
      'Discard changes?',
      name: 'common_discardChangesQuestion',
      desc: '',
      args: [],
    );
  }

  /// `Keep editing`
  String get common_keepEditing {
    return Intl.message(
      'Keep editing',
      name: 'common_keepEditing',
      desc: '',
      args: [],
    );
  }

  /// `Discard`
  String get common_discard {
    return Intl.message(
      'Discard',
      name: 'common_discard',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get common_logout {
    return Intl.message(
      'Logout',
      name: 'common_logout',
      desc: '',
      args: [],
    );
  }

  /// `Archive`
  String get common_archive {
    return Intl.message(
      'Archive',
      name: 'common_archive',
      desc: '',
      args: [],
    );
  }

  /// `Archived`
  String get common_archived {
    return Intl.message(
      'Archived',
      name: 'common_archived',
      desc: '',
      args: [],
    );
  }

  /// `Unarchive`
  String get common_unarchive {
    return Intl.message(
      'Unarchive',
      name: 'common_unarchive',
      desc: '',
      args: [],
    );
  }

  /// `Personalized ads`
  String get common_personalizedAds {
    return Intl.message(
      'Personalized ads',
      name: 'common_personalizedAds',
      desc: '',
      args: [],
    );
  }

  /// `Diagnosis`
  String get common_diagnosis {
    return Intl.message(
      'Diagnosis',
      name: 'common_diagnosis',
      desc: '',
      args: [],
    );
  }

  /// `Analytics`
  String get common_analytics {
    return Intl.message(
      'Analytics',
      name: 'common_analytics',
      desc: '',
      args: [],
    );
  }

  /// `Grid view`
  String get common_gridView {
    return Intl.message(
      'Grid view',
      name: 'common_gridView',
      desc: '',
      args: [],
    );
  }

  /// `List view`
  String get common_listView {
    return Intl.message(
      'List view',
      name: 'common_listView',
      desc: '',
      args: [],
    );
  }

  /// `No internet connection`
  String get common_noInternetConnection {
    return Intl.message(
      'No internet connection',
      name: 'common_noInternetConnection',
      desc: '',
      args: [],
    );
  }

  /// `Please restore your internet connection before you proceed.`
  String get common_pleaseRestoreInternetConnection {
    return Intl.message(
      'Please restore your internet connection before you proceed.',
      name: 'common_pleaseRestoreInternetConnection',
      desc: '',
      args: [],
    );
  }

  /// `Delete account`
  String get common_deleteAccount {
    return Intl.message(
      'Delete account',
      name: 'common_deleteAccount',
      desc: '',
      args: [],
    );
  }

  /// `Credits`
  String get common_credits {
    return Intl.message(
      'Credits',
      name: 'common_credits',
      desc: '',
      args: [],
    );
  }

  /// `Licenses`
  String get common_licenses {
    return Intl.message(
      'Licenses',
      name: 'common_licenses',
      desc: '',
      args: [],
    );
  }

  /// `Filter`
  String get common_filter {
    return Intl.message(
      'Filter',
      name: 'common_filter',
      desc: '',
      args: [],
    );
  }

  /// `Sort`
  String get common_sort {
    return Intl.message(
      'Sort',
      name: 'common_sort',
      desc: '',
      args: [],
    );
  }

  /// `Sort by`
  String get common_sortBy {
    return Intl.message(
      'Sort by',
      name: 'common_sortBy',
      desc: '',
      args: [],
    );
  }

  /// `Latest`
  String get common_sortBy_latest {
    return Intl.message(
      'Latest',
      name: 'common_sortBy_latest',
      desc: '',
      args: [],
    );
  }

  /// `Oldest`
  String get common_sortBy_oldest {
    return Intl.message(
      'Oldest',
      name: 'common_sortBy_oldest',
      desc: '',
      args: [],
    );
  }

  /// `Highest`
  String get common_sortBy_highest {
    return Intl.message(
      'Highest',
      name: 'common_sortBy_highest',
      desc: '',
      args: [],
    );
  }

  /// `Lowest`
  String get common_sortBy_lowest {
    return Intl.message(
      'Lowest',
      name: 'common_sortBy_lowest',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get common_error {
    return Intl.message(
      'Error',
      name: 'common_error',
      desc: '',
      args: [],
    );
  }

  /// `Refresh`
  String get common_refresh {
    return Intl.message(
      'Refresh',
      name: 'common_refresh',
      desc: '',
      args: [],
    );
  }

  /// `No internet connection. Please check that your device is connected to the Wi-Fi or activate your mobile data transfer.`
  String get common_noInternet_message {
    return Intl.message(
      'No internet connection. Please check that your device is connected to the Wi-Fi or activate your mobile data transfer.',
      name: 'common_noInternet_message',
      desc: '',
      args: [],
    );
  }

  /// `Something went wrong`
  String get common_somethingWentWrong {
    return Intl.message(
      'Something went wrong',
      name: 'common_somethingWentWrong',
      desc: '',
      args: [],
    );
  }

  /// `The operation has failed. Please try again later.`
  String get common_operationFailedTryAgainLater {
    return Intl.message(
      'The operation has failed. Please try again later.',
      name: 'common_operationFailedTryAgainLater',
      desc: '',
      args: [],
    );
  }

  /// `Account connected!`
  String get confirmation_accountConnected {
    return Intl.message(
      'Account connected!',
      name: 'confirmation_accountConnected',
      desc: '',
      args: [],
    );
  }

  /// `Account disconnected!`
  String get confirmation_accountDisconnected {
    return Intl.message(
      'Account disconnected!',
      name: 'confirmation_accountDisconnected',
      desc: '',
      args: [],
    );
  }

  /// `Password changed!`
  String get confirmation_passwordChanged {
    return Intl.message(
      'Password changed!',
      name: 'confirmation_passwordChanged',
      desc: '',
      args: [],
    );
  }

  /// `Appearance rewarded!`
  String get confirmation_themeUnlocked {
    return Intl.message(
      'Appearance rewarded!',
      name: 'confirmation_themeUnlocked',
      desc: '',
      args: [],
    );
  }

  /// `Saved!`
  String get confirmation_saveSuccess {
    return Intl.message(
      'Saved!',
      name: 'confirmation_saveSuccess',
      desc: '',
      args: [],
    );
  }

  /// `Deleted!`
  String get confirmation_deleted {
    return Intl.message(
      'Deleted!',
      name: 'confirmation_deleted',
      desc: '',
      args: [],
    );
  }

  /// `Archived!`
  String get confirmation_archived {
    return Intl.message(
      'Archived!',
      name: 'confirmation_archived',
      desc: '',
      args: [],
    );
  }

  /// `Unarchived!`
  String get confirmation_unarchived {
    return Intl.message(
      'Unarchived!',
      name: 'confirmation_unarchived',
      desc: '',
      args: [],
    );
  }

  /// `Subscribed!`
  String get confirmation_subscribed {
    return Intl.message(
      'Subscribed!',
      name: 'confirmation_subscribed',
      desc: '',
      args: [],
    );
  }

  /// `Unsubscribed!`
  String get confirmation_unsubscribed {
    return Intl.message(
      'Unsubscribed!',
      name: 'confirmation_unsubscribed',
      desc: '',
      args: [],
    );
  }

  /// `Red`
  String get color_red {
    return Intl.message(
      'Red',
      name: 'color_red',
      desc: '',
      args: [],
    );
  }

  /// `Orange`
  String get color_orange {
    return Intl.message(
      'Orange',
      name: 'color_orange',
      desc: '',
      args: [],
    );
  }

  /// `Yellow`
  String get color_yellow {
    return Intl.message(
      'Yellow',
      name: 'color_yellow',
      desc: '',
      args: [],
    );
  }

  /// `Green`
  String get color_green {
    return Intl.message(
      'Green',
      name: 'color_green',
      desc: '',
      args: [],
    );
  }

  /// `Teal`
  String get color_teal {
    return Intl.message(
      'Teal',
      name: 'color_teal',
      desc: '',
      args: [],
    );
  }

  /// `Blue`
  String get color_blue {
    return Intl.message(
      'Blue',
      name: 'color_blue',
      desc: '',
      args: [],
    );
  }

  /// `Light blue`
  String get color_lightBlue {
    return Intl.message(
      'Light blue',
      name: 'color_lightBlue',
      desc: '',
      args: [],
    );
  }

  /// `Purple`
  String get color_purple {
    return Intl.message(
      'Purple',
      name: 'color_purple',
      desc: '',
      args: [],
    );
  }

  /// `Pink`
  String get color_pink {
    return Intl.message(
      'Pink',
      name: 'color_pink',
      desc: '',
      args: [],
    );
  }

  /// `Black`
  String get color_black {
    return Intl.message(
      'Black',
      name: 'color_black',
      desc: '',
      args: [],
    );
  }

  /// `Category`
  String get common_category {
    return Intl.message(
      'Category',
      name: 'common_category',
      desc: '',
      args: [],
    );
  }

  /// `Grade category`
  String get common_gradeCategory {
    return Intl.message(
      'Grade category',
      name: 'common_gradeCategory',
      desc: '',
      args: [],
    );
  }

  /// `Grade average`
  String get common_gradeAverage {
    return Intl.message(
      'Grade average',
      name: 'common_gradeAverage',
      desc: '',
      args: [],
    );
  }

  /// `Times`
  String get common_times {
    return Intl.message(
      'Times',
      name: 'common_times',
      desc: '',
      args: [],
    );
  }

  /// `Until`
  String get common_until {
    return Intl.message(
      'Until',
      name: 'common_until',
      desc: '',
      args: [],
    );
  }

  /// `Timetable`
  String get common_timetable {
    return Intl.message(
      'Timetable',
      name: 'common_timetable',
      desc: '',
      args: [],
    );
  }

  /// `Agenda`
  String get common_agenda {
    return Intl.message(
      'Agenda',
      name: 'common_agenda',
      desc: '',
      args: [],
    );
  }

  /// `Time of day`
  String get common_timeOfDay {
    return Intl.message(
      'Time of day',
      name: 'common_timeOfDay',
      desc: '',
      args: [],
    );
  }

  /// `Every day`
  String get common_everyDay {
    return Intl.message(
      'Every day',
      name: 'common_everyDay',
      desc: '',
      args: [],
    );
  }

  /// `Days of week`
  String get common_daysOfWeek {
    return Intl.message(
      'Days of week',
      name: 'common_daysOfWeek',
      desc: '',
      args: [],
    );
  }

  /// `Avatar`
  String get common_avatar {
    return Intl.message(
      'Avatar',
      name: 'common_avatar',
      desc: '',
      args: [],
    );
  }

  /// `Gallery`
  String get common_gallery {
    return Intl.message(
      'Gallery',
      name: 'common_gallery',
      desc: '',
      args: [],
    );
  }

  /// `Take photo`
  String get common_takePhoto {
    return Intl.message(
      'Take photo',
      name: 'common_takePhoto',
      desc: '',
      args: [],
    );
  }

  /// `Delete current photo`
  String get common_deleteCurrentPhoto {
    return Intl.message(
      'Delete current photo',
      name: 'common_deleteCurrentPhoto',
      desc: '',
      args: [],
    );
  }

  /// `Save recurring event`
  String get common_saveRecurringEvent {
    return Intl.message(
      'Save recurring event',
      name: 'common_saveRecurringEvent',
      desc: '',
      args: [],
    );
  }

  /// `Delete recurring event`
  String get common_deleteRecurringEvent {
    return Intl.message(
      'Delete recurring event',
      name: 'common_deleteRecurringEvent',
      desc: '',
      args: [],
    );
  }

  /// `Every day`
  String get repeatOptions_everyDay {
    return Intl.message(
      'Every day',
      name: 'repeatOptions_everyDay',
      desc: '',
      args: [],
    );
  }

  /// `Every %s days`
  String get repeatOptions_everyNDays {
    return Intl.message(
      'Every %s days',
      name: 'repeatOptions_everyNDays',
      desc: '',
      args: [],
    );
  }

  /// `Every week`
  String get repeatOptions_everyWeek {
    return Intl.message(
      'Every week',
      name: 'repeatOptions_everyWeek',
      desc: '',
      args: [],
    );
  }

  /// `Every %s weeks`
  String get repeatOptions_everyNWeeks {
    return Intl.message(
      'Every %s weeks',
      name: 'repeatOptions_everyNWeeks',
      desc: '',
      args: [],
    );
  }

  /// `Every month`
  String get repeatOptions_everyMonth {
    return Intl.message(
      'Every month',
      name: 'repeatOptions_everyMonth',
      desc: '',
      args: [],
    );
  }

  /// `Every %s months`
  String get repeatOptions_everyNMonths {
    return Intl.message(
      'Every %s months',
      name: 'repeatOptions_everyNMonths',
      desc: '',
      args: [],
    );
  }

  /// `Does not repeat`
  String get repeatOptions_doesNotRepeat {
    return Intl.message(
      'Does not repeat',
      name: 'repeatOptions_doesNotRepeat',
      desc: '',
      args: [],
    );
  }

  /// `Custom`
  String get repeatOptions_custom {
    return Intl.message(
      'Custom',
      name: 'repeatOptions_custom',
      desc: '',
      args: [],
    );
  }

  /// `This event`
  String get recurringEvent_thisEvent {
    return Intl.message(
      'This event',
      name: 'recurringEvent_thisEvent',
      desc: '',
      args: [],
    );
  }

  /// `This and following events`
  String get recurringEvent_thisAndFollowingEvents {
    return Intl.message(
      'This and following events',
      name: 'recurringEvent_thisAndFollowingEvents',
      desc: '',
      args: [],
    );
  }

  /// `All events`
  String get recurringEvent_allEvents {
    return Intl.message(
      'All events',
      name: 'recurringEvent_allEvents',
      desc: '',
      args: [],
    );
  }

  /// `Repeats every`
  String get repeatOptionsPage_repeatsEvery {
    return Intl.message(
      'Repeats every',
      name: 'repeatOptionsPage_repeatsEvery',
      desc: '',
      args: [],
    );
  }

  /// `Repeats on`
  String get repeatOptionsPage_repeatsOn {
    return Intl.message(
      'Repeats on',
      name: 'repeatOptionsPage_repeatsOn',
      desc: '',
      args: [],
    );
  }

  /// `Ends`
  String get repeatOptionsPage_ends {
    return Intl.message(
      'Ends',
      name: 'repeatOptionsPage_ends',
      desc: '',
      args: [],
    );
  }

  /// `On`
  String get repeatOptionsPage_onDate {
    return Intl.message(
      'On',
      name: 'repeatOptionsPage_onDate',
      desc: '',
      args: [],
    );
  }

  /// `day`
  String get repeatOptionsPage_day {
    return Intl.message(
      'day',
      name: 'repeatOptionsPage_day',
      desc: '',
      args: [],
    );
  }

  /// `days`
  String get repeatOptionsPage_days {
    return Intl.message(
      'days',
      name: 'repeatOptionsPage_days',
      desc: '',
      args: [],
    );
  }

  /// `week`
  String get repeatOptionsPage_week {
    return Intl.message(
      'week',
      name: 'repeatOptionsPage_week',
      desc: '',
      args: [],
    );
  }

  /// `weeks`
  String get repeatOptionsPage_weeks {
    return Intl.message(
      'weeks',
      name: 'repeatOptionsPage_weeks',
      desc: '',
      args: [],
    );
  }

  /// `month`
  String get repeatOptionsPage_month {
    return Intl.message(
      'month',
      name: 'repeatOptionsPage_month',
      desc: '',
      args: [],
    );
  }

  /// `months`
  String get repeatOptionsPage_months {
    return Intl.message(
      'months',
      name: 'repeatOptionsPage_months',
      desc: '',
      args: [],
    );
  }

  /// `After %s occurrence`
  String get repeatOptionsPage_afterOneOccurrence {
    return Intl.message(
      'After %s occurrence',
      name: 'repeatOptionsPage_afterOneOccurrence',
      desc: '',
      args: [],
    );
  }

  /// `After %s occurrences`
  String get repeatOptionsPage_afterNOccurrences {
    return Intl.message(
      'After %s occurrences',
      name: 'repeatOptionsPage_afterNOccurrences',
      desc: '',
      args: [],
    );
  }

  /// `Login failed!`
  String get loginOptions_loginFailed_title {
    return Intl.message(
      'Login failed!',
      name: 'loginOptions_loginFailed_title',
      desc: '',
      args: [],
    );
  }

  /// `Something went wrong. The login attempt was unsuccessful. Please try again later.`
  String get loginOptions_loginFailed_message {
    return Intl.message(
      'Something went wrong. The login attempt was unsuccessful. Please try again later.',
      name: 'loginOptions_loginFailed_message',
      desc: '',
      args: [],
    );
  }

  /// `Welcome back!`
  String get login_header {
    return Intl.message(
      'Welcome back!',
      name: 'login_header',
      desc: '',
      args: [],
    );
  }

  /// `Forgot password`
  String get login_forgotPassword_header {
    return Intl.message(
      'Forgot password',
      name: 'login_forgotPassword_header',
      desc: '',
      args: [],
    );
  }

  /// `Don’t worry. Please enter your email and we will send you instructions how to reset your password.`
  String get login_forgotPassword_message {
    return Intl.message(
      'Don’t worry. Please enter your email and we will send you instructions how to reset your password.',
      name: 'login_forgotPassword_message',
      desc: '',
      args: [],
    );
  }

  /// `Instructions sent!`
  String get login_resetPassword_header {
    return Intl.message(
      'Instructions sent!',
      name: 'login_resetPassword_header',
      desc: '',
      args: [],
    );
  }

  /// `Please check your inbox and spam folder for instructions how to change your password.`
  String get login_resetPassword_message {
    return Intl.message(
      'Please check your inbox and spam folder for instructions how to change your password.',
      name: 'login_resetPassword_message',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to log out? Your data won’t be synchronized anymore on this device.`
  String get profile_logout_message {
    return Intl.message(
      'Are you sure you want to log out? Your data won’t be synchronized anymore on this device.',
      name: 'profile_logout_message',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete your account? Your data won’t be synchronized anymore. After you delete your account we will remove all your data stored in the cloud. Your local data on the device won’t be affected.`
  String get profile_deleteAccount_message {
    return Intl.message(
      'Are you sure you want to delete your account? Your data won’t be synchronized anymore. After you delete your account we will remove all your data stored in the cloud. Your local data on the device won’t be affected.',
      name: 'profile_deleteAccount_message',
      desc: '',
      args: [],
    );
  }

  /// `Manage your profile`
  String get settings_account_label {
    return Intl.message(
      'Manage your profile',
      name: 'settings_account_label',
      desc: '',
      args: [],
    );
  }

  /// `Create an account, it’s free!`
  String get settings_accountPlaceholder_message {
    return Intl.message(
      'Create an account, it’s free!',
      name: 'settings_accountPlaceholder_message',
      desc: '',
      args: [],
    );
  }

  /// `Create or login`
  String get settings_accountPlaceholder_button {
    return Intl.message(
      'Create or login',
      name: 'settings_accountPlaceholder_button',
      desc: '',
      args: [],
    );
  }

  /// `No ads. No interruptions`
  String get settings_premium_label {
    return Intl.message(
      'No ads. No interruptions',
      name: 'settings_premium_label',
      desc: '',
      args: [],
    );
  }

  /// `Use School Pen on other platforms`
  String get settings_pcOrWeb_label {
    return Intl.message(
      'Use School Pen on other platforms',
      name: 'settings_pcOrWeb_label',
      desc: '',
      args: [],
    );
  }

  /// `Sync data across multiple devices`
  String get settings_syncAndRestore_label {
    return Intl.message(
      'Sync data across multiple devices',
      name: 'settings_syncAndRestore_label',
      desc: '',
      args: [],
    );
  }

  /// `A new pen for a new year`
  String get settings_schoolYears_label {
    return Intl.message(
      'A new pen for a new year',
      name: 'settings_schoolYears_label',
      desc: '',
      args: [],
    );
  }

  /// `Divide your school year into parts`
  String get settings_semesters_label {
    return Intl.message(
      'Divide your school year into parts',
      name: 'settings_semesters_label',
      desc: '',
      args: [],
    );
  }

  /// `Don’t miss important events`
  String get settings_notifications_label {
    return Intl.message(
      'Don’t miss important events',
      name: 'settings_notifications_label',
      desc: '',
      args: [],
    );
  }

  /// `Decide how much you share with us`
  String get settings_privacy_label {
    return Intl.message(
      'Decide how much you share with us',
      name: 'settings_privacy_label',
      desc: '',
      args: [],
    );
  }

  /// `Where to get help`
  String get settings_support_label {
    return Intl.message(
      'Where to get help',
      name: 'settings_support_label',
      desc: '',
      args: [],
    );
  }

  /// `Connect third party applications`
  String get settings_integrations_label {
    return Intl.message(
      'Connect third party applications',
      name: 'settings_integrations_label',
      desc: '',
      args: [],
    );
  }

  /// `If you enjoy the app, help us by leaving a review in the market`
  String get settings_rateUs_label {
    return Intl.message(
      'If you enjoy the app, help us by leaving a review in the market',
      name: 'settings_rateUs_label',
      desc: '',
      args: [],
    );
  }

  /// `Invite your friends to use the app`
  String get settings_share_label {
    return Intl.message(
      'Invite your friends to use the app',
      name: 'settings_share_label',
      desc: '',
      args: [],
    );
  }

  /// `Version %s`
  String get settings_about_label {
    return Intl.message(
      'Version %s',
      name: 'settings_about_label',
      desc: '',
      args: [],
    );
  }

  /// `Unlock additional themes by watching ads or go premium.`
  String get settings_themes_unlockByAdsOrGoPremium {
    return Intl.message(
      'Unlock additional themes by watching ads or go premium.',
      name: 'settings_themes_unlockByAdsOrGoPremium',
      desc: '',
      args: [],
    );
  }

  /// `Failed to load the rewarded ad. Please try again later`
  String get settings_themes_adFailedToLoad_message {
    return Intl.message(
      'Failed to load the rewarded ad. Please try again later',
      name: 'settings_themes_adFailedToLoad_message',
      desc: '',
      args: [],
    );
  }

  /// `This option allows us to collect and process information about how you interact with our app. We use this data for analytical purposes in order to understand our users and improve the app.`
  String get settings_privacy_analytics_description {
    return Intl.message(
      'This option allows us to collect and process information about how you interact with our app. We use this data for analytical purposes in order to understand our users and improve the app.',
      name: 'settings_privacy_analytics_description',
      desc: '',
      args: [],
    );
  }

  /// `This option allows us to collect and process anonymous crash reports. We use them in order to improve the stability and overall user experience.`
  String get settings_privacy_diagnosis_description {
    return Intl.message(
      'This option allows us to collect and process anonymous crash reports. We use them in order to improve the stability and overall user experience.',
      name: 'settings_privacy_diagnosis_description',
      desc: '',
      args: [],
    );
  }

  /// `This option allows us and our advertising partners to collect certain information about you in order to serve ads that are relevant to you.`
  String get settings_privacy_personalizedAds_description {
    return Intl.message(
      'This option allows us and our advertising partners to collect certain information about you in order to serve ads that are relevant to you.',
      name: 'settings_privacy_personalizedAds_description',
      desc: '',
      args: [],
    );
  }

  /// `School Years help you to divide your school stuff into pieces so that you can manage smaller parts of it. E.g. you can have separate notes or courses per each school year.`
  String get settings_schoolYears_description {
    return Intl.message(
      'School Years help you to divide your school stuff into pieces so that you can manage smaller parts of it. E.g. you can have separate notes or courses per each school year.',
      name: 'settings_schoolYears_description',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this school year? Deleting it will also remove all associated data as well, including semesters, notes and courses for that year.`
  String get settings_schoolYears_deleteDialog_message {
    return Intl.message(
      'Are you sure you want to delete this school year? Deleting it will also remove all associated data as well, including semesters, notes and courses for that year.',
      name: 'settings_schoolYears_deleteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `Set as current year`
  String get settings_schoolYears_setAsCurrent {
    return Intl.message(
      'Set as current year',
      name: 'settings_schoolYears_setAsCurrent',
      desc: '',
      args: [],
    );
  }

  /// `Terms help you divide your school year into pieces so that you can manage smaller parts of it. Imagine a summer term and a winter term, wouldn’t that be nice?`
  String get settings_semesters_description {
    return Intl.message(
      'Terms help you divide your school year into pieces so that you can manage smaller parts of it. Imagine a summer term and a winter term, wouldn’t that be nice?',
      name: 'settings_semesters_description',
      desc: '',
      args: [],
    );
  }

  /// `Set as current term`
  String get settings_semesters_setAsCurrent {
    return Intl.message(
      'Set as current term',
      name: 'settings_semesters_setAsCurrent',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this term? Deleting it will also remove all associated data as well, including attendance and grades for that term.`
  String get settings_semesters_deleteDialog_message {
    return Intl.message(
      'Are you sure you want to delete this term? Deleting it will also remove all associated data as well, including attendance and grades for that term.',
      name: 'settings_semesters_deleteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `Notify even if there are no events`
  String get settings_notifications_notifyIfNoEvents {
    return Intl.message(
      'Notify even if there are no events',
      name: 'settings_notifications_notifyIfNoEvents',
      desc: '',
      args: [],
    );
  }

  /// `Do not notify if there are no events`
  String get settings_notifications_doNotNotifyIfNoEvents {
    return Intl.message(
      'Do not notify if there are no events',
      name: 'settings_notifications_doNotNotifyIfNoEvents',
      desc: '',
      args: [],
    );
  }

  /// `Reminder about upcoming lessons`
  String get settings_notifications_reminderAboutUpcomingLessons {
    return Intl.message(
      'Reminder about upcoming lessons',
      name: 'settings_notifications_reminderAboutUpcomingLessons',
      desc: '',
      args: [],
    );
  }

  /// `When a lesson starts`
  String get settings_notifications_whenLessonStarts {
    return Intl.message(
      'When a lesson starts',
      name: 'settings_notifications_whenLessonStarts',
      desc: '',
      args: [],
    );
  }

  /// `%s minutes before`
  String get settings_notifications_minutesBefore {
    return Intl.message(
      '%s minutes before',
      name: 'settings_notifications_minutesBefore',
      desc: '',
      args: [],
    );
  }

  /// `Notes help you organize your thoughts! Lets create your first note.`
  String get notes_emptyState_message {
    return Intl.message(
      'Notes help you organize your thoughts! Lets create your first note.',
      name: 'notes_emptyState_message',
      desc: '',
      args: [],
    );
  }

  /// `Show archived notes`
  String get notes_filter_showArchivedNotes {
    return Intl.message(
      'Show archived notes',
      name: 'notes_filter_showArchivedNotes',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this note? You won’t be able to restore it.`
  String get notes_deleteDialog_message {
    return Intl.message(
      'Are you sure you want to delete this note? You won’t be able to restore it.',
      name: 'notes_deleteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this exam? You won’t be able to restore it.`
  String get exams_deleteDialog_message {
    return Intl.message(
      'Are you sure you want to delete this exam? You won’t be able to restore it.',
      name: 'exams_deleteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this homework? You won’t be able to restore it.`
  String get homework_deleteDialog_message {
    return Intl.message(
      'Are you sure you want to delete this homework? You won’t be able to restore it.',
      name: 'homework_deleteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this reminder? You won’t be able to restore it.`
  String get reminders_deleteDialog_message {
    return Intl.message(
      'Are you sure you want to delete this reminder? You won’t be able to restore it.',
      name: 'reminders_deleteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `No teachers found. Add your first teacher using the button.`
  String get teachers_emptyState_message {
    return Intl.message(
      'No teachers found. Add your first teacher using the button.',
      name: 'teachers_emptyState_message',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this teacher? You won’t be able to restore it.`
  String get teachers_deleteDialog_message {
    return Intl.message(
      'Are you sure you want to delete this teacher? You won’t be able to restore it.',
      name: 'teachers_deleteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `No friends found. Add your first friend using the button.`
  String get friends_emptyState_message {
    return Intl.message(
      'No friends found. Add your first friend using the button.',
      name: 'friends_emptyState_message',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this friend? You won’t be able to restore it.`
  String get friends_deleteDialog_message {
    return Intl.message(
      'Are you sure you want to delete this friend? You won’t be able to restore it.',
      name: 'friends_deleteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `%s has invited you to become friends. Accept or decline the invitation.`
  String get friends_invitationFromUser_message {
    return Intl.message(
      '%s has invited you to become friends. Accept or decline the invitation.',
      name: 'friends_invitationFromUser_message',
      desc: '',
      args: [],
    );
  }

  /// `Please enter your friend’s e-mail address to send an invitation.`
  String get friends_inviteDialog_message {
    return Intl.message(
      'Please enter your friend’s e-mail address to send an invitation.',
      name: 'friends_inviteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `Grading system set automatically to %s`
  String get courses_gradingSystemSet_message {
    return Intl.message(
      'Grading system set automatically to %s',
      name: 'courses_gradingSystemSet_message',
      desc: '',
      args: [],
    );
  }

  /// `Add your first course to track your grades, set up timetable and upload files.`
  String get courses_emptyState_message {
    return Intl.message(
      'Add your first course to track your grades, set up timetable and upload files.',
      name: 'courses_emptyState_message',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this course? All related grades, lessons and files will be deleted as well.`
  String get courses_deleteDialog_message {
    return Intl.message(
      'Are you sure you want to delete this course? All related grades, lessons and files will be deleted as well.',
      name: 'courses_deleteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `Please enter your target grade you wish to achieve.`
  String get courses_gradeObjectiveDialog_message {
    return Intl.message(
      'Please enter your target grade you wish to achieve.',
      name: 'courses_gradeObjectiveDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `%s/%s active`
  String get courses_activeEventsFormat {
    return Intl.message(
      '%s/%s active',
      name: 'courses_activeEventsFormat',
      desc: '',
      args: [],
    );
  }

  /// `%s overdue ⚠`
  String get courses_overdueEventsFormat {
    return Intl.message(
      '%s overdue ⚠',
      name: 'courses_overdueEventsFormat',
      desc: '',
      args: [],
    );
  }

  /// `Timetable helps you to organize your daily schedule.`
  String get timetables_emptyState_message {
    return Intl.message(
      'Timetable helps you to organize your daily schedule.',
      name: 'timetables_emptyState_message',
      desc: '',
      args: [],
    );
  }

  /// `Start now by creating your own timetable or subscribe to one that is publicly available.`
  String get timetables_emptyState_subscribeMessage {
    return Intl.message(
      'Start now by creating your own timetable or subscribe to one that is publicly available.',
      name: 'timetables_emptyState_subscribeMessage',
      desc: '',
      args: [],
    );
  }

  /// `We weren’t able to find your timetable. This might mean that it was deleted.`
  String get timetables_notFoundState_message {
    return Intl.message(
      'We weren’t able to find your timetable. This might mean that it was deleted.',
      name: 'timetables_notFoundState_message',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this timetable? You won’t be able to restore it.`
  String get timetables_deleteDialog_message {
    return Intl.message(
      'Are you sure you want to delete this timetable? You won’t be able to restore it.',
      name: 'timetables_deleteDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `Day`
  String get timetable_viewMode_day {
    return Intl.message(
      'Day',
      name: 'timetable_viewMode_day',
      desc: '',
      args: [],
    );
  }

  /// `3 days`
  String get timetable_viewMode_threeDays {
    return Intl.message(
      '3 days',
      name: 'timetable_viewMode_threeDays',
      desc: '',
      args: [],
    );
  }

  /// `Week`
  String get timetable_viewMode_week {
    return Intl.message(
      'Week',
      name: 'timetable_viewMode_week',
      desc: '',
      args: [],
    );
  }

  /// `Day view`
  String get timetable_viewMode_dayView {
    return Intl.message(
      'Day view',
      name: 'timetable_viewMode_dayView',
      desc: '',
      args: [],
    );
  }

  /// `3 days view`
  String get timetable_viewMode_threeDaysView {
    return Intl.message(
      '3 days view',
      name: 'timetable_viewMode_threeDaysView',
      desc: '',
      args: [],
    );
  }

  /// `Week view`
  String get timetable_viewMode_weekView {
    return Intl.message(
      'Week view',
      name: 'timetable_viewMode_weekView',
      desc: '',
      args: [],
    );
  }

  /// `Percentage`
  String get gradingSystem_percentage {
    return Intl.message(
      'Percentage',
      name: 'gradingSystem_percentage',
      desc: '',
      args: [],
    );
  }

  /// `Numeric`
  String get gradingSystem_numeric {
    return Intl.message(
      'Numeric',
      name: 'gradingSystem_numeric',
      desc: '',
      args: [],
    );
  }

  /// `Alphabetic`
  String get gradingSystem_alphabetic {
    return Intl.message(
      'Alphabetic',
      name: 'gradingSystem_alphabetic',
      desc: '',
      args: [],
    );
  }

  /// `Lowest grade`
  String get gradingSystem_lowestGrade {
    return Intl.message(
      'Lowest grade',
      name: 'gradingSystem_lowestGrade',
      desc: '',
      args: [],
    );
  }

  /// `Highest grade`
  String get gradingSystem_highestGrade {
    return Intl.message(
      'Highest grade',
      name: 'gradingSystem_highestGrade',
      desc: '',
      args: [],
    );
  }

  /// `Lowest grade must not be empty.`
  String get gradingSystem_error_lowestGradeMustBeFilled {
    return Intl.message(
      'Lowest grade must not be empty.',
      name: 'gradingSystem_error_lowestGradeMustBeFilled',
      desc: '',
      args: [],
    );
  }

  /// `Highest grade must not be empty.`
  String get gradingSystem_error_highestGradeMustBeFilled {
    return Intl.message(
      'Highest grade must not be empty.',
      name: 'gradingSystem_error_highestGradeMustBeFilled',
      desc: '',
      args: [],
    );
  }

  /// `Lowest grade must be different than highest grade.`
  String get gradingSystem_error_minValueCannotEqualMaxValue {
    return Intl.message(
      'Lowest grade must be different than highest grade.',
      name: 'gradingSystem_error_minValueCannotEqualMaxValue',
      desc: '',
      args: [],
    );
  }

  /// `(Unnamed)`
  String get grades_unnamed {
    return Intl.message(
      '(Unnamed)',
      name: 'grades_unnamed',
      desc: '',
      args: [],
    );
  }

  /// `Weight (1.0 = 100%)`
  String get grades_weight_placeholder {
    return Intl.message(
      'Weight (1.0 = 100%)',
      name: 'grades_weight_placeholder',
      desc: '',
      args: [],
    );
  }

  /// `w. %s`
  String get grades_weight_short {
    return Intl.message(
      'w. %s',
      name: 'grades_weight_short',
      desc: '',
      args: [],
    );
  }

  /// `Please enter the name of your custom grade category.`
  String get grades_gradeCategoryDialog_message {
    return Intl.message(
      'Please enter the name of your custom grade category.',
      name: 'grades_gradeCategoryDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this grade?`
  String get grades_deletedDialog_message {
    return Intl.message(
      'Are you sure you want to delete this grade?',
      name: 'grades_deletedDialog_message',
      desc: '',
      args: [],
    );
  }

  /// `Written`
  String get gradeCategory_written {
    return Intl.message(
      'Written',
      name: 'gradeCategory_written',
      desc: '',
      args: [],
    );
  }

  /// `Oral`
  String get gradeCategory_oral {
    return Intl.message(
      'Oral',
      name: 'gradeCategory_oral',
      desc: '',
      args: [],
    );
  }

  /// `Practical`
  String get gradeCategory_practical {
    return Intl.message(
      'Practical',
      name: 'gradeCategory_practical',
      desc: '',
      args: [],
    );
  }

  /// `Add a new category`
  String get gradeCategory_addNew {
    return Intl.message(
      'Add a new category',
      name: 'gradeCategory_addNew',
      desc: '',
      args: [],
    );
  }

  /// `%s of %s events done`
  String get home_eventsDone {
    return Intl.message(
      '%s of %s events done',
      name: 'home_eventsDone',
      desc: '',
      args: [],
    );
  }

  /// `No scheduled events. You have a free day!`
  String get home_emptyState_message {
    return Intl.message(
      'No scheduled events. You have a free day!',
      name: 'home_emptyState_message',
      desc: '',
      args: [],
    );
  }

  /// `You are all done!`
  String get home_doneState_message {
    return Intl.message(
      'You are all done!',
      name: 'home_doneState_message',
      desc: '',
      args: [],
    );
  }

  /// `When you’ll receive a notification, it’ll appear here!`
  String get notifications_emptyState_message {
    return Intl.message(
      'When you’ll receive a notification, it’ll appear here!',
      name: 'notifications_emptyState_message',
      desc: '',
      args: [],
    );
  }

  /// `Rate this app`
  String get rateApp_title {
    return Intl.message(
      'Rate this app',
      name: 'rateApp_title',
      desc: '',
      args: [],
    );
  }

  /// `Are you enjoying School Pen? Maybe you'd like to pass a valuable feedback to improve the app? Don't hesitate.`
  String get rateApp_message {
    return Intl.message(
      'Are you enjoying School Pen? Maybe you\'d like to pass a valuable feedback to improve the app? Don\'t hesitate.',
      name: 'rateApp_message',
      desc: '',
      args: [],
    );
  }

  /// `Passwords do not match.`
  String get error_passwordsDoNotMatch {
    return Intl.message(
      'Passwords do not match.',
      name: 'error_passwordsDoNotMatch',
      desc: '',
      args: [],
    );
  }

  /// `Wrong password.`
  String get error_invalidPassword {
    return Intl.message(
      'Wrong password.',
      name: 'error_invalidPassword',
      desc: '',
      args: [],
    );
  }

  /// `A different account is already connected with this service.`
  String get error_accountAlreadyLinked {
    return Intl.message(
      'A different account is already connected with this service.',
      name: 'error_accountAlreadyLinked',
      desc: '',
      args: [],
    );
  }

  /// `Max upload size for the avatar image is 5 MB.`
  String get error_avatarSizeTooBig {
    return Intl.message(
      'Max upload size for the avatar image is 5 MB.',
      name: 'error_avatarSizeTooBig',
      desc: '',
      args: [],
    );
  }

  /// `Password cannot contain username.`
  String get error_passwordCannotContainUsername {
    return Intl.message(
      'Password cannot contain username.',
      name: 'error_passwordCannotContainUsername',
      desc: '',
      args: [],
    );
  }

  /// `Too many failed login attempts. Please try again later.`
  String get error_tooManyFailedLoginAttempts {
    return Intl.message(
      'Too many failed login attempts. Please try again later.',
      name: 'error_tooManyFailedLoginAttempts',
      desc: '',
      args: [],
    );
  }

  /// `This username is already taken.`
  String get error_usernameTaken {
    return Intl.message(
      'This username is already taken.',
      name: 'error_usernameTaken',
      desc: '',
      args: [],
    );
  }

  /// `The minimum username length is %s symbols.`
  String get error_usernameTooShort {
    return Intl.message(
      'The minimum username length is %s symbols.',
      name: 'error_usernameTooShort',
      desc: '',
      args: [],
    );
  }

  /// `Field must not be empty.`
  String get error_mustNotBeEmpty {
    return Intl.message(
      'Field must not be empty.',
      name: 'error_mustNotBeEmpty',
      desc: '',
      args: [],
    );
  }

  /// `Value must be between %s and %s.`
  String get error_valueMustBeBetween {
    return Intl.message(
      'Value must be between %s and %s.',
      name: 'error_valueMustBeBetween',
      desc: '',
      args: [],
    );
  }

  /// `Value must be one of %s.`
  String get error_valueMustBeOneOf {
    return Intl.message(
      'Value must be one of %s.',
      name: 'error_valueMustBeOneOf',
      desc: '',
      args: [],
    );
  }

  /// `The email is invalid.`
  String get error_invalidEmail {
    return Intl.message(
      'The email is invalid.',
      name: 'error_invalidEmail',
      desc: '',
      args: [],
    );
  }

  /// `The minimum password length is %s symbols.`
  String get error_passwordTooShort {
    return Intl.message(
      'The minimum password length is %s symbols.',
      name: 'error_passwordTooShort',
      desc: '',
      args: [],
    );
  }

  /// `An account with the given email already exists.`
  String get error_emailTaken {
    return Intl.message(
      'An account with the given email already exists.',
      name: 'error_emailTaken',
      desc: '',
      args: [],
    );
  }

  /// `Something went wrong.`
  String get error_somethingWentWrong {
    return Intl.message(
      'Something went wrong.',
      name: 'error_somethingWentWrong',
      desc: '',
      args: [],
    );
  }

  /// `Wrong email or password.`
  String get error_invalidCredentials {
    return Intl.message(
      'Wrong email or password.',
      name: 'error_invalidCredentials',
      desc: '',
      args: [],
    );
  }

  /// `No internet connection.`
  String get error_noInternet {
    return Intl.message(
      'No internet connection.',
      name: 'error_noInternet',
      desc: '',
      args: [],
    );
  }

  /// `Start date must be before end date.`
  String get error_startDateMustBeBeforeEndDate {
    return Intl.message(
      'Start date must be before end date.',
      name: 'error_startDateMustBeBeforeEndDate',
      desc: '',
      args: [],
    );
  }

  /// `At least one value must be selected`
  String get error_atLeastOneValueMustBeSelected {
    return Intl.message(
      'At least one value must be selected',
      name: 'error_atLeastOneValueMustBeSelected',
      desc: '',
      args: [],
    );
  }

  /// `Close`
  String get common_close {
    return Intl.message(
      'Close',
      name: 'common_close',
      desc: '',
      args: [],
    );
  }

  /// `No active events`
  String get common_noActiveEvents {
    return Intl.message(
      'No active events',
      name: 'common_noActiveEvents',
      desc: '',
      args: [],
    );
  }

  /// `Recent grades`
  String get common_recentGrades {
    return Intl.message(
      'Recent grades',
      name: 'common_recentGrades',
      desc: '',
      args: [],
    );
  }

  /// `Recent files`
  String get common_recentFiles {
    return Intl.message(
      'Recent files',
      name: 'common_recentFiles',
      desc: '',
      args: [],
    );
  }

  /// `Grades`
  String get common_grades {
    return Intl.message(
      'Grades',
      name: 'common_grades',
      desc: '',
      args: [],
    );
  }

  /// `Grade`
  String get common_grade {
    return Intl.message(
      'Grade',
      name: 'common_grade',
      desc: '',
      args: [],
    );
  }

  /// `Grading system`
  String get common_gradingSystem {
    return Intl.message(
      'Grading system',
      name: 'common_gradingSystem',
      desc: '',
      args: [],
    );
  }

  /// `View more`
  String get common_viewMore {
    return Intl.message(
      'View more',
      name: 'common_viewMore',
      desc: '',
      args: [],
    );
  }

  /// `View all`
  String get common_viewAll {
    return Intl.message(
      'View all',
      name: 'common_viewAll',
      desc: '',
      args: [],
    );
  }

  /// `Add grade`
  String get common_addGrade {
    return Intl.message(
      'Add grade',
      name: 'common_addGrade',
      desc: '',
      args: [],
    );
  }

  /// `Add teacher`
  String get common_addTeacher {
    return Intl.message(
      'Add teacher',
      name: 'common_addTeacher',
      desc: '',
      args: [],
    );
  }

  /// `Add friend`
  String get common_addFriend {
    return Intl.message(
      'Add friend',
      name: 'common_addFriend',
      desc: '',
      args: [],
    );
  }

  /// `Monthly`
  String get common_monthly {
    return Intl.message(
      'Monthly',
      name: 'common_monthly',
      desc: '',
      args: [],
    );
  }

  /// `Teachers`
  String get common_teachers {
    return Intl.message(
      'Teachers',
      name: 'common_teachers',
      desc: '',
      args: [],
    );
  }

  /// `Friends`
  String get common_friends {
    return Intl.message(
      'Friends',
      name: 'common_friends',
      desc: '',
      args: [],
    );
  }

  /// `Yearly`
  String get common_yearly {
    return Intl.message(
      'Yearly',
      name: 'common_yearly',
      desc: '',
      args: [],
    );
  }

  /// `Facebook`
  String get common_facebook {
    return Intl.message(
      'Facebook',
      name: 'common_facebook',
      desc: '',
      args: [],
    );
  }

  /// `Twitter`
  String get common_twitter {
    return Intl.message(
      'Twitter',
      name: 'common_twitter',
      desc: '',
      args: [],
    );
  }

  /// `WhatsApp`
  String get common_whatsapp {
    return Intl.message(
      'WhatsApp',
      name: 'common_whatsapp',
      desc: '',
      args: [],
    );
  }

  /// `Instagram`
  String get common_instagram {
    return Intl.message(
      'Instagram',
      name: 'common_instagram',
      desc: '',
      args: [],
    );
  }

  /// `Other apps`
  String get common_otherApps {
    return Intl.message(
      'Other apps',
      name: 'common_otherApps',
      desc: '',
      args: [],
    );
  }

  /// `Share App`
  String get common_shareApp {
    return Intl.message(
      'Share App',
      name: 'common_shareApp',
      desc: '',
      args: [],
    );
  }

  /// `Privacy Policy`
  String get common_privacyPolicy {
    return Intl.message(
      'Privacy Policy',
      name: 'common_privacyPolicy',
      desc: '',
      args: [],
    );
  }

  /// `Active`
  String get common_active {
    return Intl.message(
      'Active',
      name: 'common_active',
      desc: '',
      args: [],
    );
  }

  /// `Completed`
  String get common_completed {
    return Intl.message(
      'Completed',
      name: 'common_completed',
      desc: '',
      args: [],
    );
  }

  /// `Not completed`
  String get common_notCompleted {
    return Intl.message(
      'Not completed',
      name: 'common_notCompleted',
      desc: '',
      args: [],
    );
  }

  /// `Title`
  String get common_title {
    return Intl.message(
      'Title',
      name: 'common_title',
      desc: '',
      args: [],
    );
  }

  /// `Homework`
  String get common_homework {
    return Intl.message(
      'Homework',
      name: 'common_homework',
      desc: '',
      args: [],
    );
  }

  /// `All homework`
  String get common_allHomework {
    return Intl.message(
      'All homework',
      name: 'common_allHomework',
      desc: '',
      args: [],
    );
  }

  /// `Add homework`
  String get common_addHomework {
    return Intl.message(
      'Add homework',
      name: 'common_addHomework',
      desc: '',
      args: [],
    );
  }

  /// `Reminder`
  String get common_reminder {
    return Intl.message(
      'Reminder',
      name: 'common_reminder',
      desc: '',
      args: [],
    );
  }

  /// `Reminders`
  String get common_reminders {
    return Intl.message(
      'Reminders',
      name: 'common_reminders',
      desc: '',
      args: [],
    );
  }

  /// `All reminders`
  String get common_allReminders {
    return Intl.message(
      'All reminders',
      name: 'common_allReminders',
      desc: '',
      args: [],
    );
  }

  /// `Add reminder`
  String get common_addReminder {
    return Intl.message(
      'Add reminder',
      name: 'common_addReminder',
      desc: '',
      args: [],
    );
  }

  /// `Exam`
  String get common_exam {
    return Intl.message(
      'Exam',
      name: 'common_exam',
      desc: '',
      args: [],
    );
  }

  /// `Exams`
  String get common_exams {
    return Intl.message(
      'Exams',
      name: 'common_exams',
      desc: '',
      args: [],
    );
  }

  /// `All exams`
  String get common_allExams {
    return Intl.message(
      'All exams',
      name: 'common_allExams',
      desc: '',
      args: [],
    );
  }

  /// `Add exam`
  String get common_addExam {
    return Intl.message(
      'Add exam',
      name: 'common_addExam',
      desc: '',
      args: [],
    );
  }

  /// `Note`
  String get common_note {
    return Intl.message(
      'Note',
      name: 'common_note',
      desc: '',
      args: [],
    );
  }

  /// `Add note`
  String get common_addNote {
    return Intl.message(
      'Add note',
      name: 'common_addNote',
      desc: '',
      args: [],
    );
  }

  /// `Events`
  String get common_events {
    return Intl.message(
      'Events',
      name: 'common_events',
      desc: '',
      args: [],
    );
  }

  /// `Past`
  String get common_past {
    return Intl.message(
      'Past',
      name: 'common_past',
      desc: '',
      args: [],
    );
  }

  /// `Recent`
  String get common_recent {
    return Intl.message(
      'Recent',
      name: 'common_recent',
      desc: '',
      args: [],
    );
  }

  /// `Add title`
  String get common_addTitle {
    return Intl.message(
      'Add title',
      name: 'common_addTitle',
      desc: '',
      args: [],
    );
  }

  /// `Pick grade`
  String get common_pickGrade {
    return Intl.message(
      'Pick grade',
      name: 'common_pickGrade',
      desc: '',
      args: [],
    );
  }

  /// `Pick course`
  String get common_pickCourse {
    return Intl.message(
      'Pick course',
      name: 'common_pickCourse',
      desc: '',
      args: [],
    );
  }

  /// `Pick color`
  String get common_pickColor {
    return Intl.message(
      'Pick color',
      name: 'common_pickColor',
      desc: '',
      args: [],
    );
  }

  /// `Pick teacher`
  String get common_pickTeacher {
    return Intl.message(
      'Pick teacher',
      name: 'common_pickTeacher',
      desc: '',
      args: [],
    );
  }

  /// `Pick category`
  String get common_pickCategory {
    return Intl.message(
      'Pick category',
      name: 'common_pickCategory',
      desc: '',
      args: [],
    );
  }

  /// `Your list item`
  String get common_yourListItem {
    return Intl.message(
      'Your list item',
      name: 'common_yourListItem',
      desc: '',
      args: [],
    );
  }

  /// `List item`
  String get common_listItem {
    return Intl.message(
      'List item',
      name: 'common_listItem',
      desc: '',
      args: [],
    );
  }

  /// `Add list item`
  String get common_addListItem {
    return Intl.message(
      'Add list item',
      name: 'common_addListItem',
      desc: '',
      args: [],
    );
  }

  /// `Notification`
  String get common_notification {
    return Intl.message(
      'Notification',
      name: 'common_notification',
      desc: '',
      args: [],
    );
  }

  /// `Add notification`
  String get common_addNotification {
    return Intl.message(
      'Add notification',
      name: 'common_addNotification',
      desc: '',
      args: [],
    );
  }

  /// `Delete`
  String get common_delete {
    return Intl.message(
      'Delete',
      name: 'common_delete',
      desc: '',
      args: [],
    );
  }

  /// `Date`
  String get common_date {
    return Intl.message(
      'Date',
      name: 'common_date',
      desc: '',
      args: [],
    );
  }

  /// `Start date`
  String get common_startDate {
    return Intl.message(
      'Start date',
      name: 'common_startDate',
      desc: '',
      args: [],
    );
  }

  /// `End date`
  String get common_endDate {
    return Intl.message(
      'End date',
      name: 'common_endDate',
      desc: '',
      args: [],
    );
  }

  /// `Begin`
  String get common_begin {
    return Intl.message(
      'Begin',
      name: 'common_begin',
      desc: '',
      args: [],
    );
  }

  /// `End`
  String get common_end {
    return Intl.message(
      'End',
      name: 'common_end',
      desc: '',
      args: [],
    );
  }

  /// `View mode`
  String get common_viewMode {
    return Intl.message(
      'View mode',
      name: 'common_viewMode',
      desc: '',
      args: [],
    );
  }

  /// `Term`
  String get common_semesterTerm {
    return Intl.message(
      'Term',
      name: 'common_semesterTerm',
      desc: '',
      args: [],
    );
  }

  /// `Terms`
  String get common_semesterTerms {
    return Intl.message(
      'Terms',
      name: 'common_semesterTerms',
      desc: '',
      args: [],
    );
  }

  /// `Add a new term`
  String get common_addNewSemesterTerm {
    return Intl.message(
      'Add a new term',
      name: 'common_addNewSemesterTerm',
      desc: '',
      args: [],
    );
  }

  /// `New term`
  String get common_newSemesterTerm {
    return Intl.message(
      'New term',
      name: 'common_newSemesterTerm',
      desc: '',
      args: [],
    );
  }

  /// `Edit term`
  String get common_editSemesterTerm {
    return Intl.message(
      'Edit term',
      name: 'common_editSemesterTerm',
      desc: '',
      args: [],
    );
  }

  /// `Automatic term change`
  String get common_automaticSemesterTermChange {
    return Intl.message(
      'Automatic term change',
      name: 'common_automaticSemesterTermChange',
      desc: '',
      args: [],
    );
  }

  /// `Today`
  String get common_today {
    return Intl.message(
      'Today',
      name: 'common_today',
      desc: '',
      args: [],
    );
  }

  /// `Tomorrow`
  String get common_tomorrow {
    return Intl.message(
      'Tomorrow',
      name: 'common_tomorrow',
      desc: '',
      args: [],
    );
  }

  /// `Yesterday`
  String get common_yesterday {
    return Intl.message(
      'Yesterday',
      name: 'common_yesterday',
      desc: '',
      args: [],
    );
  }

  /// `In an hour`
  String get common_inHour {
    return Intl.message(
      'In an hour',
      name: 'common_inHour',
      desc: '',
      args: [],
    );
  }

  /// `Next week`
  String get common_nextWeek {
    return Intl.message(
      'Next week',
      name: 'common_nextWeek',
      desc: '',
      args: [],
    );
  }

  /// `Pick date`
  String get common_pickDate {
    return Intl.message(
      'Pick date',
      name: 'common_pickDate',
      desc: '',
      args: [],
    );
  }

  /// `Pick a date & time`
  String get common_pickDateAndTime {
    return Intl.message(
      'Pick a date & time',
      name: 'common_pickDateAndTime',
      desc: '',
      args: [],
    );
  }

  /// `Right now`
  String get common_rightNow {
    return Intl.message(
      'Right now',
      name: 'common_rightNow',
      desc: '',
      args: [],
    );
  }

  /// `Later this day`
  String get common_laterThisDay {
    return Intl.message(
      'Later this day',
      name: 'common_laterThisDay',
      desc: '',
      args: [],
    );
  }

  /// `Done`
  String get common_done {
    return Intl.message(
      'Done',
      name: 'common_done',
      desc: '',
      args: [],
    );
  }

  /// `Edit`
  String get common_edit {
    return Intl.message(
      'Edit',
      name: 'common_edit',
      desc: '',
      args: [],
    );
  }

  /// `Notifications`
  String get common_notifications {
    return Intl.message(
      'Notifications',
      name: 'common_notifications',
      desc: '',
      args: [],
    );
  }

  /// `Notify me`
  String get common_notifyMe {
    return Intl.message(
      'Notify me',
      name: 'common_notifyMe',
      desc: '',
      args: [],
    );
  }

  /// `Minutes`
  String get common_minutes {
    return Intl.message(
      'Minutes',
      name: 'common_minutes',
      desc: '',
      args: [],
    );
  }

  /// `Minutes before`
  String get common_minutesBefore {
    return Intl.message(
      'Minutes before',
      name: 'common_minutesBefore',
      desc: '',
      args: [],
    );
  }

  /// `Hours`
  String get common_hours {
    return Intl.message(
      'Hours',
      name: 'common_hours',
      desc: '',
      args: [],
    );
  }

  /// `Hours before`
  String get common_hoursBefore {
    return Intl.message(
      'Hours before',
      name: 'common_hoursBefore',
      desc: '',
      args: [],
    );
  }

  /// `Days`
  String get common_days {
    return Intl.message(
      'Days',
      name: 'common_days',
      desc: '',
      args: [],
    );
  }

  /// `Days before`
  String get common_daysBefore {
    return Intl.message(
      'Days before',
      name: 'common_daysBefore',
      desc: '',
      args: [],
    );
  }

  /// `Weeks`
  String get common_weeks {
    return Intl.message(
      'Weeks',
      name: 'common_weeks',
      desc: '',
      args: [],
    );
  }

  /// `Weeks before`
  String get common_weeksBefore {
    return Intl.message(
      'Weeks before',
      name: 'common_weeksBefore',
      desc: '',
      args: [],
    );
  }

  /// `Go back`
  String get common_goBack {
    return Intl.message(
      'Go back',
      name: 'common_goBack',
      desc: '',
      args: [],
    );
  }

  /// `Add lesson`
  String get common_addLesson {
    return Intl.message(
      'Add lesson',
      name: 'common_addLesson',
      desc: '',
      args: [],
    );
  }

  /// `Lesson`
  String get common_lesson {
    return Intl.message(
      'Lesson',
      name: 'common_lesson',
      desc: '',
      args: [],
    );
  }

  /// `Add`
  String get common_add {
    return Intl.message(
      'Add',
      name: 'common_add',
      desc: '',
      args: [],
    );
  }

  /// `Room`
  String get common_room {
    return Intl.message(
      'Room',
      name: 'common_room',
      desc: '',
      args: [],
    );
  }

  /// `Teacher`
  String get common_teacher {
    return Intl.message(
      'Teacher',
      name: 'common_teacher',
      desc: '',
      args: [],
    );
  }

  /// `Manage`
  String get common_manage {
    return Intl.message(
      'Manage',
      name: 'common_manage',
      desc: '',
      args: [],
    );
  }

  /// `Dismiss`
  String get common_dismiss {
    return Intl.message(
      'Dismiss',
      name: 'common_dismiss',
      desc: '',
      args: [],
    );
  }

  /// `Credit hours`
  String get common_creditHours {
    return Intl.message(
      'Credit hours',
      name: 'common_creditHours',
      desc: '',
      args: [],
    );
  }

  /// `Logged in to account: %s`
  String get common_signedInToAccount {
    return Intl.message(
      'Logged in to account: %s',
      name: 'common_signedInToAccount',
      desc: '',
      args: [],
    );
  }

  /// `Logged out from account: %s`
  String get common_signedOutFromAccount {
    return Intl.message(
      'Logged out from account: %s',
      name: 'common_signedOutFromAccount',
      desc: '',
      args: [],
    );
  }

  /// `by Maroon Bells Apps`
  String get common_byOurCompanyName {
    return Intl.message(
      'by Maroon Bells Apps',
      name: 'common_byOurCompanyName',
      desc: '',
      args: [],
    );
  }

  /// `%s, %s`
  String get formatter_relativeDate {
    return Intl.message(
      '%s, %s',
      name: 'formatter_relativeDate',
      desc: '',
      args: [],
    );
  }

  /// `Hey, found out a new app for my timetable. It's really great, check it out!`
  String get shareAppOptions_quote {
    return Intl.message(
      'Hey, found out a new app for my timetable. It\'s really great, check it out!',
      name: 'shareAppOptions_quote',
      desc: '',
      args: [],
    );
  }

  /// `Notes`
  String get notificationChannel_note_title {
    return Intl.message(
      'Notes',
      name: 'notificationChannel_note_title',
      desc: '',
      args: [],
    );
  }

  /// `Exams`
  String get notificationChannel_exam_title {
    return Intl.message(
      'Exams',
      name: 'notificationChannel_exam_title',
      desc: '',
      args: [],
    );
  }

  /// `Homework`
  String get notificationChannel_homework_title {
    return Intl.message(
      'Homework',
      name: 'notificationChannel_homework_title',
      desc: '',
      args: [],
    );
  }

  /// `Reminders`
  String get notificationChannel_reminder_title {
    return Intl.message(
      'Reminders',
      name: 'notificationChannel_reminder_title',
      desc: '',
      args: [],
    );
  }

  /// `Agenda`
  String get notificationChannel_agenda_title {
    return Intl.message(
      'Agenda',
      name: 'notificationChannel_agenda_title',
      desc: '',
      args: [],
    );
  }

  /// `Upcoming lessons`
  String get notificationChannel_upcomingLessons_title {
    return Intl.message(
      'Upcoming lessons',
      name: 'notificationChannel_upcomingLessons_title',
      desc: '',
      args: [],
    );
  }

  /// `%s events today. Tap to view more.`
  String get notification_agenda_body {
    return Intl.message(
      '%s events today. Tap to view more.',
      name: 'notification_agenda_body',
      desc: '',
      args: [],
    );
  }

  /// `No events today.`
  String get notification_agenda_bodyEmpty {
    return Intl.message(
      'No events today.',
      name: 'notification_agenda_bodyEmpty',
      desc: '',
      args: [],
    );
  }

  /// `%s is about to start %s`
  String get notification_upcomingLessons_body {
    return Intl.message(
      '%s is about to start %s',
      name: 'notification_upcomingLessons_body',
      desc: '',
      args: [],
    );
  }

  /// `You have a pending purchase`
  String get smartNote_pendingBilling_title {
    return Intl.message(
      'You have a pending purchase',
      name: 'smartNote_pendingBilling_title',
      desc: '',
      args: [],
    );
  }

  /// `We detected that you started a purchase which is still pending. Click the button to manage your purchases.`
  String get smartNote_pendingBilling_body {
    return Intl.message(
      'We detected that you started a purchase which is still pending. Click the button to manage your purchases.',
      name: 'smartNote_pendingBilling_body',
      desc: '',
      args: [],
    );
  }

  /// `Login and backup your data`
  String get smartNote_loginOffer_title {
    return Intl.message(
      'Login and backup your data',
      name: 'smartNote_loginOffer_title',
      desc: '',
      args: [],
    );
  }

  /// `Backup & sync your data across multiple devices.`
  String get smartNote_loginOffer_body {
    return Intl.message(
      'Backup & sync your data across multiple devices.',
      name: 'smartNote_loginOffer_body',
      desc: '',
      args: [],
    );
  }

  /// `You have work to do ⚠`
  String get smartNote_missedWork_title {
    return Intl.message(
      'You have work to do ⚠',
      name: 'smartNote_missedWork_title',
      desc: '',
      args: [],
    );
  }

  /// `Some of your events are overdue. View and complete your events to stay on track.`
  String get smartNote_missedWork_body {
    return Intl.message(
      'Some of your events are overdue. View and complete your events to stay on track.',
      name: 'smartNote_missedWork_body',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'de'),
      Locale.fromSubtags(languageCode: 'es'),
      Locale.fromSubtags(languageCode: 'fr'),
      Locale.fromSubtags(languageCode: 'pl'),
      Locale.fromSubtags(languageCode: 'zh'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}