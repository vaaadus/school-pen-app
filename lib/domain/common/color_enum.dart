/// Defines possible, selectable colors for the user to customize notes, etc..
enum ColorEnum {
  red,
  orange,
  yellow,
  green,
  teal,
  blue,
  lightBlue,
  purple,
  pink,
  black,
}

extension ColorEnumExt on ColorEnum {
  String get tag {
    switch (this) {
      case ColorEnum.red:
        return 'red';
      case ColorEnum.orange:
        return 'orange';
      case ColorEnum.yellow:
        return 'yellow';
      case ColorEnum.green:
        return 'green';
      case ColorEnum.teal:
        return 'teal';
      case ColorEnum.blue:
        return 'blue';
      case ColorEnum.lightBlue:
        return 'lightBlue';
      case ColorEnum.purple:
        return 'purple';
      case ColorEnum.pink:
        return 'pink';
      case ColorEnum.black:
        return 'black';
    }
    throw ArgumentError('Color not supported: $this');
  }

  static ColorEnum forTag(String tag) {
    for (ColorEnum color in ColorEnum.values) {
      if (color.tag == tag) return color;
    }
    return null;
  }
}
