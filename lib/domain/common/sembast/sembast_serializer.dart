import 'package:school_pen/domain/common/notification_options.dart';

const String _field = 'v';

class SembastSerializer {
  static int serializeDateTimeField(DateTime dateTime) {
    return dateTime?.microsecondsSinceEpoch;
  }

  static DateTime deserializeDateTimeField(int microsecondsSinceEpoch, {bool isUtc = false}) {
    return microsecondsSinceEpoch != null
        ? DateTime.fromMicrosecondsSinceEpoch(microsecondsSinceEpoch, isUtc: isUtc)
        : null;
  }

  static Map<String, dynamic> serializeDateTime(DateTime dateTime) {
    if (dateTime == null) return null;
    return {_field: serializeDateTimeField(dateTime)};
  }

  static DateTime deserializeDateTime(Map<String, dynamic> map) {
    if (map == null) return null;
    return deserializeDateTimeField(map[_field]);
  }

  static Map<String, dynamic> serializeInt(int value) {
    if (value == null) return null;
    return {_field: value};
  }

  static int deserializeInt(Map<String, dynamic> map) {
    if (map == null) return null;
    return map[_field];
  }

  static Map<String, dynamic> serializeStringList(List<String> strings) {
    if (strings == null) return null;
    return {_field: serializeStringListField(strings)};
  }

  static List<String> deserializeStringList(Map<String, dynamic> map) {
    if (map == null) return [];
    return deserializeStringListField(map[_field]);
  }

  static List<dynamic> serializeStringListField(List<String> strings) {
    return strings;
  }

  static List<String> deserializeStringListField(List<dynamic> list) {
    final List<dynamic> result = list ?? [];
    return result.map((e) => e.toString()).toList();
  }

  static Map<String, dynamic> serializeString(String string) {
    if (string == null) return null;
    return {_field: string};
  }

  static String deserializeString(Map<String, dynamic> map) {
    if (map == null) return null;
    return map[_field]?.toString();
  }

  static Map<String, dynamic> serializeBool(bool value) {
    if (value == null) return null;
    return {_field: value};
  }

  static bool deserializeBool(Map<String, dynamic> map) {
    if (map == null) return null;
    return map[_field];
  }

  static Map<String, dynamic> serializeNotificationOptions(NotificationOptions options) {
    if (options == null) return null;

    return {
      'amount': options.amount,
      'unit': options.unit.tag,
    };
  }

  static NotificationOptions deserializeNotificationOptions(Map<String, dynamic> map) {
    if (map == null) return null;

    return NotificationOptions(
      amount: map['amount'],
      unit: NotificationOptionsUnitExt.forTag(map['unit']),
    );
  }
}
