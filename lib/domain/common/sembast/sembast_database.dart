import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:school_pen/domain/common/lazy.dart';
import 'package:school_pen/domain/common/sembast/sembast_encrypt.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast_web/sembast_web.dart';

class SembastDatabase {
  static const int _databaseVersion = 1;
  static const String _databaseName = 'sembast_data.db';
  static const String _databasePassword = 'nmy29@PX-V73ZcvqgL+a=tyx';

  static final LazyFuture<Database> instance = LazyFuture(() => _create());

  /// Creates a new instance of sembast database, encrypted with a password.
  static Future<Database> _create() async {
    return databaseFactory.openDatabase(
      await databasePath(_databaseName),
      version: _databaseVersion,
      codec: SembastCodecFactory.getCodec(password: _databasePassword),
    );
  }

  static DatabaseFactory get databaseFactory {
    if (kIsWeb) {
      return databaseFactoryWeb;
    } else {
      return databaseFactoryIo;
    }
  }

  static Future<String> databasePath(String databaseName) async {
    if (kIsWeb) {
      return databaseName;
    } else {
      final Directory directory = await getApplicationDocumentsDirectory();
      return join(directory.path, databaseName);
    }
  }
}
