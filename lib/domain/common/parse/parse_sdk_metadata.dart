import 'dart:ui';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/env_options.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/course/repository/parse/parse_course.dart';
import 'package:school_pen/domain/course/repository/parse/parse_course_bin.dart';
import 'package:school_pen/domain/exam/repository/parse/parse_exam.dart';
import 'package:school_pen/domain/exam/repository/parse/parse_exam_bin.dart';
import 'package:school_pen/domain/grade/repository/category/parse/parse_grade_category.dart';
import 'package:school_pen/domain/grade/repository/category/parse/parse_grade_category_bin.dart';
import 'package:school_pen/domain/grade/repository/grade/parse/parse_grade.dart';
import 'package:school_pen/domain/grade/repository/grade/parse/parse_grade_bin.dart';
import 'package:school_pen/domain/homework/repository/parse/parse_homework.dart';
import 'package:school_pen/domain/homework/repository/parse/parse_homework_bin.dart';
import 'package:school_pen/domain/note/repository/parse/parse_note.dart';
import 'package:school_pen/domain/note/repository/parse/parse_note_bin.dart';
import 'package:school_pen/domain/reminder/repository/parse/parse_reminder.dart';
import 'package:school_pen/domain/reminder/repository/parse/parse_reminder_bin.dart';
import 'package:school_pen/domain/schoolyear/repository/parse/parse_school_year.dart';
import 'package:school_pen/domain/schoolyear/repository/parse/parse_school_year_bin.dart';
import 'package:school_pen/domain/semester/repository/parse/parse_semester.dart';
import 'package:school_pen/domain/semester/repository/parse/parse_semester_bin.dart';
import 'package:school_pen/domain/teacher/repository/parse/parse_teacher.dart';
import 'package:school_pen/domain/teacher/repository/parse/parse_teacher_bin.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/parse/parse_custom_timetable_event.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/parse/parse_custom_timetable_event_bin.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/parse/parse_custom_timetable.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/parse/parse_custom_timetable_bin.dart';
import 'package:school_pen/domain/timetable/repository/public/parse/parse_public_school.dart';
import 'package:school_pen/domain/timetable/repository/public/parse/parse_public_timetable.dart';
import 'package:school_pen/domain/userconfig/repository/parse/parse_user_config.dart';

class ParseSdkMetadata {
  static const String headerApplicationId = 'X-Parse-Application-Id';
  static const String headerClientKey = 'X-Parse-Client-Key';
  static const String headerSessionToken = 'X-Parse-Session-Token';
  static const String _databaseName = 'parse_sembast_data.db';
  static const String _databasePassword = 'X7f*4nfv5kxLqmz82QAPT%gM';

  static Future<void> initialize() async {
    await Parse().initialize(
      appId,
      serverUrl,
      appName: appName,
      appVersion: EnvOptions.appVersion,
      appPackageName: appPackageName,
      locale: locale,
      liveQueryUrl: liveQueryUrl,
      clientKey: clientKey,
      debug: kDebugMode,
      autoSendSessionId: true,
      coreStore: await CoreStoreSembastImp.getInstance(
        await SembastDatabase.databasePath(_databaseName),
        password: _databasePassword,
        factory: SembastDatabase.databaseFactory,
      ),
      connectivityProvider: _ParseConnectivityProvider(),
      registeredSubClassMap: {
        ParseUserConfig.keyClassName: () => ParseUserConfig(),
        ParseSchoolYear.keyClassName: () => ParseSchoolYear(),
        ParseSchoolYearBin.keyClassName: () => ParseSchoolYearBin(),
        ParseSemester.keyClassName: () => ParseSemester(),
        ParseSemesterBin.keyClassName: () => ParseSemesterBin(),
        ParseNote.keyClassName: () => ParseNote(),
        ParseNoteBin.keyClassName: () => ParseNoteBin(),
        ParseTeacher.keyClassName: () => ParseTeacher(),
        ParseTeacherBin.keyClassName: () => ParseTeacherBin(),
        ParseCourse.keyClassName: () => ParseCourse(),
        ParseCourseBin.keyClassName: () => ParseCourseBin(),
        ParseGrade.keyClassName: () => ParseGrade(),
        ParseGradeBin.keyClassName: () => ParseGradeBin(),
        ParseGradeCategory.keyClassName: () => ParseGradeCategory(),
        ParseGradeCategoryBin.keyClassName: () => ParseGradeCategoryBin(),
        ParsePublicSchool.keyClassName: () => ParsePublicSchool(),
        ParsePublicTimetable.keyClassName: () => ParsePublicTimetable(),
        ParseCustomTimetable.keyClassName: () => ParseCustomTimetable(),
        ParseCustomTimetableBin.keyClassName: () => ParseCustomTimetableBin(),
        ParseCustomTimetableEvent.keyClassName: () => ParseCustomTimetableEvent(),
        ParseCustomTimetableEventBin.keyClassName: () => ParseCustomTimetableEventBin(),
        ParseExam.keyClassName: () => ParseExam(),
        ParseExamBin.keyClassName: () => ParseExamBin(),
        ParseHomework.keyClassName: () => ParseHomework(),
        ParseHomeworkBin.keyClassName: () => ParseHomeworkBin(),
        ParseReminder.keyClassName: () => ParseReminder(),
        ParseReminderBin.keyClassName: () => ParseReminderBin(),
      },
    );
  }

  static String get appName {
    return 'School Pen';
  }

  static String get appPackageName {
    return 'com.shemhazai.school.timetable';
  }

  static String get locale {
    return window.locale.toLanguageTag();
  }

  static String get appId {
    return EnvOptions.useProductionEnv ? 'bbd68b2167734e1b91d12ebe617e0034' : '56f764a988754153a62138f3553d952d';
  }

  static String get clientKey {
    return EnvOptions.useProductionEnv ? 'a4fe9fa0fa394ae1be092d6a831eb4de' : '6e134dc5d68e4095af1eb0c912b85af0';
  }

  static String get serverUrl {
    return EnvOptions.useProductionEnv ? 'https://parse.schoolpen.app/api' : 'https://parse-dev.schoolpen.app/api';
  }

  static String get graphQLUrl {
    return EnvOptions.useProductionEnv
        ? 'https://parse.schoolpen.app/graphql'
        : 'https://parse-dev.schoolpen.app/graphql';
  }

  static String get liveQueryUrl {
    return EnvOptions.useProductionEnv ? 'wss://live.parse.schoolpen.app' : 'wss://live.parse-dev.schoolpen.app';
  }

  static Map<String, String> headers({String sessionToken}) {
    return {
      headerApplicationId: appId,
      headerClientKey: clientKey,
      if (sessionToken != null) headerSessionToken: sessionToken,
    };
  }
}

class _ParseConnectivityProvider implements ParseConnectivityProvider {
  final Connectivity _connectivity = Connectivity();

  @override
  Future<ParseConnectivityResult> checkConnectivity() async {
    final ConnectivityResult result = await _connectivity.checkConnectivity();
    return _mapResult(result);
  }

  @override
  Stream<ParseConnectivityResult> get connectivityStream => _connectivity.onConnectivityChanged.map(_mapResult);

  ParseConnectivityResult _mapResult(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.wifi:
        return ParseConnectivityResult.wifi;
      case ConnectivityResult.mobile:
        return ParseConnectivityResult.mobile;
      case ConnectivityResult.none:
        return ParseConnectivityResult.none;
    }
    throw ArgumentError('Unsupported ConnectivityResult: $result');
  }
}
