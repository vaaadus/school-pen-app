import 'package:parse_server_sdk/parse_server_sdk.dart';

enum ParseErrorType {
  generic,
  noResults,
  objectNotFound,
  usernameTaken,
  invalidEmail,
  emailTaken,
  emailNotFound,
  accountAlreadyLinked,
  sessionMissing,
  mustCreateUserThroughSignUp,
  invalidSessionToken,
}

extension ParseErrorExt on ParseError {
  ParseErrorType get typed {
    switch (type) {
      case 'No Results':
        return ParseErrorType.noResults;
      case 'ObjectNotFound':
        return ParseErrorType.objectNotFound;
      case 'InvalidEmailAddress':
        return ParseErrorType.invalidEmail;
      case 'UsernameTaken':
        return ParseErrorType.usernameTaken;
      case 'EmailTaken':
        return ParseErrorType.emailTaken;
      case 'EmailNotFound':
        return ParseErrorType.emailNotFound;
      case 'AccountAlreadyLinked':
        return ParseErrorType.accountAlreadyLinked;
      case 'SessionMissing':
        return ParseErrorType.sessionMissing;
      case 'MustCreateUserThroughSignUp':
        return ParseErrorType.mustCreateUserThroughSignUp;
      case 'InvalidSessionToken':
        return ParseErrorType.invalidSessionToken;
      default:
        return ParseErrorType.generic;
    }
  }
}
