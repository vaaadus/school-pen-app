import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/math.dart';
import 'package:school_pen/domain/common/parse/parse_error_type.dart';

extension ParseObjectExt<T extends ParseObject> on T {
  static const String keyUuid = 'uuid';

  /// Read the double from parse object. Doubles due to
  /// parse architecture can be parsed into either a double
  /// or an int, which causes some trouble.
  /// This method handles this scenario.
  double getDouble(String key) {
    return asDouble(getNullable(key));
  }

  DateTime getDateTime(String key) {
    final dynamic value = getNullable(key);
    return getDateTimeOf(value);
  }

  /// Reads the date time from parse object.
  /// Either just straight from the property or if parse encodes
  /// this in a map, inside 'iso' field, then from that field.
  static DateTime getDateTimeOf(dynamic value) {
    if (value is DateTime) return value.toLocal();
    if (value is Map && value.containsKey('iso')) return DateTime.parse(value['iso']).toLocal();
    return null;
  }

  /// Returns ID of the pointer for the [key].
  String getPointerId(String key) {
    final dynamic value = get(key);
    if (value is ParseObject) return value.objectId;
    return null;
  }

  void setPointerId(String key, String className, String id) {
    if (id == null) {
      set(key, {'__op': 'Delete'});
    } else {
      final ParseObject object = ParseObject(className)
        ..objectId = id
        ..clearUnsavedChanges();

      set(key, object);
    }
  }

  dynamic getNullable(String key) {
    final dynamic value = get(key);
    if (value is Map && value['__op'] == 'Delete') return null;
    return value;
  }

  void setNullable(String key, dynamic value) {
    if (value == null) {
      set(key, {'__op': 'Delete'});
    } else {
      set(key, value);
    }
  }

  /// Returns the unique (generated offline) object id.
  String get uuid => getNullable(keyUuid);

  /// Sets the unique (generated offline) object id.
  set uuid(String uuid) => setNullable(keyUuid, uuid);

  /// Fetches the object by querying the class by [uuid].
  Future<T> getByUuid(String uuid) async {
    final QueryBuilder builder = QueryBuilder(this);
    builder.whereEqualTo('uuid', uuid);

    final ParseResponse response = await builder.query();
    if (response.error?.typed == ParseErrorType.noResults) return null;
    if (response.error != null) throw Exception(response.error);

    return response.result;
  }

  /// Deletes the object of this class by its [uuid].
  Future<ParseResponse> deleteByUuid([String overrideUuid]) async {
    return ParseCloudFunction('deleteObjectByUUID').execute(parameters: {
      'className': parseClassName,
      'uuid': overrideUuid ?? uuid,
    });
  }
}
