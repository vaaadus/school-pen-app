import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';

const String _keyUser = 'user';
const String _keyPushEngine = 'pushEngine';

extension ParseInstallationExt on ParseInstallation {
  ParseUser get user => getNullable(_keyUser);

  set user(ParseUser user) => setNullable(_keyUser, user);

  String get pushEngine => getNullable(_keyPushEngine);

  set pushEngine(String engine) => setNullable(_keyPushEngine, engine);
}
