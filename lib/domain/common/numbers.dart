/// Commonly used utils related to mathematical operations.
class Numbers {
  static int floorToTens(int number) {
    return number - (number % 10);
  }
}
