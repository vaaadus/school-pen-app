import 'package:school_pen/domain/common/extensions/strings_ext.dart';

/// Returns the double if the text could be parsed or null.
double tryParseDouble(String text) {
  if (Strings.isBlank(text)) return null;

  try {
    text = text.trim();
    text = text.replaceAll(RegExp(','), '.');
    return double.tryParse(text);
  } catch (e) {
    return null;
  }
}

/// Tries to extract double from object.
double asDouble(dynamic object) {
  if (object is double) return object;
  if (object is int) return object.toDouble();
  if (object is String) return tryParseDouble(object);
  return null;
}

extension DoubleExt on double {
  /// Whether two numbers are almost equal with some error [delta].
  bool isCloseTo(double other, {double delta = 0.0001}) {
    return (this - other).abs() < delta;
  }
}
