import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

enum NotificationOptionsUnit {
  minutes,
  hours,
  days,
  weeks,
}

extension NotificationOptionsUnitExt on NotificationOptionsUnit {
  static NotificationOptionsUnit forTag(String tag) {
    for (NotificationOptionsUnit unit in NotificationOptionsUnit.values) {
      if (unit.tag == tag) return unit;
    }
    throw ArgumentError('No unit for tag: $tag');
  }

  String get tag {
    switch (this) {
      case NotificationOptionsUnit.minutes:
        return 'minutes';
      case NotificationOptionsUnit.hours:
        return 'hours';
      case NotificationOptionsUnit.days:
        return 'days';
      case NotificationOptionsUnit.weeks:
        return 'weeks';
    }

    throw ArgumentError('Unsupported unit: $this');
  }
}

class NotificationOptions extends Equatable {
  final int amount;
  final NotificationOptionsUnit unit;

  const NotificationOptions({@required this.amount, @required this.unit});

  /// The date time at which this notification fires when relates to the [inRelationTo] date.
  DateTime getFireDateTime(DateTime inRelationTo) {
    return inRelationTo.subtract(_duration);
  }

  Duration get _duration {
    switch (unit) {
      case NotificationOptionsUnit.minutes:
        return Duration(minutes: amount);
      case NotificationOptionsUnit.hours:
        return Duration(hours: amount);
      case NotificationOptionsUnit.days:
        return Duration(days: amount);
      case NotificationOptionsUnit.weeks:
        return Duration(days: amount * 7);
    }

    throw ArgumentError('Unsupported unit: $unit');
  }

  @override
  List<Object> get props => [amount, unit];
}
