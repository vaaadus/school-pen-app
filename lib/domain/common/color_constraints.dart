import 'dart:ui';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class ColorConstraints extends Equatable {
  final List<Color> availableColors;

  const ColorConstraints({@required this.availableColors});

  @override
  List<Object> get props => [availableColors];
}
