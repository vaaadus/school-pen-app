import 'package:equatable/equatable.dart';

/// A result of a computation, contains either value or exception (optionally stackTrace).
/// Can be returned from an isolate.
class Result<T> extends Equatable {
  final T value;
  final Object error;
  final String _stackTraceString;

  StackTrace get stackTrace {
    return _stackTraceString == null ? null : StackTrace.fromString(_stackTraceString);
  }

  const Result._(this.value, this.error, this._stackTraceString);

  factory Result.success(T value) {
    return Result._(value, null, null);
  }

  factory Result.failure(Object error, [StackTrace stackTrace]) {
    return Result._(null, error, stackTrace?.toString());
  }

  /// If the [value] is present then return the value, else throw the exception.
  static T unwrapResult<T>(Result<T> result) {
    if (result.value != null) {
      return result.value;
    } else {
      return throw result.error;
    }
  }

  @override
  List<Object> get props => [value, error, stackTrace];
}
