/// Custom types repeated across the app.
typedef VoidFutureBuilder = Future<void> Function();
