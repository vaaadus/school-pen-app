import 'package:equatable/equatable.dart';

/// Denotes an optional value. Can be used in cases where a null is also a valid state.
class Optional<T> extends Equatable {
  final T value;

  const Optional(this.value);

  /// Returns the value of the optional if the optional is present or the default value.
  static R unwrapOrElse<R>(Optional<R> optional, [R defaultValue]) {
    if (optional != null) return optional.value;
    return defaultValue;
  }

  @override
  List<Object> get props => [value];
}
