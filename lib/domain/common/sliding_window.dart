import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/preferences/preferences.dart';

/// Timing utils.
abstract class SlidingWindow {
  /// Debounces the event for the specified duration of time: [nextWindow].
  /// During this duration the returned value will be false.
  /// Once the [nextWindow] passes next calls will be returning [true] until [restart] is called.
  /// Then the whole process repeats.
  bool isPassed({
    @required String key,
    Duration initialWindow,
    Duration nextWindow,
  });

  /// Restart the timing window. During next window,
  /// the [isPassed] will be returning false until the window passes.
  void restart({@required String key});

  /// Reset the sliding window to factory settings.
  void reset({@required String key});
}

class PersistableSlidingWindow implements SlidingWindow {
  static const String key_firstLaunch_ts = 'key_slidingWindow_firstLaunch_ts';

  final Preferences _preferences;
  final DateTimeProvider _dateTimeProvider;

  PersistableSlidingWindow(this._preferences, this._dateTimeProvider) {
    if (_preferences.getDateTime(key_firstLaunch_ts) == null) {
      _preferences.setDateTime(key_firstLaunch_ts, _dateTimeProvider.now());
    }
  }

  @override
  bool isPassed({
    @required String key,
    Duration initialWindow,
    Duration nextWindow,
  }) {
    if (initialWindow != null) {
      final DateTime firstLaunch = _preferences.getDateTime(key_firstLaunch_ts);
      if (firstLaunch == null) return false;
      if (firstLaunch.add(initialWindow).isAfter(_dateTimeProvider.now())) return false;
    }

    if (nextWindow != null) {
      final DateTime lastDateTime = _preferences.getDateTime(key);
      if (lastDateTime == null) return true;
      return lastDateTime.add(nextWindow).isBefore(_dateTimeProvider.now());
    }

    return true;
  }

  @override
  void restart({@required String key}) {
    _preferences.setDateTime(key, _dateTimeProvider.now());
  }

  @override
  void reset({@required String key}) {
    _preferences.clear(key);
  }
}

class ForcedSlidingWindow implements SlidingWindow {
  final Map<String, bool> _values = {};

  void forcePassed({@required String key, @required bool passed}) {
    _values[key] = passed;
  }

  @override
  bool isPassed({
    @required String key,
    Duration initialWindow,
    Duration nextWindow,
  }) {
    return _values[key] ?? true;
  }

  @override
  void restart({@required String key}) {
    _values[key] = false;
  }

  @override
  void reset({@required String key}) {
    _values.remove(key);
  }
}
