import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:rrule/rrule.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:time_machine/time_machine.dart';

import 'optional.dart';

/// Specifies the unit at which the event repeats.
enum EventRepeatOptionsType {
  single,
  daily,
  weekly,
  monthly,
}

/// Specifies how the event should be updated.
enum EventRepeatOptionsEditMode {
  thisEvent,
  thisAndFollowingEvents,
  allEvents,
}

extension EventRepeatOptionsTypeExt on EventRepeatOptionsType {
  String get tag {
    switch (this) {
      case EventRepeatOptionsType.single:
        return 'single';
      case EventRepeatOptionsType.daily:
        return 'daily';
      case EventRepeatOptionsType.weekly:
        return 'weekly';
      case EventRepeatOptionsType.monthly:
        return 'monthly';
    }

    throw ArgumentError('Unsupported repeat options type: $this');
  }

  static EventRepeatOptionsType forTag(String tag) {
    for (EventRepeatOptionsType type in EventRepeatOptionsType.values) {
      if (type.tag == tag) return type;
    }
    return null;
  }
}

/// Specifies when the events repeats.
class EventRepeatOptions extends Equatable {
  final EventRepeatOptionsType type;
  final DateTimeRange dateRange;
  final int repeatInterval;
  final List<Weekday> weeklyRepeatsOn;
  final DateTime endsAt;
  final int endsAfterOccurrences;

  EventRepeatOptions({
    @required this.type,
    @required this.dateRange,
    this.repeatInterval = 1,
    this.weeklyRepeatsOn = const [],
    this.endsAt,
    this.endsAfterOccurrences,
  })  : assert(dateRange.start.isUtc && dateRange.end.isUtc, 'start & end should be local dates in utc'),
        assert(endsAt == null || endsAt.isUtc, 'endsAt should be in utc'),
        assert(endsAt == null || endsAt.isWithoutTime(), 'endsAt must not have time'),
        assert(
          (endsAt == null && endsAfterOccurrences == null) ||
              (endsAt == null && endsAfterOccurrences != null) ||
              (endsAt != null && endsAfterOccurrences == null),
          'Either endsAt or endsAfterOccurrences is supported, but not both',
        ),
        assert(
          type == EventRepeatOptionsType.weekly || weeklyRepeatsOn.isEmpty,
          'weeklyRepeatsOn is supported only with weekly type',
        );

  /// Constructs an [EventRepeatOptions] that never repeat and happen only once.
  factory EventRepeatOptions.single(DateTimeRange range) {
    return EventRepeatOptions(
      type: EventRepeatOptionsType.single,
      dateRange: range,
      repeatInterval: 0,
    );
  }

  /// Returns a list of [DateTimeRange] when a given event repeats at the [range].
  List<DateTimeRange> getRepeatRanges(DateTimeRange range) {
    if (type == EventRepeatOptionsType.single) {
      return _generateRanges([LocalDateTime.dateTime(dateRange.start)], range);
    } else {
      final RecurrenceRule rule = RecurrenceRule(
        frequency: _frequency,
        until: LocalDateTime.dateTime(_resolveEndsAt(range).atEndOfDay()),
        interval: repeatInterval,
        byWeekDays: weeklyRepeatsOn.map((e) => ByWeekDayEntry(_mapToDayOfWeek(e))).toSet(),
        weekStart: _mapToDayOfWeek(Weekday.firstOfWeek()),
      );

      final Iterable<LocalDateTime> instances = rule.getInstances(start: LocalDateTime.dateTime(_resolveStart(range)));
      return _generateRanges(instances, range);
    }
  }

  List<DateTimeRange> _generateRanges(Iterable<LocalDateTime> dateTimes, DateTimeRange range) {
    Iterable<DateTimeRange> ranges = _mapIntoRanges(dateTimes);
    ranges = _clampIntoRange(ranges, range);
    return ranges.toList();
  }

  Iterable<DateTimeRange> _mapIntoRanges(Iterable<LocalDateTime> dateTimes) {
    final Period period = _dateRangePeriod;
    return dateTimes.map((LocalDateTime dateTime) => DateTimeRange(
          start: dateTime.toDateTimeLocal().asUtc(),
          end: dateTime.add(period).toDateTimeLocal().asUtc(),
        ));
  }

  Iterable<DateTimeRange> _clampIntoRange(Iterable<DateTimeRange> ranges, DateTimeRange targetRange) {
    return ranges.map((e) => e.clamp(targetRange)).withoutNulls();
  }

  Period get _dateRangePeriod {
    return LocalDateTime.dateTime(dateRange.end).periodSince(LocalDateTime.dateTime(dateRange.start));
  }

  static Iterable<DateTimeRange> splitManyIntoSingleDays(Iterable<DateTimeRange> ranges) {
    final List<DateTimeRange> result = [];
    for (DateTimeRange range in ranges) {
      result.addAll(splitIntoSingleDays(range));
    }
    return result;
  }

  static Iterable<DateTimeRange> splitIntoSingleDays(DateTimeRange range) {
    final List<DateTimeRange> result = [];
    DateTime start = range.start.withoutTime();
    while (start.isBefore(range.end)) {
      final DateTimeRange r = range.clamp(DateTimeRange(
        start: start.withoutTime(),
        end: start.atEndOfDay(),
      ));

      if (r != null) result.add(r);

      start = start.plusDays(1);
    }
    return result;
  }

  Frequency get _frequency {
    switch (type) {
      case EventRepeatOptionsType.daily:
        return Frequency.daily;
      case EventRepeatOptionsType.weekly:
        return Frequency.weekly;
      case EventRepeatOptionsType.monthly:
        return Frequency.monthly;
      case EventRepeatOptionsType.single:
        throw ArgumentError('Single does not repeat');
    }
    throw ArgumentError('Unsupported type: $type');
  }

  DayOfWeek _mapToDayOfWeek(Weekday weekday) {
    if (weekday == Weekday.monday) return DayOfWeek.monday;
    if (weekday == Weekday.tuesday) return DayOfWeek.tuesday;
    if (weekday == Weekday.wednesday) return DayOfWeek.wednesday;
    if (weekday == Weekday.thursday) return DayOfWeek.thursday;
    if (weekday == Weekday.friday) return DayOfWeek.friday;
    if (weekday == Weekday.saturday) return DayOfWeek.saturday;
    if (weekday == Weekday.sunday) return DayOfWeek.sunday;
    throw ArgumentError('Unsupported weekday: $weekday');
  }

  DateTime _resolveStart(DateTimeRange targetRange) {
    if (dateRange.start.isBefore(targetRange.start)) {
      final int pastOccurrences = _countOccurrencesUpTo(targetRange.start);
      DateTime start = _calculateStartDateAfterOccurrences(pastOccurrences);
      int occurrences = 1;

      while (start.add(dateRange.duration).isAfter(targetRange.start)) {
        start = _subtractOccurrencesFromStartDate(start, occurrences);
        occurrences++;
      }
      return start;
    } else {
      return dateRange.start;
    }
  }

  DateTime _resolveEndsAt(DateTimeRange targetRange) {
    if (endsAfterOccurrences != null) {
      final DateTime dateTime = _addOccurrencesToStartDate(dateRange.start, endsAfterOccurrences - 1).withoutTime();
      if (dateTime.isBefore(targetRange.end)) return dateTime;
    }

    if (endsAt != null && endsAt.isBefore(targetRange.end)) {
      return endsAt;
    }

    return targetRange.end;
  }

  bool _hasMoreOccurrencesAfter(DateTimeRange occurrenceRange) {
    final DateTime end = _resolveEndsAt(DateTimeRange(start: DateTime.utc(1970), end: DateTime.utc(2199)));
    final DateTime nextOccurrence = _addOccurrencesToStartDate(occurrenceRange.start, 1);
    return nextOccurrence.isBeforeOrAt(end.atEndOfDay());
  }

  bool _hasMoreOccurrencesBefore(DateTimeRange occurrenceRange) {
    final DateTime previousOccurrence = _subtractOccurrencesFromStartDate(occurrenceRange.start, 1);
    return previousOccurrence.isAfterOrAt(dateRange.start);
  }

  int _countOccurrencesUpTo(DateTime dateExclusive) {
    switch (type) {
      case EventRepeatOptionsType.daily:
        return _countDailyOccurrencesUpTo(dateExclusive);
      case EventRepeatOptionsType.weekly:
        return _countWeeklyOccurrencesUpTo(dateExclusive);
      case EventRepeatOptionsType.monthly:
        return _countMonthlyOccurrencesUpTo(dateExclusive);
      case EventRepeatOptionsType.single:
        throw ArgumentError('$type has no limit of occurrences');
    }

    throw ArgumentError('Unsupported type: $type');
  }

  int _countDailyOccurrencesUpTo(DateTime date) {
    assert(date.isWithoutTime());
    return DateTimeRanges.durationInDaysOf(start: dateRange.start, end: date.atEndOfDay()) ~/ repeatInterval;
  }

  int _countWeeklyOccurrencesUpTo(DateTime date) {
    assert(date.isWithoutTime());

    final int duration = DateTimeRanges.durationInWeeksOf(start: dateRange.start, end: date);
    int occurrences = (duration ~/ repeatInterval) * weeklyRepeatsOn.length;

    DateTime dt = dateRange.start.plusWeeks(duration);
    while (dt.isBefore(date)) {
      if (weeklyRepeatsOn.contains(Weekday.fromDateTime(dt))) {
        occurrences++;
      }
      dt = dt.plusDays(1);
    }

    return occurrences;
  }

  int _countMonthlyOccurrencesUpTo(DateTime date) {
    assert(date.isWithoutTime());
    return 1 + DateTimeRanges.durationInMonthsOf(start: dateRange.start, end: date) ~/ repeatInterval;
  }

  DateTimeRange _nextOccurrenceAfter(DateTimeRange recurrenceRange) {
    final DateTime newStart = _addOccurrencesToStartDate(recurrenceRange.start, 1);
    final DateTime newEnd = newStart.add(dateRange.duration);
    return DateTimeRange(start: newStart, end: newEnd);
  }

  DateTime _addOccurrencesToStartDate(DateTime startDate, int occurrences) {
    switch (type) {
      case EventRepeatOptionsType.daily:
        return _jumpDailyToNextDateByOccurrences(startDate, occurrences);
      case EventRepeatOptionsType.weekly:
        return _jumpWeeklyToNextDateByOccurrences(startDate, occurrences);
      case EventRepeatOptionsType.monthly:
        return _jumpMonthlyToNextDateByOccurrences(startDate, occurrences);
      case EventRepeatOptionsType.single:
        throw ArgumentError('Single does not repeat');
    }
    throw ArgumentError('Unsupported type: $type');
  }

  DateTime _subtractOccurrencesFromStartDate(DateTime startDate, int occurrences) {
    return _addOccurrencesToStartDate(startDate, -occurrences);
  }

  DateTime _calculateStartDateAfterOccurrences(int occurrences) {
    switch (type) {
      case EventRepeatOptionsType.daily:
        return _jumpDailyToNextDateByOccurrences(dateRange.start, occurrences);
      case EventRepeatOptionsType.weekly:
        return _jumpWeeklyToNextDateByOccurrences(dateRange.start, occurrences);
      case EventRepeatOptionsType.monthly:
        return _jumpMonthlyToNextDateByOccurrences(dateRange.start, occurrences);
      case EventRepeatOptionsType.single:
        throw ArgumentError('Single does not repeat');
    }
    throw ArgumentError('Unsupported type: $type');
  }

  DateTime _jumpDailyToNextDateByOccurrences(DateTime previous, int occurrences) {
    return previous.plusDays(repeatInterval * occurrences);
  }

  DateTime _jumpWeeklyToNextDateByOccurrences(DateTime previous, int occurrences) {
    final int fullSpans = occurrences ~/ weeklyRepeatsOn.length;
    DateTime dateTime = previous.plusWeeks(repeatInterval * fullSpans);
    int missingOccurrences = occurrences - fullSpans * weeklyRepeatsOn.length;

    while (missingOccurrences > 0) {
      dateTime = dateTime.plusDays(1);
      if (_matchesWeeklyRepeatInterval(dateTime) && _matchesWeeklyRepeatsOn(dateTime)) {
        missingOccurrences--;
      }
    }

    while (missingOccurrences < 0) {
      dateTime = dateTime.minusDays(1);
      if (_matchesWeeklyRepeatInterval(dateTime) && _matchesWeeklyRepeatsOn(dateTime)) {
        missingOccurrences++;
      }
    }

    return dateTime;
  }

  bool _matchesWeeklyRepeatInterval(DateTime start) {
    final DateTime zeroDate = DateTime.fromMillisecondsSinceEpoch(0, isUtc: true).adjustToStartOfWeek();
    final int startWeekNumber = dateRange.start.withoutTime().difference(zeroDate).inDays ~/ 7;
    final int weekNumber = start.difference(zeroDate).inDays ~/ 7;
    return (weekNumber - startWeekNumber) % repeatInterval == 0;
  }

  bool _matchesWeeklyRepeatsOn(DateTime start) {
    return weeklyRepeatsOn.contains(Weekday.fromDateTime(start));
  }

  DateTime _jumpMonthlyToNextDateByOccurrences(DateTime previous, int occurrences) {
    return previous.plusMonths(repeatInterval * occurrences);
  }

  /// Whether event has no occurrences.
  bool get isEmpty {
    if (endsAt != null) return dateRange.start.isAfter(endsAt.atEndOfDay());
    if (endsAfterOccurrences != null) return endsAfterOccurrences < 1;
    return false;
  }

  /// Whether event has at least one occurrence.
  bool get isNotEmpty => !isEmpty;

  EventRepeatOptions copyWith({
    Optional<EventRepeatOptionsType> type,
    Optional<DateTimeRange> dateRange,
    Optional<int> repeatInterval,
    Optional<List<Weekday>> weeklyRepeatsOn,
    Optional<DateTime> endsAt,
    Optional<int> endsAfterOccurrences,
  }) {
    return EventRepeatOptions(
      type: Optional.unwrapOrElse(type, this.type),
      dateRange: Optional.unwrapOrElse(dateRange, this.dateRange),
      repeatInterval: Optional.unwrapOrElse(repeatInterval, this.repeatInterval),
      weeklyRepeatsOn: Optional.unwrapOrElse(weeklyRepeatsOn, this.weeklyRepeatsOn),
      endsAt: Optional.unwrapOrElse(endsAt, this.endsAt),
      endsAfterOccurrences: Optional.unwrapOrElse(endsAfterOccurrences, this.endsAfterOccurrences),
    );
  }

  bool _equalsWithoutDates(EventRepeatOptions other) =>
      type == other.type &&
      repeatInterval == other.repeatInterval &&
      listEquals(weeklyRepeatsOn, other.weeklyRepeatsOn) &&
      endsAt == other.endsAt &&
      endsAfterOccurrences == other.endsAfterOccurrences;

  @override
  List<Object> get props => [type, dateRange, repeatInterval, weeklyRepeatsOn, endsAt, endsAfterOccurrences];
}

extension EventRepeatOptionsUpdate on EventRepeatOptions {
  /// Lists all allowed edit modes when updating an event with [EventRepeatOptions].
  List<EventRepeatOptionsEditMode> getAllowedUpdateModes(
      EventRepeatOptions pastOptions, DateTimeRange recurrenceRange) {
    if (pastOptions == null ||
        pastOptions.type == EventRepeatOptionsType.single ||
        type == EventRepeatOptionsType.single) {
      return [EventRepeatOptionsEditMode.allEvents];
    } else {
      final bool hasMoreOccurrencesAfter = pastOptions._hasMoreOccurrencesAfter(recurrenceRange);
      final bool hasMoreOccurrencesBefore = pastOptions._hasMoreOccurrencesBefore(recurrenceRange);

      return [
        if (hasMoreOccurrencesAfter && hasMoreOccurrencesBefore) EventRepeatOptionsEditMode.thisAndFollowingEvents,
        if (!pastOptions._equalsWithoutDates(this) || hasMoreOccurrencesBefore || hasMoreOccurrencesAfter)
          EventRepeatOptionsEditMode.allEvents,
        if (pastOptions._equalsWithoutDates(this) && (hasMoreOccurrencesBefore || hasMoreOccurrencesAfter))
          EventRepeatOptionsEditMode.thisEvent,
      ];
    }
  }

  /// Lists all allowed edit modes when deleting an event with [EventRepeatOptions].
  List<EventRepeatOptionsEditMode> getAllowedDeleteModes(DateTimeRange recurrenceRange) {
    if (type == EventRepeatOptionsType.single) {
      return [EventRepeatOptionsEditMode.allEvents];
    } else {
      final bool hasMoreOccurrencesAfter = _hasMoreOccurrencesAfter(recurrenceRange);
      final bool hasMoreOccurrencesBefore = _hasMoreOccurrencesBefore(recurrenceRange);

      return [
        if (hasMoreOccurrencesAfter && hasMoreOccurrencesBefore) EventRepeatOptionsEditMode.thisAndFollowingEvents,
        if (hasMoreOccurrencesBefore || hasMoreOccurrencesAfter) EventRepeatOptionsEditMode.allEvents,
        EventRepeatOptionsEditMode.thisEvent,
      ];
    }
  }

  /// Calculates how an event with [EventRepeatOptions] should be updated.
  static EventRepeatOptionsUpdateModel<T> getUpdateModel<T extends EventRepeatOptionsAware<T>>({
    @required T previousEvent,
    @required T nextEvent,
    @required DateTimeRange recurrenceRange,
    @required EventRepeatOptionsEditMode editMode,
  }) {
    if (previousEvent == null) return EventRepeatOptionsUpdateModel();

    if (editMode == EventRepeatOptionsEditMode.allEvents) {
      return _calculateAllEventsUpdateModel(previousEvent, nextEvent, recurrenceRange);
    } else if (editMode == EventRepeatOptionsEditMode.thisAndFollowingEvents) {
      return _calculateThisAndFollowingEventsUpdateModel(previousEvent, nextEvent, recurrenceRange);
    } else if (editMode == EventRepeatOptionsEditMode.thisEvent) {
      return _calculateThisEventUpdateModel(previousEvent, nextEvent, recurrenceRange);
    }

    throw ArgumentError('Unsupported edit mode: $editMode');
  }

  static EventRepeatOptionsUpdateModel<T> _calculateAllEventsUpdateModel<T extends EventRepeatOptionsAware<T>>(
    T previousEvent,
    T nextEvent,
    DateTimeRange recurrenceRange,
  ) {
    final DateTimeRange previousRange = previousEvent.repeatOptions.dateRange;
    final DateTimeRange nextRange = nextEvent.repeatOptions.dateRange;

    final DateTimeRange range = DateTimeRange(
      start: previousRange.start.add(nextRange.start.difference(recurrenceRange.start)),
      end: previousRange.end.add(nextRange.end.difference(recurrenceRange.end)),
    );

    final T event = nextEvent.copyWith(
      repeatOptions: Optional(
        nextEvent.repeatOptions.copyWith(dateRange: Optional(range)),
      ),
      updatedAt: Optional(DateTime.now()),
    );

    if (event.repeatOptions.isEmpty) {
      return EventRepeatOptionsUpdateModel(deletePrevious: true);
    } else {
      return EventRepeatOptionsUpdateModel(updatePreviousWith: event);
    }
  }

  static EventRepeatOptionsUpdateModel<T>
      _calculateThisAndFollowingEventsUpdateModel<T extends EventRepeatOptionsAware<T>>(
    T previousEvent,
    T nextEvent,
    DateTimeRange recurrenceRange,
  ) {
    final DateTime endsAt =
        previousEvent.repeatOptions._subtractOccurrencesFromStartDate(recurrenceRange.start, 1).withoutTime();

    final T earlierEvent = previousEvent.copyWith(
      repeatOptions: Optional(previousEvent.repeatOptions.copyWith(
        endsAt: Optional(endsAt),
        endsAfterOccurrences: Optional(null),
      )),
      updatedAt: Optional(DateTime.now()),
    );

    final int endsAfterOccurrences = previousEvent.repeatOptions.endsAfterOccurrences != null
        ? previousEvent.repeatOptions.endsAfterOccurrences -
            previousEvent.repeatOptions._countOccurrencesUpTo(recurrenceRange.start.withoutTime())
        : null;

    final T laterEvent = nextEvent.copyWith(
      repeatOptions: Optional(nextEvent.repeatOptions.copyWith(
        dateRange: Optional(nextEvent.repeatOptions.dateRange == previousEvent.repeatOptions.dateRange
            ? recurrenceRange
            : nextEvent.repeatOptions.dateRange),
        endsAfterOccurrences: Optional(endsAfterOccurrences),
      )),
      updatedAt: Optional(DateTime.now()),
    );

    return EventRepeatOptionsUpdateModel(
      deletePrevious: earlierEvent.repeatOptions.isEmpty,
      updatePreviousWith: earlierEvent.repeatOptions.isEmpty ? null : earlierEvent,
      inserts: [
        if (laterEvent.repeatOptions.isNotEmpty) laterEvent,
      ],
    );
  }

  static EventRepeatOptionsUpdateModel<T> _calculateThisEventUpdateModel<T extends EventRepeatOptionsAware<T>>(
    T previousEvent,
    T nextEvent,
    DateTimeRange recurrenceRange,
  ) {
    final DateTime endsAt =
        previousEvent.repeatOptions._subtractOccurrencesFromStartDate(recurrenceRange.start, 1).withoutTime();

    final T earlierEvent = previousEvent.copyWith(
      repeatOptions: Optional(previousEvent.repeatOptions.copyWith(
        endsAt: Optional(endsAt),
        endsAfterOccurrences: Optional(null),
      )),
      updatedAt: Optional(DateTime.now()),
    );

    final DateTimeRange dateRange = nextEvent.repeatOptions.dateRange == previousEvent.repeatOptions.dateRange
        ? recurrenceRange
        : nextEvent.repeatOptions.dateRange;

    final T thisEvent = nextEvent.copyWith(
      repeatOptions: Optional(EventRepeatOptions.single(dateRange)),
      updatedAt: Optional(DateTime.now()),
    );

    final int endsAfterOccurrences = previousEvent.repeatOptions.endsAfterOccurrences != null
        ? previousEvent.repeatOptions.endsAfterOccurrences -
            previousEvent.repeatOptions._countOccurrencesUpTo(recurrenceRange.start.withoutTime()) -
            1
        : null;

    final T laterEvent = previousEvent.copyWith(
      repeatOptions: Optional(nextEvent.repeatOptions.copyWith(
        dateRange: Optional(previousEvent.repeatOptions._nextOccurrenceAfter(recurrenceRange)),
        endsAfterOccurrences: Optional(endsAfterOccurrences),
      )),
      updatedAt: Optional(DateTime.now()),
    );

    return EventRepeatOptionsUpdateModel(
      deletePrevious: earlierEvent.repeatOptions.isEmpty,
      updatePreviousWith: earlierEvent.repeatOptions.isEmpty ? null : earlierEvent,
      inserts: [
        if (thisEvent.repeatOptions.isNotEmpty) thisEvent,
        if (laterEvent.repeatOptions.isNotEmpty) laterEvent,
      ],
    );
  }

  /// Calculates how an event with [EventRepeatOptions] should be deleted.
  static EventRepeatOptionsUpdateModel<T> getDeleteModel<T extends EventRepeatOptionsAware<T>>({
    @required T event,
    @required DateTimeRange recurrenceRange,
    @required EventRepeatOptionsEditMode editMode,
  }) {
    if (editMode == EventRepeatOptionsEditMode.allEvents) {
      return _calculateAllEventsDeleteModel(event, recurrenceRange);
    } else if (editMode == EventRepeatOptionsEditMode.thisAndFollowingEvents) {
      return _calculateThisAndFollowingEventsDeleteModel(event, recurrenceRange);
    } else if (editMode == EventRepeatOptionsEditMode.thisEvent) {
      return _calculateThisEventDeleteModel(event, recurrenceRange);
    }

    throw ArgumentError('Unsupported edit mode: $editMode');
  }

  static EventRepeatOptionsUpdateModel<T> _calculateAllEventsDeleteModel<T extends EventRepeatOptionsAware<T>>(
      T event, DateTimeRange recurrenceRange) {
    if (event == null) {
      return EventRepeatOptionsUpdateModel();
    } else {
      return EventRepeatOptionsUpdateModel(deletePrevious: true);
    }
  }

  static EventRepeatOptionsUpdateModel<T>
      _calculateThisAndFollowingEventsDeleteModel<T extends EventRepeatOptionsAware<T>>(
          T event, DateTimeRange recurrenceRange) {
    if (event == null) return EventRepeatOptionsUpdateModel();

    final DateTime endsAt =
        event.repeatOptions._subtractOccurrencesFromStartDate(recurrenceRange.start, 1).withoutTime();

    final T earlierEvent = event.copyWith(
      repeatOptions: Optional(event.repeatOptions.copyWith(
        endsAt: Optional(endsAt),
        endsAfterOccurrences: Optional(null),
      )),
      updatedAt: Optional(DateTime.now()),
    );

    return EventRepeatOptionsUpdateModel(
      deletePrevious: earlierEvent.repeatOptions.isEmpty,
      updatePreviousWith: earlierEvent.repeatOptions.isEmpty ? null : earlierEvent,
    );
  }

  static EventRepeatOptionsUpdateModel<T> _calculateThisEventDeleteModel<T extends EventRepeatOptionsAware<T>>(
      T event, DateTimeRange recurrenceRange) {
    if (event == null) return EventRepeatOptionsUpdateModel();

    final DateTime endsAt =
        event.repeatOptions._subtractOccurrencesFromStartDate(recurrenceRange.start, 1).withoutTime();

    final T earlierEvent = event.copyWith(
      repeatOptions: Optional(event.repeatOptions.copyWith(
        endsAt: Optional(endsAt),
        endsAfterOccurrences: Optional(null),
      )),
      updatedAt: Optional(DateTime.now()),
    );

    final int endsAfterOccurrences = event.repeatOptions.endsAfterOccurrences != null
        ? event.repeatOptions.endsAfterOccurrences -
            event.repeatOptions._countOccurrencesUpTo(recurrenceRange.start.withoutTime()) -
            1
        : null;

    final T laterEvent = event.copyWith(
      repeatOptions: Optional(event.repeatOptions.copyWith(
        dateRange: Optional(event.repeatOptions._nextOccurrenceAfter(recurrenceRange)),
        endsAfterOccurrences: Optional(endsAfterOccurrences),
      )),
      updatedAt: Optional(DateTime.now()),
    );

    return EventRepeatOptionsUpdateModel(
      deletePrevious: earlierEvent.repeatOptions.isEmpty,
      updatePreviousWith: earlierEvent.repeatOptions.isEmpty ? null : earlierEvent,
      inserts: [
        if (laterEvent.repeatOptions.isNotEmpty) laterEvent,
      ],
    );
  }
}

class EventRepeatOptionsUpdateModel<T extends EventRepeatOptionsAware<T>> extends Equatable {
  /// Whether the previous event should be deleted.
  final bool deletePrevious;

  /// If non-null, contains the object with which the previous event should be updated.
  final T updatePreviousWith;

  /// Lists all objects to be created.
  final List<T> inserts;

  const EventRepeatOptionsUpdateModel({
    this.deletePrevious = false,
    this.updatePreviousWith,
    this.inserts = const [],
  });

  @override
  List<Object> get props => [deletePrevious, updatePreviousWith, inserts];
}

abstract class EventRepeatOptionsAware<T> {
  /// Accessor for the repeat options. The object must provide non-null repeat options.
  EventRepeatOptions get repeatOptions;

  /// The object must create exact copy of itself, assigning new [repeatOptions].
  T copyWith({@required Optional<EventRepeatOptions> repeatOptions, @required Optional<DateTime> updatedAt});
}
