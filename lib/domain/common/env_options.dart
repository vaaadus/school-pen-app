import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

/// Contains all compile constants defined by --dart-define.
class EnvOptions {
  // environment variables must be const
  static const String _targetStore = String.fromEnvironment('TARGET_STORE');
  static const String appVersion = String.fromEnvironment('APP_VERSION', defaultValue: '1.0.0');
  static const bool useProductionEnv = bool.fromEnvironment('USE_PRODUCTION_ENV', defaultValue: false);

  // parsed
  static final TargetStore store = _parseStore(_targetStore);
  static final String googleClientId = _googleClientId;
}

/// The client ID for google-services related purposes.
String get _googleClientId {
  if (kIsWeb) {
    return '564464527939-7jffoku0kforbqalp65k7vrk1jlqepg4.apps.googleusercontent.com';
  } else if (Platform.isAndroid) {
    if (kReleaseMode) {
      return '564464527939-26lurm07beuo3dej0d6c5go8h2stq687.apps.googleusercontent.com';
    } else {
      return '564464527939-5ba073jig45t36lq3cacttu70gr4701e.apps.googleusercontent.com';
    }
  } else if (Platform.isIOS) {
    return '564464527939-o6psehpc5d5brm966b93mo3ris4mss1j.apps.googleusercontent.com';
  } else {
    // desktop
    return '564464527939-fsennm3e379k4p82ncuka68lbquojve2.apps.googleusercontent.com';
  }
}

/// The store to which this app will be deployed.
enum TargetStore {
  /// The Google play store.
  play,

  /// The Amazon app store.
  amazon,

  /// The apple app store for iOS.
  iOS,

  /// The apple app store for macOS.
  macOS,

  /// The windows app store.
  windows,

  /// The linux app store.
  linux,

  /// The website.
  web,
}

TargetStore _parseStore(String store) {
  if (Strings.isBlank(store)) return _defaultStore();

  switch (store.toLowerCase()) {
    case 'play':
      return TargetStore.play;
    case 'amazon':
      return TargetStore.amazon;
    case 'ios':
      return TargetStore.iOS;
    case 'macos':
      return TargetStore.macOS;
    case 'windows':
      return TargetStore.windows;
    case 'linux':
      return TargetStore.linux;
    case 'web':
      return TargetStore.web;
  }

  throw ArgumentError('Unsupported store: $store');
}

TargetStore _defaultStore() {
  if (kIsWeb) return TargetStore.web;
  if (Platform.isAndroid) return TargetStore.play;
  if (Platform.isIOS) return TargetStore.iOS;
  if (Platform.isMacOS) return TargetStore.macOS;
  if (Platform.isWindows) return TargetStore.windows;
  if (Platform.isLinux) return TargetStore.linux;
  throw ArgumentError('No default store for platform: ${Platform.operatingSystem}');
}
