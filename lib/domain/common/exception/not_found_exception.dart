import 'package:equatable/equatable.dart';

/// Indicates that the resource couldn't be found.
class NotFoundException extends Equatable implements Exception {
  final String message;

  NotFoundException(this.message);

  @override
  List<Object> get props => [message];
}
