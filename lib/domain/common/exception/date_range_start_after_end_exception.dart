import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

/// In [DateTimeRange], the end should be after start.
class DateRangeStartAfterEndException extends Equatable implements Exception {
  const DateRangeStartAfterEndException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
