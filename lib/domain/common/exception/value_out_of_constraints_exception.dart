import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

/// A value must one of allowed values but wasn't.
class ValueOutOfConstraintsException extends Equatable implements Exception {
  final List<Object> allowedValues;
  final String message;

  const ValueOutOfConstraintsException({@required this.allowedValues, this.message});

  @override
  List<Object> get props => [allowedValues, message];
}
