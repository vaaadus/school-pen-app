import 'package:equatable/equatable.dart';

/// Indicates a generic error without any specific meaning.
class GenericException extends Equatable implements Exception {
  final Error cause;

  const GenericException(this.cause);

  @override
  List<Object> get props => [cause];
}
