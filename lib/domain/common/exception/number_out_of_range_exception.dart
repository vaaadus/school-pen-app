import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

/// A number must be between specified range.
class NumberOutOfRangeException extends Equatable implements Exception {
  final double min;
  final double max;
  final String message;

  const NumberOutOfRangeException({@required this.min, @required this.max, this.message});

  @override
  List<Object> get props => [min, max, message];
}
