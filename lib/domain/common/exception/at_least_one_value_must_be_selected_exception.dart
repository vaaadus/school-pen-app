import 'package:equatable/equatable.dart';

class AtLeastOneValueMustBeSelectedException extends Equatable implements Exception {
  const AtLeastOneValueMustBeSelectedException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
