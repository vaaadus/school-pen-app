import 'package:equatable/equatable.dart';

/// A field must be filled.
class FieldMustNotBeEmptyException extends Equatable implements Exception {
  const FieldMustNotBeEmptyException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
