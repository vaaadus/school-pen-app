import 'package:equatable/equatable.dart';

/// Indicates that the operation failed because of disconnected network connection.
class NoInternetException extends Equatable implements Exception {
  @override
  List<Object> get props => [];
}
