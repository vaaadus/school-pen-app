/// Abstracts how date time is provided.
abstract class DateTimeProvider {
  DateTime now();
}

class DefaultDateTimeProvider implements DateTimeProvider {
  @override
  DateTime now() => DateTime.now();
}
