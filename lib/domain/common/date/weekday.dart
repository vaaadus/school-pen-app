import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';

/// Represents a day of the week like Monday or Friday.
class Weekday extends Equatable {
  static const Weekday monday = Weekday._internal('mon', DateTime.monday);
  static const Weekday tuesday = Weekday._internal('tue', DateTime.tuesday);
  static const Weekday wednesday = Weekday._internal('wed', DateTime.wednesday);
  static const Weekday thursday = Weekday._internal('thu', DateTime.thursday);
  static const Weekday friday = Weekday._internal('fri', DateTime.friday);
  static const Weekday saturday = Weekday._internal('sat', DateTime.saturday);
  static const Weekday sunday = Weekday._internal('sun', DateTime.sunday);
  static const List<Weekday> values = [monday, tuesday, wednesday, thursday, friday, saturday, sunday];

  final String tag;
  final int dateTimeWeekday;

  const Weekday._internal(this.tag, this.dateTimeWeekday);

  /// Constructs the weekday from the [DateTime].
  factory Weekday.fromDateTime(DateTime date) {
    return Weekday.fromDateTimeWeekday(date.weekday);
  }

  /// Constructs the weekday from the [DateTime] weekday.
  factory Weekday.fromDateTimeWeekday(int dateTimeWeekday) {
    return values[dateTimeWeekday - 1];
  }

  /// Constructs a weekday from the [tag].
  factory Weekday.fromTag(String tag) {
    for (Weekday weekday in values) {
      if (weekday.tag == tag) return weekday;
    }
    throw ArgumentError('Unsupported weekday $tag');
  }

  /// Constructs a weekday from the first day of week (usually Monday or Sunday).
  factory Weekday.firstOfWeek() {
    return Weekday.fromDateTimeWeekday(DateFormat().dateSymbols.FIRSTDAYOFWEEK + 1);
  }

  /// Constructs a list of weekdays from the [tag]s.
  static List<Weekday> fromTags(List<dynamic> tags) {
    final List<Weekday> result = [];
    for (dynamic tag in tags) {
      final Weekday weekday = Weekday.fromTag(tag.toString());
      if (weekday != null) result.add(weekday);
    }

    return result;
  }

  /// Converts [weekdays] to list of string [tag]s.
  static List<String> toTags(List<Weekday> weekdays) {
    return weekdays.map((e) => e.tag).toList();
  }

  /// Returns the earliest (to the start of the week) weekday from [weekdays].
  static Weekday earliest(List<Weekday> weekdays) {
    final Map<Weekday, int> weekdayByOccurrence = {};

    Weekday weekday = Weekday.firstOfWeek();
    for (int i = 0; i < values.length; i++) {
      weekdayByOccurrence[weekday] = i;
      weekday = weekday.nextDay();
    }

    Weekday earliest;
    for (Weekday weekday in weekdays) {
      if (earliest == null || weekdayByOccurrence[weekday] < weekdayByOccurrence[earliest]) {
        earliest = weekday;
      }
    }

    return earliest;
  }

  /// Returns the next day. If the day is outside current week, then the first day from next week is returned.
  Weekday nextDay() {
    if (dateTimeWeekday + 1 <= sunday.dateTimeWeekday) {
      return Weekday.fromDateTimeWeekday(dateTimeWeekday + 1);
    } else {
      return Weekday.monday;
    }
  }

  /// Returns the previous day. If the day is outside current week, then the last day from previous week is returned.
  Weekday previousDay() {
    if (dateTimeWeekday - 1 >= monday.dateTimeWeekday) {
      return Weekday.fromDateTimeWeekday(dateTimeWeekday - 1);
    } else {
      return Weekday.sunday;
    }
  }

  /// Returns whether this weekday is before [other] weekday considering the week timeline.
  /// E.g. Friday is before Saturday.
  bool isBefore(Weekday other) {
    if (this == other) return false;

    Weekday weekday = this;
    for (int i = 0; i < values.length; i++) {
      weekday = weekday.nextDay();

      if (weekday == Weekday.firstOfWeek()) return false;
      if (weekday == other) return true;
    }

    throw ArgumentError('For loop must be exhaustive');
  }

  /// Returns whether this weekday is after [other] weekday considering the week timeline.
  /// E.g. Saturday is after Friday.
  bool isAfter(Weekday other) {
    if (this == other) return false;

    Weekday weekday = this;
    for (int i = 0; i < values.length; i++) {
      weekday = weekday.previousDay();

      if (weekday == Weekday.firstOfWeek()) return false;
      if (weekday == other) return true;
    }

    throw ArgumentError('For loop must be exhaustive');
  }

  @override
  List<Object> get props => [tag, dateTimeWeekday];
}
