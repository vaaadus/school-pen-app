import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'time.dart';

/// Represents a start & end time.
class TimeRange extends Equatable implements Comparable<TimeRange> {
  final Time start;
  final Time end;

  const TimeRange._internal(this.start, this.end);

  factory TimeRange({@required Time start, @required Time end}) {
    if (start.isBeforeOrAt(end)) {
      return TimeRange._internal(start, end);
    } else {
      return TimeRange._internal(end, start);
    }
  }

  factory TimeRange.fromJson(Map<String, dynamic> json) => TimeRange(
        start: Time.parse(json['start']),
        end: Time.parse(json['end']),
      );

  Map<String, dynamic> toJson() => {
        'start': start.toString(),
        'end': end.toString(),
      };

  /// Returns an amount of time between the [end] and the [start].
  Duration get duration => end.minus(start);

  /// Returns whether this time range contains (including edges) the [time].
  bool contains(Time time) {
    return start.isBeforeOrAt(time) && end.isAfterOrAt(time);
  }

  /// Returns whether this time range has any common parts with the [other] time range.
  bool overlapsWith(TimeRange other) {
    return start.isBefore(other.end) && end.isAfter(other.start);
  }

  @override
  int compareTo(TimeRange other) {
    if (start != other.start) {
      return start.compareTo(other.start);
    }
    return end.compareTo(other.end);
  }

  @override
  List<Object> get props => [start, end];

  @override
  String toString() => '$start - $end';
}
