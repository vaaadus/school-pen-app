import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:sprintf/sprintf.dart';

/// Represents a time, for example: 08:00.
/// [TimeOfDay] can't be used because of its strong relationship with UI (part of material package).
class Time extends Equatable implements Comparable<Time> {
  static const Time min = Time(hour: 0, minute: 0);
  static const Time max = Time(hour: 23, minute: 59);
  static const String _parseRegex = '([\\d]{1,2})(:)([\\d]{1,2})';

  final int hour;
  final int minute;

  const Time({
    @required this.hour,
    @required this.minute,
  });

  factory Time.fromTimeOfDay(TimeOfDay time) {
    if (time == null) return null;
    return Time(hour: time.hour, minute: time.minute);
  }

  factory Time.now() {
    return Time.fromDateTime(DateTime.now());
  }

  factory Time.fromDateTime(DateTime date) {
    if (date == null) return null;
    return Time(hour: date.hour, minute: date.minute);
  }

  factory Time.fromMinutes(int minutes) {
    return Time(hour: minutes ~/ 60 % 24, minute: minutes % 60);
  }

  factory Time.fromString(String string) {
    final RegExpMatch match = RegExp(_parseRegex).firstMatch(string);
    if (match != null) {
      return Time(
        hour: int.parse(match.group(1)),
        minute: int.parse(match.group(3)),
      );
    }
    throw ArgumentError('$string cannot be parsed to Time');
  }

  TimeOfDay toTimeOfDay() => TimeOfDay(hour: hour, minute: minute);

  bool isBefore(Time time) => toMinutes() < time.toMinutes();

  bool isBeforeOrAt(Time time) => toMinutes() <= time.toMinutes();

  bool isAfter(Time time) => toMinutes() > time.toMinutes();

  bool isAfterOrAt(Time time) => toMinutes() >= time.toMinutes();

  int toMinutes() => Duration(hours: hour, minutes: minute).inMinutes;

  Time plus(Duration duration) => Time.fromMinutes(toMinutes() + duration.inMinutes);

  Duration minus(Time time) => Duration(minutes: toMinutes() - time.toMinutes());

  @override
  String toString() => sprintf('%02d:%02d', [hour, minute]);

  @override
  List<Object> get props => [hour, minute];

  @override
  int compareTo(Time other) {
    if (hour != other.hour) {
      return hour.compareTo(other.hour);
    }
    return minute.compareTo(other.minute);
  }

  static Time parse(String string) {
    return Time.fromString(string);
  }

  static String format(Time time) {
    return time?.toString() ?? '--:--';
  }
}
