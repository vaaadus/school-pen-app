import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class DateConstraints extends Equatable {
  final DateTime minimumDate;
  final DateTime maximumDate;

  const DateConstraints({@required this.minimumDate, @required this.maximumDate});

  @override
  List<Object> get props => [minimumDate, maximumDate];
}
