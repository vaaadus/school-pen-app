import 'dart:math';

import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';

extension DateTimes on DateTime {
  /// Creates a new utc date.
  static DateTime utcNow() => DateTime.now().asUtc();

  /// Returns true if the [DateTime] does not include time part.
  bool isWithoutTime() => hour == 0 && minute == 0 && second == 0 && millisecond == 0 && microsecond == 0;

  /// Checks for equality with [other] [DateTime] skipping the time part.
  bool isSameDateAs(DateTime other) {
    DateTime left = this;
    DateTime right = other;
    if (left.isUtc != other.isUtc) {
      left = left.toUtc();
      right = right.toUtc();
    }
    return left.year == right.year && left.month == right.month && left.day == right.day;
  }

  /// Checks whether the day is today.
  bool get isToday => isSameDateAs(DateTime.now());

  /// Checks whether the day was yesterday.
  bool get isYesterday => isSameDateAs(DateTime.now().minusDays(1));

  /// Checks whether the day is tomorrow.
  bool get isTomorrow => isSameDateAs(DateTime.now().plusDays(1));

  /// Checks if [other] is after or equal to this date.
  bool isAfterOrAt(DateTime other) => isAfter(other) || this == other;

  /// Checks if [other] is before or equal to this date.
  bool isBeforeOrAt(DateTime other) => isBefore(other) || this == other;

  /// Returns a copy of this date with [time].
  DateTime withTime(Time time) => isUtc
      ? DateTime.utc(year, month, day, time.hour, time.minute)
      : DateTime(year, month, day, time.hour, time.minute);

  /// Returns a copy of this date with [minutes].
  DateTime withMinutes(int minutes) =>
      isUtc ? DateTime.utc(year, month, day, hour, minutes) : DateTime(year, month, day, hour, minutes);

  /// Returns a copy of this date skipping the time part.
  DateTime withoutTime() => isUtc ? DateTime.utc(year, month, day) : DateTime(year, month, day);

  /// Returns a copy of this date skipping time unit smaller than [minute].
  DateTime withoutSeconds() =>
      isUtc ? DateTime.utc(year, month, day, hour, minute) : DateTime(year, month, day, hour, minute);

  /// Returns the date time at this day but with last possible clock time (23:59:59.999999).
  DateTime atEndOfDay() =>
      isUtc ? DateTime.utc(year, month, day, 23, 59, 59, 999, 999) : DateTime(year, month, day, 23, 59, 59, 999, 999);

  /// Returns the UTC date time as if it would be in local.
  DateTime asUtc() => isUtc ? this : DateTime.utc(year, month, day, hour, minute, second, millisecond, microsecond);

  /// Returns the local date time as if it would be in utc.
  DateTime asLocal() => isUtc ? DateTime(year, month, day, hour, minute, second, millisecond, microsecond) : this;

  /// Returns the time part from this [DateTime].
  Time get time => Time.fromDateTime(this);

  /// Returns the time part from this [DateTime] as [TimeOfDay].
  TimeOfDay get timeOfDay => TimeOfDay.fromDateTime(this);

  /// Adds [days] to this [DateTime] and handles DST change.
  /// Here, the meaning of days is not 24 hours but an amount
  /// of time from one day at an exact time to the next day at the same time.
  DateTime plusDays(int days) {
    if (days == 0) return this;

    final DateTime dateTime = add(Duration(days: days));
    if (hour == dateTime.hour) return dateTime;

    // keep adding/removing hours lost/gained due to DST
    int counter = 0;
    while (true) {
      counter++;

      final DateTime subtracted = dateTime.subtract(Duration(hours: counter));
      if (hour == subtracted.hour) return subtracted;

      final DateTime added = dateTime.add(Duration(hours: counter));
      if (hour == added.hour) return added;
    }
  }

  /// Subtracts [days] from this [DateTime] and handles DST change.
  /// Here, the meaning of days is not 24 hours but an amount
  /// of time from one day at an exact time to the previous day at the same time.
  DateTime minusDays(int days) => plusDays(-days);

  /// Adds a certain amount of weeks to this date. Retains the time part.
  DateTime plusWeeks(int num) => plusDays(num * 7);

  /// Subtracts a certain amount of weeks from this date. Retains the time part.
  DateTime minusWeeks(int num) => minusDays(num * 7);

  /// Adds a certain amount of months to this date. Retains the time part.
  /// Adjusts the day to the last day of the month if the resulting day would be outside the allowed range.
  DateTime plusMonths(int num) {
    int selectedYear = year + num ~/ 12;
    int nextMonth = month + num % 12;

    if (nextMonth > DateTime.december) {
      selectedYear++;
      nextMonth -= DateTime.december;
    }

    final int selectedDay = min(day, daysInMonth(selectedYear, nextMonth));
    return isUtc
        ? DateTime.utc(selectedYear, nextMonth, selectedDay, hour, minute, second, millisecond, microsecond)
        : DateTime(selectedYear, nextMonth, selectedDay, hour, minute, second, millisecond, microsecond);
  }

  /// Subtracts a certain amount of months from this date. Retains the time part.
  /// Adjusts the day to the last day of the month if the resulting day would be outside the allowed range.
  DateTime minusMonths(int num) {
    int selectedYear = year - num ~/ 12;
    int previousMonth = month - num % 12;

    if (previousMonth < DateTime.january) {
      selectedYear--;
      previousMonth += DateTime.december;
    }

    final int selectedDay = min(day, daysInMonth(selectedYear, previousMonth));
    return isUtc
        ? DateTime.utc(selectedYear, previousMonth, selectedDay, hour, minute, second, millisecond, microsecond)
        : DateTime(selectedYear, previousMonth, selectedDay, hour, minute, second, millisecond, microsecond);
  }

  /// Adds a certain amount of days to the [dateTime] so that the day of the week is [weekday].
  DateTime adjustToNextWeekday(Weekday weekday) {
    DateTime dateTime = this;
    while (Weekday.fromDateTime(dateTime) != weekday) {
      dateTime = dateTime.plusDays(1);
    }
    return dateTime;
  }

  /// Subtracts a certain amount of days from the [dateTime] so that the day of the week is [weekday].
  DateTime adjustToPreviousWeekday(Weekday weekday) {
    DateTime dateTime = this;
    while (Weekday.fromDateTime(dateTime) != weekday) {
      dateTime = dateTime.minusDays(1);
    }
    return dateTime;
  }

  /// Subtracts a certain amount of days from the [dateTime] so that the day is the first day of the week.
  DateTime adjustToStartOfWeek() {
    DateTime dateTime = this;
    while (Weekday.fromDateTime(dateTime) != Weekday.firstOfWeek()) {
      dateTime = dateTime.minusDays(1);
    }
    return dateTime;
  }

  /// Calculates the amount of days a month has.
  static int daysInMonth(int year, int month) {
    DateTime firstOfNextMonth;
    if (month == DateTime.december) {
      firstOfNextMonth = DateTime.utc(year + 1, DateTime.january, 1, 12);
    } else {
      firstOfNextMonth = DateTime.utc(year, month + 1, 1, 12);
    }
    return firstOfNextMonth.subtract(Duration(days: 1)).day;
  }
}

extension DateTimeRanges on DateTimeRange {
  static const Duration _oneDay = Duration(days: 1);

  /// Returns the duration in full days of between [start] and [end]. Handles DST change.
  static int durationInDaysOf({@required DateTime start, @required DateTime end}) {
    // calculate days span in hours instead to handle the DST, sometimes a day can be 23 or 25 hours
    final int hoursInDay = _oneDay.inHours;
    int hours = end.difference(start).inHours;
    if (hours % hoursInDay >= (_oneDay.inHours - 1)) hours++;
    return hours ~/ hoursInDay;
  }

  /// Returns the duration in full weeks between [start] and [end].
  static int durationInWeeksOf({@required DateTime start, @required DateTime end}) {
    return durationInDaysOf(start: start, end: end) ~/ 7;
  }

  /// Returns the duration in full months between [start] and [end].
  static int durationInMonthsOf({@required DateTime start, @required DateTime end}) {
    int months = (end.year * 12 + end.month) - (start.year * 12 + start.month);
    if (end.day < start.day && DateTimes.daysInMonth(end.year, end.month) != end.day) months--;
    return months;
  }

  /// Returns the duration in full days of this [DateTimeRange]. Handles DST change.
  int durationInDays() => durationInDaysOf(start: start, end: end);

  /// Returns the duration in full weeks of this [DateTimeRange].
  int durationInWeeks() => durationInWeeksOf(start: start, end: end);

  /// Returns the duration in full months of this [DateTimeRange].
  int durationInMonths() => durationInMonthsOf(start: start, end: end);

  /// Adds [duration] to [start] and [end] dates.
  DateTimeRange plusDuration(Duration duration) {
    return DateTimeRange(
      start: start.add(duration),
      end: end.add(duration),
    );
  }

  /// Adds [days] to [start] and [end] dates.
  DateTimeRange plusDays(int days) {
    return DateTimeRange(
      start: start.plusDays(days),
      end: end.plusDays(days),
    );
  }

  /// Adds [weeks] to [start] and [end] dates.
  DateTimeRange plusWeeks(int weeks) {
    return DateTimeRange(
      start: start.plusWeeks(weeks),
      end: end.plusWeeks(weeks),
    );
  }

  /// Adds [months] to [start] and [end] dates.
  DateTimeRange plusMonths(int months) {
    return DateTimeRange(
      start: start.plusMonths(months),
      end: end.plusMonths(months),
    );
  }

  /// Returns a [DateTimeRange] where [start] and [end] have been stripped of the time part.
  DateTimeRange withoutTime() {
    return DateTimeRange(
      start: start.withoutTime(),
      end: end.withoutTime(),
    );
  }

  /// Returns a [TimeRange] with [start] and [end] times.
  TimeRange get timeRange => TimeRange(start: start.time, end: end.time);

  /// Returns whether this [DateTimeRange] has any common parts or is adjacent to [other].
  bool overlapsOrAdjacentWith(DateTimeRange other) {
    return start.isBeforeOrAt(other.end) && end.isAfterOrAt(other.start);
  }

  /// Returns whether this [DateTimeRange] has any common parts with [other].
  bool overlapsWith(DateTimeRange other) {
    return start.isBefore(other.end) && end.isAfter(other.start);
  }

  /// Whether the [dateTime] fits into this range.
  bool contains(DateTime dateTime, {bool endInclusive = true}) {
    return start.isBeforeOrAt(dateTime) && (endInclusive ? end.isAfterOrAt(dateTime) : end.isAfter(dateTime));
  }

  /// Whether this [DateTimeRange] fully contains [other].
  bool containsRange(DateTimeRange other) {
    return start.isBeforeOrAt(other.start) && end.isAfterOrAt(other.end);
  }

  /// Returns this [DateTimeRange] clamped to be in the range of [into].
  /// Returns null if this [DateTimeRange] and [into] do not overlap.
  DateTimeRange clamp(DateTimeRange into) {
    if (!overlapsWith(into)) return null;

    return DateTimeRange(
      start: start.isBefore(into.start) ? into.start : start,
      end: end.isAfter(into.end) ? into.end : end,
    );
  }
}
