import 'package:synchronized/synchronized.dart';

typedef FutureProvider<T> = Future<T> Function();

/// A lazy value provider. The [provider] callback won't be executed until the value is requested.
class LazyFuture<T> {
  final Lock _lock = Lock();
  final FutureProvider<T> provider;
  T _value;

  LazyFuture(this.provider);

  Future<T> get() async {
    if (_value != null) return _value;

    return _lock.synchronized(() async {
      _value = await provider();
      return _value;
    });
  }
}
