class Files {
  static String removeExtension(String string) {
    if (string == null) return null;

    final int index = string.lastIndexOf(RegExp('\\.'));
    if (index < 0) return string;
    return string.substring(0, index);
  }

  static double megabytesToBytes(double mb) {
    return 1024 * 1024 * mb;
  }
}
