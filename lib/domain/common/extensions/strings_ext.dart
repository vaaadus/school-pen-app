/// Common extensions for String.
class Strings {
  static bool isBlank(String string) => string.isBlank;

  static bool isNotBlank(String string) => string.isNotBlank;

  static String firstLetter(String text) {
    return substringUpTo(text, 1);
  }

  static String retainDigits(String text) {
    if (text == null) return null;
    return text.replaceAll(RegExp('[^\\d.]'), '');
  }

  static String substringUpTo(String text, int characters) {
    if (text == null) return null;

    final Runes runes = text.runes;
    if (runes.length < characters) characters = runes.length;

    return String.fromCharCodes(text.runes.toList().sublist(0, characters));
  }
}

extension StringExt on String {
  bool get isBlank => this == null || trim().isEmpty;

  bool get isNotBlank => !isBlank;
}
