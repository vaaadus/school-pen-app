/// Extension functions for any list.
extension Lists<T> on List<T> {
  T getOrNull(int index) {
    if (index >= 0 && index < length) {
      return this[index];
    }
    return null;
  }

  T get firstOrNull => getOrNull(0);

  T firstWhereOrNull(bool Function(T e) filter) {
    for (var element in this) {
      if (filter(element)) return element;
    }
    return null;
  }

  bool addIfAbsentAll(List<T> values) {
    bool result = false;
    for (T value in values) {
      result |= addIfAbsent(value);
    }
    return result;
  }

  bool addIfAbsent(T value) {
    if (!contains(value)) {
      add(value);
      return true;
    }
    return false;
  }

  List<T> sorted(int Function(T, T) compare) {
    return List.of(this)..sort(compare);
  }

  List<R> mapIndexed<R>(R Function(T e, int index) mapper) {
    final List<R> result = List.filled(length, null);
    for (var i = 0; i < length; i++) {
      result[i] = mapper(this[i], i);
    }
    return result;
  }

  bool none(bool Function(T e) predicate) {
    for (T element in this) {
      if (predicate(element)) return false;
    }
    return true;
  }

  List<int> indicesWhere(bool Function(T e) predicate) {
    final List<int> result = [];
    for (int i = 0; i < length; i++) {
      if (predicate(this[i])) result.add(i);
    }
    return result;
  }

  List<T> plus(T element) => [...this, element];

  List<T> minus(T element) => where((e) => e != element).toList();

  List<T> minusAll(List<T> elements) => where((e) => !elements.contains(e)).toList();

  List<T> skipDuplicates() => toSet().toList();
}

extension ComparableIterable<T extends Comparable<T>> on Iterable<T> {
  /// Returns the min value or null if there are no elements in this list.
  T minOrNull() {
    T min;
    for (T element in this) {
      if (min == null || element.compareTo(min) < 0) {
        min = element;
      }
    }

    return min;
  }

  /// Returns the max value or null if there are no elements in this list.
  T maxOrNull() {
    T max;
    for (T element in this) {
      if (max == null || element.compareTo(max) > 0) {
        max = element;
      }
    }

    return max;
  }
}

extension Iterables<T> on Iterable<T> {
  Iterable<T> withoutNulls() => where((e) => e != null);
}
