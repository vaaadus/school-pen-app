import 'package:flutter/foundation.dart';

extension MapExt<K, V> on Map<K, V> {
  /// Puts all keys & values from [other] map into this map.
  /// Returns this map to allow for cascade invocations.
  Map<K, V> putAll(Map<K, V> other) {
    other.forEach((K key, V value) => this[key] = value);
    return this;
  }

  /// Similar to [mapEquals], however this implementation is aware of the children
  /// and the possibility that some lists do not implement equals correctly (or at all).
  /// Therefore it is more specific to compare lists in a proper manner.
  static bool equalsWithListChildren<K, V>(Map<K, List<V>> a, Map<K, List<V>> b) {
    if (a == null) return b == null;
    if (b == null || a.length != b.length) return false;
    if (identical(a, b)) return true;

    for (final K key in a.keys) {
      if (!b.containsKey(key)) return false;
      if (!listEquals(a[key], b[key])) return false;
    }
    return true;
  }
}
