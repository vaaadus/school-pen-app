/// Returns a double rounded to two decimal places (2.005 becomes 2.01).
double roundToTwoDecimalPlaces(double num) {
  return (num * 100).round() / 100.0;
}
