import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:image/image.dart';
import 'package:school_pen/domain/common/files.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';

class ImageResizer {
  /// Crops the image to a square and resizes to the [size].
  ///
  /// The resulting [MemoryFile] won't have [MemoryFile.localPath] assigned
  /// and the image format will be converted to png.
  Future<MemoryFile> cropSquareAndResize(MemoryFile file, int size) {
    return compute(_cropSquareAndResize, _CropInput(file, size));
  }

  static MemoryFile _cropSquareAndResize(_CropInput input) {
    Image image = decodeImage(input.file.bytes);
    image = bakeOrientation(image);
    image = copyResizeCropSquare(image, input.size);

    return input.file.copyWith(
      name: Optional(Files.removeExtension(input.file.name) + '.png'),
      localPath: Optional(null),
      bytes: Optional(encodePng(image)),
    );
  }
}

class _CropInput extends Equatable {
  final MemoryFile file;
  final int size;

  const _CropInput(this.file, this.size);

  @override
  List<Object> get props => [file, size];
}
