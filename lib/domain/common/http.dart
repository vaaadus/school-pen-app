import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:school_pen/domain/common/result.dart';
import 'package:school_pen/domain/logger/specialized/http_logger.dart';

import 'exception/not_found_exception.dart';

const int _notFoundCode = 404;
const int _successCodeStart = 200;
const int _successCodeEnd = 299;
const Duration _timeout = Duration(seconds: 30);

typedef ResultParser<T> = T Function(String string);

Future<http.Response> httpGet(String path) {
  return http
      .get(path)
      .timeout(_timeout)
      .then((response) => logHttpResponse(response))
      .then((response) => _throwHttpExceptionIfError(response));
}

Future<http.Response> httpPost(String path, {Map<String, String> headers, String body}) {
  return http
      .post(path, headers: headers, body: body)
      .timeout(_timeout)
      .then((response) => logHttpResponse(response))
      .then((response) => _throwHttpExceptionIfError(response));
}

Future<Result<T>> httpGetAndParse<T>(String path, ResultParser<T> parser) async {
  try {
    final http.Response response = await httpGet(path);
    return Result.success(await compute(parser, response.body));
  } catch (error, stackTrace) {
    return Result.failure(error, stackTrace);
  }
}

Future<Result<T>> httpPostAndParse<T>(
  String path, {
  Map<String, String> headers,
  String body,
  @required ResultParser<T> parser,
}) async {
  try {
    final http.Response response = await httpPost(path, headers: headers, body: body);
    return Result.success(await compute(parser, response.body));
  } catch (error, stackTrace) {
    return Result.failure(error, stackTrace);
  }
}

http.Response _throwHttpExceptionIfError(http.Response response) {
  if (response.statusCode == _notFoundCode) {
    throw NotFoundException(response.request.url.toString());
  } else if (response.statusCode < _successCodeStart || response.statusCode > _successCodeEnd) {
    throw HttpException('Failed to ${response.request.method?.toLowerCase()} '
        '${response.request.url} with status code: ${response.statusCode}');
  } else {
    return response;
  }
}
