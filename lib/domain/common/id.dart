import 'package:uuid/uuid.dart';

class Id {
  static final Uuid _uuid = Uuid();

  /// Generates a random identifier (UUID).
  static String random() {
    return _uuid.v4();
  }
}
