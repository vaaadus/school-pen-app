import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/grade/model/add_grade_category.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/repository/category/grade_category_repository.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// A [GradeCategoryRepository] that saves categories in sembast document database.
class SembastGradeCategoryRepository implements GradeCategoryRepository {
  static const String keyDeletedIds = 'key_sembast_deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('grade_categories');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('grade_categories_bin');
  final BehaviorSubject<GradeCategoryRepository> _onChange = BehaviorSubject();
  final BehaviorSubject<String> _onCleanupCategory = BehaviorSubject();
  final Lock _lock = Lock();

  SembastGradeCategoryRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<GradeCategoryRepository> get onChange => _onChange;

  @override
  Stream<String> get onCleanupCategory => _onCleanupCategory;

  @override
  Future<List<GradeCategory>> getAll() async {
    final List<RecordSnapshot> snapshots = await _store.find(await _db);
    return snapshots.map((e) => _deserializeGradeCategory(e.value).toGradeCategory(id: e.key)).toList();
  }

  @override
  Future<GradeCategory> getById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> record = await _store.record(id).get(await _db);
    return _deserializeGradeCategory(record)?.toGradeCategory(id: id);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(keyDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<String> create(AddGradeCategory category) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = _serializeGradeCategory(category);
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddGradeCategory grade) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = _serializeGradeCategory(grade);
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = await getDeletedIds();
      if (ids.addIfAbsent(id)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      _onCleanupCategory.add(id);
      return deleted;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}

Map<String, dynamic> _serializeGradeCategory(AddGradeCategory category) {
  if (category == null) return null;

  return {
    'customName': category.customName,
    'updatedAt': SembastSerializer.serializeDateTimeField(category.updatedAt),
  };
}

AddGradeCategory _deserializeGradeCategory(Map<String, dynamic> map) {
  if (map == null) return null;

  return AddGradeCategory(
    customName: map['customName'],
    updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
  );
}
