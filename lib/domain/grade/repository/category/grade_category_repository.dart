import 'package:school_pen/domain/grade/model/add_grade_category.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';

abstract class GradeCategoryRepository {
  /// Stream changes whenever category repository changes.
  Stream<GradeCategoryRepository> get onChange;

  /// Stream ids of deleted categories to cleanup dependencies.
  Stream<String> get onCleanupCategory;

  /// Returns all stored categories.
  Future<List<GradeCategory>> getAll();

  /// Returns a list of ids of removed categories.
  Future<List<String>> getDeletedIds();

  /// Returns the category by id or null if it doesn't exist.
  Future<GradeCategory> getById(String id);

  /// Creates a new category. Returns its id.
  Future<String> create(AddGradeCategory category);

  /// Updates or creates a new category with id.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddGradeCategory category);

  /// Removes a category with id.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
