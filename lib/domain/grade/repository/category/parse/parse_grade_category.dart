import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/grade/model/add_grade_category.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';

class ParseGradeCategory extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'GradeCategory';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keyCustomName = 'customName';

  ParseGradeCategory() : super(keyClassName);

  ParseGradeCategory.clone() : this();

  factory ParseGradeCategory.fromModel(GradeCategory category, ParseUser user) => ParseGradeCategory()
    ..user = user
    ..uuid = category.id
    ..customName = category.customName
    ..updatedAt = category.updatedAt;

  AddGradeCategory toAddGradeCategory() => AddGradeCategory(
        customName: customName,
        updatedAt: updatedAt,
      );

  @override
  ParseGradeCategory clone(Map map) => ParseGradeCategory.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get customName => getNullable(keyCustomName);

  set customName(String customName) => setNullable(keyCustomName, customName);

  /// Creates or updates a category.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeGradeCategory').execute(parameters: {
      keyUuid: uuid,
      keyCustomName: customName,
    });
  }
}
