import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';

class ParseGradeCategoryBin extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'GradeCategory_Bin';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';

  ParseGradeCategoryBin() : super(keyClassName);

  ParseGradeCategoryBin.clone() : this();

  @override
  ParseGradeCategoryBin clone(Map map) => ParseGradeCategoryBin.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);
}
