import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/grade/model/add_grade_category.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/repository/category/grade_category_repository.dart';
import 'package:school_pen/domain/grade/repository/category/parse/parse_grade_category.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';

class ParseGradeCategoryRepository implements GradeCategoryRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseGradeCategoryRepository');
  final GradeCategoryRepository _delegate;
  Subscription _subscription;

  ParseGradeCategoryRepository(this._delegate);

  @override
  Stream<GradeCategoryRepository> get onChange => _delegate.onChange;

  @override
  Stream<String> get onCleanupCategory => _delegate.onCleanupCategory;

  @override
  Future<List<GradeCategory>> getAll() => _delegate.getAll();

  @override
  Future<GradeCategory> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddGradeCategory category) async {
    final String id = await _delegate.create(category);
    _try(() async {
      final GradeCategory newCategory = await _delegate.getById(id);
      await _storeOnParse(newCategory);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddGradeCategory category) async {
    final bool updated = await _delegate.update(id, category);
    if (updated) {
      _try(() async {
        final GradeCategory newCategory = await _delegate.getById(id);
        await _storeOnParse(newCategory);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(GradeCategory category) async {
    await _delegate.update(category.id, category.toAddGradeCategory());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseGradeCategory()));
    _subscription.on(LiveQueryEvent.create, (ParseGradeCategory object) {
      _try(() => _delegate.update(object.uuid, object.toAddGradeCategory()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseGradeCategory object) {
      _try(() => _delegate.update(object.uuid, object.toAddGradeCategory()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseGradeCategory object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(GradeCategory category) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseGradeCategory.fromModel(category, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseGradeCategory().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
