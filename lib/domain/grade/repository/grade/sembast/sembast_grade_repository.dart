import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/grade/model/add_grade.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

import '../grade_repository.dart';

/// A [GradeRepository] that saves grades in sembast document database.
class SembastGradeRepository implements GradeRepository {
  static const String keyDeletedIds = 'key_sembast_deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('grades');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('grades_bin');
  final BehaviorSubject<GradeRepository> _onChange = BehaviorSubject();
  final BehaviorSubject<String> _onCleanupGrade = BehaviorSubject();
  final Lock _lock = Lock();

  SembastGradeRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<GradeRepository> get onChange => _onChange;

  @override
  Stream<String> get onCleanupGrade => _onCleanupGrade;

  @override
  Future<List<Grade>> getAll({String courseId, String semesterId, String categoryId}) async {
    final List<Filter> filters = [
      if (courseId != null) Filter.equals('courseId', courseId),
      if (semesterId != null) Filter.equals('semesterId', semesterId),
      if (categoryId != null) Filter.equals('categoryId', categoryId),
    ];

    final Finder finder = filters.isEmpty ? null : Finder(filter: Filter.and(filters));
    final List<RecordSnapshot> snapshots = await _store.find(await _db, finder: finder);
    return snapshots.map((e) => _deserializeGrade(e.value).toGrade(id: e.key)).toList();
  }

  @override
  Future<Grade> getById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> record = await _store.record(id).get(await _db);
    return _deserializeGrade(record)?.toGrade(id: id);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(keyDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<String> create(AddGrade grade) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = _serializeGrade(grade);
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddGrade grade) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = _serializeGrade(grade);
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = await getDeletedIds();
      if (ids.addIfAbsent(id)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      _onCleanupGrade.add(id);
      return deleted;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}

Map<String, dynamic> _serializeGrade(AddGrade grade) {
  if (grade == null) return null;

  return {
    'semesterId': grade.semesterId,
    'courseId': grade.courseId,
    'name': grade.name,
    'value': grade.value,
    'weight': grade.weight,
    'categoryId': grade.categoryId,
    'dateTime': SembastSerializer.serializeDateTimeField(grade.dateTime),
    'updatedAt': SembastSerializer.serializeDateTimeField(grade.updatedAt),
  };
}

AddGrade _deserializeGrade(Map<String, dynamic> map) {
  if (map == null) return null;

  return AddGrade(
    semesterId: map['semesterId'],
    courseId: map['courseId'],
    name: map['name'],
    value: map['value'],
    weight: map['weight'],
    categoryId: map['categoryId'],
    dateTime: SembastSerializer.deserializeDateTimeField(map['dateTime']),
    updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
  );
}
