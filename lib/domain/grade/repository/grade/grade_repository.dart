import 'package:school_pen/domain/grade/model/add_grade.dart';
import 'package:school_pen/domain/grade/model/grade.dart';

abstract class GradeRepository {
  /// Stream changes whenever grades repository changes.
  Stream<GradeRepository> get onChange;

  /// Stream IDs of removed grades to provide cleanup of dependencies.
  Stream<String> get onCleanupGrade;

  /// Returns all stored grades.
  /// Can be filtered by [courseId], null means no filter.
  /// Can be filtered by [semesterId], null means no filter.
  /// Can by filtered by [categoryId], null means no filter.
  Future<List<Grade>> getAll({String courseId, String semesterId, String categoryId});

  /// Returns a list of ids of removed grades.
  Future<List<String>> getDeletedIds();

  /// Returns the grade by ID or null if it doesn't exist.
  Future<Grade> getById(String id);

  /// Creates a new grade and returns its ID.
  Future<String> create(AddGrade grade);

  /// Updates or creates a new grade with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddGrade grade);

  /// Removes a grade with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
