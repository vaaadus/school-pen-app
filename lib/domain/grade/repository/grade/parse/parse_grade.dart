import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/grade/model/add_grade.dart';
import 'package:school_pen/domain/grade/model/grade.dart';

class ParseGrade extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Grade';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keySemesterId = 'semesterUuid';
  static const String keyCourseId = 'courseUuid';
  static const String keyName = 'name';
  static const String keyValue = 'value';
  static const String keyWeight = 'weight';
  static const String keyCategoryId = 'categoryUuid';
  static const String keyDateTime = 'dateTime';

  ParseGrade() : super(keyClassName);

  ParseGrade.clone() : this();

  factory ParseGrade.fromModel(Grade grade, ParseUser user) => ParseGrade()
    ..user = user
    ..uuid = grade.id
    ..updatedAt = grade.updatedAt
    ..semesterId = grade.semesterId
    ..courseId = grade.courseId
    ..name = grade.name
    ..value = grade.value
    ..weight = grade.weight
    ..categoryId = grade.categoryId
    ..dateTime = grade.dateTime;

  AddGrade toAddGrade() => AddGrade(
        semesterId: semesterId,
        courseId: courseId,
        name: name,
        value: value,
        weight: weight,
        categoryId: categoryId,
        dateTime: dateTime,
        updatedAt: updatedAt,
      );

  @override
  ParseGrade clone(Map map) => ParseGrade.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get semesterId => getNullable(keySemesterId);

  set semesterId(String semesterId) => setNullable(keySemesterId, semesterId);

  String get courseId => getNullable(keyCourseId);

  set courseId(String courseId) => setNullable(keyCourseId, courseId);

  String get name => getNullable(keyName);

  set name(String name) => setNullable(keyName, name);

  double get value => getDouble(keyValue);

  set value(double value) => setNullable(keyValue, value);

  double get weight => getDouble(keyWeight);

  set weight(double weight) => setNullable(keyWeight, weight);

  String get categoryId => getNullable(keyCategoryId);

  set categoryId(String id) => setNullable(keyCategoryId, id);

  DateTime get dateTime => getDateTime(keyDateTime);

  set dateTime(DateTime dateTime) => setNullable(keyDateTime, dateTime);

  /// Creates or updates a grade.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeGrade').execute(parameters: {
      keyUuid: uuid,
      keySemesterId: semesterId,
      keyCourseId: courseId,
      keyName: name,
      keyValue: value,
      keyWeight: weight,
      keyCategoryId: categoryId,
      keyDateTime: dateTime?.toString(),
    });
  }
}
