import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';

class ParseGradeBin extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Grade_Bin';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';

  ParseGradeBin() : super(keyClassName);

  ParseGradeBin.clone() : this();

  @override
  ParseGradeBin clone(Map map) => ParseGradeBin.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);
}
