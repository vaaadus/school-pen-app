import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/grade/model/add_grade.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/repository/grade/grade_repository.dart';
import 'package:school_pen/domain/grade/repository/grade/parse/parse_grade.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';

class ParseGradeRepository implements GradeRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseGradeRepository');
  final GradeRepository _delegate;
  Subscription _subscription;

  ParseGradeRepository(this._delegate);

  @override
  Stream<GradeRepository> get onChange => _delegate.onChange;

  @override
  Stream<String> get onCleanupGrade => _delegate.onCleanupGrade;

  @override
  Future<List<Grade>> getAll({String courseId, String semesterId, String categoryId}) =>
      _delegate.getAll(courseId: courseId, semesterId: semesterId, categoryId: categoryId);

  @override
  Future<Grade> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddGrade grade) async {
    final String id = await _delegate.create(grade);
    _try(() async {
      final Grade newGrade = await _delegate.getById(id);
      await _storeOnParse(newGrade);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddGrade grade) async {
    final bool updated = await _delegate.update(id, grade);
    if (updated) {
      _try(() async {
        final Grade newGrade = await _delegate.getById(id);
        await _storeOnParse(newGrade);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(Grade grade) async {
    await _delegate.update(grade.id, grade.toAddGrade());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseGrade()));
    _subscription.on(LiveQueryEvent.create, (ParseGrade object) {
      _try(() => _delegate.update(object.uuid, object.toAddGrade()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseGrade object) {
      _try(() => _delegate.update(object.uuid, object.toAddGrade()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseGrade object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(Grade grade) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseGrade.fromModel(grade, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseGrade().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
