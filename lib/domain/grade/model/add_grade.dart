import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/date/date_constraints.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/grade/model/grade.dart';

/// Represents a request to add a student score.
/// The same as [Grade] but without the ID.
class AddGrade extends Equatable {
  final String semesterId;
  final String courseId;
  final String name;
  final double value;
  final double weight;
  final String categoryId;
  final DateTime dateTime;
  final DateTime updatedAt;

  const AddGrade({
    @required this.semesterId,
    @required this.courseId,
    this.name,
    @required this.value,
    this.weight = 1.0,
    this.categoryId,
    @required this.dateTime,
    @required this.updatedAt,
  })  : assert(value >= Grade.minValue && value <= Grade.maxValue),
        assert(weight >= Grade.minWeight && weight <= Grade.maxWeight);

  Grade toGrade({@required String id}) {
    return Grade(
      id: id,
      semesterId: semesterId,
      courseId: courseId,
      name: name,
      value: value,
      weight: weight,
      categoryId: categoryId,
      dateTime: dateTime,
      updatedAt: updatedAt,
    );
  }

  AddGrade trim() {
    return copyWith(name: Optional(name?.trim()));
  }

  AddGrade copyWith({
    Optional<String> semesterId,
    Optional<String> courseId,
    Optional<String> name,
    Optional<double> value,
    Optional<double> weight,
    Optional<String> categoryId,
    Optional<DateTime> dateTime,
    Optional<DateTime> updatedAt,
  }) {
    return AddGrade(
      semesterId: Optional.unwrapOrElse(semesterId, this.semesterId),
      courseId: Optional.unwrapOrElse(courseId, this.courseId),
      name: Optional.unwrapOrElse(name, this.name),
      value: Optional.unwrapOrElse(value, this.value),
      weight: Optional.unwrapOrElse(weight, this.weight),
      categoryId: Optional.unwrapOrElse(categoryId, this.categoryId),
      dateTime: Optional.unwrapOrElse(dateTime, this.dateTime),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  /// The constraints for the dateTime.
  static DateConstraints dateTimeConstraints(DateTime now) {
    return DateConstraints(
      minimumDate: now.minusDays(365),
      maximumDate: DateTime(2029, 12, 31),
    );
  }

  @override
  List<Object> get props => [semesterId, courseId, name, value, weight, categoryId, dateTime, updatedAt];
}
