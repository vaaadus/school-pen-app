import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/grade/model/add_grade_category.dart';

/// A group for grades having similar meaning to the user.
class GradeCategory extends Equatable {
  static final GradeCategory written = GradeCategory(id: 'written', updatedAt: DateTime.now());
  static final GradeCategory oral = GradeCategory(id: 'oral', updatedAt: DateTime.now());
  static final GradeCategory practical = GradeCategory(id: 'practical', updatedAt: DateTime.now());
  static final List<GradeCategory> predefined = [written, oral, practical];

  final String id;
  final String customName;
  final DateTime updatedAt;

  const GradeCategory({@required this.id, this.customName, @required this.updatedAt});

  AddGradeCategory toAddGradeCategory() {
    return AddGradeCategory(
      customName: customName,
      updatedAt: updatedAt,
    );
  }

  @override
  List<Object> get props => [id, customName, updatedAt];
}
