import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'add_grade.dart';

/// Represents a student score at school.
class Grade extends Equatable implements Comparable<Grade> {
  static const double minValue = 0.0;
  static const double maxValue = 100.0;
  static const double minWeight = 0.0;
  static const double maxWeight = 100.0;

  final String id;
  final String semesterId;
  final String courseId;
  final String name;
  final double value;
  final double weight;
  final String categoryId;
  final DateTime dateTime;
  final DateTime updatedAt;

  const Grade({
    @required this.id,
    @required this.semesterId,
    @required this.courseId,
    this.name,
    @required this.value,
    this.weight = 1.0,
    this.categoryId,
    @required this.dateTime,
    @required this.updatedAt,
  })  : assert(value >= minValue && value <= maxValue),
        assert(weight >= minWeight && weight <= maxWeight);

  AddGrade toAddGrade() {
    return AddGrade(
      semesterId: semesterId,
      courseId: courseId,
      name: name,
      value: value,
      weight: weight,
      categoryId: categoryId,
      dateTime: dateTime,
      updatedAt: updatedAt,
    );
  }

  @override
  int compareTo(Grade other) {
    return other.dateTime.compareTo(dateTime);
  }

  @override
  List<Object> get props => [id, semesterId, courseId, name, value, weight, categoryId, dateTime, updatedAt];
}
