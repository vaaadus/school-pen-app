import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:school_pen/app/common/formatter/number_formatter.dart';
import 'package:school_pen/domain/common/math.dart';

/// Defines possible ways students are evaluated:
/// - in percents 94/100%
/// - in letter units (A-F)
/// - in numbers
enum GradingSystemType {
  percentage,
  alphabetic,
  numeric,
}

/// A grading system type with constraints.
abstract class GradingSystem extends Equatable {
  final double minValue;
  final double maxValue;

  const GradingSystem({
    @required this.minValue,
    @required this.maxValue,
  });

  factory GradingSystem.of(GradingSystemType type, {double minValue, double maxValue}) {
    switch (type) {
      case GradingSystemType.percentage:
        return _PercentageGradingSystem();
      case GradingSystemType.alphabetic:
        return _AlphabeticGradingSystem();
      case GradingSystemType.numeric:
        assert(minValue != null);
        assert(maxValue != null);
        return _NumericGradingSystem(minValue: minValue, maxValue: maxValue);
    }
    throw ArgumentError('Unsupported grading system type: $type');
  }

  factory GradingSystem.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return GradingSystem.of(
      GradingSystemTypeExt.forTag(json['type']),
      minValue: json['minValue']?.toDouble(),
      maxValue: json['maxValue']?.toDouble(),
    );
  }

  static GradingSystem defaultSystem() => _PercentageGradingSystem();

  Map<String, dynamic> toJson() => {
        'type': type.tag,
        'minValue': minValue,
        'maxValue': maxValue,
      };

  /// The canonical type of the grading system.
  GradingSystemType get type;

  /// Formats the [gradle].
  String decorate(double grade);

  /// Attempts to parse the value from a raw string.
  /// The [value] will be coerced into min..max range.
  ///
  /// Returns null if the [value] cannot be parsed.
  double convert(String value);

  /// A list of all supported values.
  /// For numbers, only integers will be returned.
  List<String> allowedValues();

  @override
  List<Object> get props => [type, minValue, maxValue];
}

/// 0-100% grading system.
class _PercentageGradingSystem extends GradingSystem {
  const _PercentageGradingSystem() : super(minValue: 0, maxValue: 100);

  @override
  GradingSystemType get type => GradingSystemType.percentage;

  @override
  String decorate(double grade) {
    return NumberFormatter.format(grade) + '%';
  }

  @override
  double convert(String value) {
    value = value.replaceAll(RegExp('%'), '');

    final double number = tryParseDouble(value);
    if (number == null) return null;
    if (number > maxValue) return null;
    if (number < minValue) return null;
    return number;
  }

  @override
  List<String> allowedValues() {
    return [
      for (double i = minValue; i <= maxValue; i++) NumberFormatter.format(i),
    ];
  }
}

/// A-F (0-5) grading system.
class _AlphabeticGradingSystem extends GradingSystem {
  const _AlphabeticGradingSystem() : super(minValue: 0, maxValue: 100);

  @override
  GradingSystemType get type => GradingSystemType.alphabetic;

  @override
  String decorate(double grade) {
    if (grade < 65) return 'F';
    if (grade < 67) return 'D';
    if (grade < 70) return 'D+';
    if (grade < 73) return 'C-';
    if (grade < 77) return 'C';
    if (grade < 80) return 'C+';
    if (grade < 83) return 'B-';
    if (grade < 87) return 'B';
    if (grade < 90) return 'B+';
    if (grade < 93) return 'A-';
    if (grade < 97) return 'A';
    return 'A+';
  }

  @override
  double convert(String value) {
    value ??= '';
    value = value.replaceAll(RegExp('\\s'), '');
    value = value.toUpperCase();

    if (value.length == 2) {
      double result = _convert(value.substring(0, 2));
      if (result != null) return result;
    }

    // ignore: prefer_is_empty
    if (value.length == 1) {
      double result = _convert(value.substring(0, 1));
      if (result != null) return result;
    }

    return null;
  }

  double _convert(String value) {
    if (value == 'A+' || value == '+A') return 100;
    if (value == 'A') return 95;
    if (value == 'A-' || value == '-A') return 91.5;
    if (value == 'B+' || value == '+B') return 88.5;
    if (value == 'B') return 85;
    if (value == 'B-' || value == '-B') return 81.5;
    if (value == 'C+' || value == '+C') return 78.5;
    if (value == 'C') return 75;
    if (value == 'C-' || value == '-C') return 71.5;
    if (value == 'D+' || value == '+D') return 68.5;
    if (value == 'D') return 66;
    if (value == 'F') return 30;

    return null;
  }

  @override
  List<String> allowedValues() {
    return ['A+', 'A', 'A-', 'B+', 'B', 'B-', 'C+', 'C', 'C-', 'D+', 'D', 'F'];
  }
}

/// [minValue]...[maxValue] grading system.
class _NumericGradingSystem extends GradingSystem {
  const _NumericGradingSystem({@required double minValue, @required double maxValue})
      : super(minValue: minValue, maxValue: maxValue);

  @override
  GradingSystemType get type => GradingSystemType.numeric;

  @override
  String decorate(double grade) {
    final double value = grade * (maxValue - minValue) / 100 + minValue;
    return NumberFormatter.format(value);
  }

  @override
  double convert(String value) {
    double number = tryParseDouble(value);
    if (number == null) return null;
    if (number > max(minValue, maxValue)) return null;
    if (number < min(minValue, maxValue)) return null;

    number = (number - minValue) / (maxValue - minValue);
    return number.abs() * 100;
  }

  @override
  List<String> allowedValues() {
    return [
      if (minValue <= maxValue)
        for (double i = minValue; i <= maxValue; i++) NumberFormatter.format(i),
      if (minValue > maxValue)
        for (double i = minValue; i >= maxValue; i--) NumberFormatter.format(i),
    ];
  }
}

extension GradingSystemTypeExt on GradingSystemType {
  static GradingSystemType forTag(String tag) {
    for (GradingSystemType unit in GradingSystemType.values) {
      if (unit.tag == tag) return unit;
    }

    return null;
  }

  String get tag {
    switch (this) {
      case GradingSystemType.percentage:
        return 'percentage';
      case GradingSystemType.numeric:
        return 'numeric';
      case GradingSystemType.alphabetic:
        return 'alphabetic';
    }
    throw ArgumentError('Unsupported $this');
  }
}
