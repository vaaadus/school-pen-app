import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';

/// Represents a request to add a custom grade category.
class AddGradeCategory extends Equatable {
  final String customName;
  final DateTime updatedAt;

  const AddGradeCategory({this.customName, @required this.updatedAt});

  GradeCategory toGradeCategory({@required String id}) {
    return GradeCategory(
      id: id,
      customName: customName,
      updatedAt: updatedAt,
    );
  }

  AddGradeCategory trim() {
    return AddGradeCategory(
      customName: customName?.trim(),
      updatedAt: updatedAt,
    );
  }

  bool get isValid => Strings.isNotBlank(customName);

  @override
  List<Object> get props => [customName, updatedAt];
}
