import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

/// Represents student score stats.
class CourseGradesStats extends Equatable {
  final String courseId;
  final String semesterId;
  final int gradesCount;
  final double averageGrade;

  const CourseGradesStats({
    @required this.courseId,
    @required this.semesterId,
    @required this.gradesCount,
    this.averageGrade,
  });

  @override
  List<Object> get props => [courseId, semesterId, gradesCount, averageGrade];
}
