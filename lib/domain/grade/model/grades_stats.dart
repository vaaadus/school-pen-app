import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

/// Represents student score stats.
class GradesStats extends Equatable {
  final String semesterId;
  final int coursesCount;
  final double averageGrade;
  final double totalCreditHours;

  const GradesStats({
    @required this.semesterId,
    @required this.coursesCount,
    this.averageGrade,
    @required this.totalCreditHours,
  });

  @override
  List<Object> get props => [semesterId, coursesCount, averageGrade, totalCreditHours];
}
