abstract class GradeCleanupListener {
  /// Called when a grade is removed and all related data must be removed as well.
  Future<void> onCleanupGrade(String gradeId);
}
