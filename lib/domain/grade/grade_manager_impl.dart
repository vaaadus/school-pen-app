import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/course/course_cleanup_listener.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/grade/grade_cleanup_listener.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/model/add_grade.dart';
import 'package:school_pen/domain/grade/model/add_grade_category.dart';
import 'package:school_pen/domain/grade/model/course_grades_stats.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/model/grades_stats.dart';
import 'package:school_pen/domain/grade/repository/category/grade_category_repository.dart';
import 'package:school_pen/domain/grade/repository/grade/grade_repository.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_cleanup_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';

import 'grade_manager.dart';
import 'model/grading_system.dart';

class GradeManagerImpl implements GradeManager, CourseListener, CourseCleanupListener, SemesterCleanupListener {
  static const Logger _logger = Logger('GradeManagerImpl');

  final Set<GradeListener> _listeners = {};
  final Set<GradeCleanupListener> _cleanupListeners = {};
  final GradeRepository _repository;
  final GradeCategoryRepository _categoryRepository;
  final SemesterManager _semesterManager;
  final CourseManager _courseManager;
  final UserConfigRepository _userconfig;

  GradeManagerImpl(
      this._repository, this._categoryRepository, this._semesterManager, this._courseManager, this._userconfig) {
    _courseManager.addCourseListener(this);
    _courseManager.addCourseCleanupListener(this);
    _semesterManager.addSemesterCleanupListener(this);
    _repository.onChange.listen((event) => _dispatchGradesChanged());
    _repository.onCleanupGrade.listen((gradeId) => _dispatchGradesCleanup(gradeId));
    _categoryRepository.onChange.listen((event) => _dispatchGradesChanged());
    _categoryRepository.onCleanupCategory.listen((String categoryId) => _onCleanupCategory(categoryId));
    _userconfig.onChange.listen((List<Property> properties) {
      if (properties.contains(Property.gradingSystem)) {
        _dispatchGradesChanged();
      }
    });
  }

  @override
  void addGradeListener(GradeListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onGradesChanged(this);
  }

  @override
  void removeGradeListener(GradeListener listener) {
    _listeners.remove(listener);
  }

  @override
  void addGradeCleanupListener(GradeCleanupListener listener) {
    _cleanupListeners.add(listener);
  }

  @override
  void removeGradeCleanupListener(GradeCleanupListener listener) {
    _cleanupListeners.remove(listener);
  }

  @override
  Future<GradingSystem> getGradingSystem() async {
    return await _userconfig.getGradingSystem() ?? GradingSystem.defaultSystem();
  }

  @override
  Future<void> setGradingSystem(GradingSystem system) async {
    await _userconfig.setGradingSystem(system);
  }

  @override
  Future<List<GradeCategory>> getCategories() async {
    final List<GradeCategory> customCategories = await _categoryRepository.getAll();
    return [
      ...GradeCategory.predefined,
      ...customCategories,
    ];
  }

  @override
  Future<GradeCategory> getCategoryById(String id) async {
    final GradeCategory predefinedCategory = GradeCategory.predefined.firstWhereOrNull((e) => e.id == id);
    if (predefinedCategory != null) {
      return predefinedCategory;
    }
    return _categoryRepository.getById(id);
  }

  @override
  Future<String> createCategory(AddGradeCategory category) async {
    if (category.isValid) {
      return _categoryRepository.create(category.trim());
    } else {
      throw ArgumentError('Category not valid: $category');
    }
  }

  @override
  Future<void> updateCategory(String id, AddGradeCategory category) async {
    if (GradeCategory.predefined.any((e) => e.id == id)) {
      throw ArgumentError('Predefined category cannot be updated');
    } else if (category.isValid) {
      await _categoryRepository.update(id, category.trim());
    } else {
      throw ArgumentError('Category not valid: $category');
    }
  }

  @override
  Future<void> deleteCategory(String id) async {
    await _categoryRepository.delete(id);
  }

  @override
  Future<GradesStats> getStats({@required String semesterId}) async {
    final Semester semester = await _semesterManager.getById(semesterId);
    final List<Course> courses = await _courseManager.getAll(semester.schoolYearId);
    final List<Grade> grades = await getAll(semesterId: semesterId);
    return _calculateStats(semesterId, courses, grades);
  }

  @override
  Future<CourseGradesStats> getStatsById({@required String courseId, String semesterId}) async {
    final List<Grade> grades = await getAll(courseId: courseId, semesterId: semesterId);
    final Course course = await _courseManager.getById(courseId);
    if (course == null) return null;

    return _calculateCourseStats(semesterId, course, grades);
  }

  @override
  Future<List<Grade>> getAll({String courseId, String semesterId}) async {
    final List<Grade> grades = await _repository.getAll(courseId: courseId, semesterId: semesterId);
    grades.sort();
    return grades;
  }

  @override
  Future<Grade> getById(String id) {
    if (id == null) return null;
    return _repository.getById(id);
  }

  @override
  Future<String> create(AddGrade grade) async {
    return _repository.create(grade?.trim());
  }

  @override
  Future<void> update(String id, AddGrade grade) async {
    await _repository.update(id, grade?.trim());
  }

  @override
  Future<void> delete(String id) async {
    await _repository.delete(id);
  }

  @override
  void onCoursesChanged(CourseManager manager) {
    _dispatchGradesChanged();
  }

  @override
  Future<void> onCleanupSemester(String semesterId) async {
    final List<Future> futures = [];

    for (Grade grade in await getAll(semesterId: semesterId)) {
      futures.add(delete(grade.id));
    }

    return Future.wait(futures);
  }

  @override
  Future<void> onCleanupCourse(String courseId) async {
    final List<Future> futures = [];

    for (Grade grade in await getAll(courseId: courseId)) {
      futures.add(delete(grade.id));
    }

    return Future.wait(futures);
  }

  GradesStats _calculateStats(String semesterId, List<Course> courses, List<Grade> grades) {
    final List<CourseGradesStats> stats = courses.map((e) => _calculateCourseStats(semesterId, e, grades)).toList();

    double totalSum = 0;
    int count = 0;
    for (CourseGradesStats s in stats) {
      if (s.averageGrade != null) {
        totalSum += s.averageGrade;
        count++;
      }
    }

    double totalCreditHours = 0;
    for (Course course in courses) {
      totalCreditHours += course.creditHours ?? 0;
    }

    return GradesStats(
      semesterId: semesterId,
      coursesCount: courses.length,
      averageGrade: count == 0 ? null : totalSum / count,
      totalCreditHours: totalCreditHours,
    );
  }

  CourseGradesStats _calculateCourseStats(String semesterId, Course course, List<Grade> grades) {
    grades = grades.where((e) => e.courseId == course.id).toList();

    return CourseGradesStats(
      courseId: course.id,
      semesterId: semesterId,
      gradesCount: grades.length,
      averageGrade: _calculateAverage(grades),
    );
  }

  double _calculateAverage(List<Grade> grades) {
    if (grades.isEmpty) return null;

    double weightedValuesSum = 0;
    double weightsSum = 0;

    grades.forEach((e) {
      weightedValuesSum += e.value * e.weight;
      weightsSum += e.weight;
    });

    if (weightsSum > 0.0) {
      return weightedValuesSum / weightsSum;
    } else {
      return 0.0;
    }
  }

  void _onCleanupCategory(String categoryId) async {
    try {
      for (Grade grade in await _repository.getAll(categoryId: categoryId)) {
        final AddGrade newGrade = grade.toAddGrade().copyWith(categoryId: null);
        await _repository.update(grade.id, newGrade);
      }
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  void _dispatchGradesChanged() {
    for (var listener in _listeners) {
      listener.onGradesChanged(this);
    }
  }

  void _dispatchGradesCleanup(String gradeId) {
    for (var listener in _cleanupListeners) {
      listener.onCleanupGrade(gradeId);
    }
  }
}
