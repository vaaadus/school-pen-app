import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/grade/model/add_grade.dart';
import 'package:school_pen/domain/grade/model/add_grade_category.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/model/grades_stats.dart';

import 'grade_cleanup_listener.dart';
import 'grade_listener.dart';
import 'model/course_grades_stats.dart';
import 'model/grade.dart';
import 'model/grading_system.dart';

abstract class GradeManager {
  void addGradeListener(GradeListener listener, {bool notifyOnAttach = true});

  void removeGradeListener(GradeListener listener);

  void addGradeCleanupListener(GradeCleanupListener listener);

  void removeGradeCleanupListener(GradeCleanupListener listener);

  /// Returns the global grading system. Will be always non-null.
  Future<GradingSystem> getGradingSystem();

  /// Sets the global grade system Must be non-null.
  Future<void> setGradingSystem(GradingSystem system);

  /// Returns all grade categories.
  Future<List<GradeCategory>> getCategories();

  /// Returns a category by ID or null if it doesn't exist.
  Future<GradeCategory> getCategoryById(String id);

  /// Creates a new grade category and returns its ID.
  Future<String> createCategory(AddGradeCategory category);

  Future<void> updateCategory(String id, AddGradeCategory category);

  Future<void> deleteCategory(String id);

  /// Returns the calculated stats for all the subjects in the [semesterId].
  Future<GradesStats> getStats({@required String semesterId});

  /// Returns the calculated stats for the subject by ID or null if it doesn't exist.
  /// Can be filtered by semester ID, null means no filter.
  Future<CourseGradesStats> getStatsById({@required String courseId, String semesterId});

  /// Returns all grades assigned to a subject.
  /// Can be filtered by semester ID, null means no filter.
  Future<List<Grade>> getAll({String courseId, String semesterId});

  /// Returns a grade by ID or null if it doesn't exist.
  Future<Grade> getById(String id);

  /// Creates a new grade and returns its ID.
  Future<String> create(AddGrade grade);

  Future<void> update(String id, AddGrade grade);

  Future<void> delete(String id);
}
