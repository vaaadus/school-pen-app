import 'grade_manager.dart';

abstract class GradeListener {
  /// Called whenever any property of [GradeManager] changes.
  void onGradesChanged(GradeManager manager);
}
