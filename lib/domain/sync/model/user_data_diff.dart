import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/sync/model/user_data.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/userconfig/model/user_config.dart';

/// A set of differences between user data stored locally (on the device) and remotely (in the cloud).
class UserDataDiff extends Equatable {
  final UserConfigDiffDetails userConfig;
  final UserDataDiffDetails<SchoolYear> schoolYears;
  final UserDataDiffDetails<Semester> semesters;
  final UserDataDiffDetails<Note> notes;
  final UserDataDiffDetails<Teacher> teachers;
  final UserDataDiffDetails<Course> courses;
  final UserDataDiffDetails<Grade> grades;
  final UserDataDiffDetails<GradeCategory> gradeCategories;
  final UserDataDiffDetails<Timetable> publicTimetables;
  final UserDataDiffDetails<TimetableInfo> customTimetables;
  final UserDataDiffDetails<TimetableEvent> timetableEvents;
  final UserDataDiffDetails<Exam> exams;
  final UserDataDiffDetails<Homework> homework;
  final UserDataDiffDetails<Reminder> reminders;

  const UserDataDiff({
    @required this.userConfig,
    @required this.schoolYears,
    @required this.semesters,
    @required this.notes,
    @required this.teachers,
    @required this.courses,
    @required this.grades,
    @required this.gradeCategories,
    @required this.publicTimetables,
    @required this.customTimetables,
    @required this.timetableEvents,
    @required this.exams,
    @required this.homework,
    @required this.reminders,
  });

  factory UserDataDiff.from({@required UserData remote, @required UserData local}) {
    return UserDataDiff(
      userConfig: UserConfigDiffDetails.from(
        remote: remote.userConfig,
        local: local.userConfig,
      ),
      schoolYears: UserDataDiffDetails.from(
        remote: remote.schoolYears.map((e) => _SchoolYearSyncObject(e)).toList(),
        local: local.schoolYears.map((e) => _SchoolYearSyncObject(e)).toList(),
        remoteBin: remote.deletedSchoolYears,
        localBin: local.deletedSchoolYears,
      ),
      semesters: UserDataDiffDetails.from(
        remote: remote.semesters.map((e) => _SemesterSyncObject(e)).toList(),
        local: local.semesters.map((e) => _SemesterSyncObject(e)).toList(),
        remoteBin: remote.deletedSemesters,
        localBin: local.deletedSemesters,
      ),
      notes: UserDataDiffDetails.from(
        remote: remote.notes.map((e) => _NoteSyncObject(e)).toList(),
        local: local.notes.map((e) => _NoteSyncObject(e)).toList(),
        remoteBin: remote.deletedNotes,
        localBin: local.deletedNotes,
      ),
      teachers: UserDataDiffDetails.from(
        remote: remote.teachers.map((e) => _TeacherSyncObject(e)).toList(),
        local: local.teachers.map((e) => _TeacherSyncObject(e)).toList(),
        remoteBin: remote.deletedTeachers,
        localBin: local.deletedTeachers,
      ),
      courses: UserDataDiffDetails.from(
        remote: remote.courses.map((e) => _CourseSyncObject(e)).toList(),
        local: local.courses.map((e) => _CourseSyncObject(e)).toList(),
        remoteBin: remote.deletedCourses,
        localBin: local.deletedCourses,
      ),
      grades: UserDataDiffDetails.from(
        remote: remote.grades.map((e) => _GradeSyncObject(e)).toList(),
        local: local.grades.map((e) => _GradeSyncObject(e)).toList(),
        remoteBin: remote.deletedGrades,
        localBin: local.deletedGrades,
      ),
      gradeCategories: UserDataDiffDetails.from(
        remote: remote.gradeCategories.map((e) => _GradeCategorySyncObject(e)).toList(),
        local: local.gradeCategories.map((e) => _GradeCategorySyncObject(e)).toList(),
        remoteBin: remote.deletedGradeCategories,
        localBin: local.deletedGradeCategories,
      ),
      publicTimetables: UserDataDiffDetails(
        createdOnLocal: [],
        updatedOnLocal: [],
        createdOnRemote: [],
        updatedOnRemote: remote.publicTimetables,
        deletedOnLocal: [],
        deletedOnRemote: [],
      ),
      customTimetables: UserDataDiffDetails.from(
        remote: remote.customTimetables.map((e) => _TimetableInfoSyncObject(e)).toList(),
        local: local.customTimetables.map((e) => _TimetableInfoSyncObject(e)).toList(),
        remoteBin: remote.deletedCustomTimetables,
        localBin: local.deletedCustomTimetables,
      ),
      timetableEvents: UserDataDiffDetails.from(
        remote: remote.customTimetableEvents.map((e) => _TimetableEventSyncObject(e)).toList(),
        local: local.customTimetableEvents.map((e) => _TimetableEventSyncObject(e)).toList(),
        remoteBin: remote.deletedCustomTimetableEvents,
        localBin: local.deletedCustomTimetableEvents,
      ),
      exams: UserDataDiffDetails.from(
        remote: remote.exams.map((e) => _ExamSyncObject(e)).toList(),
        local: local.exams.map((e) => _ExamSyncObject(e)).toList(),
        remoteBin: remote.deletedExams,
        localBin: local.deletedExams,
      ),
      homework: UserDataDiffDetails.from(
        remote: remote.homework.map((e) => _HomeworkSyncObject(e)).toList(),
        local: local.homework.map((e) => _HomeworkSyncObject(e)).toList(),
        remoteBin: remote.deletedHomework,
        localBin: local.deletedHomework,
      ),
      reminders: UserDataDiffDetails.from(
        remote: remote.reminders.map((e) => _ReminderSyncObject(e)).toList(),
        local: local.reminders.map((e) => _ReminderSyncObject(e)).toList(),
        remoteBin: remote.deletedReminders,
        localBin: local.deletedReminders,
      ),
    );
  }

  @override
  List<Object> get props => [
        userConfig,
        schoolYears,
        semesters,
        notes,
        teachers,
        courses,
        grades,
        gradeCategories,
        publicTimetables,
        customTimetables,
        timetableEvents,
        exams,
      ];
}

class UserConfigDiffDetails extends Equatable {
  final UserConfig updatedOnLocal;
  final UserConfig updatedOnRemote;

  const UserConfigDiffDetails({
    @required this.updatedOnLocal,
    @required this.updatedOnRemote,
  });

  factory UserConfigDiffDetails.from({
    @required UserConfig remote,
    @required UserConfig local,
  }) {
    final DateTime dateTime = DateTime.now();
    if (remote.withUpdatedAt(dateTime) == local.withUpdatedAt(dateTime)) {
      return UserConfigDiffDetails(updatedOnLocal: null, updatedOnRemote: null);
    } else {
      final UserConfig merged = remote.mergeWith(local);
      return UserConfigDiffDetails(updatedOnLocal: merged, updatedOnRemote: merged);
    }
  }

  @override
  List<Object> get props => [updatedOnLocal, updatedOnRemote];
}

class UserDataDiffDetails<T> extends Equatable {
  final List<T> createdOnLocal;
  final List<T> updatedOnLocal;
  final List<T> deletedOnLocal;
  final List<T> createdOnRemote;
  final List<T> updatedOnRemote;
  final List<T> deletedOnRemote;

  const UserDataDiffDetails({
    @required this.createdOnLocal,
    @required this.updatedOnLocal,
    @required this.deletedOnLocal,
    @required this.createdOnRemote,
    @required this.updatedOnRemote,
    @required this.deletedOnRemote,
  });

  factory UserDataDiffDetails.from({
    @required List<_SyncObject<T>> remote,
    @required List<_SyncObject<T>> local,
    @required List<String> remoteBin,
    @required List<String> localBin,
  }) {
    final List<_SyncObject<T>> createdOnLocal = [];
    final List<_SyncObject<T>> updatedOnLocal = [];
    final List<_SyncObject<T>> deletedOnLocal = [];
    final List<_SyncObject<T>> createdOnRemote = [];
    final List<_SyncObject<T>> updatedOnRemote = [];
    final List<_SyncObject<T>> deletedOnRemote = [];

    for (_SyncObject<T> remoteObject in remote) {
      final _SyncObject<T> localObject = local.firstWhereOrNull((e) => e.id == remoteObject.id);
      if (localObject == null && localBin.contains(remoteObject.id)) {
        deletedOnLocal.add(remoteObject);
      } else if (localObject == null) {
        createdOnRemote.add(remoteObject);
      } else if (localObject.updatedAt.isBefore(remoteObject.updatedAt)) {
        updatedOnRemote.add(remoteObject);
      } else if (localObject.updatedAt.isAfter(remoteObject.updatedAt)) {
        updatedOnLocal.add(localObject);
      }
    }

    for (_SyncObject<T> localObject in local) {
      final _SyncObject<T> remoteObject = remote.firstWhereOrNull((e) => e.id == localObject.id);
      if (remoteObject == null && remoteBin.contains(localObject.id)) {
        deletedOnRemote.add(localObject);
      } else if (remoteObject == null) {
        createdOnLocal.add(localObject);
      }
    }

    return UserDataDiffDetails(
      createdOnLocal: createdOnLocal.map((e) => e.object).toList(),
      updatedOnLocal: updatedOnLocal.map((e) => e.object).toList(),
      deletedOnLocal: deletedOnLocal.map((e) => e.object).toList(),
      createdOnRemote: createdOnRemote.map((e) => e.object).toList(),
      updatedOnRemote: updatedOnRemote.map((e) => e.object).toList(),
      deletedOnRemote: deletedOnRemote.map((e) => e.object).toList(),
    );
  }

  @override
  List<Object> get props => [
        createdOnLocal,
        updatedOnLocal,
        deletedOnLocal,
        createdOnRemote,
        updatedOnRemote,
        deletedOnRemote,
      ];
}

abstract class _SyncObject<T> extends Equatable {
  const _SyncObject();

  T get object;

  String get id;

  DateTime get updatedAt;

  @override
  List<Object> get props => [object, id, updatedAt];
}

class _SchoolYearSyncObject extends _SyncObject<SchoolYear> {
  final SchoolYear year;

  const _SchoolYearSyncObject(this.year);

  @override
  SchoolYear get object => year;

  @override
  String get id => year.id;

  @override
  DateTime get updatedAt => year.updatedAt;
}

class _SemesterSyncObject extends _SyncObject<Semester> {
  final Semester semester;

  const _SemesterSyncObject(this.semester);

  @override
  Semester get object => semester;

  @override
  String get id => semester.id;

  @override
  DateTime get updatedAt => semester.updatedAt;
}

class _NoteSyncObject extends _SyncObject<Note> {
  final Note note;

  const _NoteSyncObject(this.note);

  @override
  Note get object => note;

  @override
  String get id => note.id;

  @override
  DateTime get updatedAt => note.updatedAt;
}

class _TeacherSyncObject extends _SyncObject<Teacher> {
  final Teacher teacher;

  const _TeacherSyncObject(this.teacher);

  @override
  Teacher get object => teacher;

  @override
  String get id => teacher.id;

  @override
  DateTime get updatedAt => teacher.updatedAt;
}

class _CourseSyncObject extends _SyncObject<Course> {
  final Course course;

  const _CourseSyncObject(this.course);

  @override
  Course get object => course;

  @override
  String get id => course.id;

  @override
  DateTime get updatedAt => course.updatedAt;
}

class _GradeSyncObject extends _SyncObject<Grade> {
  final Grade grade;

  const _GradeSyncObject(this.grade);

  @override
  Grade get object => grade;

  @override
  String get id => grade.id;

  @override
  DateTime get updatedAt => grade.updatedAt;
}

class _GradeCategorySyncObject extends _SyncObject<GradeCategory> {
  final GradeCategory category;

  const _GradeCategorySyncObject(this.category);

  @override
  String get id => category.id;

  @override
  GradeCategory get object => category;

  @override
  DateTime get updatedAt => category.updatedAt;
}

class _TimetableInfoSyncObject extends _SyncObject<TimetableInfo> {
  final TimetableInfo timetable;

  const _TimetableInfoSyncObject(this.timetable);

  @override
  String get id => timetable.id;

  @override
  TimetableInfo get object => timetable;

  @override
  DateTime get updatedAt => timetable.updatedAt;
}

class _TimetableEventSyncObject extends _SyncObject<TimetableEvent> {
  final TimetableEvent event;

  const _TimetableEventSyncObject(this.event);

  @override
  String get id => event.id;

  @override
  TimetableEvent get object => event;

  @override
  DateTime get updatedAt => event.updatedAt;
}

class _ExamSyncObject extends _SyncObject<Exam> {
  final Exam exam;

  const _ExamSyncObject(this.exam);

  @override
  String get id => exam.id;

  @override
  Exam get object => exam;

  @override
  DateTime get updatedAt => exam.updatedAt;
}

class _HomeworkSyncObject extends _SyncObject<Homework> {
  final Homework homework;

  const _HomeworkSyncObject(this.homework);

  @override
  String get id => homework.id;

  @override
  Homework get object => homework;

  @override
  DateTime get updatedAt => homework.updatedAt;
}

class _ReminderSyncObject extends _SyncObject<Reminder> {
  final Reminder reminder;

  const _ReminderSyncObject(this.reminder);

  @override
  String get id => reminder.id;

  @override
  Reminder get object => reminder;

  @override
  DateTime get updatedAt => reminder.updatedAt;
}
