import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/userconfig/model/user_config.dart';

/// A set of all user data that is synced.
class UserData extends Equatable {
  final UserConfig userConfig;
  final List<SchoolYear> schoolYears;
  final List<String> deletedSchoolYears;
  final List<Semester> semesters;
  final List<String> deletedSemesters;
  final List<Note> notes;
  final List<String> deletedNotes;
  final List<Teacher> teachers;
  final List<String> deletedTeachers;
  final List<Course> courses;
  final List<String> deletedCourses;
  final List<Grade> grades;
  final List<String> deletedGrades;
  final List<GradeCategory> gradeCategories;
  final List<String> deletedGradeCategories;
  final List<Timetable> publicTimetables;
  final List<TimetableInfo> customTimetables;
  final List<String> deletedCustomTimetables;
  final List<TimetableEvent> customTimetableEvents;
  final List<String> deletedCustomTimetableEvents;
  final List<Exam> exams;
  final List<String> deletedExams;
  final List<Homework> homework;
  final List<String> deletedHomework;
  final List<Reminder> reminders;
  final List<String> deletedReminders;

  const UserData({
    @required this.userConfig,
    @required this.schoolYears,
    @required this.deletedSchoolYears,
    @required this.semesters,
    @required this.deletedSemesters,
    @required this.notes,
    @required this.deletedNotes,
    @required this.teachers,
    @required this.deletedTeachers,
    @required this.courses,
    @required this.deletedCourses,
    @required this.grades,
    @required this.deletedGrades,
    @required this.gradeCategories,
    @required this.deletedGradeCategories,
    @required this.publicTimetables,
    @required this.customTimetables,
    @required this.deletedCustomTimetables,
    @required this.customTimetableEvents,
    @required this.deletedCustomTimetableEvents,
    @required this.exams,
    @required this.deletedExams,
    @required this.homework,
    @required this.deletedHomework,
    @required this.reminders,
    @required this.deletedReminders,
  })  : assert(userConfig != null),
        assert(schoolYears != null),
        assert(deletedSchoolYears != null),
        assert(semesters != null),
        assert(deletedSemesters != null),
        assert(notes != null),
        assert(deletedNotes != null),
        assert(teachers != null),
        assert(deletedTeachers != null),
        assert(courses != null),
        assert(deletedCourses != null),
        assert(grades != null),
        assert(deletedGrades != null),
        assert(gradeCategories != null),
        assert(deletedGradeCategories != null),
        assert(publicTimetables != null),
        assert(customTimetables != null),
        assert(deletedCustomTimetables != null),
        assert(customTimetableEvents != null),
        assert(deletedCustomTimetableEvents != null),
        assert(exams != null),
        assert(deletedExams != null),
        assert(homework != null),
        assert(deletedHomework != null),
        assert(reminders != null),
        assert(deletedReminders != null);

  @override
  List<Object> get props => [
        userConfig,
        schoolYears,
        deletedSchoolYears,
        semesters,
        deletedSemesters,
        notes,
        deletedNotes,
        teachers,
        deletedTeachers,
        courses,
        deletedCourses,
        grades,
        deletedGrades,
        gradeCategories,
        deletedGradeCategories,
        publicTimetables,
        customTimetables,
        deletedCustomTimetables,
        customTimetableEvents,
        deletedCustomTimetableEvents,
        exams,
        deletedExams,
        homework,
        deletedHomework,
        reminders,
        deletedReminders,
      ];
}
