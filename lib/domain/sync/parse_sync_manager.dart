import 'dart:async';

import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/course/repository/parse/parse_course.dart';
import 'package:school_pen/domain/course/repository/parse/parse_course_repository.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/exam/repository/parse/parse_exam.dart';
import 'package:school_pen/domain/exam/repository/parse/parse_exam_repository.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/repository/category/parse/parse_grade_category.dart';
import 'package:school_pen/domain/grade/repository/category/parse/parse_grade_category_repository.dart';
import 'package:school_pen/domain/grade/repository/grade/parse/parse_grade.dart';
import 'package:school_pen/domain/grade/repository/grade/parse/parse_grade_repository.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/homework/repository/parse/parse_homework.dart';
import 'package:school_pen/domain/homework/repository/parse/parse_homework_repository.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/repository/parse/parse_note.dart';
import 'package:school_pen/domain/note/repository/parse/parse_note_repository.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/reminder/repository/parse/parse_reminder.dart';
import 'package:school_pen/domain/reminder/repository/parse/parse_reminder_repository.dart';
import 'package:school_pen/domain/remoteconfig/remote_config_flag.dart';
import 'package:school_pen/domain/remoteconfig/remote_config_manager.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/repository/parse/parse_school_year.dart';
import 'package:school_pen/domain/schoolyear/repository/parse/parse_school_year_repository.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/repository/parse/parse_semester.dart';
import 'package:school_pen/domain/semester/repository/parse/parse_semester_repository.dart';
import 'package:school_pen/domain/sync/model/user_data.dart';
import 'package:school_pen/domain/sync/model/user_data_diff.dart';
import 'package:school_pen/domain/sync/repository/sync_repository.dart';
import 'package:school_pen/domain/sync/sync_listener.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/repository/parse/parse_teacher.dart';
import 'package:school_pen/domain/teacher/repository/parse/parse_teacher_repository.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/parse/parse_custom_timetable_event.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/parse/parse_custom_timetable_event_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/parse/parse_custom_timetable.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/parse/parse_custom_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/public/parse/parse_public_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/public/public_timetable_repository_facade.dart';
import 'package:school_pen/domain/user/model/user_meta.dart';
import 'package:school_pen/domain/user/user_login_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';
import 'package:school_pen/domain/userconfig/model/user_config.dart';
import 'package:school_pen/domain/userconfig/repository/parse/parse_user_config.dart';
import 'package:school_pen/domain/userconfig/repository/parse/parse_user_config_repository.dart';
import 'package:synchronized/synchronized.dart';

import 'sync_manager.dart';

/// Manages synchronization of data with the cloud.
/// Opens live update channel and fetches periodically data over GraphQL.
class ParseSyncManager implements SyncManager, LifecycleListener, UserLoginListener {
  static const Logger _logger = Logger('ParseSyncManager');

  final Lock _lock = Lock();
  final LifecycleManager _lifecycle;
  final RemoteConfigManager _remoteConfig;
  final UserManager _userManager;
  final SchoolYearManager _schoolYearManager;
  final SyncRepository _syncRepository;
  final PublicTimetableRepositoryFacade _publicTimetableStorage;
  final ParsePublicTimetableRepository _publicTimetableRepository;
  final ParseUserConfigRepository _userConfigRepository;
  final ParseSchoolYearRepository _schoolYearRepository;
  final ParseSemesterRepository _semesterRepository;
  final ParseNoteRepository _noteRepository;
  final ParseTeacherRepository _teacherRepository;
  final ParseCourseRepository _courseRepository;
  final ParseGradeRepository _gradeRepository;
  final ParseGradeCategoryRepository _gradeCategoryRepository;
  final ParseCustomTimetableRepository _customTimetableRepository;
  final ParseCustomTimetableEventRepository _customTimetableEventRepository;
  final ParseExamRepository _examRepository;
  final ParseHomeworkRepository _homeworkRepository;
  final ParseReminderRepository _reminderRepository;

  final List<ParseLiveUpdateSubscriber> _subscribers = [];
  final Set<SyncListener> _listeners = {};
  LiveQuery _liveQuery;
  StreamSubscription _subscribeSubscription;
  StreamSubscription _fetchSubscription;

  ParseSyncManager(
    this._lifecycle,
    this._remoteConfig,
    this._userManager,
    this._schoolYearManager,
    this._syncRepository,
    this._publicTimetableStorage,
    this._publicTimetableRepository,
    this._userConfigRepository,
    this._schoolYearRepository,
    this._semesterRepository,
    this._noteRepository,
    this._teacherRepository,
    this._courseRepository,
    this._gradeRepository,
    this._gradeCategoryRepository,
    this._customTimetableRepository,
    this._customTimetableEventRepository,
    this._examRepository,
    this._homeworkRepository,
    this._reminderRepository,
  ) {
    _lifecycle.addLifecycleListener(this);
    _userManager.addUserLoginListener(this);

    // register live update subscribers
    _subscribers.add(_userConfigRepository);
    _subscribers.add(_schoolYearRepository);
    _subscribers.add(_semesterRepository);
    _subscribers.add(_noteRepository);
    _subscribers.add(_teacherRepository);
    _subscribers.add(_courseRepository);
    _subscribers.add(_gradeRepository);
    _subscribers.add(_gradeCategoryRepository);
    _subscribers.add(_customTimetableRepository);
    _subscribers.add(_customTimetableEventRepository);
    _subscribers.add(_examRepository);
    _subscribers.add(_homeworkRepository);
    _subscribers.add(_reminderRepository);
  }

  @override
  void addSyncListener(SyncListener listener) {
    _listeners.add(listener);

    if (_lifecycle.isResumed) {
      listener.onSyncResumed();
    }
  }

  @override
  void removeSyncListener(SyncListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<void> sync() async {
    final ParseUser user = await ParseUser.currentUser();
    if (user != null) {
      await _fetchUserDataAndSync(user);
    } else {
      await _startSyncForAnonymousUser();
    }
  }

  @override
  Future<void> onResume() => _try(() => _startSync());

  @override
  Future<void> onPause() => _try(() => _stopSync());

  @override
  Future<void> onUserSignedIn(UserMetadata user) => _try(() => _startSync());

  @override
  void onUserSignedOut(UserMetadata user) => _try(() => _stopSync());

  Future<void> _startSync() {
    return _lock.synchronized(() async {
      final ParseUser user = await ParseUser.currentUser();
      if (user != null) {
        await _startSyncForSignedInUser(user);
      } else {
        await _startSyncForAnonymousUser();
      }

      _dispatchSyncResumed();
    });
  }

  Future<void> _startSyncForSignedInUser(ParseUser user) async {
    if (_liveQuery != null) return;

    if (_remoteConfig.getBool(RemoteConfigFlag.liveQueryEnabled)) {
      // live query is optional
      await _try(() async {
        _liveQuery = LiveQuery();
        await _liveQuery.client.reconnect(userInitialized: true);
        await _observeLiveQueryAndSubscribeOnce(_liveQuery);
        _observeLiveQueryAndFetchDataWhenConnected(user, _liveQuery);
      });
    }

    await _fetchUserDataAndSync(user);
  }

  Future<void> _startSyncForAnonymousUser() async {
    final List<String> ids = await _fetchSubscribedTimetableIds();
    if (ids.isEmpty) return;

    final List<Timetable> timetables = await _publicTimetableRepository.getTimetablesOfIds(ids);
    await _publicTimetableStorage.clearAndStoreTimetables(timetables);
  }

  Future<void> _stopSync() {
    return _lock.synchronized(() async {
      if (_liveQuery != null) {
        await _subscribeSubscription?.cancel();
        await _fetchSubscription?.cancel();
        await _unsubscribeLiveQuery(_liveQuery);
        await _liveQuery.client.disconnect(userInitialized: true);

        _liveQuery = null;
        _subscribeSubscription = null;
        _fetchSubscription = null;
      }

      _dispatchSyncPaused();
    });
  }

  /// Returns true whether first attempt to subscribe succeeds, false otherwise.
  Future<bool> _observeLiveQueryAndSubscribeOnce(LiveQuery liveQuery) {
    final Lock lock = Lock();
    Completer<bool> completer = Completer();
    bool isSubscribed = false;

    _subscribeSubscription = _liveQuery.client.getClientEventStream.listen((LiveQueryClientEvent event) {
      return lock.synchronized(() async {
        if (event == LiveQueryClientEvent.CONNECTED) {
          try {
            if (!isSubscribed) {
              await _subscribeLiveQuery(liveQuery);
              isSubscribed = true;
            }
            completer?.complete(true);
            completer = null;
          } catch (error, stackTrace) {
            _logger.logError(error, stackTrace);
            completer?.complete(false);
            completer = null;
          }
        } else if (event == LiveQueryClientEvent.DISCONNECTED || event == LiveQueryClientEvent.USER_DISCONNECTED) {
          completer?.complete(false);
          completer = null;
        }
      });
    });

    return completer.future;
  }

  void _observeLiveQueryAndFetchDataWhenConnected(ParseUser user, LiveQuery liveQuery) {
    _fetchSubscription = _liveQuery.client.getClientEventStream.listen((LiveQueryClientEvent event) {
      if (event == LiveQueryClientEvent.CONNECTED) {
        _try(() => _fetchUserDataAndSync(user));
      }
    });
  }

  Future<void> _subscribeLiveQuery(LiveQuery liveQuery) async {
    final List<Future> subscriptions = [];
    for (ParseLiveUpdateSubscriber subscriber in _subscribers) {
      subscriptions.add(subscriber.subscribeLiveUpdates(_liveQuery));
    }
    await Future.wait(subscriptions);
  }

  Future<void> _unsubscribeLiveQuery(LiveQuery liveQuery) async {
    final List<Future> subscriptions = [];
    for (ParseLiveUpdateSubscriber subscriber in _subscribers) {
      subscriptions.add(subscriber.unsubscribeLiveUpdates(_liveQuery));
    }
    await Future.wait(subscriptions);
  }

  // TODO: consider to aggregate data

  /// Fetches user data and dispatches it to repositories
  ///   1. create new objects
  ///   2. set new references
  ///   3. drop old objects
  Future<void> _fetchUserDataAndSync(ParseUser user) async {
    // download full state from remote
    final List<String> timetableIds = await _fetchSubscribedTimetableIds();
    final UserData remoteUserData = await _syncRepository.getUserData(timetableIds);
    final UserData localUserData = await _getLocalUserData();
    final UserDataDiff diff = UserDataDiff.from(remote: remoteUserData, local: localUserData);

    await _updateObjects(diff, user);
    await _updateConfig(diff, user);
    await _deleteObjects(diff, user);
  }

  Future<List<String>> _fetchSubscribedTimetableIds() async {
    final SchoolYear year = await _schoolYearManager.getActiveOrNull();
    if (year == null) return [];

    final List<String> ids = await _userConfigRepository.getSubscribedTimetableIdsPerYear(year.id);
    return ids ?? [];
  }

  Future<void> _updateObjects(UserDataDiff diff, ParseUser user) async {
    for (var year in diff.schoolYears.createdOnLocal) await ParseSchoolYear.fromModel(year, user).store();
    for (var year in diff.schoolYears.updatedOnLocal) await ParseSchoolYear.fromModel(year, user).store();
    for (var year in diff.schoolYears.createdOnRemote) await _schoolYearRepository.store(year);
    for (var year in diff.schoolYears.updatedOnRemote) await _schoolYearRepository.store(year);

    for (var semester in diff.semesters.createdOnLocal) await ParseSemester.fromModel(semester, user).store();
    for (var semester in diff.semesters.updatedOnLocal) await ParseSemester.fromModel(semester, user).store();
    for (var semester in diff.semesters.createdOnRemote) await _semesterRepository.store(semester);
    for (var semester in diff.semesters.updatedOnRemote) await _semesterRepository.store(semester);

    for (var obj in diff.notes.createdOnLocal) await ParseNote.fromModel(obj, user).store();
    for (var obj in diff.notes.updatedOnLocal) await ParseNote.fromModel(obj, user).store();
    for (var obj in diff.notes.createdOnRemote) await _noteRepository.store(obj);
    for (var obj in diff.notes.updatedOnRemote) await _noteRepository.store(obj);

    for (var obj in diff.teachers.createdOnLocal) await ParseTeacher.fromModel(obj, user).store();
    for (var obj in diff.teachers.updatedOnLocal) await ParseTeacher.fromModel(obj, user).store();
    for (var obj in diff.teachers.createdOnRemote) await _teacherRepository.store(obj);
    for (var obj in diff.teachers.updatedOnRemote) await _teacherRepository.store(obj);

    for (var obj in diff.courses.createdOnLocal) await ParseCourse.fromModel(obj, user).store();
    for (var obj in diff.courses.updatedOnLocal) await ParseCourse.fromModel(obj, user).store();
    for (var obj in diff.courses.createdOnRemote) await _courseRepository.store(obj);
    for (var obj in diff.courses.updatedOnRemote) await _courseRepository.store(obj);

    for (var obj in diff.gradeCategories.createdOnLocal) await ParseGradeCategory.fromModel(obj, user).store();
    for (var obj in diff.gradeCategories.updatedOnLocal) await ParseGradeCategory.fromModel(obj, user).store();
    for (var obj in diff.gradeCategories.createdOnRemote) await _gradeCategoryRepository.store(obj);
    for (var obj in diff.gradeCategories.updatedOnRemote) await _gradeCategoryRepository.store(obj);

    for (var obj in diff.grades.createdOnLocal) await ParseGrade.fromModel(obj, user).store();
    for (var obj in diff.grades.updatedOnLocal) await ParseGrade.fromModel(obj, user).store();
    for (var obj in diff.grades.createdOnRemote) await _gradeRepository.store(obj);
    for (var obj in diff.grades.updatedOnRemote) await _gradeRepository.store(obj);

    for (var obj in diff.customTimetables.createdOnLocal) await ParseCustomTimetable.fromModel(obj, user).store();
    for (var obj in diff.customTimetables.updatedOnLocal) await ParseCustomTimetable.fromModel(obj, user).store();
    for (var obj in diff.customTimetables.createdOnRemote) await _customTimetableRepository.store(obj);
    for (var obj in diff.customTimetables.updatedOnRemote) await _customTimetableRepository.store(obj);

    for (var obj in diff.timetableEvents.createdOnLocal) await ParseCustomTimetableEvent.fromModel(obj, user).store();
    for (var obj in diff.timetableEvents.updatedOnLocal) await ParseCustomTimetableEvent.fromModel(obj, user).store();
    for (var obj in diff.timetableEvents.createdOnRemote) await _customTimetableEventRepository.store(obj);
    for (var obj in diff.timetableEvents.updatedOnRemote) await _customTimetableEventRepository.store(obj);

    for (var obj in diff.exams.createdOnLocal) await ParseExam.fromModel(obj, user).store();
    for (var obj in diff.exams.updatedOnLocal) await ParseExam.fromModel(obj, user).store();
    for (var obj in diff.exams.createdOnRemote) await _examRepository.store(obj);
    for (var obj in diff.exams.updatedOnRemote) await _examRepository.store(obj);

    for (var obj in diff.homework.createdOnLocal) await ParseHomework.fromModel(obj, user).store();
    for (var obj in diff.homework.updatedOnLocal) await ParseHomework.fromModel(obj, user).store();
    for (var obj in diff.homework.createdOnRemote) await _homeworkRepository.store(obj);
    for (var obj in diff.homework.updatedOnRemote) await _homeworkRepository.store(obj);

    for (var obj in diff.reminders.createdOnLocal) await ParseReminder.fromModel(obj, user).store();
    for (var obj in diff.reminders.updatedOnLocal) await ParseReminder.fromModel(obj, user).store();
    for (var obj in diff.reminders.createdOnRemote) await _reminderRepository.store(obj);
    for (var obj in diff.reminders.updatedOnRemote) await _reminderRepository.store(obj);

    await _publicTimetableStorage.clearAndStoreTimetables([
      ...diff.publicTimetables.createdOnRemote,
      ...diff.publicTimetables.updatedOnRemote,
    ]);
  }

  Future<void> _updateConfig(UserDataDiff diff, ParseUser user) async {
    if (diff.userConfig.updatedOnLocal != null) {
      await ParseUserConfig.fromModel(diff.userConfig.updatedOnLocal, user).store();
    }

    if (diff.userConfig.updatedOnRemote != null) {
      await _userConfigRepository.store(diff.userConfig.updatedOnRemote);
    }
  }

  Future<void> _deleteObjects(UserDataDiff diff, ParseUser user) async {
    for (var obj in diff.reminders.deletedOnLocal) await ParseReminder().deleteByUuid(obj.id);
    for (var obj in diff.reminders.deletedOnRemote) await _reminderRepository.delete(obj.id);

    for (var obj in diff.homework.deletedOnLocal) await ParseHomework().deleteByUuid(obj.id);
    for (var obj in diff.homework.deletedOnRemote) await _homeworkRepository.drop(obj.id);

    for (var obj in diff.exams.deletedOnLocal) await ParseExam().deleteByUuid(obj.id);
    for (var obj in diff.exams.deletedOnRemote) await _examRepository.drop(obj.id);

    for (var obj in diff.timetableEvents.deletedOnLocal) await ParseCustomTimetableEvent().deleteByUuid(obj.id);
    for (var obj in diff.timetableEvents.deletedOnRemote) await _customTimetableEventRepository.drop(obj.id);

    for (var obj in diff.customTimetables.deletedOnLocal) await ParseCustomTimetable().deleteByUuid(obj.id);
    for (var obj in diff.customTimetables.deletedOnRemote) await _customTimetableRepository.drop(obj.id);

    for (var obj in diff.grades.deletedOnLocal) await ParseGrade().deleteByUuid(obj.id);
    for (var obj in diff.grades.deletedOnRemote) await _gradeRepository.drop(obj.id);

    for (var obj in diff.gradeCategories.deletedOnLocal) await ParseGradeCategory().deleteByUuid(obj.id);
    for (var obj in diff.gradeCategories.deletedOnRemote) await _gradeRepository.drop(obj.id);

    for (var obj in diff.courses.deletedOnLocal) await ParseCourse().deleteByUuid(obj.id);
    for (var obj in diff.courses.deletedOnRemote) await _courseRepository.drop(obj.id);

    for (var obj in diff.teachers.deletedOnLocal) await ParseTeacher().deleteByUuid(obj.id);
    for (var obj in diff.teachers.deletedOnRemote) await _teacherRepository.drop(obj.id);

    for (var obj in diff.notes.deletedOnLocal) await ParseNote().deleteByUuid(obj.id);
    for (var obj in diff.notes.deletedOnRemote) await _noteRepository.drop(obj.id);

    for (var obj in diff.semesters.deletedOnLocal) await ParseSemester().deleteByUuid(obj.id);
    for (var obj in diff.semesters.deletedOnRemote) await _semesterRepository.drop(obj.id);

    for (var obj in diff.schoolYears.deletedOnLocal) await ParseSchoolYear().deleteByUuid(obj.id);
    for (var obj in diff.schoolYears.deletedOnRemote) await _schoolYearRepository.drop(obj.id);
  }

  Future<UserData> _getLocalUserData() async {
    final Future<UserConfig> userConfig = _userConfigRepository.getUserConfig();
    final Future<List<SchoolYear>> schoolYears = _schoolYearRepository.getAll();
    final Future<List<String>> deletedSchoolYears = _schoolYearRepository.getDeletedIds();
    final Future<List<Semester>> semesters = _semesterRepository.getAll();
    final Future<List<String>> deletedSemesters = _semesterRepository.getDeletedIds();
    final Future<List<Note>> notes = _noteRepository.getAll();
    final Future<List<String>> deletedNotes = _noteRepository.getDeletedIds();
    final Future<List<Teacher>> teachers = _teacherRepository.getAll();
    final Future<List<String>> deletedTeachers = _teacherRepository.getDeletedIds();
    final Future<List<Course>> courses = _courseRepository.getAll();
    final Future<List<String>> deletedCourses = _courseRepository.getDeletedIds();
    final Future<List<Grade>> grades = _gradeRepository.getAll();
    final Future<List<String>> deletedGrades = _gradeRepository.getDeletedIds();
    final Future<List<GradeCategory>> gradeCategories = _gradeCategoryRepository.getAll();
    final Future<List<String>> deletedGradeCategories = _gradeCategoryRepository.getDeletedIds();
    final Future<List<TimetableInfo>> customTimetables = _customTimetableRepository.getAll();
    final Future<List<String>> deletedCustomTimetables = _customTimetableRepository.getDeletedIds();
    final Future<List<TimetableEvent>> customTimetableEvents = _customTimetableEventRepository.getAll();
    final Future<List<String>> deletedCustomTimetableEvents = _customTimetableEventRepository.getDeletedIds();
    final Future<List<Exam>> exams = _examRepository.getAll();
    final Future<List<String>> deletedExams = _examRepository.getDeletedIds();
    final Future<List<Homework>> homework = _homeworkRepository.getAll();
    final Future<List<String>> deletedHomework = _homeworkRepository.getDeletedIds();
    final Future<List<Reminder>> reminders = _reminderRepository.getAll();
    final Future<List<String>> deletedReminders = _reminderRepository.getDeletedIds();

    return UserData(
      userConfig: await userConfig,
      schoolYears: await schoolYears,
      deletedSchoolYears: await deletedSchoolYears,
      semesters: await semesters,
      deletedSemesters: await deletedSemesters,
      notes: await notes,
      deletedNotes: await deletedNotes,
      teachers: await teachers,
      deletedTeachers: await deletedTeachers,
      courses: await courses,
      deletedCourses: await deletedCourses,
      grades: await grades,
      deletedGrades: await deletedGrades,
      gradeCategories: await gradeCategories,
      deletedGradeCategories: await deletedGradeCategories,
      publicTimetables: [],
      customTimetables: await customTimetables,
      deletedCustomTimetables: await deletedCustomTimetables,
      customTimetableEvents: await customTimetableEvents,
      deletedCustomTimetableEvents: await deletedCustomTimetableEvents,
      exams: await exams,
      deletedExams: await deletedExams,
      homework: await homework,
      deletedHomework: await deletedHomework,
      reminders: await reminders,
      deletedReminders: await deletedReminders,
    );
  }

  void _dispatchSyncResumed() {
    for (SyncListener listener in _listeners) {
      listener.onSyncResumed();
    }
  }

  void _dispatchSyncPaused() {
    for (SyncListener listener in _listeners) {
      listener.onSyncPaused();
    }
  }

  Future<void> _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}

abstract class ParseLiveUpdateSubscriber {
  /// Called whenever subscribers are asked to subscribe to live updates of a given [liveQuery].
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery);

  /// Called whenever subscribers are asked to unsubscribe from live updates of a given [liveQuery].
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery);
}
