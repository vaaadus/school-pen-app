import 'package:school_pen/domain/sync/sync_listener.dart';

/// Periodically synchronizes data.
abstract class SyncManager {
  void addSyncListener(SyncListener listener);

  void removeSyncListener(SyncListener listener);

  /// Syncs data without subscribing for live updates. No-op if subscribed.
  Future<void> sync();
}
