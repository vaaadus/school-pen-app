import 'dart:convert';

import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/common/http.dart';
import 'package:school_pen/domain/common/parse/parse_sdk_metadata.dart';
import 'package:school_pen/domain/common/result.dart';
import 'package:school_pen/domain/network/network_manager.dart';
import 'package:school_pen/domain/sync/model/user_data.dart';
import 'package:school_pen/domain/sync/repository/json/json_user_data.dart';
import 'package:school_pen/domain/sync/repository/sync_repository.dart';
import 'package:school_pen/domain/user/exception/unauthorized_exception.dart';

class ParseSyncRepository implements SyncRepository {
  final NetworkManager _networkManager;

  ParseSyncRepository(this._networkManager);

  @override
  Future<UserData> getUserData(List<String> subscribedTimetables) async {
    try {
      return await _getUserData(subscribedTimetables);
    } catch (error) {
      if (await _networkManager.isNetworkAvailable()) rethrow;
      throw NoInternetException();
    }
  }

  Future<UserData> _getUserData(List<String> subscribedTimetables) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) throw UnauthorizedException('User is not signed in');

    final Result<UserData> result = await httpPostAndParse(
      ParseSdkMetadata.graphQLUrl,
      headers: {
        ...ParseSdkMetadata.headers(sessionToken: user.sessionToken),
        'Content-Type': 'application/json',
      },
      body: _buildQuery(subscribedTimetables),
      parser: _parseUserData,
    );

    return Result.unwrapResult(result);
  }

  String _buildQuery(List<String> subscribedTimetables) {
    return _queryGetUserData.replaceAll(RegExp(_publicTimetablesVar), _serializeStringList(subscribedTimetables));
  }

  String _serializeStringList(List<String> strings) {
    return '[' + strings.map(_wrapWithQuotes).join(',') + ']';
  }

  String _wrapWithQuotes(String string) {
    return '\\"' + string + '\\"';
  }
}

UserData _parseUserData(String string) {
  return JsonUserData.fromJson(json.decode(string)).toModel();
}

const String _publicTimetablesVar = '{{PUBLIC_TIMETABLES}}';

final String _queryGetUserData = '''
{"query":"query GetUserData {
    userConfigs {
      edges {
        node {
          updatedAt,
          schoolYearUuid,
          semesterUuidPerSchoolYear,
          autoSemesterChange,
          analyticsEnabled,
          diagnosisEnabled,
          personalizedAdsEnabled,
          gradingSystem,
          timetableUuidPerSchoolYear,
          subscribedTimetableUuidsPerSchoolYear,
          agendaOptions,
          timetableOptions
        }
      }
    }
    schoolYears(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          name,
        }
      }
    }
    schoolYear_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    semesters(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          schoolYearUuid,
          name,
          startDate,
          endDate,
        }
      }
    }
    semester_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    notes(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          schoolYearUuid,
          title,
          note,
          items {
            ... on Element {
              value,
            },
          },
          color,
          notificationDate,
          isArchived,
        }
      }
    }
    note_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    teachers(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          schoolYearUuid,
          name,
          phone,
          email,
          website,
          address,
          officeHours,
        }
      }
    }
    teacher_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    courses(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          schoolYearUuid,
          name,
          color,
          room,
          teachersUuids {
            ... on Element {
              value,
            }
          },
          creditHours,
          note,
        }
      }
    }
    course_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    grades(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          semesterUuid,
          courseUuid,
          name,
          value,
          weight,
          categoryUuid,
          dateTime,
        }
      }
    }
    grade_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    gradeCategories(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          customName,
        }
      }
    }
    gradeCategory_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    publicTimetables(where: {objectId: {in: $_publicTimetablesVar}}, first: 10000) {
      edges {
        node {
          objectId,
          updatedAt,
          name, 
          schedule,
          type,
        }
      }
    }
    customTimetables(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          schoolYearUuid,
          name,
          timetableType,
        }
      }
    }
    customTimetable_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    customTimetableEvents(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          timetableUuid,
          courseUuid,
          customRoom,
          customTeacher,
          customCourse,
          customStudent,
          note,
          repeatOptions,
        }
      }
    }
    customTimetableEvent_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    exams(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          semesterUuid,
          courseUuid,
          gradeUuid,
          title,
          note,
          dateTime,
          notification,
          isArchived,
        }
      }
    }
    exam_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    assignments(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          semesterUuid,
          courseUuid,
          gradeUuid,
          title,
          note,
          dateTime,
          notification,
          isArchived,
        }
      }
    }
    assignment_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
    reminders(first: 10000) {
      edges {
        node {
          updatedAt,
          uuid,
          semesterUuid,
          courseUuid,
          title,
          note,
          dateTime,
          isArchived,
        }
      }
    }
    reminder_Bins(first: 10000) {
      edges {
        node {
          uuid,
        }
      }
    }
  }"
}
'''
    .replaceAll(RegExp(r'\n'), '');
