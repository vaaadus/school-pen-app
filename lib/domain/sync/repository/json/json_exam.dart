import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/exam/model/exam.dart';

part 'json_exam.g.dart';

@JsonSerializable()
class JsonExams {
  final List<JsonExamEdge> edges;

  const JsonExams({@required this.edges});

  factory JsonExams.fromJson(Map<String, dynamic> json) => _$JsonExamsFromJson(json);

  Map<String, dynamic> toJson() => _$JsonExamsToJson(this);

  List<Exam> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonExamEdge {
  final JsonExam node;

  const JsonExamEdge({@required this.node});

  factory JsonExamEdge.fromJson(Map<String, dynamic> json) => _$JsonExamEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonExamEdgeToJson(this);

  Exam toModel() => node.toModel();
}

@JsonSerializable()
class JsonExam {
  final DateTime updatedAt;
  final String uuid;
  final String semesterUuid;
  final String courseUuid;
  final String gradeUuid;
  final String title;
  final String note;
  final DateTime dateTime;
  final Map<String, dynamic> notification;
  final bool isArchived;

  const JsonExam({
    @required this.updatedAt,
    @required this.uuid,
    @required this.semesterUuid,
    @required this.courseUuid,
    this.gradeUuid,
    this.title,
    this.note,
    @required this.dateTime,
    this.notification,
    @required this.isArchived,
  });

  factory JsonExam.fromJson(Map<String, dynamic> json) => _$JsonExamFromJson(json);

  Map<String, dynamic> toJson() => _$JsonExamToJson(this);

  Exam toModel() => Exam(
        id: uuid,
        semesterId: semesterUuid,
        courseId: courseUuid,
        gradeId: gradeUuid,
        title: title,
        note: note,
        dateTime: dateTime,
        notification: SembastSerializer.deserializeNotificationOptions(notification),
        isArchived: isArchived,
        updatedAt: updatedAt,
      );
}
