// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_bin.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonBins _$JsonBinsFromJson(Map<String, dynamic> json) {
  return JsonBins(
    edges: (json['edges'] as List)
        ?.map((e) =>
            e == null ? null : JsonBinEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonBinsToJson(JsonBins instance) => <String, dynamic>{
      'edges': instance.edges,
    };

JsonBinEdge _$JsonBinEdgeFromJson(Map<String, dynamic> json) {
  return JsonBinEdge(
    node: json['node'] == null
        ? null
        : JsonBin.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonBinEdgeToJson(JsonBinEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonBin _$JsonBinFromJson(Map<String, dynamic> json) {
  return JsonBin(
    uuid: json['uuid'] as String,
  );
}

Map<String, dynamic> _$JsonBinToJson(JsonBin instance) => <String, dynamic>{
      'uuid': instance.uuid,
    };
