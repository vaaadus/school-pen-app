import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/grade/model/grade.dart';

part 'json_grade.g.dart';

@JsonSerializable()
class JsonGrades {
  final List<JsonGradeEdge> edges;

  const JsonGrades({@required this.edges});

  factory JsonGrades.fromJson(Map<String, dynamic> json) => _$JsonGradesFromJson(json);

  Map<String, dynamic> toJson() => _$JsonGradesToJson(this);

  List<Grade> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonGradeEdge {
  final JsonGrade node;

  const JsonGradeEdge({@required this.node});

  factory JsonGradeEdge.fromJson(Map<String, dynamic> json) => _$JsonGradeEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonGradeEdgeToJson(this);

  Grade toModel() => node.toModel();
}

@JsonSerializable()
class JsonGrade {
  final DateTime updatedAt;
  final String uuid;
  final String semesterUuid;
  final String courseUuid;
  final String name;
  final double value;
  final double weight;
  final String categoryUuid;
  final DateTime dateTime;

  const JsonGrade({
    @required this.updatedAt,
    @required this.uuid,
    @required this.semesterUuid,
    @required this.courseUuid,
    this.name,
    @required this.value,
    @required this.weight,
    this.categoryUuid,
    @required this.dateTime,
  });

  factory JsonGrade.fromJson(Map<String, dynamic> json) => _$JsonGradeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonGradeToJson(this);

  Grade toModel() => Grade(
        id: uuid,
        semesterId: semesterUuid,
        courseId: courseUuid,
        name: name,
        value: value,
        weight: weight,
        categoryId: categoryUuid,
        dateTime: dateTime,
        updatedAt: updatedAt,
      );
}
