// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_public_timetable.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonPublicTimetables _$JsonPublicTimetablesFromJson(Map<String, dynamic> json) {
  return JsonPublicTimetables(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonPublicTimetableEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonPublicTimetablesToJson(
        JsonPublicTimetables instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonPublicTimetableEdge _$JsonPublicTimetableEdgeFromJson(
    Map<String, dynamic> json) {
  return JsonPublicTimetableEdge(
    node: json['node'] == null
        ? null
        : JsonPublicTimetable.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonPublicTimetableEdgeToJson(
        JsonPublicTimetableEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonPublicTimetable _$JsonPublicTimetableFromJson(Map<String, dynamic> json) {
  return JsonPublicTimetable(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    objectId: json['objectId'] as String,
    schoolYearUuid: json['schoolYearUuid'] as String,
    name: json['name'] as String,
    type: json['type'] as String,
    schedule: json['schedule'] as Map<String, dynamic>,
  );
}

Map<String, dynamic> _$JsonPublicTimetableToJson(
        JsonPublicTimetable instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'objectId': instance.objectId,
      'schoolYearUuid': instance.schoolYearUuid,
      'name': instance.name,
      'type': instance.type,
      'schedule': instance.schedule,
    };
