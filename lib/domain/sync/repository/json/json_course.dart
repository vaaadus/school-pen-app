import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/course/model/course.dart';

part 'json_course.g.dart';

@JsonSerializable()
class JsonCourses {
  final List<JsonCourseEdge> edges;

  const JsonCourses({@required this.edges});

  factory JsonCourses.fromJson(Map<String, dynamic> json) => _$JsonCoursesFromJson(json);

  Map<String, dynamic> toJson() => _$JsonCoursesToJson(this);

  List<Course> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonCourseEdge {
  final JsonCourse node;

  const JsonCourseEdge({@required this.node});

  factory JsonCourseEdge.fromJson(Map<String, dynamic> json) => _$JsonCourseEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonCourseEdgeToJson(this);

  Course toModel() => node.toModel();
}

@JsonSerializable()
class JsonCourse {
  final DateTime updatedAt;
  final String uuid;
  final String schoolYearUuid;
  final String name;
  final String color;
  final String room;
  final List<JsonTeacherId> teachersUuids;
  final double creditHours;
  final String note;

  const JsonCourse({
    @required this.updatedAt,
    @required this.uuid,
    @required this.schoolYearUuid,
    @required this.name,
    this.color,
    this.room,
    @required this.teachersUuids,
    this.creditHours,
    this.note,
  });

  factory JsonCourse.fromJson(Map<String, dynamic> json) => _$JsonCourseFromJson(json);

  Map<String, dynamic> toJson() => _$JsonCourseToJson(this);

  Course toModel() => Course(
        id: uuid,
        schoolYearId: schoolYearUuid,
        name: name,
        color: ColorEnumExt.forTag(color),
        room: room,
        teachersIds: teachersUuids.map((e) => e.toModel()).toList(),
        creditHours: creditHours,
        note: note,
        updatedAt: updatedAt,
      );
}

@JsonSerializable()
class JsonTeacherId {
  final String value;

  const JsonTeacherId({@required this.value});

  factory JsonTeacherId.fromJson(Map<String, dynamic> json) => _$JsonTeacherIdFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTeacherIdToJson(this);

  String toModel() => value;
}
