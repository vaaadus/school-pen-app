// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_timetable_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonTimetableEvents _$JsonTimetableEventsFromJson(Map<String, dynamic> json) {
  return JsonTimetableEvents(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonTimetableEventEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonTimetableEventsToJson(
        JsonTimetableEvents instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonTimetableEventEdge _$JsonTimetableEventEdgeFromJson(
    Map<String, dynamic> json) {
  return JsonTimetableEventEdge(
    node: json['node'] == null
        ? null
        : JsonTimetableEvent.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonTimetableEventEdgeToJson(
        JsonTimetableEventEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonTimetableEvent _$JsonTimetableEventFromJson(Map<String, dynamic> json) {
  return JsonTimetableEvent(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    timetableUuid: json['timetableUuid'] as String,
    courseUuid: json['courseUuid'] as String,
    customRoom: json['customRoom'] as String,
    customTeacher: json['customTeacher'] as String,
    customCourse: json['customCourse'] as String,
    customStudent: json['customStudent'] as String,
    note: json['note'] as String,
    repeatOptions: json['repeatOptions'] as Map<String, dynamic>,
  );
}

Map<String, dynamic> _$JsonTimetableEventToJson(JsonTimetableEvent instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'timetableUuid': instance.timetableUuid,
      'courseUuid': instance.courseUuid,
      'customRoom': instance.customRoom,
      'customTeacher': instance.customTeacher,
      'customCourse': instance.customCourse,
      'customStudent': instance.customStudent,
      'note': instance.note,
      'repeatOptions': instance.repeatOptions,
    };
