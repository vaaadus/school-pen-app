import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';

part 'json_school_year.g.dart';

@JsonSerializable()
class JsonSchoolYears {
  final List<JsonSchoolYearEdge> edges;

  const JsonSchoolYears({@required this.edges});

  factory JsonSchoolYears.fromJson(Map<String, dynamic> json) => _$JsonSchoolYearsFromJson(json);

  Map<String, dynamic> toJson() => _$JsonSchoolYearsToJson(this);

  List<SchoolYear> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonSchoolYearEdge {
  final JsonSchoolYear node;

  const JsonSchoolYearEdge({@required this.node});

  factory JsonSchoolYearEdge.fromJson(Map<String, dynamic> json) => _$JsonSchoolYearEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonSchoolYearEdgeToJson(this);

  SchoolYear toModel() => node.toModel();
}

@JsonSerializable()
class JsonSchoolYear {
  final DateTime updatedAt;
  final String uuid;
  final String name;

  const JsonSchoolYear({
    @required this.updatedAt,
    @required this.uuid,
    @required this.name,
  });

  factory JsonSchoolYear.fromJson(Map<String, dynamic> json) => _$JsonSchoolYearFromJson(json);

  Map<String, dynamic> toJson() => _$JsonSchoolYearToJson(this);

  SchoolYear toModel() => SchoolYear(
        id: uuid,
        name: name,
        updatedAt: updatedAt,
      );
}
