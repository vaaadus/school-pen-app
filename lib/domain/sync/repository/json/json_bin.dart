import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'json_bin.g.dart';

@JsonSerializable()
class JsonBins {
  final List<JsonBinEdge> edges;

  const JsonBins({@required this.edges});

  factory JsonBins.fromJson(Map<String, dynamic> json) => _$JsonBinsFromJson(json);

  Map<String, dynamic> toJson() => _$JsonBinsToJson(this);

  List<String> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonBinEdge {
  final JsonBin node;

  const JsonBinEdge({@required this.node});

  factory JsonBinEdge.fromJson(Map<String, dynamic> json) => _$JsonBinEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonBinEdgeToJson(this);

  String toModel() => node.toModel();
}

@JsonSerializable()
class JsonBin {
  final String uuid;

  const JsonBin({@required this.uuid});

  factory JsonBin.fromJson(Map<String, dynamic> json) => _$JsonBinFromJson(json);

  Map<String, dynamic> toJson() => _$JsonBinToJson(this);

  String toModel() => uuid;
}
