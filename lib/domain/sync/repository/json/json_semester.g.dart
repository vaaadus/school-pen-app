// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_semester.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonSemesters _$JsonSemestersFromJson(Map<String, dynamic> json) {
  return JsonSemesters(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonSemesterEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonSemestersToJson(JsonSemesters instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonSemesterEdge _$JsonSemesterEdgeFromJson(Map<String, dynamic> json) {
  return JsonSemesterEdge(
    node: json['node'] == null
        ? null
        : JsonSemester.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonSemesterEdgeToJson(JsonSemesterEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonSemester _$JsonSemesterFromJson(Map<String, dynamic> json) {
  return JsonSemester(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    schoolYearUuid: json['schoolYearUuid'] as String,
    name: json['name'] as String,
    startDate: json['startDate'] == null
        ? null
        : DateTime.parse(json['startDate'] as String),
    endDate: json['endDate'] == null
        ? null
        : DateTime.parse(json['endDate'] as String),
  );
}

Map<String, dynamic> _$JsonSemesterToJson(JsonSemester instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'schoolYearUuid': instance.schoolYearUuid,
      'name': instance.name,
      'startDate': instance.startDate?.toIso8601String(),
      'endDate': instance.endDate?.toIso8601String(),
    };
