// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_grade.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonGrades _$JsonGradesFromJson(Map<String, dynamic> json) {
  return JsonGrades(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonGradeEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonGradesToJson(JsonGrades instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonGradeEdge _$JsonGradeEdgeFromJson(Map<String, dynamic> json) {
  return JsonGradeEdge(
    node: json['node'] == null
        ? null
        : JsonGrade.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonGradeEdgeToJson(JsonGradeEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonGrade _$JsonGradeFromJson(Map<String, dynamic> json) {
  return JsonGrade(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    semesterUuid: json['semesterUuid'] as String,
    courseUuid: json['courseUuid'] as String,
    name: json['name'] as String,
    value: (json['value'] as num)?.toDouble(),
    weight: (json['weight'] as num)?.toDouble(),
    categoryUuid: json['categoryUuid'] as String,
    dateTime: json['dateTime'] == null
        ? null
        : DateTime.parse(json['dateTime'] as String),
  );
}

Map<String, dynamic> _$JsonGradeToJson(JsonGrade instance) => <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'semesterUuid': instance.semesterUuid,
      'courseUuid': instance.courseUuid,
      'name': instance.name,
      'value': instance.value,
      'weight': instance.weight,
      'categoryUuid': instance.categoryUuid,
      'dateTime': instance.dateTime?.toIso8601String(),
    };
