// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_note.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonNotes _$JsonNotesFromJson(Map<String, dynamic> json) {
  return JsonNotes(
    edges: (json['edges'] as List)
        ?.map((e) =>
            e == null ? null : JsonNoteEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonNotesToJson(JsonNotes instance) => <String, dynamic>{
      'edges': instance.edges,
    };

JsonNoteEdge _$JsonNoteEdgeFromJson(Map<String, dynamic> json) {
  return JsonNoteEdge(
    node: json['node'] == null
        ? null
        : JsonNote.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonNoteEdgeToJson(JsonNoteEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonNote _$JsonNoteFromJson(Map<String, dynamic> json) {
  return JsonNote(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    schoolYearUuid: json['schoolYearUuid'] as String,
    title: json['title'] as String,
    note: json['note'] as String,
    items: (json['items'] as List)
        ?.map((e) =>
            e == null ? null : JsonNoteItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    color: json['color'] as String,
    notificationDate: json['notificationDate'] == null
        ? null
        : DateTime.parse(json['notificationDate'] as String),
    isArchived: json['isArchived'] as bool,
  );
}

Map<String, dynamic> _$JsonNoteToJson(JsonNote instance) => <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'schoolYearUuid': instance.schoolYearUuid,
      'title': instance.title,
      'note': instance.note,
      'items': instance.items,
      'color': instance.color,
      'notificationDate': instance.notificationDate?.toIso8601String(),
      'isArchived': instance.isArchived,
    };

JsonNoteItem _$JsonNoteItemFromJson(Map<String, dynamic> json) {
  return JsonNoteItem(
    value: json['value'] == null
        ? null
        : JsonNoteItemValue.fromJson(json['value'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonNoteItemToJson(JsonNoteItem instance) =>
    <String, dynamic>{
      'value': instance.value,
    };

JsonNoteItemValue _$JsonNoteItemValueFromJson(Map<String, dynamic> json) {
  return JsonNoteItemValue(
    id: json['id'] as String,
    text: json['text'] as String,
    isCompleted: json['isCompleted'] as bool,
  );
}

Map<String, dynamic> _$JsonNoteItemValueToJson(JsonNoteItemValue instance) =>
    <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'isCompleted': instance.isCompleted,
    };
