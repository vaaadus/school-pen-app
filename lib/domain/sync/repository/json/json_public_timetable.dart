import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';
import 'package:school_pen/domain/timetable/repository/public/parse/json/json_timetable_schedule.dart';

part 'json_public_timetable.g.dart';

@JsonSerializable()
class JsonPublicTimetables {
  final List<JsonPublicTimetableEdge> edges;

  const JsonPublicTimetables({@required this.edges});

  factory JsonPublicTimetables.fromJson(Map<String, dynamic> json) => _$JsonPublicTimetablesFromJson(json);

  Map<String, dynamic> toJson() => _$JsonPublicTimetablesToJson(this);

  List<Timetable> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonPublicTimetableEdge {
  final JsonPublicTimetable node;

  const JsonPublicTimetableEdge({@required this.node});

  factory JsonPublicTimetableEdge.fromJson(Map<String, dynamic> json) => _$JsonPublicTimetableEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonPublicTimetableEdgeToJson(this);

  Timetable toModel() => node.toModel();
}

@JsonSerializable()
class JsonPublicTimetable {
  final DateTime updatedAt;
  final String objectId;
  final String schoolYearUuid;
  final String name;
  final String type;
  final Map<String, dynamic> schedule;

  const JsonPublicTimetable({
    @required this.updatedAt,
    @required this.objectId,
    @required this.schoolYearUuid,
    @required this.name,
    @required this.type,
    @required this.schedule,
  });

  factory JsonPublicTimetable.fromJson(Map<String, dynamic> json) => _$JsonPublicTimetableFromJson(json);

  Map<String, dynamic> toJson() => _$JsonPublicTimetableToJson(this);

  Timetable toModel() => Timetable(
        id: objectId,
        schoolYearId: schoolYearUuid,
        name: name,
        type: TimetableTypeExt.forTag(type),
        events: JsonTimetableSchedule.fromJson(schedule).toEvents(timetableId: objectId, updatedAt: updatedAt),
        updatedAt: updatedAt,
      );
}
