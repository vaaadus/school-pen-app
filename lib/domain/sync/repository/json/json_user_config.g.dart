// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_user_config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonUserConfigs _$JsonUserConfigsFromJson(Map<String, dynamic> json) {
  return JsonUserConfigs(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonUserConfigEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonUserConfigsToJson(JsonUserConfigs instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonUserConfigEdge _$JsonUserConfigEdgeFromJson(Map<String, dynamic> json) {
  return JsonUserConfigEdge(
    node: json['node'] == null
        ? null
        : JsonUserConfig.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonUserConfigEdgeToJson(JsonUserConfigEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonUserConfig _$JsonUserConfigFromJson(Map<String, dynamic> json) {
  return JsonUserConfig(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    schoolYearUuid: json['schoolYearUuid'] as String,
    semesterUuidPerSchoolYear:
        (json['semesterUuidPerSchoolYear'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    ),
    autoSemesterChange: json['autoSemesterChange'] as bool,
    analyticsEnabled: json['analyticsEnabled'] as bool,
    diagnosisEnabled: json['diagnosisEnabled'] as bool,
    personalizedAdsEnabled: json['personalizedAdsEnabled'] as bool,
    gradingSystem: json['gradingSystem'] == null
        ? null
        : JsonGradingSystem.fromJson(
            json['gradingSystem'] as Map<String, dynamic>),
    timetableUuidPerSchoolYear:
        (json['timetableUuidPerSchoolYear'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    ),
    subscribedTimetableUuidsPerSchoolYear:
        (json['subscribedTimetableUuidsPerSchoolYear'] as Map<String, dynamic>)
            ?.map(
      (k, e) => MapEntry(k, (e as List)?.map((e) => e as String)?.toList()),
    ),
    agendaOptions: json['agendaOptions'] as Map<String, dynamic>,
    timetableOptions: json['timetableOptions'] as Map<String, dynamic>,
  );
}

Map<String, dynamic> _$JsonUserConfigToJson(JsonUserConfig instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'schoolYearUuid': instance.schoolYearUuid,
      'semesterUuidPerSchoolYear': instance.semesterUuidPerSchoolYear,
      'autoSemesterChange': instance.autoSemesterChange,
      'analyticsEnabled': instance.analyticsEnabled,
      'diagnosisEnabled': instance.diagnosisEnabled,
      'personalizedAdsEnabled': instance.personalizedAdsEnabled,
      'gradingSystem': instance.gradingSystem,
      'timetableUuidPerSchoolYear': instance.timetableUuidPerSchoolYear,
      'subscribedTimetableUuidsPerSchoolYear':
          instance.subscribedTimetableUuidsPerSchoolYear,
      'agendaOptions': instance.agendaOptions,
      'timetableOptions': instance.timetableOptions,
    };

JsonGradingSystem _$JsonGradingSystemFromJson(Map<String, dynamic> json) {
  return JsonGradingSystem(
    type: json['type'] as String,
    minValue: (json['minValue'] as num)?.toDouble(),
    maxValue: (json['maxValue'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$JsonGradingSystemToJson(JsonGradingSystem instance) =>
    <String, dynamic>{
      'type': instance.type,
      'minValue': instance.minValue,
      'maxValue': instance.maxValue,
    };
