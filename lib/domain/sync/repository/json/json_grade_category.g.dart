// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_grade_category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonGradeCategories _$JsonGradeCategoriesFromJson(Map<String, dynamic> json) {
  return JsonGradeCategories(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonGradeCategoryEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonGradeCategoriesToJson(
        JsonGradeCategories instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonGradeCategoryEdge _$JsonGradeCategoryEdgeFromJson(
    Map<String, dynamic> json) {
  return JsonGradeCategoryEdge(
    node: json['node'] == null
        ? null
        : JsonGradeCategory.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonGradeCategoryEdgeToJson(
        JsonGradeCategoryEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonGradeCategory _$JsonGradeCategoryFromJson(Map<String, dynamic> json) {
  return JsonGradeCategory(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    customName: json['customName'] as String,
  );
}

Map<String, dynamic> _$JsonGradeCategoryToJson(JsonGradeCategory instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'customName': instance.customName,
    };
