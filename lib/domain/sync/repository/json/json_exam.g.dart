// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_exam.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonExams _$JsonExamsFromJson(Map<String, dynamic> json) {
  return JsonExams(
    edges: (json['edges'] as List)
        ?.map((e) =>
            e == null ? null : JsonExamEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonExamsToJson(JsonExams instance) => <String, dynamic>{
      'edges': instance.edges,
    };

JsonExamEdge _$JsonExamEdgeFromJson(Map<String, dynamic> json) {
  return JsonExamEdge(
    node: json['node'] == null
        ? null
        : JsonExam.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonExamEdgeToJson(JsonExamEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonExam _$JsonExamFromJson(Map<String, dynamic> json) {
  return JsonExam(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    semesterUuid: json['semesterUuid'] as String,
    courseUuid: json['courseUuid'] as String,
    gradeUuid: json['gradeUuid'] as String,
    title: json['title'] as String,
    note: json['note'] as String,
    dateTime: json['dateTime'] == null
        ? null
        : DateTime.parse(json['dateTime'] as String),
    notification: json['notification'] as Map<String, dynamic>,
    isArchived: json['isArchived'] as bool,
  );
}

Map<String, dynamic> _$JsonExamToJson(JsonExam instance) => <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'semesterUuid': instance.semesterUuid,
      'courseUuid': instance.courseUuid,
      'gradeUuid': instance.gradeUuid,
      'title': instance.title,
      'note': instance.note,
      'dateTime': instance.dateTime?.toIso8601String(),
      'notification': instance.notification,
      'isArchived': instance.isArchived,
    };
