import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/repository/common/timetable_serializer.dart';

part 'json_timetable_event.g.dart';

@JsonSerializable()
class JsonTimetableEvents {
  final List<JsonTimetableEventEdge> edges;

  const JsonTimetableEvents({@required this.edges});

  factory JsonTimetableEvents.fromJson(Map<String, dynamic> json) => _$JsonTimetableEventsFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTimetableEventsToJson(this);

  List<TimetableEvent> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonTimetableEventEdge {
  final JsonTimetableEvent node;

  const JsonTimetableEventEdge({@required this.node});

  factory JsonTimetableEventEdge.fromJson(Map<String, dynamic> json) => _$JsonTimetableEventEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTimetableEventEdgeToJson(this);

  TimetableEvent toModel() => node.toModel();
}

@JsonSerializable()
class JsonTimetableEvent {
  final DateTime updatedAt;
  final String uuid;
  final String timetableUuid;
  final String courseUuid;
  final String customRoom;
  final String customTeacher;
  final String customCourse;
  final String customStudent;
  final String note;
  final Map<String, dynamic> repeatOptions;

  const JsonTimetableEvent({
    @required this.updatedAt,
    @required this.uuid,
    @required this.timetableUuid,
    this.courseUuid,
    this.customRoom,
    this.customTeacher,
    this.customCourse,
    this.customStudent,
    this.note,
    @required this.repeatOptions,
  });

  factory JsonTimetableEvent.fromJson(Map<String, dynamic> json) => _$JsonTimetableEventFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTimetableEventToJson(this);

  TimetableEvent toModel() => TimetableEvent(
        id: uuid,
        timetableId: timetableUuid,
        courseId: courseUuid,
        customRoom: customRoom,
        customCourse: customCourse,
        customTeacher: customTeacher,
        customStudent: customStudent,
        repeatOptions: TimetableSerializer.deserializeEventRepeatOptions(repeatOptions),
        note: note,
        updatedAt: updatedAt,
      );
}
