// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_reminder.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonReminders _$JsonRemindersFromJson(Map<String, dynamic> json) {
  return JsonReminders(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonReminderEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonRemindersToJson(JsonReminders instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonReminderEdge _$JsonReminderEdgeFromJson(Map<String, dynamic> json) {
  return JsonReminderEdge(
    node: json['node'] == null
        ? null
        : JsonReminder.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonReminderEdgeToJson(JsonReminderEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonReminder _$JsonReminderFromJson(Map<String, dynamic> json) {
  return JsonReminder(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    semesterUuid: json['semesterUuid'] as String,
    courseUuid: json['courseUuid'] as String,
    title: json['title'] as String,
    note: json['note'] as String,
    dateTime: json['dateTime'] == null
        ? null
        : DateTime.parse(json['dateTime'] as String),
    isArchived: json['isArchived'] as bool,
  );
}

Map<String, dynamic> _$JsonReminderToJson(JsonReminder instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'semesterUuid': instance.semesterUuid,
      'courseUuid': instance.courseUuid,
      'title': instance.title,
      'note': instance.note,
      'dateTime': instance.dateTime?.toIso8601String(),
      'isArchived': instance.isArchived,
    };
