// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_course.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonCourses _$JsonCoursesFromJson(Map<String, dynamic> json) {
  return JsonCourses(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonCourseEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonCoursesToJson(JsonCourses instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonCourseEdge _$JsonCourseEdgeFromJson(Map<String, dynamic> json) {
  return JsonCourseEdge(
    node: json['node'] == null
        ? null
        : JsonCourse.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonCourseEdgeToJson(JsonCourseEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonCourse _$JsonCourseFromJson(Map<String, dynamic> json) {
  return JsonCourse(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    schoolYearUuid: json['schoolYearUuid'] as String,
    name: json['name'] as String,
    color: json['color'] as String,
    room: json['room'] as String,
    teachersUuids: (json['teachersUuids'] as List)
        ?.map((e) => e == null
            ? null
            : JsonTeacherId.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    creditHours: (json['creditHours'] as num)?.toDouble(),
    note: json['note'] as String,
  );
}

Map<String, dynamic> _$JsonCourseToJson(JsonCourse instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'schoolYearUuid': instance.schoolYearUuid,
      'name': instance.name,
      'color': instance.color,
      'room': instance.room,
      'teachersUuids': instance.teachersUuids,
      'creditHours': instance.creditHours,
      'note': instance.note,
    };

JsonTeacherId _$JsonTeacherIdFromJson(Map<String, dynamic> json) {
  return JsonTeacherId(
    value: json['value'] as String,
  );
}

Map<String, dynamic> _$JsonTeacherIdToJson(JsonTeacherId instance) =>
    <String, dynamic>{
      'value': instance.value,
    };
