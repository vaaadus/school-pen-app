import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';

part 'json_reminder.g.dart';

@JsonSerializable()
class JsonReminders {
  final List<JsonReminderEdge> edges;

  const JsonReminders({@required this.edges});

  factory JsonReminders.fromJson(Map<String, dynamic> json) => _$JsonRemindersFromJson(json);

  Map<String, dynamic> toJson() => _$JsonRemindersToJson(this);

  List<Reminder> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonReminderEdge {
  final JsonReminder node;

  const JsonReminderEdge({@required this.node});

  factory JsonReminderEdge.fromJson(Map<String, dynamic> json) => _$JsonReminderEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonReminderEdgeToJson(this);

  Reminder toModel() => node.toModel();
}

@JsonSerializable()
class JsonReminder {
  final DateTime updatedAt;
  final String uuid;
  final String semesterUuid;
  final String courseUuid;
  final String title;
  final String note;
  final DateTime dateTime;
  final bool isArchived;

  const JsonReminder({
    @required this.updatedAt,
    @required this.uuid,
    @required this.semesterUuid,
    this.courseUuid,
    this.title,
    this.note,
    @required this.dateTime,
    @required this.isArchived,
  });

  factory JsonReminder.fromJson(Map<String, dynamic> json) => _$JsonReminderFromJson(json);

  Map<String, dynamic> toJson() => _$JsonReminderToJson(this);

  Reminder toModel() => Reminder(
        id: uuid,
        semesterId: semesterUuid,
        courseId: courseUuid,
        title: title,
        note: note,
        dateTime: dateTime,
        isArchived: isArchived,
        updatedAt: updatedAt,
      );
}
