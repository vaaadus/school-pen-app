// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_homework.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonHomeworks _$JsonHomeworksFromJson(Map<String, dynamic> json) {
  return JsonHomeworks(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonHomeworkEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonHomeworksToJson(JsonHomeworks instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonHomeworkEdge _$JsonHomeworkEdgeFromJson(Map<String, dynamic> json) {
  return JsonHomeworkEdge(
    node: json['node'] == null
        ? null
        : JsonHomework.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonHomeworkEdgeToJson(JsonHomeworkEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonHomework _$JsonHomeworkFromJson(Map<String, dynamic> json) {
  return JsonHomework(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    semesterUuid: json['semesterUuid'] as String,
    courseUuid: json['courseUuid'] as String,
    gradeUuid: json['gradeUuid'] as String,
    title: json['title'] as String,
    note: json['note'] as String,
    dateTime: json['dateTime'] == null
        ? null
        : DateTime.parse(json['dateTime'] as String),
    notification: json['notification'] as Map<String, dynamic>,
    isArchived: json['isArchived'] as bool,
  );
}

Map<String, dynamic> _$JsonHomeworkToJson(JsonHomework instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'semesterUuid': instance.semesterUuid,
      'courseUuid': instance.courseUuid,
      'gradeUuid': instance.gradeUuid,
      'title': instance.title,
      'note': instance.note,
      'dateTime': instance.dateTime?.toIso8601String(),
      'notification': instance.notification,
      'isArchived': instance.isArchived,
    };
