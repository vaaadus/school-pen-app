// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_timetable_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonTimetablesInfo _$JsonTimetablesInfoFromJson(Map<String, dynamic> json) {
  return JsonTimetablesInfo(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonTimetableInfoEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonTimetablesInfoToJson(JsonTimetablesInfo instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonTimetableInfoEdge _$JsonTimetableInfoEdgeFromJson(
    Map<String, dynamic> json) {
  return JsonTimetableInfoEdge(
    node: json['node'] == null
        ? null
        : JsonTimetableInfo.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonTimetableInfoEdgeToJson(
        JsonTimetableInfoEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonTimetableInfo _$JsonTimetableInfoFromJson(Map<String, dynamic> json) {
  return JsonTimetableInfo(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    schoolYearUuid: json['schoolYearUuid'] as String,
    name: json['name'] as String,
    timetableType: json['timetableType'] as String,
  );
}

Map<String, dynamic> _$JsonTimetableInfoToJson(JsonTimetableInfo instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'schoolYearUuid': instance.schoolYearUuid,
      'name': instance.name,
      'timetableType': instance.timetableType,
    };
