import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/homework/model/homework.dart';

part 'json_homework.g.dart';

@JsonSerializable()
class JsonHomeworks {
  final List<JsonHomeworkEdge> edges;

  const JsonHomeworks({@required this.edges});

  factory JsonHomeworks.fromJson(Map<String, dynamic> json) => _$JsonHomeworksFromJson(json);

  Map<String, dynamic> toJson() => _$JsonHomeworksToJson(this);

  List<Homework> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonHomeworkEdge {
  final JsonHomework node;

  const JsonHomeworkEdge({@required this.node});

  factory JsonHomeworkEdge.fromJson(Map<String, dynamic> json) => _$JsonHomeworkEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonHomeworkEdgeToJson(this);

  Homework toModel() => node.toModel();
}

@JsonSerializable()
class JsonHomework {
  final DateTime updatedAt;
  final String uuid;
  final String semesterUuid;
  final String courseUuid;
  final String gradeUuid;
  final String title;
  final String note;
  final DateTime dateTime;
  final Map<String, dynamic> notification;
  final bool isArchived;

  const JsonHomework({
    @required this.updatedAt,
    @required this.uuid,
    @required this.semesterUuid,
    @required this.courseUuid,
    this.gradeUuid,
    this.title,
    this.note,
    @required this.dateTime,
    this.notification,
    @required this.isArchived,
  });

  factory JsonHomework.fromJson(Map<String, dynamic> json) => _$JsonHomeworkFromJson(json);

  Map<String, dynamic> toJson() => _$JsonHomeworkToJson(this);

  Homework toModel() => Homework(
        id: uuid,
        semesterId: semesterUuid,
        courseId: courseUuid,
        gradeId: gradeUuid,
        title: title,
        note: note,
        dateTime: dateTime,
        notification: SembastSerializer.deserializeNotificationOptions(notification),
        isArchived: isArchived,
        updatedAt: updatedAt,
      );
}
