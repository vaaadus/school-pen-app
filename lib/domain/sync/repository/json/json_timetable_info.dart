import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';

part 'json_timetable_info.g.dart';

@JsonSerializable()
class JsonTimetablesInfo {
  final List<JsonTimetableInfoEdge> edges;

  const JsonTimetablesInfo({@required this.edges});

  factory JsonTimetablesInfo.fromJson(Map<String, dynamic> json) => _$JsonTimetablesInfoFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTimetablesInfoToJson(this);

  List<TimetableInfo> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonTimetableInfoEdge {
  final JsonTimetableInfo node;

  const JsonTimetableInfoEdge({@required this.node});

  factory JsonTimetableInfoEdge.fromJson(Map<String, dynamic> json) => _$JsonTimetableInfoEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTimetableInfoEdgeToJson(this);

  TimetableInfo toModel() => node.toModel();
}

@JsonSerializable()
class JsonTimetableInfo {
  final DateTime updatedAt;
  final String uuid;
  final String schoolYearUuid;
  final String name;
  final String timetableType;

  const JsonTimetableInfo({
    @required this.updatedAt,
    @required this.uuid,
    @required this.schoolYearUuid,
    @required this.name,
    @required this.timetableType,
  });

  factory JsonTimetableInfo.fromJson(Map<String, dynamic> json) => _$JsonTimetableInfoFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTimetableInfoToJson(this);

  TimetableInfo toModel() => TimetableInfo(
        id: uuid,
        schoolYearId: schoolYearUuid,
        name: name,
        type: TimetableTypeExt.forTag(timetableType),
        updatedAt: updatedAt,
      );
}
