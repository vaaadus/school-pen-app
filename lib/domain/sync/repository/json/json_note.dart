import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/model/note_item.dart';

part 'json_note.g.dart';

@JsonSerializable()
class JsonNotes {
  final List<JsonNoteEdge> edges;

  const JsonNotes({@required this.edges});

  factory JsonNotes.fromJson(Map<String, dynamic> json) => _$JsonNotesFromJson(json);

  Map<String, dynamic> toJson() => _$JsonNotesToJson(this);

  List<Note> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonNoteEdge {
  final JsonNote node;

  const JsonNoteEdge({@required this.node});

  factory JsonNoteEdge.fromJson(Map<String, dynamic> json) => _$JsonNoteEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonNoteEdgeToJson(this);

  Note toModel() => node.toModel();
}

@JsonSerializable()
class JsonNote {
  final DateTime updatedAt;
  final String uuid;
  final String schoolYearUuid;
  final String title;
  final String note;
  final List<JsonNoteItem> items;
  final String color;
  final DateTime notificationDate;
  final bool isArchived;

  const JsonNote({
    @required this.updatedAt,
    @required this.uuid,
    @required this.schoolYearUuid,
    this.title,
    this.note,
    @required this.items,
    this.color,
    this.notificationDate,
    @required this.isArchived,
  });

  factory JsonNote.fromJson(Map<String, dynamic> json) => _$JsonNoteFromJson(json);

  Map<String, dynamic> toJson() => _$JsonNoteToJson(this);

  Note toModel() => Note(
        id: uuid,
        schoolYearId: schoolYearUuid,
        title: title,
        note: note,
        items: items.map((e) => e.toModel()).toList(),
        color: color != null ? ColorEnumExt.forTag(color) : null,
        notificationDate: notificationDate,
        isArchived: isArchived,
        updatedAt: updatedAt,
      );
}

@JsonSerializable()
class JsonNoteItem {
  final JsonNoteItemValue value;

  const JsonNoteItem({@required this.value});

  factory JsonNoteItem.fromJson(Map<String, dynamic> json) => _$JsonNoteItemFromJson(json);

  Map<String, dynamic> toJson() => _$JsonNoteItemToJson(this);

  NoteItem toModel() => value.toModel();
}

@JsonSerializable()
class JsonNoteItemValue {
  final String id;
  final String text;
  final bool isCompleted;

  const JsonNoteItemValue({
    @required this.id,
    @required this.text,
    @required this.isCompleted,
  });

  factory JsonNoteItemValue.fromJson(Map<String, dynamic> json) => _$JsonNoteItemValueFromJson(json);

  Map<String, dynamic> toJson() => _$JsonNoteItemValueToJson(this);

  NoteItem toModel() => NoteItem(
        id: id,
        text: text,
        isCompleted: isCompleted,
      );
}
