import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/userconfig/model/agenda_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/timetable_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/user_config.dart';

part 'json_user_config.g.dart';

@JsonSerializable()
class JsonUserConfigs {
  final List<JsonUserConfigEdge> edges;

  const JsonUserConfigs({@required this.edges});

  factory JsonUserConfigs.fromJson(Map<String, dynamic> json) => _$JsonUserConfigsFromJson(json);

  Map<String, dynamic> toJson() => _$JsonUserConfigsToJson(this);

  List<UserConfig> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonUserConfigEdge {
  final JsonUserConfig node;

  const JsonUserConfigEdge({@required this.node});

  factory JsonUserConfigEdge.fromJson(Map<String, dynamic> json) => _$JsonUserConfigEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonUserConfigEdgeToJson(this);

  UserConfig toModel() => node.toModel();
}

@JsonSerializable()
class JsonUserConfig {
  final DateTime updatedAt;
  final String schoolYearUuid;
  final Map<String, String> semesterUuidPerSchoolYear;
  final bool autoSemesterChange;
  final bool analyticsEnabled;
  final bool diagnosisEnabled;
  final bool personalizedAdsEnabled;
  final JsonGradingSystem gradingSystem;
  final Map<String, String> timetableUuidPerSchoolYear;
  final Map<String, List<String>> subscribedTimetableUuidsPerSchoolYear;
  final Map<String, dynamic> agendaOptions;
  final Map<String, dynamic> timetableOptions;

  const JsonUserConfig({
    @required this.updatedAt,
    @required this.schoolYearUuid,
    @required this.semesterUuidPerSchoolYear,
    @required this.autoSemesterChange,
    @required this.analyticsEnabled,
    @required this.diagnosisEnabled,
    @required this.personalizedAdsEnabled,
    @required this.gradingSystem,
    @required this.timetableUuidPerSchoolYear,
    @required this.subscribedTimetableUuidsPerSchoolYear,
    @required this.agendaOptions,
    @required this.timetableOptions,
  });

  factory JsonUserConfig.fromJson(Map<String, dynamic> json) => _$JsonUserConfigFromJson(json);

  Map<String, dynamic> toJson() => _$JsonUserConfigToJson(this);

  UserConfig toModel() => UserConfig(
        updatedAt: updatedAt,
        schoolYearId: schoolYearUuid,
        semesterIdPerYear: semesterUuidPerSchoolYear,
        autoSemesterChange: autoSemesterChange,
        analyticsEnabled: analyticsEnabled,
        diagnosisEnabled: diagnosisEnabled,
        personalizedAdsEnabled: personalizedAdsEnabled,
        gradingSystem: gradingSystem?.toModel(),
        timetableIdPerYear: timetableUuidPerSchoolYear,
        subscribedTimetablesPerYear: subscribedTimetableUuidsPerSchoolYear,
        agendaOptions: agendaOptions != null ? AgendaNotificationOptions.fromJson(agendaOptions) : null,
        timetableOptions: timetableOptions != null ? TimetableNotificationOptions.fromJson(timetableOptions) : null,
      );
}

@JsonSerializable()
class JsonGradingSystem {
  final String type;
  final double minValue;
  final double maxValue;

  const JsonGradingSystem({
    @required this.type,
    @required this.minValue,
    @required this.maxValue,
  });

  factory JsonGradingSystem.fromJson(Map<String, dynamic> json) => _$JsonGradingSystemFromJson(json);

  Map<String, dynamic> toJson() => _$JsonGradingSystemToJson(this);

  GradingSystem toModel() => GradingSystem.of(
        GradingSystemTypeExt.forTag(type),
        minValue: minValue,
        maxValue: maxValue,
      );
}
