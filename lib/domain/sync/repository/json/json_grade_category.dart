import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';

part 'json_grade_category.g.dart';

@JsonSerializable()
class JsonGradeCategories {
  final List<JsonGradeCategoryEdge> edges;

  const JsonGradeCategories({@required this.edges});

  factory JsonGradeCategories.fromJson(Map<String, dynamic> json) => _$JsonGradeCategoriesFromJson(json);

  Map<String, dynamic> toJson() => _$JsonGradeCategoriesToJson(this);

  List<GradeCategory> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonGradeCategoryEdge {
  final JsonGradeCategory node;

  const JsonGradeCategoryEdge({@required this.node});

  factory JsonGradeCategoryEdge.fromJson(Map<String, dynamic> json) => _$JsonGradeCategoryEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonGradeCategoryEdgeToJson(this);

  GradeCategory toModel() => node.toModel();
}

@JsonSerializable()
class JsonGradeCategory {
  final DateTime updatedAt;
  final String uuid;
  final String customName;

  const JsonGradeCategory({
    @required this.updatedAt,
    @required this.uuid,
    @required this.customName,
  });

  factory JsonGradeCategory.fromJson(Map<String, dynamic> json) => _$JsonGradeCategoryFromJson(json);

  Map<String, dynamic> toJson() => _$JsonGradeCategoryToJson(this);

  GradeCategory toModel() => GradeCategory(
        id: uuid,
        customName: customName,
        updatedAt: updatedAt,
      );
}
