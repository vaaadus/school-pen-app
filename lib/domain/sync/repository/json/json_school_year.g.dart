// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_school_year.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonSchoolYears _$JsonSchoolYearsFromJson(Map<String, dynamic> json) {
  return JsonSchoolYears(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonSchoolYearEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonSchoolYearsToJson(JsonSchoolYears instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonSchoolYearEdge _$JsonSchoolYearEdgeFromJson(Map<String, dynamic> json) {
  return JsonSchoolYearEdge(
    node: json['node'] == null
        ? null
        : JsonSchoolYear.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonSchoolYearEdgeToJson(JsonSchoolYearEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonSchoolYear _$JsonSchoolYearFromJson(Map<String, dynamic> json) {
  return JsonSchoolYear(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$JsonSchoolYearToJson(JsonSchoolYear instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'name': instance.name,
    };
