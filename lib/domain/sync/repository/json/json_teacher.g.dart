// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_teacher.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonTeachers _$JsonTeachersFromJson(Map<String, dynamic> json) {
  return JsonTeachers(
    edges: (json['edges'] as List)
        ?.map((e) => e == null
            ? null
            : JsonTeacherEdge.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonTeachersToJson(JsonTeachers instance) =>
    <String, dynamic>{
      'edges': instance.edges,
    };

JsonTeacherEdge _$JsonTeacherEdgeFromJson(Map<String, dynamic> json) {
  return JsonTeacherEdge(
    node: json['node'] == null
        ? null
        : JsonTeacher.fromJson(json['node'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonTeacherEdgeToJson(JsonTeacherEdge instance) =>
    <String, dynamic>{
      'node': instance.node,
    };

JsonTeacher _$JsonTeacherFromJson(Map<String, dynamic> json) {
  return JsonTeacher(
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uuid: json['uuid'] as String,
    schoolYearUuid: json['schoolYearUuid'] as String,
    name: json['name'] as String,
    phone: json['phone'] as String,
    email: json['email'] as String,
    website: json['website'] as String,
    address: json['address'] as String,
    officeHours: json['officeHours'] as Map<String, dynamic>,
  );
}

Map<String, dynamic> _$JsonTeacherToJson(JsonTeacher instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uuid': instance.uuid,
      'schoolYearUuid': instance.schoolYearUuid,
      'name': instance.name,
      'phone': instance.phone,
      'email': instance.email,
      'website': instance.website,
      'address': instance.address,
      'officeHours': instance.officeHours,
    };
