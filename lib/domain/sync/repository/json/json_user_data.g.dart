// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_user_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonUserData _$JsonUserDataFromJson(Map<String, dynamic> json) {
  return JsonUserData(
    data: json['data'] == null
        ? null
        : JsonData.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonUserDataToJson(JsonUserData instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

JsonData _$JsonDataFromJson(Map<String, dynamic> json) {
  return JsonData(
    userConfigs: json['userConfigs'] == null
        ? null
        : JsonUserConfigs.fromJson(json['userConfigs'] as Map<String, dynamic>),
    schoolYears: json['schoolYears'] == null
        ? null
        : JsonSchoolYears.fromJson(json['schoolYears'] as Map<String, dynamic>),
    schoolYear_Bins: json['schoolYear_Bins'] == null
        ? null
        : JsonBins.fromJson(json['schoolYear_Bins'] as Map<String, dynamic>),
    semesters: json['semesters'] == null
        ? null
        : JsonSemesters.fromJson(json['semesters'] as Map<String, dynamic>),
    semester_Bins: json['semester_Bins'] == null
        ? null
        : JsonBins.fromJson(json['semester_Bins'] as Map<String, dynamic>),
    notes: json['notes'] == null
        ? null
        : JsonNotes.fromJson(json['notes'] as Map<String, dynamic>),
    note_Bins: json['note_Bins'] == null
        ? null
        : JsonBins.fromJson(json['note_Bins'] as Map<String, dynamic>),
    teachers: json['teachers'] == null
        ? null
        : JsonTeachers.fromJson(json['teachers'] as Map<String, dynamic>),
    teacher_Bins: json['teacher_Bins'] == null
        ? null
        : JsonBins.fromJson(json['teacher_Bins'] as Map<String, dynamic>),
    courses: json['courses'] == null
        ? null
        : JsonCourses.fromJson(json['courses'] as Map<String, dynamic>),
    course_Bins: json['course_Bins'] == null
        ? null
        : JsonBins.fromJson(json['course_Bins'] as Map<String, dynamic>),
    grades: json['grades'] == null
        ? null
        : JsonGrades.fromJson(json['grades'] as Map<String, dynamic>),
    grade_Bins: json['grade_Bins'] == null
        ? null
        : JsonBins.fromJson(json['grade_Bins'] as Map<String, dynamic>),
    gradeCategories: json['gradeCategories'] == null
        ? null
        : JsonGradeCategories.fromJson(
            json['gradeCategories'] as Map<String, dynamic>),
    gradeCategory_Bins: json['gradeCategory_Bins'] == null
        ? null
        : JsonBins.fromJson(json['gradeCategory_Bins'] as Map<String, dynamic>),
    publicTimetables: json['publicTimetables'] == null
        ? null
        : JsonPublicTimetables.fromJson(
            json['publicTimetables'] as Map<String, dynamic>),
    customTimetables: json['customTimetables'] == null
        ? null
        : JsonTimetablesInfo.fromJson(
            json['customTimetables'] as Map<String, dynamic>),
    customTimetable_Bins: json['customTimetable_Bins'] == null
        ? null
        : JsonBins.fromJson(
            json['customTimetable_Bins'] as Map<String, dynamic>),
    customTimetableEvents: json['customTimetableEvents'] == null
        ? null
        : JsonTimetableEvents.fromJson(
            json['customTimetableEvents'] as Map<String, dynamic>),
    customTimetableEvent_Bins: json['customTimetableEvent_Bins'] == null
        ? null
        : JsonBins.fromJson(
            json['customTimetableEvent_Bins'] as Map<String, dynamic>),
    exams: json['exams'] == null
        ? null
        : JsonExams.fromJson(json['exams'] as Map<String, dynamic>),
    exam_Bins: json['exam_Bins'] == null
        ? null
        : JsonBins.fromJson(json['exam_Bins'] as Map<String, dynamic>),
    assignments: json['assignments'] == null
        ? null
        : JsonHomeworks.fromJson(json['assignments'] as Map<String, dynamic>),
    assignment_Bins: json['assignment_Bins'] == null
        ? null
        : JsonBins.fromJson(json['assignment_Bins'] as Map<String, dynamic>),
    reminders: json['reminders'] == null
        ? null
        : JsonReminders.fromJson(json['reminders'] as Map<String, dynamic>),
    reminder_Bins: json['reminder_Bins'] == null
        ? null
        : JsonBins.fromJson(json['reminder_Bins'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JsonDataToJson(JsonData instance) => <String, dynamic>{
      'userConfigs': instance.userConfigs,
      'schoolYears': instance.schoolYears,
      'schoolYear_Bins': instance.schoolYear_Bins,
      'semesters': instance.semesters,
      'semester_Bins': instance.semester_Bins,
      'notes': instance.notes,
      'note_Bins': instance.note_Bins,
      'teachers': instance.teachers,
      'teacher_Bins': instance.teacher_Bins,
      'courses': instance.courses,
      'course_Bins': instance.course_Bins,
      'grades': instance.grades,
      'grade_Bins': instance.grade_Bins,
      'gradeCategories': instance.gradeCategories,
      'gradeCategory_Bins': instance.gradeCategory_Bins,
      'publicTimetables': instance.publicTimetables,
      'customTimetables': instance.customTimetables,
      'customTimetable_Bins': instance.customTimetable_Bins,
      'customTimetableEvents': instance.customTimetableEvents,
      'customTimetableEvent_Bins': instance.customTimetableEvent_Bins,
      'exams': instance.exams,
      'exam_Bins': instance.exam_Bins,
      'assignments': instance.assignments,
      'assignment_Bins': instance.assignment_Bins,
      'reminders': instance.reminders,
      'reminder_Bins': instance.reminder_Bins,
    };
