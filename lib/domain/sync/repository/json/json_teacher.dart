import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/teacher/model/office_hours.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';

part 'json_teacher.g.dart';

@JsonSerializable()
class JsonTeachers {
  final List<JsonTeacherEdge> edges;

  const JsonTeachers({@required this.edges});

  factory JsonTeachers.fromJson(Map<String, dynamic> json) => _$JsonTeachersFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTeachersToJson(this);

  List<Teacher> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonTeacherEdge {
  final JsonTeacher node;

  const JsonTeacherEdge({@required this.node});

  factory JsonTeacherEdge.fromJson(Map<String, dynamic> json) => _$JsonTeacherEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTeacherEdgeToJson(this);

  Teacher toModel() => node.toModel();
}

@JsonSerializable()
class JsonTeacher {
  final DateTime updatedAt;
  final String uuid;
  final String schoolYearUuid;
  final String name;
  final String phone;
  final String email;
  final String website;
  final String address;
  final Map<String, dynamic> officeHours;

  const JsonTeacher({
    @required this.updatedAt,
    @required this.uuid,
    @required this.schoolYearUuid,
    this.name,
    this.phone,
    this.email,
    this.website,
    this.address,
    this.officeHours,
  });

  factory JsonTeacher.fromJson(Map<String, dynamic> json) => _$JsonTeacherFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTeacherToJson(this);

  Teacher toModel() => Teacher(
        id: uuid,
        schoolYearId: schoolYearUuid,
        name: name,
        phone: phone,
        email: email,
        website: website,
        address: address,
        officeHours: OfficeHours.fromJson(officeHours),
        updatedAt: updatedAt,
      );
}
