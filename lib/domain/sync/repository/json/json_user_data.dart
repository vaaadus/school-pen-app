import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
import 'package:school_pen/domain/sync/model/user_data.dart';
import 'package:school_pen/domain/sync/repository/json/json_bin.dart';
import 'package:school_pen/domain/sync/repository/json/json_course.dart';
import 'package:school_pen/domain/sync/repository/json/json_exam.dart';
import 'package:school_pen/domain/sync/repository/json/json_grade.dart';
import 'package:school_pen/domain/sync/repository/json/json_grade_category.dart';
import 'package:school_pen/domain/sync/repository/json/json_homework.dart';
import 'package:school_pen/domain/sync/repository/json/json_note.dart';
import 'package:school_pen/domain/sync/repository/json/json_public_timetable.dart';
import 'package:school_pen/domain/sync/repository/json/json_reminder.dart';
import 'package:school_pen/domain/sync/repository/json/json_school_year.dart';
import 'package:school_pen/domain/sync/repository/json/json_semester.dart';
import 'package:school_pen/domain/sync/repository/json/json_teacher.dart';
import 'package:school_pen/domain/sync/repository/json/json_timetable_event.dart';
import 'package:school_pen/domain/sync/repository/json/json_timetable_info.dart';
import 'package:school_pen/domain/sync/repository/json/json_user_config.dart';

part 'json_user_data.g.dart';

@JsonSerializable()
class JsonUserData {
  final JsonData data;

  const JsonUserData({
    @required this.data,
  });

  factory JsonUserData.fromJson(Map<String, dynamic> json) => _$JsonUserDataFromJson(json);

  Map<String, dynamic> toJson() => _$JsonUserDataToJson(this);

  UserData toModel() => data.toModel();
}

@JsonSerializable()
class JsonData {
  final JsonUserConfigs userConfigs;
  final JsonSchoolYears schoolYears;
  final JsonBins schoolYear_Bins;
  final JsonSemesters semesters;
  final JsonBins semester_Bins;
  final JsonNotes notes;
  final JsonBins note_Bins;
  final JsonTeachers teachers;
  final JsonBins teacher_Bins;
  final JsonCourses courses;
  final JsonBins course_Bins;
  final JsonGrades grades;
  final JsonBins grade_Bins;
  final JsonGradeCategories gradeCategories;
  final JsonBins gradeCategory_Bins;
  final JsonPublicTimetables publicTimetables;
  final JsonTimetablesInfo customTimetables;
  final JsonBins customTimetable_Bins;
  final JsonTimetableEvents customTimetableEvents;
  final JsonBins customTimetableEvent_Bins;
  final JsonExams exams;
  final JsonBins exam_Bins;
  final JsonHomeworks assignments;
  final JsonBins assignment_Bins;
  final JsonReminders reminders;
  final JsonBins reminder_Bins;

  const JsonData({
    @required this.userConfigs,
    @required this.schoolYears,
    @required this.schoolYear_Bins,
    @required this.semesters,
    @required this.semester_Bins,
    @required this.notes,
    @required this.note_Bins,
    @required this.teachers,
    @required this.teacher_Bins,
    @required this.courses,
    @required this.course_Bins,
    @required this.grades,
    @required this.grade_Bins,
    @required this.gradeCategories,
    @required this.gradeCategory_Bins,
    @required this.publicTimetables,
    @required this.customTimetables,
    @required this.customTimetable_Bins,
    @required this.customTimetableEvents,
    @required this.customTimetableEvent_Bins,
    @required this.exams,
    @required this.exam_Bins,
    @required this.assignments,
    @required this.assignment_Bins,
    @required this.reminders,
    @required this.reminder_Bins,
  });

  factory JsonData.fromJson(Map<String, dynamic> json) => _$JsonDataFromJson(json);

  Map<String, dynamic> toJson() => _$JsonDataToJson(this);

  UserData toModel() => UserData(
        userConfig: userConfigs.toModel().first,
        schoolYears: schoolYears.toModel(),
        deletedSchoolYears: schoolYear_Bins.toModel(),
        semesters: semesters.toModel(),
        deletedSemesters: semester_Bins.toModel(),
        notes: notes.toModel(),
        deletedNotes: note_Bins.toModel(),
        teachers: teachers.toModel(),
        deletedTeachers: teacher_Bins.toModel(),
        courses: courses.toModel(),
        deletedCourses: course_Bins.toModel(),
        grades: grades.toModel(),
        deletedGrades: grade_Bins.toModel(),
        gradeCategories: gradeCategories.toModel(),
        deletedGradeCategories: gradeCategory_Bins.toModel(),
        publicTimetables: publicTimetables.toModel(),
        customTimetables: customTimetables.toModel(),
        deletedCustomTimetables: customTimetable_Bins.toModel(),
        customTimetableEvents: customTimetableEvents.toModel(),
        deletedCustomTimetableEvents: customTimetableEvent_Bins.toModel(),
        exams: exams.toModel(),
        deletedExams: exam_Bins.toModel(),
        homework: assignments.toModel(),
        deletedHomework: assignment_Bins.toModel(),
        reminders: reminders.toModel(),
        deletedReminders: reminder_Bins.toModel(),
      );
}
