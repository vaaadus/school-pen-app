import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/semester/model/semester.dart';

part 'json_semester.g.dart';

@JsonSerializable()
class JsonSemesters {
  final List<JsonSemesterEdge> edges;

  const JsonSemesters({@required this.edges});

  factory JsonSemesters.fromJson(Map<String, dynamic> json) => _$JsonSemestersFromJson(json);

  Map<String, dynamic> toJson() => _$JsonSemestersToJson(this);

  List<Semester> toModel() => edges.map((e) => e.toModel()).toList();
}

@JsonSerializable()
class JsonSemesterEdge {
  final JsonSemester node;

  const JsonSemesterEdge({@required this.node});

  factory JsonSemesterEdge.fromJson(Map<String, dynamic> json) => _$JsonSemesterEdgeFromJson(json);

  Map<String, dynamic> toJson() => _$JsonSemesterEdgeToJson(this);

  Semester toModel() => node.toModel();
}

@JsonSerializable()
class JsonSemester {
  final DateTime updatedAt;
  final String uuid;
  final String schoolYearUuid;
  final String name;
  final DateTime startDate;
  final DateTime endDate;

  const JsonSemester({
    @required this.updatedAt,
    @required this.uuid,
    @required this.schoolYearUuid,
    @required this.name,
    this.startDate,
    this.endDate,
  });

  factory JsonSemester.fromJson(Map<String, dynamic> json) => _$JsonSemesterFromJson(json);

  Map<String, dynamic> toJson() => _$JsonSemesterToJson(this);

  Semester toModel() => Semester(
        id: uuid,
        schoolYearId: schoolYearUuid,
        name: name,
        startDate: startDate,
        endDate: endDate,
        updatedAt: updatedAt,
      );
}
