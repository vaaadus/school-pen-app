import 'package:school_pen/domain/sync/model/user_data.dart';

abstract class SyncRepository {
  /// Fetches all user data from sync server.
  Future<UserData> getUserData(List<String> subscribedTimetables);
}
