import 'package:school_pen/domain/sync/sync_manager.dart';

abstract class SyncListener {
  /// Called when the [SyncManager] has synced data.
  void onSyncResumed();

  /// Called when the [SyncManager] stops syncing data.
  void onSyncPaused();
}
