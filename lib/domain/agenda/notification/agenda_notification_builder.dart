import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

class AgendaNotificationBuilder {
  AddNotification build(DateTime dateTime, int numberOfEvents) {
    assert(dateTime != null);
    assert(numberOfEvents != null);

    return AddNotification(
      title: S().notificationChannel_agenda_title,
      body: _formatBody(numberOfEvents),
      dateTime: dateTime,
      channel: NotificationChannel.agenda,
      payload: NotificationPayload(),
    );
  }

  String _formatBody(int numberOfEvents) {
    if (numberOfEvents == 0) return S().notification_agenda_bodyEmpty;

    return sprintf(S().notification_agenda_body, [numberOfEvents]);
  }
}
