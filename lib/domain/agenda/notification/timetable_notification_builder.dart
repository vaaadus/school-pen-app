import 'package:school_pen/app/common/formatter/timetable_event_formatter.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

class TimetableNotificationBuilder {
  AddNotification build(DateTime dateTime, List<SingleTimetableEvent> events, List<Course> courses) {
    assert(dateTime != null);
    _assertEventsHaveSameStartTime(events);

    return AddNotification(
      title: S().notificationChannel_upcomingLessons_title,
      body: _formatBody(events, courses),
      dateTime: dateTime,
      channel: NotificationChannel.upcomingLessons,
      payload: NotificationPayload(),
    );
  }

  void _assertEventsHaveSameStartTime(List<SingleTimetableEvent> events) {
    assert(events.isNotEmpty);

    for (SingleTimetableEvent event in events) {
      assert(events.first.timeRange.start == event.timeRange.start);
    }
  }

  String _formatBody(List<SingleTimetableEvent> events, List<Course> courses) {
    String result = '';
    for (int i = 0; i < events.length; i++) {
      final SingleTimetableEvent event = events[i];
      final Course course = courses.firstWhereOrNull((e) => e.id == event.courseId);
      result += _formatSingleBody(event, course);
      if (i + 1 < events.length) result += '.\n';
    }
    return result;
  }

  String _formatSingleBody(SingleTimetableEvent event, Course course) {
    return sprintf(
      S().notification_upcomingLessons_body,
      [
        TimetableEventFormatter.formatCourseName(event, course),
        _formatLessonDetails(event, course),
      ],
    );
  }

  String _formatLessonDetails(SingleTimetableEvent event, Course course) {
    String result = TimetableEventFormatter.formatTimeRange(event.timeRange);
    final String room = TimetableEventFormatter.formatRoom(event, course);
    if (Strings.isNotBlank(room)) {
      result += ', ' + room;
    }
    return result;
  }
}
