import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';

enum AgendaEventKind { timetable, note, exam, homework, reminder }

extension AgendaEventKindExt on AgendaEventKind {
  String get tag {
    switch (this) {
      case AgendaEventKind.timetable:
        return 'timetable';
      case AgendaEventKind.note:
        return 'note';
      case AgendaEventKind.exam:
        return 'exam';
      case AgendaEventKind.homework:
        return 'homework';
      case AgendaEventKind.reminder:
        return 'reminder';
    }

    throw Exception('Unsupported kind: $this');
  }
}

/// A base class for all agenda events.
abstract class AgendaEvent implements Comparable<AgendaEvent> {
  const AgendaEvent();

  /// Whether the event is done at the [dateTime].
  bool isDoneAt(DateTime dateTime);

  /// Returns the date time range of the event.
  /// If the event does not have a concept of duration, then
  /// the end date should be set to a value relatively close to
  /// start date so that during the date range the event can be considered in active state.
  DateTimeRange get dateTimeRange;

  /// The type of the event.
  AgendaEventKind get kind;

  /// Put first earlier events, put last later events.
  @override
  int compareTo(AgendaEvent other) {
    final int result = dateTimeRange.start.compareTo(other.dateTimeRange.start);
    if (result != 0) return result;

    return dateTimeRange.end.compareTo(other.dateTimeRange.end);
  }
}

class AgendaTimetableEvent extends AgendaEvent with EquatableMixin {
  final SingleTimetableEvent raw;
  final TimetableType timetableType;
  final DateTimeRange _dateTimeRange;

  AgendaTimetableEvent(this.raw, this.timetableType)
      : _dateTimeRange = DateTimeRange(
          start: raw.dateTime.withTime(raw.timeRange.start).asLocal(),
          end: raw.dateTime.withTime(raw.timeRange.end).asLocal(),
        );

  @override
  bool isDoneAt(DateTime dateTime) => _dateTimeRange.end.isBefore(dateTime);

  @override
  DateTimeRange get dateTimeRange => _dateTimeRange;

  @override
  AgendaEventKind get kind => AgendaEventKind.timetable;

  @override
  List<Object> get props => [raw, timetableType];
}

class AgendaNoteEvent extends AgendaEvent with EquatableMixin {
  final Note note;
  final DateTimeRange _dateTimeRange;

  AgendaNoteEvent(this.note)
      : assert(note.notificationDate != null),
        _dateTimeRange = DateTimeRange(
          start: note.notificationDate,
          end: note.notificationDate.add(Duration(minutes: 15)),
        );

  @override
  bool isDoneAt(DateTime dateTime) => note.isArchived;

  @override
  DateTimeRange get dateTimeRange => _dateTimeRange;

  @override
  AgendaEventKind get kind => AgendaEventKind.note;

  @override
  List<Object> get props => [note];
}

class AgendaExamEvent extends AgendaEvent with EquatableMixin {
  final Exam exam;
  final DateTimeRange _dateTimeRange;

  AgendaExamEvent(this.exam)
      : _dateTimeRange = DateTimeRange(
          start: exam.dateTime,
          end: exam.dateTime.add(Duration(minutes: 15)),
        );

  @override
  bool isDoneAt(DateTime dateTime) => exam.isArchived;

  @override
  DateTimeRange get dateTimeRange => _dateTimeRange;

  @override
  AgendaEventKind get kind => AgendaEventKind.exam;

  @override
  List<Object> get props => [exam];
}

class AgendaHomeworkEvent extends AgendaEvent with EquatableMixin {
  final Homework homework;
  final DateTimeRange _dateTimeRange;

  AgendaHomeworkEvent(this.homework)
      : _dateTimeRange = DateTimeRange(
          start: homework.dateTime,
          end: homework.dateTime.add(Duration(minutes: 15)),
        );

  @override
  bool isDoneAt(DateTime dateTime) => homework.isArchived;

  @override
  DateTimeRange get dateTimeRange => _dateTimeRange;

  @override
  AgendaEventKind get kind => AgendaEventKind.homework;

  @override
  List<Object> get props => [homework];
}

class AgendaReminderEvent extends AgendaEvent with EquatableMixin {
  final Reminder reminder;
  final DateTimeRange _dateTimeRange;

  AgendaReminderEvent(this.reminder)
      : _dateTimeRange = DateTimeRange(
          start: reminder.dateTime,
          end: reminder.dateTime.add(Duration(minutes: 15)),
        );

  @override
  bool isDoneAt(DateTime dateTime) => reminder.isArchived;

  @override
  DateTimeRange get dateTimeRange => _dateTimeRange;

  @override
  AgendaEventKind get kind => AgendaEventKind.reminder;

  @override
  List<Object> get props => [reminder];
}
