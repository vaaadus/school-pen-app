import 'dart:async';

import 'package:flutter/material.dart';
import 'package:school_pen/domain/agenda/agenda_listener.dart';
import 'package:school_pen/domain/agenda/agenda_manager.dart';
import 'package:school_pen/domain/agenda/model/agenda_event.dart';
import 'package:school_pen/domain/agenda/notification/agenda_notification_builder.dart';
import 'package:school_pen/domain/agenda/notification/timetable_notification_builder.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/exam/exam_listener.dart';
import 'package:school_pen/domain/exam/exam_manager.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/homework/homework_listener.dart';
import 'package:school_pen/domain/homework/homework_manager.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/note_listener.dart';
import 'package:school_pen/domain/note/note_manager.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/scheduled_notification.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/reminder/reminder_listener.dart';
import 'package:school_pen/domain/reminder/reminder_manager.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/timetable_listener.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';
import 'package:school_pen/domain/userconfig/model/agenda_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/timetable_notification_options.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:school_pen/domain/work/work_listener.dart';
import 'package:school_pen/domain/work/work_manager.dart';
import 'package:synchronized/synchronized.dart';

class AgendaManagerImpl
    implements
        AgendaManager,
        SchoolYearListener,
        SemesterListener,
        TimetableListener,
        NoteListener,
        ExamListener,
        HomeworkListener,
        ReminderListener,
        CourseListener,
        WorkListener {
  static const Logger _logger = Logger('AgendaManagerImpl');
  static const Duration _dispatchChangesDelay = Duration(milliseconds: 500);

  final Set<AgendaListener> _listeners = {};
  final SchoolYearManager _schoolYearManager;
  final SemesterManager _semesterManager;
  final TimetableManager _timetableManager;
  final NoteManager _noteManager;
  final ExamManager _examManager;
  final HomeworkManager _homeworkManager;
  final ReminderManager _reminderManager;
  final CourseManager _courseManager;
  final UserConfigRepository _userConfig;
  final WorkManager _workManager;
  final AgendaNotificationsManager _agendaNotifications;
  final DateTimeProvider _dateTimeProvider;

  Timer _dispatchChangesTimer;

  AgendaManagerImpl(
    this._schoolYearManager,
    this._semesterManager,
    this._timetableManager,
    this._noteManager,
    this._examManager,
    this._homeworkManager,
    this._reminderManager,
    this._courseManager,
    this._userConfig,
    this._workManager,
    this._agendaNotifications,
    this._dateTimeProvider,
  ) {
    _schoolYearManager.addSchoolYearListener(this, notifyOnAttach: false);
    _semesterManager.addSemesterListener(this, notifyOnAttach: false);
    _timetableManager.addTimetableListener(this, notifyOnAttach: false);
    _noteManager.addNoteListener(this, notifyOnAttach: false);
    _examManager.addExamListener(this, notifyOnAttach: false);
    _homeworkManager.addHomeworkListener(this, notifyOnAttach: false);
    _reminderManager.addReminderListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _workManager.addWorkListener(this);

    _userConfig.onChange.listen((List<Property> properties) {
      if (properties.contains(Property.agendaOptions) || properties.contains(Property.timetableOptions)) {
        _dispatchAgendaChanged();
      }
    });
  }

  @override
  void addAgendaListener(AgendaListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onAgendaChanged(this);
  }

  @override
  void removeAgendaListener(AgendaListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<List<AgendaEvent>> getEvents(DateTime dateTime, {List<AgendaEventKind> kinds = AgendaEventKind.values}) async {
    final SchoolYear year = await _schoolYearManager.getActive();
    final List<Semester> semesters = await _semesterManager.getAll();
    final List<String> semestersIds = semesters.map((e) => e.id).toList();

    final List<List<AgendaEvent>> futureEvents = await Future.wait([
      if (kinds.contains(AgendaEventKind.timetable)) _fetchTimetableEventsAt(dateTime),
      if (kinds.contains(AgendaEventKind.note)) _fetchNotes(year.id, (e) => e.notificationDate?.isSameDateAs(dateTime) ?? false),
      if (kinds.contains(AgendaEventKind.exam)) _fetchExams(semestersIds, (e) => e.dateTime.isSameDateAs(dateTime)),
      if (kinds.contains(AgendaEventKind.homework)) _fetchHomework(semestersIds, (e) => e.dateTime.isSameDateAs(dateTime)),
      if (kinds.contains(AgendaEventKind.reminder)) _fetchReminders(semestersIds, (e) => e.dateTime.isSameDateAs(dateTime)),
    ]);

    final List<AgendaEvent> events = futureEvents.reduce((value, element) => [...value, ...element]);
    events.sort();
    return events;
  }

  @override
  Future<List<AgendaEvent>> getOverdueEvents() async {
    final DateTime dateTime = _dateTimeProvider.now().withoutTime();
    final List<Semester> semesters = await _semesterManager.getAll();
    final List<String> semestersIds = semesters.map((e) => e.id).toList();

    final List<List<AgendaEvent>> futureEvents = await Future.wait([
      _fetchExams(semestersIds, (e) => e.dateTime.isBefore(dateTime)),
      _fetchHomework(semestersIds, (e) => e.dateTime.isBefore(dateTime)),
      _fetchReminders(semestersIds, (e) => e.dateTime.isBefore(dateTime)),
    ]);

    final List<AgendaEvent> events =
        futureEvents.reduce((value, element) => [...value, ...element]).where((e) => !e.isDoneAt(dateTime)).toList();
    events.sort();
    return events;
  }

  @override
  Future<AgendaNotificationOptions> getAgendaOptions() async {
    final AgendaNotificationOptions options = await _userConfig.getAgendaOptions();
    return options ?? AgendaNotificationOptions();
  }

  @override
  Future<void> setAgendaOptions(AgendaNotificationOptions options) async {
    await _userConfig.setAgendaOptions(options);
  }

  @override
  Future<TimetableNotificationOptions> getTimetableOptions() async {
    final TimetableNotificationOptions options = await _userConfig.getTimetableOptions();
    return options ?? TimetableNotificationOptions();
  }

  @override
  Future<void> setTimetableOptions(TimetableNotificationOptions options) async {
    await _userConfig.setTimetableOptions(options);
  }

  Future<List<AgendaTimetableEvent>> _fetchTimetableEventsAt(DateTime dateTime) async {
    dateTime = dateTime.asUtc();

    final List<Timetable> timetables = await _timetableManager.getUserTimetables();
    final List<AgendaTimetableEvent> result = [];

    final DateTimeRange dateRange = DateTimeRange(
      start: dateTime.withoutTime(),
      end: dateTime.atEndOfDay(),
    );

    for (Timetable timetable in timetables) {
      final List<SingleTimetableEvent> events = (await _timetableManager.getSingleEventsOf(timetable, dateRange))
          .where((e) => dateRange.overlapsWith(_dateRangeOfTimetableEvent(e)))
          .toList();

      for (SingleTimetableEvent event in events) {
        result.add(AgendaTimetableEvent(event, timetable.type));
      }
    }

    return result;
  }

  DateTimeRange _dateRangeOfTimetableEvent(SingleTimetableEvent event) {
    return DateTimeRange(
      start: event.dateTime.withTime(event.timeRange.start),
      end: event.dateTime.withTime(event.timeRange.end),
    );
  }

  Future<List<AgendaNoteEvent>> _fetchNotes(String schoolYearId, bool Function(Note) predicate) async {
    return (await _noteManager.getAll(schoolYearId)).where(predicate).map((e) => AgendaNoteEvent(e)).toList();
  }

  Future<List<AgendaExamEvent>> _fetchExams(List<String> semestersIds, bool Function(Exam) predicate) async {
    return (await _examManager.getAll())
        .where((e) => semestersIds.contains(e.semesterId))
        .where(predicate)
        .map((e) => AgendaExamEvent(e))
        .toList();
  }

  Future<List<AgendaHomeworkEvent>> _fetchHomework(List<String> semestersIds, bool Function(Homework) predicate) async {
    return (await _homeworkManager.getAll())
        .where((e) => semestersIds.contains(e.semesterId))
        .where(predicate)
        .map((e) => AgendaHomeworkEvent(e))
        .toList();
  }

  Future<List<AgendaReminderEvent>> _fetchReminders(List<String> semestersIds, bool Function(Reminder) predicate) async {
    return (await _reminderManager.getAll())
        .where((e) => semestersIds.contains(e.semesterId))
        .where(predicate)
        .map((e) => AgendaReminderEvent(e))
        .toList();
  }

  @override
  void onSchoolYearsChanged(SchoolYearManager manager) {
    _dispatchAgendaChanged();
  }

  @override
  void onSemestersChanged(SemesterManager manager) {
    _dispatchAgendaChanged();
  }

  @override
  void onTimetablesChanged(TimetableManager manager) {
    _dispatchAgendaChanged();
  }

  @override
  void onNotesChanged(NoteManager manager) {
    _dispatchAgendaChanged();
  }

  @override
  void onExamsChanged(ExamManager manager) {
    _dispatchAgendaChanged();
  }

  @override
  void onHomeworkChanged(HomeworkManager manager) {
    _dispatchAgendaChanged();
  }

  @override
  void onRemindersChanged(ReminderManager manager) {
    _dispatchAgendaChanged();
  }

  @override
  void onCoursesChanged(CourseManager manager) {
    _dispatchAgendaChanged();
  }

  @override
  Future<void> onBackgroundWork() async {
    await _setupNotifications();
  }

  void _dispatchAgendaChanged() {
    _dispatchChangesTimer?.cancel();
    _dispatchChangesTimer = Timer(_dispatchChangesDelay, () {
      _setupNotifications();

      for (AgendaListener listener in _listeners) {
        listener.onAgendaChanged(this);
      }
    });
  }

  Future<void> _setupNotifications() async {
    try {
      await _agendaNotifications.setupNotifications(this);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}

class AgendaNotificationsManager {
  static const int numberOfDaysToScheduleInAdvance = 2;
  static const Duration cancelIfOlderThan = Duration(minutes: 30);
  static const List<AgendaEventKind> agendaKinds = [
    AgendaEventKind.note,
    AgendaEventKind.exam,
    AgendaEventKind.homework,
    AgendaEventKind.reminder,
  ];

  final NotificationManager _notifications;
  final AgendaNotificationBuilder _agendaNotificationBuilder;
  final TimetableNotificationBuilder _timetableNotificationBuilder;
  final CourseManager _courseManager;
  final DateTimeProvider _dateTimeProvider;
  final Lock _lock = Lock();

  AgendaNotificationsManager(
    this._notifications,
    this._agendaNotificationBuilder,
    this._timetableNotificationBuilder,
    this._courseManager,
    this._dateTimeProvider,
  );

  Future<void> setupNotifications(AgendaManager manager) async {
    await _lock.synchronized(() async {
      await Future.wait([
        _setupAgendaNotifications(manager),
        _setupTimetableNotifications(manager),
      ]);
    });
  }

  Future<void> _setupAgendaNotifications(AgendaManager manager) async {
    final AgendaNotificationOptions options = await manager.getAgendaOptions();
    if (!options.enabled) {
      await _notifications.cancelAllOnChannel(NotificationChannel.agenda);
      return;
    }

    final List<AddNotification> notifications = await _fetchAgendaNotifications(manager, options);
    await _notifications.scheduleAllOnChannel(NotificationChannel.agenda, notifications);
  }

  Future<List<AddNotification>> _fetchAgendaNotifications(AgendaManager manager, AgendaNotificationOptions options) async {
    final List<AddNotification> notifications = [];
    for (int i = 0; i < numberOfDaysToScheduleInAdvance; i++) {
      final DateTime dateTime = _dateTimeProvider.now().add(Duration(days: i)).withoutTime();
      if (!options.weekdays.contains(Weekday.fromDateTime(dateTime))) {
        continue;
      }

      final List<AgendaEvent> events = await manager.getEvents(dateTime, kinds: [
        AgendaEventKind.note,
        AgendaEventKind.exam,
        AgendaEventKind.homework,
        AgendaEventKind.reminder,
      ]);

      if (events.isNotEmpty || options.notifyIfNoEvents) {
        notifications.add(_agendaNotificationBuilder.build(
          dateTime.withTime(options.time),
          events.length,
        ));
      }
    }

    return notifications;
  }

  Future<void> _setupTimetableNotifications(AgendaManager manager) async {
    final TimetableNotificationOptions options = await manager.getTimetableOptions();

    if (!options.enabled) {
      await _notifications.cancelAllOnChannel(NotificationChannel.upcomingLessons);
      return;
    }

    final DateTime now = _dateTimeProvider.now();
    final DateTime past = now.subtract(cancelIfOlderThan);

    final List<ScheduledNotification> scheduled = await _notifications.getAllScheduledOn(NotificationChannel.upcomingLessons);
    final List<AddNotification> scheduledAdd = scheduled.map((e) => e.toAddNotification()).toList();

    final List<ScheduledNotification> nonCancellable =
        scheduled.where((e) => e.dateTime.isAfterOrAt(past) && e.dateTime.isBefore(now)).toList();

    final List<AddNotification> schedulable =
        (await _fetchTimetableNotifications(manager, options)).where((e) => e.dateTime.isAfter(now)).toList();

    final List<ScheduledNotification> cancellable =
        scheduled.where((e) => !nonCancellable.contains(e)).where((e) => !schedulable.contains(e.toAddNotification())).toList();

    final List<AddNotification> optimizedSchedulable = schedulable.where((e) => !scheduledAdd.contains(e)).toList();

    await _notifications.cancelAll(cancellable.map((e) => e.id).toList());
    await _notifications.scheduleAll(optimizedSchedulable);
  }

  Future<List<AddNotification>> _fetchTimetableNotifications(AgendaManager manager, TimetableNotificationOptions options) async {
    final List<AddNotification> notifications = [];
    final List<Course> courses = await _courseManager.getAll();

    for (int i = 0; i < numberOfDaysToScheduleInAdvance; i++) {
      final DateTime dateTime = _dateTimeProvider.now().add(Duration(days: i)).withoutTime();
      final List<AgendaEvent> events = await manager.getEvents(dateTime, kinds: [AgendaEventKind.timetable]);

      final Map<Time, List<SingleTimetableEvent>> eventsByStartTime = {};
      for (AgendaTimetableEvent agendaEvent in events.cast()) {
        final Time time = agendaEvent.raw.timeRange.start;
        final List<SingleTimetableEvent> group = eventsByStartTime[time] ?? [];
        group.add(agendaEvent.raw);
        eventsByStartTime[time] = group;
      }

      for (List<SingleTimetableEvent> events in eventsByStartTime.values) {
        final DateTime dateTime = events.first.reoccursAt.start;
        notifications.add(
          _timetableNotificationBuilder.build(
            dateTime.subtract(Duration(minutes: options.notifyBeforeLessonInMinutes)).asLocal(),
            events,
            courses,
          ),
        );
      }
    }

    return notifications;
  }
}
