import 'package:school_pen/domain/agenda/agenda_manager.dart';

abstract class AgendaListener {
  /// Called when the agenda changes.
  void onAgendaChanged(AgendaManager manager);
}
