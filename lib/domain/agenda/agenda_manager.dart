import 'package:school_pen/domain/agenda/agenda_listener.dart';
import 'package:school_pen/domain/agenda/model/agenda_event.dart';
import 'package:school_pen/domain/userconfig/model/agenda_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/timetable_notification_options.dart';

abstract class AgendaManager {
  void addAgendaListener(AgendaListener listener, {bool notifyOnAttach = true});

  void removeAgendaListener(AgendaListener listener);

  /// Returns a list of all agenda events in the current school year, in the current semester.
  Future<List<AgendaEvent>> getEvents(DateTime dateTime, {List<AgendaEventKind> kinds = AgendaEventKind.values});

  /// Returns a list of all agenda events which should be finished by now. By default, does not include events from today.
  /// Skips notes & timetable events.
  Future<List<AgendaEvent>> getOverdueEvents();

  Future<AgendaNotificationOptions> getAgendaOptions();

  Future<void> setAgendaOptions(AgendaNotificationOptions options);

  Future<TimetableNotificationOptions> getTimetableOptions();

  Future<void> setTimetableOptions(TimetableNotificationOptions options);
}
