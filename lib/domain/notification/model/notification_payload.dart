import 'package:equatable/equatable.dart';

/// Extra data passed to the notification,
/// received when notification is opened.
class NotificationPayload extends Equatable {
  /// Points to the object about which is this notification.
  final String objectId;

  const NotificationPayload({this.objectId});

  factory NotificationPayload.fromJson(Map<String, dynamic> map) {
    return NotificationPayload(objectId: map['objectId']);
  }

  Map<String, dynamic> toJson() {
    return {'objectId': objectId};
  }

  @override
  List<Object> get props => [objectId];
}
