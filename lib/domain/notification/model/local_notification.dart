import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';

/// Represents a notification shown to the user, scheduled by the local device.
class LocalNotification extends Equatable {
  final int id;
  final NotificationChannel channel;
  final NotificationPayload payload;

  const LocalNotification({
    @required this.id,
    @required this.channel,
    @required this.payload,
  });

  @override
  List<Object> get props => [id, channel, payload];
}
