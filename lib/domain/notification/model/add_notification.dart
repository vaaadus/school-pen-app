import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';
import 'package:school_pen/domain/notification/model/scheduled_notification.dart';

/// Model entity for adding a local notification.
class AddNotification extends Equatable {
  /// The notification header.
  final String title;

  /// The notification message.
  final String body;

  /// Specifies when to show the notification.
  final DateTime dateTime;

  /// Specifies when the event occurs.
  final DateTime when;

  /// The notification channel to which the notification will be sent.
  final NotificationChannel channel;

  /// The extra information for this notification.
  final NotificationPayload payload;

  const AddNotification({
    @required this.title,
    @required this.body,
    @required this.dateTime,
    @required this.channel,
    @required this.payload,
    this.when,
  })  : assert(title != null),
        assert(body != null),
        assert(dateTime != null),
        assert(channel != null),
        assert(payload != null);

  factory AddNotification.fromJson(Map<String, dynamic> map) => AddNotification(
        title: map['title'],
        body: map['body'],
        dateTime: DateTime.fromMicrosecondsSinceEpoch(map['dateTime']),
        channel: NotificationChannelExt.forId(map['channel']),
        payload: NotificationPayload.fromJson(map['payload']),
        when: map['when'] != null ? DateTime.fromMicrosecondsSinceEpoch(map['when']) : null,
      );

  Map<String, dynamic> toJson() => {
        'title': title,
        'body': body,
        'dateTime': dateTime.microsecondsSinceEpoch,
        'channel': channel.id,
        'payload': payload.toJson(),
        'when': when?.microsecondsSinceEpoch,
      };

  ScheduledNotification toScheduled({@required int id}) {
    return ScheduledNotification(
      id: id,
      title: title,
      body: body,
      dateTime: dateTime,
      channel: channel,
      payload: payload,
      when: when,
    );
  }

  @override
  List<Object> get props => [title, body, dateTime, channel, payload, when];
}
