import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';

/// Model entity representing a scheduled notification.
/// See [AddNotification] for more docs.
class ScheduledNotification extends Equatable {
  final int id;
  final String title;
  final String body;
  final DateTime dateTime;
  final DateTime when;
  final NotificationChannel channel;
  final NotificationPayload payload;

  const ScheduledNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.dateTime,
    @required this.channel,
    @required this.payload,
    this.when,
  });

  AddNotification toAddNotification() {
    return AddNotification(
      title: title,
      body: body,
      dateTime: dateTime,
      channel: channel,
      payload: payload,
      when: when,
    );
  }

  @override
  List<Object> get props => [id, title, body, dateTime, channel, payload, when];
}
