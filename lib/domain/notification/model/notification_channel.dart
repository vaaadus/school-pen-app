/// Lists all notifications channels related to local notifications.
enum NotificationChannel {
  note,
  exam,
  homework,
  reminder,
  agenda,
  upcomingLessons,
}

extension NotificationChannelExt on NotificationChannel {
  String get id {
    switch (this) {
      case NotificationChannel.note:
        return 'note';
      case NotificationChannel.exam:
        return 'exam';
      case NotificationChannel.homework:
        return 'homework';
      case NotificationChannel.reminder:
        return 'reminder';
      case NotificationChannel.agenda:
        return 'agenda';
      case NotificationChannel.upcomingLessons:
        return 'uupcomingLessons';
    }
    throw ArgumentError('Unsupported channel: $this');
  }

  /// Search the [NotificationChannel] by [id].
  static NotificationChannel forId(String id) {
    for (NotificationChannel channel in NotificationChannel.values) {
      if (channel.id == id) {
        return channel;
      }
    }
    return null;
  }
}
