import 'package:school_pen/domain/notification/local_notification_listener.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/scheduled_notification.dart';

import 'exception/permission_denied_exception.dart';
import 'model/add_notification.dart';

/// Manages local notifications.
abstract class NotificationManager {
  /// Whether notifications are supported.
  bool get isSupported;

  /// Must be called before any use.
  Future<void> initialize();

  void addNotificationListener(LocalNotificationListener listener);

  void removeNotificationListener(LocalNotificationListener listener);

  /// Returns all scheduled notifications on a [channel].
  Future<List<ScheduledNotification>> getAllScheduledOn(NotificationChannel channel);

  /// Cancels all notifications which are not specified in [notifications] on the [channel.
  /// Reschedules missing [notifications].
  Future<List<int>> scheduleAllOnChannel(NotificationChannel channel, List<AddNotification> notifications);

  /// Schedules a list of [notifications].
  Future<List<int>> scheduleAll(List<AddNotification> notifications);

  /// Schedules a notification, returns a [Future] with notification ID.
  /// Throws [PermissionDeniedException] if the permission was denied by the user.
  Future<int> scheduleNotification(AddNotification notification);

  /// Unschedules all notifications on a channel.
  Future<void> cancelAllOnChannel(NotificationChannel channel);

  /// Unschedules the [notifications].
  Future<void> cancelAll(List<int> notifications);

  /// Unschedules the notification by a given id.
  Future<void> cancelNotification(int id);
}
