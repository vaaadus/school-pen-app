import 'model/local_notification.dart';

abstract class LocalNotificationListener {
  /// Called when the app receives an event about opened local notification.
  void onLocalNotification(LocalNotification notification);
}
