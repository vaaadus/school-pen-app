import 'package:equatable/equatable.dart';

class PermissionDeniedException extends Equatable implements Exception {
  const PermissionDeniedException();

  @override
  List<Object> get props => [];
}
