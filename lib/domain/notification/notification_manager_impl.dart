import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/notification/local_notification_listener.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/scheduled_notification.dart';
import 'package:school_pen/domain/notification/repository/notification_history.dart';
import 'package:school_pen/domain/notification/repository/notification_storage.dart';
import 'package:synchronized/synchronized.dart';

import 'model/add_notification.dart';
import 'model/local_notification.dart';
import 'notification_manager.dart';
import 'repository/notification_repository.dart';

class NotificationManagerImpl implements NotificationManager, LifecycleListener {
  static const Logger _logger = Logger('NotificationManagerImpl');

  final Set<LocalNotificationListener> _listeners = {};
  final Lock _lock = Lock();
  final LifecycleManager _lifecycle;
  final NotificationRepository _repository;
  final NotificationHistory _history;
  final NotificationStorage _storage;
  final DateTimeProvider _dateTimeProvider;

  StreamSubscription _subscription;
  LocalNotification _lastNotification;

  NotificationManagerImpl(this._lifecycle, this._repository, this._history, this._storage, this._dateTimeProvider) {
    _lifecycle.addLifecycleListener(this);
  }

  @override
  bool get isSupported => !kIsWeb;

  @override
  Future<void> initialize() async {
    await _repository.initialize();
  }

  @override
  void onResume() {
    _subscription?.cancel();
    _subscription = _repository.notifications.listen(handleLocalNotification);
  }

  @override
  void onPause() {
    _lastNotification = null;
    _subscription?.cancel();
    _subscription = null;
  }

  @override
  void addNotificationListener(LocalNotificationListener listener) {
    _listeners.add(listener);
    if (_lastNotification != null) listener.onLocalNotification(_lastNotification);
  }

  @override
  void removeNotificationListener(LocalNotificationListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<List<ScheduledNotification>> getAllScheduledOn(NotificationChannel channel) {
    return _lock.synchronized(() => _storage.getAll(channel: channel));
  }

  @override
  Future<List<int>> scheduleAllOnChannel(NotificationChannel channel, List<AddNotification> nextList) {
    return _lock.synchronized(() async {
      nextList = _filterExpiredNotifications(nextList);

      final List<ScheduledNotification> previousList = await _storage.getAll(channel: channel);
      for (ScheduledNotification notification in _findDeletedNotifications(previousList, nextList)) {
        await _cancelNotification(notification.id);
      }

      final List<int> ids = [];
      for (AddNotification notification in _findNewNotifications(previousList, nextList)) {
        ids.add(await _scheduleNotification(notification));
      }
      return ids;
    });
  }

  @override
  Future<List<int>> scheduleAll(List<AddNotification> notifications) {
    return _lock.synchronized(() async {
      final List<int> ids = [];
      for (AddNotification notification in _filterExpiredNotifications(notifications)) {
        ids.add(await _scheduleNotification(notification));
      }
      return ids;
    });
  }

  @override
  Future<int> scheduleNotification(AddNotification notification) {
    return _lock.synchronized(() => _scheduleNotification(notification));
  }

  @override
  Future<void> cancelAllOnChannel(NotificationChannel channel) {
    return _lock.synchronized(() async {
      for (ScheduledNotification notification in await _storage.getAll(channel: channel)) {
        await _cancelNotification(notification.id);
      }
    });
  }

  @override
  Future<void> cancelAll(List<int> notifications) {
    return _lock.synchronized(() async {
      for (int id in notifications) {
        await _cancelNotification(id);
      }
    });
  }

  @override
  Future<void> cancelNotification(int id) {
    return _lock.synchronized(() => _cancelNotification(id));
  }

  Future<void> handleLocalNotification(LocalNotification notification) {
    return _lock.synchronized(() => _handleLocalNotification(notification));
  }

  Future<int> _scheduleNotification(AddNotification notification) async {
    _logger.log('scheduleNotification, notification: ${notification.title}');

    final int id = await _repository.scheduleNotification(notification);
    await _storage.store(notification.toScheduled(id: id));
    return id;
  }

  Future<void> _cancelNotification(int id) async {
    _logger.log('cancelNotification, id: $id');

    await _repository.cancelNotification(id);
    await _storage.drop(id);
  }

  Future<void> _handleLocalNotification(LocalNotification notification) async {
    _logger.log('handleLocalNotification: $notification');

    try {
      final bool consumed = await _history.consumeNotification(notification.id);
      if (!consumed) return;

      _dispatchLocalNotification(notification);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _lastNotification = notification;
  }

  List<AddNotification> _filterExpiredNotifications(List<AddNotification> notifications) {
    final DateTime now = _dateTimeProvider.now();
    return notifications.where((e) => e.dateTime.isAfter(now)).toList();
  }

  List<ScheduledNotification> _findDeletedNotifications(List<ScheduledNotification> previous, List<AddNotification> next) {
    return previous.where((e) => !next.contains(e.toAddNotification())).toList();
  }

  List<AddNotification> _findNewNotifications(List<ScheduledNotification> previous, List<AddNotification> next) {
    final List<AddNotification> list = previous.map((e) => e.toAddNotification()).toList();
    return next.where((e) => !list.contains(e)).toList();
  }

  void _dispatchLocalNotification(LocalNotification notification) {
    for (LocalNotificationListener listener in _listeners) {
      listener.onLocalNotification(notification);
    }
  }
}
