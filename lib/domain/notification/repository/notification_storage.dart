import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/scheduled_notification.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// Stores scheduled notifications to avoid scheduling them twice.
class NotificationStorage {
  static final StoreRef<int, Map<String, dynamic>> _store = intMapStoreFactory.store('notification_storage');
  final Lock _lock = Lock();

  Future<List<ScheduledNotification>> getAll({@required NotificationChannel channel}) {
    return _lock.synchronized(() async {
      final Finder finder = Finder(filter: Filter.equals('channel', channel.id));
      final List<RecordSnapshot<int, Map<String, dynamic>>> records = await _store.find(await _db, finder: finder);
      return records.map((e) => AddNotification.fromJson(e.value).toScheduled(id: e.key)).toList();
    });
  }

  Future<void> store(ScheduledNotification notification) {
    return _lock.synchronized(() async {
      if (notification == null) return;

      final Map<String, dynamic> value = notification?.toAddNotification()?.toJson();
      await _store.record(notification.id).put(await _db, value);
    });
  }

  Future<void> drop(int notificationId) {
    return _lock.synchronized(() async {
      if (notificationId == null) return;

      await _store.record(notificationId).delete(await _db);
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}
