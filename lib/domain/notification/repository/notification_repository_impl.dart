import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/notification/exception/permission_denied_exception.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/local_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

import 'notification_channel_locator.dart';
import 'notification_repository.dart';

class NotificationRepositoryImpl implements NotificationRepository {
  static const String keyLastId = 'key_localNotification_lastId';
  static const int firstId = 1000;

  final FlutterLocalNotificationsPlugin _plugin = FlutterLocalNotificationsPlugin();
  final BehaviorSubject<LocalNotification> _notifications = BehaviorSubject();
  final NotificationChannelLocator _channelLocator;
  final Preferences _preferences;

  NotificationRepositoryImpl(this._channelLocator, this._preferences);

  /// The stream with notifications invoked by the user.
  @override
  Stream<LocalNotification> get notifications => _notifications.stream;

  @override
  Future<void> initialize() async {
    tz.initializeTimeZones();

    final androidSettings = AndroidInitializationSettings(_channelLocator.getDefaultIcon());

    final iosSettings = IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
    );

    final macOSSettings = MacOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
    );

    final initializationSettings = InitializationSettings(
      android: androidSettings,
      iOS: iosSettings,
      macOS: macOSSettings,
    );

    await _plugin.initialize(initializationSettings, onSelectNotification: _onNotificationSelected);
    final NotificationAppLaunchDetails details = await _plugin.getNotificationAppLaunchDetails();
    if (Strings.isNotBlank(details?.payload)) {
      await _onNotificationSelected(details.payload);
    }
  }

  @override
  Future<int> scheduleNotification(AddNotification notification) async {
    final notificationsAllowed = await _requestPermissions();
    if (!notificationsAllowed) {
      throw PermissionDeniedException();
    }

    final int id = _incrementAndGetId();

    await _plugin.zonedSchedule(
      id,
      notification.title,
      notification.body,
      tz.TZDateTime.from(notification.dateTime, tz.local),
      NotificationDetails(
        android: _createAndroidNotificationDetails(notification),
        iOS: _createIosNotificationsDetails(notification),
      ),
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.wallClockTime,
      androidAllowWhileIdle: true,
      payload: _DecodablePayload.fromNotification(id, notification).encode(),
    );

    return id;
  }

  @override
  Future<void> cancelNotification(int id) {
    return _plugin.cancel(id);
  }

  Future<bool> _requestPermissions() async {
    if (Platform.isIOS) {
      final iosPlugin = _plugin.resolvePlatformSpecificImplementation<IOSFlutterLocalNotificationsPlugin>();
      return await iosPlugin.requestPermissions(
        alert: true,
        badge: true,
        sound: true,
      );
    } else {
      return true;
    }
  }

  int _incrementAndGetId() {
    final currentId = _preferences.getInt(keyLastId) ?? firstId;
    final nextId = currentId + 1;
    _preferences.setInt(keyLastId, nextId);
    return nextId;
  }

  AndroidNotificationDetails _createAndroidNotificationDetails(AddNotification notification) {
    return AndroidNotificationDetails(
      notification.channel.id,
      _channelLocator.getName(notification.channel),
      _channelLocator.getDescription(notification.channel),
      importance: _channelLocator.getImportance(notification.channel),
      priority: _channelLocator.getPriority(notification.channel),
      showWhen: _channelLocator.getShowWhen(notification.channel) && notification.when != null,
      when: notification.when?.millisecondsSinceEpoch,
      channelShowBadge: _channelLocator.getShowBadge(notification.channel),
      enableLights: _channelLocator.getEnableLights(notification.channel),
      ticker: notification.title,
    );
  }

  IOSNotificationDetails _createIosNotificationsDetails(AddNotification notification) {
    return IOSNotificationDetails(
      presentBadge: _channelLocator.getShowBadge(notification.channel),
    );
  }

  Future<void> _onNotificationSelected(String decodedPayload) async {
    final _DecodablePayload decoded = _DecodablePayload.decode(decodedPayload);
    if (decoded != null) {
      _notifications.add(decoded.notification);
    }
  }
}

/// A util to encode/decode payload of the notification
/// to be able later to handle it when notification is clicked.
class _DecodablePayload {
  static const Logger _logger = Logger('_DecodablePayload');

  final int id;
  final NotificationChannel channel;
  final String objectId;

  _DecodablePayload({
    @required this.id,
    @required this.channel,
    this.objectId,
  }) {
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(channel, 'channel');
  }

  factory _DecodablePayload.fromNotification(int id, AddNotification notification) {
    return _DecodablePayload(
      id: id,
      channel: notification.channel,
      objectId: notification.payload.objectId,
    );
  }

  /// Encode this payload to a string.
  String encode() {
    return json.encode({
      'id': id,
      'channelId': channel.id,
      'objectId': objectId,
    });
  }

  /// Decode [_DecodablePayload] from a string. Returns null if cannot be decoded.
  static _DecodablePayload decode(String encoded) {
    try {
      final Map<String, dynamic> map = json.decode(encoded);

      return _DecodablePayload(
        id: map['id'],
        channel: NotificationChannelExt.forId(map['channelId']),
        objectId: map['objectId'],
      );
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      return null;
    }
  }

  LocalNotification get notification {
    return LocalNotification(
      id: id,
      channel: channel,
      payload: payload,
    );
  }

  NotificationPayload get payload {
    return NotificationPayload(objectId: objectId);
  }
}
