import 'package:school_pen/domain/notification/exception/permission_denied_exception.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/local_notification.dart';

/// Acts as a proxy to system related services,
/// therefore abstracting from it.
abstract class NotificationRepository {
  /// Must be called before any use.
  Future<void> initialize();

  /// Returns the stream with notifications selected by the user.
  Stream<LocalNotification> get notifications;

  /// Schedules a notification and returns a [Future] which ends with notification ID.
  /// Throws [PermissionDeniedException] if the permission was denied by the user.
  Future<int> scheduleNotification(AddNotification notification);

  /// Unschedules a notification by ID.
  Future<void> cancelNotification(int id);
}
