import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// Keeps track of already consumed notifications
/// to avoid consuming the same notification twice.
class NotificationHistory {
  static final StoreRef<int, Map<String, dynamic>> _store = intMapStoreFactory.store('local_notifications_history');
  final Lock _lock = Lock();

  /// Returns true if a notification with [id] was never consumed, false otherwise.
  /// If the result was true, next calls will return false given the same [id].
  Future<bool> consumeNotification(int id) async {
    if (id == null) return false;

    return _lock.synchronized(() async {
      final bool consumed = SembastSerializer.deserializeBool(await _store.record(id).get(await _db)) ?? false;
      if (!consumed) await _store.record(id).put(await _db, SembastSerializer.serializeBool(true));

      return !consumed;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}
