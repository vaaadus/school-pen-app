import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/local_notification.dart';
import 'package:school_pen/domain/notification/repository/notification_repository.dart';

/// No-op implementation.
class EmptyNotificationRepository implements NotificationRepository {
  final PublishSubject<LocalNotification> _notifications = PublishSubject();

  @override
  Future<void> cancelNotification(int id) async {}

  @override
  Future<void> initialize() async {}

  @override
  Stream<LocalNotification> get notifications => _notifications;

  @override
  Future<int> scheduleNotification(AddNotification notification) async {
    return -1;
  }
}
