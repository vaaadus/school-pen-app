import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/generated/l10n.dart';

/// Locates resources for [NotificationChannel].
class NotificationChannelLocator {
  /// The default icon for all notifications.
  String getDefaultIcon() {
    return 'ic_notification';
  }

  /// Returns the human readable name of the channel.
  String getName(NotificationChannel channel) {
    switch (channel) {
      case NotificationChannel.note:
        return S().notificationChannel_note_title;
      case NotificationChannel.exam:
        return S().notificationChannel_exam_title;
      case NotificationChannel.homework:
        return S().notificationChannel_homework_title;
      case NotificationChannel.reminder:
        return S().notificationChannel_reminder_title;
      case NotificationChannel.agenda:
        return S().notificationChannel_agenda_title;
      case NotificationChannel.upcomingLessons:
        return S().notificationChannel_upcomingLessons_title;
    }
    throw ArgumentError('Not supported channel: $channel');
  }

  /// Returns the human readable description of the channel.
  String getDescription(NotificationChannel channel) {
    switch (channel) {
      case NotificationChannel.note:
      case NotificationChannel.exam:
      case NotificationChannel.homework:
      case NotificationChannel.reminder:
      case NotificationChannel.agenda:
      case NotificationChannel.upcomingLessons:
        return '';
    }
    throw ArgumentError('Not supported channel: $channel');
  }

  /// The importance of the channel. Should be similar to the priority.
  Importance getImportance(NotificationChannel channel) {
    switch (channel) {
      case NotificationChannel.note:
      case NotificationChannel.exam:
      case NotificationChannel.homework:
      case NotificationChannel.reminder:
      case NotificationChannel.agenda:
      case NotificationChannel.upcomingLessons:
        return Importance.high;
    }
    throw ArgumentError('Not supported channel: $channel');
  }

  /// The priority of the channel. Should be similar to the importance.
  Priority getPriority(NotificationChannel channel) {
    switch (channel) {
      case NotificationChannel.note:
      case NotificationChannel.exam:
      case NotificationChannel.homework:
      case NotificationChannel.reminder:
      case NotificationChannel.agenda:
      case NotificationChannel.upcomingLessons:
        return Priority.high;
    }
    throw ArgumentError('Not supported channel: $channel');
  }

  /// Returns bool whether to enable 'showWhen' flag.
  bool getShowWhen(NotificationChannel channel) {
    switch (channel) {
      case NotificationChannel.note:
      case NotificationChannel.exam:
      case NotificationChannel.homework:
      case NotificationChannel.reminder:
      case NotificationChannel.upcomingLessons:
        return true;
      case NotificationChannel.agenda:
        return false;
    }
    throw ArgumentError('Not supported channel: $channel');
  }

  /// Returns whether to show the badge.
  bool getShowBadge(NotificationChannel channel) {
    switch (channel) {
      case NotificationChannel.note:
      case NotificationChannel.exam:
      case NotificationChannel.homework:
      case NotificationChannel.reminder:
        return true;
      case NotificationChannel.agenda:
      case NotificationChannel.upcomingLessons:
        return false;
    }
    throw ArgumentError('Not supported channel: $channel');
  }

  /// Returns whether to enable lights (LED).
  bool getEnableLights(NotificationChannel channel) {
    switch (channel) {
      case NotificationChannel.note:
      case NotificationChannel.exam:
      case NotificationChannel.homework:
      case NotificationChannel.reminder:
      case NotificationChannel.agenda:
      case NotificationChannel.upcomingLessons:
        return true;
    }
    throw ArgumentError('Not supported channel: $channel');
  }
}
