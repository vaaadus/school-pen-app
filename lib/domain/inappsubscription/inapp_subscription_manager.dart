import 'package:school_pen/domain/inappsubscription/inapp_subscription_listener.dart';
import 'package:school_pen/domain/inappsubscription/model/inapp_subscribed_plan.dart';

import 'model/inapp_subscription_offer.dart';

abstract class InAppSubscriptionManager {
  /// Whether the payment platform is ready & enabled.
  Future<bool> get isAvailable;

  /// Whether the user has bought the subscription and it is active.
  Future<bool> get isSubscribed;

  /// Cached [isSubscribed] flag or false if nothing cached.
  bool get isSubscriptionActive;

  /// Whether the user has any started purchases which require an action.
  Future<bool> get hasPendingPurchases;

  /// Returns the subscribed (active) plan or null.
  Future<InAppSubscribedPlan> get subscribedPlan;

  /// Get the subscriptions offered in the store.
  Future<List<InAppSubscriptionOffer>> get availableSubscriptions;

  /// Start the billing flow to buy the subscription.
  Future<void> buySubscription(InAppSubscriptionOffer offer);

  /// Register a listener for upcoming subscription changes.
  void addInAppSubscriptionListener(InAppSubscriptionListener listener);

  /// Unregister the listener from subscription changes.
  void removeInAppSubscriptionListener(InAppSubscriptionListener listener);
}
