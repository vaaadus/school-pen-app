/// Lists all supported subscription plans.
enum InAppSubscriptionPlan {
  premiumMonthly,
  premiumYearly,
}

extension InAppSubscriptionPlanExt on InAppSubscriptionPlan {
  /// The identifier for the subscription plan.
  String get tag {
    switch (this) {
      case InAppSubscriptionPlan.premiumMonthly:
        return 'premium_monthly_subscription';
      case InAppSubscriptionPlan.premiumYearly:
        return 'premium_yearly_subscription';
    }
    throw ArgumentError('Not supported subscription plan: $this');
  }

  static InAppSubscriptionPlan forTag(String tag) {
    for (var plan in InAppSubscriptionPlan.values) {
      if (plan.tag.toLowerCase() == tag.toLowerCase()) {
        return plan;
      }
    }
    throw ArgumentError('Not supported subscription plan: $tag');
  }
}
