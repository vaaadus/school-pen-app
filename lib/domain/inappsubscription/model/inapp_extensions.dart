import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/domain/common/extensions/math_ext.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/math.dart';

import 'inapp_subscription_plan.dart';

extension PurchaseDetailsExt on PurchaseDetails {
  /// Returns a string representation of [PurchaseDetails] without blank fields.
  String toStringWithoutNulls() {
    String str = '';

    if (Strings.isNotBlank(productID)) str += ', productId=${productID}';
    if (Strings.isNotBlank(purchaseID)) str += ', purchaseId=${purchaseID}';
    if (Strings.isNotBlank(transactionDate)) str += ', transactionDate=${transactionDate}';
    if (status != null) str += ', status=${status}';

    if (error != null) {
      str += ', error[';
      if (Strings.isNotBlank(error.message)) str += ', message=${error.message}';
      if (Strings.isNotBlank(error.details)) str += ', details=${error.details}';
      if (Strings.isNotBlank(error.code)) str += ', code=${error.code}';
      str += ']';
    }
    return str;
  }
}

extension ProductDetailsExt on ProductDetails {
  /// The monthly unit price calculated from total price.
  double get monthlyPrice {
    var plan = InAppSubscriptionPlanExt.forTag(id);
    switch (plan) {
      case InAppSubscriptionPlan.premiumMonthly:
        return roundToTwoDecimalPlaces(totalPrice);
      case InAppSubscriptionPlan.premiumYearly:
        return roundToTwoDecimalPlaces(totalPrice / 12.0);
    }
    throw ArgumentError('Not supported subscription plan: $plan');
  }

  String get formattedMonthlyPrice {
    var symbol = NumberFormat.currency().simpleCurrencySymbol(currencyCode);
    return NumberFormat.currency(symbol: symbol, decimalDigits: 2).format(monthlyPrice);
  }

  /// The total price for the product.
  double get totalPrice {
    if (skuDetail != null) {
      return roundToTwoDecimalPlaces(skuDetail.priceAmountMicros / 1000000.0);
    }
    if (skProduct != null) {
      return roundToTwoDecimalPlaces(tryParseDouble(skProduct.price) ?? 0);
    }

    throw ArgumentError('Neither SkuDetail nor SkProduct present');
  }

  /// The currency code for the locale, e.g. USD for US locale.
  String get currencyCode {
    if (skuDetail != null) {
      return skuDetail.priceCurrencyCode;
    }
    if (skProduct != null) {
      return skProduct.priceLocale.currencyCode;
    }

    throw ArgumentError('Neither SkuDetail nor SkProduct present');
  }

  /// The short current code like '$' or '€'.
  String get currencySymbol {
    return NumberFormat.currency().simpleCurrencySymbol(currencyCode);
  }
}
