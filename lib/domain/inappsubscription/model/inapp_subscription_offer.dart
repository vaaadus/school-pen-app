import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'inapp_subscription_plan.dart';

/// A subscription user can buy to access premium content.
class InAppSubscriptionOffer extends Equatable {
  /// Human readable short title of the offer.
  final String title;

  /// Human readable description of the offer.
  final String description;

  /// Formatted with currency, total price for the plan.
  final String formattedPrice;

  /// The subscription plan, specifies the product ID as well as the duration.
  final InAppSubscriptionPlan plan;

  /// Formatted with currency, monthly price for the plan.
  final String formattedMonthlyPrice;

  /// Monthly price for the plan.
  final double monthlyPrice;

  /// Savings how much user saves when buying this offer instead the most expensive one.
  /// A double value between 0.0 ... 1.0 or null if no savings.
  final double savings;

  /// Store implementation dependent object.
  final Object customData;

  const InAppSubscriptionOffer({
    @required this.title,
    @required this.description,
    @required this.formattedPrice,
    @required this.plan,
    @required this.formattedMonthlyPrice,
    @required this.monthlyPrice,
    @required this.savings,
    @required this.customData,
  });

  @override
  List<Object> get props => [
        title,
        description,
        formattedMonthlyPrice,
        plan,
        formattedMonthlyPrice,
        monthlyPrice,
        savings,
        customData,
      ];
}
