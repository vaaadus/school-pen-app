import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:school_pen/domain/inappsubscription/model/inapp_subscription_plan.dart';

/// A subscription plan that is subscribed & active.
class InAppSubscribedPlan extends Equatable {
  final InAppSubscriptionPlan plan;
  final String title;
  final String description;

  const InAppSubscribedPlan({
    @required this.plan,
    @required this.title,
    @required this.description,
  });

  @override
  List<Object> get props => [plan, title, description];
}
