import 'dart:async';

import 'package:school_pen/domain/inappsubscription/inapp_subscription_listener.dart';
import 'package:school_pen/domain/inappsubscription/model/inapp_subscribed_plan.dart';

import 'inapp_subscription_manager.dart';
import 'model/inapp_subscription_offer.dart';

class EmptyInAppSubscriptionManager implements InAppSubscriptionManager {
  @override
  Future<bool> get isAvailable async {
    return false;
  }

  @override
  Future<bool> get isSubscribed async {
    return false;
  }

  @override
  bool get isSubscriptionActive {
    return false;
  }

  @override
  Future<bool> get hasPendingPurchases async {
    return false;
  }

  @override
  Future<InAppSubscribedPlan> get subscribedPlan async {
    return null;
  }

  @override
  Future<List<InAppSubscriptionOffer>> get availableSubscriptions async {
    return [];
  }

  @override
  Future<void> buySubscription(InAppSubscriptionOffer offer) async {}

  @override
  void addInAppSubscriptionListener(InAppSubscriptionListener listener) {}

  @override
  void removeInAppSubscriptionListener(InAppSubscriptionListener listener) {}
}
