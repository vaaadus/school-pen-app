import 'package:school_pen/domain/inappsubscription/inapp_subscription_manager.dart';

abstract class InAppSubscriptionListener {
  /// Called whenever an in app bought subscriptions have changed.
  void onInAppSubscriptionsChanged(InAppSubscriptionManager manager);
}
