import 'dart:async';

import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:pedantic/pedantic.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/inappsubscription/inapp_product_offer_mapper.dart';
import 'package:school_pen/domain/inappsubscription/inapp_subscription_listener.dart';
import 'package:school_pen/domain/inappsubscription/model/inapp_subscribed_plan.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/preferences/preferences.dart';

import 'inapp_subscription_manager.dart';
import 'model/inapp_extensions.dart';
import 'model/inapp_subscription_offer.dart';
import 'model/inapp_subscription_plan.dart';

class InAppSubscriptionManagerImpl implements InAppSubscriptionManager, LifecycleListener {
  static const String keySubscriptionActive = 'inApp_subscription_active';
  static const Duration buyTimeout = Duration(seconds: 60);
  static const Logger _logger = Logger('InAppSubscriptionManager');

  final PublishSubject<List<PurchaseDetails>> _purchasesStream = PublishSubject();
  final Set<InAppSubscriptionListener> _listeners = {};
  final InAppPurchaseConnection _store;
  final InAppProductOfferMapper _mapper;
  final LifecycleManager _lifecycle;
  final Preferences _preferences;

  InAppSubscriptionManagerImpl(
    this._store,
    this._mapper,
    this._lifecycle,
    this._preferences,
  ) {
    _store.purchaseUpdatedStream.listen((List<PurchaseDetails> list) async {
      _logPurchaseEvents(list);
      _purchasesStream.add(list);
      unawaited(_completeAnyPendingPurchases(list));
      unawaited(_cacheSubscriptionActiveFlag());
      _notifyInAppSubscriptionListeners(list);
    });

    _lifecycle.addLifecycleListener(this);
  }

  @override
  Future<bool> get isAvailable {
    return _store.isAvailable();
  }

  @override
  Future<bool> get isSubscribed async {
    return await subscribedPlan != null;
  }

  @override
  bool get isSubscriptionActive {
    return _preferences.getBool(keySubscriptionActive) ?? false;
  }

  @override
  Future<bool> get hasPendingPurchases async {
    if (!await isAvailable) return false;

    var purchases = await _store.queryPastPurchases();
    return purchases.pastPurchases.any((it) => it.status == PurchaseStatus.pending);
  }

  @override
  Future<InAppSubscribedPlan> get subscribedPlan async {
    if (!await isAvailable) {
      _storeSubscriptionFlagAsActive(false);
      return null;
    }

    var purchases = await _store.queryPastPurchases();
    var purchase = purchases.pastPurchases.firstWhereOrNull(_isPurchaseValid);

    if (purchase == null) {
      _storeSubscriptionFlagAsActive(false);
      return null;
    }

    var products = await _store.queryProductDetails({purchase.productID});
    var product = products.productDetails.firstOrNull;

    if (product == null) {
      _storeSubscriptionFlagAsActive(false);
      return null;
    }

    var plan = InAppSubscribedPlan(
      plan: InAppSubscriptionPlanExt.forTag(purchase.productID),
      title: product.title,
      description: product.description,
    );

    _storeSubscriptionFlagAsActive(plan != null);

    return plan;
  }

  @override
  Future<List<InAppSubscriptionOffer>> get availableSubscriptions async {
    if (!await isAvailable) return [];

    List<InAppSubscriptionOffer> offers = [];
    var response = await _store.queryProductDetails(_supportedProducts);
    var allProducts = response.productDetails;

    for (var product in allProducts) {
      offers.add(_mapper.asOffer(allProducts, product));
    }

    return offers;
  }

  @override
  Future<void> buySubscription(InAppSubscriptionOffer offer) async {
    var completer = Completer<bool>();
    var product = _mapper.asProduct(offer);

    StreamSubscription subscription;

    subscription = _purchasesStream.listen((List<PurchaseDetails> list) {
      for (var details in list) {
        if (details.status == PurchaseStatus.purchased) {
          completer.complete();
          subscription.cancel();
        } else if (details.status == PurchaseStatus.error) {
          completer.completeError(Exception(details.error.message));
          subscription.cancel();
        }
      }
    });

    try {
      await _store.buyNonConsumable(purchaseParam: PurchaseParam(productDetails: product));
    } catch (error) {
      completer.completeError(error);
      unawaited(subscription.cancel());
    }

    return completer.future.timeout(buyTimeout, onTimeout: () {
      subscription.cancel();
      throw TimeoutException('The purchase timed out');
    });
  }

  @override
  void addInAppSubscriptionListener(InAppSubscriptionListener listener) {
    _listeners.add(listener);
  }

  @override
  void removeInAppSubscriptionListener(InAppSubscriptionListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<void> onResume() async {
    await _queryAndCompleteAnyPendingPurchases();
    await _cacheSubscriptionActiveFlag();
  }

  @override
  void onPause() {
    // do nothing
  }

  Set<String> get _supportedProducts {
    return InAppSubscriptionPlan.values.map((e) => e.tag).toSet();
  }

  bool _isPurchaseValid(PurchaseDetails details) {
    return details != null &&
        details.status == PurchaseStatus.purchased &&
        _supportedProducts.contains(details.productID);
  }

  void _logPurchaseEvents(List<PurchaseDetails> list) {
    for (var details in list) {
      _logger.log('onPurchaseUpdated: ' + details.toStringWithoutNulls());
    }
  }

  void _notifyInAppSubscriptionListeners(List<PurchaseDetails> list) {
    for (var listener in _listeners) {
      listener.onInAppSubscriptionsChanged(this);
    }
  }

  Future<void> _queryAndCompleteAnyPendingPurchases() async {
    if (!await isAvailable) return;

    try {
      var result = await _store.queryPastPurchases();
      await _completeAnyPendingPurchases(result.pastPurchases);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<void> _completeAnyPendingPurchases(List<PurchaseDetails> list) async {
    try {
      for (var details in list) {
        if (details.pendingCompletePurchase) {
          _logger.log('completing purchase: ${details.productID}');
          await _store.completePurchase(details);
        }
      }
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<void> _cacheSubscriptionActiveFlag() async {
    try {
      await isSubscribed;
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  void _storeSubscriptionFlagAsActive(bool active) {
    _preferences.setBool(keySubscriptionActive, active);
  }
}
