import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:school_pen/domain/common/extensions/math_ext.dart';
import 'package:school_pen/domain/inappsubscription/model/inapp_subscription_offer.dart';

import 'model/inapp_extensions.dart';
import 'model/inapp_subscription_plan.dart';

class InAppProductOfferMapper {
  /// Maps the [InAppSubscriptionOffer] to [ProductDetails].
  ProductDetails asProduct(InAppSubscriptionOffer offer) {
    return offer.customData as ProductDetails;
  }

  /// Maps the [ProductDetails] to [InAppSubscriptionOffer].
  InAppSubscriptionOffer asOffer(List<ProductDetails> all, ProductDetails product) {
    return InAppSubscriptionOffer(
      title: product.title,
      description: product.description,
      formattedPrice: product.price,
      plan: InAppSubscriptionPlanExt.forTag(product.id),
      formattedMonthlyPrice: product.formattedMonthlyPrice,
      monthlyPrice: product.monthlyPrice,
      savings: _calculateSavings(all, product),
      customData: product,
    );
  }

  double _calculateSavings(List<ProductDetails> allProducts, ProductDetails product) {
    var mostExpensive = _getMostExpensiveProduct(allProducts);
    if (product.monthlyPrice >= mostExpensive.monthlyPrice) {
      return null;
    } else {
      var saved = mostExpensive.monthlyPrice - product.monthlyPrice;
      return roundToTwoDecimalPlaces(saved / mostExpensive.monthlyPrice);
    }
  }

  ProductDetails _getMostExpensiveProduct(List<ProductDetails> products) {
    var mostExpensive = products.first;
    for (var product in products) {
      if (product.monthlyPrice > mostExpensive.monthlyPrice) {
        mostExpensive = product;
      }
    }
    return mostExpensive;
  }
}
