import 'model/user.dart';

abstract class UserListener {
  /// Called whenever used object changes.
  void onUserChanged(User user);
}
