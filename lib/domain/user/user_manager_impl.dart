import 'dart:convert';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/files.dart';
import 'package:school_pen/domain/common/image_resizer.dart';
import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/user/exception/avatar_size_too_big_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_email_exception.dart';
import 'package:school_pen/domain/user/exception/password_cannot_contain_username_exception.dart';
import 'package:school_pen/domain/user/exception/password_too_short_exception.dart';
import 'package:school_pen/domain/user/exception/username_too_short_exception.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/domain/user/model/auth_result.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/model/user_meta.dart';
import 'package:school_pen/domain/user/repository/user_repository.dart';
import 'package:school_pen/domain/user/user_listener.dart';
import 'package:school_pen/domain/user/user_login_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';

class UserManagerImpl implements UserManager, LifecycleListener {
  static const Logger _logger = Logger('UserManagerImpl');

  static const String key_storedUserMetadata = 'key_auth_storedUserMetadata';
  static const String key_refreshUser_slidingWindow = 'key_auth_refreshUser_window';
  static const Duration refreshUserDuration = Duration(days: 1);
  static const int minUsernameLength = 3;
  static const int minPasswordLength = 8;
  static const int avatarSize = 256;
  static const double avatarFileSizeLimitInMB = 5.0;

  final Set<UserLoginListener> _loginListeners = {};
  final Set<UserListener> _userListeners = {};
  final UserRepository _repository;
  final Preferences _preferences;
  final LifecycleManager _lifecycle;
  final SlidingWindow _slidingWindow;
  final ImageResizer _imageResizer;

  UserManagerImpl(this._repository, this._preferences, this._lifecycle, this._slidingWindow, this._imageResizer) {
    _lifecycle.addLifecycleListener(this);
  }

  @override
  void addUserLoginListener(UserLoginListener listener) {
    _loginListeners.add(listener);
  }

  @override
  void removeUserLoginListener(UserLoginListener listener) {
    _loginListeners.remove(listener);
  }

  @override
  void addUserListener(UserListener listener, {bool notifyOnAttach = true}) async {
    _userListeners.add(listener);

    if (notifyOnAttach) {
      try {
        final User user = await _repository.getCurrentUser();
        if (_userListeners.contains(listener)) {
          listener.onUserChanged(user);
        }
      } catch (error, stackTrace) {
        _logger.logError(error, stackTrace);
      }
    }
  }

  @override
  void removeUserListener(UserListener listener) {
    _userListeners.remove(listener);
  }

  @override
  Future<User> getCurrentUser() {
    return _repository.getCurrentUser();
  }

  @override
  Future<User> refresh() {
    return _refresh();
  }

  @override
  Future<void> signUp({@required String username, @required String email, @required String password}) async {
    _validateUsername(username);
    _validateEmail(email);
    _validatePassword(password, username);
    await _repository.signUp(username: username, email: email, password: password);
    await _refresh();
  }

  @override
  Future<void> signIn({@required String email, @required String password}) async {
    _validateEmail(email);
    await _repository.signIn(email: email, password: password);
    await _refresh();
  }

  @override
  Future<void> signInWith(AuthResult result) async {
    await _repository.signInWith(provider: result.provider, authData: result.authData);
    final User user = await _repository.refreshCurrentUser();

    if (Strings.isBlank(user.email)) {
      await _repository.editProfile(email: result.email);
    }

    await _refresh();
  }

  @override
  Future<void> linkWith(AuthResult result) async {
    await _repository.linkWith(provider: result.provider, authData: result.authData);
    await _refresh();
  }

  @override
  Future<void> unlinkFrom(AuthProvider provider) async {
    await _repository.unlinkFrom(provider);
    await _refresh();
  }

  @override
  Future<void> signOut() async {
    await _repository.signOut();
    await _refresh();
  }

  @override
  Future<void> editProfile({@required String username, @required String email}) async {
    _validateUsername(username);
    _validateEmail(email);

    await _repository.editProfile(username: username, email: email);
    await _refresh();
  }

  @override
  Future<void> editAvatar({MemoryFile file}) async {
    if (file != null) {
      if (file.bytes.length > Files.megabytesToBytes(avatarFileSizeLimitInMB)) throw AvatarSizeTooBigException();
      file = await _imageResizer.cropSquareAndResize(file, avatarSize);
    }

    await _repository.editAvatar(file: file);
    await _refresh();
  }

  @override
  Future<void> changePassword({@required String oldPassword, @required String newPassword}) async {
    _validatePassword(newPassword, null);
    await _repository.changePassword(oldPassword: oldPassword, newPassword: newPassword);
    await _refresh();
  }

  @override
  Future<void> deleteUser() async {
    await _repository.deleteUser();
    await _refresh();
  }

  @override
  Future<void> requestPasswordReset({@required String email}) async {
    _validateEmail(email);
    return _repository.requestPasswordReset(email: email);
  }

  @override
  Future<List<AuthProvider>> getAvailableAuthProviders() {
    return _repository.getAvailableAuthProviders();
  }

  @override
  Future<bool> consumeAuthError(dynamic error) async {
    if (_repository.isSessionInvalidError(error)) {
      await _refresh();
      return true;
    }

    return false;
  }

  @override
  Future<void> onResume() async {
    final bool canRefresh = _slidingWindow.isPassed(
      key: key_refreshUser_slidingWindow,
      nextWindow: refreshUserDuration,
    );

    if (!canRefresh) return;

    try {
      await _refresh();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  @override
  void onPause() {
    // do nothing
  }

  Future<User> _refresh() async {
    final User user = await _repository.refreshCurrentUser();
    final UserMetadata meta = user?.toMetadata();
    final UserMetadata previousMeta = _getStoredUserMetadata();

    _storeUserMetadata(meta);
    _slidingWindow.restart(key: key_refreshUser_slidingWindow);

    if (meta == null && previousMeta != null) {
      _notifyUserSignedOut(previousMeta);
    } else if (meta != null && previousMeta == null) {
      _notifyUserSignedIn(meta);
    }

    if (meta != previousMeta) {
      _notifyUserChanged(user);
    }

    return user;
  }

  void _storeUserMetadata(UserMetadata metadata) {
    if (metadata == null) {
      _preferences.clear(key_storedUserMetadata);
    } else {
      _preferences.setString(key_storedUserMetadata, json.encode(metadata.toJson()));
    }
  }

  UserMetadata _getStoredUserMetadata() {
    final String string = _preferences.getString(key_storedUserMetadata);
    if (string == null) return null;
    return UserMetadata.fromJson(json.decode(string));
  }

  void _validateUsername(String username) {
    if (username.length < minUsernameLength) throw UsernameTooShortException(minUsernameLength);
  }

  void _validateEmail(String email) {
    if (!EmailValidator.validate(email)) throw InvalidEmailException(email);
  }

  void _validatePassword(String password, String username) {
    if (username != null && password.contains(RegExp(username))) throw PasswordCannotContainUsernameException();
    if (password.length < minPasswordLength) throw PasswordTooShortException(minPasswordLength);
  }

  void _notifyUserSignedIn(UserMetadata user) {
    for (final listener in _loginListeners) {
      listener.onUserSignedIn(user);
    }
  }

  void _notifyUserSignedOut(UserMetadata user) {
    for (final listener in _loginListeners) {
      listener.onUserSignedOut(user);
    }
  }

  void _notifyUserChanged(User user) {
    for (final listener in _userListeners) {
      listener.onUserChanged(user);
    }
  }
}
