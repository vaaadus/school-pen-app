import 'package:school_pen/domain/user/model/user_meta.dart';

abstract class UserLoginListener {
  /// Called whenever the user signs in on this device.
  void onUserSignedIn(UserMetadata user);

  /// Called whenever the user is signed out from this device.
  void onUserSignedOut(UserMetadata user);
}
