import 'package:equatable/equatable.dart';

class PasswordsDoNotMatchException extends Equatable implements Exception {
  const PasswordsDoNotMatchException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
