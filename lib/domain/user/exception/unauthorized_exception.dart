import 'package:equatable/equatable.dart';

class UnauthorizedException extends Equatable implements Exception {
  const UnauthorizedException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
