import 'package:equatable/equatable.dart';

class UsernameTooShortException extends Equatable implements Exception {
  const UsernameTooShortException(this.minLength, [this.message]);

  final int minLength;
  final String message;

  @override
  List<Object> get props => [minLength, message];
}
