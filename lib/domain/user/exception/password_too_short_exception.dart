import 'package:equatable/equatable.dart';

class PasswordTooShortException extends Equatable implements Exception {
  const PasswordTooShortException(this.minLength, [this.message]);

  final int minLength;
  final String message;

  @override
  List<Object> get props => [minLength, message];
}
