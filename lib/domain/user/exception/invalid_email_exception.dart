import 'package:equatable/equatable.dart';

class InvalidEmailException extends Equatable implements Exception {
  const InvalidEmailException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
