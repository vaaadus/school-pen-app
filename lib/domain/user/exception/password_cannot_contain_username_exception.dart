import 'package:equatable/equatable.dart';

class PasswordCannotContainUsernameException extends Equatable implements Exception {
  const PasswordCannotContainUsernameException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
