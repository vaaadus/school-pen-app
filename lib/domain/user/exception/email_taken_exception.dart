import 'package:equatable/equatable.dart';

class EmailTakenException extends Equatable implements Exception {
  const EmailTakenException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
