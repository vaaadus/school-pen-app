import 'package:equatable/equatable.dart';

class UsernameTakenException extends Equatable implements Exception {
  const UsernameTakenException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
