import 'package:equatable/equatable.dart';

class TooManyFailedLoginAttemptsException extends Equatable implements Exception {
  const TooManyFailedLoginAttemptsException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
