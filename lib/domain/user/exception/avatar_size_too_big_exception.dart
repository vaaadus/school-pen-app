import 'package:equatable/equatable.dart';

class AvatarSizeTooBigException extends Equatable implements Exception {
  const AvatarSizeTooBigException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
