import 'package:equatable/equatable.dart';

class InvalidPasswordException extends Equatable implements Exception {
  const InvalidPasswordException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
