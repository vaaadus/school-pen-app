import 'package:equatable/equatable.dart';

class InvalidCredentialsException extends Equatable implements Exception {
  const InvalidCredentialsException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
