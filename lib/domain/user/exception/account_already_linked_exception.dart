import 'package:equatable/equatable.dart';

class AccountAlreadyLinkedException extends Equatable implements Exception {
  const AccountAlreadyLinkedException([this.message]);

  final String message;

  @override
  List<Object> get props => [message];
}
