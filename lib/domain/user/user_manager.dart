import 'package:flutter/widgets.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/user/exception/avatar_size_too_big_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_password_exception.dart';
import 'package:school_pen/domain/user/exception/password_cannot_contain_username_exception.dart';
import 'package:school_pen/domain/user/exception/username_taken_exception.dart';
import 'package:school_pen/domain/user/exception/username_too_short_exception.dart';
import 'package:school_pen/domain/user/model/auth_result.dart';
import 'package:school_pen/domain/user/user_listener.dart';
import 'package:school_pen/domain/user/user_login_listener.dart';

import 'exception/account_already_linked_exception.dart';
import 'exception/email_taken_exception.dart';
import 'exception/invalid_credentials_exception.dart';
import 'exception/invalid_email_exception.dart';
import 'exception/password_too_short_exception.dart';
import 'model/auth_provider.dart';
import 'model/user.dart';

abstract class UserManager {
  void addUserLoginListener(UserLoginListener listener);

  void removeUserLoginListener(UserLoginListener listener);

  void addUserListener(UserListener listener, {bool notifyOnAttach = true});

  void removeUserListener(UserListener listener);

  /// Returns the currently signed in user or null.
  Future<User> getCurrentUser();

  /// Request to refresh the user.
  /// This might fetch a user for the server.
  Future<User> refresh();

  /// Register the user.
  ///
  /// Throws [UsernameTooShortException] if username is too short.
  /// Throws [InvalidEmailException] if email has invalid format.
  /// Throws [PasswordCannotContainUsernameException] if password contains username.
  /// Throws [PasswordTooShortException] if password is too short.
  /// Throws [UsernameTakenException] if username already used.
  /// Throws [EmailTakenException] if email already used.
  Future<void> signUp({@required String username, @required String email, @required String password});

  /// Login the user.
  ///
  /// Throws [InvalidEmailException] if email has invalid format.
  /// Throws [InvalidCredentialsException] if credentials are wrong.
  /// Throws [AccountLockedDueToTooManyAttemptsException] if user fails to login too many times.
  Future<void> signIn({@required String email, @required String password});

  /// Login the user using auth provider.
  Future<void> signInWith(AuthResult result);

  /// Links the user account with the auth provider.
  ///
  /// Throws [AccountAlreadyLinkedException] if the account is linked already.
  Future<void> linkWith(AuthResult result);

  /// Unlink the user account from the auth provider.
  Future<void> unlinkFrom(AuthProvider provider);

  /// Logout from this device.
  Future<void> signOut();

  /// Edit the user profile.
  ///
  /// Throws [UsernameTooShortException] if username is too short.
  /// Throws [InvalidEmailException] if email has invalid format.
  /// Throws [UsernameTakenException] if username already used.
  /// Throws [EmailTakenException] if email already used.
  Future<void> editProfile({@required String username, @required String email});

  /// Edit the user avatar. Set [file] to null to remove the avatar.
  ///
  /// Throws [AvatarSizeTooBigException] if the avatar is too big.
  Future<void> editAvatar({MemoryFile file});

  /// Updates the password.
  ///
  /// Throws [PasswordTooShortException] if the new password is too short.
  /// Throws [InvalidPasswordException] if the old password is invalid.
  Future<void> changePassword({@required String oldPassword, @required String newPassword});

  /// Logout from all devices & remove the user.
  Future<void> deleteUser();

  /// Starts the flow of password restoration.
  ///
  /// Throws [InvalidEmailException] if email has invalid format.
  Future<void> requestPasswordReset({@required String email});

  /// Returns a list of available auth providers depending on the platform.
  Future<List<AuthProvider>> getAvailableAuthProviders();

  /// Detects and consumes related auth errors from server response,
  /// returns true if consumed, false otherwise.
  Future<bool> consumeAuthError(dynamic error);
}
