import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

/// A minimal information about the user.
class UserMetadata extends Equatable {
  final String id;
  final String username;
  final String email;
  final int fullUserHashCode;

  const UserMetadata({
    @required this.id,
    @required this.username,
    @required this.email,
    @required this.fullUserHashCode,
  });

  factory UserMetadata.fromJson(Map<String, dynamic> json) => UserMetadata(
        id: json['id'],
        username: json['username'],
        email: json['email'],
        fullUserHashCode: json['fullUserHashCode'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'username': username,
        'email': email,
        'fullUserHashCode': fullUserHashCode,
      };

  /// The user identifiable name.
  String get displayName {
    if (Strings.isNotBlank(email)) return email;
    if (Strings.isNotBlank(username)) return username;
    return '--';
  }

  @override
  List<Object> get props => [id, username, email, fullUserHashCode];
}
