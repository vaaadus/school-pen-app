import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';

class AuthResult extends Equatable {
  final AuthProvider provider;
  final String email;
  final dynamic authData;

  AuthResult({@required this.provider, this.email, @required this.authData}) {
    ArgumentError.checkNotNull(provider, 'provider');
    ArgumentError.checkNotNull(authData, 'authData');
  }

  @override
  List<Object> get props => [provider, email, authData];
}
