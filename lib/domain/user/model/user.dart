import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/files/model/remote_file.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/domain/user/model/user_meta.dart';

const List<AuthProvider> _empty = [];

class User extends Equatable {
  final String id;
  final String username;
  final String email;
  final RemoteFile avatar;
  final List<AuthProvider> linkedProviders;

  const User({
    @required this.id,
    this.username,
    this.email,
    this.avatar,
    this.linkedProviders = _empty,
  });

  /// The user identifiable name.
  String get displayName => toMetadata().displayName;

  UserMetadata toMetadata() {
    return UserMetadata(
      id: id,
      username: username,
      email: email,
      fullUserHashCode: hashCode,
    );
  }

  @override
  List<Object> get props => [id, username, email, avatar, linkedProviders];
}
