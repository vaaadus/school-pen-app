/// Possible options to sign with.
enum AuthProvider {
  apple,
  google,
  facebook,
}

extension AuthProviderExt on AuthProvider {
  String get tag {
    switch (this) {
      case AuthProvider.apple:
        return 'apple';
      case AuthProvider.google:
        return 'google';
      case AuthProvider.facebook:
        return 'facebook';
    }
    throw ArgumentError('Provider not supported: $this');
  }
}
