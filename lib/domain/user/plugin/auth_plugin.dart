import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/domain/user/model/auth_result.dart';
import 'package:school_pen/domain/user/plugin/facebook_auth_plugin.dart';
import 'package:school_pen/domain/user/plugin/google_auth_plugin.dart';

/// An interface for all third party authentication providers.
abstract class AuthPlugin {
  /// Login with the auth provider.
  ///
  /// Throws [NoInternetException] in case the internet connection is required but missing.
  Future<AuthResult> login();

  /// Create an exact auth plugin for the [provider].
  static AuthPlugin of(AuthProvider provider) {
    switch (provider) {
      case AuthProvider.google:
        return GoogleAuthPlugin();
      case AuthProvider.facebook:
        return FacebookAuthPlugin();
      case AuthProvider.apple:
      // TODO apple
    }
    throw ArgumentError('Unsupported provider: $provider');
  }
}
