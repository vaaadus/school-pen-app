import 'package:google_sign_in/google_sign_in.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/network/network_manager.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/domain/user/model/auth_result.dart';
import 'package:school_pen/domain/user/plugin/auth_plugin.dart';

/// Encapsulates Google Sign-in process.
class GoogleAuthPlugin implements AuthPlugin {
  static const List<String> scopes = ['profile', 'email'];

  final NetworkManager _networkManager = Injection.get();

  @override
  Future<AuthResult> login() async {
    try {
      final GoogleSignIn plugin = GoogleSignIn(scopes: scopes);

      final GoogleSignInAccount account = await plugin.signIn();
      if (account == null) return null;

      final GoogleSignInAuthentication authentication = await account.authentication;
      if (authentication == null) return null;

      return AuthResult(
        provider: AuthProvider.google,
        email: account.email,
        authData: {
          'id': account.id,
          'id_token': authentication.idToken,
          'access_token': authentication.accessToken,
        },
      );
    } catch (error) {
      if (await _networkManager.isNetworkAvailable()) rethrow;
      throw NoInternetException();
    }
  }
}
