import 'dart:convert';

import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:http/http.dart' as http;
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/common/http.dart';
import 'package:school_pen/domain/network/network_manager.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/domain/user/model/auth_result.dart';
import 'package:school_pen/domain/user/plugin/auth_plugin.dart';

/// Encapsulates Facebook Sign-In process.
class FacebookAuthPlugin implements AuthPlugin {
  static const String profileUrl = 'https://graph.facebook.com/v8.0/me?fields=email&access_token=';
  static const List<String> permissions = ['email'];

  final NetworkManager _networkManager = Injection.get();

  // TODO: macOS support
  @override
  Future<AuthResult> login() async {
    try {
      final AccessToken accessToken = await FacebookAuth.instance.login(permissions: permissions);
      if (accessToken == null) return null;

      return _fetchProfileAndMapAsResult(accessToken);
    } catch (error) {
      if (await _networkManager.isNetworkAvailable()) rethrow;
      throw NoInternetException();
    }
  }

  Future<AuthResult> _fetchProfileAndMapAsResult(AccessToken accessToken) async {
    return AuthResult(
      provider: AuthProvider.facebook,
      email: await fetchUserProfileEmail(accessToken.token),
      authData: {
        'id': accessToken.userId,
        'access_token': accessToken.token,
        'expiration_date': accessToken.expires.toString(),
      },
    );
  }

  static Future<String> fetchUserProfileEmail(String accessToken) async {
    final http.Response response = await httpGet(profileUrl + accessToken);
    final Map<String, dynamic> profile = json.decode(response.body);
    return profile['email'];
  }
}
