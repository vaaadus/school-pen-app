import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/env_options.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/common/exception/not_found_exception.dart';
import 'package:school_pen/domain/common/parse/parse_error_type.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/files/file_manager.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/files/model/remote_file.dart';
import 'package:school_pen/domain/network/network_manager.dart';
import 'package:school_pen/domain/user/exception/account_already_linked_exception.dart';
import 'package:school_pen/domain/user/exception/email_taken_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_credentials_exception.dart';
import 'package:school_pen/domain/user/exception/too_many_failed_login_attempts_exception.dart';
import 'package:school_pen/domain/user/exception/unauthorized_exception.dart';
import 'package:school_pen/domain/user/exception/username_taken_exception.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/repository/user_repository.dart';

class ParseUserRepository implements UserRepository {
  final NetworkManager _networkManager;
  final RemoteFileManager _fileManager;

  const ParseUserRepository(this._networkManager, this._fileManager);

  @override
  Future<User> getCurrentUser() async {
    final ParseUser user = await ParseUser.currentUser();
    return user != null ? user.toModel(_fileManager) : null;
  }

  @override
  Future<User> refreshCurrentUser() async {
    final ParseUser currentUser = await ParseUser.currentUser();
    if (currentUser == null) return null;

    // clear the avatar as the SDK does not clear it
    await currentUser.unset(_keyAvatar, offlineOnly: true);

    final ParseResponse response = await currentUser.getUpdatedUser();
    if (isSessionInvalidError(response?.error)) {
      await currentUser?.logout();
      return null;
    }

    if (response?.error != null) return _unwrapException(response.error);

    final ParseUser updatedUser = await ParseUser.currentUser();
    return updatedUser?.toModel(_fileManager);
  }

  @override
  Future<void> signUp({@required String username, @required String email, @required String password}) async {
    final ParseResponse response = await ParseUser(username, password, email).signUp();
    if (response?.error?.typed == ParseErrorType.usernameTaken) throw UsernameTakenException();
    if (response?.error?.typed == ParseErrorType.emailTaken) throw EmailTakenException();
    if (response?.error != null) return _unwrapException(response.error);
  }

  @override
  Future<void> signIn({@required String email, @required String password}) async {
    final ParseResponse response = await ParseUser(email, password, email).login();

    if (response?.error?.typed == ParseErrorType.emailNotFound) {
      throw InvalidCredentialsException();
    }

    if (response?.error?.message?.contains(RegExp('account is locked due to multiple failed login attempts')) == true) {
      throw TooManyFailedLoginAttemptsException(response.error.message);
    }

    if (response?.error != null) {
      if (!await _networkManager.isNetworkAvailable()) {
        throw NoInternetException();
      } else {
        throw InvalidCredentialsException(response.error.message);
      }
    }
  }

  @override
  Future<void> signInWith({@required AuthProvider provider, @required dynamic authData}) async {
    final ParseResponse response = await ParseUser.loginWith(provider.tag, authData);
    if (response?.error != null) return _unwrapException(response.error);
  }

  @override
  Future<void> linkWith({@required AuthProvider provider, @required dynamic authData}) async {
    final ParseUser user = await _requireUser();
    final Map<String, dynamic> data = user.authData ?? {};
    if (data[provider.tag] != null) throw AccountAlreadyLinkedException();

    data[provider.tag] = authData;
    user.authData = data;

    final ParseResponse response = await user.save();
    if (response?.error?.typed == ParseErrorType.accountAlreadyLinked) throw AccountAlreadyLinkedException();
    if (response?.error != null) return _unwrapException(response.error);
  }

  @override
  Future<void> unlinkFrom(AuthProvider provider) async {
    final ParseUser user = await _requireUser();
    final Map<String, dynamic> data = user.authData ?? {};
    data[provider.tag] = null;
    user.authData = data;

    final ParseResponse response = await user.save();
    if (response?.error != null) return _unwrapException(response.error);
  }

  @override
  Future<void> signOut() async {
    final ParseUser user = await ParseUser.currentUser();
    await user?.logout();
  }

  @override
  Future<void> editProfile({String username, String email}) async {
    final ParseUser user = await _requireUser();

    if (username != null) user.username = username;
    if (email != null) user.emailAddress = email;

    final ParseResponse response = await user.save();
    if (response.error?.typed == ParseErrorType.usernameTaken) throw UsernameTakenException();
    if (response.error?.typed == ParseErrorType.emailTaken) throw EmailTakenException();
    if (response.error != null) return _unwrapException(response.error);
  }

  @override
  Future<void> editAvatar({MemoryFile file}) async {
    String name;
    String url;

    if (file != null) {
      final ParseWebFile parseFile = ParseWebFile(file.bytes, name: file.name);
      final ParseResponse uploadResponse = await parseFile.upload();
      if (uploadResponse?.error != null) return _unwrapException(uploadResponse.error);

      name = file.name;
      url = parseFile.url;
    } else {
      name = null;
      url = null;
    }

    final ParseResponse response = await ParseCloudFunction('updateUserAvatar').execute(parameters: {
      'url': url,
      'name': name,
    });

    if (response.error != null) return _unwrapException(response.error);
  }

  @override
  Future<void> changePassword({@required String oldPassword, @required String newPassword}) async {
    final ParseUser user = await _requireUser();

    // TODO: validate oldPassword

    user.password = newPassword;
    final ParseResponse response = await user.save();
    if (response?.error != null) return _unwrapException(response.error);
  }

  @override
  Future<void> deleteUser() async {
    final ParseUser user = await ParseUser.currentUser();
    final ParseResponse response = await user?.destroy();

    if (isSessionInvalidError(response.error)) {
      await user?.logout();
    } else if (response.error != null) {
      return _unwrapException(response.error);
    } else {
      user?.forgetLocalSession();
      await user?.deleteLocalUserData();
    }
  }

  @override
  Future<void> requestPasswordReset({@required String email}) async {
    final ParseResponse response = await ParseUser('', '', email).requestPasswordReset();
    if (response?.error != null) return _unwrapException(response.error);
  }

  // TODO: amazon
  @override
  Future<List<AuthProvider>> getAvailableAuthProviders() async {
    return [
      if (!kReleaseMode && !kIsWeb && (Platform.isIOS || Platform.isMacOS)) AuthProvider.apple,
      if (kIsWeb || Platform.isIOS || Platform.isAndroid) AuthProvider.facebook,
      if (EnvOptions.store != TargetStore.amazon) AuthProvider.google,
    ];
  }

  @override
  bool isSessionInvalidError(error) {
    return error is ParseError && error.typed == ParseErrorType.invalidSessionToken;
  }

  Future<ParseUser> _requireUser() async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) throw UnauthorizedException('The user is not signed in');
    return user;
  }

  Future<T> _unwrapException<T>(Object error) async {
    if (error is ParseError && error.typed == ParseErrorType.objectNotFound) {
      throw NotFoundException(error.message);
    }

    final bool networkAvailable = await _networkManager.isNetworkAvailable();
    throw networkAvailable ? Exception(error.toString()) : NoInternetException();
  }
}

const String _keyAvatar = 'avatar';

extension _ParseUserExt on ParseUser {
  Future<User> toModel(RemoteFileManager _fileManager) async {
    final String avatarUrl = avatar?.url;
    final RemoteFile avatarFile = avatarUrl != null ? _fileManager.loadFile(avatarUrl) : null;

    return User(
      id: objectId,
      username: username,
      email: emailAddress,
      avatar: avatarFile,
      linkedProviders: _authProviders,
    );
  }

  List<AuthProvider> get _authProviders {
    final Map<String, dynamic> data = authData ?? {};
    final List<AuthProvider> providers = [];
    for (AuthProvider provider in AuthProvider.values) {
      if (data[provider.tag] != null) {
        providers.add(provider);
      }
    }
    return providers;
  }

  ParseFileBase get avatar => getNullable(_keyAvatar);
}
