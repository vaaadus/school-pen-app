import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/user/exception/account_already_linked_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_password_exception.dart';
import 'package:school_pen/domain/user/exception/too_many_failed_login_attempts_exception.dart';
import 'package:school_pen/domain/user/exception/username_taken_exception.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/domain/user/model/user.dart';

abstract class UserRepository {
  /// Returns the currently signed in user or null.
  Future<User> getCurrentUser();

  /// Refreshes the current user (from server) and returns it.
  /// Null if user not signed in.
  Future<User> refreshCurrentUser();

  /// Register the user.
  ///
  /// Throws [UsernameTakenException] if username already used.
  /// Throws [EmailTakenException] if email already used.
  Future<void> signUp({@required String username, @required String email, @required String password});

  /// Login the user.
  ///
  /// Throws [InvalidCredentialsException] if credentials are wrong.
  /// Throws [TooManyFailedLoginAttemptsException] if user fails to login too many times.
  Future<void> signIn({@required String email, @required String password});

  /// Login the user using auth provider.
  Future<void> signInWith({@required AuthProvider provider, @required dynamic authData});

  /// Links the user account with the auth provider.
  ///
  /// Throws [AccountAlreadyLinkedException] if the account is linked already.
  Future<void> linkWith({@required AuthProvider provider, @required dynamic authData});

  /// Unlink the user account from the auth provider.
  Future<void> unlinkFrom(AuthProvider provider);

  /// Logout from this device.
  Future<void> signOut();

  /// Edit the user profile.
  ///
  /// Throws [UsernameTakenException] if username already used.
  /// Throws [EmailTakenException] if email already used.
  Future<void> editProfile({String username, String email});

  /// Edit the user avatar.
  Future<void> editAvatar({MemoryFile file});

  /// Updates the password.
  ///
  /// Throws [InvalidPasswordException] if the old password is invalid.
  Future<void> changePassword({@required String oldPassword, @required String newPassword});

  /// Logout from all devices & remove the user.
  Future<void> deleteUser();

  /// Starts the flow of password restoration.
  Future<void> requestPasswordReset({@required String email});

  /// Returns a list of available auth providers depending on the platform.
  Future<List<AuthProvider>> getAvailableAuthProviders();

  /// Checks whether this [error] is related to session being revoked.
  bool isSessionInvalidError(dynamic error);
}
