import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/extensions/map_ext.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/userconfig/model/agenda_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/timetable_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/user_config.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

class SembastUserConfigRepository implements UserConfigRepository {
  static const String _keyTimestamp = 'userConfig_lastUpdate_timestamp';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('userConfig');
  final BehaviorSubject<List<Property>> _onChange = BehaviorSubject();
  final Lock _lock = Lock();
  final Preferences _preferences;

  SembastUserConfigRepository(this._preferences) {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((List<RecordSnapshot<String, dynamic>> records) {
      final List<Property> properties = records.map((e) => PropertyExt.forTag(e.key)).where((e) => e != null).toList();
      _onChange.add(properties);
    });
  }

  @override
  Stream<List<Property>> get onChange => _onChange;

  @override
  Future<UserConfig> getUserConfig() async {
    return UserConfig(
      updatedAt: await _getTimestamp(),
      schoolYearId: await getSchoolYearId(),
      semesterIdPerYear: await _getSemesterIdPerYears(),
      autoSemesterChange: await isAutoSemesterChangeEnabled(),
      analyticsEnabled: await isAnalyticsEnabled(),
      diagnosisEnabled: await isDiagnosisEnabled(),
      personalizedAdsEnabled: await isPersonalizedAdsEnabled(),
      gradingSystem: await getGradingSystem(),
      timetableIdPerYear: await _getTimetableIdPerYears(),
      subscribedTimetablesPerYear: await _getSubscribedTimetableIdsPerYears(),
      agendaOptions: await _getAgendaOptions(),
      timetableOptions: await _getTimetableOptions(),
    );
  }

  @override
  Future<bool> setUserConfig(UserConfig config) {
    return _lock.synchronized(() async {
      final List<bool> futures = await Future.wait([
        _setString(Property.schoolYearId, config.schoolYearId),
        _setSemesterIdsPerYears(config.semesterIdPerYear),
        _setBool(Property.autoSemesterChange, config.autoSemesterChange),
        _setBool(Property.analyticsEnabled, config.analyticsEnabled),
        _setBool(Property.diagnosisEnabled, config.diagnosisEnabled),
        _setBool(Property.personalizedAdsEnabled, config.personalizedAdsEnabled),
        _setGradingSystem(config.gradingSystem),
        _setTimetableIdsPerYears(config.timetableIdPerYear),
        _setSubscribedTimetableIdsPerYears(config.subscribedTimetablesPerYear),
        _setAgendaOptions(config.agendaOptions),
        _setTimetableOptions(config.timetableOptions),
      ]);

      final bool updated = futures.any((element) => element);
      if (updated) await _updateTimestamp(config.updatedAt);
      return updated;
    });
  }

  @override
  Future<String> getSchoolYearId() {
    return _lock.synchronized(() => _getString(Property.schoolYearId));
  }

  @override
  Future<bool> setSchoolYearId(String id) {
    return _lock.synchronized(() async {
      final bool updated = await _setString(Property.schoolYearId, id);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<String> getSemesterIdPerYear(String schoolYearId) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> record = await _getSemesterIdPerYears();
      if (record == null) return null;
      return record[schoolYearId];
    });
  }

  @override
  Future<bool> setSemesterIdPerYear(String schoolYearId, String semesterId) {
    return _lock.synchronized(() async {
      final bool updated = await _setSemesterIdPerYear(schoolYearId, semesterId);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<bool> isAutoSemesterChangeEnabled() {
    return _lock.synchronized(() => _getBool(Property.autoSemesterChange));
  }

  @override
  Future<bool> setAutoSemesterChangeEnabled(bool enabled) {
    return _lock.synchronized(() async {
      final bool updated = await _setBool(Property.autoSemesterChange, enabled);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<bool> isAnalyticsEnabled() {
    return _lock.synchronized(() => _getBool(Property.analyticsEnabled));
  }

  @override
  Future<bool> setAnalyticsEnabled(bool enabled) {
    return _lock.synchronized(() async {
      final bool updated = await _setBool(Property.analyticsEnabled, enabled);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<bool> isDiagnosisEnabled() {
    return _lock.synchronized(() => _getBool(Property.diagnosisEnabled));
  }

  @override
  Future<bool> setDiagnosisEnabled(bool enabled) {
    return _lock.synchronized(() async {
      final bool updated = await _setBool(Property.diagnosisEnabled, enabled);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<bool> isPersonalizedAdsEnabled() {
    return _lock.synchronized(() => _getBool(Property.personalizedAdsEnabled));
  }

  @override
  Future<bool> setPersonalizedAdsEnabled(bool enabled) {
    return _lock.synchronized(() async {
      final bool updated = await _setBool(Property.personalizedAdsEnabled, enabled);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<GradingSystem> getGradingSystem() {
    return _lock.synchronized(() => _getGradingSystem());
  }

  @override
  Future<bool> setGradingSystem(GradingSystem system) {
    return _lock.synchronized(() async {
      final bool updated = await _setGradingSystem(system);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<String> getTimetableIdPerYear(String schoolYearId) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> record = await _getTimetableIdPerYears();
      if (record == null) return null;
      return record[schoolYearId];
    });
  }

  @override
  Future<bool> setTimetableIdPerYear(String schoolYearId, String timetableId) {
    return _lock.synchronized(() async {
      final bool updated = await _setTimetableIdPerYear(schoolYearId, timetableId);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<List<String>> getSubscribedTimetableIdsPerYear(String schoolYearId) {
    return _lock.synchronized(() => _getSubscribedTimetableIdsPerYear(schoolYearId));
  }

  @override
  Future<bool> subscribeTimetablePerYear(String schoolYearId, String timetableId) {
    return _lock.synchronized(() async {
      final List<String> timetableIds = await _getSubscribedTimetableIdsPerYear(schoolYearId);
      timetableIds.addIfAbsent(timetableId);

      final bool updated = await _setSubscribedTimetableIdsPerYear(schoolYearId, timetableIds);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<bool> unsubscribeTimetablePerYear(String schoolYearId, String timetableId) {
    return _lock.synchronized(() async {
      final List<String> timetableIds = await _getSubscribedTimetableIdsPerYear(schoolYearId);
      timetableIds.remove(timetableId);

      final bool updated = await _setSubscribedTimetableIdsPerYear(schoolYearId, timetableIds);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<AgendaNotificationOptions> getAgendaOptions() {
    return _lock.synchronized(() => _getAgendaOptions());
  }

  @override
  Future<bool> setAgendaOptions(AgendaNotificationOptions options) {
    return _lock.synchronized(() async {
      final bool updated = await _setAgendaOptions(options);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  @override
  Future<TimetableNotificationOptions> getTimetableOptions() {
    return _lock.synchronized(() => _getTimetableOptions());
  }

  @override
  Future<bool> setTimetableOptions(TimetableNotificationOptions options) {
    return _lock.synchronized(() async {
      final bool updated = await _setTimetableOptions(options);
      if (updated) await _updateTimestamp();
      return updated;
    });
  }

  Future<Map<String, String>> _getSemesterIdPerYears() async {
    final RecordRef<String, dynamic> recordRef = _store.record(Property.semesterIdPerYear.tag);
    final Map<String, dynamic> record = await recordRef.get(await _db) ?? {};

    final Map<String, String> result = {};
    record.forEach((String key, dynamic value) => result[key] = value?.toString());
    return result;
  }

  Future<bool> _setSemesterIdsPerYears(Map<String, String> map) async {
    if (!mapEquals(map, await _getSemesterIdPerYears())) {
      await _store.record(Property.semesterIdPerYear.tag).put(await _db, map);
      return true;
    }

    return false;
  }

  Future<bool> _setSemesterIdPerYear(String schoolYearId, String semesterId) async {
    final Map<String, dynamic> record = await _getSemesterIdPerYears();
    if (record[schoolYearId] == semesterId) return false;

    record[schoolYearId] = semesterId;
    await _store.record(Property.semesterIdPerYear.tag).put(await _db, record);
    return true;
  }

  Future<Map<String, String>> _getTimetableIdPerYears() async {
    final RecordRef<String, dynamic> recordRef = _store.record(Property.timetableIdPerYear.tag);
    final Map<String, dynamic> record = await recordRef.get(await _db) ?? {};

    final Map<String, String> result = {};
    record.forEach((String key, dynamic value) => result[key] = value?.toString());
    return result;
  }

  Future<bool> _setTimetableIdsPerYears(Map<String, String> map) async {
    if (!mapEquals(map, await _getTimetableIdPerYears())) {
      await _store.record(Property.timetableIdPerYear.tag).put(await _db, map);
      return true;
    }

    return false;
  }

  Future<bool> _setTimetableIdPerYear(String schoolYearId, String timetableId) async {
    final Map<String, dynamic> record = await _getTimetableIdPerYears();
    if (record[schoolYearId] == timetableId) return false;

    record[schoolYearId] = timetableId;
    await _store.record(Property.timetableIdPerYear.tag).put(await _db, record);
    return true;
  }

  Future<Map<String, List<String>>> _getSubscribedTimetableIdsPerYears() async {
    final RecordRef<String, dynamic> recordRef = _store.record(Property.subscribedTimetableIdsPerYear.tag);
    final Map<String, dynamic> record = await recordRef.get(await _db) ?? {};

    final Map<String, List<String>> result = {};
    record.forEach((String key, dynamic value) {
      result[key] = (value as List<dynamic>).map((e) => e.toString()).toList();
    });
    return result;
  }

  Future<List<String>> _getSubscribedTimetableIdsPerYear(String schoolYearId) async {
    final RecordRef<String, dynamic> recordRef = _store.record(Property.subscribedTimetableIdsPerYear.tag);
    final Map<String, dynamic> record = await recordRef.get(await _db) ?? {};

    final List<dynamic> result = record[schoolYearId] ?? [];
    return result.map((e) => e.toString()).toList();
  }

  Future<bool> _setSubscribedTimetableIdsPerYears(Map<String, List<String>> map) async {
    if (!MapExt.equalsWithListChildren(map, await _getSubscribedTimetableIdsPerYears())) {
      await _store.record(Property.subscribedTimetableIdsPerYear.tag).put(await _db, map);
      return true;
    }

    return false;
  }

  Future<bool> _setSubscribedTimetableIdsPerYear(String schoolYearId, List<String> timetableIds) async {
    final Map<String, List<String>> map = await _getSubscribedTimetableIdsPerYears();
    final List<String> currentValue = map[schoolYearId] ?? [];

    if (!listEquals(timetableIds, currentValue)) {
      map[schoolYearId] = timetableIds;
      await _store.record(Property.subscribedTimetableIdsPerYear.tag).put(await _db, map);
      return true;
    }

    return false;
  }

  Future<GradingSystem> _getGradingSystem() async {
    final Map<String, dynamic> record = await _store.record(Property.gradingSystem.tag).get(await _db);
    return GradingSystem.fromJson(record);
  }

  Future<bool> _setGradingSystem(GradingSystem system) async {
    if (await _getGradingSystem() == system) return false;

    if (system == null) {
      await _store.record(Property.gradingSystem.tag).delete(await _db);
    } else {
      await _store.record(Property.gradingSystem.tag).put(await _db, system.toJson());
    }
    return true;
  }

  Future<String> _getString(Property property) async {
    final Map<String, dynamic> record = await _store.record(property.tag).get(await _db);
    return SembastSerializer.deserializeString(record);
  }

  Future<bool> _setString(Property property, String value) async {
    final RecordRef<String, dynamic> recordRef = _store.record(property.tag);
    final Map<String, dynamic> record = await recordRef.get(await _db);
    if (SembastSerializer.deserializeString(record) == value) return false;

    await recordRef.put(await _db, SembastSerializer.serializeString(value));
    return true;
  }

  Future<bool> _getBool(Property property) async {
    final Map<String, dynamic> record = await _store.record(property.tag).get(await _db);
    return SembastSerializer.deserializeBool(record);
  }

  Future<bool> _setBool(Property property, bool value) async {
    final RecordRef<String, dynamic> recordRef = _store.record(property.tag);
    final Map<String, dynamic> record = await recordRef.get(await _db);
    if (SembastSerializer.deserializeBool(record) == value) return false;

    await recordRef.put(await _db, SembastSerializer.serializeBool(value));
    return true;
  }

  Future<AgendaNotificationOptions> _getAgendaOptions() async {
    final Map<String, dynamic> record = await _store.record(Property.agendaOptions.tag).get(await _db);
    return AgendaNotificationOptions.fromJson(record);
  }

  Future<bool> _setAgendaOptions(AgendaNotificationOptions options) async {
    if (await _getAgendaOptions() == options) return false;

    if (options == null) {
      await _store.record(Property.agendaOptions.tag).delete(await _db);
    } else {
      await _store.record(Property.agendaOptions.tag).put(await _db, options.toJson());
    }
    return true;
  }

  Future<TimetableNotificationOptions> _getTimetableOptions() async {
    final Map<String, dynamic> record = await _store.record(Property.timetableOptions.tag).get(await _db);
    return TimetableNotificationOptions.fromJson(record);
  }

  Future<bool> _setTimetableOptions(TimetableNotificationOptions options) async {
    if (await _getTimetableOptions() == options) return false;

    if (options == null) {
      await _store.record(Property.timetableOptions.tag).delete(await _db);
    } else {
      await _store.record(Property.timetableOptions.tag).put(await _db, options.toJson());
    }
    return true;
  }

  Future<void> _updateTimestamp([DateTime dateTime]) async {
    _preferences.setDateTime(_keyTimestamp, dateTime);
  }

  Future<DateTime> _getTimestamp() async {
    return _preferences.getDateTime(_keyTimestamp) ?? DateTime.now();
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}
