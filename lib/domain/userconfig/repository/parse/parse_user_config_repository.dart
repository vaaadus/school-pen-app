import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:pedantic/pedantic.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';
import 'package:school_pen/domain/userconfig/model/agenda_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/timetable_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/user_config.dart';
import 'package:school_pen/domain/userconfig/repository/parse/parse_user_config.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';

class ParseUserConfigRepository implements UserConfigRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseUserConfigRepository');

  final UserConfigRepository _delegate;
  Subscription _subscription;

  ParseUserConfigRepository(this._delegate);

  @override
  Stream<List<Property>> get onChange => _delegate.onChange;

  @override
  Future<UserConfig> getUserConfig() => _delegate.getUserConfig();

  @override
  Future<bool> setUserConfig(UserConfig config) async {
    final bool updated = await _delegate.setUserConfig(config);
    if (updated) {
      await _tryWithUser((user) => ParseUserConfig.fromModel(config, user).store());
    }
    return updated;
  }

  @override
  Future<String> getSchoolYearId() => _delegate.getSchoolYearId();

  @override
  Future<bool> setSchoolYearId(String id) async {
    final bool updated = await _delegate.setSchoolYearId(id);
    if (updated) {
      await _tryWithUser((user) => ParseUserConfig().updateSchoolYear(id));
    }
    return updated;
  }

  @override
  Future<String> getSemesterIdPerYear(String schoolYearId) => _delegate.getSemesterIdPerYear(schoolYearId);

  @override
  Future<bool> setSemesterIdPerYear(String schoolYearId, String semesterId) async {
    final bool updated = await _delegate.setSemesterIdPerYear(schoolYearId, semesterId);
    if (updated) {
      await _tryWithUser((user) => ParseUserConfig().updateSemester(schoolYearId, semesterId));
    }
    return updated;
  }

  @override
  Future<bool> isAutoSemesterChangeEnabled() => _delegate.isAutoSemesterChangeEnabled();

  @override
  Future<bool> setAutoSemesterChangeEnabled(bool enabled) async {
    final bool updated = await _delegate.setAutoSemesterChangeEnabled(enabled);
    if (updated) {
      await _tryWithUser((user) => ParseUserConfig().updateAutoSemesterChange(enabled));
    }
    return updated;
  }

  @override
  Future<bool> isAnalyticsEnabled() => _delegate.isAnalyticsEnabled();

  @override
  Future<bool> setAnalyticsEnabled(bool enabled) async {
    final bool updated = await _delegate.setAnalyticsEnabled(enabled);
    if (updated) {
      await _tryWithUser((user) => ParseUserConfig().updateAnalyticsEnabled(enabled));
    }
    return updated;
  }

  @override
  Future<bool> isDiagnosisEnabled() => _delegate.isDiagnosisEnabled();

  @override
  Future<bool> setDiagnosisEnabled(bool enabled) async {
    final bool updated = await _delegate.setDiagnosisEnabled(enabled);
    if (updated) {
      await _tryWithUser((user) => ParseUserConfig().updateDiagnosisEnabled(enabled));
    }
    return updated;
  }

  @override
  Future<bool> isPersonalizedAdsEnabled() => _delegate.isPersonalizedAdsEnabled();

  @override
  Future<bool> setPersonalizedAdsEnabled(bool enabled) async {
    final bool updated = await _delegate.setPersonalizedAdsEnabled(enabled);
    if (updated) {
      await _tryWithUser((user) => ParseUserConfig().updatePersonalizedAdsEnabled(enabled));
    }
    return updated;
  }

  @override
  Future<GradingSystem> getGradingSystem() => _delegate.getGradingSystem();

  @override
  Future<bool> setGradingSystem(GradingSystem system) async {
    final bool updated = await _delegate.setGradingSystem(system);
    if (updated) {
      await _tryWithUser((user) => ParseUserConfig().updateGradingSystem(system));
    }
    return updated;
  }

  @override
  Future<String> getTimetableIdPerYear(String schoolYearId) => _delegate.getTimetableIdPerYear(schoolYearId);

  @override
  Future<bool> setTimetableIdPerYear(String schoolYearId, String timetableId) async {
    final bool updated = await _delegate.setTimetableIdPerYear(schoolYearId, timetableId);
    if (updated) {
      unawaited(_tryWithUser((user) => ParseUserConfig().updateTimetable(schoolYearId, timetableId)));
    }
    return updated;
  }

  @override
  Future<List<String>> getSubscribedTimetableIdsPerYear(String schoolYearId) =>
      _delegate.getSubscribedTimetableIdsPerYear(schoolYearId);

  @override
  Future<bool> subscribeTimetablePerYear(String schoolYearId, String timetableId) async {
    final bool updated = await _delegate.subscribeTimetablePerYear(schoolYearId, timetableId);
    if (updated) {
      await _tryWithUser((user) async {
        final List<String> timetableIds = await _delegate.getSubscribedTimetableIdsPerYear(schoolYearId);
        await ParseUserConfig().updateSubscribedTimetables(schoolYearId, timetableIds);
      });
    }
    return updated;
  }

  @override
  Future<bool> unsubscribeTimetablePerYear(String schoolYearId, String timetableId) async {
    final bool updated = await _delegate.unsubscribeTimetablePerYear(schoolYearId, timetableId);
    if (updated) {
      await _tryWithUser((user) async {
        final List<String> timetableIds = await _delegate.getSubscribedTimetableIdsPerYear(schoolYearId);
        await ParseUserConfig().updateSubscribedTimetables(schoolYearId, timetableIds);
      });
    }
    return updated;
  }

  @override
  Future<AgendaNotificationOptions> getAgendaOptions() => _delegate.getAgendaOptions();

  @override
  Future<bool> setAgendaOptions(AgendaNotificationOptions options) async {
    final bool updated = await _delegate.setAgendaOptions(options);
    if (updated) {
      await _tryWithUser((user) => ParseUserConfig().updateAgendaOptions(options));
    }
    return updated;
  }

  @override
  Future<TimetableNotificationOptions> getTimetableOptions() => _delegate.getTimetableOptions();

  @override
  Future<bool> setTimetableOptions(TimetableNotificationOptions options) async {
    final bool updated = await _delegate.setTimetableOptions(options);
    if (updated) {
      await _tryWithUser((user) => ParseUserConfig().updateTimetableOptions(options));
    }
    return updated;
  }

  Future<void> store(UserConfig config) async {
    await _delegate.setUserConfig(config);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseUserConfig()));
    _subscription.on(LiveQueryEvent.update, (ParseUserConfig object) {
      _try(() => _delegate.setUserConfig(object.toModel()));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  /// Executes callback if user is signed in, ignores exceptions.
  Future<void> _tryWithUser(Future<void> Function(ParseUser user) callback) {
    return _try(() async {
      final ParseUser user = await ParseUser.currentUser();
      if (user != null) {
        await callback(user);
      }
    });
  }

  Future<void> _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
