import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/userconfig/model/agenda_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/timetable_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/user_config.dart';

class ParseUserConfig extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'UserConfig';
  static const String keyUser = 'user';
  static const String keySchoolYear = 'schoolYearUuid';
  static const String keySemesterPerYear = 'semesterUuidPerSchoolYear';
  static const String keyAutoSemesterChange = 'autoSemesterChange';
  static const String keyAnalyticsEnabled = 'analyticsEnabled';
  static const String keyDiagnosisEnabled = 'diagnosisEnabled';
  static const String keyPersonalizedAdsEnabled = 'personalizedAdsEnabled';
  static const String keyGradingSystem = 'gradingSystem';
  static const String keyTimetablePerYear = 'timetableUuidPerSchoolYear';
  static const String keySubscribedTimetablesPerYear = 'subscribedTimetableUuidsPerSchoolYear';
  static const String keyAgendaOptions = 'agendaOptions';
  static const String keyTimetableOptions = 'timetableOptions';

  ParseUserConfig() : super(keyClassName);

  ParseUserConfig.clone() : this();

  factory ParseUserConfig.fromModel(UserConfig config, ParseUser user) {
    return ParseUserConfig()
      ..user = user
      ..schoolYearId = config.schoolYearId
      ..semesterIdPerYear = config.semesterIdPerYear
      ..autoSemesterChange = config.autoSemesterChange
      ..analyticsEnabled = config.analyticsEnabled
      ..diagnosisEnabled = config.diagnosisEnabled
      ..personalizedAdsEnabled = config.personalizedAdsEnabled
      ..gradingSystem = config.gradingSystem
      ..timetableIdPerYear = config.timetableIdPerYear
      ..subscribedTimetableIdsPerYear = config.subscribedTimetablesPerYear
      ..agendaOptions = config.agendaOptions
      ..timetableOptions = config.timetableOptions;
  }

  UserConfig toModel() => UserConfig(
        updatedAt: updatedAt,
        schoolYearId: schoolYearId,
        semesterIdPerYear: semesterIdPerYear,
        autoSemesterChange: autoSemesterChange,
        analyticsEnabled: analyticsEnabled,
        diagnosisEnabled: diagnosisEnabled,
        personalizedAdsEnabled: personalizedAdsEnabled,
        gradingSystem: gradingSystem,
        timetableIdPerYear: timetableIdPerYear,
        subscribedTimetablesPerYear: subscribedTimetableIdsPerYear,
        agendaOptions: agendaOptions,
        timetableOptions: timetableOptions,
      );

  @override
  ParseUserConfig clone(Map map) => ParseUserConfig.clone()..fromJson(map);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get schoolYearId => getNullable(keySchoolYear);

  set schoolYearId(String value) => setNullable(keySchoolYear, value);

  Map<String, String> get semesterIdPerYear => Map.from(getNullable(keySemesterPerYear));

  set semesterIdPerYear(Map<String, String> value) => setNullable(keySemesterPerYear, value);

  bool get autoSemesterChange => getNullable(keyAutoSemesterChange);

  set autoSemesterChange(bool value) => setNullable(keyAutoSemesterChange, value);

  bool get analyticsEnabled => getNullable(keyAnalyticsEnabled);

  set analyticsEnabled(bool value) => setNullable(keyAnalyticsEnabled, value);

  bool get diagnosisEnabled => getNullable(keyDiagnosisEnabled);

  set diagnosisEnabled(bool value) => setNullable(keyDiagnosisEnabled, value);

  bool get personalizedAdsEnabled => getNullable(keyPersonalizedAdsEnabled);

  set personalizedAdsEnabled(bool value) => setNullable(keyPersonalizedAdsEnabled, value);

  GradingSystem get gradingSystem => GradingSystem.fromJson(getNullable(keyGradingSystem));

  set gradingSystem(GradingSystem system) => setNullable(keyGradingSystem, system?.toJson());

  Map<String, String> get timetableIdPerYear => Map.from(getNullable(keyTimetablePerYear));

  set timetableIdPerYear(Map<String, String> value) => setNullable(keyTimetablePerYear, value);

  Map<String, List<String>> get subscribedTimetableIdsPerYear {
    final Map<String, dynamic> value = getNullable(keySubscribedTimetablesPerYear) ?? {};
    return value.map((key, value) => MapEntry(key, (value as List).cast()));
  }

  set subscribedTimetableIdsPerYear(Map<String, List<String>> value) =>
      setNullable(keySubscribedTimetablesPerYear, value);

  AgendaNotificationOptions get agendaOptions {
    final Map<String, dynamic> value = getNullable(keyAgendaOptions);
    return value != null ? AgendaNotificationOptions.fromJson(value) : null;
  }

  set agendaOptions(AgendaNotificationOptions options) => setNullable(keyAgendaOptions, options?.toJson());

  TimetableNotificationOptions get timetableOptions {
    final Map<String, dynamic> value = getNullable(keyTimetableOptions);
    return value != null ? TimetableNotificationOptions.fromJson(value) : null;
  }

  set timetableOptions(TimetableNotificationOptions options) => setNullable(keyTimetableOptions, options?.toJson());

  Future<ParseResponse> updateSchoolYear(String schoolYearId) {
    return _patchUserConfig({keySchoolYear: schoolYearId});
  }

  Future<ParseResponse> updateSemester(String schoolYearId, String semesterId) {
    return _patchUserConfig({
      keySemesterPerYear: {schoolYearId: semesterId},
    });
  }

  Future<ParseResponse> updateAutoSemesterChange(bool enabled) {
    return _patchUserConfig({keyAutoSemesterChange: enabled});
  }

  Future<ParseResponse> updateAnalyticsEnabled(bool enabled) {
    return _patchUserConfig({keyAnalyticsEnabled: enabled});
  }

  Future<ParseResponse> updateDiagnosisEnabled(bool enabled) {
    return _patchUserConfig({keyDiagnosisEnabled: enabled});
  }

  Future<ParseResponse> updatePersonalizedAdsEnabled(bool enabled) {
    return _patchUserConfig({keyPersonalizedAdsEnabled: enabled});
  }

  Future<ParseResponse> updateGradingSystem(GradingSystem system) {
    return _patchUserConfig({keyGradingSystem: system?.toJson()});
  }

  Future<ParseResponse> updateTimetable(String schoolYearId, String timetableId) {
    return _patchUserConfig({
      keyTimetablePerYear: {schoolYearId: timetableId}
    });
  }

  Future<ParseResponse> updateSubscribedTimetables(String schoolYearId, List<String> timetableIds) {
    return _patchUserConfig({
      keySubscribedTimetablesPerYear: {schoolYearId: timetableIds}
    });
  }

  Future<ParseResponse> updateAgendaOptions(AgendaNotificationOptions options) {
    return _patchUserConfig({
      keyAgendaOptions: options?.toJson(),
    });
  }

  Future<ParseResponse> updateTimetableOptions(TimetableNotificationOptions options) {
    return _patchUserConfig({
      keyTimetableOptions: options?.toJson(),
    });
  }

  /// Updates a user config.
  Future<ParseResponse> store() {
    return _patchUserConfig({
      keySchoolYear: schoolYearId,
      keySemesterPerYear: semesterIdPerYear,
      keyAutoSemesterChange: autoSemesterChange,
      keyAnalyticsEnabled: analyticsEnabled,
      keyDiagnosisEnabled: diagnosisEnabled,
      keyPersonalizedAdsEnabled: personalizedAdsEnabled,
      keyGradingSystem: gradingSystem?.toJson(),
      keyTimetablePerYear: timetableIdPerYear,
      keySubscribedTimetablesPerYear: subscribedTimetableIdsPerYear,
      keyAgendaOptions: agendaOptions?.toJson(),
      keyTimetableOptions: timetableOptions?.toJson(),
    });
  }

  Future<ParseResponse> _patchUserConfig(Map<String, dynamic> parameters) {
    return ParseCloudFunction('patchUserConfig').execute(parameters: parameters);
  }
}
