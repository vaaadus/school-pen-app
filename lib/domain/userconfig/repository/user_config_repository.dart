import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/userconfig/model/agenda_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/timetable_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/user_config.dart';

abstract class UserConfigRepository {
  /// Stream changes whenever user config repository changes.
  Stream<List<Property>> get onChange;

  /// Returns the whole user config
  Future<UserConfig> getUserConfig();

  /// Sets the whole user config.
  /// Returns true if set, false otherwise.
  Future<bool> setUserConfig(UserConfig config);

  /// Returns the id of the active school year or null.
  Future<String> getSchoolYearId();

  /// Sets a new school year as active.
  /// Returns true if set, false otherwise.
  Future<bool> setSchoolYearId(String id);

  /// Returns the id of the active semester in a given year by [schoolYearId].
  Future<String> getSemesterIdPerYear(String schoolYearId);

  /// Sets a new semester as active in the given year by [schoolYearId].
  /// Returns true if set, false otherwise.
  Future<bool> setSemesterIdPerYear(String schoolYearId, String semesterId);

  /// Returns whether the terms should change automatically due to date ranges.
  Future<bool> isAutoSemesterChangeEnabled();

  /// Sets whether the terms should change automatically.
  /// Returns true if set, false otherwise.
  Future<bool> setAutoSemesterChangeEnabled(bool enabled);

  /// Returns whether user agrees to analytics collection.
  Future<bool> isAnalyticsEnabled();

  /// Sets whether user agrees to analytics collection.
  /// Returns true if set, false otherwise.
  Future<bool> setAnalyticsEnabled(bool enabled);

  /// Returns whether user agrees to crash reports collection.
  Future<bool> isDiagnosisEnabled();

  /// Sets whether user agrees to crash reports collection.
  /// Returns true if set, false otherwise.
  Future<bool> setDiagnosisEnabled(bool enabled);

  /// Returns whether user agrees to personalized ads.
  Future<bool> isPersonalizedAdsEnabled();

  /// Sets whether user agrees to personalized ads.
  /// Returns true if set, false otherwise.
  Future<bool> setPersonalizedAdsEnabled(bool enabled);

  /// Returns the grading system for the whole app.
  Future<GradingSystem> getGradingSystem();

  /// Sets a new grading system.
  /// Returns true if set, false otherwise.
  Future<bool> setGradingSystem(GradingSystem system);

  /// Returns the id of the active timetable in a given year by [schoolYearId].
  Future<String> getTimetableIdPerYear(String schoolYearId);

  /// Sets a new timetable as active in the given year by [schoolYearId].
  /// Returns true if set, false otherwise.
  Future<bool> setTimetableIdPerYear(String schoolYearId, String timetableId);

  /// Returns a list of subscribed public timetables at a given [schoolYearId].
  Future<List<String>> getSubscribedTimetableIdsPerYear(String schoolYearId);

  /// Ads the [timetableId] to the list of subscribed public timetables at a given [schoolYearId].
  /// Returns true if subscribed, false otherwise.
  Future<bool> subscribeTimetablePerYear(String schoolYearId, String timetableId);

  /// Removes the [timetableId] from the list of subscribed public timetables at a given [schoolYearId].
  /// Returns true if unsubscribed, false otherwise.
  Future<bool> unsubscribeTimetablePerYear(String schoolYearId, String timetableId);

  /// Returns a notification options for agenda.
  Future<AgendaNotificationOptions> getAgendaOptions();

  /// Sets a notification options for agenda.
  /// Returns true if set, false otherwise.
  Future<bool> setAgendaOptions(AgendaNotificationOptions options);

  /// Returns a notification options for timetable.
  Future<TimetableNotificationOptions> getTimetableOptions();

  /// Sets a notification options for timetable.
  /// Returns true if set, false otherwise.
  Future<bool> setTimetableOptions(TimetableNotificationOptions options);
}

/// Lists all changeable properties.
enum Property {
  schoolYearId,
  semesterIdPerYear,
  autoSemesterChange,
  analyticsEnabled,
  diagnosisEnabled,
  personalizedAdsEnabled,
  gradingSystem,
  timetableIdPerYear,
  subscribedTimetableIdsPerYear,
  agendaOptions,
  timetableOptions,
}

extension PropertyExt on Property {
  String get tag {
    switch (this) {
      case Property.schoolYearId:
        return 'schoolYear';
      case Property.semesterIdPerYear:
        return 'semesterOfYear';
      case Property.autoSemesterChange:
        return 'autoSemesterChange';
      case Property.analyticsEnabled:
        return 'analyticsEnabled';
      case Property.diagnosisEnabled:
        return 'diagnosisEnabled';
      case Property.personalizedAdsEnabled:
        return 'personalizedAdsEnabled';
      case Property.gradingSystem:
        return 'gradingSystem';
      case Property.timetableIdPerYear:
        return 'timetableOfYear';
      case Property.subscribedTimetableIdsPerYear:
        return 'subscribedTimetableIdsOfYear';
      case Property.agendaOptions:
        return 'agendaOptions';
      case Property.timetableOptions:
        return 'timetableOptions';
    }

    throw ArgumentError('Unsupported property: $this');
  }

  static Property forTag(String tag) {
    for (Property property in Property.values) {
      if (property.tag == tag) return property;
    }
    return null;
  }
}
