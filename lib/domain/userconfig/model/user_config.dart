import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/extensions/map_ext.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/userconfig/model/agenda_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/timetable_notification_options.dart';

class UserConfig extends Equatable {
  final DateTime updatedAt;
  final String schoolYearId;
  final Map<String, String> semesterIdPerYear;
  final bool autoSemesterChange;
  final bool analyticsEnabled;
  final bool diagnosisEnabled;
  final bool personalizedAdsEnabled;
  final GradingSystem gradingSystem;
  final Map<String, String> timetableIdPerYear;
  final Map<String, List<String>> subscribedTimetablesPerYear;
  final AgendaNotificationOptions agendaOptions;
  final TimetableNotificationOptions timetableOptions;

  const UserConfig({
    @required this.updatedAt,
    @required this.schoolYearId,
    @required this.semesterIdPerYear,
    @required this.autoSemesterChange,
    @required this.analyticsEnabled,
    @required this.diagnosisEnabled,
    @required this.personalizedAdsEnabled,
    @required this.gradingSystem,
    @required this.timetableIdPerYear,
    @required this.subscribedTimetablesPerYear,
    @required this.agendaOptions,
    @required this.timetableOptions,
  }) : assert(updatedAt != null);

  /// Resolves a new [UserConfig] from [this] and [other] config.
  /// The config will contain properties from both objects, whatever is newer.
  UserConfig mergeWith(UserConfig other) {
    UserConfig newer;
    UserConfig older;

    if (updatedAt.isAfter(other.updatedAt)) {
      newer = this;
      older = other;
    } else {
      newer = other;
      older = this;
    }

    final Map<String, String> semesterIdPerYear = Map.from(older.semesterIdPerYear ?? {});
    semesterIdPerYear.putAll(newer.semesterIdPerYear ?? {});

    final Map<String, String> timetableIdPerYear = Map.from(older.timetableIdPerYear ?? {});
    timetableIdPerYear.putAll(newer.timetableIdPerYear ?? {});

    final Map<String, List<String>> subscribedTimetablesPerYear = Map.from(older.subscribedTimetablesPerYear ?? {});
    subscribedTimetablesPerYear.putAll(newer.subscribedTimetablesPerYear ?? {});

    return UserConfig(
      updatedAt: newer.updatedAt,
      schoolYearId: newer.schoolYearId ?? older.schoolYearId,
      semesterIdPerYear: semesterIdPerYear,
      autoSemesterChange: newer.autoSemesterChange ?? older.autoSemesterChange,
      analyticsEnabled: newer.analyticsEnabled ?? older.analyticsEnabled,
      diagnosisEnabled: newer.diagnosisEnabled ?? older.diagnosisEnabled,
      personalizedAdsEnabled: newer.personalizedAdsEnabled ?? older.personalizedAdsEnabled,
      gradingSystem: newer.gradingSystem ?? older.gradingSystem,
      timetableIdPerYear: timetableIdPerYear,
      subscribedTimetablesPerYear: subscribedTimetablesPerYear,
      agendaOptions: newer.agendaOptions ?? older.agendaOptions,
      timetableOptions: newer.timetableOptions ?? older.timetableOptions,
    );
  }

  UserConfig withUpdatedAt(DateTime updatedAt) {
    return UserConfig(
      updatedAt: updatedAt,
      schoolYearId: schoolYearId,
      semesterIdPerYear: semesterIdPerYear,
      autoSemesterChange: autoSemesterChange,
      analyticsEnabled: analyticsEnabled,
      diagnosisEnabled: diagnosisEnabled,
      personalizedAdsEnabled: personalizedAdsEnabled,
      gradingSystem: gradingSystem,
      timetableIdPerYear: timetableIdPerYear,
      subscribedTimetablesPerYear: subscribedTimetablesPerYear,
      agendaOptions: agendaOptions,
      timetableOptions: timetableOptions,
    );
  }

  @override
  List<Object> get props => [
        updatedAt,
        schoolYearId,
        semesterIdPerYear,
        autoSemesterChange,
        analyticsEnabled,
        diagnosisEnabled,
        personalizedAdsEnabled,
        gradingSystem,
        timetableIdPerYear,
        subscribedTimetablesPerYear,
        agendaOptions,
        timetableOptions,
      ];
}
