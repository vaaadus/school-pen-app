import 'package:equatable/equatable.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/optional.dart';

class AgendaNotificationOptions extends Equatable {
  final bool enabled;
  final Time time;
  final List<Weekday> weekdays;
  final bool notifyIfNoEvents;

  const AgendaNotificationOptions({
    this.enabled = true,
    this.time = const Time(hour: 8, minute: 0),
    this.weekdays = Weekday.values,
    this.notifyIfNoEvents = true,
  })  : assert(enabled != null),
        assert(time != null),
        assert(weekdays != null),
        assert(notifyIfNoEvents != null);

  factory AgendaNotificationOptions.fromJson(Map<String, dynamic> data) {
    if (data == null) return null;

    return AgendaNotificationOptions(
      enabled: data['enabled'],
      time: Time.parse(data['time']),
      weekdays: Weekday.fromTags(data['weekdays']),
      notifyIfNoEvents: data['notifyIfNoEvents'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'enabled': enabled,
      'time': time.toString(),
      'weekdays': Weekday.toTags(weekdays),
      'notifyIfNoEvents': notifyIfNoEvents,
    };
  }

  AgendaNotificationOptions copyWith({
    Optional<bool> enabled,
    Optional<Time> time,
    Optional<List<Weekday>> weekdays,
    Optional<bool> notifyIfNoEvents,
  }) {
    return AgendaNotificationOptions(
      enabled: Optional.unwrapOrElse(enabled, this.enabled),
      time: Optional.unwrapOrElse(time, this.time),
      weekdays: Optional.unwrapOrElse(weekdays, this.weekdays),
      notifyIfNoEvents: Optional.unwrapOrElse(notifyIfNoEvents, this.notifyIfNoEvents),
    );
  }

  @override
  List<Object> get props => [enabled, time, weekdays, notifyIfNoEvents];
}
