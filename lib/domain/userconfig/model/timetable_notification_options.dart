import 'package:equatable/equatable.dart';
import 'package:school_pen/domain/common/optional.dart';

class TimetableNotificationOptions extends Equatable {
  final bool enabled;
  final int notifyBeforeLessonInMinutes;

  const TimetableNotificationOptions({
    this.enabled = true,
    this.notifyBeforeLessonInMinutes = 15,
  });

  factory TimetableNotificationOptions.fromJson(Map<String, dynamic> data) {
    if (data == null) return null;

    return TimetableNotificationOptions(
      enabled: data['enabled'],
      notifyBeforeLessonInMinutes: data['notifyBeforeLessonInMinutes'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'enabled': enabled,
      'notifyBeforeLessonInMinutes': notifyBeforeLessonInMinutes,
    };
  }

  TimetableNotificationOptions copyWith({
    Optional<bool> enabled,
    Optional<int> notifyBeforeLessonInMinutes,
  }) {
    return TimetableNotificationOptions(
      enabled: Optional.unwrapOrElse(enabled, this.enabled),
      notifyBeforeLessonInMinutes: Optional.unwrapOrElse(notifyBeforeLessonInMinutes, this.notifyBeforeLessonInMinutes),
    );
  }

  @override
  List<Object> get props => [enabled, notifyBeforeLessonInMinutes];
}
