import 'push_notification_listener.dart';

abstract class PushNotificationManager {
  /// Configures the manager, must be called as early as possible.
  Future<void> initialize();

  void addPushNotificationListener(PushNotificationListener listener);

  void removePushNotificationListener(PushNotificationListener listener);
}
