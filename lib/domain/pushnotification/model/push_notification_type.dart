enum PushNotificationType {
  /// A notification that the school timetable has
  /// been changed and requires to be refreshed.
  ///
  /// The notification with this type will contain schoolId.
  schoolTimetableChanged,

  /// The push notification type wasn't recognized.
  unknown,
}

extension PushNotificationTypeExt on PushNotificationType {
  /// Maps a string tag to a push notification type.
  static PushNotificationType forTag(String tag) {
    for (var type in PushNotificationType.values) {
      if (type.tag.toLowerCase() == tag?.toLowerCase()) {
        return type;
      }
    }
    return PushNotificationType.unknown;
  }

  /// Returns a technical tag for the push notification type.
  String get tag {
    switch (this) {
      case PushNotificationType.schoolTimetableChanged:
        return 'school_timetable_changed';
      case PushNotificationType.unknown:
        return 'unknown';
    }
    throw ArgumentError('Not supported notification type: $this');
  }
}
