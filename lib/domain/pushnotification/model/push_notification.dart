import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification_payload.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification_type.dart';

/// Represents a raw push notification.
class PushNotification extends Equatable {
  final PushNotificationType type;
  final PushNotificationPayload payload;

  const PushNotification({@required this.type, @required this.payload});

  @override
  List<Object> get props => [type, payload];
}
