import 'package:equatable/equatable.dart';

/// The notification topic a user can subscribe to and receive events.
class NotificationTopic extends Equatable {
  final String tag;

  NotificationTopic._(this.tag);

  factory NotificationTopic.forTag(String tag) {
    if (SchoolTimetableNotificationTopic.matchesTag(tag)) {
      return SchoolTimetableNotificationTopic._(tag);
    }
    return NotificationTopic._(tag);
  }

  @override
  List<Object> get props => [tag];
}

/// An exact notification topic about school timetable changes.
class SchoolTimetableNotificationTopic extends NotificationTopic {
  static const String prefix = 'school_timetable_';

  SchoolTimetableNotificationTopic._(String tag) : super._(tag);

  factory SchoolTimetableNotificationTopic.forSchoolId(String schoolId) {
    return SchoolTimetableNotificationTopic._('$prefix$schoolId');
  }

  static bool matchesTag(String tag) {
    return tag != null && tag.startsWith(RegExp(prefix));
  }
}
