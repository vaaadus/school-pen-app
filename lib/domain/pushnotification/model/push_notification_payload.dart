import 'package:equatable/equatable.dart';

/// Extra data passed to the notification,
/// received when notification is opened.
class PushNotificationPayload extends Equatable {
  final String schoolId;

  const PushNotificationPayload({this.schoolId});

  @override
  List<Object> get props => [schoolId];
}
