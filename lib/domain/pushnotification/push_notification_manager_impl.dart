import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/pushnotification/engine/push_notification_engine.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification_type.dart';
import 'package:school_pen/domain/pushnotification/repository/push_notification_repository.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/user_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';

import 'model/push_notification.dart';
import 'push_notification_listener.dart';
import 'push_notification_manager.dart';

class PushNotificationManagerImpl implements PushNotificationManager, LifecycleListener, UserListener {
  static const Logger _logger = Logger('PushNotificationManagerImpl');
  static const String _keyPeriodicUpdate = 'key_parseInstallation_periodicUpdate_slidingWindow';
  static const Duration _periodicUpdateInterval = Duration(days: 1);

  final Set<PushNotificationListener> _listeners = {};
  final PushNotificationEngine _engine;
  final PushNotificationRepository _repository;
  final TimetableManager _timetableManager;
  final UserManager _userManager;
  final LifecycleManager _lifecycleManager;
  final SlidingWindow _slidingWindow;

  PushNotificationManagerImpl(
    this._engine,
    this._repository,
    this._timetableManager,
    this._userManager,
    this._lifecycleManager,
    this._slidingWindow,
  ) {
    // _timetableManager.addConfigListener(this);
    _userManager.addUserListener(this);
    _lifecycleManager.addLifecycleListener(this);
  }

  @override
  Future<void> initialize() async {
    await _engine.initialize();
    _engine.onTokenRefresh.listen(onTokenChanged);
    _engine.onPushNotification.listen(onPushNotification);
  }

  @override
  void addPushNotificationListener(PushNotificationListener listener) {
    _listeners.add(listener);
  }

  @override
  void removePushNotificationListener(PushNotificationListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<void> onResume() async {
    if (_slidingWindow.isPassed(key: _keyPeriodicUpdate, nextWindow: _periodicUpdateInterval)) {
      await _tryUpdateInstallation();
    }
  }

  @override
  void onPause() {
    // do nothing
  }

  // @override
  // Future<void> onTimetableConfigured(String schoolId, String timetableId) async {
  //   _logger.log('onTimetableConfigured: $schoolId');
  //   await _tryResubscribeToSchoolTimetableChangesOf(schoolId);
  // }

  Future<void> onTokenChanged(String token) async {
    _logger.log('onTokenRefresh: $token');
    // _tryResubscribeToSchoolTimetableChangesOf(_timetableManager.configuredSchoolId);
    await _tryUpdateInstallation();
  }

  Future<void> onPushNotification(PushNotification notification) async {
    _logger.log('onReceivePushNotification: $notification');

    if (notification.type == PushNotificationType.unknown) {
      _logger.log('onReceivePushNotification: skipping unknown push');
      return;
    }

    for (PushNotificationListener listener in _listeners) {
      listener.onPushNotification(notification);
    }
  }

  @override
  Future<void> onUserChanged(User user) async {
    await _tryUpdateInstallation();
  }

  // Future<void> _tryResubscribeToSchoolTimetableChangesOf(String schoolId) async {
  //   try {
  //     await _resubscribeToSchoolTimetableChangesOf(schoolId);
  //   } catch (error, stackTrace) {
  //     _logger.logError(error, stackTrace);
  //   }
  // }
  //
  // Future<void> _resubscribeToSchoolTimetableChangesOf(String schoolId) async {
  //   _logger.log('resubscribeToSchoolTimetableChangesOf: $schoolId');
  //
  //   await _ensureInstallationExists();
  //
  //   if (schoolId != null) {
  //     var topic = SchoolTimetableNotificationTopic.forSchoolId(schoolId);
  //     await _subscribeTopic(topic);
  //     await _unsubscribeAllTopicsMatching((it) {
  //       return it != topic && it is SchoolTimetableNotificationTopic;
  //     });
  //   } else {
  //     await _unsubscribeAllTopicsMatching((it) {
  //       return it is SchoolTimetableNotificationTopic;
  //     });
  //   }
  // }
  //
  // Future<void> _subscribeTopic(NotificationTopic topic) async {
  //   _logger.log('subscribeTopic: ${topic.tag}');
  //   await _repository.subscribeTopic(topic);
  // }
  //
  // Future<void> _unsubscribeTopic(NotificationTopic topic) async {
  //   _logger.log('unsubscribeTopic: ${topic.tag}');
  //   await _repository.unsubscribeTopic(topic);
  // }
  //
  // Future<void> _unsubscribeAllTopicsMatching(bool Function(NotificationTopic) predicate) async {
  //   final List<Future<void>> futures = [];
  //   for (var topic in await _repository.getSubscribedTopics()) {
  //     if (predicate(topic)) {
  //       futures.add(_unsubscribeTopic(topic));
  //     }
  //   }
  //   return Future.wait(futures);
  // }

  // Future<void> _ensureInstallationExists() async {
  //   await _tryUpdateInstallation();
  // }

  Future<void> _tryUpdateInstallation() async {
    try {
      await _updateInstallation();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<void> _updateInstallation() async {
    await _repository.updateInstallation(
      token: await _engine.token,
      pushEngine: _engine.name,
      user: await _userManager.getCurrentUser(),
    );

    _slidingWindow.restart(key: _keyPeriodicUpdate);
  }
}
