import 'package:school_pen/domain/pushnotification/push_notification_listener.dart';
import 'package:school_pen/domain/pushnotification/push_notification_manager.dart';

// No-op implementation.
class EmptyPushNotificationManager implements PushNotificationManager {
  @override
  Future<void> initialize() async {
    // do nothing
  }

  @override
  void addPushNotificationListener(PushNotificationListener listener) {
    // do nothing
  }

  @override
  void removePushNotificationListener(PushNotificationListener listener) {
    // do nothing
  }
}
