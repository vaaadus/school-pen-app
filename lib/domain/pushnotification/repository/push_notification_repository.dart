import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/pushnotification/model/notification_topic.dart';
import 'package:school_pen/domain/user/model/user.dart';

abstract class PushNotificationRepository {
  /// Returns a list of all subscribed topics.
  Future<List<NotificationTopic>> getSubscribedTopics();

  Future<void> subscribeTopic(NotificationTopic topic);

  Future<void> unsubscribeTopic(NotificationTopic topic);

  /// Creates or updates the installation. Must be called before subscribing/unsubscribing from a topic.
  Future<void> updateInstallation({@required String token, @required String pushEngine, @required User user});
}
