import 'package:flutter/foundation.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_installation_ext.dart';
import 'package:school_pen/domain/pushnotification/model/notification_topic.dart';
import 'package:school_pen/domain/pushnotification/repository/push_notification_repository.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:synchronized/synchronized.dart';

// TODO: optimize topic subscriptions, do not subscribe if subscribed
class ParsePushNotificationRepository implements PushNotificationRepository {
  final Lock _lock = Lock();

  @override
  Future<List<NotificationTopic>> getSubscribedTopics() async {
    return _lock.synchronized(() async {
      final ParseInstallation installation = await ParseInstallation.currentInstallation();
      if (installation.createdAt == null) await installation.save();

      final List<dynamic> channels = await installation.getSubscribedChannels();
      if (channels == null) return [];

      return channels.map((e) => NotificationTopic.forTag(e.toString())).toList();
    });
  }

  @override
  Future<void> subscribeTopic(NotificationTopic topic) async {
    await _lock.synchronized(() async {
      final ParseInstallation installation = await ParseInstallation.currentInstallation();
      if (installation.createdAt == null) {
        throw Exception('Installation must be created before the topic can be subscribed');
      }

      await installation.subscribeToChannel(topic.tag);
    });
  }

  @override
  Future<void> unsubscribeTopic(NotificationTopic topic) async {
    await _lock.synchronized(() async {
      final ParseInstallation installation = await ParseInstallation.currentInstallation();
      if (installation.createdAt == null) {
        throw Exception('Installation must be created before the topic can be unsubscribed');
      }

      await installation.unsubscribeFromChannel(topic.tag);
    });
  }

  @override
  Future<void> updateInstallation({@required String token, @required String pushEngine, @required User user}) async {
    await _lock.synchronized(() async {
      final ParseInstallation installation = await ParseInstallation.currentInstallation();
      final ParseUser parseUser = await ParseUser.currentUser();
      bool hasChanged = false;

      if (user?.id != parseUser?.objectId) {
        throw Exception('Only a single user supported!');
      }

      if (installation.user?.objectId != parseUser?.objectId) {
        installation.user = parseUser;
        hasChanged = true;
      }

      if (installation.deviceToken != token || installation.pushEngine != pushEngine) {
        installation.deviceToken = token;
        installation.pushEngine = pushEngine;
        hasChanged = true;
      }

      if (hasChanged) {
        await installation.save();
      }
    });
  }
}
