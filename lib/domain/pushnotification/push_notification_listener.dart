import 'package:school_pen/domain/pushnotification/model/push_notification.dart';

abstract class PushNotificationListener {
  /// Called when the app has received the push notification.
  void onPushNotification(PushNotification notification);
}
