import 'package:school_pen/domain/pushnotification/model/push_notification.dart';

abstract class PushNotificationEngine {
  /// Returns the engine name. It must describe the underlying
  /// framework used to deliver pushes such as FCM or HMS.
  String get name;

  /// Push notification device token.
  Future<String> get token;

  /// A stream with changes of the underlying push notification device token.
  Stream<String> get onTokenRefresh;

  /// A stream with received push notifications.
  Stream<PushNotification> get onPushNotification;

  /// Initialize the underlying plugin, must be called before any usage.
  Future<void> initialize();
}
