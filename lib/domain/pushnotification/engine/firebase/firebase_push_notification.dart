import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification_payload.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification_type.dart';

/// Represents a raw push notification.
///
/// On Android, the message contains an additional field data containing
/// the data. On iOS, the data is directly appended to the message
/// and the additional data-field is omitted.
class FirebasePushNotification extends Equatable {
  final PushNotificationType type;
  final PushNotificationPayload payload;

  const FirebasePushNotification({@required this.type, @required this.payload});

  /// Decodes the push notification from a raw firebase remote message.
  factory FirebasePushNotification.decode(Map<String, dynamic> remoteMessage) {
    return FirebasePushNotification(
      type: _decodeType(remoteMessage),
      payload: PushNotificationPayload(
        schoolId: _decodeSchoolId(remoteMessage),
      ),
    );
  }

  static PushNotificationType _decodeType(Map<String, dynamic> remoteMessage) {
    final data = remoteMessage['data'] ?? remoteMessage;
    return PushNotificationTypeExt.forTag(data['notification_type']);
  }

  static String _decodeSchoolId(Map<String, dynamic> remoteMessage) {
    final data = remoteMessage['data'] ?? remoteMessage;
    return data['school_id'];
  }

  PushNotification get asModel {
    return PushNotification(
      type: type,
      payload: payload,
    );
  }

  @override
  List<Object> get props => [type, payload];
}
