import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/domain/pushnotification/engine/firebase/firebase_push_notification.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification.dart';

import '../push_notification_engine.dart';

class FirebasePushNotificationEngine implements PushNotificationEngine {
  final BehaviorSubject<String> _onTokenRefresh = BehaviorSubject();
  final BehaviorSubject<PushNotification> _onPushNotification = BehaviorSubject();
  final FirebaseMessaging _plugin = FirebaseMessaging.instance;

  FirebasePushNotificationEngine();

  @override
  String get name => 'fcm';

  @override
  Future<String> get token => _plugin.getToken();

  @override
  Stream<String> get onTokenRefresh => _onTokenRefresh.stream;

  @override
  Stream<PushNotification> get onPushNotification => _onPushNotification.stream;

  @override
  Future<void> initialize() async {
    FirebaseMessaging.onMessage.listen(_handlePushNotification);
    FirebaseMessaging.onMessageOpenedApp.listen(_handlePushNotification);
    FirebaseMessaging.onBackgroundMessage(_handlePushNotification);

    _plugin.onTokenRefresh.listen((String token) {
      _onTokenRefresh.add(token);
    });
  }

  static Future<void> _handlePushNotification(RemoteMessage message) async {
    final PushNotificationEngine repository = Injection.get<PushNotificationEngine>();
    final FirebasePushNotificationEngine firebase = repository as FirebasePushNotificationEngine;
    final FirebasePushNotification notification = FirebasePushNotification.decode(message.data);
    firebase._onPushNotification.add(notification.asModel);
  }
}
