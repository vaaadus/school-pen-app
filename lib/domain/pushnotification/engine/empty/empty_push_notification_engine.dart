import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/pushnotification/engine/push_notification_engine.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification.dart';

class EmptyPushNotificationEngine implements PushNotificationEngine {
  @override
  String get name => 'empty';

  @override
  Future<String> get token async => 'empty';

  @override
  Stream<String> get onTokenRefresh => BehaviorSubject();

  @override
  Stream<PushNotification> get onPushNotification => BehaviorSubject();

  @override
  Future<void> initialize() async {
    // do nothing
  }
}
