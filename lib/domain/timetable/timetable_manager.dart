import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/timetable_listener.dart';

import 'model/school.dart';
import 'model/timetable.dart';

/// Manages user timetables.
///
/// Two kinds are supported: public and custom.
/// Users can subscribe to a public timetable (but can't edit)
/// or they can create their own timetables (custom).
abstract class TimetableManager {
  /// Register for changes in timetable manager.
  void addTimetableListener(TimetableListener listener, {bool notifyOnAttach = true});

  /// Unregister from timetable manager changes.
  void removeTimetableListener(TimetableListener listener);

  /// Returns all public schools.
  Future<List<School>> getSchools();

  /// Returns all public timetables of a school by [schoolId].
  Future<List<TimetableInfo>> getTimetablesInfoOfSchool(String schoolId);

  /// Returns all subscribed or created timetables by the user.
  Future<List<TimetableInfo>> getUserTimetablesInfo();

  /// Returns all subscribed or created timetables by the user.
  Future<List<Timetable>> getUserTimetables();

  /// Returns a given timetable (either public or custom) by [timetableId].
  Future<Timetable> getTimetable(String timetableId);

  /// Returns a given timetable info (either public or custom) by [timetableId].
  Future<TimetableInfo> getTimetableInfo(String timetableId);

  /// Returns the id of the active timetable (either public or custom) in the current school year.
  Future<String> getActiveTimetableId();

  /// Returns the active timetable info (either public or custom) in the current school year.
  Future<TimetableInfo> getActiveTimetableInfo();

  /// Returns an active timetable (either public or custom) in the current school year.
  /// Returns future that resolves to null if there is no active timetable.
  ///
  /// If the there is no active timetable but user has created or subscribed to
  /// some timetables, then the first available timetable will be selected.
  Future<Timetable> getActiveTimetable();

  /// Sets the active timetable in the current school year.
  Future<void> setActiveTimetable(String timetableId);

  /// Unsubscribes from the active public timetable.
  /// Selects the first [getUserTimetablesInfo] as the active timetable or null if there are no timetables.
  Future<void> unsubscribeAndResolveNewActiveTimetable();

  /// Subscribes to a public timetable. User wants to consume this timetable
  Future<void> subscribeToPublicTimetable(String timetableId);

  /// Unsubscribes from a public timetable. User no longer wants to consume this timetable
  Future<void> unsubscribeFromPublicTimetable(String timetableId);

  /// Creates a new custom timetable and returns its ID.
  Future<String> createTimetable(AddTimetable timetable);

  /// Creates a copy of a custom [timetable] and assigns a new name to it.
  /// Sets [dateTime] as updatedAt for timetable & events.
  /// Returns the ID of the newly created timetable.
  Future<String> duplicateTimetable(Timetable timetable, DateTime dateTime, {String newName});

  /// Renames a custom timetable.
  /// Sets [dateTime] as updatedAt for timetable.
  Future<void> renameTimetable(String id, String newName, DateTime dateTime);

  /// Deletes a custom timetable in the current school year.
  /// Selects the first [getUserTimetablesInfo] as the active timetable or null if there are no timetables.
  Future<void> deleteAndResolveNewActiveTimetable(String timetableId);

  /// Returns a custom [TimetableEvent] by [eventId] or null if it doesn't exist.
  Future<TimetableEvent> getTimetableEventById(String eventId);

  /// Calculates & caches [SingleTimetableEvent]s.
  Future<List<SingleTimetableEvent>> getSingleEventsOf(Timetable timetable, DateTimeRange dateRange);

  /// Creates a new timetable event and returns its ID.
  Future<String> createTimetableEvent(AddTimetableEvent event);

  /// Updates an [eventId] with [event] at [recurrenceRange] and [editMode] update preference.
  Future<void> updateTimetableEvent(
      String eventId, AddTimetableEvent event, DateTimeRange recurrenceRange, EventRepeatOptionsEditMode editMode);

  /// Deletes the timetable event with [eventId].
  Future<void> deleteTimetableEvent(String eventId, DateTimeRange recurrenceRange, EventRepeatOptionsEditMode editMode);
}
