import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/repository/common/timetable_serializer.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

class SembastPublicTimetableRepository {
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('publicTimetable');
  final StoreRef<String, Map<String, dynamic>> _infoStore = stringMapStoreFactory.store('publicTimetableInfo');
  final Lock _lock = Lock();

  Future<List<TimetableInfo>> getTimetablesInfoOfIds(List<String> ids) async {
    if (ids == null || ids.isEmpty) return [];

    return _lock.synchronized(() async {
      final List<Map<String, dynamic>> records = await _infoStore.records(ids).get(await _db);
      return records.map((e) => TimetableSerializer.deserializeTimetableInfo(e)).withoutNulls().toList();
    });
  }

  Future<void> storeTimetablesInfo(List<TimetableInfo> list) async {
    if (list == null || list.isEmpty) return;

    return _lock.synchronized(() async {
      final List<String> keys = list.map((e) => e.id).toList();
      final List<Map<String, dynamic>> value = TimetableSerializer.serializeTimetablesInfo(list);
      await _infoStore.records(keys).put(await _db, value);
    });
  }

  Future<Timetable> getTimetable(String timetableId) async {
    if (timetableId == null) return null;

    return _lock.synchronized(() async {
      final Map<String, dynamic> record = await _store.record(timetableId).get(await _db);
      return TimetableSerializer.deserializeTimetable(record);
    });
  }

  Future<TimetableInfo> getTimetableInfo(String timetableId) async {
    if (timetableId == null) return null;

    return _lock.synchronized(() async {
      final Map<String, dynamic> record = await _store.record(timetableId).get(await _db);
      return TimetableSerializer.deserializeTimetableInfo(record);
    });
  }

  Future<void> storeTimetables(List<Timetable> list, {bool clearAllRecords = false}) {
    return _lock.synchronized(() async {
      if (clearAllRecords) {
        await Future.wait([
          _store.delete(await _db),
          _infoStore.delete(await _db),
        ]);
      }

      if (list == null || list.isEmpty) return;

      final List<String> keys = list.map((e) => e.id).toList();
      await Future.wait([
        _store.records(keys).put(await _db, TimetableSerializer.serializeTimetables(list)),
        _infoStore.records(keys).put(await _db, TimetableSerializer.serializeTimetablesAsInfo(list)),
      ]);
    });
  }

  Future<void> setTimetable(Timetable timetable) async {
    if (timetable == null) return;

    return _lock.synchronized(() async {
      await Future.wait([
        _store.record(timetable.id).put(await _db, TimetableSerializer.serializeTimetable(timetable)),
        _infoStore.record(timetable.id).put(await _db, TimetableSerializer.serializeTimetableAsInfo(timetable)),
      ]);
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}
