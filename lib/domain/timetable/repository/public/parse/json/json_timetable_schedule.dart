import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';

part 'json_timetable_schedule.g.dart';

@JsonSerializable()
class JsonTimetableSchedule {
  final List<JsonLesson> mon;
  final List<JsonLesson> tue;
  final List<JsonLesson> wed;
  final List<JsonLesson> thu;
  final List<JsonLesson> fri;
  final List<JsonLesson> sat;
  final List<JsonLesson> sun;

  const JsonTimetableSchedule({
    @required this.mon,
    @required this.tue,
    @required this.wed,
    @required this.thu,
    @required this.fri,
    @required this.sat,
    @required this.sun,
  });

  factory JsonTimetableSchedule.fromJson(Map<String, dynamic> json) => _$JsonTimetableScheduleFromJson(json);

  Map<String, dynamic> toJson() => _$JsonTimetableScheduleToJson(this);

  List<TimetableEvent> toEvents({@required String timetableId, @required DateTime updatedAt}) => [
        ..._mapLessonsToEvents(mon, Weekday.monday, timetableId, updatedAt),
        ..._mapLessonsToEvents(tue, Weekday.tuesday, timetableId, updatedAt),
        ..._mapLessonsToEvents(wed, Weekday.wednesday, timetableId, updatedAt),
        ..._mapLessonsToEvents(thu, Weekday.thursday, timetableId, updatedAt),
        ..._mapLessonsToEvents(fri, Weekday.friday, timetableId, updatedAt),
        ..._mapLessonsToEvents(sat, Weekday.saturday, timetableId, updatedAt),
        ..._mapLessonsToEvents(sun, Weekday.sunday, timetableId, updatedAt),
      ];

  List<TimetableEvent> _mapLessonsToEvents(
      List<JsonLesson> lessons, Weekday weekday, String timetableId, DateTime updatedAt) {
    return [
      for (JsonLesson lesson in lessons) ..._mapLessonToEvents(lesson, weekday, timetableId, updatedAt),
    ];
  }

  List<TimetableEvent> _mapLessonToEvents(JsonLesson lesson, Weekday weekday, String timetableId, DateTime updatedAt) {
    return [
      for (JsonLessonGroup group in lesson.groups) _mapGroupToEvent(lesson, group, weekday, timetableId, updatedAt),
    ];
  }

  TimetableEvent _mapGroupToEvent(
      JsonLesson lesson, JsonLessonGroup group, Weekday weekday, String timetableId, DateTime updatedAt) {
    final DateTime dateTime = DateTime.utc(2010).adjustToNextWeekday(weekday);
    return TimetableEvent(
      id: Id.random(),
      timetableId: timetableId,
      customRoom: group.room,
      customTeacher: group.teacher,
      customCourse: group.subject,
      customStudent: group.student,
      repeatOptions: EventRepeatOptions(
        type: EventRepeatOptionsType.weekly,
        dateRange: DateTimeRange(
          start: dateTime.withTime(lesson.start),
          end: dateTime.withTime(lesson.end),
        ),
        weeklyRepeatsOn: [weekday],
      ),
      updatedAt: updatedAt,
    );
  }
}

@JsonSerializable()
class JsonLesson {
  final int num;
  @JsonKey(toJson: Time.format, fromJson: Time.parse)
  final Time start;
  @JsonKey(toJson: Time.format, fromJson: Time.parse)
  final Time end;
  final List<JsonLessonGroup> groups;

  const JsonLesson({
    @required this.num,
    @required this.start,
    @required this.end,
    @required this.groups,
  });

  factory JsonLesson.fromJson(Map<String, dynamic> json) => _$JsonLessonFromJson(json);

  Map<String, dynamic> toJson() => _$JsonLessonToJson(this);
}

@JsonSerializable()
class JsonLessonGroup {
  final String student;
  final String teacher;
  final String subject;
  final String room;

  const JsonLessonGroup({
    @required this.student,
    @required this.teacher,
    @required this.subject,
    @required this.room,
  });

  factory JsonLessonGroup.fromJson(Map<String, dynamic> json) => _$JsonLessonGroupFromJson(json);

  Map<String, dynamic> toJson() => _$JsonLessonGroupToJson(this);
}
