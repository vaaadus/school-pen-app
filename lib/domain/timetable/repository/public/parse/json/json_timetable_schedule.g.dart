// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_timetable_schedule.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonTimetableSchedule _$JsonTimetableScheduleFromJson(
    Map<String, dynamic> json) {
  return JsonTimetableSchedule(
    mon: (json['mon'] as List)
        ?.map((e) =>
            e == null ? null : JsonLesson.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    tue: (json['tue'] as List)
        ?.map((e) =>
            e == null ? null : JsonLesson.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    wed: (json['wed'] as List)
        ?.map((e) =>
            e == null ? null : JsonLesson.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    thu: (json['thu'] as List)
        ?.map((e) =>
            e == null ? null : JsonLesson.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    fri: (json['fri'] as List)
        ?.map((e) =>
            e == null ? null : JsonLesson.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    sat: (json['sat'] as List)
        ?.map((e) =>
            e == null ? null : JsonLesson.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    sun: (json['sun'] as List)
        ?.map((e) =>
            e == null ? null : JsonLesson.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonTimetableScheduleToJson(
        JsonTimetableSchedule instance) =>
    <String, dynamic>{
      'mon': instance.mon,
      'tue': instance.tue,
      'wed': instance.wed,
      'thu': instance.thu,
      'fri': instance.fri,
      'sat': instance.sat,
      'sun': instance.sun,
    };

JsonLesson _$JsonLessonFromJson(Map<String, dynamic> json) {
  return JsonLesson(
    num: json['num'] as int,
    start: Time.parse(json['start'] as String),
    end: Time.parse(json['end'] as String),
    groups: (json['groups'] as List)
        ?.map((e) => e == null
            ? null
            : JsonLessonGroup.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonLessonToJson(JsonLesson instance) =>
    <String, dynamic>{
      'num': instance.num,
      'start': Time.format(instance.start),
      'end': Time.format(instance.end),
      'groups': instance.groups,
    };

JsonLessonGroup _$JsonLessonGroupFromJson(Map<String, dynamic> json) {
  return JsonLessonGroup(
    student: json['student'] as String,
    teacher: json['teacher'] as String,
    subject: json['subject'] as String,
    room: json['room'] as String,
  );
}

Map<String, dynamic> _$JsonLessonGroupToJson(JsonLessonGroup instance) =>
    <String, dynamic>{
      'student': instance.student,
      'teacher': instance.teacher,
      'subject': instance.subject,
      'room': instance.room,
    };
