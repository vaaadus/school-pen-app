import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/common/exception/not_found_exception.dart';
import 'package:school_pen/domain/common/parse/parse_error_type.dart';
import 'package:school_pen/domain/network/network_manager.dart';
import 'package:school_pen/domain/timetable/model/school.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/repository/public/parse/parse_public_school.dart';
import 'package:school_pen/domain/timetable/repository/public/parse/parse_public_timetable.dart';

class ParsePublicTimetableRepository {
  static const int queryMaxLimit = 1000;
  final NetworkManager _networkManager;

  ParsePublicTimetableRepository(this._networkManager);

  Future<List<School>> getSchools() async {
    final QueryBuilder<ParsePublicSchool> builder = _buildQuery(ParsePublicSchool());
    ParsePublicSchool.limitKeysToReturn(builder);

    final ParseResponse response = await builder.query();
    final List<ParsePublicSchool> parseSchools = await _unwrapParseObjects(response);
    return parseSchools.map((e) => e.toSchool()).toList();
  }

  Future<School> getSchool(String schoolId) async {
    final ParseResponse response = await ParsePublicSchool().getObject(schoolId);
    final ParsePublicSchool parseSchool = await _unwrapParseObject(response);
    return parseSchool?.toSchool();
  }

  Future<List<TimetableInfo>> getTimetablesInfoOfSchool(String schoolId) async {
    final ParsePublicSchool school = ParsePublicSchool();
    school.set(keyVarObjectId, schoolId);

    final QueryBuilder<ParsePublicTimetable> builder = _buildQuery(ParsePublicTimetable());
    builder.whereEqualTo(ParsePublicTimetable.keySchool, school);
    ParsePublicTimetable.limitInfoKeysToReturn(builder);

    final ParseResponse response = await builder.query();
    final List<ParsePublicTimetable> timetables = await _unwrapParseObjects(response);
    return timetables.map((e) => e.toTimetableInfo()).toList();
  }

  Future<List<TimetableInfo>> getTimetablesInfoOfIds(List<String> ids) async {
    if (ids == null || ids.isEmpty) return [];

    final QueryBuilder<ParsePublicTimetable> builder = _buildQuery(ParsePublicTimetable());
    builder.whereContainedIn(keyVarObjectId, ids);
    ParsePublicTimetable.limitInfoKeysToReturn(builder);

    final ParseResponse response = await builder.query();
    final List<ParsePublicTimetable> timetables = await _unwrapParseObjects(response);
    return timetables.map((e) => e.toTimetableInfo()).toList();
  }

  Future<List<Timetable>> getTimetablesOfIds(List<String> ids) async {
    if (ids == null || ids.isEmpty) return [];

    final QueryBuilder<ParsePublicTimetable> builder = _buildQuery(ParsePublicTimetable());
    builder.whereContainedIn(keyVarObjectId, ids);

    final ParseResponse response = await builder.query();
    final List<ParsePublicTimetable> timetables = await _unwrapParseObjects(response);
    return timetables.map((e) => e.toTimetable()).toList();
  }

  Future<Timetable> getTimetable(String timetableId) async {
    if (timetableId == null) return null;

    final ParseResponse response = await ParsePublicTimetable().getObject(timetableId);
    final ParsePublicTimetable parseTimetable = await _unwrapParseObject(response);
    return parseTimetable?.toTimetable();
  }

  Future<T> _unwrapParseObject<T extends ParseObject>(ParseResponse response) async {
    if (response.error?.typed == ParseErrorType.objectNotFound) return null;
    if (response.error != null) return _unwrapException(response.error);
    return response.result;
  }

  Future<List<T>> _unwrapParseObjects<T extends ParseObject>(ParseResponse response) async {
    if (response.error?.typed == ParseErrorType.noResults) return [];
    if (response.error != null) return _unwrapException(response.error);
    return response.results.cast();
  }

  Future<T> _unwrapException<T>(Object error) async {
    if (error is ParseError && error.typed == ParseErrorType.objectNotFound) {
      throw NotFoundException(error.message);
    }

    if (!await _networkManager.isNetworkAvailable()) {
      throw NoInternetException();
    }

    throw error;
  }

  QueryBuilder<T> _buildQuery<T extends ParseObject>(T object) {
    final QueryBuilder<T> builder = QueryBuilder(object);
    builder.setLimit(queryMaxLimit);
    return builder;
  }
}
