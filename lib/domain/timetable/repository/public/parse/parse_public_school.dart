import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/timetable/model/school.dart';
import 'package:school_pen/domain/timetable/model/school_address.dart';

class ParsePublicSchool extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'PublicSchool';
  static const String keyName = 'name';
  static const String keyAddress = 'address';

  ParsePublicSchool() : super(keyClassName);

  ParsePublicSchool.clone() : this();

  School toSchool() => School(
        id: objectId,
        name: name,
        address: address,
        updatedAt: updatedAt,
      );

  static void limitKeysToReturn(QueryBuilder<ParsePublicSchool> builder) {
    builder.keysToReturn([
      keyVarObjectId,
      keyName,
      keyAddress,
      keyVarUpdatedAt,
    ]);
  }

  @override
  ParsePublicSchool clone(Map map) => ParsePublicSchool.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  String get name => getNullable(keyName);

  set name(String name) => setNullable(keyName, name);

  SchoolAddress get address {
    final Map<String, dynamic> value = getNullable(keyAddress);
    if (value == null) return null;
    return SchoolAddress(country: value['country'], city: value['city']);
  }

  set address(SchoolAddress address) {
    if (address == null) {
      setNullable(keyAddress, null);
    } else {
      setNullable(keyAddress, {'country': address.country, 'city': address.city});
    }
  }
}
