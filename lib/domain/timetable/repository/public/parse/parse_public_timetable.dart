import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';
import 'package:school_pen/domain/timetable/repository/public/parse/json/json_timetable_schedule.dart';

class ParsePublicTimetable extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'PublicTimetable';
  static const String keySchool = 'school';
  static const String keyName = 'name';
  static const String keyTimetableType = 'type';
  static const String keySchedule = 'schedule';

  ParsePublicTimetable() : super(keyClassName);

  ParsePublicTimetable.clone() : this();

  Timetable toTimetable() => Timetable(
        id: objectId,
        name: name,
        type: timetableType,
        updatedAt: updatedAt,
        events: events,
      );

  TimetableInfo toTimetableInfo() => TimetableInfo(
        id: objectId,
        name: name,
        type: timetableType,
        updatedAt: updatedAt,
      );

  static void limitInfoKeysToReturn(QueryBuilder<ParsePublicTimetable> builder) {
    builder.keysToReturn([
      keyVarObjectId,
      keyName,
      keyTimetableType,
      keyVarUpdatedAt,
    ]);
  }

  @override
  ParsePublicTimetable clone(Map map) => ParsePublicTimetable.clone()..fromJson(map);

  String get name => getNullable(keyName);

  TimetableType get timetableType => TimetableTypeExt.forTag(getNullable(keyTimetableType));

  List<TimetableEvent> get events {
    final Map<String, dynamic> value = getNullable(keySchedule);
    assert(value != null);

    final JsonTimetableSchedule schedule = JsonTimetableSchedule.fromJson(value);
    return schedule.toEvents(timetableId: uuid, updatedAt: updatedAt);
  }
}
