import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/timetable/model/school.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/repository/public/parse/parse_public_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/public/public_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/public/sembast/sembast_public_timetable_repository.dart';
import 'package:synchronized/synchronized.dart';

/// A [PublicTimetableRepository] which merges results from local storage
/// and the parse server in order to try to serve first results that a
/// re cached to speed up loading.
class PublicTimetableRepositoryFacade implements PublicTimetableRepository {
  final Lock _lock = Lock();
  final SembastPublicTimetableRepository _sembast;
  final ParsePublicTimetableRepository _parse;

  PublicTimetableRepositoryFacade(this._sembast, this._parse);

  @override
  Future<List<School>> getSchools() => _parse.getSchools();

  @override
  Future<School> getSchool(String schoolId) => _parse.getSchool(schoolId);

  @override
  Future<List<TimetableInfo>> getTimetablesInfoOfSchool(String schoolId) => _parse.getTimetablesInfoOfSchool(schoolId);

  @override
  Future<List<TimetableInfo>> getTimetablesInfoOfIds(List<String> ids) {
    return _lock.synchronized(() async {
      final List<TimetableInfo> sembastList = await _sembast.getTimetablesInfoOfIds(ids);
      if (sembastList.length == ids.length) return sembastList;

      final List<String> fetchedIds = sembastList.map((e) => e.id).toList();
      final List<String> missingIds = ids.minusAll(fetchedIds);
      final List<TimetableInfo> parseList = await _parse.getTimetablesInfoOfIds(missingIds);
      await _sembast.storeTimetablesInfo(parseList);

      return [
        ...sembastList,
        ...parseList,
      ];
    });
  }

  @override
  Future<Timetable> getTimetable(String timetableId) {
    return _lock.synchronized(() async {
      Timetable result = await _sembast.getTimetable(timetableId);
      if (result == null) {
        result = await _parse.getTimetable(timetableId);
        await _sembast.setTimetable(result);
      }
      return result;
    });
  }

  @override
  Future<TimetableInfo> getTimetableInfo(String timetableId) {
    return _lock.synchronized(() async {
      TimetableInfo result = await _sembast.getTimetableInfo(timetableId);
      if (result == null) {
        final Timetable timetable = await _parse.getTimetable(timetableId);
        await _sembast.setTimetable(timetable);
        result = timetable?.toTimetableInfo();
      }
      return result;
    });
  }

  Future<void> clearAndStoreTimetables(List<Timetable> timetables) {
    return _lock.synchronized(() async {
      await _sembast.storeTimetables(timetables, clearAllRecords: true);
    });
  }
}
