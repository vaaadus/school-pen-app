import 'package:school_pen/domain/timetable/model/school.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';

abstract class PublicTimetableRepository {
  /// Fetch all public schools.
  Future<List<School>> getSchools();

  /// Fetch a public school by [schoolId].
  Future<School> getSchool(String schoolId);

  /// Fetch all timetables by [schoolId].
  Future<List<TimetableInfo>> getTimetablesInfoOfSchool(String schoolId);

  /// Fetch metadata about timetables by [ids].
  Future<List<TimetableInfo>> getTimetablesInfoOfIds(List<String> ids);

  /// Fetch the timetable with schedule by [timetableId].
  Future<Timetable> getTimetable(String timetableId);

  /// Fetch the timetable info by [timetableId].
  Future<TimetableInfo> getTimetableInfo(String timetableId);
}
