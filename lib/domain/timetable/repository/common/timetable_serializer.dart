import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';

/// Common code related to serializing [Timetable] & [TimetableInfo].
class TimetableSerializer {
  static List<Map<String, dynamic>> serializeTimetablesAsInfo(List<Timetable> list) {
    return list.map((e) => serializeTimetableAsInfo(e)).toList();
  }

  static Map<String, dynamic> serializeTimetableAsInfo(Timetable timetable) {
    if (timetable == null) return null;

    return {
      'id': timetable.id,
      'schoolYearId': timetable.schoolYearId,
      'name': timetable.name,
      'type': timetable.type.tag,
      'updatedAt': SembastSerializer.serializeDateTimeField(timetable.updatedAt),
    };
  }

  static List<Map<String, dynamic>> serializeTimetablesInfo(List<TimetableInfo> list) {
    return list.map((e) => serializeTimetableInfo(e)).toList();
  }

  static Map<String, dynamic> serializeTimetableInfo(TimetableInfo timetable) {
    if (timetable == null) return null;

    return {
      'id': timetable.id,
      'schoolYearId': timetable.schoolYearId,
      'name': timetable.name,
      'type': timetable.type.tag,
      'updatedAt': SembastSerializer.serializeDateTimeField(timetable.updatedAt),
    };
  }

  static TimetableInfo deserializeTimetableInfo(Map<String, dynamic> map) {
    if (map == null) return null;

    return TimetableInfo(
      id: map['id'],
      schoolYearId: map['schoolYearId'],
      name: map['name'],
      type: TimetableTypeExt.forTag(map['type']),
      updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
    );
  }

  static List<Map<String, dynamic>> serializeTimetables(List<Timetable> list) {
    return list.map((e) => serializeTimetable(e)).toList();
  }

  static Map<String, dynamic> serializeTimetable(Timetable timetable) {
    if (timetable == null) return null;

    return {
      'id': timetable.id,
      'schoolYearId': timetable.schoolYearId,
      'name': timetable.name,
      'type': timetable.type.tag,
      'updatedAt': SembastSerializer.serializeDateTimeField(timetable.updatedAt),
      'events': serializeTimetableEvents(timetable.events),
    };
  }

  static Timetable deserializeTimetable(Map<String, dynamic> map) {
    if (map == null) return null;

    return Timetable(
      id: map['id'],
      schoolYearId: map['schoolYearId'],
      name: map['name'],
      type: TimetableTypeExt.forTag(map['type']),
      updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
      events: TimetableSerializer.deserializeTimetableEvents(map['events']),
    );
  }

  static Map<String, dynamic> serializeAddTimetable(AddTimetable timetable) {
    if (timetable == null) return null;

    return {
      'schoolYearId': timetable.schoolYearId,
      'name': timetable.name,
      'type': timetable.type.tag,
      'updatedAt': SembastSerializer.serializeDateTimeField(timetable.updatedAt),
    };
  }

  static AddTimetable deserializeAddTimetable(Map<String, dynamic> map) {
    if (map == null) return null;

    return AddTimetable(
      schoolYearId: map['schoolYearId'],
      name: map['name'],
      type: TimetableTypeExt.forTag(map['type']),
      updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
    );
  }

  static List<dynamic> serializeTimetableEvents(List<TimetableEvent> events) {
    return events.map((e) => serializeTimetableEvent(e)).toList();
  }

  static List<TimetableEvent> deserializeTimetableEvents(List<dynamic> list) {
    if (list == null) return [];

    return list.map((e) => deserializeTimetableEvent(e)).toList();
  }

  static Map<String, dynamic> serializeTimetableEvent(TimetableEvent event) {
    if (event == null) return null;

    return {
      'id': event.id,
      'timetableId': event.timetableId,
      'courseId': event.courseId,
      'customRoom': event.customRoom,
      'customTeacher': event.customTeacher,
      'customCourse': event.customCourse,
      'customStudent': event.customStudent,
      'repeatOptions': serializeEventRepeatOptions(event.repeatOptions),
      'note': event.note,
      'updatedAt': SembastSerializer.serializeDateTimeField(event.updatedAt),
    };
  }

  static TimetableEvent deserializeTimetableEvent(Map<String, dynamic> map) {
    if (map == null) return null;

    return TimetableEvent(
      id: map['id'],
      timetableId: map['timetableId'],
      courseId: map['courseId'],
      customRoom: map['customRoom'],
      customTeacher: map['customTeacher'],
      customCourse: map['customCourse'],
      customStudent: map['customStudent'],
      repeatOptions: deserializeEventRepeatOptions(map['repeatOptions']),
      note: map['note'],
      updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
    );
  }

  static Map<String, dynamic> serializeEventRepeatOptions(EventRepeatOptions options) {
    if (options == null) return null;

    return {
      'start': SembastSerializer.serializeDateTimeField(options.dateRange.start),
      'end': SembastSerializer.serializeDateTimeField(options.dateRange.end),
      'type': options.type.tag,
      'repeatInterval': options.repeatInterval,
      'weeklyRepeatsOn': Weekday.toTags(options.weeklyRepeatsOn),
      'endsAt': SembastSerializer.serializeDateTimeField(options.endsAt),
      'endsAfterOccurrences': options.endsAfterOccurrences,
    };
  }

  static EventRepeatOptions deserializeEventRepeatOptions(Map<String, dynamic> map) {
    if (map == null) return null;

    return EventRepeatOptions(
      type: EventRepeatOptionsTypeExt.forTag(map['type']),
      dateRange: DateTimeRange(
        start: SembastSerializer.deserializeDateTimeField(map['start'], isUtc: true),
        end: SembastSerializer.deserializeDateTimeField(map['end'], isUtc: true),
      ),
      repeatInterval: map['repeatInterval'],
      weeklyRepeatsOn: Weekday.fromTags(map['weeklyRepeatsOn']),
      endsAt: SembastSerializer.deserializeDateTimeField(map['endsAt'], isUtc: true),
      endsAfterOccurrences: map['endsAfterOccurrences'],
    );
  }
}
