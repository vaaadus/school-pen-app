import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/repository/common/timetable_serializer.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/custom_timetable_event_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/custom_timetable_repository.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// A [CustomTimetableRepository] that saves timetable events in sembast document database.
class SembastCustomTimetableEventRepository implements CustomTimetableEventRepository {
  static const String keyDeletedIds = 'key_sembast_deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('customTimetableEvents');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('customTimetableEvents_bin');
  final BehaviorSubject<CustomTimetableEventRepository> _onChange = BehaviorSubject();
  final Lock _lock = Lock();

  SembastCustomTimetableEventRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<CustomTimetableEventRepository> get onChange => _onChange;

  @override
  Future<List<TimetableEvent>> getAll({String timetableId, String courseId, List<String> ids}) async {
    final List<Filter> filters = [
      if (timetableId != null) Filter.equals('timetableId', timetableId),
      if (courseId != null) Filter.equals('courseId', courseId),
      if (ids != null) Filter.inList('id', ids),
    ];

    final Finder finder = filters.isEmpty ? null : Finder(filter: Filter.and(filters));
    final List<RecordSnapshot> snapshots = await _store.find(await _db, finder: finder);
    return snapshots.map((e) => TimetableSerializer.deserializeTimetableEvent(e.value)).toList();
  }

  @override
  Future<TimetableEvent> getById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> record = await _store.record(id).get(await _db);
    return TimetableSerializer.deserializeTimetableEvent(record);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(keyDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<List<String>> createAll(List<AddTimetableEvent> events) {
    return _lock.synchronized(() async {
      final List<TimetableEvent> newEvents = events.map((e) => e.toTimetableEvent(id: Id.random())).toList();
      final List<String> ids = newEvents.map((e) => e.id).toList();
      final List<Map<String, dynamic>> values = newEvents.map(TimetableSerializer.serializeTimetableEvent).toList();

      await _store.records(ids).put(await _db, values);
      return ids;
    });
  }

  @override
  Future<String> create(AddTimetableEvent event) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = TimetableSerializer.serializeTimetableEvent(event.toTimetableEvent(id: id));
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddTimetableEvent event) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = TimetableSerializer.serializeTimetableEvent(event.toTimetableEvent(id: id));
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<List<String>> deleteAll(String timetableId) {
    return _lock.synchronized(() async {
      final List<TimetableEvent> events = await getAll(timetableId: timetableId);
      if (events.isEmpty) return [];

      final List<String> ids = events.map((e) => e.id).toList();
      await _store.records(ids).delete(await _db);

      final List<String> deletedIds = await getDeletedIds();
      if (deletedIds.addIfAbsentAll(ids)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(deletedIds));
      }

      return ids;
    });
  }

  @override
  Future<List<String>> deleteAllByCourseId(String courseId) {
    return _lock.synchronized(() async {
      final List<TimetableEvent> events = await getAll(courseId: courseId);
      if (events.isEmpty) return [];

      final List<String> ids = events.map((e) => e.id).toList();
      await _store.records(ids).delete(await _db);

      final List<String> deletedIds = await getDeletedIds();
      if (deletedIds.addIfAbsentAll(ids)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(deletedIds));
      }

      return ids;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = await getDeletedIds();
      if (ids.addIfAbsent(id)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      return deleted;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}
