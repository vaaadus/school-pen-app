import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';

abstract class CustomTimetableEventRepository {
  /// Stream changes whenever timetable event repository changes.
  Stream<CustomTimetableEventRepository> get onChange;

  /// Returns all stored timetable events.
  /// Can be filtered by [timetableId]. Null means no filter.
  /// Can be filtered by [courseId]. Null means no filter.
  /// Can be filtered by [ids]. Null means no filter.
  Future<List<TimetableEvent>> getAll({String timetableId, String courseId, List<String> ids});

  /// Returns a list of ids of removed timetable events.
  Future<List<String>> getDeletedIds();

  /// Returns the timetable event by ID or null if it doesn't exist.
  Future<TimetableEvent> getById(String id);

  /// Creates a list of new timetables events and returns their IDs (in the same order as given events).
  Future<List<String>> createAll(List<AddTimetableEvent> events);

  /// Creates a new timetable event and returns its ID.
  Future<String> create(AddTimetableEvent event);

  /// Updates or creates a new timetable event with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddTimetableEvent event);

  /// Removes all timetable events by [timetableId].
  /// Returns the list of ids of deleted events.
  Future<List<String>> deleteAll(String timetableId);

  /// Removes all timetable events [courseId].
  /// Returns the list of ids of deleted events.
  Future<List<String>> deleteAllByCourseId(String courseId);

  /// Removes a timetable event with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
