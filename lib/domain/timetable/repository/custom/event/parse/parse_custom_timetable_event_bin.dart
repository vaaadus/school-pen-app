import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';

class ParseCustomTimetableEventBin extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'CustomTimetableEvent_Bin';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';

  ParseCustomTimetableEventBin() : super(keyClassName);

  ParseCustomTimetableEventBin.clone() : this();

  @override
  ParseCustomTimetableEventBin clone(Map map) => ParseCustomTimetableEventBin.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);
}
