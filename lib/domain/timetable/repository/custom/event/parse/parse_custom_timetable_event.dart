import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/repository/common/timetable_serializer.dart';

class ParseCustomTimetableEvent extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'CustomTimetableEvent';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keyTimetableId = 'timetableUuid';
  static const String keyCourseId = 'courseUuid';
  static const String keyCustomRoom = 'customRoom';
  static const String keyCustomTeacher = 'customTeacher';
  static const String keyCustomCourse = 'customCourse';
  static const String keyCustomStudent = 'customStudent';
  static const String keyNote = 'note';
  static const String keyRepeatOptions = 'repeatOptions';

  ParseCustomTimetableEvent() : super(keyClassName);

  ParseCustomTimetableEvent.clone() : this();

  factory ParseCustomTimetableEvent.fromModel(TimetableEvent event, ParseUser user) => ParseCustomTimetableEvent()
    ..user = user
    ..uuid = event.id
    ..updatedAt = event.updatedAt
    ..timetableId = event.timetableId
    ..courseId = event.courseId
    ..customRoom = event.customRoom
    ..customTeacher = event.customTeacher
    ..customCourse = event.customCourse
    ..customStudent = event.customStudent
    ..note = event.note
    ..repeatOptions = event.repeatOptions;

  AddTimetableEvent toAddTimetableEvent() => AddTimetableEvent(
        timetableId: timetableId,
        courseId: courseId,
        customRoom: customRoom,
        customTeacher: customTeacher,
        customCourse: customCourse,
        customStudent: customStudent,
        repeatOptions: repeatOptions,
        note: note,
        updatedAt: updatedAt,
      );

  @override
  ParseCustomTimetableEvent clone(Map map) => ParseCustomTimetableEvent.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get timetableId => getNullable(keyTimetableId);

  set timetableId(String timetableId) => setNullable(keyTimetableId, timetableId);

  String get courseId => getNullable(keyCourseId);

  set courseId(String courseId) => setNullable(keyCourseId, courseId);

  String get customRoom => getNullable(keyCustomRoom);

  set customRoom(String customRoom) => setNullable(keyCustomRoom, customRoom);

  String get customTeacher => getNullable(keyCustomTeacher);

  set customTeacher(String customTeacher) => setNullable(keyCustomTeacher, customTeacher);

  String get customCourse => getNullable(keyCustomCourse);

  set customCourse(String customCourse) => setNullable(keyCustomCourse, customCourse);

  String get customStudent => getNullable(keyCustomStudent);

  set customStudent(String customStudent) => setNullable(keyCustomStudent, customStudent);

  String get note => getNullable(keyNote);

  set note(String note) => setNullable(keyNote, note);

  EventRepeatOptions get repeatOptions =>
      TimetableSerializer.deserializeEventRepeatOptions(getNullable(keyRepeatOptions));

  set repeatOptions(EventRepeatOptions repeatOptions) =>
      setNullable(keyRepeatOptions, TimetableSerializer.serializeEventRepeatOptions(repeatOptions));

  /// Creates or updates a timetable event.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeCustomTimetableEvent').execute(parameters: {
      keyUuid: uuid,
      keyTimetableId: timetableId,
      keyCourseId: courseId,
      keyCustomRoom: customRoom,
      keyCustomTeacher: customTeacher,
      keyCustomCourse: customCourse,
      keyCustomStudent: customStudent,
      keyNote: note,
      keyRepeatOptions: TimetableSerializer.serializeEventRepeatOptions(repeatOptions),
    });
  }
}
