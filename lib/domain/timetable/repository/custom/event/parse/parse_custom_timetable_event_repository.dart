import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/custom_timetable_event_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/parse/parse_custom_timetable_event.dart';

class ParseCustomTimetableEventRepository implements CustomTimetableEventRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseCustomTimetableEventRepository');
  final CustomTimetableEventRepository _delegate;
  Subscription _subscription;

  ParseCustomTimetableEventRepository(this._delegate);

  @override
  Stream<CustomTimetableEventRepository> get onChange => _delegate.onChange;

  @override
  Future<List<TimetableEvent>> getAll({String timetableId, String courseId, List<String> ids}) {
    return _delegate.getAll(timetableId: timetableId, courseId: courseId, ids: ids);
  }

  @override
  Future<TimetableEvent> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<List<String>> createAll(List<AddTimetableEvent> events) async {
    final List<String> ids = await _delegate.createAll(events);
    _try(() async {
      final List<TimetableEvent> newEvents = await _delegate.getAll(ids: ids);
      await _storeOnParseAll(newEvents);
    });
    return ids;
  }

  @override
  Future<String> create(AddTimetableEvent event) async {
    final String id = await _delegate.create(event);
    _try(() async {
      final TimetableEvent newEvent = await _delegate.getById(id);
      await _storeOnParse(newEvent);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddTimetableEvent event) async {
    final bool updated = await _delegate.update(id, event);
    if (updated) {
      _try(() async {
        final TimetableEvent newEvent = await _delegate.getById(id);
        await _storeOnParse(newEvent);
      });
    }
    return updated;
  }

  @override
  Future<List<String>> deleteAll(String timetableId) async {
    final List<String> ids = await _delegate.deleteAll(timetableId);
    if (ids.isNotEmpty) {
      _try(() => _deleteOnParseAll(ids));
    }
    return ids;
  }

  @override
  Future<List<String>> deleteAllByCourseId(String courseId) async {
    final List<String> ids = await _delegate.deleteAllByCourseId(courseId);
    if (ids.isNotEmpty) {
      _try(() => _deleteOnParseAll(ids));
    }
    return ids;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(TimetableEvent event) async {
    await _delegate.update(event.id, event.toAddTimetableEvent());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseCustomTimetableEvent()));
    _subscription.on(LiveQueryEvent.create, (ParseCustomTimetableEvent object) {
      _try(() => _delegate.update(object.uuid, object.toAddTimetableEvent()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseCustomTimetableEvent object) {
      _try(() => _delegate.update(object.uuid, object.toAddTimetableEvent()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseCustomTimetableEvent object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParseAll(List<TimetableEvent> events) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    final List<Future<void>> futures = [];
    for (TimetableEvent event in events) {
      futures.add(ParseCustomTimetableEvent.fromModel(event, user).store());
    }
    return Future.wait(futures);
  }

  Future<void> _storeOnParse(TimetableEvent event) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseCustomTimetableEvent.fromModel(event, user).store();
  }

  Future<void> _deleteOnParseAll(List<String> ids) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    final List<Future<void>> futures = [];
    for (String id in ids) {
      futures.add(ParseCustomTimetableEvent().deleteByUuid(id));
    }
    return Future.wait(futures);
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseCustomTimetableEvent().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
