import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/repository/common/timetable_serializer.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/custom_timetable_repository.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// A [CustomTimetableRepository] that saves timetables in sembast document database.
class SembastCustomTimetableRepository implements CustomTimetableRepository {
  static const String keyDeletedIds = 'key_sembast_deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('customTimetables');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('customTimetables_bin');
  final BehaviorSubject<CustomTimetableRepository> _onChange = BehaviorSubject();
  final Lock _lock = Lock();

  SembastCustomTimetableRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<CustomTimetableRepository> get onChange => _onChange;

  @override
  Future<List<TimetableInfo>> getAll([String schoolYearId]) async {
    final Finder finder = schoolYearId == null ? null : Finder(filter: Filter.equals('schoolYearId', schoolYearId));
    final List<RecordSnapshot> snapshots = await _store.find(await _db, finder: finder);
    return snapshots.map((e) => TimetableSerializer.deserializeTimetableInfo(e.value)).toList();
  }

  @override
  Future<TimetableInfo> getInfoById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> record = await _store.record(id).get(await _db);
    return TimetableSerializer.deserializeTimetableInfo(record);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(keyDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<String> create(AddTimetable timetable) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = TimetableSerializer.serializeTimetableInfo(timetable.toTimetableInfo(id: id));
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddTimetable timetable) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = TimetableSerializer.serializeTimetableInfo(timetable.toTimetableInfo(id: id));
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = await getDeletedIds();
      if (ids.addIfAbsent(id)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      return deleted;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}
