import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';

abstract class CustomTimetableRepository {
  /// Stream changes whenever timetable repository changes.
  Stream<CustomTimetableRepository> get onChange;

  /// Returns all stored timetables.
  /// Can be filtered by [schoolYearId], null means no filter.
  Future<List<TimetableInfo>> getAll([String schoolYearId]);

  /// Returns a list of ids of removed timetables.
  Future<List<String>> getDeletedIds();

  /// Returns the timetable info by ID or null if it doesn't exist.
  Future<TimetableInfo> getInfoById(String id);

  /// Creates a new timetable and returns its ID.
  Future<String> create(AddTimetable timetable);

  /// Updates or creates a new timetable with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddTimetable timetable);

  /// Removes a timetable with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
