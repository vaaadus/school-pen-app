import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';

class ParseCustomTimetable extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'CustomTimetable';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keySchoolYearId = 'schoolYearUuid';
  static const String keyName = 'name';
  static const String keyTimetableType = 'timetableType';

  ParseCustomTimetable() : super(keyClassName);

  ParseCustomTimetable.clone() : this();

  factory ParseCustomTimetable.fromModel(TimetableInfo timetable, ParseUser user) => ParseCustomTimetable()
    ..user = user
    ..uuid = timetable.id
    ..updatedAt = timetable.updatedAt
    ..schoolYearId = timetable.schoolYearId
    ..name = timetable.name
    ..timetableType = timetable.type;

  AddTimetable toAddTimetable() => AddTimetable(
        schoolYearId: schoolYearId,
        name: name,
        type: timetableType,
        updatedAt: updatedAt,
      );

  @override
  ParseCustomTimetable clone(Map map) => ParseCustomTimetable.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get schoolYearId => getNullable(keySchoolYearId);

  set schoolYearId(String schoolYearId) => setNullable(keySchoolYearId, schoolYearId);

  String get name => getNullable(keyName);

  set name(String name) => setNullable(keyName, name);

  TimetableType get timetableType => TimetableTypeExt.forTag(getNullable(keyTimetableType));

  set timetableType(TimetableType type) => setNullable(keyTimetableType, type?.tag);

  /// Creates or updates a timetable.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeCustomTimetable').execute(parameters: {
      keyUuid: uuid,
      keySchoolYearId: schoolYearId,
      keyName: name,
      keyTimetableType: timetableType?.tag,
    });
  }
}
