import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';

class ParseCustomTimetableBin extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'CustomTimetable_Bin';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';

  ParseCustomTimetableBin() : super(keyClassName);

  ParseCustomTimetableBin.clone() : this();

  @override
  ParseCustomTimetableBin clone(Map map) => ParseCustomTimetableBin.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);
}
