import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';
import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/custom_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/parse/parse_custom_timetable.dart';

class ParseCustomTimetableRepository implements CustomTimetableRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseCustomTimetableRepository');
  final CustomTimetableRepository _delegate;
  Subscription _subscription;

  ParseCustomTimetableRepository(this._delegate);

  @override
  Stream<CustomTimetableRepository> get onChange => _delegate.onChange;

  @override
  Future<List<TimetableInfo>> getAll([String schoolYearId]) => _delegate.getAll(schoolYearId);

  @override
  Future<TimetableInfo> getInfoById(String id) => _delegate.getInfoById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddTimetable timetable) async {
    final String id = await _delegate.create(timetable);
    _try(() async {
      final TimetableInfo newTimetable = await _delegate.getInfoById(id);
      await _storeOnParse(newTimetable);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddTimetable timetable) async {
    final bool updated = await _delegate.update(id, timetable);
    if (updated) {
      _try(() async {
        final TimetableInfo newTimetable = await _delegate.getInfoById(id);
        await _storeOnParse(newTimetable);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(TimetableInfo timetable) async {
    await _delegate.update(timetable.id, timetable.toAddTimetable());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseCustomTimetable()));
    _subscription.on(LiveQueryEvent.create, (ParseCustomTimetable object) {
      _try(() => _delegate.update(object.uuid, object.toAddTimetable()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseCustomTimetable object) {
      _try(() => _delegate.update(object.uuid, object.toAddTimetable()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseCustomTimetable object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(TimetableInfo timetable) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseCustomTimetable.fromModel(timetable, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseCustomTimetable().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
