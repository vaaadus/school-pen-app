import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/course/course_cleanup_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_cleanup_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/sync/sync_listener.dart';
import 'package:school_pen/domain/sync/sync_manager.dart';
import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/school.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/custom_timetable_event_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/custom_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/public/public_timetable_repository.dart';
import 'package:school_pen/domain/timetable/timetable_listener.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:synchronized/synchronized.dart';

class TimetableManagerImpl
    implements TimetableManager, SyncListener, SchoolYearListener, SchoolYearCleanupListener, CourseCleanupListener {
  static const Logger _logger = Logger('TimetableManagerImpl');

  final _TimetableEvents _timetableEvents = _TimetableEvents();
  final Lock _lock = Lock();
  final Set<TimetableListener> _listeners = {};
  final PublicTimetableRepository _publicRepository;
  final CustomTimetableRepository _customRepository;
  final CustomTimetableEventRepository _eventRepository;
  final SyncManager _syncManager;
  final SchoolYearManager _schoolYearManager;
  final CourseManager _courseManager;
  final UserConfigRepository _userConfig;

  TimetableManagerImpl(
    this._publicRepository,
    this._customRepository,
    this._eventRepository,
    this._syncManager,
    this._schoolYearManager,
    this._courseManager,
    this._userConfig,
  ) {
    _customRepository.onChange.listen((event) => _dispatchTimetablesChanged());
    _eventRepository.onChange.listen((event) => _dispatchTimetablesChanged());

    _syncManager.addSyncListener(this);
    _schoolYearManager.addSchoolYearListener(this, notifyOnAttach: false);
    _schoolYearManager.addSchoolYearCleanupListener(this);
    _courseManager.addCourseCleanupListener(this);

    _userConfig.onChange.listen((List<Property> properties) {
      if (properties.contains(Property.timetableIdPerYear) ||
          properties.contains(Property.subscribedTimetableIdsPerYear)) {
        _dispatchTimetablesChanged();
      }
    });
  }

  @override
  void addTimetableListener(TimetableListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onTimetablesChanged(this);
  }

  @override
  void removeTimetableListener(TimetableListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<List<School>> getSchools() async {
    final List<School> schools = await _publicRepository.getSchools();
    schools.sort();
    return schools;
  }

  @override
  Future<List<TimetableInfo>> getTimetablesInfoOfSchool(String schoolId) async {
    final List<TimetableInfo> timetables = await _publicRepository.getTimetablesInfoOfSchool(schoolId);
    timetables.sort();
    return timetables;
  }

  @override
  Future<List<TimetableInfo>> getUserTimetablesInfo() {
    return _lock.synchronized(() async {
      final SchoolYear year = await _schoolYearManager.getActive();
      return _getUserTimetablesInfo(year.id);
    });
  }

  @override
  Future<List<Timetable>> getUserTimetables() {
    return _lock.synchronized(() async {
      final SchoolYear year = await _schoolYearManager.getActive();
      return _getUserTimetables(year.id);
    });
  }

  @override
  Future<Timetable> getTimetable(String timetableId) {
    return _getTimetable(timetableId);
  }

  @override
  Future<TimetableInfo> getTimetableInfo(String timetableId) {
    return _getTimetableInfo(timetableId);
  }

  @override
  Future<String> getActiveTimetableId() {
    return _lock.synchronized(() async {
      final SchoolYear year = await _schoolYearManager.getActive();
      return _userConfig.getTimetableIdPerYear(year.id);
    });
  }

  @override
  Future<TimetableInfo> getActiveTimetableInfo() {
    return _lock.synchronized(() async {
      final SchoolYear year = await _schoolYearManager.getActive();
      final String timetableId = await _userConfig.getTimetableIdPerYear(year.id);
      return _getTimetableInfo(timetableId);
    });
  }

  @override
  Future<Timetable> getActiveTimetable() {
    return _lock.synchronized(() async {
      final SchoolYear year = await _schoolYearManager.getActive();
      return _getActiveTimetable(year.id);
    });
  }

  @override
  Future<void> setActiveTimetable(String timetableId) {
    return _lock.synchronized(() async {
      final SchoolYear year = await _schoolYearManager.getActive();
      return _setActiveTimetable(year.id, timetableId);
    });
  }

  @override
  Future<void> unsubscribeAndResolveNewActiveTimetable() {
    return _lock.synchronized(() async {
      final SchoolYear year = await _schoolYearManager.getActive();
      final String timetableId = await _userConfig.getTimetableIdPerYear(year.id);
      await _userConfig.unsubscribeTimetablePerYear(year.id, timetableId);
      await _resolveNewActiveTimetable(year.id);
    });
  }

  @override
  Future<void> subscribeToPublicTimetable(String timetableId) {
    return _lock.synchronized(() async {
      final SchoolYear year = await _schoolYearManager.getActive();
      await _userConfig.subscribeTimetablePerYear(year.id, timetableId);
    });
  }

  @override
  Future<void> unsubscribeFromPublicTimetable(String timetableId) {
    return _lock.synchronized(() async {
      final SchoolYear year = await _schoolYearManager.getActive();
      await _userConfig.unsubscribeTimetablePerYear(year.id, timetableId);
      if (await _userConfig.getTimetableIdPerYear(year.id) == timetableId) {
        await _resolveNewActiveTimetable(year.id);
      }
    });
  }

  @override
  Future<String> createTimetable(AddTimetable timetable) {
    return _lock.synchronized(() => _customRepository.create(timetable));
  }

  @override
  Future<String> duplicateTimetable(Timetable timetable, DateTime dateTime, {String newName}) {
    return _lock.synchronized(() async {
      final AddTimetable newTimetable = timetable.toAddTimetable().copyWith(
            name: Optional(newName ?? timetable.name),
            updatedAt: Optional(dateTime),
          );

      final String id = await _customRepository.create(newTimetable);

      final List<AddTimetableEvent> events = timetable.events.map((e) {
        return e.toAddTimetableEvent().copyWith(
              timetableId: Optional(id),
              updatedAt: Optional(dateTime),
            );
      }).toList();

      await _eventRepository.createAll(events);

      return id;
    });
  }

  @override
  Future<void> renameTimetable(String id, String newName, DateTime dateTime) {
    return _lock.synchronized(() async {
      final TimetableInfo timetable = await _customRepository.getInfoById(id);
      if (timetable == null) return;

      final AddTimetable newTimetable = timetable.toAddTimetable().copyWith(
            name: Optional(newName),
            updatedAt: Optional(dateTime),
          );

      await _customRepository.update(id, newTimetable);
    });
  }

  @override
  Future<void> deleteAndResolveNewActiveTimetable(String timetableId) {
    return _lock.synchronized(() async {
      await _eventRepository.deleteAll(timetableId);
      await _customRepository.delete(timetableId);

      final SchoolYear year = await _schoolYearManager.getActive();
      await _resolveNewActiveTimetable(year.id);
    });
  }

  @override
  Future<TimetableEvent> getTimetableEventById(String eventId) {
    return _eventRepository.getById(eventId);
  }

  @override
  Future<List<SingleTimetableEvent>> getSingleEventsOf(Timetable timetable, DateTimeRange dateRange) {
    return _timetableEvents.getSingleEventsOf(timetable, dateRange);
  }

  @override
  Future<String> createTimetableEvent(AddTimetableEvent event) {
    return _lock.synchronized(() => _eventRepository.create(event));
  }

  @override
  Future<void> updateTimetableEvent(
      String eventId, AddTimetableEvent event, DateTimeRange recurrenceRange, EventRepeatOptionsEditMode editMode) {
    return _lock.synchronized(() async {
      final TimetableEvent previousEvent = await _eventRepository.getById(eventId);

      final EventRepeatOptionsUpdateModel<AddTimetableEvent> model = EventRepeatOptionsUpdate.getUpdateModel(
        previousEvent: previousEvent?.toAddTimetableEvent(),
        nextEvent: event,
        recurrenceRange: recurrenceRange,
        editMode: editMode,
      );

      if (model.deletePrevious) await _eventRepository.delete(eventId);
      if (model.updatePreviousWith != null) await _eventRepository.update(eventId, model.updatePreviousWith);
      for (AddTimetableEvent e in model.inserts) await _eventRepository.create(e);
    });
  }

  @override
  Future<void> deleteTimetableEvent(
      String eventId, DateTimeRange recurrenceRange, EventRepeatOptionsEditMode editMode) {
    return _lock.synchronized(() async {
      final TimetableEvent previousEvent = await _eventRepository.getById(eventId);

      final EventRepeatOptionsUpdateModel<AddTimetableEvent> model = EventRepeatOptionsUpdate.getDeleteModel(
        event: previousEvent?.toAddTimetableEvent(),
        recurrenceRange: recurrenceRange,
        editMode: editMode,
      );

      if (model.deletePrevious) await _eventRepository.delete(eventId);
      if (model.updatePreviousWith != null) await _eventRepository.update(eventId, model.updatePreviousWith);
      for (AddTimetableEvent e in model.inserts) await _eventRepository.create(e);
    });
  }

  @override
  Future<void> onSyncResumed() {
    return _lock.synchronized(() async {
      try {
        final SchoolYear year = await _schoolYearManager.getActiveOrNull();
        if (year == null) return;

        await _precomputeActiveTimetableEvents(year.id);
      } catch (error, stackTrace) {
        _logger.logError(error, stackTrace);
      }
    });
  }

  @override
  Future<void> onSyncPaused() {
    return _lock.synchronized(() async {
      await _timetableEvents.clear();
    });
  }

  @override
  void onSchoolYearsChanged(SchoolYearManager manager) {
    _dispatchTimetablesChanged();
  }

  @override
  Future<void> onCleanupSchoolYear(String schoolYearId) async {
    for (TimetableInfo timetable in await _getUserTimetablesInfo(schoolYearId)) {
      await _eventRepository.deleteAll(timetable.id);

      if (timetable.type == TimetableType.custom) {
        await _customRepository.delete(timetable.id);
      } else {
        await _userConfig.unsubscribeTimetablePerYear(schoolYearId, timetable.id);
      }
    }
  }

  @override
  Future<void> onCleanupCourse(String courseId) async {
    await _eventRepository.deleteAllByCourseId(courseId);
  }

  Future<void> _precomputeActiveTimetableEvents(String schoolYearId) async {
    final Timetable timetable = await _getActiveTimetable(schoolYearId);
    if (timetable == null) return;

    final DateTime today = DateTime.now().withoutTime();
    final DateTimeRange range = DateTimeRange(
      start: today.minusWeeks(2),
      end: today.plusWeeks(2),
    );

    await _timetableEvents.cacheSingleEventsOf(timetable, range);
  }

  Future<Timetable> _getActiveTimetable(String schoolYearId) async {
    final String timetableId = await _userConfig.getTimetableIdPerYear(schoolYearId);
    return _getTimetable(timetableId);
  }

  Future<void> _setActiveTimetable(String schoolYearId, String timetableId) async {
    await _userConfig.setTimetableIdPerYear(schoolYearId, timetableId);
  }

  Future<Timetable> _resolveNewActiveTimetable(String schoolYearId) async {
    final List<TimetableInfo> timetables = await _getUserTimetablesInfo(schoolYearId);
    if (timetables.isEmpty) {
      await _userConfig.setTimetableIdPerYear(schoolYearId, null);
      return null;
    } else {
      final String timetableId = timetables.first.id;
      await _userConfig.setTimetableIdPerYear(schoolYearId, timetableId);
      return _getTimetable(timetableId);
    }
  }

  Future<List<TimetableInfo>> _getUserTimetablesInfo(String schoolYearId) async {
    final List<String> subscribedIds = await _userConfig.getSubscribedTimetableIdsPerYear(schoolYearId);
    final Future<List<TimetableInfo>> publicTimetables = _publicRepository.getTimetablesInfoOfIds(subscribedIds);
    final Future<List<TimetableInfo>> customTimetables = _customRepository.getAll(schoolYearId);

    final List<TimetableInfo> timetables = [
      ...await publicTimetables,
      ...await customTimetables,
    ];

    timetables.sort();
    return timetables;
  }

  Future<List<Timetable>> _getUserTimetables(String schoolYearId) async {
    final List<String> subscribedIds = await _userConfig.getSubscribedTimetableIdsPerYear(schoolYearId);
    final Future<List<Timetable>> publicTimetables = _getPublicTimetables(subscribedIds);
    final Future<List<Timetable>> customTimetables = _getCustomTimetables(await _customRepository.getAll(schoolYearId));

    final List<Timetable> timetables = [
      ...await publicTimetables,
      ...await customTimetables,
    ];

    timetables.sort();
    return timetables;
  }

  Future<List<Timetable>> _getPublicTimetables(List<String> ids) async {
    final List<Future<Timetable>> futures = [];
    for (String id in ids) {
      futures.add(_publicRepository.getTimetable(id));
    }
    final List<Timetable> result = await Future.wait(futures);
    return result.withoutNulls().toList();
  }

  Future<List<Timetable>> _getCustomTimetables(List<TimetableInfo> timetables) async {
    final List<Future<Timetable>> futures = [];
    for (TimetableInfo timetable in timetables) {
      futures.add(_getCustomTimetableFromInfo(timetable));
    }
    final List<Timetable> result = await Future.wait(futures);
    return result.withoutNulls().toList();
  }

  Future<Timetable> _getTimetable(String timetableId) async {
    if (timetableId == null) return null;

    Timetable result = await _getCustomTimetable(timetableId);
    result ??= await _publicRepository.getTimetable(timetableId);
    return result;
  }

  Future<Timetable> _getCustomTimetableFromInfo(TimetableInfo timetable) async {
    if (timetable == null) return null;

    return timetable.toTimetable(events: await _eventRepository.getAll(timetableId: timetable.id));
  }

  Future<Timetable> _getCustomTimetable(String timetableId) async {
    if (timetableId == null) return null;

    final TimetableInfo timetable = await _customRepository.getInfoById(timetableId);
    return _getCustomTimetableFromInfo(timetable);
  }

  Future<TimetableInfo> _getTimetableInfo(String timetableId) async {
    if (timetableId == null) return null;

    TimetableInfo result = await _customRepository.getInfoById(timetableId);
    result ??= await _publicRepository.getTimetableInfo(timetableId);
    return result;
  }

  void _dispatchTimetablesChanged() {
    for (TimetableListener listener in _listeners) {
      listener.onTimetablesChanged(this);
    }
  }
}

class _TimetableEvents {
  final List<_TimetableEventsEntry> _cache = [];
  final Lock _lock = Lock();

  Future<List<SingleTimetableEvent>> getSingleEventsOf(Timetable timetable, DateTimeRange dateRange) {
    return _lock.synchronized(() async {
      await _cacheSingleEventsOf(timetable, dateRange);
      final _TimetableEventsEntry result = _findEntry(timetable.id, dateRange);
      return result.events.where((e) => dateRange.contains(e.dateTime, endInclusive: false)).toList();
    });
  }

  Future<void> cacheSingleEventsOf(Timetable timetable, DateTimeRange dateRange) {
    return _lock.synchronized(() async {
      await _cacheSingleEventsOf(timetable, dateRange, isolated: true);
    });
  }

  Future<void> _cacheSingleEventsOf(Timetable timetable, DateTimeRange dateRange, {bool isolated = false}) async {
    final _TimetableEventsEntry cachedEntry = _findEntry(timetable.id, dateRange);

    if (cachedEntry == null ||
        cachedEntry.updatedAt.isBefore(timetable.lastUpdatedAt) ||
        cachedEntry.sourceEventsCount != timetable.events.length) {
      _invalidateEntry(cachedEntry);
      _storeEntry(await _calculateEntry(timetable, dateRange, isolated));
    }
  }

  _TimetableEventsEntry _findEntry(String timetableId, DateTimeRange dateRange) {
    for (_TimetableEventsEntry entry in _cache) {
      if (entry.timetableId == timetableId && entry.dateRange.containsRange(dateRange)) return entry;
    }
    return null;
  }

  Future<_TimetableEventsEntry> _calculateEntry(Timetable timetable, DateTimeRange range, bool isolated) async {
    final _TimetableCalculationOutput result = isolated
        ? await compute(_calculateEvents, _TimetableCalculationInput(timetable, range))
        : await _calculateEvents(_TimetableCalculationInput(timetable, range));

    return _TimetableEventsEntry(
      timetableId: timetable.id,
      updatedAt: timetable.lastUpdatedAt,
      sourceEventsCount: timetable.events.length,
      events: result.events,
      dateRange: range,
    );
  }

  static Future<_TimetableCalculationOutput> _calculateEvents(_TimetableCalculationInput input) async {
    return _TimetableCalculationOutput(input.timetable.getSingleEventsIn(range: input.dateRange));
  }

  void _storeEntry(_TimetableEventsEntry entry) {
    _cache.add(entry);
  }

  void _invalidateEntry(_TimetableEventsEntry entry) {
    _cache.remove(entry);
  }

  Future<void> clear() {
    return _lock.synchronized(() async => _cache.clear());
  }
}

class _TimetableEventsEntry extends Equatable {
  final String timetableId;
  final DateTime updatedAt;
  final int sourceEventsCount;
  final List<SingleTimetableEvent> events;
  final DateTimeRange dateRange;

  const _TimetableEventsEntry({
    @required this.timetableId,
    @required this.updatedAt,
    @required this.sourceEventsCount,
    @required this.events,
    @required this.dateRange,
  });

  @override
  List<Object> get props => [timetableId, updatedAt, sourceEventsCount, events, dateRange];
}

class _TimetableCalculationInput extends Equatable {
  final Timetable timetable;
  final DateTimeRange dateRange;

  const _TimetableCalculationInput(this.timetable, this.dateRange);

  @override
  List<Object> get props => [timetable, dateRange];
}

class _TimetableCalculationOutput extends Equatable {
  final List<SingleTimetableEvent> events;

  const _TimetableCalculationOutput(this.events);

  @override
  List<Object> get props => [events];
}
