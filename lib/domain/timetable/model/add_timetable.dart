import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';

/// The complete timetable with a schedule but without the ID and events.
class AddTimetable extends Equatable {
  final String schoolYearId;
  final String name;
  final TimetableType type;
  final DateTime updatedAt;

  const AddTimetable({
    @required this.schoolYearId,
    @required this.name,
    @required this.type,
    @required this.updatedAt,
  });

  Timetable toTimetable({@required String id, @required List<TimetableEvent> events}) {
    return Timetable(
      id: id,
      schoolYearId: schoolYearId,
      name: name,
      type: type,
      events: events,
      updatedAt: updatedAt,
    );
  }

  TimetableInfo toTimetableInfo({@required String id}) {
    return TimetableInfo(
      id: id,
      schoolYearId: schoolYearId,
      name: name,
      type: type,
      updatedAt: updatedAt,
    );
  }

  AddTimetable copyWith({
    Optional<String> schoolYearId,
    Optional<String> name,
    Optional<TimetableType> type,
    Optional<DateTime> updatedAt,
  }) {
    return AddTimetable(
      schoolYearId: Optional.unwrapOrElse(schoolYearId, this.schoolYearId),
      name: Optional.unwrapOrElse(name, this.name),
      type: Optional.unwrapOrElse(type, this.type),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  @override
  List<Object> get props => [schoolYearId, name, type, updatedAt];
}
