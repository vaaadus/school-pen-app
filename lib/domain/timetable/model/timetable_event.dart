import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';

/// Specifies an event in the timetable schedule that might occur periodically, e.g. every week.
class TimetableEvent extends Equatable {
  final String id;
  final String timetableId;
  final String courseId;
  final String customRoom;
  final String customTeacher;
  final String customCourse;
  final String customStudent;
  final EventRepeatOptions repeatOptions;
  final String note;
  final DateTime updatedAt;

  const TimetableEvent({
    @required this.id,
    @required this.timetableId,
    this.courseId,
    this.customRoom,
    this.customTeacher,
    this.customCourse,
    this.customStudent,
    @required this.repeatOptions,
    this.note,
    @required this.updatedAt,
  });

  /// Returns a list of [SingleTimetableEvent] in the given [targetRange].
  List<SingleTimetableEvent> getSingleEventsIn(DateTimeRange targetRange) {
    final List<SingleTimetableEvent> result = [];
    for (DateTimeRange range in repeatOptions.getRepeatRanges(targetRange)) {
      for (DateTimeRange singleDayRange in EventRepeatOptions.splitIntoSingleDays(range)) {
        result.add(_toSingleAt(
          reoccursAt: range,
          dateTime: singleDayRange.start.withoutTime(),
          timeRange: singleDayRange.timeRange,
        ));
      }
    }
    return result;
  }

  SingleTimetableEvent _toSingleAt({
    @required DateTimeRange reoccursAt,
    @required DateTime dateTime,
    @required TimeRange timeRange,
  }) {
    return SingleTimetableEvent(
      id: id,
      timetableId: timetableId,
      dateTime: dateTime,
      timeRange: timeRange,
      reoccursAt: reoccursAt,
      repeatOptions: repeatOptions,
      courseId: courseId,
      customRoom: customRoom,
      customTeacher: customTeacher,
      customCourse: customCourse,
      customStudent: customStudent,
      note: note,
      updatedAt: updatedAt,
    );
  }

  AddTimetableEvent toAddTimetableEvent() {
    return AddTimetableEvent(
      timetableId: timetableId,
      courseId: courseId,
      customRoom: customRoom,
      customTeacher: customTeacher,
      customStudent: customStudent,
      repeatOptions: repeatOptions,
      note: note,
      updatedAt: updatedAt,
    );
  }

  @override
  List<Object> get props => [
        id,
        timetableId,
        courseId,
        customRoom,
        customTeacher,
        customCourse,
        customStudent,
        repeatOptions,
        note,
        updatedAt,
      ];
}

/// A [TimetableEvent] without repeats options that happens only on a single day.
class SingleTimetableEvent extends Equatable {
  /// The unique id for the [TimetableEvent].
  /// As a result of splitting the timetable event,
  /// a couple of single timetable events might be created with the same id.
  final String id;

  /// The id of the timetable to which this event belongs.
  final String timetableId;

  /// The date when this event reoccurs. It is a part of the [reoccursAt] range.
  /// This date does not use time zone, it is in UTC.
  final DateTime dateTime;

  /// The time range when this event reoccurs.
  final TimeRange timeRange;

  /// The recurrence date range.
  /// For events that span only a single day it will be equal to [dateTime] with [timeRange].
  /// This date time range does not use time zone, it is in UTC.
  final DateTimeRange reoccursAt;

  /// The original repeat options that specify when this event reoccurs.
  final EventRepeatOptions repeatOptions;

  final String courseId;
  final String customRoom;
  final String customTeacher;
  final String customCourse;
  final String customStudent;
  final String note;
  final DateTime updatedAt;

  const SingleTimetableEvent({
    @required this.id,
    @required this.timetableId,
    @required this.dateTime,
    @required this.timeRange,
    @required this.reoccursAt,
    @required this.repeatOptions,
    this.courseId,
    this.customRoom,
    this.customTeacher,
    this.customCourse,
    this.customStudent,
    this.note,
    @required this.updatedAt,
  });

  /// Builds a comparator function which puts earlier events at the start and later events at the end.
  static int Function(SingleTimetableEvent a, SingleTimetableEvent b) get compareByDate {
    return (SingleTimetableEvent a, SingleTimetableEvent b) {
      if (a.dateTime != b.dateTime) {
        return a.dateTime.compareTo(b.dateTime);
      }
      return a.timeRange.compareTo(b.timeRange);
    };
  }

  @override
  List<Object> get props => [
        id,
        timetableId,
        dateTime,
        timeRange,
        reoccursAt,
        repeatOptions,
        courseId,
        customRoom,
        customTeacher,
        customCourse,
        customStudent,
        note,
        updatedAt,
      ];
}
