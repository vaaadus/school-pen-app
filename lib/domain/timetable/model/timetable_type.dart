/// Possible types of the timetable.
enum TimetableType {
  student,
  teacher,
  room,
  custom,
  unknown,
}

extension TimetableTypeExt on TimetableType {
  String get tag {
    switch (this) {
      case TimetableType.student:
        return 'student';
      case TimetableType.teacher:
        return 'teacher';
      case TimetableType.room:
        return 'room';
      case TimetableType.custom:
        return 'custom';
      case TimetableType.unknown:
        return 'unknown';
    }
    throw ArgumentError('Unsupported timetable type: $this');
  }

  static TimetableType forTag(String tag) {
    for (final type in TimetableType.values) {
      if (type.tag == tag) return type;
    }
    return TimetableType.unknown;
  }
}
