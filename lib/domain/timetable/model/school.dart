import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'school_address.dart';

/// Represents a school, that can have timetables.
class School extends Equatable implements Comparable<School> {
  final String id;
  final String name;
  final SchoolAddress address;
  final DateTime updatedAt;

  const School({
    @required this.id,
    @required this.name,
    @required this.address,
    @required this.updatedAt,
  });

  @override
  int compareTo(School other) {
    if (address != other.address) {
      return address.compareTo(other.address);
    }

    return name.compareTo(other.name);
  }

  @override
  List<Object> get props => [id, name, address, updatedAt];
}
