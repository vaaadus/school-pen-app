import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';

/// Specifies an event in the timetable schedule that might occur periodically, e.g. every week.
/// Similar to [TimetableEvent] but without an ID.
class AddTimetableEvent extends Equatable implements EventRepeatOptionsAware<AddTimetableEvent> {
  final String timetableId;
  final String courseId;
  final String customRoom;
  final String customTeacher;
  final String customCourse;
  final String customStudent;
  @override
  final EventRepeatOptions repeatOptions;
  final String note;
  final DateTime updatedAt;

  const AddTimetableEvent({
    @required this.timetableId,
    this.courseId,
    this.customRoom,
    this.customTeacher,
    this.customCourse,
    this.customStudent,
    @required this.repeatOptions,
    this.note,
    @required this.updatedAt,
  });

  TimetableEvent toTimetableEvent({@required String id}) {
    return TimetableEvent(
      id: id,
      timetableId: timetableId,
      courseId: courseId,
      customRoom: customRoom,
      customTeacher: customTeacher,
      customCourse: customCourse,
      customStudent: customStudent,
      repeatOptions: repeatOptions,
      note: note,
      updatedAt: updatedAt,
    );
  }

  @override
  AddTimetableEvent copyWith({
    Optional<String> timetableId,
    Optional<String> courseId,
    Optional<String> customRoom,
    Optional<String> customTeacher,
    Optional<String> customCourse,
    Optional<String> customStudent,
    Optional<EventRepeatOptions> repeatOptions,
    Optional<String> note,
    Optional<DateTime> updatedAt,
  }) {
    return AddTimetableEvent(
      timetableId: Optional.unwrapOrElse(timetableId, this.timetableId),
      courseId: Optional.unwrapOrElse(courseId, this.courseId),
      customRoom: Optional.unwrapOrElse(customRoom, this.customRoom),
      customTeacher: Optional.unwrapOrElse(customTeacher, this.customTeacher),
      customCourse: Optional.unwrapOrElse(customCourse, this.customCourse),
      customStudent: Optional.unwrapOrElse(customStudent, this.customStudent),
      repeatOptions: Optional.unwrapOrElse(repeatOptions, this.repeatOptions),
      note: Optional.unwrapOrElse(note, this.note),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  @override
  List<Object> get props => [
        timetableId,
        courseId,
        customRoom,
        customTeacher,
        customCourse,
        customStudent,
        repeatOptions,
        note,
        updatedAt,
      ];
}
