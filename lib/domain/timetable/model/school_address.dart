import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

/// Specifies the not-detailed address of a school.
class SchoolAddress extends Equatable implements Comparable<SchoolAddress> {
  final String country;
  final String city;

  const SchoolAddress({
    @required this.country,
    @required this.city,
  });

  @override
  int compareTo(SchoolAddress other) {
    if (country != other.country) {
      return country.compareTo(other.country);
    }
    return city.compareTo(other.city);
  }

  @override
  List<Object> get props => [country, city];
}
