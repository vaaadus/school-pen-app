import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';

/// The complete timetable with a schedule.
class Timetable extends Equatable implements Comparable<Timetable> {
  final String id;
  final String schoolYearId;
  final String name;
  final TimetableType type;
  final List<TimetableEvent> events;
  final DateTime updatedAt;

  const Timetable({
    @required this.id,
    this.schoolYearId,
    @required this.name,
    @required this.type,
    @required this.events,
    @required this.updatedAt,
  });

  /// Returns a list of [SingleTimetableEvent] that happen in the [range].
  static List<SingleTimetableEvent> getSingleEventsOf({
    @required List<TimetableEvent> events,
    @required DateTimeRange range,
  }) {
    final List<SingleTimetableEvent> result = [];
    for (TimetableEvent event in events) {
      result.addAll(event.getSingleEventsIn(range));
    }
    return result;
  }

  /// Returns the start time of the earliest event and the end time of the latest event.
  ///
  /// Use [minimumRange] in case there are no events or the time range
  /// is too short and you'd like to extend it to cover the minimum.
  static TimeRange getTimeRangeOfEventsOf({
    @required List<SingleTimetableEvent> events,
    TimeRange minimumRange,
  }) {
    Time startTime;
    Time endTime;
    for (SingleTimetableEvent event in events) {
      if (startTime == null || event.timeRange.start.isBefore(startTime)) {
        startTime = event.timeRange.start;
      }
      if (endTime == null || event.timeRange.end.isAfter(endTime)) {
        endTime = event.timeRange.end;
      }
    }

    if (minimumRange != null && (startTime == null || minimumRange.start.isBefore(startTime))) {
      startTime = minimumRange.start;
    }

    if (minimumRange != null && (endTime == null || minimumRange.end.isAfter(endTime))) {
      endTime = minimumRange.end;
    }

    return TimeRange(start: startTime, end: endTime);
  }

  /// See [getSingleEventsOf].
  List<SingleTimetableEvent> getSingleEventsIn({@required DateTimeRange range}) {
    return getSingleEventsOf(events: events, range: range);
  }

  /// See [getTimeRangeOfEventsOf].
  TimeRange getTimeRangeOfEventsIn({@required DateTimeRange range, TimeRange minimumRange}) {
    return getTimeRangeOfEventsOf(
      events: getSingleEventsOf(events: events, range: range),
      minimumRange: minimumRange,
    );
  }

  AddTimetable toAddTimetable() {
    return AddTimetable(
      schoolYearId: schoolYearId,
      name: name,
      type: type,
      updatedAt: updatedAt,
    );
  }

  TimetableInfo toTimetableInfo() {
    return TimetableInfo(
      id: id,
      schoolYearId: schoolYearId,
      name: name,
      type: type,
      updatedAt: updatedAt,
    );
  }

  /// Returns the last updated at timestamp from either this timetable or any [TimetableEvent].
  DateTime get lastUpdatedAt {
    DateTime last = updatedAt;
    for (TimetableEvent event in events) {
      if (event.updatedAt.isAfter(last)) last = event.updatedAt;
    }
    return last;
  }

  @override
  int compareTo(Timetable other) {
    if (type != other.type) {
      return type.index - other.type.index;
    }
    return name.compareTo(other.name);
  }

  @override
  List<Object> get props => [id, schoolYearId, name, type, events, updatedAt];
}

/// The metadata collected about timetable. Does not contain the schedule.
class TimetableInfo extends Equatable implements Comparable<TimetableInfo> {
  final String id;
  final String schoolYearId;
  final String name;
  final TimetableType type;
  final DateTime updatedAt;

  const TimetableInfo({
    @required this.id,
    this.schoolYearId,
    @required this.name,
    @required this.type,
    @required this.updatedAt,
  });

  AddTimetable toAddTimetable() {
    return AddTimetable(
      schoolYearId: schoolYearId,
      name: name,
      type: type,
      updatedAt: updatedAt,
    );
  }

  Timetable toTimetable({@required List<TimetableEvent> events}) {
    return Timetable(
      id: id,
      schoolYearId: schoolYearId,
      name: name,
      type: type,
      events: events,
      updatedAt: updatedAt,
    );
  }

  @override
  int compareTo(TimetableInfo other) {
    if (type != other.type) {
      return type.index - other.type.index;
    }
    return name.compareTo(other.name);
  }

  @override
  List<Object> get props => [id, schoolYearId, name, type, updatedAt];
}
