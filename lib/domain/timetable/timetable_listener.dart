import 'package:school_pen/domain/timetable/timetable_manager.dart';

abstract class TimetableListener {
  /// Called whenever any property of [TimetableManager] changes.
  void onTimetablesChanged(TimetableManager manager);
}
