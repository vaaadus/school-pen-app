abstract class PersonalizedAdsConsentListener {
  /// Called whenever the user consent for personalized ads changes.
  void onPersonalizedAdsConsentChanged(bool enabled);
}
