import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/ads/personalized_ads_consent_listener.dart';
import 'package:school_pen/domain/ads/repository/location_repository.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/preferences/preferences_listener.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';

import 'ad_manager.dart';

const String keyBannerAdWindow = 'key_bannerAdWindow';
const Logger _logger = Logger('BaseAdManager');

/// A base implementation of [AdManager].
/// Each class intending to implement [AdManager] should extend from this class.
abstract class BaseAdManager implements AdManager, PreferencesListener {
  static const String _keyUserLocatedInEEA = 'key_ads_userLocatedInEEA';

  final Set<PersonalizedAdsConsentListener> _listeners = {};
  final UserConfigRepository _userConfig;
  final LocationRepository _location;
  final Preferences _preferences;
  bool _personalizedAdsEnabled;

  BaseAdManager(this._userConfig, this._location, this._preferences) {
    _preferences.addPreferencesListener(this, notifyOnAttach: false);
    _userConfig.onChange.listen(_onUserConfigChanged);
  }

  @mustCallSuper
  @override
  Future<void> initialize() async {
    await _tryResolveAdsPersonalization();
  }

  @override
  bool get arePersonalizedAdsEnabled => _personalizedAdsEnabled ?? false;

  @override
  Future<void> setPersonalizedAdsEnabled(bool enabled) async {
    await _userConfig.setPersonalizedAdsEnabled(enabled);
  }

  @override
  void addPersonalizedAdsConsentListener(PersonalizedAdsConsentListener listener) {
    _listeners.add(listener);
  }

  @override
  void removePersonalizedAdsConsentListener(PersonalizedAdsConsentListener listener) {
    _listeners.remove(listener);
  }

  @override
  void onPreferencesChanged(Preferences preferences, List<String> changedKeys) {
    if (changedKeys.contains(_keyUserLocatedInEEA)) {
      _tryResolveAdsPersonalization();
    }
  }

  void _onUserConfigChanged(List<Property> properties) {
    if (properties.contains(Property.personalizedAdsEnabled)) {
      _tryResolveAdsPersonalization();
    }
  }

  Future<void> _tryResolveAdsPersonalization() async {
    try {
      await _resolveAdsPersonalization();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<void> _resolveAdsPersonalization() async {
    final bool initialValue = _personalizedAdsEnabled;
    _personalizedAdsEnabled = await _userConfig.isPersonalizedAdsEnabled();
    _personalizedAdsEnabled ??= !(await _isUserLocatedInEEA());

    if (initialValue != _personalizedAdsEnabled) {
      _dispatchConsentChanged();
    }
  }

  Future<bool> _isUserLocatedInEEA() async {
    final bool value = _preferences.getBool(_keyUserLocatedInEEA) ?? await _fetchIsUserLocatedInEEA();
    _preferences.setBool(_keyUserLocatedInEEA, value);
    return value ?? false;
  }

  Future<bool> _fetchIsUserLocatedInEEA() async {
    try {
      return await _location.isLocatedInEEA();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      return null;
    }
  }

  void _dispatchConsentChanged() {
    for (var listener in _listeners) {
      listener.onPersonalizedAdsConsentChanged(_personalizedAdsEnabled);
    }
  }
}
