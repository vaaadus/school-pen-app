import 'package:school_pen/domain/ads/exception/ad_failed_to_load_exception.dart';
import 'package:school_pen/domain/ads/model/ad_placement.dart';
import 'package:school_pen/domain/ads/personalized_ads_consent_listener.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';

abstract class AdManager {
  /// Must be called before the app start. Initializes the ads.
  Future<void> initialize();

  /// Whether personalized ads can be shown (i.e. in EEA they cannot without user consent).
  bool get arePersonalizedAdsEnabled;

  /// Enable / disable personalization of ads.
  Future<void> setPersonalizedAdsEnabled(bool enabled);

  /// Register for consent changes.
  void addPersonalizedAdsConsentListener(PersonalizedAdsConsentListener listener);

  /// Unregister from consent changes.
  void removePersonalizedAdsConsentListener(PersonalizedAdsConsentListener listener);

  /// Requests to show the rewarded ad.
  /// The resulting future will indicate whether a reward was rewarded.
  ///
  /// Throws [NoInternetException] if the internet connection is required.
  /// Throws [AdFailedToLoadException] if an ad couldn't be loaded.
  Future<bool> showRewardedAd(RewardedAdPlacement placement);

  /// Preloads rewarded ad which reduces the delay for the user when he'd like to play the ad.
  ///
  /// Throws [NoInternetException] if the internet connection is required.
  /// Throws [AdFailedToLoadException] if an ad couldn't be loaded.
  Future<void> preloadRewardedAd(RewardedAdPlacement placement);
}
