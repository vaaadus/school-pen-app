// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_request_location.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonRequestLocation _$JsonRequestLocationFromJson(Map<String, dynamic> json) {
  return JsonRequestLocation(
    is_request_in_eea_or_unknown: json['is_request_in_eea_or_unknown'] as bool,
  );
}

Map<String, dynamic> _$JsonRequestLocationToJson(
        JsonRequestLocation instance) =>
    <String, dynamic>{
      'is_request_in_eea_or_unknown': instance.is_request_in_eea_or_unknown,
    };
