import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'json_request_location.g.dart';

@JsonSerializable()
class JsonRequestLocation {
  final bool is_request_in_eea_or_unknown;

  JsonRequestLocation({
    @required this.is_request_in_eea_or_unknown,
  });

  factory JsonRequestLocation.fromJson(Map<String, dynamic> json) => _$JsonRequestLocationFromJson(json);
}
