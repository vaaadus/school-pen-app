import 'dart:convert';

import 'package:school_pen/domain/common/http.dart';
import 'package:school_pen/domain/common/result.dart';

import 'json/json_request_location.dart';
import 'location_repository.dart';

class AdServiceLocationRepository implements LocationRepository {
  static const String _baseUrl = 'https://adservice.google.com/getconfig/pubvendors';

  @override
  Future<bool> isLocatedInEEA() async {
    final JsonRequestLocation location = Result.unwrapResult(await _getUserLocation());
    return location.is_request_in_eea_or_unknown;
  }

  static Future<Result<JsonRequestLocation>> _getUserLocation() async {
    return httpGetAndParse('$_baseUrl', _parseLocation);
  }

  static JsonRequestLocation _parseLocation(String string) {
    return JsonRequestLocation.fromJson(json.decode(string));
  }
}
