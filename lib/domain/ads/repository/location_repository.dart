abstract class LocationRepository {
  /// Whether the user is in European Economic Area.
  Future<bool> isLocatedInEEA();
}
