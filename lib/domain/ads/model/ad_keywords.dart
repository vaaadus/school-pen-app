/// A list of keywords which might be interesting to our users.
const List<String> advertisingKeywords = [
  'school',
  'college',
  'university',
  'education',
  'high school',
  'learn',
  'study',
  'teacher',
  'student',
  'course',
];
