import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/ads/exception/ad_failed_to_load_exception.dart';
import 'package:school_pen/domain/ads/model/ad_keywords.dart';
import 'package:school_pen/domain/ads/repository/location_repository.dart';
import 'package:school_pen/domain/common/env_options.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/network/network_manager.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:synchronized/synchronized.dart';

import 'base_ad_manager.dart';
import 'model/ad_placement.dart';

const List<String> testDevices = ['E93E4234618B6FB0B03C108B01061545'];
const Logger _logger = Logger('AdmobManager');

class AdmobManager extends BaseAdManager {
  final Lock _lock = Lock();
  final NetworkManager _networkManager;
  final AdmobConfig _config;

  bool _isRewardedAdPreloaded = false;

  AdmobManager(
    UserConfigRepository userConfig,
    LocationRepository location,
    Preferences preferences,
    this._networkManager,
    this._config,
  ) : super(userConfig, location, preferences);

  @mustCallSuper
  @override
  Future<void> initialize() async {
    await super.initialize();

    await FirebaseAdMob.instance.initialize(
      appId: _config.appId,
      analyticsEnabled: EnvOptions.useProductionEnv,
    );
  }

  @override
  Future<bool> showRewardedAd(RewardedAdPlacement placement) {
    return _lock.synchronized(() async {
      _logger.log('showRewardedAd: $placement');

      final Completer<void> loadCompleter = _awaitRewardedVideoAdLoaded();
      await RewardedVideoAd.instance.load(
        adUnitId: _config.getRewardedAdId(placement),
        targetingInfo: getTargetingInfo(),
      );
      await loadCompleter.future;

      final Completer<bool> rewardCompleter = _awaitRewardedVideoAdReward();
      await RewardedVideoAd.instance.show();
      _isRewardedAdPreloaded = false;
      return rewardCompleter.future;
    });
  }

  @override
  Future<void> preloadRewardedAd(RewardedAdPlacement placement) {
    return _lock.synchronized(() async {
      _logger.log('preloadRewardedAd: $placement');

      final Completer<void> loadCompleter = _awaitRewardedVideoAdLoaded();
      await RewardedVideoAd.instance.load(
        adUnitId: _config.getRewardedAdId(placement),
        targetingInfo: getTargetingInfo(),
      );
      await loadCompleter.future;
    });
  }

  Completer<void> _awaitRewardedVideoAdLoaded() {
    final Completer<void> completer = Completer();

    if (_isRewardedAdPreloaded) {
      completer.complete(null);
      return completer;
    }

    RewardedVideoAd.instance.listener = (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) async {
      _logger.log('awaitRewardedVideoAdLoaded: $event');

      if (event == RewardedVideoAdEvent.failedToLoad) {
        RewardedVideoAd.instance.listener = null;

        if (await _networkManager.isNetworkAvailable()) {
          completer.complete(AdFailedToLoadException());
        } else {
          completer.completeError(NoInternetException());
        }
      } else if (event == RewardedVideoAdEvent.loaded) {
        RewardedVideoAd.instance.listener = null;
        _isRewardedAdPreloaded = true;
        completer.complete(null);
      }
    };
    return completer;
  }

  Completer<bool> _awaitRewardedVideoAdReward() {
    final Completer<bool> completer = Completer();
    bool _result;

    RewardedVideoAd.instance.listener = (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) {
      _logger.log('awaitRewardedVideoAdReward: $event');

      if (event == RewardedVideoAdEvent.rewarded) {
        _result = true;
      } else if (event == RewardedVideoAdEvent.closed) {
        RewardedVideoAd.instance.listener = null;
        _result ??= false;
        completer.complete(_result);
      }
    };

    return completer;
  }

  MobileAdTargetingInfo getTargetingInfo() {
    return MobileAdTargetingInfo(
      keywords: advertisingKeywords,
      nonPersonalizedAds: !arePersonalizedAdsEnabled,
      testDevices: testDevices,
    );
  }
}

/// Defines the application config for ad purposes.
abstract class AdmobConfig extends Equatable {
  String get appId;

  String getRewardedAdId(RewardedAdPlacement placement);

  @override
  List<Object> get props => [appId];
}

class AndroidReleaseAdmobConfig extends AdmobConfig {
  @override
  String get appId => 'ca-app-pub-8363094052355678~2979801887';

  @override
  String getRewardedAdId(RewardedAdPlacement placement) {
    switch (placement) {
      case RewardedAdPlacement.Themes:
        return 'ca-app-pub-8363094052355678/6149661177';
    }
    throw ArgumentError('Placement no supported $placement');
  }
}

class AndroidDebugAdmobConfig extends AdmobConfig {
  @override
  String get appId => FirebaseAdMob.testAppId;

  @override
  String getRewardedAdId(RewardedAdPlacement placement) => RewardedVideoAd.testAdUnitId;
}

class IOSReleaseAdmobConfig extends AdmobConfig {
  @override
  String get appId => 'ca-app-pub-8363094052355678~6986378583';

  @override
  String getRewardedAdId(RewardedAdPlacement placement) {
    switch (placement) {
      case RewardedAdPlacement.Themes:
        return 'ca-app-pub-8363094052355678/6153530607';
    }
    throw ArgumentError('Placement no supported $placement');
  }
}

class IOSDebugAdmobConfig extends AdmobConfig {
  @override
  String get appId => FirebaseAdMob.testAppId;

  @override
  String getRewardedAdId(RewardedAdPlacement placement) => RewardedVideoAd.testAdUnitId;
}
