import 'package:school_pen/domain/ads/base_ad_manager.dart';
import 'package:school_pen/domain/ads/model/ad_placement.dart';
import 'package:school_pen/domain/ads/repository/location_repository.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';

// TODO implement
class EmptyAdManager extends BaseAdManager {
  EmptyAdManager(
    UserConfigRepository userConfig,
    LocationRepository location,
    Preferences preferences,
  ) : super(userConfig, location, preferences);

  @override
  Future<void> preloadRewardedAd(RewardedAdPlacement placement) async {
    // TODO: implement preloadRewardedAd
  }

  @override
  Future<bool> showRewardedAd(RewardedAdPlacement placement) async {
    // TODO: implement showRewardedAd
    return true;
  }
}
