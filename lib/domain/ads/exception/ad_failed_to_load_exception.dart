import 'package:equatable/equatable.dart';

/// Indicates that the an has has failed to load.
class AdFailedToLoadException extends Equatable implements Exception {
  @override
  List<Object> get props => [];
}
