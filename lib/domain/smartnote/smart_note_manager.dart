import 'package:school_pen/domain/smartnote/smart_note_listener.dart';

import 'smart_note.dart';

abstract class SmartNoteManager {
  void addSmartNoteListener(SmartNoteListener listener, {bool notifyOnAttach = true});

  void removeSmartNoteListener(SmartNoteListener listener);

  /// Returns the primary smart note or null if none.
  Future<SmartNote> get smartNote;

  /// Notify the manager that user has postponed the pending billing.
  void onPostponePendingBillingNote();

  /// Notify the manager that user has postponed the login offer note.
  void onPostponeLoginOfferNote();
}
