import 'package:school_pen/domain/smartnote/smart_note_manager.dart';

abstract class SmartNoteListener {
  /// Called whenever smart notes change.
  void onSmartNotesChanged(SmartNoteManager manager);
}
