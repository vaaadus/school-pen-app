import 'package:school_pen/domain/agenda/agenda_manager.dart';
import 'package:school_pen/domain/agenda/model/agenda_event.dart';
import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/inappsubscription/inapp_subscription_manager.dart';
import 'package:school_pen/domain/smartnote/smart_note_listener.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/model/user_meta.dart';
import 'package:school_pen/domain/user/user_login_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';

import 'smart_note.dart';
import 'smart_note_manager.dart';

class SmartNoteManagerImpl extends SmartNoteManager implements UserLoginListener {
  static const String keyPostponedBillingWindow = 'key_smartNote_postponedBilling_window';
  static const String keyPostponedLoginWindow = 'key_smartNote_postponedLogin_window';

  static const Duration delayBetweenPostponedBilling = Duration(days: 1);
  static const Duration delayBetweenPostponedLogin = Duration(days: 30);
  static const Duration initialDelayLogin = Duration(days: 0);

  final Set<SmartNoteListener> _listeners = {};
  final InAppSubscriptionManager _subscriptionManager;
  final UserManager _userManager;
  final SlidingWindow _slidingWindow;
  final AgendaManager _agendaManager;

  SmartNoteManagerImpl(
    this._subscriptionManager,
    this._userManager,
    this._slidingWindow,
    this._agendaManager,
  ) {
    _userManager.addUserLoginListener(this);
  }

  @override
  void addSmartNoteListener(SmartNoteListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onSmartNotesChanged(this);
  }

  @override
  void removeSmartNoteListener(SmartNoteListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<SmartNote> get smartNote async {
    if (await _subscriptionManager.hasPendingPurchases && _hasDelayBetweenPostponedBillingPassed) {
      return SmartNote.pendingBilling;
    }

    final User user = await _userManager.getCurrentUser();
    if (user == null && _hasDelayBetweenPostponedLoginPassed) {
      return SmartNote.loginOffer;
    }

    final List<AgendaEvent> overdueEvents = await _agendaManager.getOverdueEvents();
    if (overdueEvents.isNotEmpty) {
      return SmartNote.missedWork;
    }

    return null;
  }

  @override
  void onPostponePendingBillingNote() {
    _slidingWindow.restart(key: keyPostponedBillingWindow);
    _dispatchSmartNotesChanged();
  }

  @override
  void onPostponeLoginOfferNote() {
    _slidingWindow.restart(key: keyPostponedLoginWindow);
    _dispatchSmartNotesChanged();
  }

  @override
  void onUserSignedIn(UserMetadata user) {
    _dispatchSmartNotesChanged();
  }

  @override
  void onUserSignedOut(UserMetadata user) {
    _dispatchSmartNotesChanged();
  }

  bool get _hasDelayBetweenPostponedBillingPassed {
    return _slidingWindow.isPassed(
      key: keyPostponedBillingWindow,
      nextWindow: delayBetweenPostponedBilling,
    );
  }

  bool get _hasDelayBetweenPostponedLoginPassed {
    return _slidingWindow.isPassed(
      key: keyPostponedLoginWindow,
      initialWindow: initialDelayLogin,
      nextWindow: delayBetweenPostponedLogin,
    );
  }

  void _dispatchSmartNotesChanged() {
    for (SmartNoteListener listener in _listeners) {
      listener.onSmartNotesChanged(this);
    }
  }
}
