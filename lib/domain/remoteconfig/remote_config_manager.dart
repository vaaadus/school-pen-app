import 'remote_config_flag.dart';
import 'remote_config_listener.dart';

/// Allows to check for feature flags, etc...
abstract class RemoteConfigManager {
  String getString(RemoteConfigFlag flag);

  bool getBool(RemoteConfigFlag flag);

  int getInt(RemoteConfigFlag flag);

  void addRemoteConfigListener(RemoteConfigListener listener);

  void removeRemoteConfigListener(RemoteConfigListener listener);
}
