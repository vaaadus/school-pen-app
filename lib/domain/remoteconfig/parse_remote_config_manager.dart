import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/remoteconfig/remote_config_flag.dart';
import 'package:school_pen/domain/remoteconfig/remote_config_listener.dart';
import 'package:school_pen/domain/remoteconfig/remote_config_manager.dart';
import 'package:sembast/sembast.dart';

class ParseRemoteConfigManager implements RemoteConfigManager, LifecycleListener {
  static const Logger _logger = Logger('ParseRemoteConfigManager');
  static const String _keyLastFetchWindow = 'key_remoteConfig_lastFetch';
  static const Duration _fetchConfigInterval = Duration(hours: 12);
  static final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('remote_config');
  static final String _storeRecord = 'config';

  static final Map<String, dynamic> _defaultConfig = {
    RemoteConfigFlag.liveQueryEnabled.tag: true,
  };

  static final Map<String, dynamic> _debugConfig = {
    RemoteConfigFlag.liveQueryEnabled.tag: true,
  };

  final Map<String, dynamic> _currentConfig = {};
  final Set<RemoteConfigListener> _listeners = {};
  final SlidingWindow _slidingWindow;
  final LifecycleManager _lifecycle;
  final bool _useDebugConfig;

  ParseRemoteConfigManager(
    this._slidingWindow,
    this._lifecycle,
    this._useDebugConfig,
  ) {
    _lifecycle.addLifecycleListener(this);

    _loadCurrentConfig();
  }

  @override
  bool getBool(RemoteConfigFlag flag) {
    if (_useDebugConfig) return _debugConfig[flag.tag];
    return _currentConfig[flag.tag] ?? _defaultConfig[flag.tag];
  }

  @override
  int getInt(RemoteConfigFlag flag) {
    if (_useDebugConfig) return _debugConfig[flag.tag];
    return _currentConfig[flag.tag] ?? _defaultConfig[flag.tag];
  }

  @override
  String getString(RemoteConfigFlag flag) {
    if (_useDebugConfig) return _debugConfig[flag.tag];
    return _currentConfig[flag.tag] ?? _defaultConfig[flag.tag];
  }

  @override
  void addRemoteConfigListener(RemoteConfigListener listener) {
    _listeners.add(listener);
    listener.onRemoteConfigChanged(this);
  }

  @override
  void removeRemoteConfigListener(RemoteConfigListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<void> onResume() async {
    if (!_hasConfigExpired) return;

    try {
      await _fetchConfig();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  @override
  void onPause() {
    // do nothing
  }

  bool get _hasConfigExpired {
    return _slidingWindow.isPassed(
      key: _keyLastFetchWindow,
      nextWindow: _fetchConfigInterval,
    );
  }

  Future<void> _fetchConfig() async {
    final ParseResponse response = await ParseConfig().getConfigs();
    if (response.error != null) {
      _logger.logError(response.error);
      return;
    }

    final Map<String, dynamic> config = response.result;
    for (final flag in RemoteConfigFlag.values) {
      final String key = flag.tag;
      _currentConfig[key] = config[key];
    }

    await _storeCurrentConfig();
    _slidingWindow.restart(key: _keyLastFetchWindow);
    _logCurrentConfig();
    _notifyListeners();
  }

  void _logCurrentConfig() {
    final StringBuffer buffer = StringBuffer();

    buffer.write('new config={');
    _currentConfig.forEach((key, value) {
      buffer.write(key);
      buffer.write(':');
      buffer.write(value.toString());
      buffer.write(';');
    });
    buffer.write('}');

    _logger.log(buffer.toString());
  }

  Future<void> _loadCurrentConfig() async {
    _currentConfig.addAll(await _store.record(_storeRecord).get(await _db) ?? {});
  }

  Future<void> _storeCurrentConfig() async {
    await _store.record(_storeRecord).put(await _db, _currentConfig);
  }

  void _notifyListeners() {
    for (final listener in _listeners) {
      listener.onRemoteConfigChanged(this);
    }
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}
