import 'remote_config_manager.dart';

abstract class RemoteConfigListener {
  /// called when the remote config changes
  void onRemoteConfigChanged(RemoteConfigManager config);
}
