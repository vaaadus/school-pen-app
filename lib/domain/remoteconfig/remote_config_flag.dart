enum RemoteConfigFlag {
  /// Whether live query is allowed to be used.
  liveQueryEnabled,
}

extension RemoteConfigFlagExt on RemoteConfigFlag {
  /// Returns the technical tag for the flag.
  String get tag {
    switch (this) {
      case RemoteConfigFlag.liveQueryEnabled:
        return 'live_query_enabled';
    }
    throw ArgumentError('Unsupported remote config flag: $this');
  }
}
