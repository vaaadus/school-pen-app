import 'lifecycle_listener.dart';
import 'lifecycle_manager.dart';

class LifecycleManagerImpl implements LifecycleManager {
  final Set<LifecycleListener> _listeners = {};

  @override
  bool isResumed = false;

  @override
  void addLifecycleListener(LifecycleListener listener) {
    _listeners.add(listener);
    if (isResumed) listener.onResume();
  }

  @override
  void removeLifecycleListener(LifecycleListener listener) {
    _listeners.remove(listener);
  }

  @override
  void resume() {
    _dispatchLifecycleStateIfChanged(true);
  }

  @override
  void pause() {
    _dispatchLifecycleStateIfChanged(false);
  }

  void _dispatchLifecycleStateIfChanged(bool isResumed) {
    if (this.isResumed != isResumed) {
      this.isResumed = isResumed;
      for (var listener in _listeners) {
        _dispatchLifecycleEvent(listener, isResumed);
      }
    }
  }

  void _dispatchLifecycleEvent(LifecycleListener listener, bool isResumed) {
    if (isResumed) {
      listener.onResume();
    } else {
      listener.onPause();
    }
  }
}
