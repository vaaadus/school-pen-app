abstract class LifecycleListener {
  /// Called when the application resumes.
  void onResume();

  /// Called when the application pauses.
  void onPause();
}
