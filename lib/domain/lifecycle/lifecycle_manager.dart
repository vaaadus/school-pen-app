import 'lifecycle_listener.dart';

/// Allows to listen for the application lifecycle.
abstract class LifecycleManager {
  void addLifecycleListener(LifecycleListener listener);

  void removeLifecycleListener(LifecycleListener listener);

  bool get isResumed;

  void resume();

  void pause();
}
