import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

import 'school_year.dart';

/// Represents a request to add/edit a school year, such as 6th or 7th grade.
class AddSchoolYear extends Equatable {
  final String name;
  final DateTime updatedAt;

  const AddSchoolYear({
    @required this.name,
    @required this.updatedAt,
  })  : assert(name != null),
        assert(updatedAt != null);

  SchoolYear toSchoolYear({@required String id}) => SchoolYear(
        id: id,
        name: name,
        updatedAt: updatedAt,
      );

  bool get isValid => Strings.isNotBlank(name);

  AddSchoolYear trim() => AddSchoolYear(
        name: name.trim(),
        updatedAt: updatedAt,
      );

  @override
  List<Object> get props => [name, updatedAt];
}
