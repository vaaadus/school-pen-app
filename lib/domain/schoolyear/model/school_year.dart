import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'add_school_year.dart';

/// Represents a school year, such as 6th or 7th grade.
class SchoolYear extends Equatable implements Comparable<SchoolYear> {
  final String id;
  final String name;
  final DateTime updatedAt;

  const SchoolYear({
    @required this.id,
    @required this.name,
    @required this.updatedAt,
  })  : assert(id != null),
        assert(name != null),
        assert(updatedAt != null);

  AddSchoolYear toAddSchoolYear() => AddSchoolYear(
        name: name,
        updatedAt: updatedAt,
      );

  @override
  int compareTo(SchoolYear other) => name.compareTo(other.name);

  @override
  List<Object> get props => [id, name, updatedAt];
}
