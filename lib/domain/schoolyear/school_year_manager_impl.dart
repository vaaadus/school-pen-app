import 'dart:async';

import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/schoolyear/model/add_school_year.dart';
import 'package:school_pen/domain/schoolyear/repository/school_year_repository.dart';
import 'package:school_pen/domain/schoolyear/school_year_cleanup_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_locator.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:synchronized/synchronized.dart';

import 'model/school_year.dart';

class SchoolYearManagerImpl implements SchoolYearManager {
  static const Logger _logger = Logger('SchoolYearManagerImpl');
  static const String _defaultSchoolYearId = 'default-school-year-id';

  final Lock _lock = Lock();
  final Set<SchoolYearListener> _listeners = {};
  final Set<SchoolYearCleanupListener> _cleanupListeners = {};
  final SchoolYearRepository _repository;
  final SchoolYearLocator _locator;
  final UserConfigRepository _userConfig;
  final DateTimeProvider _dateTimeProvider;

  SchoolYearManagerImpl(this._repository, this._locator, this._userConfig, this._dateTimeProvider) {
    _repository.onChange.listen((_) => _dispatchSchoolYearsChanged());
    _repository.onCleanupSchoolYear.listen((String id) => _dispatchSchoolYearCleanup(id));
    _userConfig.onChange.listen((List<Property> properties) {
      if (properties.contains(Property.schoolYearId)) _dispatchSchoolYearsChanged();
    });
  }

  @override
  void addSchoolYearListener(SchoolYearListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onSchoolYearsChanged(this);
  }

  @override
  void removeSchoolYearListener(SchoolYearListener listener) {
    _listeners.remove(listener);
  }

  @override
  void addSchoolYearCleanupListener(SchoolYearCleanupListener listener) {
    _cleanupListeners.add(listener);
  }

  @override
  void removeSchoolYearCleanupListener(SchoolYearCleanupListener listener) {
    _cleanupListeners.remove(listener);
  }

  @override
  Future<List<SchoolYear>> getAll() {
    return _lock.synchronized(() async {
      await _ensureDefaultSchoolYearExists();

      final List<SchoolYear> years = await _repository.getAll();
      years.sort();
      return years;
    });
  }

  @override
  Future<SchoolYear> getById(String id) async {
    if (id == null) return null;

    return _lock.synchronized(() async {
      await _ensureDefaultSchoolYearExists();
      return _repository.getById(id);
    });
  }

  @override
  Future<SchoolYear> getActive() {
    return _lock.synchronized(() async {
      await _ensureDefaultSchoolYearExists();
      return _getActiveSchoolYear();
    });
  }

  @override
  Future<SchoolYear> getActiveOrNull() {
    return _lock.synchronized(() => _getActiveSchoolYear());
  }

  @override
  Future<void> setActive(String id) {
    return _lock.synchronized(() async {
      _logger.log('setActive, id: $id');

      if (await _repository.getById(id) != null) {
        await _userConfig.setSchoolYearId(id);
      } else {
        throw Exception('Cannot set active school year as it does not exist');
      }
    });
  }

  @override
  Future<String> create(AddSchoolYear year) {
    return _lock.synchronized(() async {
      _logger.log('create, name: "${year.name}"');

      if (year.isValid) {
        return await _repository.create(year.trim());
      } else {
        throw ArgumentError('Year not valid: $year');
      }
    });
  }

  @override
  Future<void> update(String id, AddSchoolYear year) {
    return _lock.synchronized(() async {
      _logger.log('update, id: $id');

      if (year.isValid) {
        await _repository.update(id, year.trim());
      } else {
        throw ArgumentError('Year must not be empty: $year');
      }
    });
  }

  @override
  Future<void> delete(String id) {
    return _lock.synchronized(() async {
      _logger.log('delete, id: $id');

      if (await _canDelete(id)) {
        await _repository.delete(id);
      } else {
        throw Exception('Cannot delete school year: $id');
      }
    });
  }

  @override
  Future<bool> canDelete(String id) {
    return _lock.synchronized(() => _canDelete(id));
  }

  Future<bool> _canDelete(String id) async {
    final SchoolYear active = await _getActiveSchoolYear();
    return active?.id != id;
  }

  Future<void> _ensureDefaultSchoolYearExists() async {
    final SchoolYear activeYear = await _getActiveSchoolYear();
    if (activeYear != null) return;

    final List<SchoolYear> years = await _repository.getAll();
    if (years.isNotEmpty) {
      await _userConfig.setSchoolYearId(years.first.id);
    } else {
      final SchoolYear year = _buildDefaultSchoolYear();
      await _repository.update(year.id, year.toAddSchoolYear());
      await _userConfig.setSchoolYearId(year.id);
    }
  }

  SchoolYear _buildDefaultSchoolYear() {
    return SchoolYear(
      id: _defaultSchoolYearId,
      name: _locator.buildDefaultName(),
      updatedAt: _dateTimeProvider.now(),
    );
  }

  Future<SchoolYear> _getActiveSchoolYear() async {
    return _repository.getById(await _userConfig.getSchoolYearId());
  }

  void _dispatchSchoolYearsChanged() {
    for (var listener in _listeners) {
      listener.onSchoolYearsChanged(this);
    }
  }

  Future<void> _dispatchSchoolYearCleanup(String yearId) async {
    for (var listener in _cleanupListeners) {
      await listener.onCleanupSchoolYear(yearId);
    }
  }
}
