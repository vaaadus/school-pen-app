import 'package:school_pen/domain/schoolyear/school_year_manager.dart';

abstract class SchoolYearListener {
  /// Called when any of school years changes.
  void onSchoolYearsChanged(SchoolYearManager manager);
}
