abstract class SchoolYearCleanupListener {
  /// Called when a school year is removed and all related data must be removed as well.
  Future<void> onCleanupSchoolYear(String schoolYearId);
}
