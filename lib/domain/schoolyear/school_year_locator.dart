import 'package:school_pen/domain/schoolyear/model/school_year.dart';

/// Provides resources for [SchoolYear].
class SchoolYearLocator {
  String buildDefaultName() {
    return DateTime.now().year.toString();
  }
}
