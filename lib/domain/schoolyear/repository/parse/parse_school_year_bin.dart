import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';

class ParseSchoolYearBin extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'SchoolYear_Bin';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';

  ParseSchoolYearBin() : super(keyClassName);

  ParseSchoolYearBin.clone() : this();

  @override
  ParseSchoolYearBin clone(Map map) => ParseSchoolYearBin.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);
}
