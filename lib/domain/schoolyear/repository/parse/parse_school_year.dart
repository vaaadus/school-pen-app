import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/schoolyear/model/add_school_year.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';

class ParseSchoolYear extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'SchoolYear';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keyName = 'name';

  ParseSchoolYear() : super(keyClassName);

  ParseSchoolYear.clone() : this();

  factory ParseSchoolYear.fromModel(SchoolYear year, ParseUser user) => ParseSchoolYear()
    ..updatedAt = year.updatedAt
    ..user = user
    ..uuid = year.id
    ..name = year.name;

  AddSchoolYear toAddSchoolYear() => AddSchoolYear(
        name: name,
        updatedAt: updatedAt,
      );

  @override
  ParseSchoolYear clone(Map map) => ParseSchoolYear.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get name => getNullable(keyName);

  set name(String name) => setNullable(keyName, name);

  /// Creates or updates a school year.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeSchoolYear').execute(parameters: {
      keyUuid: uuid,
      keyName: name,
    });
  }
}
