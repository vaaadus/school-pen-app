import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/schoolyear/model/add_school_year.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/repository/parse/parse_school_year.dart';
import 'package:school_pen/domain/schoolyear/repository/school_year_repository.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';

class ParseSchoolYearRepository implements SchoolYearRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseSchoolYearRepository');
  final SchoolYearRepository _delegate;
  Subscription _subscription;

  ParseSchoolYearRepository(this._delegate);

  @override
  Stream<SchoolYearRepository> get onChange => _delegate.onChange;

  @override
  Stream<String> get onCleanupSchoolYear => _delegate.onCleanupSchoolYear;

  @override
  Future<List<SchoolYear>> getAll() => _delegate.getAll();

  @override
  Future<SchoolYear> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddSchoolYear year) async {
    final String id = await _delegate.create(year);
    _try(() async {
      final SchoolYear newYear = await _delegate.getById(id);
      await _storeOnParse(newYear);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddSchoolYear year) async {
    final bool updated = await _delegate.update(id, year);
    if (updated) {
      _try(() async {
        final SchoolYear newYear = await _delegate.getById(id);
        await _storeOnParse(newYear);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(SchoolYear year) async {
    await _delegate.update(year.id, year.toAddSchoolYear());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseSchoolYear()));
    _subscription.on(LiveQueryEvent.create, (ParseSchoolYear object) {
      _try(() => _delegate.update(object.uuid, object.toAddSchoolYear()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseSchoolYear object) {
      _try(() => _delegate.update(object.uuid, object.toAddSchoolYear()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseSchoolYear object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(SchoolYear year) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseSchoolYear.fromModel(year, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseSchoolYear().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
