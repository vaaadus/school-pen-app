import 'package:school_pen/domain/schoolyear/model/add_school_year.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';

abstract class SchoolYearRepository {
  /// Stream changes whenever school years repository changes.
  Stream<SchoolYearRepository> get onChange;

  /// Stream IDs of removed school years to provide cleanup of dependencies.
  Stream<String> get onCleanupSchoolYear;

  /// Returns all stored school years.
  Future<List<SchoolYear>> getAll();

  /// Returns the school year by id or null.
  Future<SchoolYear> getById(String id);

  /// Returns a list of ids of removed school years.
  Future<List<String>> getDeletedIds();

  /// Creates a new school year and returns its ID.
  Future<String> create(AddSchoolYear year);

  /// Updates or creates a new school year with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddSchoolYear year);

  /// Removes a school year with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
