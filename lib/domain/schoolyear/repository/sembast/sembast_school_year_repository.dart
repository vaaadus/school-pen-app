import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/schoolyear/model/add_school_year.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/repository/school_year_repository.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// A [SchoolYearRepository] that saves school years in sembast document database.
class SembastSchoolYearRepository implements SchoolYearRepository {
  static const String recordDeletedIds = 'deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('schoolYears');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('schoolYears_bin');
  final BehaviorSubject<SchoolYearRepository> _onChange = BehaviorSubject();
  final BehaviorSubject<String> _onCleanupSchoolYear = BehaviorSubject();
  final Lock _lock = Lock();

  SembastSchoolYearRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<SchoolYearRepository> get onChange => _onChange;

  @override
  Stream<String> get onCleanupSchoolYear => _onCleanupSchoolYear;

  @override
  Future<List<SchoolYear>> getAll() async {
    final List<RecordSnapshot> snapshots = await _store.find(await _db);
    return snapshots.map((e) => _deserializeSchoolYear(e.value).toSchoolYear(id: e.key)).toList();
  }

  @override
  Future<SchoolYear> getById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> record = await _store.record(id).get(await _db);
    return _deserializeSchoolYear(record)?.toSchoolYear(id: id);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(recordDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<String> create(AddSchoolYear year) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = _serializeSchoolYear(year);
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddSchoolYear year) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = _serializeSchoolYear(year);
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = List.of(await getDeletedIds());
      if (ids.addIfAbsent(id)) {
        await _binStore.record(recordDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      _onCleanupSchoolYear.add(id);
      return deleted;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}

Map<String, dynamic> _serializeSchoolYear(AddSchoolYear year) {
  if (year == null) return null;

  return {
    'name': year.name,
    'updatedAt': SembastSerializer.serializeDateTimeField(year.updatedAt),
  };
}

AddSchoolYear _deserializeSchoolYear(Map<String, dynamic> map) {
  if (map == null) return null;

  return AddSchoolYear(
    name: map['name'],
    updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
  );
}
