import 'model/add_school_year.dart';
import 'model/school_year.dart';
import 'school_year_cleanup_listener.dart';
import 'school_year_listener.dart';

abstract class SchoolYearManager {
  void addSchoolYearListener(SchoolYearListener listener, {bool notifyOnAttach = true});

  void removeSchoolYearListener(SchoolYearListener listener);

  void addSchoolYearCleanupListener(SchoolYearCleanupListener listener);

  void removeSchoolYearCleanupListener(SchoolYearCleanupListener listener);

  /// Returns all created school years
  Future<List<SchoolYear>> getAll();

  /// Returns the school year by ID or null if it doesn't exist.
  Future<SchoolYear> getById(String id);

  /// Returns the currently selected school year. There always is an active school year.
  Future<SchoolYear> getActive();

  /// Returns the currently selected school year or null if it wasn't created yet.
  /// This might be useful if some code tries to cache something related to the school year.
  Future<SchoolYear> getActiveOrNull();

  /// Selects a new active school year.
  Future<void> setActive(String schoolYearId);

  /// Creates a new school year and returns its ID.
  Future<String> create(AddSchoolYear year);

  /// Updates or creates a new school year.
  Future<void> update(String id, AddSchoolYear year);

  /// Deletes the school year by ID.
  Future<void> delete(String id);

  /// Returns whether it is allowed to remove given school year.
  /// One case when it's not allowed is when there is just one school year.
  Future<bool> canDelete(String id);
}
