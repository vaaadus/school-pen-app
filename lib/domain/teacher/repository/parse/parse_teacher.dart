import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/teacher/model/add_teacher.dart';
import 'package:school_pen/domain/teacher/model/office_hours.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';

class ParseTeacher extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Teacher';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keySchoolYearId = 'schoolYearUuid';
  static const String keyName = 'name';
  static const String keyPhone = 'phone';
  static const String keyEmail = 'email';
  static const String keyWebsite = 'website';
  static const String keyAddress = 'address';
  static const String keyOfficeHours = 'officeHours';

  ParseTeacher() : super(keyClassName);

  ParseTeacher.clone() : this();

  factory ParseTeacher.fromModel(Teacher teacher, ParseUser user) => ParseTeacher()
    ..user = user
    ..uuid = teacher.id
    ..updatedAt = teacher.updatedAt
    ..schoolYearId = teacher.schoolYearId
    ..name = teacher.name
    ..phone = teacher.phone
    ..email = teacher.email
    ..website = teacher.website
    ..address = teacher.address
    ..officeHours = teacher.officeHours;

  AddTeacher toAddTeacher() => AddTeacher(
        schoolYearId: schoolYearId,
        name: name,
        phone: phone,
        email: email,
        website: website,
        address: address,
        officeHours: officeHours,
        updatedAt: updatedAt,
      );

  @override
  ParseTeacher clone(Map map) => ParseTeacher.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get schoolYearId => getNullable(keySchoolYearId);

  set schoolYearId(String id) => setNullable(keySchoolYearId, id);

  String get name => getNullable(keyName);

  set name(String name) => setNullable(keyName, name);

  String get phone => getNullable(keyPhone);

  set phone(String phone) => setNullable(keyPhone, phone);

  String get email => getNullable(keyEmail);

  set email(String email) => setNullable(keyEmail, email);

  String get website => getNullable(keyWebsite);

  set website(String website) => setNullable(keyWebsite, website);

  String get address => getNullable(keyAddress);

  set address(String address) => setNullable(keyAddress, address);

  OfficeHours get officeHours {
    final Map<String, dynamic> json = getNullable(keyOfficeHours);
    if (json == null) return null;
    return OfficeHours.fromJson(json);
  }

  set officeHours(OfficeHours officeHours) => setNullable(keyOfficeHours, officeHours?.toJson());

  /// Creates or updates a teacher.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeTeacher').execute(parameters: {
      keyUuid: uuid,
      keySchoolYearId: schoolYearId,
      keyName: name,
      keyPhone: phone,
      keyEmail: email,
      keyWebsite: website,
      keyAddress: address,
      keyOfficeHours: officeHours?.toJson(),
    });
  }
}
