import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';

class ParseTeacherBin extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Teacher_Bin';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';

  ParseTeacherBin() : super(keyClassName);

  ParseTeacherBin.clone() : this();

  @override
  ParseTeacherBin clone(Map map) => ParseTeacherBin.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);
}
