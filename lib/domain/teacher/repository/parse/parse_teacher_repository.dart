import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';
import 'package:school_pen/domain/teacher/model/add_teacher.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/repository/parse/parse_teacher.dart';
import 'package:school_pen/domain/teacher/repository/teacher_repository.dart';

class ParseTeacherRepository implements TeacherRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseTeacherRepository');
  final TeacherRepository _delegate;
  Subscription _subscription;

  ParseTeacherRepository(this._delegate);

  @override
  Stream<TeacherRepository> get onChange => _delegate.onChange;

  @override
  Stream<String> get onCleanupTeacher => _delegate.onCleanupTeacher;

  @override
  Future<List<Teacher>> getAll({String schoolYearId, List<String> ids}) {
    return _delegate.getAll(schoolYearId: schoolYearId, ids: ids);
  }

  @override
  Future<Teacher> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddTeacher teacher) async {
    final String id = await _delegate.create(teacher);
    _try(() async {
      final Teacher newTeacher = await _delegate.getById(id);
      await _storeOnParse(newTeacher);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddTeacher teacher) async {
    final bool updated = await _delegate.update(id, teacher);
    if (updated) {
      _try(() async {
        final Teacher newTeacher = await _delegate.getById(id);
        await _storeOnParse(newTeacher);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(Teacher teacher) async {
    await _delegate.update(teacher.id, teacher.toAddTeacher());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseTeacher()));
    _subscription.on(LiveQueryEvent.create, (ParseTeacher object) {
      _try(() => _delegate.update(object.uuid, object.toAddTeacher()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseTeacher object) {
      _try(() => _delegate.update(object.uuid, object.toAddTeacher()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseTeacher object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(Teacher teacher) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseTeacher.fromModel(teacher, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseTeacher().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
