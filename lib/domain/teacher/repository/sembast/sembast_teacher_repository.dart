import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/teacher/model/add_teacher.dart';
import 'package:school_pen/domain/teacher/model/office_hours.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/repository/teacher_repository.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// A [TeacherRepository] that saves teachers in sembast document database.
class SembastTeacherRepository implements TeacherRepository {
  static const String keyDeletedIds = 'key_sembast_deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('teachers');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('teachers_bin');
  final BehaviorSubject<TeacherRepository> _onChange = BehaviorSubject();
  final BehaviorSubject<String> _onCleanupTeacher = BehaviorSubject();
  final Lock _lock = Lock();

  SembastTeacherRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<TeacherRepository> get onChange => _onChange;

  @override
  Stream<String> get onCleanupTeacher => _onCleanupTeacher;

  @override
  Future<List<Teacher>> getAll({String schoolYearId, List<String> ids}) async {
    final List<Filter> filters = [
      if (schoolYearId != null) Filter.equals('schoolYearId', schoolYearId),
      if (ids != null) Filter.inList('id', ids),
    ];

    final Finder finder = filters.isEmpty ? null : Finder(filter: Filter.and(filters));
    final List<RecordSnapshot> snapshots = await _store.find(await _db, finder: finder);
    return snapshots.map((e) => _deserializeTeacher(e.value)).toList();
  }

  @override
  Future<Teacher> getById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> snapshot = await _store.record(id).get(await _db);
    return _deserializeTeacher(snapshot);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(keyDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<String> create(AddTeacher teacher) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = _serializeTeacher(teacher.toTeacher(id: id));
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddTeacher teacher) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = _serializeTeacher(teacher.toTeacher(id: id));
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = List.of(await getDeletedIds());
      if (ids.addIfAbsent(id)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      _onCleanupTeacher.add(id);
      return deleted;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}

Map<String, dynamic> _serializeTeacher(Teacher teacher) {
  if (teacher == null) return null;

  return {
    'id': teacher.id,
    'schoolYearId': teacher.schoolYearId,
    'name': teacher.name,
    'phone': teacher.phone,
    'email': teacher.email,
    'website': teacher.website,
    'address': teacher.address,
    'officeHours': teacher.officeHours.toJson(),
    'updatedAt': SembastSerializer.serializeDateTimeField(teacher.updatedAt),
  };
}

Teacher _deserializeTeacher(Map<String, dynamic> map) {
  if (map == null) return null;

  return Teacher(
    id: map['id'],
    schoolYearId: map['schoolYearId'],
    name: map['name'],
    phone: map['phone'],
    email: map['email'],
    website: map['website'],
    address: map['address'],
    officeHours: OfficeHours.fromJson(map['officeHours']),
    updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
  );
}
