import 'package:school_pen/domain/teacher/model/add_teacher.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';

abstract class TeacherRepository {
  /// Stream changes whenever teacher repository changes.
  Stream<TeacherRepository> get onChange;

  /// Stream ids of deleted teachers to cleanup dependencies.
  Stream<String> get onCleanupTeacher;

  /// Returns all stored teachers.
  /// Can be filtered by [schoolYearId], null means no filter.
  /// Can be filtered by [ids], null means no filter.
  Future<List<Teacher>> getAll({String schoolYearId, List<String> ids});

  /// Returns the teacher by ID or null if it doesn't exist.
  Future<Teacher> getById(String id);

  /// Returns a list of ids of deleted teachers.
  Future<List<String>> getDeletedIds();

  /// Creates a new teacher and returns its ID.
  Future<String> create(AddTeacher teacher);

  /// Updates or creates a new teacher with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddTeacher teacher);

  /// Removes a teacher with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
