import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/teacher/model/office_hours.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';

/// A model entity describing a request to add a teacher.
class AddTeacher extends Equatable {
  final String schoolYearId;
  final String name;
  final String phone;
  final String email;
  final String website;
  final String address;
  final OfficeHours officeHours;
  final DateTime updatedAt;

  AddTeacher({
    @required this.schoolYearId,
    @required this.name,
    this.phone,
    this.email,
    this.website,
    this.address,
    @required this.officeHours,
    @required this.updatedAt,
  })  : assert(schoolYearId != null),
        assert(name != null),
        assert(officeHours != null),
        assert(updatedAt != null);

  Teacher toTeacher({@required String id}) {
    return Teacher(
      id: id,
      schoolYearId: schoolYearId,
      name: name,
      phone: phone,
      email: email,
      website: website,
      address: address,
      officeHours: officeHours,
      updatedAt: updatedAt,
    );
  }

  bool get isValid => Strings.isNotBlank(name);

  /// Removes blank fields.
  AddTeacher trim() {
    return copyWith(
      name: Optional(name?.trim()),
      phone: Optional(phone?.trim()),
      email: Optional(email?.trim()),
      website: Optional(website?.trim()),
      address: Optional(address?.trim()),
    );
  }

  AddTeacher copyWith({
    Optional<String> schoolYearId,
    Optional<String> name,
    Optional<String> phone,
    Optional<String> email,
    Optional<String> website,
    Optional<String> address,
    Optional<OfficeHours> officeHours,
    Optional<DateTime> updatedAt,
  }) {
    return AddTeacher(
      schoolYearId: Optional.unwrapOrElse(schoolYearId, this.schoolYearId),
      name: Optional.unwrapOrElse(name, this.name),
      phone: Optional.unwrapOrElse(phone, this.phone),
      email: Optional.unwrapOrElse(email, this.email),
      website: Optional.unwrapOrElse(website, this.website),
      address: Optional.unwrapOrElse(address, this.address),
      officeHours: Optional.unwrapOrElse(officeHours, this.officeHours),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  @override
  List<Object> get props => [schoolYearId, name, phone, email, website, address, officeHours, updatedAt];
}
