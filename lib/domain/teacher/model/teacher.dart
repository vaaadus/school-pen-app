import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/teacher/model/add_teacher.dart';
import 'package:school_pen/domain/teacher/model/office_hours.dart';

/// A model entity describing a created teacher.
class Teacher extends Equatable implements Comparable<Teacher> {
  final String id;
  final String schoolYearId;
  final String name;
  final String phone;
  final String email;
  final String website;
  final String address;
  final OfficeHours officeHours;
  final DateTime updatedAt;

  Teacher({
    @required this.id,
    @required this.schoolYearId,
    @required this.name,
    this.phone,
    this.email,
    this.website,
    this.address,
    @required this.officeHours,
    @required this.updatedAt,
  })  : assert(id != null),
        assert(schoolYearId != null),
        assert(name != null),
        assert(officeHours != null),
        assert(updatedAt != null);

  AddTeacher toAddTeacher() {
    return AddTeacher(
      schoolYearId: schoolYearId,
      name: name,
      phone: phone,
      email: email,
      website: website,
      address: address,
      officeHours: officeHours,
      updatedAt: updatedAt,
    );
  }

  @override
  List<Object> get props => [schoolYearId, id, name, phone, email, website, address, officeHours, updatedAt];

  @override
  int compareTo(Teacher other) => name.compareTo(other.name);
}
