import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';

/// Represents ranges of time when a teacher is available for students for extra activities, etc.
class OfficeHours extends Equatable {
  final Map<Weekday, List<TimeRange>> map;

  OfficeHours(Map<Weekday, List<TimeRange>> initialMap) : map = Map.of(initialMap) {
    for (Weekday weekday in Weekday.values) {
      if (!map.containsKey(weekday)) map[weekday] = [];
    }
  }

  OfficeHours.empty() : map = {} {
    for (Weekday weekday in Weekday.values) {
      map[weekday] = [];
    }
  }

  factory OfficeHours.fromJson(Map<String, dynamic> json) => OfficeHours({
        Weekday.monday: _deserializeTimeRangeList(json['mon']),
        Weekday.tuesday: _deserializeTimeRangeList(json['tue']),
        Weekday.wednesday: _deserializeTimeRangeList(json['wed']),
        Weekday.thursday: _deserializeTimeRangeList(json['thu']),
        Weekday.friday: _deserializeTimeRangeList(json['fri']),
        Weekday.saturday: _deserializeTimeRangeList(json['sat']),
        Weekday.sunday: _deserializeTimeRangeList(json['sun']),
      });

  Map<String, dynamic> toJson() => {
        'mon': _serializeTimeRangeList(map[Weekday.monday]),
        'tue': _serializeTimeRangeList(map[Weekday.tuesday]),
        'wed': _serializeTimeRangeList(map[Weekday.wednesday]),
        'thu': _serializeTimeRangeList(map[Weekday.thursday]),
        'fri': _serializeTimeRangeList(map[Weekday.friday]),
        'sat': _serializeTimeRangeList(map[Weekday.saturday]),
        'sun': _serializeTimeRangeList(map[Weekday.sunday]),
      };

  /// Returns a new copy of this office hours with added time range.
  OfficeHours plus(Weekday weekday, TimeRange range) {
    return OfficeHours({
      ...map,
      weekday: [
        ...map[weekday],
        range,
      ],
    });
  }

  /// Returns a new copy of this office hours with removed time range.
  OfficeHours minus(Weekday weekday, TimeRange range) {
    return OfficeHours({
      ...map,
      weekday: map[weekday].where((e) => e != range).toList(),
    });
  }

  /// Returns a list of sorted time ranges by a weekday.
  List<OfficeHoursTime> sortedTimes() {
    final List<OfficeHoursTime> result = [];
    for (Weekday weekday in Weekday.values) {
      final List<TimeRange> ranges = List.of(map[weekday]);
      ranges.sort();
      for (TimeRange range in ranges) {
        result.add(OfficeHoursTime(weekday: weekday, timeRange: range));
      }
    }
    return result;
  }

  /// Whether there are none office hours available.
  bool get isEmpty {
    for (Weekday weekday in Weekday.values) {
      if (map[weekday].isNotEmpty) return false;
    }
    return true;
  }

  /// Whether there are any office hours available.
  bool get isNotEmpty => !isEmpty;

  @override
  List<Object> get props => [map];
}

List<dynamic> _serializeTimeRangeList(List<TimeRange> ranges) {
  if (ranges == null) return null;
  return ranges.map((range) => range.toJson()).toList();
}

List<TimeRange> _deserializeTimeRangeList(List<dynamic> list) {
  if (list == null) return null;
  return list.map((data) => TimeRange.fromJson(data)).toList();
}

class OfficeHoursTime extends Equatable {
  final Weekday weekday;
  final TimeRange timeRange;

  const OfficeHoursTime({@required this.weekday, @required this.timeRange});

  @override
  List<Object> get props => [weekday, timeRange];
}
