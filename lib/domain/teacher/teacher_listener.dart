import 'package:school_pen/domain/teacher/teacher_manager.dart';

abstract class TeacherListener {
  /// Called when any of teachers changes.
  void onTeachersChanged(TeacherManager manager);
}
