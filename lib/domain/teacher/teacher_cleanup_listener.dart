abstract class TeacherCleanupListener {
  /// Called when a teacher is removed and all related data must be removed as well.
  Future<void> onCleanupTeacher(String teacherId);
}
