import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/schoolyear/school_year_cleanup_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/teacher/model/add_teacher.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/teacher_cleanup_listener.dart';

import 'repository/teacher_repository.dart';
import 'teacher_listener.dart';
import 'teacher_manager.dart';

class TeacherManagerImpl implements TeacherManager, SchoolYearCleanupListener {
  static const Logger _logger = Logger('TeacherManagerImpl');

  final Set<TeacherListener> _listeners = {};
  final Set<TeacherCleanupListener> _cleanupListeners = {};
  final TeacherRepository _repository;
  final SchoolYearManager _schoolYearManager;

  TeacherManagerImpl(
    this._repository,
    this._schoolYearManager,
  ) {
    _schoolYearManager.addSchoolYearCleanupListener(this);
    _repository.onChange.listen((event) => _dispatchTeachersChanged());
    _repository.onCleanupTeacher.listen((id) => _dispatchTeacherCleanup(id));
  }

  @override
  void addTeacherListener(TeacherListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onTeachersChanged(this);
  }

  @override
  void removeTeacherListener(TeacherListener listener) {
    _listeners.remove(listener);
  }

  @override
  void addTeacherCleanupListener(TeacherCleanupListener listener) {
    _cleanupListeners.add(listener);
  }

  @override
  void removeTeacherCleanupListener(TeacherCleanupListener listener) {
    _cleanupListeners.remove(listener);
  }

  @override
  Future<List<Teacher>> getAll({String schoolYearId, List<String> ids}) async {
    final List<Teacher> teachers = await _repository.getAll(schoolYearId: schoolYearId, ids: ids);
    teachers.sort();
    return teachers;
  }

  @override
  Future<Teacher> getById(String id) async {
    if (id == null) return null;
    return _repository.getById(id);
  }

  @override
  Future<String> create(AddTeacher teacher) async {
    _logger.log('create, name: "${teacher.name}"');

    if (teacher.isValid) {
      return await _repository.create(teacher.trim());
    } else {
      throw ArgumentError('Teacher is not valid: $teacher');
    }
  }

  @override
  Future<void> update(String id, AddTeacher teacher) async {
    _logger.log('update, id: $id');

    if (teacher.isValid) {
      await _repository.update(id, teacher.trim());
    } else {
      throw ArgumentError('Teacher is not valid: $teacher');
    }
  }

  @override
  Future<void> delete(String id) async {
    _logger.log('delete, id: $id');

    await _repository.delete(id);
  }

  @override
  Future<void> onCleanupSchoolYear(String schoolYearId) async {
    final List<Future> futures = [];
    for (Teacher teacher in await _repository.getAll(schoolYearId: schoolYearId)) {
      futures.add(delete(teacher.id));
    }
    return Future.wait(futures);
  }

  void _dispatchTeachersChanged() {
    for (final listener in _listeners) {
      listener.onTeachersChanged(this);
    }
  }

  Future<void> _dispatchTeacherCleanup(String teacherId) async {
    for (final listener in _cleanupListeners) {
      await listener.onCleanupTeacher(teacherId);
    }
  }
}
