import 'package:school_pen/domain/teacher/model/add_teacher.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/teacher_cleanup_listener.dart';

import 'teacher_listener.dart';

/// Stores teachers.
abstract class TeacherManager {
  void addTeacherListener(TeacherListener listener, {bool notifyOnAttach = true});

  void removeTeacherListener(TeacherListener listener);

  void addTeacherCleanupListener(TeacherCleanupListener listener);

  void removeTeacherCleanupListener(TeacherCleanupListener listener);

  /// Returns a list of all created teachers.
  /// Can be filtered by [schoolYearId], null means no filter.
  /// Can be filtered by [ids], null means no filter.
  Future<List<Teacher>> getAll({String schoolYearId, List<String> ids});

  /// Returns the teacher by ID or null if it doesn't exist.
  Future<Teacher> getById(String id);

  /// Creates a new teacher and returns its ID.
  Future<String> create(AddTeacher teacher);

  /// Creates or updates a new teacher.
  Future<void> update(String id, AddTeacher teacher);

  Future<void> delete(String id);
}
