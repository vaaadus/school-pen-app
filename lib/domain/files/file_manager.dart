import 'package:school_pen/domain/files/model/remote_file.dart';

abstract class RemoteFileManager {
  /// Builds the remote file from an url.
  /// The implementation will know details how to get the actual file.
  RemoteFile loadFile(String url);
}
