import 'package:school_pen/domain/files/file_manager.dart';
import 'package:school_pen/domain/files/model/remote_file.dart';
import 'package:school_pen/domain/files/model/specialized/network_remote_file.dart';
import 'package:school_pen/domain/network/network_manager.dart';

class NetworkFileManager implements RemoteFileManager {
  final NetworkManager _networkManager;

  NetworkFileManager(this._networkManager);

  @override
  RemoteFile loadFile(String url) {
    return NetworkRemoteFile(_networkManager, url);
  }
}
