import 'dart:typed_data';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/optional.dart';

/// A file loaded into memory.
class MemoryFile extends Equatable {
  /// The name of the origin file.
  final String name;

  /// The path to the local origin file. Null on platforms that do not support the file system access (web).
  final String localPath;

  /// The file bytes.
  final Uint8List bytes;

  const MemoryFile({
    @required this.name,
    this.localPath,
    @required this.bytes,
  });

  MemoryFile copyWith({
    Optional<String> name,
    Optional<String> localPath,
    Optional<Uint8List> bytes,
  }) {
    return MemoryFile(
      name: Optional.unwrapOrElse(name, this.name),
      localPath: Optional.unwrapOrElse(localPath, this.localPath),
      bytes: Optional.unwrapOrElse(bytes, this.bytes),
    );
  }

  @override
  List<Object> get props => [name, localPath, bytes];
}
