import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:path/path.dart' as path;
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/files/model/remote_file.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/network/network_manager.dart';

class NetworkRemoteFile extends RemoteFile {
  static const Logger _logger = Logger('NetworkRemoteFile');
  final NetworkManager _networkManager;
  final String _url;

  const NetworkRemoteFile(this._networkManager, this._url);

  @override
  Future<MemoryFile> download() async {
    _logger.log('download, url: $_url');

    try {
      final Uri uri = Uri.parse(_url);
      final HttpClient httpClient = HttpClient();
      final HttpClientRequest request = await httpClient.getUrl(uri);
      final HttpClientResponse response = await request.close();

      if (response.statusCode == 200) {
        return MemoryFile(
          name: path.basename(_url),
          bytes: await consolidateHttpClientResponseBytes(response),
        );
      } else {
        throw HttpException('StatusCode: ${response.statusCode}', uri: uri);
      }
    } catch (error) {
      if (await _networkManager.isNetworkAvailable()) rethrow;
      throw NoInternetException();
    }
  }

  @override
  Future<File> store() async => null;

  @override
  Future<String> share() async => _url;

  @override
  String get url => _url;

  @override
  List<Object> get props => [_url];
}
