import 'dart:io';

import 'package:school_pen/domain/files/filesystem_file_manager.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/files/model/remote_file.dart';

class CachedNetworkRemoteFile extends RemoteFile {
  final FileSystemFileManager _files;
  final RemoteFile _delegate;
  final String _url;

  const CachedNetworkRemoteFile(this._files, this._delegate, this._url);

  @override
  Future<MemoryFile> download() async {
    final MemoryFile storedFile = await _files.readFile(_url);
    if (storedFile != null) return storedFile;

    final MemoryFile delegateFile = await _delegate.download();
    await _files.writeFile(_url, delegateFile.bytes);
    return delegateFile;
  }

  @override
  Future<File> store() async {
    final File file = await _files.getFile(_url);
    if (await _files.isCachedAndNotExpired(file, DateTime.now())) return file;

    final MemoryFile delegateFile = await _delegate.download();
    await _files.writeFile(_url, delegateFile.bytes);
    return file;
  }

  @override
  Future<String> share() async => _url;

  @override
  String get url => _url;

  @override
  List<Object> get props => [_delegate, _url];
}
