import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';

/// Represents a file resource, which either might be accessible locally or only through network interface.
abstract class RemoteFile extends Equatable {
  const RemoteFile();

  /// Gets the file.
  ///
  /// Throws [NoInternetException] if internet is required to get the file.
  Future<MemoryFile> download();

  /// Loads the remote file into the local storage and returns the file.
  /// Caches the file for future reads.
  /// Returns null on platforms where access to the file system is not supported.
  ///
  /// Throws [NoInternetException] if internet is required to get the file.
  Future<File> store();

  /// Makes the file publicly available and returns the url.
  /// Returns null if the file is not share'able.
  Future<String> share();

  /// The url used to construct this remote file.
  String get url;
}
