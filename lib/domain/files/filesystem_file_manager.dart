import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/files/file_manager.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/files/model/remote_file.dart';
import 'package:school_pen/domain/files/model/specialized/cached_network_remote_file.dart';
import 'package:school_pen/domain/files/model/specialized/network_remote_file.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/network/network_manager.dart';
import 'package:synchronized/synchronized.dart';

const Duration _cacheExpiration = Duration(days: 60);
const Logger _logger = Logger('FileSystemFileManager');

class FileSystemFileManager implements RemoteFileManager, LifecycleListener {
  final Lock _lock = Lock();
  final LifecycleManager _lifecycleManager;
  final NetworkManager _networkManager;
  final DateTimeProvider _dateTimeProvider;
  Timer _cleanupCacheTimer;

  FileSystemFileManager(this._lifecycleManager, this._networkManager, this._dateTimeProvider) {
    _lifecycleManager.addLifecycleListener(this);
  }

  @override
  RemoteFile loadFile(String url) {
    final NetworkRemoteFile networkFile = NetworkRemoteFile(_networkManager, url);
    return CachedNetworkRemoteFile(this, networkFile, url);
  }

  @override
  Future<void> onResume() async {
    _cleanupCacheTimer ??= Timer(
      Duration(seconds: 30),
      () => _lock.synchronized(() => _tryDeleteExpiredFiles()),
    );
  }

  @override
  void onPause() {
    _cleanupCacheTimer?.cancel();
    _cleanupCacheTimer = null;
  }

  Future<File> getFile(String url) {
    return _lock.synchronized(() => _getFile(url));
  }

  Future<MemoryFile> readFile(String url) {
    return _lock.synchronized(() async {
      final File localFile = await _getFile(url);
      if (await _isCachedAndNotExpired(localFile, _dateTimeProvider.now())) {
        return _readFile(localFile);
      }
      return null;
    });
  }

  Future<void> writeFile(String url, Uint8List bytes) {
    return _lock.synchronized(() async {
      final File destination = await _getFile(url);
      final File temp = File(destination.path + '.tmp');

      // Write to temp file, then rename to be sure that the destination file is not corrupted.
      // This can happen in case the app is destroyed when half of the file is written, etc...
      if (!await temp.exists()) await temp.create(recursive: true);
      await temp.writeAsBytes(bytes, flush: true);
      await temp.rename(destination.path);
    });
  }

  Future<bool> isCachedAndNotExpired(File file, DateTime now) {
    return _lock.synchronized(() => _isCachedAndNotExpired(file, now));
  }

  Future<File> _getFile(String url) async {
    final Directory tempDir = await _getCacheDir();
    final Uri uri = Uri.parse(url);
    return File(tempDir.path + uri.path);
  }

  Future<MemoryFile> _readFile(File file) async {
    return MemoryFile(
      name: path.basename(file.path),
      localPath: file.path,
      bytes: await file.readAsBytes(),
    );
  }

  void _tryDeleteExpiredFiles() async {
    try {
      await _deleteExpiredFiles(_dateTimeProvider.now());
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<void> _deleteExpiredFiles(DateTime at) async {
    final Directory directory = await _getCacheDir();
    if (!directory.existsSync()) return;

    final List<FileSystemEntity> fsEntities = directory.listSync();
    for (FileSystemEntity entity in fsEntities) {
      if (entity is File && await _isExpired(entity, at)) {
        await entity.delete();
      }
    }
  }

  Future<bool> _isCachedAndNotExpired(File file, DateTime now) async {
    if (!await file.exists()) return false;
    return !await _isExpired(file, now);
  }

  Future<bool> _isExpired(File file, DateTime at) async {
    final DateTime lastModified = await file.lastModified();
    return at.isAfter(lastModified.add(_cacheExpiration));
  }

  Future<Directory> _getCacheDir() async {
    final Directory tempDir = await getTemporaryDirectory();
    return Directory(tempDir.path + 'files/');
  }
}
