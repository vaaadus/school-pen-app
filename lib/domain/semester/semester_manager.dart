import 'model/add_semester.dart';
import 'model/semester.dart';
import 'semester_cleanup_listener.dart';
import 'semester_listener.dart';

abstract class SemesterManager {
  void addSemesterListener(SemesterListener listener, {bool notifyOnAttach = true});

  void removeSemesterListener(SemesterListener listener);

  void addSemesterCleanupListener(SemesterCleanupListener listener);

  void removeSemesterCleanupListener(SemesterCleanupListener listener);

  /// Returns all created semesters for the current school year.
  Future<List<Semester>> getAll();

  /// Returns the semester by ID or null if it doesn't exist.
  Future<Semester> getById(String id);

  /// Returns the currently selected semester in the active school year. There always is an active semester.
  Future<Semester> getActive();

  /// Selects a new active semester in the active school year.
  Future<void> setActive(String semesterId);

  /// Creates a new semester and returns its ID.
  Future<String> create(AddSemester semester);

  /// Updates or creates a new semester.
  Future<void> update(String id, AddSemester semester);

  /// Deletes the semester by ID.
  Future<void> delete(String id);

  /// Returns whether it is allowed to remove given semester.
  /// One case when it's not allowed is when there is just one semester.
  Future<bool> canDelete(String id);

  /// Returns whether an automatic semester change is enabled.
  ///
  /// Enabling this option will mean that the active semester
  /// is resolved by searching for the first semester which
  /// date range contains current date.
  Future<bool> isAutomaticSemesterChangeEnabled();

  /// Sets whether automatic semester change is able to work.
  Future<void> setAutomaticSemesterChangeEnabled(bool enabled);
}
