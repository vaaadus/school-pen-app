abstract class SemesterCleanupListener {
  /// Called when a semester is removed and all related data must be removed as well.
  Future<void> onCleanupSemester(String semesterId);
}
