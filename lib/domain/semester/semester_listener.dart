import 'package:school_pen/domain/semester/semester_manager.dart';

abstract class SemesterListener {
  /// Called when any of semesters changes.
  void onSemestersChanged(SemesterManager manager);
}
