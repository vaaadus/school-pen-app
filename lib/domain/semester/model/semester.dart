import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';

import 'add_semester.dart';

/// Represents a school term, such as winter/summer semester.
class Semester extends Equatable implements Comparable<Semester> {
  final String id;
  final String schoolYearId;
  final String name;
  final DateTime startDate;
  final DateTime endDate;
  final DateTime updatedAt;

  Semester({
    @required this.id,
    @required this.schoolYearId,
    @required this.name,
    this.startDate,
    this.endDate,
    @required this.updatedAt,
  })  : assert(id != null),
        assert(schoolYearId != null),
        assert(name != null),
        assert(updatedAt != null);

  AddSemester toAddSemester() => AddSemester(
        schoolYearId: schoolYearId,
        name: name,
        startDate: startDate,
        endDate: endDate,
        updatedAt: updatedAt,
      );

  bool isActiveAt(DateTime now) {
    if (endDate != null && now.isAfter(endDate)) return false;
    if (startDate == null) return false;
    return now.isAfterOrAt(startDate);
  }

  @override
  int compareTo(Semester other) {
    if (startDate != null && other.startDate != null) {
      final int result = startDate.compareTo(other.startDate);
      if (result != 0) return result;
    }

    if (endDate != null && other.endDate != null) {
      final int result = endDate.compareTo(other.endDate);
      if (result != 0) return result;
    }

    return name.compareTo(other.name);
  }

  @override
  List<Object> get props => [id, schoolYearId, name, startDate, endDate, updatedAt];
}
