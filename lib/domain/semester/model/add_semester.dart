import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

import 'semester.dart';

/// Represents a request to add/edit a school term, such as winter/summer semester.
class AddSemester extends Equatable {
  final String schoolYearId;
  final String name;
  final DateTime startDate;
  final DateTime endDate;
  final DateTime updatedAt;

  const AddSemester({
    @required this.schoolYearId,
    @required this.name,
    this.startDate,
    this.endDate,
    @required this.updatedAt,
  })  : assert(schoolYearId != null),
        assert(name != null),
        assert(updatedAt != null);

  Semester toSemester({@required String id}) => Semester(
        id: id,
        schoolYearId: schoolYearId,
        name: name,
        startDate: startDate,
        endDate: endDate,
        updatedAt: updatedAt,
      );

  bool get isValid => Strings.isNotBlank(name);

  AddSemester trim() => AddSemester(
        schoolYearId: schoolYearId,
        name: name.trim(),
        startDate: startDate,
        endDate: endDate,
        updatedAt: updatedAt,
      );

  @override
  List<Object> get props => [schoolYearId, name, startDate, endDate, updatedAt];
}
