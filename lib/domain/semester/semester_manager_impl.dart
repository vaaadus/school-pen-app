import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_cleanup_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/repository/semester_repository.dart';
import 'package:school_pen/domain/semester/semester_cleanup_listener.dart';
import 'package:school_pen/domain/semester/semester_listener.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:synchronized/synchronized.dart';

import 'model/semester.dart';
import 'semester_manager.dart';

class SemesterManagerImpl implements SemesterManager, SchoolYearListener, SchoolYearCleanupListener, LifecycleListener {
  static const Logger _logger = Logger('SemesterManagerImpl');

  final Lock _lock = Lock();
  final Set<SemesterListener> _listeners = {};
  final Set<SemesterCleanupListener> _cleanupListeners = {};
  final SemesterRepository _repository;
  final UserConfigRepository _userConfig;
  final SchoolYearManager _schoolYearManager;
  final LifecycleManager _lifecycle;
  final DateTimeProvider _dateTimeProvider;
  final DefaultSemesterNameGenerator _nameGenerator;
  final Map<String, String> _activeSemesterPerYearOverride = {};

  SemesterManagerImpl(
    this._repository,
    this._userConfig,
    this._schoolYearManager,
    this._lifecycle,
    this._dateTimeProvider,
    this._nameGenerator,
  ) {
    _repository.onChange.listen((event) => _dispatchSemestersChanged());
    _repository.onCleanupSemester.listen((id) => _dispatchSemesterCleanup(id));
    _userConfig.onChange.listen((List<Property> properties) {
      if (properties.contains(Property.semesterIdPerYear) || properties.contains(Property.autoSemesterChange))
        _dispatchSemestersChanged();
    });
    _schoolYearManager.addSchoolYearListener(this, notifyOnAttach: false);
    _schoolYearManager.addSchoolYearCleanupListener(this);
    _lifecycle.addLifecycleListener(this);
  }

  @override
  void addSemesterListener(SemesterListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onSemestersChanged(this);
  }

  @override
  void removeSemesterListener(SemesterListener listener) {
    _listeners.remove(listener);
  }

  @override
  void addSemesterCleanupListener(SemesterCleanupListener listener) {
    _cleanupListeners.add(listener);
  }

  @override
  void removeSemesterCleanupListener(SemesterCleanupListener listener) {
    _cleanupListeners.remove(listener);
  }

  @override
  Future<List<Semester>> getAll() async {
    final SchoolYear year = await _schoolYearManager.getActive();
    await _ensureDefaultSemesterExistsForYear(year.id);

    final List<Semester> semesters = await _repository.getAll(year.id);
    semesters.sort();
    return semesters;
  }

  @override
  Future<Semester> getById(String id) async {
    if (id == null) return null;
    return _repository.getById(id);
  }

  @override
  Future<Semester> getActive() async {
    final SchoolYear year = await _schoolYearManager.getActive();
    await _ensureDefaultSemesterExistsForYear(year.id);
    await _resolveActiveSemesterPerYear(year.id);
    return _getActiveSemesterPerYear(year.id);
  }

  @override
  Future<void> setActive(String id) async {
    _logger.log('setActive, id: $id');

    if (await _repository.getById(id) != null) {
      final SchoolYear year = await _schoolYearManager.getActive();
      _activeSemesterPerYearOverride[year.id] = id;
      await _setActiveSemesterIdPerYear(year.id, id);
    } else {
      throw Exception('Cannot set active semester as it does not exist');
    }
  }

  @override
  Future<String> create(AddSemester semester) async {
    _logger.log('create, name: "${semester.name}"');

    if (semester.isValid) {
      final String id = await _repository.create(semester.trim());
      await _resolveActiveSemesterPerYear(semester.schoolYearId);
      return id;
    } else {
      throw ArgumentError('Semester not valid: $semester');
    }
  }

  @override
  Future<void> update(String id, AddSemester semester) async {
    _logger.log('update, id: $id');

    if (semester.isValid) {
      await _repository.update(id, semester.trim());
      await _resolveActiveSemesterPerYear(semester.schoolYearId);
    } else {
      throw ArgumentError('Semester not valid: $semester');
    }
  }

  @override
  Future<void> delete(String id) async {
    _logger.log('delete, id: $id');

    if (await canDelete(id)) {
      await _repository.delete(id);
    } else {
      throw Exception('Cannot delete semester: $id');
    }
  }

  @override
  Future<bool> canDelete(String id) async {
    for (SchoolYear year in await _schoolYearManager.getAll()) {
      final Semester semester = await _getActiveSemesterPerYear(year.id);
      if (semester?.id == id) return false;
    }

    return true;
  }

  @override
  Future<bool> isAutomaticSemesterChangeEnabled() async {
    return await _userConfig.isAutoSemesterChangeEnabled() ?? true;
  }

  @override
  Future<void> setAutomaticSemesterChangeEnabled(bool enabled) async {
    await _userConfig.setAutoSemesterChangeEnabled(enabled);

    final SchoolYear year = await _schoolYearManager.getActive();
    await _resolveActiveSemesterPerYear(year.id);
  }

  @override
  void onSchoolYearsChanged(SchoolYearManager manager) {
    _dispatchSemestersChanged();
  }

  @override
  Future<void> onCleanupSchoolYear(String schoolYearId) async {
    final List<Future> futures = [];
    for (Semester semester in await _repository.getAll(schoolYearId)) {
      futures.add(delete(semester.id));
    }
    return Future.wait(futures);
  }

  @override
  Future<void> onResume() async {
    // do nothing
  }

  @override
  void onPause() async {
    _activeSemesterPerYearOverride.clear();
  }

  Future<void> _ensureDefaultSemesterExistsForYear(String schoolYearId) {
    return _lock.synchronized(() async {
      final Semester activeSemester = await _getActiveSemesterPerYear(schoolYearId);
      if (activeSemester != null) return;

      _activeSemesterPerYearOverride.clear();

      final List<Semester> semesters = await _repository.getAll(schoolYearId);
      if (semesters.isNotEmpty) {
        await _setActiveSemesterIdPerYear(schoolYearId, semesters.first.id);
      } else {
        final String id = await _repository.create(await _buildDefaultSemesterOfYear(schoolYearId));
        await _setActiveSemesterIdPerYear(schoolYearId, id);
      }
    });
  }

  Future<AddSemester> _buildDefaultSemesterOfYear(String schoolYearId) async {
    return AddSemester(
      schoolYearId: schoolYearId,
      name: await _nameGenerator.generateName(),
      updatedAt: _dateTimeProvider.now(),
    );
  }

  Future<Semester> _getActiveSemesterPerYear(String schoolYearId) async {
    return _repository.getById(await _getActiveSemesterIdPerYear(schoolYearId));
  }

  Future<void> _resolveActiveSemesterPerYear(String schoolYearId) async {
    if (_activeSemesterPerYearOverride.containsKey(schoolYearId)) return;

    if (await isAutomaticSemesterChangeEnabled()) {
      final Semester semester = await _findFirstActiveSemesterByDatePerYear(schoolYearId);
      if (semester != null) await _setActiveSemesterIdPerYear(schoolYearId, semester.id);
    }
  }

  Future<Semester> _findFirstActiveSemesterByDatePerYear(String schoolYearId) async {
    for (Semester semester in await _repository.getAll(schoolYearId)) {
      if (semester.isActiveAt(_dateTimeProvider.now())) return semester;
    }
    return null;
  }

  Future<String> _getActiveSemesterIdPerYear(String schoolYearId) async {
    if (_activeSemesterPerYearOverride.containsKey(schoolYearId)) {
      return _activeSemesterPerYearOverride[schoolYearId];
    } else {
      return _userConfig.getSemesterIdPerYear(schoolYearId);
    }
  }

  Future<void> _setActiveSemesterIdPerYear(String schoolYearId, String id) async {
    await _userConfig.setSemesterIdPerYear(schoolYearId, id);
  }

  void _dispatchSemestersChanged() {
    for (var listener in _listeners) {
      listener.onSemestersChanged(this);
    }
  }

  Future<void> _dispatchSemesterCleanup(String semesterId) async {
    for (var listener in _cleanupListeners) {
      await listener.onCleanupSemester(semesterId);
    }
  }

  static String buildActiveSemesterKey(String schoolYearId) {
    return 'key_semester_activeIdInYear_' + schoolYearId;
  }
}

class DefaultSemesterNameGenerator {
  Future<String> generateName() async {
    final S s = S.current ?? await S.load(_locale);
    return s.common_semesterTerm + ' I';
  }

  Locale get _locale {
    if (kIsWeb) return window.locale;
    return Locale(Platform.localeName);
  }
}
