import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

import '../semester_repository.dart';

/// A [SemesterRepository] that saves semesters in sembast document database.
class SembastSemesterRepository implements SemesterRepository {
  static const String keyDeletedIds = 'key_sembast_deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('semesters');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('semesters_bin');
  final BehaviorSubject<SemesterRepository> _onChange = BehaviorSubject();
  final BehaviorSubject<String> _onCleanupSemester = BehaviorSubject();
  final Lock _lock = Lock();

  SembastSemesterRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<SemesterRepository> get onChange => _onChange;

  @override
  Stream<String> get onCleanupSemester => _onCleanupSemester;

  @override
  Future<List<Semester>> getAll([String schoolYearId]) async {
    final Finder finder = schoolYearId == null ? null : Finder(filter: Filter.equals('schoolYear', schoolYearId));
    final List<RecordSnapshot> snapshots = await _store.find(await _db, finder: finder);
    return snapshots.map((e) => _deserializeSemester(e.value).toSemester(id: e.key)).toList();
  }

  @override
  Future<Semester> getById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> record = await _store.record(id).get(await _db);
    return _deserializeSemester(record)?.toSemester(id: id);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(keyDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<String> create(AddSemester semester) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = _serializeSemester(semester);
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddSemester semester) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = _serializeSemester(semester);
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = List.of(await getDeletedIds());
      if (ids.addIfAbsent(id)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      _onCleanupSemester.add(id);
      return deleted;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}

Map<String, dynamic> _serializeSemester(AddSemester semester) {
  if (semester == null) return null;

  return {
    'schoolYear': semester.schoolYearId,
    'name': semester.name,
    if (semester.startDate != null) 'startDate': SembastSerializer.serializeDateTimeField(semester.startDate),
    if (semester.endDate != null) 'endDate': SembastSerializer.serializeDateTimeField(semester.endDate),
    'updatedAt': SembastSerializer.serializeDateTimeField(semester.updatedAt),
  };
}

AddSemester _deserializeSemester(Map<String, dynamic> map) {
  if (map == null) return null;

  return AddSemester(
    name: map['name'],
    schoolYearId: map['schoolYear'],
    startDate: SembastSerializer.deserializeDateTimeField(map['startDate']),
    endDate: SembastSerializer.deserializeDateTimeField(map['endDate']),
    updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
  );
}
