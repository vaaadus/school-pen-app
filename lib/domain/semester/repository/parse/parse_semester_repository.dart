import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/repository/parse/parse_semester.dart';
import 'package:school_pen/domain/semester/repository/semester_repository.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';

class ParseSemesterRepository implements SemesterRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseSemesterRepository');
  final SemesterRepository _delegate;
  Subscription _subscription;

  ParseSemesterRepository(this._delegate);

  @override
  Stream<SemesterRepository> get onChange => _delegate.onChange;

  @override
  Stream<String> get onCleanupSemester => _delegate.onCleanupSemester;

  @override
  Future<List<Semester>> getAll([String schoolYearId]) => _delegate.getAll(schoolYearId);

  @override
  Future<Semester> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddSemester semester) async {
    final String id = await _delegate.create(semester);
    _try(() async {
      final Semester newSemester = await _delegate.getById(id);
      await _storeOnParse(newSemester);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddSemester semester) async {
    final bool updated = await _delegate.update(id, semester);
    if (updated) {
      _try(() async {
        final Semester newSemester = await _delegate.getById(id);
        await _storeOnParse(newSemester);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(Semester semester) async {
    await _delegate.update(semester.id, semester.toAddSemester());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseSemester()));
    _subscription.on(LiveQueryEvent.create, (ParseSemester object) {
      _try(() => _delegate.update(object.uuid, object.toAddSemester()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseSemester object) {
      _try(() => _delegate.update(object.uuid, object.toAddSemester()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseSemester object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(Semester semester) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseSemester.fromModel(semester, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseSemester().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
