import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/model/semester.dart';

class ParseSemester extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Semester';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keySchoolYear = 'schoolYearUuid';
  static const String keyName = 'name';
  static const String keyStartDate = 'startDate';
  static const String keyEndDate = 'endDate';

  ParseSemester() : super(keyClassName);

  ParseSemester.clone() : this();

  factory ParseSemester.fromModel(Semester semester, ParseUser user) => ParseSemester()
    ..user = user
    ..uuid = semester.id
    ..schoolYearId = semester.schoolYearId
    ..name = semester.name
    ..updatedAt = semester.updatedAt
    ..startDate = semester.startDate
    ..endDate = semester.endDate;

  AddSemester toAddSemester() => AddSemester(
        schoolYearId: schoolYearId,
        name: name,
        startDate: startDate,
        endDate: endDate,
        updatedAt: updatedAt,
      );

  @override
  ParseSemester clone(Map map) => ParseSemester.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get schoolYearId => getNullable(keySchoolYear);

  set schoolYearId(String id) => setNullable(keySchoolYear, id);

  String get name => getNullable(keyName);

  set name(String name) => setNullable(keyName, name);

  DateTime get startDate => getDateTime(keyStartDate);

  set startDate(DateTime date) => setNullable(keyStartDate, date);

  DateTime get endDate => getDateTime(keyEndDate);

  set endDate(DateTime date) => setNullable(keyEndDate, date);

  /// Creates or updates a semester.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeSemester').execute(parameters: {
      keyUuid: uuid,
      keySchoolYear: schoolYearId,
      keyName: name,
      keyStartDate: startDate?.toString(),
      keyEndDate: endDate?.toString(),
    });
  }
}
