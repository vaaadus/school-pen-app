import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/model/semester.dart';

abstract class SemesterRepository {
  /// Stream changes whenever semester repository changes.
  Stream<SemesterRepository> get onChange;

  /// Stream IDs of removed semesters to provide cleanup of dependencies.
  Stream<String> get onCleanupSemester;

  /// Returns all stored semesters.
  /// Can be filtered by [schoolYearId], null means no filter.
  Future<List<Semester>> getAll([String schoolYearId]);

  /// Returns the semester by id or null.
  Future<Semester> getById(String id);

  /// Returns a list of ids of removed semesters.
  Future<List<String>> getDeletedIds();

  /// Creates a new semester and returns its ID.
  Future<String> create(AddSemester semester);

  /// Updates or creates a new semester with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddSemester semester);

  /// Removes a semester with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
