import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../logger.dart';

const Logger _logger = Logger('HttpLogger');
const int _codeSuccessStart = 200;
const int _codeSuccessEnd = 399;

http.Response logHttpResponse(http.Response response) {
  if (_requestFailed(response)) {
    _logger.log('HTTP ${response.statusCode} ${response.request.method} ${response.request.url}');
    _logger.log('reason: ${response.reasonPhrase}');
    _logger.log('body: ${response.body}');
  } else {
    _logger.log('HTTP ${response.statusCode} ${response.request.method} ${response.request.url}');
    if (kDebugMode) {
      _logger.log('body: ${response.body}');
    }
  }
  return response;
}

bool _requestFailed(http.Response response) {
  return response.statusCode < _codeSuccessStart || response.statusCode > _codeSuccessEnd;
}
