import 'package:school_pen/domain/ads/exception/ad_failed_to_load_exception.dart';
import 'package:school_pen/domain/common/exception/at_least_one_value_must_be_selected_exception.dart';
import 'package:school_pen/domain/common/exception/date_range_start_after_end_exception.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/common/exception/not_found_exception.dart';
import 'package:school_pen/domain/common/exception/number_out_of_range_exception.dart';
import 'package:school_pen/domain/common/exception/value_out_of_constraints_exception.dart';
import 'package:school_pen/domain/notification/exception/permission_denied_exception.dart';
import 'package:school_pen/domain/user/exception/account_already_linked_exception.dart';
import 'package:school_pen/domain/user/exception/avatar_size_too_big_exception.dart';
import 'package:school_pen/domain/user/exception/email_taken_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_credentials_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_email_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_password_exception.dart';
import 'package:school_pen/domain/user/exception/password_cannot_contain_username_exception.dart';
import 'package:school_pen/domain/user/exception/password_too_short_exception.dart';
import 'package:school_pen/domain/user/exception/passwords_do_not_match_exception.dart';
import 'package:school_pen/domain/user/exception/too_many_failed_login_attempts_exception.dart';
import 'package:school_pen/domain/user/exception/unauthorized_exception.dart';
import 'package:school_pen/domain/user/exception/username_taken_exception.dart';
import 'package:school_pen/domain/user/exception/username_too_short_exception.dart';
import 'package:sentry/sentry.dart';

import '../logger_appender.dart';

final List<Type> _ignoredErrors = [
  AccountAlreadyLinkedException,
  AvatarSizeTooBigException,
  UnauthorizedException,
  NotFoundException,
  NoInternetException,
  EmailTakenException,
  InvalidCredentialsException,
  InvalidEmailException,
  InvalidPasswordException,
  PasswordCannotContainUsernameException,
  PasswordTooShortException,
  PasswordsDoNotMatchException,
  TooManyFailedLoginAttemptsException,
  UsernameTakenException,
  UsernameTooShortException,
  AtLeastOneValueMustBeSelectedException,
  DateRangeStartAfterEndException,
  FieldMustNotBeEmptyException,
  NumberOutOfRangeException,
  ValueOutOfConstraintsException,
  AdFailedToLoadException,
  PermissionDeniedException,
];

class SentryLoggerAppender implements LoggerAppender {
  static const String _dsn = 'https://5de49161314c464c807d4ef720ced188@o420207.ingest.sentry.io/5338195';

  final SentryClient _sentry = SentryClient(SentryOptions(dsn: _dsn));

  @override
  void append(String message) {
    // do nothing
  }

  @override
  void appendError(dynamic error, [StackTrace stackTrace]) async {
    _captureException(error, stackTrace);
  }

  @override
  void appendCrash(dynamic error, [StackTrace stackTrace]) async {
    _captureException(error, stackTrace);
  }

  void _captureException(dynamic error, [StackTrace stackTrace]) async {
    if (_isErrorIgnored(error)) return;

    try {
      await _sentry.captureException(error, stackTrace: stackTrace);
    } catch (e) {
      // ignored
    }
  }

  bool _isErrorIgnored(dynamic error) {
    if (error == null) return true;
    if (_ignoredErrors.contains(error.runtimeType)) return true;
    if (error.toString().contains(RegExp('SocketException'))) return true;

    return false;
  }
}
