import '../logger_appender.dart';

/// Prints logs to the console.
class ConsoleLoggerAppender implements LoggerAppender {
  @override
  void append(String message) {
    print('${DateTime.now()}/ $message');
  }

  @override
  void appendError(dynamic error, [StackTrace stackTrace]) {
    print('${DateTime.now()}/ ${error?.runtimeType}: $error');
    if (stackTrace != null) {
      print('${DateTime.now()}/ $stackTrace');
    }
  }

  @override
  void appendCrash(dynamic error, [StackTrace stackTrace]) {
    print('${DateTime.now()}/ $error');
    if (stackTrace != null) {
      print('${DateTime.now()}/ $stackTrace');
    }
  }
}
