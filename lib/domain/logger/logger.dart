import 'package:school_pen/domain/diagnosis/diagnosis.dart';

import 'logger_appender.dart';

class Logger {
  static final List<LoggerAppender> _appenders = [];
  final String tag;

  const Logger(this.tag);

  static void addAppender(LoggerAppender appender) {
    _appenders.add(appender);
  }

  void log(Object message) {
    if (Diagnosis.isDiagnosisEnabled) {
      for (var appender in _appenders) {
        appender.append('$tag: $message');
      }
    }
  }

  void logError(dynamic error, [StackTrace stackTrace]) {
    if (Diagnosis.isDiagnosisEnabled) {
      for (var appender in _appenders) {
        appender.appendError(error, stackTrace);
      }
    }
  }

  void logCrash(dynamic error, StackTrace stackTrace) {
    if (Diagnosis.isDiagnosisEnabled) {
      for (var appender in _appenders) {
        appender.appendCrash(error, stackTrace);
      }
    }
  }
}
