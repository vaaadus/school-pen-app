/// Abstracts from how logs are logged.
abstract class LoggerAppender {
  void append(String message);

  void appendError(dynamic error, [StackTrace stackTrace]);

  void appendCrash(dynamic error, [StackTrace stackTrace]);
}
