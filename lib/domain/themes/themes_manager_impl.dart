import 'package:school_pen/domain/ads/ad_manager.dart';
import 'package:school_pen/domain/ads/model/ad_placement.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/themes/model/custom_theme.dart';
import 'package:school_pen/domain/themes/themes_listener.dart';
import 'package:school_pen/domain/themes/themes_manager.dart';

class ThemesManagerImpl implements ThemesManager {
  static const String keySelectedTheme = 'key_themeManager_selectedTheme';
  final Set<ThemesListener> _listeners = {};
  final AdManager _adManager;
  final Preferences _preferences;

  ThemesManagerImpl(this._adManager, this._preferences);

  @override
  void addThemesListener(ThemesListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onThemeSelected(selectedTheme);
  }

  @override
  void removeThemesListener(ThemesListener listener) {
    _listeners.remove(listener);
  }

  @override
  CustomTheme get selectedTheme {
    final String tag = _preferences.getString(keySelectedTheme);
    return CustomThemeExt.forTag(tag) ?? CustomThemeExt.defaultTheme;
  }

  @override
  Future<void> setSelectedTheme(CustomTheme theme) async {
    assert(theme != null);

    if (selectedTheme != theme && _isThemeUnlocked(theme)) {
      _preferences.setString(keySelectedTheme, theme.tag);
      _dispatchThemeSelected(theme);
    }
  }

  // TODO check premium
  @override
  Future<List<CustomTheme>> get unlockedThemes async {
    final List<CustomTheme> themes = [];
    for (CustomTheme theme in CustomTheme.values) {
      if (_isThemeUnlocked(theme)) {
        themes.add(theme);
      }
    }
    return themes;
  }

  @override
  Future<List<CustomTheme>> get lockedThemes async {
    final List<CustomTheme> result = List.of(CustomTheme.values);
    for (CustomTheme theme in await unlockedThemes) result.remove(theme);
    return result;
  }

  @override
  Future<bool> playRewardedContentForTheme(CustomTheme theme) async {
    final bool rewarded = await _adManager.showRewardedAd(RewardedAdPlacement.Themes);
    if (rewarded) {
      _setThemeUnlocked(theme);
    }
    return rewarded;
  }

  @override
  Future<void> preloadRewardedContent() async {
    await _adManager.preloadRewardedAd(RewardedAdPlacement.Themes);
  }

  bool _isThemeUnlocked(CustomTheme theme) {
    return !theme.isPremium || (_preferences.getBool(_buildKey(theme)) ?? false);
  }

  void _setThemeUnlocked(CustomTheme theme) {
    _preferences.setBool(_buildKey(theme), true);
  }

  String _buildKey(CustomTheme theme) {
    return 'themeManager_unlockedTheme_${theme.tag}';
  }

  void _dispatchThemeSelected(CustomTheme theme) {
    for (ThemesListener listener in _listeners) {
      listener.onThemeSelected(theme);
    }
  }
}
