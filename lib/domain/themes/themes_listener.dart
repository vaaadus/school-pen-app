import 'package:school_pen/domain/themes/model/custom_theme.dart';

abstract class ThemesListener {
  /// Called when a theme gets selected and it is changed.
  void onThemeSelected(CustomTheme theme);
}
