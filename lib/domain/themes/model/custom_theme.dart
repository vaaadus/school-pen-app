/// Defines all possible themes a user can set up.
/// A theme defines how the application will look & feel like.
enum CustomTheme {
  system,
  light,
  amber,
  red,
  green,
  dark,
  black,
  navyBlue,
}

extension CustomThemeExt on CustomTheme {
  static CustomTheme get defaultTheme => CustomTheme.system;

  String get tag {
    switch (this) {
      case CustomTheme.system:
        return 'system';
      case CustomTheme.light:
        return 'light';
      case CustomTheme.amber:
        return 'amber';
      case CustomTheme.red:
        return 'red';
      case CustomTheme.green:
        return 'green';
      case CustomTheme.dark:
        return 'dark';
      case CustomTheme.black:
        return 'black';
      case CustomTheme.navyBlue:
        return 'navyBlue';
    }
    throw ArgumentError('Unsupported theme: $this');
  }

  static CustomTheme forTag(String tag) {
    for (CustomTheme theme in CustomTheme.values) {
      if (theme.tag == tag) {
        return theme;
      }
    }
    return null;
  }

  bool get isPremium {
    switch (this) {
      case CustomTheme.system:
        return false;
      case CustomTheme.light:
      case CustomTheme.amber:
      case CustomTheme.red:
      case CustomTheme.green:
      case CustomTheme.dark:
      case CustomTheme.black:
      case CustomTheme.navyBlue:
        return true;
    }
    throw ArgumentError('Unsupported theme: $this');
  }
}
