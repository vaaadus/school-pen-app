import 'package:school_pen/domain/ads/exception/ad_failed_to_load_exception.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/themes/model/custom_theme.dart';
import 'package:school_pen/domain/themes/themes_listener.dart';

abstract class ThemesManager {
  /// Register a listener to listen for changed themes.
  /// Set [notifyOnAttach] if you'd like to skip initial notification.
  void addThemesListener(ThemesListener listener, {bool notifyOnAttach = true});

  /// Unregister a themes listener.
  void removeThemesListener(ThemesListener listener);

  /// Returns the currently selected theme.
  CustomTheme get selectedTheme;

  /// Sets a new theme as selected.
  /// If theme is not unlocked, the method call is no-op.
  Future<void> setSelectedTheme(CustomTheme theme);

  /// Returns the list of available themes which are unlocked. Either they are free or user has unlocked them.
  Future<List<CustomTheme>> get unlockedThemes;

  /// Returns the list of available themes which users must unlock by going premium or watching ads.
  Future<List<CustomTheme>> get lockedThemes;

  /// Requests the manager to start the unlock flow. This might result in showing ads, etc.
  /// Returns true if the theme was granted, false otherwise.
  ///
  /// Throws [NoInternetException] if internet is required.
  /// Throws [AdFailedToLoadException] if ad couldn't be loaded.
  Future<bool> playRewardedContentForTheme(CustomTheme theme);

  /// Requests the rewarded content to be cached in order to minimize the delay required to play it.
  ///
  /// Throws [NoInternetException] if internet is required.
  /// Throws [AdFailedToLoadException] if ad couldn't be loaded.
  Future<void> preloadRewardedContent();
}
