import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/sync/sync_manager.dart';
import 'package:school_pen/domain/work/repository/work_repository.dart';
import 'package:school_pen/domain/work/work_listener.dart';
import 'package:school_pen/domain/work/work_manager.dart';

const Logger _logger = Logger('WorkManagerImpl');
const Duration _frequency = Duration(minutes: 30);
const String _syncKey = 'workManager_syncData_key';
const Duration _syncDataFrequency = Duration(hours: 6);

class WorkManagerImpl implements WorkManager {
  final Set<WorkListener> _listeners = {};
  final WorkRepository _repository;
  final SyncManager _syncManager;
  final SlidingWindow _slidingWindow;

  WorkManagerImpl(this._repository, this._syncManager, this._slidingWindow);

  @override
  Future<void> initialize() async {
    await _repository.initialize(_frequency);
  }

  @override
  void addWorkListener(WorkListener listener) {
    _listeners.add(listener);
  }

  @override
  void removeWorkListener(WorkListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<void> onBackgroundWork() async {
    await _trySyncData();
    await _dispatchBackgroundWork();
  }

  Future<void> _trySyncData() async {
    if (_slidingWindow.isPassed(key: _syncKey, nextWindow: _syncDataFrequency)) {
      try {
        await _syncManager.sync();
        _slidingWindow.restart(key: _syncKey);
      } catch (error, stackTrace) {
        _logger.logError(error, stackTrace);
      }
    }
  }

  Future<void> _dispatchBackgroundWork() {
    final List<Future> futures = [];
    for (WorkListener listener in _listeners) {
      futures.add(_tryBackgroundWork(listener));
    }
    return Future.wait(futures);
  }

  Future<void> _tryBackgroundWork(WorkListener listener) async {
    try {
      await listener.onBackgroundWork();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
