import 'package:school_pen/domain/work/work_manager.dart';

abstract class WorkListener {
  /// Called when the [WorkManager] does background work.
  Future<void> onBackgroundWork();
}
