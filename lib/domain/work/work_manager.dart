import 'package:school_pen/domain/work/work_listener.dart';

abstract class WorkManager {
  /// Must be called on the app startup.
  Future<void> initialize();

  void addWorkListener(WorkListener listener);

  void removeWorkListener(WorkListener listener);

  /// Called by repository to dispatch the background work.
  Future<void> onBackgroundWork();
}
