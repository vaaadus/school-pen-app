abstract class WorkRepository {
  Future<void> initialize(Duration frequency);
}
