import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/work/repository/work_repository.dart';
import 'package:school_pen/domain/work/work_manager.dart';
import 'package:school_pen/main_common.dart';
import 'package:workmanager/workmanager.dart';

const Logger _logger = Logger('AndroidWorkRepository');

class AndroidWorkRepository implements WorkRepository {
  static const String _periodicTaskName = 'backgroundPeriodicTask_3';
  static const String _keyScheduledTaskName = 'backgroundPeriodicTask_scheduledTaskName';
  final Preferences _preferences;

  AndroidWorkRepository(this._preferences);

  @override
  Future<void> initialize(Duration frequency) async {
    await Workmanager.initialize(_callbackDispatcher);

    if (_preferences.getString(_keyScheduledTaskName) != _periodicTaskName) {
      await Workmanager.cancelByUniqueName(_preferences.getString(_keyScheduledTaskName) ?? '');

      await Workmanager.registerPeriodicTask(
        _periodicTaskName,
        _periodicTaskName,
        initialDelay: Duration(seconds: 30),
        frequency: frequency,
        existingWorkPolicy: ExistingWorkPolicy.replace,
        constraints: Constraints(
          networkType: NetworkType.not_required,
          requiresBatteryNotLow: false,
          requiresCharging: false,
          requiresDeviceIdle: false,
          requiresStorageNotLow: false,
        ),
      );

      _preferences.setString(_keyScheduledTaskName, _periodicTaskName);
    }
  }
}

void _callbackDispatcher() {
  Workmanager.executeTask(_doBackgroundWork);
}

Future<bool> _doBackgroundWork(String taskName, Map<String, dynamic> inputData) async {
  try {
    await App.ensureInitialized();
    final WorkManager manager = Injection.get();
    await manager.onBackgroundWork();
    return true;
  } catch (error, stackTrace) {
    _logger.logError(error, stackTrace);
    return false;
  }
}
