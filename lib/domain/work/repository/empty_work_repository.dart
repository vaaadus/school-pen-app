import 'package:school_pen/domain/work/repository/work_repository.dart';

/// A no-op implementation of [WorkRepository].
class EmptyWorkRepository implements WorkRepository {
  @override
  Future<void> initialize(Duration frequency) async {
    // empty
  }
}
