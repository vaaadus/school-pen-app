import 'package:background_fetch/background_fetch.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/work/repository/work_repository.dart';
import 'package:school_pen/domain/work/work_manager.dart';
import 'package:school_pen/main_common.dart';

const Logger _logger = Logger('BackgroundWorkManager');

class IosWorkRepository implements WorkRepository {
  @override
  Future<void> initialize(Duration frequency) async {
    await BackgroundFetch.configure(
      BackgroundFetchConfig(
          minimumFetchInterval: frequency.inMinutes,
          stopOnTerminate: false,
          requiresBatteryNotLow: false,
          requiresCharging: false,
          requiresDeviceIdle: false,
          requiresStorageNotLow: false,
          requiredNetworkType: NetworkType.NONE),
      _backgroundFetchHeadlessTask,
    );
  }
}

void _backgroundFetchHeadlessTask(String taskId) async {
  await _doBackgroundWork();
  BackgroundFetch.finish(taskId);
}

Future<void> _doBackgroundWork() async {
  try {
    await App.ensureInitialized();
    final WorkManager manager = Injection.get();
    await manager.onBackgroundWork();
  } catch (error, stackTrace) {
    _logger.logError(error, stackTrace);
  }
}
