import 'package:school_pen/domain/exam/exam_manager.dart';

abstract class ExamListener {
  /// Called when any of exams changes.
  void onExamsChanged(ExamManager manager);
}
