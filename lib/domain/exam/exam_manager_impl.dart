import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/course/course_cleanup_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/exam/model/add_exam.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/exam/notification/exam_notification_builder.dart';
import 'package:school_pen/domain/exam/repository/exam_repository.dart';
import 'package:school_pen/domain/grade/grade_cleanup_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/semester/semester_cleanup_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:synchronized/synchronized.dart';

import 'exam_listener.dart';
import 'exam_manager.dart';

class ExamManagerImpl implements ExamManager, SemesterCleanupListener, CourseCleanupListener, GradeCleanupListener {
  static const Logger _logger = Logger('ExamManagerImpl');

  final Lock _lock = Lock();
  final Set<ExamListener> _listeners = {};
  final ExamRepository _repository;
  final NotificationManager _notifications;
  final ExamNotificationBuilder _notificationBuilder;
  final SemesterManager _semesterManager;
  final CourseManager _courseManager;
  final GradeManager _gradeManager;

  ExamManagerImpl(
    this._repository,
    this._notifications,
    this._notificationBuilder,
    this._semesterManager,
    this._courseManager,
    this._gradeManager,
  ) {
    _semesterManager.addSemesterCleanupListener(this);
    _courseManager.addCourseCleanupListener(this);
    _gradeManager.addGradeCleanupListener(this);

    _repository.onChange.listen((event) {
      _tryRecreateNotifications();
      _dispatchExamsChanged();
    });
  }

  @override
  void addExamListener(ExamListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onExamsChanged(this);
  }

  @override
  void removeExamListener(ExamListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<List<Exam>> getAll({String semesterId, String courseId}) async {
    final List<Exam> exams = await _repository.getAll(semesterId: semesterId, courseId: courseId);
    exams.sort();
    return exams;
  }

  @override
  Future<Exam> getById(String id) async {
    if (id == null) return null;
    return _repository.getById(id);
  }

  @override
  Future<String> create(AddExam exam) async {
    _logger.log('create, title: "${exam.title}"');

    return await _repository.create(exam.trim());
  }

  @override
  Future<void> update(String id, AddExam exam) async {
    _logger.log('update, id: $id');

    await _repository.update(id, exam.trim());
  }

  @override
  Future<void> delete(String id) async {
    _logger.log('delete, id: $id');

    await _repository.delete(id);
  }

  @override
  Future<void> onCleanupSemester(String semesterId) async {
    final List<Future> futures = [];
    for (Exam exam in await _repository.getAll(semesterId: semesterId)) {
      futures.add(delete(exam.id));
    }
    return Future.wait(futures);
  }

  @override
  Future<void> onCleanupCourse(String courseId) async {
    final List<Future> futures = [];
    for (Exam exam in await _repository.getAll(courseId: courseId)) {
      futures.add(delete(exam.id));
    }
    return Future.wait(futures);
  }

  @override
  Future<void> onCleanupGrade(String gradeId) async {
    final List<Future> futures = [];
    for (Exam exam in await _repository.getAll(gradeId: gradeId)) {
      final AddExam newExam = exam.toAddExam().copyWith(gradeId: Optional(null));
      futures.add(update(exam.id, newExam));
    }
    return Future.wait(futures);
  }

  void _tryRecreateNotifications() async {
    await _lock.synchronized(() async {
      try {
        await _recreateNotifications();
      } catch (error, stackTrace) {
        _logger.logError(error, stackTrace);
      }
    });
  }

  Future<void> _recreateNotifications() async {
    final List<Exam> exams = await _repository.getAll();
    final List<AddNotification> notifications = await Future.wait(exams.map(_asNotification));
    await _notifications.scheduleAllOnChannel(NotificationChannel.exam, notifications.withoutNulls().toList());
  }

  Future<AddNotification> _asNotification(Exam exam) async {
    if (exam.notification == null) return null;

    final Course course = await _courseManager.getById(exam.courseId);
    return _notificationBuilder.build(exam, course);
  }

  void _dispatchExamsChanged() {
    for (final listener in _listeners) {
      listener.onExamsChanged(this);
    }
  }
}
