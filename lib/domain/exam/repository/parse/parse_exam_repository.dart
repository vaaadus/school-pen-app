import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/exam/model/add_exam.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/exam/repository/exam_repository.dart';
import 'package:school_pen/domain/exam/repository/parse/parse_exam.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';

class ParseExamRepository implements ExamRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseExamRepository');
  final ExamRepository _delegate;
  Subscription _subscription;

  ParseExamRepository(this._delegate);

  @override
  Stream<ExamRepository> get onChange => _delegate.onChange;

  @override
  Future<List<Exam>> getAll({String semesterId, String courseId, String gradeId}) =>
      _delegate.getAll(semesterId: semesterId, courseId: courseId, gradeId: gradeId);

  @override
  Future<Exam> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddExam exam) async {
    final String id = await _delegate.create(exam);
    _try(() async {
      final Exam newExam = await _delegate.getById(id);
      await _storeOnParse(newExam);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddExam exam) async {
    final bool updated = await _delegate.update(id, exam);
    if (updated) {
      _try(() async {
        final Exam newExam = await _delegate.getById(id);
        await _storeOnParse(newExam);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(Exam exam) async {
    await _delegate.update(exam.id, exam.toAddExam());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseExam()));
    _subscription.on(LiveQueryEvent.create, (ParseExam object) {
      _try(() => _delegate.update(object.uuid, object.toAddExam()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseExam object) {
      _try(() => _delegate.update(object.uuid, object.toAddExam()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseExam object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(Exam exam) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseExam.fromModel(exam, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseExam().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
