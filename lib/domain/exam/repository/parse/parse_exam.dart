import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/notification_options.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/exam/model/add_exam.dart';
import 'package:school_pen/domain/exam/model/exam.dart';

class ParseExam extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Exam';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keySemesterId = 'semesterUuid';
  static const String keyCourseId = 'courseUuid';
  static const String keyGradeId = 'gradeUuid';
  static const String keyTitle = 'title';
  static const String keyNote = 'note';
  static const String keyDateTime = 'dateTime';
  static const String keyNotification = 'notification';
  static const String keyIsArchived = 'isArchived';

  ParseExam() : super(keyClassName);

  ParseExam.clone() : this();

  factory ParseExam.fromModel(Exam exam, ParseUser user) => ParseExam()
    ..user = user
    ..uuid = exam.id
    ..updatedAt = exam.updatedAt
    ..semesterId = exam.semesterId
    ..courseId = exam.courseId
    ..gradeId = exam.gradeId
    ..title = exam.title
    ..note = exam.note
    ..dateTime = exam.dateTime
    ..notification = exam.notification
    ..isArchived = exam.isArchived;

  AddExam toAddExam() => AddExam(
        semesterId: semesterId,
        courseId: courseId,
        gradeId: gradeId,
        title: title,
        note: note,
        dateTime: dateTime,
        notification: notification,
        isArchived: isArchived,
        updatedAt: updatedAt,
      );

  @override
  ParseExam clone(Map map) => ParseExam.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get semesterId => getNullable(keySemesterId);

  set semesterId(String id) => setNullable(keySemesterId, id);

  String get courseId => getNullable(keyCourseId);

  set courseId(String id) => setNullable(keyCourseId, id);

  String get gradeId => getNullable(keyGradeId);

  set gradeId(String id) => setNullable(keyGradeId, id);

  String get title => getNullable(keyTitle);

  set title(String title) => setNullable(keyTitle, title);

  String get note => getNullable(keyNote);

  set note(String note) => setNullable(keyNote, note);

  DateTime get dateTime => getDateTime(keyDateTime);

  set dateTime(DateTime dateTime) => setNullable(keyDateTime, dateTime);

  NotificationOptions get notification =>
      SembastSerializer.deserializeNotificationOptions(getNullable(keyNotification));

  set notification(NotificationOptions options) =>
      setNullable(keyNotification, SembastSerializer.serializeNotificationOptions(options));

  bool get isArchived => getNullable(keyIsArchived);

  set isArchived(bool value) => setNullable(keyIsArchived, value);

  /// Creates or updates an exam.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeExam').execute(parameters: {
      keyUuid: uuid,
      keySemesterId: semesterId,
      keyCourseId: courseId,
      keyGradeId: gradeId,
      keyTitle: title,
      keyNote: note,
      keyDateTime: dateTime?.toString(),
      keyNotification: SembastSerializer.serializeNotificationOptions(notification),
      keyIsArchived: isArchived,
    });
  }
}
