import 'package:school_pen/domain/exam/model/add_exam.dart';
import 'package:school_pen/domain/exam/model/exam.dart';

abstract class ExamRepository {
  /// Stream changes whenever exam repository changes.
  Stream<ExamRepository> get onChange;

  /// Returns all stored exams.
  /// Can be filtered by [semesterId], null means no filter.
  /// Can be filtered by [courseId], null means no filter.
  /// Can be filtered by [gradeId], null means no filter.
  Future<List<Exam>> getAll({String semesterId, String courseId, String gradeId});

  /// Returns the exam by ID or null if it doesn't exist.
  Future<Exam> getById(String id);

  /// Returns a list of ids of deleted exams.
  Future<List<String>> getDeletedIds();

  /// Creates a new exam and returns its ID.
  Future<String> create(AddExam exam);

  /// Updates or creates a new exam with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddExam exam);

  /// Removes a exam with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
