import 'package:school_pen/domain/exam/model/add_exam.dart';
import 'package:school_pen/domain/exam/model/exam.dart';

import 'exam_listener.dart';

/// Stores user exams and handles reminders.
abstract class ExamManager {
  void addExamListener(ExamListener listener, {bool notifyOnAttach = true});

  void removeExamListener(ExamListener listener);

  /// Returns a list of all created exams.
  /// Can be filtered by [semesterId], null means no filter.
  /// Can be filtered by [courseId], null means no filter.
  Future<List<Exam>> getAll({String semesterId, String courseId});

  /// Returns the exam by ID or null if it doesn't exist.
  Future<Exam> getById(String id);

  /// Creates a new exam and returns its ID.
  Future<String> create(AddExam exam);

  /// Creates or updates a new exam.
  Future<void> update(String id, AddExam exam);

  Future<void> delete(String id);
}
