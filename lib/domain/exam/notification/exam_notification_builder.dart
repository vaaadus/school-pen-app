import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';
import 'package:school_pen/generated/l10n.dart';

class ExamNotificationBuilder {
  AddNotification build(Exam exam, Course course) {
    assert(exam.notification != null);

    return AddNotification(
      title: _formatTitle(exam, course),
      body: _formatBody(exam),
      dateTime: exam.notification.getFireDateTime(exam.dateTime),
      channel: NotificationChannel.exam,
      payload: NotificationPayload(objectId: exam.id),
      when: exam.dateTime,
    );
  }

  String _formatTitle(Exam exam, Course course) {
    String title = S().common_exam;
    if (course != null) title += '/' + course.name;
    if (exam.title.isNotBlank) title += ': ${exam.title}';
    return title;
  }

  String _formatBody(Exam exam) {
    return exam.note ?? '';
  }
}
