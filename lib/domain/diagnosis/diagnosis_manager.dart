import 'package:school_pen/domain/diagnosis/diagnosis_consent_listener.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';

class DiagnosisManager {
  final Set<DiagnosisConsentListener> _listeners = {};
  final UserConfigRepository _userConfig;
  bool _diagnosisEnabled = true;

  DiagnosisManager(this._userConfig) {
    _init();
  }

  void _init() async {
    _diagnosisEnabled = await _userConfig.isDiagnosisEnabled() ?? true;

    _userConfig.onChange.listen((List<Property> properties) async {
      if (properties.contains(Property.diagnosisEnabled)) {
        _diagnosisEnabled = await _userConfig.isDiagnosisEnabled() ?? true;
        for (var listener in _listeners) listener.onDiagnosisConsentChanged(_diagnosisEnabled);
      }
    });
  }

  void addDiagnosisConsentListener(DiagnosisConsentListener listener) {
    _listeners.add(listener);
  }

  void removeDiagnosisConsentListener(DiagnosisConsentListener listener) {
    _listeners.remove(listener);
  }

  bool get isDiagnosisEnabled => _diagnosisEnabled;

  Future<void> setDiagnosisEnabled(bool enabled) async {
    await _userConfig.setDiagnosisEnabled(enabled);
  }
}
