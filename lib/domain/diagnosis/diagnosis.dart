import 'diagnosis_manager.dart';

/// Provides a static access to diagnosis.
class Diagnosis {
  static DiagnosisManager _implementation;

  static void setImplementation(DiagnosisManager manager) {
    _implementation = manager;
  }

  static bool get isDiagnosisEnabled {
    return _implementation?.isDiagnosisEnabled ?? false;
  }

  static Future<void> setDiagnosisEnabled(bool enabled) async {
    await _implementation?.setDiagnosisEnabled(enabled);
  }
}
