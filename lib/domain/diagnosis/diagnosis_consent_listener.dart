abstract class DiagnosisConsentListener {
  /// Called whenever the user consent for diagnosis changes.
  void onDiagnosisConsentChanged(bool enabled);
}
