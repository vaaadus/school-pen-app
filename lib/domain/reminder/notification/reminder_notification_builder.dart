import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/generated/l10n.dart';

class ReminderNotificationBuilder {
  AddNotification build(Reminder reminder, Course course) {
    return AddNotification(
      title: _formatTitle(reminder, course),
      body: _formatBody(reminder),
      dateTime: reminder.dateTime,
      channel: NotificationChannel.reminder,
      payload: NotificationPayload(objectId: reminder.id),
      when: reminder.dateTime,
    );
  }

  String _formatTitle(Reminder reminder, Course course) {
    String title = S().common_reminder;
    if (course != null) title += '/' + course.name;
    if (reminder.title.isNotBlank) title += ': ${reminder.title}';
    return title;
  }

  String _formatBody(Reminder reminder) {
    return reminder.note ?? '';
  }
}
