import 'reminder_manager.dart';

abstract class ReminderListener {
  /// Called when any of reminders changes.
  void onRemindersChanged(ReminderManager manager);
}
