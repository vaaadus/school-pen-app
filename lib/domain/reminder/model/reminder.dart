import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

import 'add_reminder.dart';

// TODO attachments

/// A model entity describing a created reminder.
///
/// Default sorting order: reminders by [dateTime] from older to newer.
class Reminder extends Equatable implements Comparable<Reminder> {
  final String id;
  final String semesterId;
  final String courseId;
  final String title;
  final String note;
  final DateTime dateTime;
  final bool isArchived;
  final DateTime updatedAt;

  const Reminder({
    @required this.id,
    @required this.semesterId,
    this.courseId,
    this.title,
    this.note,
    @required this.dateTime,
    @required this.isArchived,
    @required this.updatedAt,
  })  : assert(id != null),
        assert(semesterId != null),
        assert(dateTime != null),
        assert(isArchived != null),
        assert(updatedAt != null);

  AddReminder toAddReminder() {
    return AddReminder(
      semesterId: semesterId,
      courseId: courseId,
      title: title,
      note: note,
      dateTime: dateTime,
      isArchived: isArchived,
      updatedAt: updatedAt,
    );
  }

  /// Returns the summary of the reminder which contains the first non-empty text field from this reminder.
  String get headline {
    if (Strings.isNotBlank(title)) return title;
    if (Strings.isNotBlank(note)) return note;
    return '';
  }

  /// Whether the notification date is in the past.
  /// False when not set.
  bool isNotificationExpired(DateTime now) {
    return dateTime.isBefore(now);
  }

  @override
  List<Object> get props => [id, semesterId, courseId, title, note, dateTime, isArchived, updatedAt];

  @override
  int compareTo(Reminder other) => dateTime.compareTo(other.dateTime);
}
