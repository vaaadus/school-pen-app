import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';

/// A model entity describing a request to add a reminder.
class AddReminder extends Equatable {
  final String semesterId;
  final String courseId;
  final String title;
  final String note;
  final DateTime dateTime;
  final bool isArchived;
  final DateTime updatedAt;

  const AddReminder({
    @required this.semesterId,
    this.courseId,
    this.title,
    this.note,
    @required this.dateTime,
    @required this.isArchived,
    @required this.updatedAt,
  })  : assert(semesterId != null),
        assert(dateTime != null),
        assert(isArchived != null),
        assert(updatedAt != null);

  Reminder toReminder({@required String id}) {
    return Reminder(
      id: id,
      semesterId: semesterId,
      courseId: courseId,
      title: title,
      note: note,
      dateTime: dateTime,
      isArchived: isArchived,
      updatedAt: updatedAt,
    );
  }

  /// Removes blank fields.
  AddReminder trim() {
    return copyWith(
      title: Optional(title?.trim()),
      note: Optional(note?.trim()),
    );
  }

  AddReminder copyWith({
    Optional<String> semesterId,
    Optional<String> courseId,
    Optional<String> title,
    Optional<String> note,
    Optional<DateTime> dateTime,
    Optional<bool> isArchived,
    Optional<DateTime> updatedAt,
  }) {
    return AddReminder(
      semesterId: Optional.unwrapOrElse(semesterId, this.semesterId),
      courseId: Optional.unwrapOrElse(courseId, this.courseId),
      title: Optional.unwrapOrElse(title, this.title),
      note: Optional.unwrapOrElse(note, this.note),
      dateTime: Optional.unwrapOrElse(dateTime, this.dateTime),
      isArchived: Optional.unwrapOrElse(isArchived, this.isArchived),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  /// Whether the notification date is in the past.
  /// False when not set.
  bool isNotificationExpired(DateTime now) {
    return toReminder(id: '').isNotificationExpired(now);
  }

  @override
  List<Object> get props => [semesterId, courseId, title, note, dateTime, isArchived, updatedAt];
}
