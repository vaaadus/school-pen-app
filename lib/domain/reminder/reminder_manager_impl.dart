import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/course/course_cleanup_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/reminder/model/add_reminder.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/reminder/notification/reminder_notification_builder.dart';
import 'package:school_pen/domain/reminder/repository/reminder_repository.dart';
import 'package:school_pen/domain/semester/semester_cleanup_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:synchronized/synchronized.dart';

import 'reminder_listener.dart';
import 'reminder_manager.dart';

class ReminderManagerImpl implements ReminderManager, SemesterCleanupListener, CourseCleanupListener {
  static const Logger _logger = Logger('ReminderManagerImpl');

  final Lock _lock = Lock();
  final Set<ReminderListener> _listeners = {};
  final ReminderRepository _repository;
  final NotificationManager _notifications;
  final ReminderNotificationBuilder _notificationBuilder;
  final SemesterManager _semesterManager;
  final CourseManager _courseManager;

  ReminderManagerImpl(
    this._repository,
    this._notifications,
    this._notificationBuilder,
    this._semesterManager,
    this._courseManager,
  ) {
    _semesterManager.addSemesterCleanupListener(this);
    _courseManager.addCourseCleanupListener(this);

    _repository.onChange.listen((event) {
      _tryRecreateNotifications();
      _dispatchRemindersChanged();
    });
  }

  @override
  void addReminderListener(ReminderListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onRemindersChanged(this);
  }

  @override
  void removeReminderListener(ReminderListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<List<Reminder>> getAll({String semesterId, String courseId}) async {
    final List<Reminder> reminders = await _repository.getAll(semesterId: semesterId, courseId: courseId);
    reminders.sort();
    return reminders;
  }

  @override
  Future<Reminder> getById(String id) async {
    if (id == null) return null;
    return _repository.getById(id);
  }

  @override
  Future<String> create(AddReminder reminder) async {
    _logger.log('create, title: "${reminder.title}"');

    return await _repository.create(reminder.trim());
  }

  @override
  Future<void> update(String id, AddReminder reminder) async {
    _logger.log('update, id: $id');

    await _repository.update(id, reminder.trim());
  }

  @override
  Future<void> delete(String id) async {
    _logger.log('delete, id: $id');

    await _repository.delete(id);
  }

  @override
  Future<void> onCleanupSemester(String semesterId) async {
    final List<Future> futures = [];
    for (Reminder reminder in await _repository.getAll(semesterId: semesterId)) {
      futures.add(delete(reminder.id));
    }
    return Future.wait(futures);
  }

  @override
  Future<void> onCleanupCourse(String courseId) async {
    final List<Future> futures = [];
    for (Reminder reminder in await _repository.getAll(courseId: courseId)) {
      futures.add(delete(reminder.id));
    }
    return Future.wait(futures);
  }

  void _tryRecreateNotifications() async {
    await _lock.synchronized(() async {
      try {
        await _recreateNotifications();
      } catch (error, stackTrace) {
        _logger.logError(error, stackTrace);
      }
    });
  }

  Future<void> _recreateNotifications() async {
    final List<Reminder> reminder = await _repository.getAll();
    final List<AddNotification> notifications = await Future.wait(reminder.map(_asNotification));
    await _notifications.scheduleAllOnChannel(NotificationChannel.reminder, notifications.withoutNulls().toList());
  }

  Future<AddNotification> _asNotification(Reminder reminder) async {
    final Course course = await _courseManager.getById(reminder.courseId);
    return _notificationBuilder.build(reminder, course);
  }

  void _dispatchRemindersChanged() {
    for (final listener in _listeners) {
      listener.onRemindersChanged(this);
    }
  }
}
