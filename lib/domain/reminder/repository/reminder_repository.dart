import 'package:school_pen/domain/reminder/model/add_reminder.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';

abstract class ReminderRepository {
  /// Stream changes whenever reminder repository changes.
  Stream<ReminderRepository> get onChange;

  /// Returns all stored reminder.
  /// Can be filtered by [semesterId], null means no filter.
  /// Can be filtered by [courseId], null means no filter.
  Future<List<Reminder>> getAll({String semesterId, String courseId});

  /// Returns the reminder by ID or null if it doesn't exist.
  Future<Reminder> getById(String id);

  /// Returns a list of ids of deleted reminder.
  Future<List<String>> getDeletedIds();

  /// Creates a new reminder and returns its ID.
  Future<String> create(AddReminder reminder);

  /// Updates or creates a new reminder with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddReminder reminder);

  /// Removes a reminder with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
