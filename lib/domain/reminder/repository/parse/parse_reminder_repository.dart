import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/reminder/model/add_reminder.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/reminder/repository/parse/parse_reminder.dart';
import 'package:school_pen/domain/reminder/repository/reminder_repository.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';

class ParseReminderRepository implements ReminderRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseReminderRepository');
  final ReminderRepository _delegate;
  Subscription _subscription;

  ParseReminderRepository(this._delegate);

  @override
  Stream<ReminderRepository> get onChange => _delegate.onChange;

  @override
  Future<List<Reminder>> getAll({String semesterId, String courseId}) =>
      _delegate.getAll(semesterId: semesterId, courseId: courseId);

  @override
  Future<Reminder> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddReminder reminder) async {
    final String id = await _delegate.create(reminder);
    _try(() async {
      final Reminder newReminder = await _delegate.getById(id);
      await _storeOnParse(newReminder);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddReminder reminder) async {
    final bool updated = await _delegate.update(id, reminder);
    if (updated) {
      _try(() async {
        final Reminder newReminder = await _delegate.getById(id);
        await _storeOnParse(newReminder);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(Reminder reminder) async {
    await _delegate.update(reminder.id, reminder.toAddReminder());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseReminder()));
    _subscription.on(LiveQueryEvent.create, (ParseReminder object) {
      _try(() => _delegate.update(object.uuid, object.toAddReminder()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseReminder object) {
      _try(() => _delegate.update(object.uuid, object.toAddReminder()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseReminder object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(Reminder reminder) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseReminder.fromModel(reminder, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseReminder().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
