import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/reminder/model/add_reminder.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';

class ParseReminder extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Reminder';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keySemesterId = 'semesterUuid';
  static const String keyCourseId = 'courseUuid';
  static const String keyTitle = 'title';
  static const String keyNote = 'note';
  static const String keyDateTime = 'dateTime';
  static const String keyIsArchived = 'isArchived';

  ParseReminder() : super(keyClassName);

  ParseReminder.clone() : this();

  factory ParseReminder.fromModel(Reminder reminder, ParseUser user) => ParseReminder()
    ..user = user
    ..uuid = reminder.id
    ..updatedAt = reminder.updatedAt
    ..semesterId = reminder.semesterId
    ..courseId = reminder.courseId
    ..title = reminder.title
    ..note = reminder.note
    ..dateTime = reminder.dateTime
    ..isArchived = reminder.isArchived;

  AddReminder toAddReminder() => AddReminder(
        semesterId: semesterId,
        courseId: courseId,
        title: title,
        note: note,
        dateTime: dateTime,
        isArchived: isArchived,
        updatedAt: updatedAt,
      );

  @override
  ParseReminder clone(Map map) => ParseReminder.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get semesterId => getNullable(keySemesterId);

  set semesterId(String id) => setNullable(keySemesterId, id);

  String get courseId => getNullable(keyCourseId);

  set courseId(String id) => setNullable(keyCourseId, id);

  String get title => getNullable(keyTitle);

  set title(String title) => setNullable(keyTitle, title);

  String get note => getNullable(keyNote);

  set note(String note) => setNullable(keyNote, note);

  DateTime get dateTime => getDateTime(keyDateTime);

  set dateTime(DateTime dateTime) => setNullable(keyDateTime, dateTime);

  bool get isArchived => getNullable(keyIsArchived);

  set isArchived(bool value) => setNullable(keyIsArchived, value);

  /// Creates or updates a reminder.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeReminder').execute(parameters: {
      keyUuid: uuid,
      keySemesterId: semesterId,
      keyCourseId: courseId,
      keyTitle: title,
      keyNote: note,
      keyDateTime: dateTime?.toString(),
      keyIsArchived: isArchived,
    });
  }
}
