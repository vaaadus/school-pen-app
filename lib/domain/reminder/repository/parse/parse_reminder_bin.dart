import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';

class ParseReminderBin extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Reminder_Bin';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';

  ParseReminderBin() : super(keyClassName);

  ParseReminderBin.clone() : this();

  @override
  ParseReminderBin clone(Map map) => ParseReminderBin.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);
}
