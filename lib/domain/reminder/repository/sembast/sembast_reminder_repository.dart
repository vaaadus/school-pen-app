import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/reminder/model/add_reminder.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/reminder/repository/reminder_repository.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// A [ReminderRepository] that saves reminders in sembast document database.
class SembastReminderRepository implements ReminderRepository {
  static const String keyDeletedIds = 'key_sembast_deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('reminders');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('reminders_bin');
  final BehaviorSubject<ReminderRepository> _onChange = BehaviorSubject();
  final Lock _lock = Lock();

  SembastReminderRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<ReminderRepository> get onChange => _onChange;

  @override
  Future<List<Reminder>> getAll({String semesterId, String courseId}) async {
    final List<Filter> filters = [
      if (semesterId != null) Filter.equals('semesterId', semesterId),
      if (courseId != null) Filter.equals('courseId', courseId),
    ];

    final Finder finder = filters.isEmpty ? null : Finder(filter: Filter.and(filters));
    final List<RecordSnapshot> snapshots = await _store.find(await _db, finder: finder);
    return snapshots.map((e) => _deserializeReminder(e.value).toReminder(id: e.key)).toList();
  }

  @override
  Future<Reminder> getById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> snapshot = await _store.record(id).get(await _db);
    return _deserializeReminder(snapshot)?.toReminder(id: id);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(keyDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<String> create(AddReminder reminder) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = _serializeReminder(reminder);
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddReminder reminder) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = _serializeReminder(reminder);
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = List.of(await getDeletedIds());
      if (ids.addIfAbsent(id)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      return deleted;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}

Map<String, dynamic> _serializeReminder(AddReminder reminder) {
  if (reminder == null) return null;

  return {
    'semesterId': reminder.semesterId,
    'courseId': reminder.courseId,
    'title': reminder.title,
    'note': reminder.note,
    'dateTime': SembastSerializer.serializeDateTimeField(reminder.dateTime),
    'isArchived': reminder.isArchived,
    'updatedAt': SembastSerializer.serializeDateTimeField(reminder.updatedAt),
  };
}

AddReminder _deserializeReminder(Map<String, dynamic> map) {
  if (map == null) return null;

  return AddReminder(
    semesterId: map['semesterId'],
    courseId: map['courseId'],
    title: map['title'],
    note: map['note'],
    dateTime: SembastSerializer.deserializeDateTimeField(map['dateTime']),
    isArchived: map['isArchived'],
    updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
  );
}
