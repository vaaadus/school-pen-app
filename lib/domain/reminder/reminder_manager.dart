import 'package:school_pen/domain/reminder/model/add_reminder.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';

import 'reminder_listener.dart';

/// Stores user reminders.
abstract class ReminderManager {
  void addReminderListener(ReminderListener listener, {bool notifyOnAttach = true});

  void removeReminderListener(ReminderListener listener);

  /// Returns a list of all created reminders.
  /// Can be filtered by [semesterId], null means no filter.
  /// Can be filtered by [courseId], null means no filter.
  Future<List<Reminder>> getAll({String semesterId, String courseId});

  /// Returns the reminder by ID or null if it doesn't exist.
  Future<Reminder> getById(String id);

  /// Creates a new reminder and returns its ID.
  Future<String> create(AddReminder reminder);

  /// Creates or updates a new reminder.
  Future<void> update(String id, AddReminder reminder);

  Future<void> delete(String id);
}
