import 'model/quick_actions.dart';

abstract class QuickActionsListener {
  /// Called whenever a user invokes a [QuickAction].
  void onQuickAction(QuickAction action);
}
