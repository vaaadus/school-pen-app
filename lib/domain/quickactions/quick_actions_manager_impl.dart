import 'dart:async';

import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/preferences/preferences.dart';

import 'model/quick_actions.dart';
import 'model/quick_actions_instance.dart';
import 'quick_actions_listener.dart';
import 'quick_actions_manager.dart';
import 'repository/quick_actions_history.dart';
import 'repository/quick_actions_repository.dart';

class QuickActionsManagerImpl implements QuickActionsManager, LifecycleListener {
  /// Flutter SDK does not notify when the app has initialized,
  /// therefore we need to do delay our logic for some time.
  static const Duration refreshDelay = Duration(seconds: 5);

  static const String keyLastActionId = 'key_quickAction_lastId';
  static const Logger _logger = Logger('QuickActionsManagerImpl');

  final Set<QuickActionsListener> _listeners = {};
  final LifecycleManager _lifecycle;
  final QuickActionsRepository _repository;
  final QuickActionsHistory _history;
  final Preferences _preferences;

  Timer _refreshTimer;
  StreamSubscription _subscription;

  QuickActionsManagerImpl(
    this._lifecycle,
    this._repository,
    this._history,
    this._preferences,
  ) {
    _lifecycle.addLifecycleListener(this);
  }

  @override
  void addQuickActionsListener(QuickActionsListener listener) {
    _listeners.add(listener);
  }

  @override
  void removeQuickActionsListener(QuickActionsListener listener) {
    _listeners.remove(listener);
  }

  @override
  void onResume() {
    _refreshTimer?.cancel();
    _refreshTimer = Timer(refreshDelay, _refreshQuickActionsAfterDelay);

    _subscription?.cancel();
    _subscription = _repository.actions.listen(handleQuickActionInstance);
  }

  @override
  void onPause() {
    _refreshTimer?.cancel();
    _refreshTimer = null;

    _subscription?.cancel();
    _subscription = null;
  }

  Future<void> handleQuickActionInstance(QuickActionInstance action) async {
    _refreshQuickActionsAfterDelay();
    await _dispatchQuickActionIfNotConsumed(action);
  }

  void _refreshQuickActionsAfterDelay() {
    _refreshTimer?.cancel();
    _refreshTimer = Timer(refreshDelay, _refreshQuickActions);
  }

  void _refreshQuickActions() async {
    try {
      await _repository.setQuickActions(_createQuickActions());
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  List<QuickActionInstance> _createQuickActions() {
    return QuickAction.values.map((e) => _createQuickAction(e)).toList();
  }

  QuickActionInstance _createQuickAction(QuickAction action) {
    return QuickActionInstance(action, _incrementAndGetId().toString());
  }

  int _incrementAndGetId() {
    final int current = _preferences.getInt(keyLastActionId) ?? 0;
    final int next = current + 1;
    _preferences.setInt(keyLastActionId, next);
    return next;
  }

  Future<void> _dispatchQuickActionIfNotConsumed(QuickActionInstance action) async {
    try {
      final bool consumed = await _history.consumeQuickAction(action.id);
      if (!consumed) return;

      _dispatchQuickAction(action.type);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  void _dispatchQuickAction(QuickAction action) {
    for (QuickActionsListener listener in _listeners) {
      listener.onQuickAction(action);
    }
  }
}
