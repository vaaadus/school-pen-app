import 'package:school_pen/domain/quickactions/quick_actions_listener.dart';
import 'package:school_pen/domain/quickactions/quick_actions_manager.dart';

/// No-op implementation.
class EmptyQuickActionsManager implements QuickActionsManager {
  @override
  void addQuickActionsListener(QuickActionsListener listener) {}

  @override
  void removeQuickActionsListener(QuickActionsListener listener) {}
}
