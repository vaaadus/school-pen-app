/// Lists all supported quick actions.
enum QuickAction {
  addReminder,
  addHomework,
  addExam,
  addNote,
}

extension QuickActionExt on QuickAction {
  /// Returns the technical tag for the quick action.
  String get tag {
    switch (this) {
      case QuickAction.addReminder:
        return 'addReminder';
      case QuickAction.addHomework:
        return 'addHomework';
      case QuickAction.addExam:
        return 'addExam';
      case QuickAction.addNote:
        return 'addNote';
    }
    throw ArgumentError('Unsupported quick action: $this');
  }
}
