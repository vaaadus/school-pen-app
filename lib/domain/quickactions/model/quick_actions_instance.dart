import 'package:equatable/equatable.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/quickactions/model/quick_actions.dart';

/// Wraps a [QuickAction] with and id.
class QuickActionInstance extends Equatable {
  static const String idSeparator = '==';

  final QuickAction type;
  final String id;

  const QuickActionInstance(this.type, this.id);

  /// Returns the technical tag consisting from action tag and id.
  String get tag {
    return type.tag + idSeparator + id;
  }

  /// Parses the [QuickActionInstance] from a [tag].
  static QuickActionInstance forTag(String tag) {
    if (Strings.isBlank(tag)) return null;

    for (QuickAction action in QuickAction.values) {
      if (tag.startsWith(RegExp(action.tag + idSeparator))) {
        final int index = action.tag.length + idSeparator.length;
        final String id = tag.substring(index);
        if (id.isNotBlank) {
          return QuickActionInstance(action, id);
        }
      }
    }

    return null;
  }

  @override
  List<Object> get props => [type, id];
}
