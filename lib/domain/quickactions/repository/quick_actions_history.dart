import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// Keeps track of already consumed actions
/// to avoid consuming the same quick action twice.
class QuickActionsHistory {
  static final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('quick_actions_history');
  final Lock _lock = Lock();

  /// Returns true if an action with [id] was never consumed, false otherwise.
  /// If the result was true, next calls will return false given the same [id].
  Future<bool> consumeQuickAction(String id) async {
    if (Strings.isBlank(id)) return false;

    return _lock.synchronized(() async {
      final bool consumed = SembastSerializer.deserializeBool(await _store.record(id).get(await _db)) ?? false;
      if (!consumed) await _store.record(id).put(await _db, SembastSerializer.serializeBool(true));

      return !consumed;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}
