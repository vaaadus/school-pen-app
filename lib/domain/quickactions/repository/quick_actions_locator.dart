import 'package:school_pen/generated/l10n.dart';

import '../model/quick_actions.dart';

/// Provides resources for quick actions.
class QuickActionsLocator {
  String getTitle(QuickAction action) {
    switch (action) {
      case QuickAction.addReminder:
        return S().common_addReminder;
      case QuickAction.addHomework:
        return S().common_addHomework;
      case QuickAction.addExam:
        return S().common_addExam;
      case QuickAction.addNote:
        return S().common_addNote;
    }
    throw ArgumentError('Unsupported quick action: $action');
  }

  String getIcon(QuickAction action) {
    switch (action) {
      case QuickAction.addReminder:
        return 'ic_add_reminder';
      case QuickAction.addHomework:
        return 'ic_add_homework';
      case QuickAction.addExam:
        return 'ic_add_exam';
      case QuickAction.addNote:
        return 'ic_add_note';
    }
    throw ArgumentError('Unsupported quick action: $action');
  }
}
