import 'package:quick_actions/quick_actions.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/quickactions/model/quick_actions_instance.dart';
import 'package:school_pen/domain/quickactions/repository/quick_actions_locator.dart';

/// Abstracts from concrete implementation of quick actions plugin.
class QuickActionsRepository {
  static const Logger _logger = Logger('QuickActionsRepository');

  final BehaviorSubject<QuickActionInstance> _actions = BehaviorSubject();
  final QuickActions _plugin = QuickActions();
  final QuickActionsLocator _locator;

  QuickActionsRepository(this._locator) {
    _plugin.initialize((String tag) {
      QuickActionInstance action = QuickActionInstance.forTag(tag);
      _logger.log('initialize, raw tag: $tag, action: $action');

      if (action != null) {
        _actions.add(action);
      }
    });
  }

  /// The stream with actions invoked by the user.
  Stream<QuickActionInstance> get actions => _actions.stream;

  Future<void> setQuickActions(List<QuickActionInstance> actions) {
    return _plugin.setShortcutItems(_createShortcutItems(actions));
  }

  List<ShortcutItem> _createShortcutItems(List<QuickActionInstance> actions) {
    return actions.map((e) => _createShortcutItem(e)).toList().reversed.toList();
  }

  ShortcutItem _createShortcutItem(QuickActionInstance action) {
    return ShortcutItem(
      type: action.tag,
      localizedTitle: _locator.getTitle(action.type),
      icon: _locator.getIcon(action.type),
    );
  }
}
