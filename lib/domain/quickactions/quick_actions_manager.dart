import 'model/quick_actions.dart';
import 'quick_actions_listener.dart';

/// Manages [QuickAction]s. Allows to listen for the invocation.
abstract class QuickActionsManager {
  void addQuickActionsListener(QuickActionsListener listener);

  void removeQuickActionsListener(QuickActionsListener listener);
}
