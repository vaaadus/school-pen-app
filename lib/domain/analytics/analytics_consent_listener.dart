abstract class AnalyticsConsentListener {
  /// Called whenever the user consent for analytics changes.
  void onAnalyticsConsentChanged(bool enabled);
}
