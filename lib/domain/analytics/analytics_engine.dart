import 'analytics_event.dart';

/// Abstract from exact implementation of analytics.
abstract class AnalyticsEngine {
  void setAnalyticsEnabled(bool enabled);

  void logEvent(AnalyticsEvent event);

  void logAppActivated();

  void logAppDeactivated();

  void setScreenName(String name);
}
