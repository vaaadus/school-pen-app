import 'package:school_pen/domain/analytics/analytics_consent_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';

import 'analytics_engine.dart';
import 'analytics_event.dart';

class AnalyticsManager implements LifecycleListener {
  final UserConfigRepository _userConfig;
  final LifecycleManager _lifecycleManager;
  final List<AnalyticsEngine> _engines;
  final Set<AnalyticsConsentListener> _listeners = {};
  bool _analyticsEnabled = true;

  AnalyticsManager(this._userConfig, this._engines, this._lifecycleManager) {
    _init();
  }

  void _init() async {
    _userConfig.onChange.listen((List<Property> properties) async {
      if (properties.contains(Property.analyticsEnabled)) {
        _analyticsEnabled = await _userConfig.isAnalyticsEnabled() ?? true;
        for (var engine in _engines) engine.setAnalyticsEnabled(_analyticsEnabled);
        for (var listener in _listeners) listener.onAnalyticsConsentChanged(_analyticsEnabled);
      }
    });

    _analyticsEnabled = await _userConfig.isAnalyticsEnabled() ?? true;
    for (var engine in _engines) {
      engine.setAnalyticsEnabled(_analyticsEnabled);
    }

    _lifecycleManager.addLifecycleListener(this);
  }

  void addAnalyticsConsentListener(AnalyticsConsentListener listener) {
    _listeners.add(listener);
  }

  void removeAnalyticsConsentListener(AnalyticsConsentListener listener) {
    _listeners.remove(listener);
  }

  void logEvent(AnalyticsEvent event) {
    for (var engine in _engines) {
      engine.logEvent(event);
    }
  }

  void setScreenName(String name) {
    for (var engine in _engines) {
      engine.setScreenName(name);
    }
  }

  bool get isAnalyticsEnabled => _analyticsEnabled;

  Future<void> setAnalyticsEnabled(bool enabled) async {
    await _userConfig.setAnalyticsEnabled(enabled);
  }

  @override
  void onResume() {
    for (var engine in _engines) {
      engine.logAppActivated();
    }
  }

  @override
  void onPause() {
    for (var engine in _engines) {
      engine.logAppDeactivated();
    }
  }
}
