import 'package:equatable/equatable.dart';

/// List all analytics events.
class AnalyticsEvent extends Equatable {
  final String name;
  final Map<String, dynamic> parameters;

  const AnalyticsEvent(this.name, {this.parameters});

  // school wizard
  static const schoolWizardStart = AnalyticsEvent('school_wizard_start');
  static const schoolWizardSchoolNotFound = AnalyticsEvent('school_wizard_school_not_found');

  // shortcuts
  static const dashboardOpenSettings = AnalyticsEvent('dashboard_open_settings');
  static const dashboardOpenTimetable = AnalyticsEvent('dashboard_open_timetable');
  static const dashboardOpenHomework = AnalyticsEvent('dashboard_open_homework');
  static const dashboardOpenGrades = AnalyticsEvent('dashboard_open_grades');
  static const dashboardOpenNews = AnalyticsEvent('dashboard_open_news');

  // promo cards
  static const dashboardPendingBilling = AnalyticsEvent('dashboard_pending_billing');
  static const dashboardPostponeBilling = AnalyticsEvent('dashboard_postpone_billing');
  static const dashboardLoginOffer = AnalyticsEvent('dashboard_login_offer');
  static const dashboardPostponeLogin = AnalyticsEvent('dashboard_postpone_login');
  static const dashboardPremiumOffer = AnalyticsEvent('dashboard_premium_offer');
  static const dashboardPostponePremium = AnalyticsEvent('dashboard_postpone_premium');
  static const dashboardRateApp = AnalyticsEvent('dashboard_rate_app');
  static const dashboardPostponeRating = AnalyticsEvent('dashboard_postpone_rating');
  static const dashboardShareApp = AnalyticsEvent('dashboard_share_app');
  static const dashboardPostponeSharing = AnalyticsEvent('dashboard_postpone_sharing');
  static const dashboardHomework = AnalyticsEvent('dashboard_homework');

  // homework
  static const homeworkAdd = AnalyticsEvent('homework_add');
  static const homeworkEdit = AnalyticsEvent('homework_edit');

  // settings
  static const settingsGoPremium = AnalyticsEvent('settings_go_premium');
  static const settingsRateApp = AnalyticsEvent('settings_rate_app');
  static const settingsShareApp = AnalyticsEvent('settings_share_app');
  static const settingsContactUs = AnalyticsEvent('settings_contact_us');

  @override
  List<Object> get props => [name, parameters];
}
