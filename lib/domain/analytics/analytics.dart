import 'analytics_event.dart';
import 'analytics_manager.dart';

/// Provides a static access to analytics.
class Analytics {
  static AnalyticsManager _implementation;

  static void setImplementation(AnalyticsManager manager) {
    _implementation = manager;
  }

  static void logEvent(AnalyticsEvent event) {
    _implementation?.logEvent(event);
  }

  static void setScreenName(String name) {
    _implementation?.setScreenName(name);
  }

  static bool get isAnalyticsEnabled {
    return _implementation?.isAnalyticsEnabled ?? false;
  }

  static Future<void> setAnalyticsEnabled(bool enabled) async {
    await _implementation?.setAnalyticsEnabled(enabled);
  }
}
