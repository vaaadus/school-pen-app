import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:school_pen/domain/analytics/analytics_engine.dart';
import 'package:school_pen/domain/analytics/analytics_event.dart';

class FacebookAnalyticsEngine implements AnalyticsEngine {
  final FacebookAppEvents _analytics;
  bool _analyticsEnabled = true;

  FacebookAnalyticsEngine(this._analytics);

  @override
  void logEvent(AnalyticsEvent event) {
    if (_analyticsEnabled) {
      _analytics.logEvent(name: event.name, parameters: event.parameters);
    }
  }

  @override
  void setAnalyticsEnabled(bool enabled) {
    _analyticsEnabled = enabled;
    _analytics.setAutoLogAppEventsEnabled(enabled);

    if (!enabled) {
      _analytics.clearUserID();
      _analytics.clearUserData();
    }
  }

  @override
  void logAppActivated() {
    if (_analyticsEnabled) {
      _analytics.logActivatedApp();
    }
  }

  @override
  void logAppDeactivated() {
    if (_analyticsEnabled) {
      _analytics.logDeactivatedApp();
    }
  }

  @override
  void setScreenName(String name) {
    // not supported
  }
}
