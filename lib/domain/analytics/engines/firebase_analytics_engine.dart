import 'package:firebase_analytics/firebase_analytics.dart';

import '../analytics_engine.dart';
import '../analytics_event.dart';

class FirebaseAnalyticsEngine implements AnalyticsEngine {
  final FirebaseAnalytics _analytics;
  bool _analyticsEnabled = true;

  FirebaseAnalyticsEngine(this._analytics);

  @override
  void logEvent(AnalyticsEvent event) {
    if (_analyticsEnabled) {
      _analytics.logEvent(name: event.name, parameters: event.parameters);
    }
  }

  @override
  void setAnalyticsEnabled(bool enabled) {
    _analyticsEnabled = enabled;
    _analytics.setAnalyticsCollectionEnabled(enabled);
    if (!enabled) {
      _analytics.resetAnalyticsData();
    }
  }

  @override
  void logAppActivated() {
    // not supported
  }

  @override
  void logAppDeactivated() {
    // not supported
  }

  @override
  void setScreenName(String name) {
    if (_analyticsEnabled) {
      _analytics.setCurrentScreen(screenName: name);
    }
  }
}
