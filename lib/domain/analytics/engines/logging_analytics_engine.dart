import 'package:school_pen/domain/analytics/analytics_event.dart';
import 'package:school_pen/domain/logger/logger.dart';

import '../analytics_engine.dart';

class LoggingAnalyticsEngine implements AnalyticsEngine {
  static const Logger _logger = Logger('LoggingAnalyticsEngine');
  bool _analyticsEnabled = true;

  @override
  void logEvent(AnalyticsEvent event) {
    if (_analyticsEnabled) {
      var message = 'logEvent: name=${event.name}';
      if (event.parameters != null) {
        message += ', parameters=${event.parameters}';
      }
      _logger.log(message);
    }
  }

  @override
  void setAnalyticsEnabled(bool enabled) {
    _analyticsEnabled = enabled;
  }

  @override
  void logAppActivated() {
    if (_analyticsEnabled) {
      _logger.log('logAppActivated');
    }
  }

  @override
  void logAppDeactivated() {
    if (_analyticsEnabled) {
      _logger.log('logAppDeactivated');
    }
  }

  @override
  void setScreenName(String name) {
    if (_analyticsEnabled) {
      _logger.log('setScreenName: $name');
    }
  }
}
