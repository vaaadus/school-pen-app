import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/note/model/add_note.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/model/note_item.dart';
import 'package:school_pen/domain/note/repository/note_repository.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// A [NoteRepository] that saves notes in sembast document database.
class SembastNoteRepository implements NoteRepository {
  static const String keyDeletedIds = 'key_sembast_deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('notes');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('notes_bin');
  final BehaviorSubject<NoteRepository> _onChange = BehaviorSubject();
  final Lock _lock = Lock();

  SembastNoteRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<NoteRepository> get onChange => _onChange;

  @override
  Future<List<Note>> getAll([String schoolYearId]) async {
    final Finder finder = schoolYearId == null ? null : Finder(filter: Filter.equals('schoolYearId', schoolYearId));
    final List<RecordSnapshot> snapshots = await _store.find(await _db, finder: finder);
    return snapshots.map((e) => _deserializeNote(e.value).toNote(id: e.key)).toList();
  }

  @override
  Future<Note> getById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> snapshot = await _store.record(id).get(await _db);
    return _deserializeNote(snapshot)?.toNote(id: id);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(keyDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<String> create(AddNote note) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = _serializeNote(note);
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddNote note) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = _serializeNote(note);
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = List.of(await getDeletedIds());
      if (ids.addIfAbsent(id)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      return deleted;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}

Map<String, dynamic> _serializeNote(AddNote note) {
  if (note == null) return null;

  return {
    'schoolYearId': note.schoolYearId,
    'title': note.title,
    'note': note.note,
    'items': _serializeNoteItems(note.items),
    if (note.notificationDate != null)
      'notificationDate': SembastSerializer.serializeDateTimeField(note.notificationDate),
    if (note.color != null) 'color': note.color.tag,
    'isArchived': note.isArchived,
    'updatedAt': SembastSerializer.serializeDateTimeField(note.updatedAt),
  };
}

AddNote _deserializeNote(Map<String, dynamic> map) {
  if (map == null) return null;

  return AddNote(
    schoolYearId: map['schoolYearId'],
    title: map['title'],
    note: map['note'],
    items: _deserializeNoteItems(map['items']),
    notificationDate: SembastSerializer.deserializeDateTimeField(map['notificationDate']),
    color: ColorEnumExt.forTag(map['color']),
    isArchived: map['isArchived'],
    updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
  );
}

List<dynamic> _serializeNoteItems(List<NoteItem> items) {
  return items.map((e) => e.toJson()).toList();
}

List<NoteItem> _deserializeNoteItems(List<dynamic> list) {
  return list.map((e) => NoteItem.fromJson(e)).toList();
}
