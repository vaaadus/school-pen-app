import 'package:school_pen/domain/note/model/add_note.dart';
import 'package:school_pen/domain/note/model/note.dart';

abstract class NoteRepository {
  /// Stream changes whenever note repository changes.
  Stream<NoteRepository> get onChange;

  /// Returns all stored notes.
  /// Can be filtered by [schoolYearId], null means no filter.
  Future<List<Note>> getAll([String schoolYearId]);

  /// Returns the note by ID or null if it doesn't exist.
  Future<Note> getById(String id);

  /// Returns a list of ids of deleted notes.
  Future<List<String>> getDeletedIds();

  /// Creates a new note and returns its ID.
  Future<String> create(AddNote note);

  /// Updates or creates a new note with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddNote note);

  /// Removes a note with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
