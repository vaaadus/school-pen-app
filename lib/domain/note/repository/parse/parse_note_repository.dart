import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/note/model/add_note.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/repository/note_repository.dart';
import 'package:school_pen/domain/note/repository/parse/parse_note.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';

class ParseNoteRepository implements NoteRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseNoteRepository');
  final NoteRepository _delegate;
  Subscription _subscription;

  ParseNoteRepository(this._delegate);

  @override
  Stream<NoteRepository> get onChange => _delegate.onChange;

  @override
  Future<List<Note>> getAll([String schoolYearId]) => _delegate.getAll(schoolYearId);

  @override
  Future<Note> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddNote note) async {
    final String id = await _delegate.create(note);
    _try(() async {
      final Note newNote = await _delegate.getById(id);
      await _storeOnParse(newNote);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddNote note) async {
    final bool updated = await _delegate.update(id, note);
    if (updated) {
      _try(() async {
        final Note newNote = await _delegate.getById(id);
        await _storeOnParse(newNote);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(Note note) async {
    await _delegate.update(note.id, note.toAddNote());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseNote()));
    _subscription.on(LiveQueryEvent.create, (ParseNote object) {
      _try(() => _delegate.update(object.uuid, object.toAddNote()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseNote object) {
      _try(() => _delegate.update(object.uuid, object.toAddNote()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseNote object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(Note note) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseNote.fromModel(note, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseNote().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
