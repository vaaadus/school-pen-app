import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/note/model/add_note.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/model/note_item.dart';

class ParseNote extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Note';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keySchoolYearId = 'schoolYearUuid';
  static const String keyTitle = 'title';
  static const String keyNote = 'note';
  static const String keyItems = 'items';
  static const String keyAttachments = 'attachments';
  static const String keyColor = 'color';
  static const String keyNotificationDate = 'notificationDate';
  static const String keyIsArchived = 'isArchived';

  ParseNote() : super(keyClassName);

  ParseNote.clone() : this();

  factory ParseNote.fromModel(Note note, ParseUser user) => ParseNote()
    ..user = user
    ..uuid = note.id
    ..updatedAt = note.updatedAt
    ..schoolYearId = note.schoolYearId
    ..title = note.title
    ..note = note.note
    ..items = note.items
    ..color = note.color
    ..notificationDate = note.notificationDate
    ..isArchived = note.isArchived;

  AddNote toAddNote() => AddNote(
        schoolYearId: schoolYearId,
        title: title,
        note: note,
        items: items,
        color: color,
        notificationDate: notificationDate,
        isArchived: isArchived,
        updatedAt: updatedAt,
      );

  @override
  ParseNote clone(Map map) => ParseNote.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get schoolYearId => getNullable(keySchoolYearId);

  set schoolYearId(String id) => setNullable(keySchoolYearId, id);

  String get title => getNullable(keyTitle);

  set title(String title) => setNullable(keyTitle, title);

  String get note => getNullable(keyNote);

  set note(String note) => setNullable(keyNote, note);

  List<NoteItem> get items {
    final List<dynamic> items = getNullable(keyItems);
    return items.map((e) => NoteItem.fromJson(e)).toList();
  }

  set items(List<NoteItem> items) {
    final List<Map<String, dynamic>> result = items.map((e) => e.toJson()).toList();
    setNullable(keyItems, result);
  }

  ColorEnum get color => ColorEnumExt.forTag(getNullable(keyColor));

  set color(ColorEnum color) => setNullable(keyColor, color?.tag);

  DateTime get notificationDate => getDateTime(keyNotificationDate);

  set notificationDate(DateTime notificationDate) => setNullable(keyNotificationDate, notificationDate);

  bool get isArchived => getNullable(keyIsArchived);

  set isArchived(bool value) => setNullable(keyIsArchived, value);

  /// Creates or updates a note.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeNote').execute(parameters: {
      keyUuid: uuid,
      keySchoolYearId: schoolYearId,
      keyTitle: title,
      keyNote: note,
      keyItems: items.map((e) => e.toJson()).toList(),
      keyAttachments: [], // TODO
      keyColor: color?.tag,
      keyNotificationDate: notificationDate?.toString(),
      keyIsArchived: isArchived,
    });
  }
}
