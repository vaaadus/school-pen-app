import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';

class ParseNoteBin extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Note_Bin';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';

  ParseNoteBin() : super(keyClassName);

  ParseNoteBin.clone() : this();

  @override
  ParseNoteBin clone(Map map) => ParseNoteBin.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);
}
