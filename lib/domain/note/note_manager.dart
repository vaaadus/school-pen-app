import 'model/add_note.dart';
import 'model/note.dart';
import 'note_listener.dart';

/// Stores user notes, memos and handles reminders.
abstract class NoteManager {
  void addNoteListener(NoteListener listener, {bool notifyOnAttach = true});

  void removeNoteListener(NoteListener listener);

  /// Returns a list of all created notes.
  /// Can be filtered by [schoolYearId], null means no filter.
  Future<List<Note>> getAll([String schoolYearId]);

  /// Returns the note by ID or null if it doesn't exist.
  Future<Note> getById(String id);

  /// Creates a new note and returns its ID.
  Future<String> create(AddNote note);

  /// Creates or updates a new note.
  Future<void> update(String id, AddNote note);

  Future<void> delete(String id);
}
