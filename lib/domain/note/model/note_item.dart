import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

/// A completable item in a note.
class NoteItem extends Equatable {
  final String id;
  final String text;
  final bool isCompleted;

  const NoteItem({
    @required this.id,
    @required this.text,
    @required this.isCompleted,
  });

  factory NoteItem.fromJson(Map<String, dynamic> map) => NoteItem(
        id: map['id'],
        text: map['text'],
        isCompleted: map['isCompleted'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'text': text,
        'isCompleted': isCompleted,
      };

  NoteItem trim() => NoteItem(
        id: id,
        text: text.trim(),
        isCompleted: isCompleted,
      );

  bool get isEmpty => text.trim().isEmpty;

  @override
  List<Object> get props => [id, text, isCompleted];
}
