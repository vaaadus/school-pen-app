import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/date/date_constraints.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/note/model/add_note.dart';
import 'package:school_pen/domain/note/model/note_item.dart';

// TODO attachments

/// A model entity describing a created note.
///
/// Default sorting order: a newer notes by [updatedAt] are preferred.
class Note extends Equatable implements Comparable<Note> {
  final String id;
  final String schoolYearId;
  final String title;
  final String note;
  final List<NoteItem> items;
  final ColorEnum color;
  final DateTime notificationDate;
  final bool isArchived;
  final DateTime updatedAt;

  Note({
    @required this.id,
    @required this.schoolYearId,
    this.title,
    this.note,
    @required List<NoteItem> items,
    this.color,
    this.notificationDate,
    @required this.isArchived,
    @required this.updatedAt,
  })  : items = List.of(items),
        assert(id != null),
        assert(schoolYearId != null),
        assert(items != null),
        assert(isArchived != null),
        assert(updatedAt != null);

  AddNote toAddNote() {
    return AddNote(
      schoolYearId: schoolYearId,
      title: title,
      note: note,
      items: items,
      color: color,
      notificationDate: notificationDate,
      isArchived: isArchived,
      updatedAt: updatedAt,
    );
  }

  /// Returns the summary of the note which contains the first non-empty text field from this note.
  String get headline {
    if (Strings.isNotBlank(title)) return title;
    if (Strings.isNotBlank(note)) return note;
    if (items.isNotEmpty) return items.first.text;
    return '';
  }

  /// Whether the notification date is in the past.
  /// False when not set.
  bool isNotificationExpired(DateTime now) {
    return notificationDate != null && notificationDate.isBefore(now);
  }

  /// The suggested notification date.
  DateTime suggestedNotificationDate(DateTime now) {
    return calculateSuggestedNotificationDate(now: now, notificationDate: notificationDate);
  }

  /// The suggested notification date.
  /// If the [notificationDate] is already picked, then it will be returned.
  /// Otherwise a new date is selected.
  static DateTime calculateSuggestedNotificationDate({
    @required DateTime now,
    DateTime notificationDate,
  }) {
    if (notificationDate != null) return notificationDate;

    return now.plusDays(1).withTime(Time(hour: 12, minute: 0));
  }

  /// The [notificationDate] constraints.
  DateConstraints notificationConstraints({@required DateTime now, DateTime notificationDate}) {
    final DateTime suggestedDate = calculateSuggestedNotificationDate(now: now, notificationDate: notificationDate);
    DateTime minimumDate;

    if (suggestedDate.isBefore(now)) {
      minimumDate = suggestedDate;
    } else {
      minimumDate = now;
    }

    if (notificationDate != null && notificationDate.isBefore(minimumDate)) {
      minimumDate = notificationDate;
    }

    return DateConstraints(
      minimumDate: minimumDate,
      maximumDate: DateTime(2029, 12, 31, 23, 59),
    );
  }

  @override
  List<Object> get props => [id, schoolYearId, title, note, items, notificationDate, color, isArchived, updatedAt];

  @override
  int compareTo(Note other) => -updatedAt.compareTo(other.updatedAt);
}
