import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/date/date_constraints.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/model/note_item.dart';

/// A model entity describing a request to add a note.
class AddNote extends Equatable {
  final String schoolYearId;
  final String title;
  final String note;
  final List<NoteItem> items;
  final ColorEnum color;
  final DateTime notificationDate;
  final bool isArchived;
  final DateTime updatedAt;

  AddNote({
    @required this.schoolYearId,
    this.title,
    this.note,
    @required List<NoteItem> items,
    this.color,
    this.notificationDate,
    @required this.isArchived,
    @required this.updatedAt,
  })  : items = List.of(items),
        assert(schoolYearId != null),
        assert(items != null),
        assert(isArchived != null),
        assert(updatedAt != null);

  Note toNote({@required String id}) {
    return Note(
      id: id,
      schoolYearId: schoolYearId,
      title: title,
      note: note,
      items: items,
      color: color,
      notificationDate: notificationDate,
      isArchived: isArchived,
      updatedAt: updatedAt,
    );
  }

  /// Removes blank fields.
  AddNote trim() {
    return copyWith(
      title: Optional(title.trim()),
      note: Optional(note.trim()),
      items: Optional(items.map((e) => e.trim()).where((e) => !e.isEmpty).toList()),
    );
  }

  AddNote copyWith({
    Optional<String> schoolYearId,
    Optional<String> title,
    Optional<String> note,
    Optional<List<NoteItem>> items,
    Optional<DateTime> notificationDate,
    Optional<ColorEnum> color,
    Optional<bool> isArchived,
    Optional<DateTime> updatedAt,
  }) {
    return AddNote(
      schoolYearId: Optional.unwrapOrElse(schoolYearId, this.schoolYearId),
      title: Optional.unwrapOrElse(title, this.title),
      note: Optional.unwrapOrElse(note, this.note),
      items: Optional.unwrapOrElse(items, this.items),
      notificationDate: Optional.unwrapOrElse(notificationDate, this.notificationDate),
      color: Optional.unwrapOrElse(color, this.color),
      isArchived: Optional.unwrapOrElse(isArchived, this.isArchived),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  bool isNotificationExpired(DateTime now) {
    return toNote(id: '').isNotificationExpired(now);
  }

  DateTime suggestedNotificationDate(DateTime now) {
    return toNote(id: '').suggestedNotificationDate(now);
  }

  DateConstraints notificationConstraints({@required DateTime now, DateTime notificationDate}) {
    return toNote(id: '').notificationConstraints(now: now, notificationDate: notificationDate);
  }

  @override
  List<Object> get props => [schoolYearId, title, note, items, notificationDate, color, isArchived, updatedAt];
}
