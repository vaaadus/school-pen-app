import 'package:school_pen/domain/note/note_manager.dart';

abstract class NoteListener {
  /// Called when any of notes changes.
  void onNotesChanged(NoteManager manager);
}
