import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/note/notification/note_notification_builder.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/schoolyear/school_year_cleanup_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:synchronized/synchronized.dart';

import 'model/add_note.dart';
import 'model/note.dart';
import 'note_listener.dart';
import 'note_manager.dart';
import 'repository/note_repository.dart';

class NoteManagerImpl implements NoteManager, SchoolYearCleanupListener {
  static const Logger _logger = Logger('NoteManagerImpl');

  final Lock _lock = Lock();
  final Set<NoteListener> _listeners = {};
  final NoteRepository _repository;
  final NotificationManager _notifications;
  final NoteNotificationBuilder _notificationBuilder;
  final SchoolYearManager _schoolYearManager;

  NoteManagerImpl(
    this._repository,
    this._notifications,
    this._notificationBuilder,
    this._schoolYearManager,
  ) {
    _schoolYearManager.addSchoolYearCleanupListener(this);

    _repository.onChange.listen((event) {
      _tryRecreateNotifications();
      _dispatchNotesChanged();
    });
  }

  @override
  void addNoteListener(NoteListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onNotesChanged(this);
  }

  @override
  void removeNoteListener(NoteListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<List<Note>> getAll([String schoolYearId]) async {
    final List<Note> notes = await _repository.getAll(schoolYearId);
    return notes.reversed.toList();
  }

  @override
  Future<Note> getById(String id) async {
    if (id == null) return null;
    return _repository.getById(id);
  }

  @override
  Future<String> create(AddNote note) async {
    _logger.log('create, title: "${note.title}"');

    return await _repository.create(note.trim());
  }

  @override
  Future<void> update(String id, AddNote note) async {
    _logger.log('update, id: $id');

    await _repository.update(id, note.trim());
  }

  @override
  Future<void> delete(String id) async {
    _logger.log('delete, id: $id');

    await _repository.delete(id);
  }

  @override
  Future<void> onCleanupSchoolYear(String schoolYearId) async {
    final List<Future> futures = [];
    for (Note note in await _repository.getAll(schoolYearId)) {
      futures.add(delete(note.id));
    }
    return Future.wait(futures);
  }

  void _tryRecreateNotifications() async {
    await _lock.synchronized(() async {
      try {
        await _recreateNotifications();
      } catch (error, stackTrace) {
        _logger.logError(error, stackTrace);
      }
    });
  }

  Future<void> _recreateNotifications() async {
    final List<Note> notes = await _repository.getAll();
    final List<AddNotification> notifications = notes.map(_asNotification).withoutNulls().toList();
    await _notifications.scheduleAllOnChannel(NotificationChannel.note, notifications);
  }

  AddNotification _asNotification(Note note) {
    if (note.notificationDate != null) {
      return _notificationBuilder.build(note);
    } else {
      return null;
    }
  }

  void _dispatchNotesChanged() {
    for (final listener in _listeners) {
      listener.onNotesChanged(this);
    }
  }
}
