import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';
import 'package:school_pen/generated/l10n.dart';

class NoteNotificationBuilder {
  AddNotification build(Note note) {
    assert(note.notificationDate != null);

    return AddNotification(
      title: _formatTitle(note),
      body: _formatBody(note),
      dateTime: note.notificationDate,
      channel: NotificationChannel.note,
      payload: NotificationPayload(objectId: note.id),
      when: note.notificationDate,
    );
  }

  String _formatTitle(Note note) {
    String title = S().common_note;
    if (note.title.isNotBlank) title += ': ${note.title}';
    return title;
  }

  String _formatBody(Note note) {
    return [
      if (note.note.isNotBlank) note.note,
      ...note.items.map((e) => e.text).where((e) => e.isNotBlank).toList(),
    ].join(' ');
  }
}
