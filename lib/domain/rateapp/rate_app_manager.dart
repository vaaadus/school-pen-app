/// Business logic when the user should rate the app.
abstract class RateAppManager {
  /// Whether the user should rate the app.
  bool get shouldRateNow;

  /// Notify the manager that user has requested to rate the app.
  void onAppRated();

  /// Notify the manager that user has dismissed the rating.
  void onRateAppDismissed();
}
