import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/preferences/preferences.dart';

import 'rate_app_manager.dart';

class RateAppManagerImpl implements RateAppManager {
  static const String keyRateAppInitialWindow = 'key_rateApp_initialWindow';
  static const String keyWasAppRated = 'key_rateApp_wasAppRated';
  static const Duration initialDelay = Duration(days: 3);
  static const Duration nextDelay = Duration(days: 30);

  final SlidingWindow _slidingWindow;
  final Preferences _preferences;

  RateAppManagerImpl(this._slidingWindow, this._preferences);

  @override
  bool get shouldRateNow {
    return !_wasAppRated && _hasRateAppInitialWindowPassed;
  }

  @override
  void onAppRated() {
    _storeAppWasRated(true);
  }

  @override
  void onRateAppDismissed() {
    _slidingWindow.restart(key: keyRateAppInitialWindow);
  }

  bool get _hasRateAppInitialWindowPassed {
    return _slidingWindow.isPassed(
      key: keyRateAppInitialWindow,
      initialWindow: initialDelay,
      nextWindow: nextDelay,
    );
  }

  bool get _wasAppRated {
    return _preferences.getBool(keyWasAppRated) ?? false;
  }

  void _storeAppWasRated(bool wasRated) {
    _preferences.setBool(keyWasAppRated, wasRated);
  }
}
