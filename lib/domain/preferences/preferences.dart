import 'preferences_listener.dart';

abstract class Preferences {
  /// Register a listener which is called whenever preferences change.
  void addPreferencesListener(PreferencesListener listener, {bool notifyOnAttach = true});

  /// Unregister the preferences listeners from any further changes.
  void removePreferencesListener(PreferencesListener listener);

  /// Returns the timestamp of the last update to this preferences.
  /// [getAll] must not contain this field.
  /// If last update timestamp cannot be obtained, then minimum date time should be returned.
  DateTime getLastUpdateTimestamp();

  /// Sets the last update timestamp to an exact date time.
  void setLastUpdateTimestamp(DateTime dateTime);

  /// Returns a map of all keys & values stored in the preferences.
  Map<String, dynamic> getAll();

  /// Stores [all] properties in the preferences, any existing
  /// preference which key would match will be replaced.
  /// Not matched keys are retained.
  void putAll(Map<String, dynamic> all);

  /// Returns the value for this key or null if key not present.
  String getString(String key);

  /// Set the string value for this key,
  /// calling with null value will remove the key.
  void setString(String key, String value);

  /// Returns the int or null if key not present.
  int getInt(String key);

  /// Set the int value for this key,
  /// calling with null value will remove the key.
  void setInt(String key, int value);

  /// Returns the bool or null if key not present.
  bool getBool(String key);

  /// Set the bool value for this key,
  /// calling with null value will remove the key.
  void setBool(String key, bool value);

  /// Returns the date time or null if key not present.
  DateTime getDateTime(String key);

  /// Set the datetime value for this key,
  /// calling with null value will remove the key.
  void setDateTime(String key, DateTime value);

  /// Remove the key & value from preferences.
  void clear(String key);

  /// Removes all keys & values.
  void clearAll();
}
