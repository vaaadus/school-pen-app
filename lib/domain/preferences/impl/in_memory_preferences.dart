import 'package:school_pen/domain/preferences/preferences_listener.dart';

import '../preferences.dart';

/// An implementation of [Preferences] that resides
/// in the RAM memory and is not persisted.
class InMemoryPreferences implements Preferences {
  static const String _keyLastUpdate = '__key_inMemoryPreferences_lastUpdate_ts';
  final Set<PreferencesListener> _listeners = {};
  final Map<String, dynamic> _data = {};

  @override
  void addPreferencesListener(PreferencesListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onPreferencesChanged(this, _data.keys.toList());
  }

  @override
  void removePreferencesListener(PreferencesListener listener) {
    _listeners.remove(listener);
  }

  @override
  DateTime getLastUpdateTimestamp() {
    return getDateTime(_keyLastUpdate) ?? DateTime.fromMicrosecondsSinceEpoch(0);
  }

  @override
  void setLastUpdateTimestamp(DateTime dateTime) {
    _setItem(_keyLastUpdate, dateTime);
  }

  @override
  Map<String, dynamic> getAll() {
    final Map<String, dynamic> result = Map.from(_data);
    result.remove(_keyLastUpdate);
    return result;
  }

  @override
  void putAll(Map<String, dynamic> all) {
    for (final key in all.keys) {
      _setItem(key, all[key]);
    }
    _onPreferencesChanged(all.keys.toList());
  }

  @override
  String getString(String key) {
    return _data[key];
  }

  @override
  void setString(String key, String value) {
    if (getString(key) != value) {
      _setItem(key, value);
      _onPreferencesChanged([key]);
    }
  }

  @override
  int getInt(String key) {
    return _data[key];
  }

  @override
  void setInt(String key, int value) {
    if (getInt(key) != value) {
      _setItem(key, value);
      _onPreferencesChanged([key]);
    }
  }

  @override
  bool getBool(String key) {
    return _data[key];
  }

  @override
  void setBool(String key, bool value) {
    if (getBool(key) != value) {
      _setItem(key, value);
      _onPreferencesChanged([key]);
    }
  }

  @override
  DateTime getDateTime(String key) {
    final int millis = _data[key];
    return millis != null ? DateTime.fromMicrosecondsSinceEpoch(millis) : null;
  }

  @override
  void setDateTime(String key, DateTime value) {
    if (getDateTime(key) != value) {
      _setItem(key, value);
      _onPreferencesChanged([key]);
    }
  }

  @override
  void clear(String key) {
    _setItem(key, null);
    _onPreferencesChanged([key]);
  }

  @override
  void clearAll() {
    final List<String> keys = _data.keys.toList();
    _data.clear();
    _onPreferencesChanged(keys);
  }

  void _setItem(String key, dynamic value) {
    if (key == null) return;

    if (value is DateTime) {
      _data[key] = value.microsecondsSinceEpoch;
    } else if (value != null) {
      _data[key] = value;
    } else {
      _data.remove(key);
    }
  }

  void _onPreferencesChanged(List<String> keys) {
    _setItem(_keyLastUpdate, DateTime.now());

    for (final listener in _listeners) {
      listener.onPreferencesChanged(this, keys);
    }
  }
}
