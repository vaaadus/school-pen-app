import 'package:school_pen/domain/preferences/preferences_listener.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../preferences.dart';

/// An implementation of [Preferences] which stores data in the
/// shared preferences, which might be backed up by the OS.
class SharedPreferencesImpl implements Preferences {
  static const String _keyLastUpdate = '__key_sharedPreferences_lastUpdate_ts';

  final Set<PreferencesListener> _listeners = {};
  final SharedPreferences _preferences;

  SharedPreferencesImpl._(this._preferences);

  static Future<SharedPreferencesImpl> create() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return SharedPreferencesImpl._(prefs);
  }

  @override
  void addPreferencesListener(PreferencesListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onPreferencesChanged(this, _preferences.getKeys().toList());
  }

  @override
  void removePreferencesListener(PreferencesListener listener) {
    _listeners.remove(listener);
  }

  @override
  DateTime getLastUpdateTimestamp() {
    return getDateTime(_keyLastUpdate) ?? DateTime.fromMicrosecondsSinceEpoch(0);
  }

  @override
  void setLastUpdateTimestamp(DateTime dateTime) {
    _setItem(_keyLastUpdate, dateTime);
  }

  @override
  Map<String, dynamic> getAll() {
    final Map<String, dynamic> all = {};
    for (final key in _preferences.getKeys()) {
      all[key] = _preferences.get(key);
    }
    all.remove(_keyLastUpdate);
    return all;
  }

  @override
  void putAll(Map<String, dynamic> all) {
    for (final key in all.keys) {
      _setItem(key, all[key]);
    }
    _onPreferencesChanged(all.keys);
  }

  @override
  String getString(String key) {
    return _preferences.getString(key);
  }

  @override
  void setString(String key, String value) {
    if (getString(key) != value) {
      _setItem(key, value);
      _onPreferencesChanged([key]);
    }
  }

  @override
  int getInt(String key) {
    return _preferences.getInt(key);
  }

  @override
  void setInt(String key, int value) {
    if (getInt(key) != value) {
      _setItem(key, value);
      _onPreferencesChanged([key]);
    }
  }

  @override
  DateTime getDateTime(String key) {
    final int millis = _preferences.getInt(key);
    return millis != null ? DateTime.fromMicrosecondsSinceEpoch(millis) : null;
  }

  @override
  void setDateTime(String key, DateTime value) {
    if (getDateTime(key) != value) {
      _setItem(key, value);
      _onPreferencesChanged([key]);
    }
  }

  @override
  bool getBool(String key) {
    return _preferences.getBool(key);
  }

  @override
  void setBool(String key, bool value) {
    if (getBool(key) != value) {
      _setItem(key, value);
      _onPreferencesChanged([key]);
    }
  }

  @override
  void clear(String key) {
    _setItem(key, null);
    _onPreferencesChanged([key]);
  }

  @override
  void clearAll() {
    final List<String> keys = _preferences.getKeys().toList();
    _preferences.clear();
    _onPreferencesChanged(keys);
  }

  void _setItem(String key, dynamic value) {
    if (key == null) return;

    if (value == null) {
      _preferences.remove(key);
    } else if (value is String) {
      _preferences.setString(key, value);
    } else if (value is int) {
      _preferences.setInt(key, value);
    } else if (value is DateTime) {
      final int intValue = value != null ? value.microsecondsSinceEpoch : null;
      _preferences.setInt(key, intValue);
    } else if (value is bool) {
      _preferences.setBool(key, value);
    }
  }

  void _onPreferencesChanged(List<String> keys) {
    _setItem(_keyLastUpdate, DateTime.now());

    // iterate on copy to avoid concurrent modification
    for (final listener in List.of(_listeners)) {
      if (_listeners.contains(listener)) {
        // if still registered
        listener.onPreferencesChanged(this, keys);
      }
    }
  }
}
