import 'preferences.dart';

abstract class PreferencesListener {
  /// Called whenever any preferences change.
  void onPreferencesChanged(Preferences preferences, List<String> changedKeys);
}
