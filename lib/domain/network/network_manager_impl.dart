import 'package:connectivity/connectivity.dart';

import 'network_manager.dart';

class NetworkManagerImpl implements NetworkManager {
  final Connectivity _connectivity;

  NetworkManagerImpl(this._connectivity);

  @override
  Future<bool> isNetworkAvailable() async {
    var connectivityResult = await _connectivity.checkConnectivity();
    return connectivityResult != ConnectivityResult.none;
  }
}
