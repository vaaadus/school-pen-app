/// Provides info about network connections.
abstract class NetworkManager {
  /// true when any internet connection is available.
  Future<bool> isNetworkAvailable();
}
