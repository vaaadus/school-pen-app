import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/course/course_cleanup_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/grade/grade_cleanup_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/homework/model/add_homework.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/homework/notification/homework_notification_builder.dart';
import 'package:school_pen/domain/homework/repository/homework_repository.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/semester/semester_cleanup_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:synchronized/synchronized.dart';

import 'homework_listener.dart';
import 'homework_manager.dart';

class HomeworkManagerImpl
    implements HomeworkManager, SemesterCleanupListener, CourseCleanupListener, GradeCleanupListener {
  static const Logger _logger = Logger('HomeworkManagerImpl');

  final Lock _lock = Lock();
  final Set<HomeworkListener> _listeners = {};
  final HomeworkRepository _repository;
  final NotificationManager _notifications;
  final HomeworkNotificationBuilder _notificationBuilder;
  final SemesterManager _semesterManager;
  final CourseManager _courseManager;
  final GradeManager _gradeManager;

  HomeworkManagerImpl(
    this._repository,
    this._notifications,
    this._notificationBuilder,
    this._semesterManager,
    this._courseManager,
    this._gradeManager,
  ) {
    _semesterManager.addSemesterCleanupListener(this);
    _courseManager.addCourseCleanupListener(this);
    _gradeManager.addGradeCleanupListener(this);

    _repository.onChange.listen((event) {
      _tryRecreateNotifications();
      _dispatchHomeworkChanged();
    });
  }

  @override
  void addHomeworkListener(HomeworkListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onHomeworkChanged(this);
  }

  @override
  void removeHomeworkListener(HomeworkListener listener) {
    _listeners.remove(listener);
  }

  @override
  Future<List<Homework>> getAll({String semesterId, String courseId}) async {
    final List<Homework> homework = await _repository.getAll(semesterId: semesterId, courseId: courseId);
    homework.sort();
    return homework;
  }

  @override
  Future<Homework> getById(String id) async {
    if (id == null) return null;
    return _repository.getById(id);
  }

  @override
  Future<String> create(AddHomework homework) async {
    _logger.log('create, title: "${homework.title}"');

    return await _repository.create(homework.trim());
  }

  @override
  Future<void> update(String id, AddHomework homework) async {
    _logger.log('update, id: $id');

    await _repository.update(id, homework.trim());
  }

  @override
  Future<void> delete(String id) async {
    _logger.log('delete, id: $id');

    await _repository.delete(id);
  }

  @override
  Future<void> onCleanupSemester(String semesterId) async {
    final List<Future> futures = [];
    for (Homework homework in await _repository.getAll(semesterId: semesterId)) {
      futures.add(delete(homework.id));
    }
    return Future.wait(futures);
  }

  @override
  Future<void> onCleanupCourse(String courseId) async {
    final List<Future> futures = [];
    for (Homework homework in await _repository.getAll(courseId: courseId)) {
      futures.add(delete(homework.id));
    }
    return Future.wait(futures);
  }

  @override
  Future<void> onCleanupGrade(String gradeId) async {
    final List<Future> futures = [];
    for (Homework homework in await _repository.getAll(gradeId: gradeId)) {
      final AddHomework newHomework = homework.toAddHomework().copyWith(gradeId: Optional(null));
      futures.add(update(homework.id, newHomework));
    }
    return Future.wait(futures);
  }

  void _tryRecreateNotifications() async {
    await _lock.synchronized(() async {
      try {
        await _recreateNotifications();
      } catch (error, stackTrace) {
        _logger.logError(error, stackTrace);
      }
    });
  }

  Future<void> _recreateNotifications() async {
    final List<Homework> homework = await _repository.getAll();
    final List<AddNotification> notifications = await Future.wait(homework.map(_asNotification));
    await _notifications.scheduleAllOnChannel(NotificationChannel.homework, notifications.withoutNulls().toList());
  }

  Future<AddNotification> _asNotification(Homework homework) async {
    if (homework.notification == null) return null;

    final Course course = await _courseManager.getById(homework.courseId);
    return _notificationBuilder.build(homework, course);
  }

  void _dispatchHomeworkChanged() {
    for (final listener in _listeners) {
      listener.onHomeworkChanged(this);
    }
  }
}
