import 'homework_manager.dart';

abstract class HomeworkListener {
  /// Called when any of homework changes.
  void onHomeworkChanged(HomeworkManager manager);
}
