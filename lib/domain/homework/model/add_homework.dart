import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/notification_options.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/homework/model/homework.dart';

/// A model entity describing a request to add a homework.
class AddHomework extends Equatable {
  final String semesterId;
  final String courseId;
  final String gradeId;
  final String title;
  final String note;
  final DateTime dateTime;
  final NotificationOptions notification;
  final bool isArchived;
  final DateTime updatedAt;

  const AddHomework({
    @required this.semesterId,
    @required this.courseId,
    this.gradeId,
    this.title,
    this.note,
    @required this.dateTime,
    this.notification,
    @required this.isArchived,
    @required this.updatedAt,
  })  : assert(semesterId != null),
        assert(courseId != null),
        assert(dateTime != null),
        assert(isArchived != null),
        assert(updatedAt != null);

  Homework toHomework({@required String id}) {
    return Homework(
      id: id,
      semesterId: semesterId,
      courseId: courseId,
      gradeId: gradeId,
      title: title,
      note: note,
      dateTime: dateTime,
      notification: notification,
      isArchived: isArchived,
      updatedAt: updatedAt,
    );
  }

  /// Removes blank fields.
  AddHomework trim() {
    return copyWith(
      title: Optional(title?.trim()),
      note: Optional(note?.trim()),
    );
  }

  AddHomework copyWith({
    Optional<String> semesterId,
    Optional<String> courseId,
    Optional<String> gradeId,
    Optional<String> title,
    Optional<String> note,
    Optional<DateTime> dateTime,
    Optional<NotificationOptions> notification,
    Optional<bool> isArchived,
    Optional<DateTime> updatedAt,
  }) {
    return AddHomework(
      semesterId: Optional.unwrapOrElse(semesterId, this.semesterId),
      courseId: Optional.unwrapOrElse(courseId, this.courseId),
      gradeId: Optional.unwrapOrElse(gradeId, this.gradeId),
      title: Optional.unwrapOrElse(title, this.title),
      note: Optional.unwrapOrElse(note, this.note),
      dateTime: Optional.unwrapOrElse(dateTime, this.dateTime),
      notification: Optional.unwrapOrElse(notification, this.notification),
      isArchived: Optional.unwrapOrElse(isArchived, this.isArchived),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  /// Whether the notification date is in the past.
  /// False when not set.
  bool isNotificationExpired(DateTime now) {
    return toHomework(id: '').isNotificationExpired(now);
  }

  @override
  List<Object> get props => [semesterId, courseId, gradeId, title, note, dateTime, notification, isArchived, updatedAt];
}
