import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/notification_options.dart';

import 'add_homework.dart';

// TODO attachments

/// A model entity describing a created homework.
///
/// Default sorting order: homeworks by [dateTime] from older to newer.
class Homework extends Equatable implements Comparable<Homework> {
  final String id;
  final String semesterId;
  final String courseId;
  final String gradeId;
  final String title;
  final String note;
  final DateTime dateTime;
  final NotificationOptions notification;
  final bool isArchived;
  final DateTime updatedAt;

  const Homework({
    @required this.id,
    @required this.semesterId,
    @required this.courseId,
    this.gradeId,
    this.title,
    this.note,
    @required this.dateTime,
    this.notification,
    @required this.isArchived,
    @required this.updatedAt,
  })  : assert(id != null),
        assert(semesterId != null),
        assert(courseId != null),
        assert(dateTime != null),
        assert(isArchived != null),
        assert(updatedAt != null);

  AddHomework toAddHomework() {
    return AddHomework(
      semesterId: semesterId,
      courseId: courseId,
      gradeId: gradeId,
      title: title,
      note: note,
      dateTime: dateTime,
      notification: notification,
      isArchived: isArchived,
      updatedAt: updatedAt,
    );
  }

  /// Returns the summary of the homework which contains the first non-empty text field from this homework.
  String get headline {
    if (Strings.isNotBlank(title)) return title;
    if (Strings.isNotBlank(note)) return note;
    return '';
  }

  /// Whether the notification date is in the past.
  /// False when not set.
  bool isNotificationExpired(DateTime now) {
    if (notification == null) return false;
    return notification.getFireDateTime(dateTime).isBefore(now);
  }

  @override
  List<Object> get props =>
      [id, semesterId, courseId, gradeId, title, note, dateTime, notification, isArchived, updatedAt];

  @override
  int compareTo(Homework other) => dateTime.compareTo(other.dateTime);
}
