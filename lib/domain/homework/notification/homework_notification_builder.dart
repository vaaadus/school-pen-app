import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';
import 'package:school_pen/generated/l10n.dart';

class HomeworkNotificationBuilder {
  AddNotification build(Homework homework, Course course) {
    assert(homework.notification != null);

    return AddNotification(
      title: _formatTitle(homework, course),
      body: _formatBody(homework),
      dateTime: homework.notification.getFireDateTime(homework.dateTime),
      channel: NotificationChannel.homework,
      payload: NotificationPayload(objectId: homework.id),
      when: homework.dateTime,
    );
  }

  String _formatTitle(Homework homework, Course course) {
    String title = S().common_homework;
    if (course != null) title += '/' + course.name;
    if (homework.title.isNotBlank) title += ': ${homework.title}';
    return title;
  }

  String _formatBody(Homework homework) {
    return homework.note ?? '';
  }
}
