import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/notification_options.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/homework/model/add_homework.dart';
import 'package:school_pen/domain/homework/model/homework.dart';

class ParseHomework extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Assignment';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keySemesterId = 'semesterUuid';
  static const String keyCourseId = 'courseUuid';
  static const String keyGradeId = 'gradeUuid';
  static const String keyTitle = 'title';
  static const String keyNote = 'note';
  static const String keyDateTime = 'dateTime';
  static const String keyNotification = 'notification';
  static const String keyIsArchived = 'isArchived';

  ParseHomework() : super(keyClassName);

  ParseHomework.clone() : this();

  factory ParseHomework.fromModel(Homework homework, ParseUser user) => ParseHomework()
    ..user = user
    ..uuid = homework.id
    ..updatedAt = homework.updatedAt
    ..semesterId = homework.semesterId
    ..courseId = homework.courseId
    ..gradeId = homework.gradeId
    ..title = homework.title
    ..note = homework.note
    ..dateTime = homework.dateTime
    ..notification = homework.notification
    ..isArchived = homework.isArchived;

  AddHomework toAddHomework() => AddHomework(
        semesterId: semesterId,
        courseId: courseId,
        gradeId: gradeId,
        title: title,
        note: note,
        dateTime: dateTime,
        notification: notification,
        isArchived: isArchived,
        updatedAt: updatedAt,
      );

  @override
  ParseHomework clone(Map map) => ParseHomework.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get semesterId => getNullable(keySemesterId);

  set semesterId(String id) => setNullable(keySemesterId, id);

  String get courseId => getNullable(keyCourseId);

  set courseId(String id) => setNullable(keyCourseId, id);

  String get gradeId => getNullable(keyGradeId);

  set gradeId(String id) => setNullable(keyGradeId, id);

  String get title => getNullable(keyTitle);

  set title(String title) => setNullable(keyTitle, title);

  String get note => getNullable(keyNote);

  set note(String note) => setNullable(keyNote, note);

  DateTime get dateTime => getDateTime(keyDateTime);

  set dateTime(DateTime dateTime) => setNullable(keyDateTime, dateTime);

  NotificationOptions get notification =>
      SembastSerializer.deserializeNotificationOptions(getNullable(keyNotification));

  set notification(NotificationOptions options) =>
      setNullable(keyNotification, SembastSerializer.serializeNotificationOptions(options));

  bool get isArchived => getNullable(keyIsArchived);

  set isArchived(bool value) => setNullable(keyIsArchived, value);

  /// Creates or updates a homework.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeAssignment').execute(parameters: {
      keyUuid: uuid,
      keySemesterId: semesterId,
      keyCourseId: courseId,
      keyGradeId: gradeId,
      keyTitle: title,
      keyNote: note,
      keyDateTime: dateTime?.toString(),
      keyNotification: SembastSerializer.serializeNotificationOptions(notification),
      keyIsArchived: isArchived,
    });
  }
}
