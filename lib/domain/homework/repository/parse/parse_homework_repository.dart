import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/homework/model/add_homework.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/homework/repository/homework_repository.dart';
import 'package:school_pen/domain/homework/repository/parse/parse_homework.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';

class ParseHomeworkRepository implements HomeworkRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseHomeworkRepository');
  final HomeworkRepository _delegate;
  Subscription _subscription;

  ParseHomeworkRepository(this._delegate);

  @override
  Stream<HomeworkRepository> get onChange => _delegate.onChange;

  @override
  Future<List<Homework>> getAll({String semesterId, String courseId, String gradeId}) =>
      _delegate.getAll(semesterId: semesterId, courseId: courseId, gradeId: gradeId);

  @override
  Future<Homework> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddHomework homework) async {
    final String id = await _delegate.create(homework);
    _try(() async {
      final Homework newHomework = await _delegate.getById(id);
      await _storeOnParse(newHomework);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddHomework homework) async {
    final bool updated = await _delegate.update(id, homework);
    if (updated) {
      _try(() async {
        final Homework newHomework = await _delegate.getById(id);
        await _storeOnParse(newHomework);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(Homework homework) async {
    await _delegate.update(homework.id, homework.toAddHomework());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseHomework()));
    _subscription.on(LiveQueryEvent.create, (ParseHomework object) {
      _try(() => _delegate.update(object.uuid, object.toAddHomework()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseHomework object) {
      _try(() => _delegate.update(object.uuid, object.toAddHomework()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseHomework object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(Homework homework) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseHomework.fromModel(homework, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseHomework().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
