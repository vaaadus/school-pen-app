import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/homework/model/add_homework.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/homework/repository/homework_repository.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// A [HomeworkRepository] that saves homework in sembast document database.
class SembastHomeworkRepository implements HomeworkRepository {
  static const String keyDeletedIds = 'key_sembast_deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('homework');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('homework_bin');
  final BehaviorSubject<HomeworkRepository> _onChange = BehaviorSubject();
  final Lock _lock = Lock();

  SembastHomeworkRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<HomeworkRepository> get onChange => _onChange;

  @override
  Future<List<Homework>> getAll({String semesterId, String courseId, String gradeId}) async {
    final List<Filter> filters = [
      if (semesterId != null) Filter.equals('semesterId', semesterId),
      if (courseId != null) Filter.equals('courseId', courseId),
      if (gradeId != null) Filter.equals('gradeId', gradeId),
    ];

    final Finder finder = filters.isEmpty ? null : Finder(filter: Filter.and(filters));
    final List<RecordSnapshot> snapshots = await _store.find(await _db, finder: finder);
    return snapshots.map((e) => _deserializeHomework(e.value).toHomework(id: e.key)).toList();
  }

  @override
  Future<Homework> getById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> snapshot = await _store.record(id).get(await _db);
    return _deserializeHomework(snapshot)?.toHomework(id: id);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(keyDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<String> create(AddHomework homework) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = _serializeHomework(homework);
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddHomework homework) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = _serializeHomework(homework);
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = List.of(await getDeletedIds());
      if (ids.addIfAbsent(id)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      return deleted;
    });
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}

Map<String, dynamic> _serializeHomework(AddHomework homework) {
  if (homework == null) return null;

  return {
    'semesterId': homework.semesterId,
    'courseId': homework.courseId,
    'gradeId': homework.gradeId,
    'title': homework.title,
    'note': homework.note,
    'dateTime': SembastSerializer.serializeDateTimeField(homework.dateTime),
    'notification': SembastSerializer.serializeNotificationOptions(homework.notification),
    'isArchived': homework.isArchived,
    'updatedAt': SembastSerializer.serializeDateTimeField(homework.updatedAt),
  };
}

AddHomework _deserializeHomework(Map<String, dynamic> map) {
  if (map == null) return null;

  return AddHomework(
    semesterId: map['semesterId'],
    courseId: map['courseId'],
    gradeId: map['gradeId'],
    title: map['title'],
    note: map['note'],
    dateTime: SembastSerializer.deserializeDateTimeField(map['dateTime']),
    notification: SembastSerializer.deserializeNotificationOptions(map['notification']),
    isArchived: map['isArchived'],
    updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
  );
}
