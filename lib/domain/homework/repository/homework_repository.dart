import 'package:school_pen/domain/homework/model/add_homework.dart';
import 'package:school_pen/domain/homework/model/homework.dart';

abstract class HomeworkRepository {
  /// Stream changes whenever homework repository changes.
  Stream<HomeworkRepository> get onChange;

  /// Returns all stored homework.
  /// Can be filtered by [semesterId], null means no filter.
  /// Can be filtered by [courseId], null means no filter.
  /// Can be filtered by [gradeId], null means no filter.
  Future<List<Homework>> getAll({String semesterId, String courseId, String gradeId});

  /// Returns the homework by ID or null if it doesn't exist.
  Future<Homework> getById(String id);

  /// Returns a list of ids of deleted homework.
  Future<List<String>> getDeletedIds();

  /// Creates a new homework and returns its ID.
  Future<String> create(AddHomework homework);

  /// Updates or creates a new homework with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddHomework homework);

  /// Removes a homework with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
