import 'package:school_pen/domain/homework/model/add_homework.dart';
import 'package:school_pen/domain/homework/model/homework.dart';

import 'homework_listener.dart';

/// Stores user homework and handles reminders.
abstract class HomeworkManager {
  void addHomeworkListener(HomeworkListener listener, {bool notifyOnAttach = true});

  void removeHomeworkListener(HomeworkListener listener);

  /// Returns a list of all created homework.
  /// Can be filtered by [semesterId], null means no filter.
  /// Can be filtered by [courseId], null means no filter.
  Future<List<Homework>> getAll({String semesterId, String courseId});

  /// Returns the homework by ID or null if it doesn't exist.
  Future<Homework> getById(String id);

  /// Creates a new homework and returns its ID.
  Future<String> create(AddHomework homework);

  /// Creates or updates a new homework.
  Future<void> update(String id, AddHomework homework);

  Future<void> delete(String id);
}
