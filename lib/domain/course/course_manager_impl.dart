import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/schoolyear/school_year_cleanup_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/teacher/teacher_cleanup_listener.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';

import 'course_cleanup_listener.dart';
import 'course_listener.dart';
import 'course_manager.dart';
import 'model/add_course.dart';
import 'model/course.dart';
import 'repository/course_repository.dart';

class CourseManagerImpl implements CourseManager, SchoolYearCleanupListener, TeacherCleanupListener {
  final Set<CourseListener> _listeners = {};
  final Set<CourseCleanupListener> _cleanupListeners = {};
  final CourseRepository _repository;
  final SchoolYearManager _schoolYearManager;
  final TeacherManager _teacherManager;

  CourseManagerImpl(this._repository, this._schoolYearManager, this._teacherManager) {
    _repository.onChange.listen((event) => _dispatchCoursesChanged());
    _repository.onCleanupCourse.listen((id) => _dispatchCourseCleanup(id));
    _schoolYearManager.addSchoolYearCleanupListener(this);
    _teacherManager.addTeacherCleanupListener(this);
  }

  @override
  void addCourseListener(CourseListener listener, {bool notifyOnAttach = true}) {
    _listeners.add(listener);
    if (notifyOnAttach) listener.onCoursesChanged(this);
  }

  @override
  void removeCourseListener(CourseListener listener) {
    _listeners.remove(listener);
  }

  @override
  void addCourseCleanupListener(CourseCleanupListener listener) {
    _cleanupListeners.add(listener);
  }

  @override
  void removeCourseCleanupListener(CourseCleanupListener listener) {
    _cleanupListeners.remove(listener);
  }

  @override
  Future<List<Course>> getAll([String schoolYearId]) {
    return _repository.getAll(schoolYearId: schoolYearId);
  }

  @override
  Future<List<Course>> getAllByTeacherId(String teacherId) {
    return _repository.getAll(teachersIds: [teacherId]);
  }

  @override
  Future<Course> getById(String id) {
    if (id == null) return null;
    return _repository.getById(id);
  }

  @override
  Future<String> create(AddCourse course) async {
    if (course.isValid) {
      return await _repository.create(course.trim());
    } else {
      throw ArgumentError('Course is invalid: $course');
    }
  }

  @override
  Future<void> update(String id, AddCourse course) async {
    if (course.isValid) {
      await _repository.update(id, course.trim());
    } else {
      throw ArgumentError('Course is invalid: $course');
    }
  }

  @override
  Future<void> delete(String id) async {
    await _repository.delete(id);
  }

  @override
  Future<void> onCleanupTeacher(String teacherId) async {
    for (Course course in await _repository.getAll(teachersIds: [teacherId])) {
      final AddCourse newCourse = course.toAddCourse().copyWith(
            teachersIds: Optional(course.teachersIds.where((e) => e != teacherId).toList()),
          );

      await _repository.update(course.id, newCourse);
    }
  }

  @override
  Future<void> onCleanupSchoolYear(String schoolYearId) async {
    for (Course course in await _repository.getAll(schoolYearId: schoolYearId)) {
      await _repository.delete(course.id);
    }
  }

  void _dispatchCoursesChanged() {
    for (var listener in _listeners) {
      listener.onCoursesChanged(this);
    }
  }

  Future<void> _dispatchCourseCleanup(String courseId) async {
    for (var listener in _cleanupListeners) {
      await listener.onCleanupCourse(courseId);
    }
  }
}
