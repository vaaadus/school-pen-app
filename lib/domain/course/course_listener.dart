import 'course_manager.dart';

abstract class CourseListener {
  /// Called whenever any property of [CourseManager] changes.
  void onCoursesChanged(CourseManager manager);
}
