import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/optional.dart';

import 'course.dart';

/// A request to add a [Course], looks the same but without the ID.
class AddCourse extends Equatable {
  final String schoolYearId;
  final String name;
  final ColorEnum color;
  final String room;
  final List<String> teachersIds;
  final double creditHours;
  final String note;
  final DateTime updatedAt;

  AddCourse({
    @required this.schoolYearId,
    @required this.name,
    this.color,
    this.room,
    List<String> teachersIds,
    this.creditHours,
    this.note,
    @required this.updatedAt,
  }) : teachersIds = List.of(teachersIds);

  Course toCourse({@required String id}) {
    return Course(
      id: id,
      schoolYearId: schoolYearId,
      name: name,
      color: color,
      room: room,
      teachersIds: teachersIds,
      creditHours: creditHours,
      note: note,
      updatedAt: updatedAt,
    );
  }

  AddCourse trim() {
    return copyWith(
      name: Optional(name.trim()),
      room: Optional(room?.trim()),
      note: Optional(note?.trim()),
      teachersIds: Optional(teachersIds.skipDuplicates()),
    );
  }

  AddCourse copyWith({
    Optional<String> schoolYearId,
    Optional<String> name,
    Optional<ColorEnum> color,
    Optional<String> room,
    Optional<List<String>> teachersIds,
    Optional<double> creditHours,
    Optional<String> note,
    Optional<DateTime> updatedAt,
  }) {
    return AddCourse(
      schoolYearId: Optional.unwrapOrElse(schoolYearId, this.schoolYearId),
      name: Optional.unwrapOrElse(name, this.name),
      color: Optional.unwrapOrElse(color, this.color),
      room: Optional.unwrapOrElse(room, this.room),
      teachersIds: Optional.unwrapOrElse(teachersIds, this.teachersIds),
      creditHours: Optional.unwrapOrElse(creditHours, this.creditHours),
      note: Optional.unwrapOrElse(note, this.note),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  bool get isValid {
    return Strings.isNotBlank(name);
  }

  @override
  List<Object> get props => [schoolYearId, name, color, room, teachersIds, creditHours, note, updatedAt];
}
