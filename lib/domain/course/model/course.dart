import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/domain/common/color_enum.dart';

import 'add_course.dart';

/// A class in a school (for example: math, English, etc.)
class Course extends Equatable {
  final String id;
  final String schoolYearId;
  final String name;
  final ColorEnum color;
  final String room;
  final List<String> teachersIds;
  final double creditHours;
  final String note;
  final DateTime updatedAt;

  Course({
    @required this.id,
    @required this.schoolYearId,
    @required this.name,
    this.color,
    this.room,
    @required List<String> teachersIds,
    this.creditHours,
    this.note,
    @required this.updatedAt,
  }) : teachersIds = List.of(teachersIds);

  AddCourse toAddCourse() {
    return AddCourse(
      schoolYearId: schoolYearId,
      name: name,
      color: color,
      room: room,
      teachersIds: teachersIds,
      creditHours: creditHours,
      note: note,
      updatedAt: updatedAt,
    );
  }

  @override
  List<Object> get props => [id, schoolYearId, name, color, room, teachersIds, creditHours, note, updatedAt];
}
