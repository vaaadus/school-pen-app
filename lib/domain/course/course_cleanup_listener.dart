abstract class CourseCleanupListener {
  /// Called when a course is removed and all related data must be removed as well.
  Future<void> onCleanupCourse(String courseId);
}
