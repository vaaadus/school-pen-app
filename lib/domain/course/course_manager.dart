import 'package:school_pen/domain/course/model/add_course.dart';

import 'course_cleanup_listener.dart';
import 'course_listener.dart';
import 'model/course.dart';

/// Manages student classes, such as math, English, etc...
abstract class CourseManager {
  void addCourseListener(CourseListener listener, {bool notifyOnAttach = true});

  void removeCourseListener(CourseListener listener);

  void addCourseCleanupListener(CourseCleanupListener listener);

  void removeCourseCleanupListener(CourseCleanupListener listener);

  /// Returns a list of all created courses.
  /// Can be filtered by [schoolYearId], null means no filter.
  Future<List<Course>> getAll([String schoolYearId]);

  /// Returns all courses led by the teacher.
  Future<List<Course>> getAllByTeacherId(String teacherId);

  /// Returns the courses by ID or null if it doesn't exist.
  Future<Course> getById(String id);

  /// Creates a new course and returns its ID.
  Future<String> create(AddCourse course);

  Future<void> update(String id, AddCourse course);

  Future<void> delete(String id);
}
