import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sembast/sembast_database.dart';
import 'package:school_pen/domain/common/sembast/sembast_serializer.dart';
import 'package:school_pen/domain/course/model/add_course.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/course/repository/course_repository.dart';
import 'package:sembast/sembast.dart';
import 'package:synchronized/synchronized.dart';

/// A [CourseRepository] that saves courses in sembast document database.
class SembastCourseRepository implements CourseRepository {
  static const String keyDeletedIds = 'key_sembast_deletedItems';
  final StoreRef<String, Map<String, dynamic>> _store = stringMapStoreFactory.store('courses');
  final StoreRef<String, Map<String, dynamic>> _binStore = stringMapStoreFactory.store('courses_bin');
  final BehaviorSubject<CourseRepository> _onChange = BehaviorSubject();
  final BehaviorSubject<String> _onCleanupCourse = BehaviorSubject();
  final Lock _lock = Lock();

  SembastCourseRepository() {
    _listenForChanges();
  }

  void _listenForChanges() async {
    _store.query().onSnapshots(await _db).listen((event) => _onChange.add(this));
  }

  @override
  Stream<CourseRepository> get onChange => _onChange;

  @override
  Stream<String> get onCleanupCourse => _onCleanupCourse;

  @override
  Future<List<Course>> getAll({String schoolYearId, List<String> teachersIds}) async {
    final List<Filter> filters = [
      if (schoolYearId != null) Filter.equals('schoolYearId', schoolYearId),
      if (teachersIds != null) Filter.custom((record) => _recordHasAnyTeacher(record, teachersIds)),
    ];

    final Finder finder = filters.isEmpty ? null : Finder(filter: Filter.and(filters));
    final List<RecordSnapshot> snapshots = await _store.find(await _db, finder: finder);
    return snapshots.map((e) => _deserializeCourse(e.value).toCourse(id: e.key)).toList();
  }

  @override
  Future<Course> getById(String id) async {
    if (id == null) return null;

    final Map<String, dynamic> record = await _store.record(id).get(await _db);
    return _deserializeCourse(record)?.toCourse(id: id);
  }

  @override
  Future<List<String>> getDeletedIds() async {
    final Map<String, dynamic> record = await _binStore.record(keyDeletedIds).get(await _db);
    return SembastSerializer.deserializeStringList(record);
  }

  @override
  Future<String> create(AddCourse course) {
    return _lock.synchronized(() async {
      final String id = Id.random();
      final Map<String, dynamic> value = _serializeCourse(course);
      await _store.record(id).add(await _db, value);
      return id;
    });
  }

  @override
  Future<bool> update(String id, AddCourse course) {
    return _lock.synchronized(() async {
      final Map<String, dynamic> value = _serializeCourse(course);
      if (mapEquals(value, await _store.record(id).get(await _db))) return false;

      await _store.record(id).put(await _db, value);
      return true;
    });
  }

  @override
  Future<bool> delete(String id) {
    return _lock.synchronized(() async {
      final bool deleted = await _store.record(id).delete(await _db) != null;

      final List<String> ids = List.of(await getDeletedIds());
      if (ids.addIfAbsent(id)) {
        await _binStore.record(keyDeletedIds).put(await _db, SembastSerializer.serializeStringList(ids));
      }

      _onCleanupCourse.add(id);
      return deleted;
    });
  }

  bool _recordHasAnyTeacher(RecordSnapshot record, List<String> teachersIds) {
    final List<dynamic> recordTeachers = record.value['teachersIds'];
    return recordTeachers.any((e) => teachersIds.contains(e));
  }

  Future<Database> get _db => SembastDatabase.instance.get();
}

Map<String, dynamic> _serializeCourse(AddCourse course) {
  if (course == null) return null;

  return {
    'schoolYearId': course.schoolYearId,
    'name': course.name,
    'color': course.color?.tag,
    'room': course.room,
    'teachersIds': SembastSerializer.serializeStringListField(course.teachersIds),
    'creditHours': course.creditHours,
    'note': course.note,
    'updatedAt': SembastSerializer.serializeDateTimeField(course.updatedAt),
  };
}

AddCourse _deserializeCourse(Map<String, dynamic> map) {
  if (map == null) return null;

  return AddCourse(
    schoolYearId: map['schoolYearId'],
    name: map['name'],
    color: ColorEnumExt.forTag(map['color']),
    room: map['room'],
    teachersIds: SembastSerializer.deserializeStringListField(map['teachersIds']),
    creditHours: map['creditHours'],
    note: map['note'],
    updatedAt: SembastSerializer.deserializeDateTimeField(map['updatedAt']),
  );
}
