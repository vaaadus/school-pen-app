import 'package:school_pen/domain/course/model/add_course.dart';
import 'package:school_pen/domain/course/model/course.dart';

abstract class CourseRepository {
  /// Stream changes whenever course repository changes.
  Stream<CourseRepository> get onChange;

  /// Stream ids of deleted courses to cleanup dependencies.
  Stream<String> get onCleanupCourse;

  /// Returns all stored courses.
  /// Can be filtered by [schoolYearId], null means no filter.
  /// Can be filtered by [teachersIds], null means no filter.
  Future<List<Course>> getAll({String schoolYearId, List<String> teachersIds});

  /// Returns the course by ID or null if it doesn't exist.
  Future<Course> getById(String id);

  /// Returns the list of ids of deleted courses.
  Future<List<String>> getDeletedIds();

  /// Creates a new course and returns its ID.
  Future<String> create(AddCourse course);

  /// Updates or creates a new course with ID.
  /// Returns true if updated, false otherwise.
  Future<bool> update(String id, AddCourse course);

  /// Removes a course with ID.
  /// Returns true if deleted, false otherwise.
  Future<bool> delete(String id);
}
