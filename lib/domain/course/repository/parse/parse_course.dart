import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/course/model/add_course.dart';
import 'package:school_pen/domain/course/model/course.dart';

class ParseCourse extends ParseObject implements ParseCloneable {
  static const String keyClassName = 'Course';
  static const String keyUuid = 'uuid';
  static const String keyUser = 'user';
  static const String keySchoolYearId = 'schoolYearUuid';
  static const String keyName = 'name';
  static const String keyColor = 'color';
  static const String keyRoom = 'room';
  static const String keyTeachers = 'teachersUuids';
  static const String keyCreditHours = 'creditHours';
  static const String keyNote = 'note';

  ParseCourse() : super(keyClassName);

  ParseCourse.clone() : this();

  factory ParseCourse.fromModel(Course course, ParseUser user) => ParseCourse()
    ..user = user
    ..uuid = course.id
    ..schoolYearId = course.schoolYearId
    ..name = course.name
    ..color = course.color
    ..room = course.room
    ..teachers = course.teachersIds
    ..creditHours = course.creditHours
    ..note = course.note
    ..updatedAt = course.updatedAt;

  AddCourse toAddCourse() => AddCourse(
        schoolYearId: schoolYearId,
        name: name,
        color: color,
        room: room,
        teachersIds: teachers,
        creditHours: creditHours,
        note: note,
        updatedAt: updatedAt,
      );

  @override
  ParseCourse clone(Map map) => ParseCourse.clone()..fromJson(map);

  set updatedAt(DateTime dateTime) => setNullable(keyVarUpdatedAt, dateTime);

  set user(ParseUser user) => setNullable(keyUser, user);

  String get uuid => getNullable(keyUuid);

  set uuid(String uuid) => setNullable(keyUuid, uuid);

  String get schoolYearId => getNullable(keySchoolYearId);

  set schoolYearId(String id) => setNullable(keySchoolYearId, id);

  String get name => getNullable(keyName);

  set name(String name) => setNullable(keyName, name);

  ColorEnum get color => ColorEnumExt.forTag(getNullable(keyColor));

  set color(ColorEnum color) => setNullable(keyColor, color?.tag);

  String get room => getNullable(keyRoom);

  set room(String room) => setNullable(keyRoom, room);

  List<String> get teachers {
    final List<dynamic> list = getNullable(keyTeachers) ?? [];
    return list.map((e) => e.toString()).toList();
  }

  set teachers(List<String> teachersIds) => setNullable(keyTeachers, teachersIds);

  double get creditHours => getDouble(keyCreditHours);

  set creditHours(double creditHours) => setNullable(keyCreditHours, creditHours);

  String get note => getNullable(keyNote);

  set note(String note) => setNullable(keyNote, note);

  /// Creates or updates a course.
  Future<ParseResponse> store() {
    return ParseCloudFunction('storeCourse').execute(parameters: {
      keyUuid: uuid,
      keySchoolYearId: schoolYearId,
      keyName: name,
      keyColor: color?.tag,
      keyRoom: room,
      keyTeachers: teachers,
      keyCreditHours: creditHours,
      keyNote: note,
    });
  }
}
