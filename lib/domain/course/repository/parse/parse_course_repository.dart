import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:school_pen/domain/common/parse/parse_object_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/model/add_course.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/course/repository/course_repository.dart';
import 'package:school_pen/domain/course/repository/parse/parse_course.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';

class ParseCourseRepository implements CourseRepository, ParseLiveUpdateSubscriber {
  static const Logger _logger = Logger('ParseCourseRepository');
  final CourseRepository _delegate;
  Subscription _subscription;

  ParseCourseRepository(this._delegate);

  @override
  Stream<CourseRepository> get onChange => _delegate.onChange;

  @override
  Stream<String> get onCleanupCourse => _delegate.onCleanupCourse;

  @override
  Future<List<Course>> getAll({String schoolYearId, List<String> teachersIds}) =>
      _delegate.getAll(schoolYearId: schoolYearId, teachersIds: teachersIds);

  @override
  Future<Course> getById(String id) => _delegate.getById(id);

  @override
  Future<List<String>> getDeletedIds() => _delegate.getDeletedIds();

  @override
  Future<String> create(AddCourse course) async {
    final String id = await _delegate.create(course);
    _try(() async {
      final Course newCourse = await _delegate.getById(id);
      await _storeOnParse(newCourse);
    });
    return id;
  }

  @override
  Future<bool> update(String id, AddCourse course) async {
    final bool updated = await _delegate.update(id, course);
    if (updated) {
      _try(() async {
        final Course newCourse = await _delegate.getById(id);
        await _storeOnParse(newCourse);
      });
    }
    return updated;
  }

  @override
  Future<bool> delete(String id) async {
    final bool deleted = await _delegate.delete(id);
    if (deleted) {
      _try(() => _deleteOnParse(id));
    }
    return deleted;
  }

  Future<void> store(Course course) async {
    await _delegate.update(course.id, course.toAddCourse());
  }

  Future<void> drop(String id) async {
    await _delegate.delete(id);
  }

  @override
  Future<void> subscribeLiveUpdates(LiveQuery liveQuery) async {
    _subscription = await liveQuery.client.subscribe(QueryBuilder(ParseCourse()));
    _subscription.on(LiveQueryEvent.create, (ParseCourse object) {
      _try(() => _delegate.update(object.uuid, object.toAddCourse()));
    });
    _subscription.on(LiveQueryEvent.update, (ParseCourse object) {
      _try(() => _delegate.update(object.uuid, object.toAddCourse()));
    });
    _subscription.on(LiveQueryEvent.delete, (ParseCourse object) {
      _try(() => _delegate.delete(object.uuid));
    });
  }

  @override
  Future<void> unsubscribeLiveUpdates(LiveQuery liveQuery) async {
    if (_subscription != null) liveQuery.client.unSubscribe(_subscription);
    _subscription = null;
  }

  Future<void> _storeOnParse(Course course) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseCourse.fromModel(course, user).store();
  }

  Future<void> _deleteOnParse(String id) async {
    final ParseUser user = await ParseUser.currentUser();
    if (user == null) return;

    await ParseCourse().deleteByUuid(id);
  }

  void _try(VoidFutureBuilder builder) async {
    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }
}
