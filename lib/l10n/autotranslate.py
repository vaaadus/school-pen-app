#!/bin/bash

import os.path
import requests
import json
import html

BASE_LOCALE = 'en'
LOCALES = ['en', 'es', 'de', 'fr', 'pl', 'zh']
API_KEY = 'AIzaSyAL2LtEOzrhzE67aWXJMC6mmNaTn5HHeVg'
LOCALE_KEY = '@@locale'


def readTranslationsFrom(file):
    if not os.path.isfile(file):
        return {}

    with open(file) as json_file:
        return json.load(json_file)


def storeTranslationsOf(locale, translations):
    with open(buildFileName(locale), 'w') as file:
        data = json.dumps(translations, ensure_ascii=False, indent=3)
        file.write(data)

def autotranslate(baseTranslations, sourceLocale, targetLocale):
    overrideTranslations = readTranslationsFrom(
        buildOverrideFileName(targetLocale))
    currentTranslations = readTranslationsFrom(buildFileName(targetLocale))

    result = {}
    result[LOCALE_KEY] = targetLocale

    totalKeys = 0
    machineTranslatedKeys = 0
    overriddenKeys = 0
    alreadyTranslatedKeys = 0

    for key in baseTranslations:
        if key == LOCALE_KEY:
            continue

        totalKeys += 1
        if key in overrideTranslations:
            result[key] = overrideTranslations[key]
            overriddenKeys += 1
        elif key in currentTranslations:
            result[key] = currentTranslations[key]
            alreadyTranslatedKeys += 1
        else:
            result[key] = translate(
                sourceLocale, targetLocale, baseTranslations[key])
            machineTranslatedKeys += 1

    print(targetLocale + ': machine-translated: ' + str(machineTranslatedKeys) + ', overridden: ' +
          str(overriddenKeys) + ', already translated: ' + str(alreadyTranslatedKeys) + ', total: ' + str(totalKeys))

    return result


def translate(sourceLocale, targetLocale, text):
    params = {
        'key': API_KEY,
        'source': sourceLocale,
        'target': targetLocale,
        'q': text
    }

    req = requests.post(
        'https://translation.googleapis.com/language/translate/v2', params=params)

    translation = req.json()['data']['translations'][0]['translatedText']
    translation = html.unescape(translation)
    return translation


def buildFileName(locale):
    return 'intl_' + locale + '.arb'


def buildOverrideFileName(locale):
    return 'overrides/override_' + locale + '.arb'


baseTranslations = readTranslationsFrom(buildOverrideFileName(BASE_LOCALE))
for locale in LOCALES:
    storeTranslationsOf(locale, autotranslate(
        baseTranslations, BASE_LOCALE, locale))
