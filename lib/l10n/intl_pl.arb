{
   "@@locale": "pl",
   "app_name": "School Pen",
   "nav_home": "Główna",
   "nav_courses": "Przedmioty",
   "nav_notes": "Uwagi",
   "nav_timetable": "Plan lekcji",
   "nav_attendance": "Frekwencja",
   "nav_people": "Ludzie",
   "nav_files": "Pliki",
   "nav_holidays": "Wakacje",
   "nav_search": "Szukaj",
   "nav_settings": "Ustawienia",
   "common_more": "Więcej",
   "common_copy": "Kopiuj",
   "common_options": "Opcje",
   "common_overdue": "Spóźnione",
   "common_less": "Mniej",
   "common_avg": "Śr.",
   "common_search": "Szukaj",
   "common_account": "Konto",
   "common_premium": "Premium",
   "common_syncAndRestore": "Synchronizuj i przywracaj",
   "common_pcOrWebVersion": "Wersja komputerowa / internetowa",
   "common_selectYourSchool": "Wybierz swoją szkołę",
   "common_selectYourTimetable": "Wybierz swój plan",
   "common_schoolYears": "Szkolne lata",
   "common_newSchoolYear": "Nowy rok",
   "common_editSchoolYear": "Edytuj rok szkolny",
   "common_students": "Studenci",
   "common_custom": "Niestandardowy",
   "common_unknown": "Nieznany",
   "common_create": "Stwórz",
   "common_subscribe": "Subskrybuj",
   "common_unsubscribe": "Odsubskrybuj",
   "common_name": "Nazwa",
   "common_newName": "Nowa nazwa",
   "common_duplicate": "Duplikuj",
   "common_noCategory": "Bez kategorii",
   "common_noLessons": "Brak lekcji",
   "common_noGradesAdded": "Brak dodanych ocen",
   "common_appearance": "Wygląd",
   "common_theme_systemLight": "System (jasny)",
   "common_theme_systemDark": "System (ciemny)",
   "common_theme_dark": "Ciemny",
   "common_theme_light": "Jasny",
   "common_theme_black": "Czarny",
   "common_theme_amber": "Bursztyn",
   "common_theme_navyBlue": "Ciemnoniebieski",
   "common_theme_red": "Czerwony",
   "common_theme_green": "Zielony",
   "common_privacy": "Prywatność",
   "common_support": "Wsparcie",
   "common_integrations": "Integracje",
   "common_rateUs": "Oceń nas",
   "common_share": "Udostępnij",
   "common_aboutApp": "O aplikacji",
   "common_anonymousUser": "Anonimowy użytkownik",
   "common_register": "Zarejestruj się",
   "common_login": "Zaloguj się",
   "common_username": "Nazwa użytkownika",
   "common_email": "E-mail",
   "common_phone": "Telefon",
   "common_website": "Strona internetowa",
   "common_address": "Adres",
   "common_officeHours": "Godziny pracy",
   "common_addOfficeHours": "Dodaj godziny pracy",
   "common_addCourse": "Dodaj przedmiot",
   "common_course": "Przedmiot",
   "common_courses": "Przedmioty",
   "common_noCourses": "Brak przedmiotów",
   "common_goToDate": "Idź do tej pory",
   "common_weekday": "Dzień",
   "common_previousPage": "Poprzednia strona",
   "common_nextPage": "Następna strona",
   "common_password": "Hasło",
   "common_oldPassword": "Stare hasło",
   "common_newPassword": "Nowe hasło",
   "common_repeatPassword": "Potwierdź hasło",
   "common_createAccount": "Utwórz konto",
   "common_forgotPassword": "Nie pamiętasz hasła?",
   "common_createTimetable": "Utwórz plan",
   "common_duplicateTimetable": "Duplikuj plan",
   "common_renameTimetable": "Zmień nazwę planu",
   "common_deleteTimetable": "Usuń plan",
   "common_subscribeOtherTimetable": "Zasubskrybuj inny plan lekcji",
   "common_unsubscribeFromTimetable": "Odsubskrybuj ten plan lekcji",
   "common_never": "Nigdy",
   "common_send": "Wyślij",
   "common_cancel": "Anuluj",
   "common_ok": "Ok",
   "common_apply": "Zapisz",
   "common_uploadFile": "Przesyłanie pliku",
   "common_emailNotSet": "Adres e-mail nie jest ustawiony",
   "common_change": "Zmiana",
   "common_changeAvatar": "Zmień awatar",
   "common_changePassword": "Zmień hasło",
   "common_profile": "Profil",
   "common_account_connections": "Znajomości",
   "common_account_apple": "Apple ID",
   "common_account_facebook": "Facebook",
   "common_account_google": "Google+",
   "common_account_signInWith_apple": "Zaloguj się przez Apple",
   "common_account_signInWith_facebook": "Zaloguj się przez Facebook",
   "common_account_signInWith_google": "Zaloguj się przez Google",
   "common_account_link": "Połącz",
   "common_account_unlink": "Odłącz",
   "common_save": "Zapisz",
   "common_discardChangesQuestion": "Odrzucić zmiany?",
   "common_keepEditing": "Edytuj",
   "common_discard": "Odrzuć",
   "common_logout": "Wyloguj",
   "common_archive": "Archiwizuj",
   "common_archived": "Zaarchiwizowane",
   "common_unarchive": "Przywróć z archiwum",
   "common_personalizedAds": "Spersonalizowane reklamy",
   "common_diagnosis": "Diagnostyka",
   "common_analytics": "Analityka",
   "common_gridView": "Widok siatki",
   "common_listView": "Widok listy",
   "common_noInternetConnection": "Brak połączenia z internetem",
   "common_pleaseRestoreInternetConnection": "Przed kontynuowaniem przywróć połączenie internetowe.",
   "common_deleteAccount": "Usuń konto",
   "common_credits": "Podziękowania",
   "common_licenses": "Licencje",
   "common_filter": "Filtr",
   "common_sort": "Sortować",
   "common_sortBy": "Sortuj według",
   "common_sortBy_latest": "Najnowszy",
   "common_sortBy_oldest": "Najstarszy",
   "common_sortBy_highest": "Najwyższe",
   "common_sortBy_lowest": "Najniższe",
   "common_error": "Błąd",
   "common_refresh": "Odśwież",
   "common_noInternet_message": "Brak połączenia z internetem. Sprawdź, czy Twoje urządzenie jest połączone z Wi-Fi lub aktywuj mobilny transfer danych.",
   "common_somethingWentWrong": "Coś poszło nie tak",
   "common_operationFailedTryAgainLater": "Operacja nie powiodła się. Spróbuj ponownie później.",
   "confirmation_accountConnected": "Konto połączone!",
   "confirmation_accountDisconnected": "Konto odłączone!",
   "confirmation_passwordChanged": "Hasło zostało zmienione!",
   "confirmation_themeUnlocked": "Wygląd zmieniony!",
   "confirmation_saveSuccess": "Zapisano!",
   "confirmation_deleted": "Usunięto!",
   "confirmation_archived": "Zarchiwizowano!",
   "confirmation_unarchived": "Przywrócono z archiwum!",
   "confirmation_subscribed": "Zasubskrybowano!",
   "confirmation_unsubscribed": "Anulowano subskrypcję!",
   "color_red": "Czerwony",
   "color_orange": "Pomarańczowy",
   "color_yellow": "Żółty",
   "color_green": "Zielony",
   "color_teal": "Morski",
   "color_blue": "Niebieski",
   "color_lightBlue": "Jasny niebieski",
   "color_purple": "Fioletowy",
   "color_pink": "Różowy",
   "color_black": "Czarny",
   "common_category": "Kategoria",
   "common_gradeCategory": "Kategoria oceny",
   "common_gradeAverage": "Średnia ocen",
   "common_times": "Powtórzeń",
   "common_until": "Aż do",
   "common_timetable": "Plan lekcji",
   "common_agenda": "Agenda",
   "common_timeOfDay": "Godzina",
   "common_everyDay": "Każdy dzień",
   "common_daysOfWeek": "Dni tygodnia",
   "common_avatar": "Awatar",
   "common_gallery": "Galeria",
   "common_takePhoto": "Zrób zdjęcie",
   "common_deleteCurrentPhoto": "Usuń bieżące zdjęcie",
   "common_saveRecurringEvent": "Zapisz wydarzenie cykliczne",
   "common_deleteRecurringEvent": "Usuń wydarzenie cykliczne",
   "repeatOptions_everyDay": "Codziennie",
   "repeatOptions_everyNDays": "Co %s dni",
   "repeatOptions_everyWeek": "Co tydzień",
   "repeatOptions_everyNWeeks": "Co %s tygodni",
   "repeatOptions_everyMonth": "Każdego miesiąca",
   "repeatOptions_everyNMonths": "Co %s miesięcy",
   "repeatOptions_doesNotRepeat": "Nie powtarza się",
   "repeatOptions_custom": "Niestandardowe",
   "recurringEvent_thisEvent": "To wydarzenie",
   "recurringEvent_thisAndFollowingEvents": "To i kolejne wydarzenia",
   "recurringEvent_allEvents": "Wszystkie zdarzenia",
   "repeatOptionsPage_repeatsEvery": "Powtarza się co",
   "repeatOptionsPage_repeatsOn": "Powtarza się",
   "repeatOptionsPage_ends": "Kończy się",
   "repeatOptionsPage_onDate": "Na",
   "repeatOptionsPage_day": "dzień",
   "repeatOptionsPage_days": "dni",
   "repeatOptionsPage_week": "tydzień",
   "repeatOptionsPage_weeks": "tygodni",
   "repeatOptionsPage_month": "miesiąc",
   "repeatOptionsPage_months": "miesięcy",
   "repeatOptionsPage_afterOneOccurrence": "Po %s wystąpieniu",
   "repeatOptionsPage_afterNOccurrences": "Po %s wystąpieniach",
   "loginOptions_loginFailed_title": "Logowanie nie powiodło się!",
   "loginOptions_loginFailed_message": "Coś poszło nie tak. Próba logowania nie powiodła się. Spróbuj ponownie później.",
   "login_header": "Witamy ponownie!",
   "login_forgotPassword_header": "Zapomniałeś hasła",
   "login_forgotPassword_message": "Nie martw się. Podaj swój adres e-mail, a wyślemy Ci instrukcje resetowania hasła.",
   "login_resetPassword_header": "Wysłano instrukcje!",
   "login_resetPassword_message": "Sprawdź skrzynkę odbiorczą i folder spamu, aby uzyskać instrukcje zmiany hasła.",
   "profile_logout_message": "Czy na pewno chcesz się wylogować? Twoje dane nie będą już synchronizowane na tym urządzeniu.",
   "profile_deleteAccount_message": "Czy na pewno chcesz usunąć swoje konto? Twoje dane nie będą już synchronizowane. Po usunięciu konta usuniemy wszystkie Twoje dane przechowywane w chmurze. Nie wpłynie to na Twoje dane lokalne na urządzeniu.",
   "settings_account_label": "Zarządzaj swoim profilem",
   "settings_accountPlaceholder_message": "Załóż konto, to nic nie kosztuje!",
   "settings_accountPlaceholder_button": "Utwórz konto lub zaloguj się",
   "settings_premium_label": "Bez reklam. Żadnych przerw",
   "settings_pcOrWeb_label": "Wypróbuj School Pen na inne platformy",
   "settings_syncAndRestore_label": "Synchronizuj dane na wielu urządzeniach",
   "settings_schoolYears_label": "Nowy rok szkolny",
   "settings_semesters_label": "Podziel swój rok szkolny na części",
   "settings_notifications_label": "Nie przegap ważnych wydarzeń",
   "settings_privacy_label": "Zdecyduj, ile nam udostępniasz",
   "settings_support_label": "Gdzie uzyskać pomoc",
   "settings_integrations_label": "Połącz aplikacje innych firm",
   "settings_rateUs_label": "Jeśli podoba Ci się aplikacja, pomóż nam, wystawiając opinię w sklepie",
   "settings_share_label": "Zaproś znajomych do korzystania z aplikacji",
   "settings_about_label": "Wersja %s",
   "settings_themes_unlockByAdsOrGoPremium": "Odblokuj dodatkowe motywy, oglądając reklamy lub przejdź do premium.",
   "settings_themes_adFailedToLoad_message": "Nie udało się załadować reklamy z nagrodą. Spróbuj ponownie później",
   "settings_privacy_analytics_description": "Ta opcja pozwala nam gromadzić i przetwarzać informacje o tym, jak korzystasz z naszej aplikacji. Używamy tych danych do celów analitycznych, aby zrozumieć naszych użytkowników i ulepszyć aplikację.",
   "settings_privacy_diagnosis_description": "Ta opcja umożliwia nam zbieranie i przetwarzanie anonimowych raportów o awariach. Używamy ich, aby poprawić stabilność i ogólne wrażenia użytkownika.",
   "settings_privacy_personalizedAds_description": "Ta opcja umożliwia nam i naszym partnerom reklamowym zbieranie pewnych informacji o Tobie w celu wyświetlania reklam, które są dla Ciebie odpowiednie.",
   "settings_schoolYears_description": "Lata szkolne pomagają podzielić szkolne rzeczy na części, abyś mógł zarządzać mniejszymi częściami. Np. Możesz mieć oddzielne notatki lub przedmioty na każdy rok szkolny.",
   "settings_schoolYears_deleteDialog_message": "Czy na pewno chcesz usunąć ten rok szkolny? Usunięcie go spowoduje również usunięcie wszystkich powiązanych danych, w tym semestrów, notatek i przedmiotów z tego roku.",
   "settings_schoolYears_setAsCurrent": "Ustaw jako bieżący rok",
   "settings_semesters_description": "Semestry pomagają podzielić rok szkolny na części, dzięki czemu można zarządzać jego mniejszymi częściami. Wyobraź sobie semestr letni i semestr zimowy, czy to nie byłoby fajne?",
   "settings_semesters_setAsCurrent": "Ustaw jako bieżący semestr",
   "settings_semesters_deleteDialog_message": "Czy na pewno chcesz usunąć ten semestr? Usunięcie go spowoduje również usunięcie wszystkich powiązanych danych, w tym obecności i ocen z tego semestru.",
   "settings_notifications_notifyIfNoEvents": "Powiadom nawet jeśli nie ma nic zaplanowanego",
   "settings_notifications_doNotNotifyIfNoEvents": "Nie powiadamiaj jeśli nie ma nic zaplanowanego",
   "settings_notifications_reminderAboutUpcomingLessons": "Powiadomienia o lekcjach",
   "settings_notifications_whenLessonStarts": "W momencie rozpoczęcia lekcji",
   "settings_notifications_minutesBefore": "%s minut przed",
   "notes_emptyState_message": "Notatki pomagają uporządkować myśli! Stwórz swoją pierwszą notatkę.",
   "notes_filter_showArchivedNotes": "Pokaż zarchiwizowane notatki",
   "notes_deleteDialog_message": "Czy na pewno usunąć tę notatkę? Nie będziesz w stanie jej przywrócić.",
   "exams_deleteDialog_message": "Czy na pewno usunąć ten test? Nie będziesz w stanie go przywrócić.",
   "homework_deleteDialog_message": "Czy na pewno usunąć to zadanie? Nie będziesz w stanie go przywrócić.",
   "reminders_deleteDialog_message": "Czy na pewno usunąć to przypomnienie? Nie będziesz w stanie go przywrócić.",
   "teachers_emptyState_message": "Nie znaleziono nauczycieli. Dodaj swojego pierwszego nauczyciela za pomocą przycisku.",
   "teachers_deleteDialog_message": "Czy na pewno chcesz usunąć tego nauczyciela? Nie będziesz w stanie go przywrócić.",
   "friends_emptyState_message": "Nie znaleziono znajomych. Dodaj swojego pierwszego znajomego za pomocą przycisku.",
   "friends_deleteDialog_message": "Czy na pewno chcesz usunąć tego znajomego? Nie będziesz w stanie go przywrócić.",
   "friends_invitationFromUser_message": "%s zaprosił Cię do grona przyjaciół. Zaakceptuj lub odrzuć zaproszenie.",
   "friends_inviteDialog_message": "Wprowadź adres e-mail znajomego, aby wysłać zaproszenie.",
   "courses_gradingSystemSet_message": "System oceniania ustawiony automatycznie na %s",
   "courses_emptyState_message": "Dodaj swój pierwszy przedmiot, aby śledzić swoje oceny, ustawić plan lekcji i przesłać pliki.",
   "courses_deleteDialog_message": "Czy na pewno chcesz usunąć ten przedmiot? Wszystkie powiązane oceny, lekcje i pliki również zostaną usunięte.",
   "courses_gradeObjectiveDialog_message": "Podaj docelową ocenę, którą chcesz osiągnąć.",
   "courses_activeEventsFormat": "%s/%s aktywne",
   "courses_overdueEventsFormat": "Spoźnione: %s⚠",
   "timetables_emptyState_message": "Plan lekcji pomaga uporządkować Twoje zajęcia.",
   "timetables_emptyState_subscribeMessage": "Zacznij już teraz, tworząc własny plan lekcji lub zapisz się na taki, który jest publicznie dostępny.",
   "timetables_notFoundState_message": "Nie mogliśmy znaleźć Twojego planu lekcji. Może to oznaczać, że został usunięty.",
   "timetables_deleteDialog_message": "Czy na pewno chcesz usunąć ten plan lekcji? Nie będziesz w stanie go przywrócić.",
   "timetable_viewMode_day": "Dzień",
   "timetable_viewMode_threeDays": "Trzy dni",
   "timetable_viewMode_week": "Tydzień",
   "timetable_viewMode_dayView": "Widok dnia",
   "timetable_viewMode_threeDaysView": "Widok z 3 dni",
   "timetable_viewMode_weekView": "Widok tygodnia",
   "gradingSystem_percentage": "Procentowy",
   "gradingSystem_numeric": "Numeryczny",
   "gradingSystem_alphabetic": "Alfabetyczny",
   "gradingSystem_lowestGrade": "Najniższa ocena",
   "gradingSystem_highestGrade": "Najwyższa ocena",
   "gradingSystem_error_lowestGradeMustBeFilled": "Najniższa ocena nie może być pusta.",
   "gradingSystem_error_highestGradeMustBeFilled": "Najwyższa ocena nie może być pusta.",
   "gradingSystem_error_minValueCannotEqualMaxValue": "Najniższa ocena musi być inna niż najwyższa.",
   "grades_unnamed": "(Brak nazwy)",
   "grades_weight_placeholder": "Waga (1,0 = 100%)",
   "grades_weight_short": "w. %",
   "grades_gradeCategoryDialog_message": "Wpisz nazwę kategorii ocen.",
   "grades_deletedDialog_message": "Czy na pewno chcesz usunąć tę ocenę?",
   "gradeCategory_written": "Pisemny",
   "gradeCategory_oral": "Ustny",
   "gradeCategory_practical": "Praktyczny",
   "gradeCategory_addNew": "Dodaj nową kategorię",
   "home_eventsDone": "Wykonano %s z %s wydarzeń",
   "home_emptyState_message": "Brak zaplanowanych wydarzeń. Masz wolny dzień!",
   "home_doneState_message": "Wszystko zrobione!",
   "notifications_emptyState_message": "Kiedy otrzymasz powiadomienie, pojawi się tutaj!",
   "rateApp_title": "Oceń tę aplikację",
   "rateApp_message": "Podoba Ci się School Pen? Może chcesz przekazać cenną opinię, aby ulepszyć aplikację? Nie wahaj się.",
   "error_passwordsDoNotMatch": "Hasła nie pasują do siebie.",
   "error_invalidPassword": "Złe hasło.",
   "error_accountAlreadyLinked": "Z tą usługą jest już połączone inne konto.",
   "error_avatarSizeTooBig": "Maksymalny rozmiar przesyłanego obrazu awatara to 5 MB.",
   "error_passwordCannotContainUsername": "Hasło nie może zawierać nazwy użytkownika.",
   "error_tooManyFailedLoginAttempts": "Zbyt wiele nieudanych prób logowania. Spróbuj ponownie później.",
   "error_usernameTaken": "Ta nazwa użytkownika jest już zajęta.",
   "error_usernameTooShort": "Minimalna długość nazwy użytkownika to %s symboli.",
   "error_mustNotBeEmpty": "Pole nie może być puste.",
   "error_valueMustBeBetween": "Wartość musi zawierać się między %s a %s.",
   "error_valueMustBeOneOf": "Wartość musi być jedną z %s.",
   "error_invalidEmail": "Email jest nieprawidłowy.",
   "error_passwordTooShort": "Minimalna długość hasła to %s symboli.",
   "error_emailTaken": "Konto z podanym adresem e-mail już istnieje.",
   "error_somethingWentWrong": "Coś poszło nie tak.",
   "error_invalidCredentials": "Zły email lub hasło.",
   "error_noInternet": "Brak połączenia z internetem.",
   "error_startDateMustBeBeforeEndDate": "Data rozpoczęcia musi być wcześniejsza niż data zakończenia.",
   "error_atLeastOneValueMustBeSelected": "Należy wybrać co najmniej jedną wartość",
   "common_close": "Zamknij",
   "common_noActiveEvents": "Brak aktywnych zdarzeń",
   "common_recentGrades": "Najnowsze oceny",
   "common_recentFiles": "Ostatnie pliki",
   "common_grades": "Oceny",
   "common_grade": "Ocena",
   "common_gradingSystem": "System oceniania",
   "common_viewMore": "Zobacz więcej",
   "common_viewAll": "Zobacz wszystko",
   "common_addGrade": "Dodaj ocenę",
   "common_addTeacher": "Dodaj nauczyciela",
   "common_addFriend": "Dodaj znajomego",
   "common_monthly": "Miesięczny",
   "common_teachers": "Nauczyciele",
   "common_friends": "Przyjaciele",
   "common_yearly": "Rocznie",
   "common_facebook": "Facebook",
   "common_twitter": "Twitter",
   "common_whatsapp": "WhatsApp",
   "common_instagram": "Instagram",
   "common_otherApps": "Inne aplikacje",
   "common_shareApp": "Udostępnij aplikacje",
   "common_privacyPolicy": "Polityka prywatności",
   "common_active": "Aktywny",
   "common_completed": "Zakończony",
   "common_notCompleted": "Nie ukończono",
   "common_title": "Tytuł",
   "common_homework": "Zadanie domowe",
   "common_allHomework": "Wszystkie zadania",
   "common_addHomework": "Dodaj zadanie",
   "common_reminder": "Przypomnienie",
   "common_reminders": "Przypomnienia",
   "common_allReminders": "Wszystkie przypomnienia",
   "common_addReminder": "Dodaj przypomnienie",
   "common_exam": "Test",
   "common_exams": "Testy",
   "common_allExams": "Wszystkie testy",
   "common_addExam": "Dodaj test",
   "common_note": "Notatka",
   "common_addNote": "Dodaj notatkę",
   "common_events": "Zdarzenia",
   "common_past": "Przeszłe",
   "common_recent": "Niedawne",
   "common_addTitle": "Dodaj tytuł",
   "common_pickGrade": "Wybierz ocenę",
   "common_pickCourse": "Wybierz przedmiot",
   "common_pickColor": "Wybierz kolor",
   "common_pickTeacher": "Wybierz nauczyciela",
   "common_pickCategory": "Wybierz kategorię",
   "common_yourListItem": "Twoja pozycja na liście",
   "common_listItem": "Element listy",
   "common_addListItem": "Dodaj element listy",
   "common_notification": "Powiadomienie",
   "common_addNotification": "Dodaj powiadomienie",
   "common_delete": "Usuń",
   "common_date": "Data",
   "common_startDate": "Data rozpoczęcia",
   "common_endDate": "Data zakończenia",
   "common_begin": "Początek",
   "common_end": "Koniec",
   "common_viewMode": "Tryb widoku",
   "common_semesterTerm": "Semestr",
   "common_semesterTerms": "Semestry",
   "common_addNewSemesterTerm": "Dodaj nowy semestr",
   "common_newSemesterTerm": "Nowy semestr",
   "common_editSemesterTerm": "Edytuj semestr",
   "common_automaticSemesterTermChange": "Automatyczna zmiana semestru",
   "common_today": "Dzisiaj",
   "common_tomorrow": "Jutro",
   "common_yesterday": "Wczoraj",
   "common_inHour": "Za godzinę",
   "common_nextWeek": "W następnym tygodniu",
   "common_pickDate": "Wybierz datę",
   "common_pickDateAndTime": "Wybierz datę i godzinę",
   "common_rightNow": "Teraz",
   "common_laterThisDay": "Później tego dnia",
   "common_done": "Gotowe",
   "common_edit": "Edytuj",
   "common_notifications": "Powiadomienia",
   "common_notifyMe": "Powiadom mnie",
   "common_minutes": "Minut",
   "common_minutesBefore": "Minut przed",
   "common_hours": "Godzin",
   "common_hoursBefore": "Godzin przed",
   "common_days": "Dni",
   "common_daysBefore": "Dni przed",
   "common_weeks": "Tygodni",
   "common_weeksBefore": "Tygodni przed",
   "common_goBack": "Wróć",
   "common_addLesson": "Dodaj lekcję",
   "common_lesson": "Lekcja",
   "common_add": "Dodaj",
   "common_room": "Pokój",
   "common_teacher": "Nauczyciel",
   "common_manage": "Zarządzaj",
   "common_dismiss": "Pomiń",
   "common_creditHours": "Waga przedmiotu",
   "common_signedInToAccount": "Zalogowano na konto: %s",
   "common_signedOutFromAccount": "Wylogowano z konta: %s",
   "common_byOurCompanyName": "przez Maroon Bells Apps",
   "formatter_relativeDate": "%s, %s",
   "shareAppOptions_quote": "Hej, nową aplikacja do mojego planu lekcji. To naprawdę świetne, sprawdź sam!",
   "notificationChannel_note_title": "Notatki",
   "notificationChannel_exam_title": "Testy",
   "notificationChannel_homework_title": "Zadanie domowe",
   "notificationChannel_reminder_title": "Przypomnienia",
   "notificationChannel_agenda_title": "Agenda",
   "notificationChannel_upcomingLessons_title": "Nadchodzące lekcje",
   "notification_agenda_body": "Liczba zaplanowanych wydarzeń na dziś: %s. Kliknij aby zobaczyć więcej.",
   "notification_agenda_bodyEmpty": "Brak zaplanowanych wydarzeń na dzisiaj.",
   "notification_upcomingLessons_body": "%s rozpoczyna się o %s",
   "smartNote_pendingBilling_title": "Twoja płatność jest procesowana",
   "smartNote_pendingBilling_body": "Wykryliśmy, że rozpocząłeś zakup, który wymaga Twojej akcji. Kliknij przycisk, aby zarządzać swoimi zakupami.",
   "smartNote_loginOffer_title": "Logowanie i kopia zapasowa",
   "smartNote_loginOffer_body": "Twórz kopie zapasowe i synchronizuj dane na wielu urządzeniach.",
   "smartNote_missedWork_title": "Masz pracę do odrobienia ⚠",
   "smartNote_missedWork_body": "Niektóre z Twoich wydarzeń są spóźnione. Przeglądaj i kończ swoje wydarzenia, aby być na bieżąco."
}