#!/bin/bash

# auto translate everything
python3 autotranslate.py

# generate mappings
cd ../..
flutter pub global run intl_utils:generate