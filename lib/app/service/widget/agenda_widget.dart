import 'package:agenda_widget/agenda_widget.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/home/agenda_event_mapper.dart';
import 'package:school_pen/domain/agenda/agenda_listener.dart';
import 'package:school_pen/domain/agenda/agenda_manager.dart';
import 'package:school_pen/domain/agenda/model/agenda_event.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/main_common.dart';

/// Allows the native part to fetch agenda data from flutter.
class AgendaWidget implements AgendaListener {
  final AgendaManager _agendaManager;

  AgendaWidget(this._agendaManager) {
    _agendaManager.addAgendaListener(this, notifyOnAttach: false);
  }

  Future<void> initialize() async {
    if (isAgendaWidgetSupported()) {
      await AgendaWidgetPlugin.initialize(
        initializer: _initializer,
        getEvents: () => _getEvents(),
      );
    }
  }

  @override
  void onAgendaChanged(AgendaManager manager) async {
    await AgendaWidgetPlugin.updateWidget();
  }

  Future<List<Map<String, dynamic>>> _getEvents() async {
    final AgendaManager agendaManager = Injection.get();
    final CourseManager courseManager = Injection.get();

    final List<AgendaEvent> events = await agendaManager.getEvents(DateTime.now());
    final List<Course> courses = await courseManager.getAll();

    return events.map((event) {
      final data = AgendaEventDataMapper.map(event, courses, false);
      return {
        'title': data.title,
        'subtitle': data.label,
        'label': data.aside,
        'color': data.thumbnailColor?.value,
        'kind': event.kind.tag,
      };
    }).toList();
  }
}

Future<void> _initializer() async {
  await App.ensureInitialized();
}
