import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/rate_app_dialog.dart';
import 'package:school_pen/app/service/applaunch/app_launch_manager.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification.dart';
import 'package:school_pen/domain/pushnotification/push_notification_listener.dart';
import 'package:school_pen/domain/pushnotification/push_notification_manager.dart';
import 'package:school_pen/domain/rateapp/rate_app_manager.dart';
import 'package:school_pen/domain/user/model/user_meta.dart';
import 'package:school_pen/domain/user/user_login_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

class AppLaunchManagerImpl implements AppLaunchManager, LifecycleListener, PushNotificationListener, UserLoginListener {
  static const Duration _rateAppDelay = Duration(seconds: 5);

  final LifecycleManager _lifecycleManager;
  final PushNotificationManager _pushNotifications;
  final UserManager _userManager;
  final RateAppManager _rateAppManager;

  Timer _rateAppTimer;

  AppLaunchManagerImpl(
    this._lifecycleManager,
    this._pushNotifications,
    this._userManager,
    this._rateAppManager,
  ) {
    _lifecycleManager.addLifecycleListener(this);
    _pushNotifications.addPushNotificationListener(this);
    _userManager.addUserLoginListener(this);
  }

  @override
  void onResume() {
    _rateAppTimer = Timer(_rateAppDelay, _showAppRating);
  }

  @override
  void onPause() {
    _rateAppTimer?.cancel();
    _rateAppTimer = null;
  }

  @override
  Future<void> onPushNotification(PushNotification notification) async {
    _handlePushNotification(notification);
  }

  @override
  void onUserSignedIn(UserMetadata user) {
    final S s = S.current;
    if (s == null || user == null) return;

    showSuccessToast(message: sprintf(s.common_signedInToAccount, [user.displayName]));
  }

  @override
  void onUserSignedOut(UserMetadata user) {
    final S s = S.current;
    if (s == null || user == null) return;

    showInfoToast(message: sprintf(s.common_signedOutFromAccount, [user.displayName]));
  }

  // TODO: implement support for push notifications
  void _handlePushNotification(PushNotification notification) {
    // if (notification.type == PushNotificationType.schoolTimetableChanged) {
    //   _timetableManager.onSchoolTimetableChanged();
    // }
  }

  void _showAppRating() async {
    final BuildContext context = Nav.key.currentContext;
    if (context == null) return;
    if (!isRateAppDialogSupported()) return;

    if (_rateAppManager.shouldRateNow) {
      final bool rated = await showRateAppDialog(context);
      if (rated ?? false) {
        _rateAppManager.onAppRated();
      } else {
        _rateAppManager.onRateAppDismissed();
      }
    }
  }
}
