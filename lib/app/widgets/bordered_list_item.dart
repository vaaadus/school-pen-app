import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/widgets/avatar.dart';
import 'package:school_pen/app/widgets/bordered_container.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/files/model/remote_file.dart';

/// Displays a title, label, aside & thumbnail icon in a list item with a border.
class BorderedListItem extends StatelessWidget {
  final double thumbnailWidth;
  final double thumbnailHeight;
  final BorderedListItemData data;
  final GestureTapCallback onTap;
  final GestureTapCallback onLongPress;

  const BorderedListItem({
    Key key,
    this.thumbnailWidth = 36.0,
    this.thumbnailHeight = 36.0,
    @required this.data,
    this.onTap,
    this.onLongPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BorderedContainer(
      onTap: onTap,
      onLongPress: onLongPress,
      padding: EdgeInsets.symmetric(
        vertical: Dimens.grid_1_5x,
        horizontal: Dimens.grid_2x,
      ),
      child: Row(
        crossAxisAlignment: data.asideAlignment ?? CrossAxisAlignment.center,
        children: [
          _buildThumbnail(context),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: Dimens.grid_2x),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildTitle(context),
                  if (Strings.isNotBlank(data.label))
                    Padding(
                      padding: EdgeInsets.only(top: Dimens.grid_1x),
                      child: _buildLabel(context),
                    ),
                ],
              ),
            ),
          ),
          if (Strings.isNotBlank(data.aside)) _buildAside(context),
        ],
      ),
    );
  }

  Widget _buildThumbnail(BuildContext context) {
    Widget thumbnail;
    if (data.thumbnail != null) {
      thumbnail = data.thumbnail;
    } else if (data.thumbnailAvatar != null) {
      thumbnail = _buildAvatarThumbnail(context);
    } else if (data.thumbnailImageAsset16 != null) {
      thumbnail = _buildImageThumbnail(context);
    } else {
      thumbnail = _buildTextThumbnail(context);
    }

    return Opacity(opacity: data.inactive ? 0.6 : 1.0, child: thumbnail);
  }

  Widget _buildAvatarThumbnail(BuildContext context) {
    return Avatar(file: data.thumbnailAvatar, width: thumbnailWidth, height: thumbnailHeight);
  }

  Widget _buildImageThumbnail(BuildContext context) {
    return ExcludeSemantics(
      child: Container(
        width: thumbnailWidth,
        height: thumbnailHeight,
        decoration: BoxDecoration(
          color: AppColors.applyDarkModeSaturation(context, _getThumbnailColor(context)),
          borderRadius: BorderRadius.all(Radius.circular(thumbnailWidth / 2)),
        ),
        child: Center(
          child: Image.asset(
            data.thumbnailImageAsset16,
            width: 16,
            height: 16,
            color: AppColors.white,
          ),
        ),
      ),
    );
  }

  Widget _buildTextThumbnail(BuildContext context) {
    final String letter = Strings.firstLetter(data.title) ?? ' ';

    return ExcludeSemantics(
      child: Container(
        width: thumbnailWidth,
        height: thumbnailHeight,
        decoration: BoxDecoration(
          color: AppColors.applyDarkModeSaturation(context, _getThumbnailColor(context)),
          borderRadius: BorderRadius.all(Radius.circular(thumbnailWidth / 2)),
        ),
        child: Center(
          child: Text(
            letter,
            style: Theme.of(context).textTheme.headline6.copyWith(color: Colors.white),
          ),
        ),
      ),
    );
  }

  Color _getThumbnailColor(BuildContext context) {
    if (data.thumbnailColor != null) return data.thumbnailColor;

    final String letter = Strings.firstLetter(data.title) ?? ' ';
    return ResourceLocator.letterToColor(context, letter);
  }

  Widget _buildTitle(BuildContext context) {
    return Text(
      data.title,
      style: Theme.of(context).textTheme.bodyText1.copyWith(
            color: _getTitleColor(context),
            decoration: data.inactive ? TextDecoration.lineThrough : TextDecoration.none,
          ),
    );
  }

  Color _getTitleColor(BuildContext context) {
    if (data.inactive) return Theme.of(context).colorScheme.primaryVariant;
    return data.titleColor ?? Theme.of(context).colorScheme.primary;
  }

  Widget _buildLabel(BuildContext context) {
    return Text(
      data.label,
      style: Theme.of(context).textTheme.caption.copyWith(
            color: data.labelColor ?? Theme.of(context).colorScheme.primaryVariant,
            decoration: data.inactive ? TextDecoration.lineThrough : TextDecoration.none,
          ),
    );
  }

  Widget _buildAside(BuildContext context) {
    return Text(
      data.aside,
      style: _getAsideTextStyle(context),
    );
  }

  TextStyle _getAsideTextStyle(BuildContext context) {
    final TextStyle style = data.asideTextStyle ?? Theme.of(context).textTheme.headline6;

    return style.copyWith(
      color: data.inactive ? Theme.of(context).colorScheme.primaryVariant : style.color,
      decoration: data.inactive ? TextDecoration.lineThrough : TextDecoration.none,
    );
  }
}

class BorderedListItemData extends Equatable {
  final Widget thumbnail;
  final RemoteFile thumbnailAvatar;
  final String thumbnailImageAsset16;
  final Color thumbnailColor;
  final String title;
  final Color titleColor;
  final String label;
  final Color labelColor;
  final String aside;
  final TextStyle asideTextStyle;
  final CrossAxisAlignment asideAlignment;
  final bool inactive;

  const BorderedListItemData({
    this.thumbnail,
    this.thumbnailAvatar,
    this.thumbnailImageAsset16,
    this.thumbnailColor,
    @required this.title,
    this.titleColor,
    @required this.label,
    this.labelColor,
    this.aside,
    this.asideTextStyle,
    this.asideAlignment,
    this.inactive = false,
  });

  @override
  List<Object> get props => [
        thumbnail,
        thumbnailAvatar,
        thumbnailImageAsset16,
        thumbnailColor,
        title,
        titleColor,
        label,
        labelColor,
        aside,
        asideTextStyle,
        asideAlignment,
        inactive
      ];
}
