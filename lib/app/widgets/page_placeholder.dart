import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/fill_viewport_scrollview.dart';
import 'package:school_pen/app/widgets/nav_button.dart';

/// A full page widget that presents an image, message and a list of buttons.
/// Optionally can include [NavButton] if [navType] is not null.
class PagePlaceholder extends StatelessWidget {
  final String imageAsset;
  final double imageWidth;
  final double imageHeight;
  final String message;
  final List<Widget> buttons;
  final NavButtonType navType;

  const PagePlaceholder({
    Key key,
    this.navType,
    @required this.imageAsset,
    @required this.imageWidth,
    @required this.imageHeight,
    @required this.message,
    this.buttons,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    final Adaptive adaptive = Adaptive.of(context);

    Widget result = FillViewportScrollView(
      padding: EdgeInsets.symmetric(
        horizontal: paddings.horizontal,
        vertical: Dimens.grid_4x,
      ),
      child: Center(
        child: Container(
          constraints: BoxConstraints(maxWidth: 360),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Spacer(),
              Column(children: [
                Padding(
                  padding: EdgeInsets.only(bottom: Dimens.grid_3x),
                  child: Image.asset(imageAsset, width: imageWidth, height: imageHeight),
                ),
                Text(message, textAlign: TextAlign.center, style: Theme.of(context).textTheme.subtitle1),
              ]),
              if (adaptive.isMobile) Spacer(),
              if (buttons != null && buttons.isNotEmpty)
                Padding(
                  padding: EdgeInsets.only(top: adaptive.isMobile ? Dimens.grid_4x : Dimens.grid_6x),
                  child: buttons[0],
                ),
              if (buttons != null)
                for (int i = 1; i < buttons.length; i++)
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.grid_2x),
                    child: buttons[i],
                  ),
              if (!adaptive.isMobile) Spacer(),
            ],
          ),
        ),
      ),
    );

    if (navType != null) {
      result = Column(children: [
        Align(
          alignment: Alignment.centerLeft,
          child: NavButton(type: navType),
        ),
        Expanded(child: result),
      ]);
    }

    return result;
  }
}
