import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';

/// Draw a horizontal line on a background which represents the progress.
class HorizontalProgressBar extends StatelessWidget {
  static const double _borderRadius = Dimens.grid_0_5x;

  final double progress;

  const HorizontalProgressBar({Key key, @required this.progress})
      : assert(progress >= 0.0 && progress <= 1.0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxHeight: Dimens.grid_1_5x),
      child: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
        return Stack(children: [
          Positioned.fill(child: _buildBackground(context)),
          Positioned(
            left: 0,
            top: 0,
            bottom: 0,
            width: constraints.maxWidth * progress,
            child: _buildForeground(context),
          ),
        ]);
      }),
    );
  }

  Widget _buildBackground(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).dividerColor,
        borderRadius: BorderRadius.all(Radius.circular(_borderRadius)),
      ),
    );
  }

  Widget _buildForeground(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.secondary,
        borderRadius: BorderRadius.all(Radius.circular(_borderRadius)),
      ),
    );
  }
}
