import 'dart:io';

import 'package:flutter/material.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/files/model/remote_file.dart';
import 'package:school_pen/domain/logger/logger.dart';

const Logger _logger = Logger('RemoteImage');

/// An image widget which tries to use get the remote image
/// and store it locally in the filesystem. If the underlying platform
/// does not support filesystem, then network image will be used.
class RemoteImage extends StatefulWidget {
  const RemoteImage({
    Key key,
    @required this.file,
    @required this.width,
    @required this.height,
    this.placeholderBuilder = _defaultPlaceholder,
    this.errorBuilder = _defaultError,
  }) : super(key: key);

  final RemoteFile file;
  final double width;
  final double height;
  final WidgetBuilder placeholderBuilder;
  final WidgetBuilder errorBuilder;

  @override
  _RemoteImageState createState() => _RemoteImageState();
}

class _RemoteImageState extends State<RemoteImage> {
  bool _loaded = false;
  File _file;
  String _url;
  MemoryFile _memoryFile;
  Object _error;

  @override
  void initState() {
    super.initState();
    _tryLoadFile();
  }

  @override
  void didUpdateWidget(RemoteImage oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.file != widget.file) {
      _loaded = false;
      _file = null;
      _url = null;
      _memoryFile = null;
      _error = null;
      _tryLoadFile();
    }
  }

  void _tryLoadFile() async {
    try {
      final File file = await widget.file.store();
      if (file != null) {
        _onDataLoaded(() => _file = file);
        return;
      }

      final String url = await widget.file.share();
      if (url != null) {
        _onDataLoaded(() => _url = url);
        return;
      }

      final MemoryFile memoryFile = await widget.file.download();
      _onDataLoaded(() => _memoryFile = memoryFile);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      _onDataLoaded(() => _error = error);
    }
  }

  void _onDataLoaded(VoidCallback callback) {
    if (!mounted) return;

    setState(() {
      _loaded = true;
      callback();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      height: widget.height,
      child: _buildContent(context),
    );
  }

  Widget _buildContent(BuildContext context) {
    if (!_loaded) return widget.placeholderBuilder(context);
    if (_error != null) return widget.errorBuilder(context);
    if (_file != null) return _buildImageFile(context, _file);
    if (_url != null) return _buildNetworkImage(context, _url);
    if (_memoryFile != null) return _buildMemoryImage(context, _memoryFile);
    throw ArgumentError('One of earlier statements should be true');
  }

  Widget _buildImageFile(BuildContext context, File file) {
    return Image.file(file, width: widget.width, height: widget.height, fit: BoxFit.cover);
  }

  Widget _buildNetworkImage(BuildContext context, String url) {
    return Image.network(url, width: widget.width, height: widget.height, fit: BoxFit.cover);
  }

  Widget _buildMemoryImage(BuildContext context, MemoryFile file) {
    return Image.memory(file.bytes, width: widget.width, height: widget.height, fit: BoxFit.cover);
  }
}

Widget _defaultPlaceholder(BuildContext context) => Container();

Widget _defaultError(BuildContext context) => Container();
