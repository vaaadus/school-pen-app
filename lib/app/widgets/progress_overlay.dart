import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:uuid/uuid.dart';

/// A full screen overlay which renders progress indicator.
class ProgressOverlay extends StatefulWidget {
  const ProgressOverlay({Key key, @required this.child}) : super(key: key);

  final Widget child;

  static ProgressOverlayState of(BuildContext context) {
    return context.findAncestorStateOfType();
  }

  @override
  State<StatefulWidget> createState() => ProgressOverlayState();
}

/// Holds the current state of the overlay.
class ProgressOverlayState extends State<ProgressOverlay> {
  final Set<String> _requestedOverlaysByKeys = {};
  OverlayEntry _overlayEntry;

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  void updateOverlay(BuildContext context, String key, bool isVisible) {
    if (isVisible) {
      _requestedOverlaysByKeys.add(key);
    } else {
      _requestedOverlaysByKeys.remove(key);
    }

    // avoid setState() during build().
    Timer.run(() {
      if (_requestedOverlaysByKeys.isNotEmpty) {
        if (_overlayEntry != null) return; // already shown

        _overlayEntry = _createOverlayEntry(context);
        Overlay.of(context, rootOverlay: true).insert(_overlayEntry);
      } else {
        _overlayEntry?.remove();
        _overlayEntry = null;
      }
    });
  }

  OverlayEntry _createOverlayEntry(BuildContext context) {
    return OverlayEntry(builder: (BuildContext context) {
      return Positioned.fill(child: _ProgressOverlayUI());
    });
  }
}

/// Handles the progress overlay initialization and management.
mixin ProgressOverlayMixin<T extends StatefulWidget> on State<T> {
  final String _progressOverlayKey = Uuid().v4();
  ProgressOverlayState _overlay;

  /// Hide the overlays & clear state.
  @override
  void dispose() {
    _overlay?.updateOverlay(context, _progressOverlayKey, false);
    _overlay = null;
    super.dispose();
  }

  /// Request to show the overlay by the client key.
  void showProgressOverlay() {
    _overlay ??= ProgressOverlay.of(context);
    _overlay.updateOverlay(context, _progressOverlayKey, true);
  }

  /// Request to hide the overlay by the client key.
  /// Note that the overlay might still be visible
  /// if there are other clients who requested to show it.
  void hideProgressOverlay() {
    _overlay?.updateOverlay(context, _progressOverlayKey, false);
  }

  /// See [showProgressOverlay] & [hideProgressOverlay].
  void updateProgressOverlay(bool isVisible) {
    if (isVisible) {
      showProgressOverlay();
    } else {
      hideProgressOverlay();
    }
  }
}

/// Defines the look of the overlay.
class _ProgressOverlayUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.overlayColor,
      child: Center(
        child: SpinKitChasingDots(
          size: 40,
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
    );
  }
}
