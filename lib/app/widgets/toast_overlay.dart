import 'dart:async';
import 'dart:collection';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/anim/animations.dart';
import 'package:school_pen/app/widgets/adaptive.dart';

/// Shows a confirmation toast with the given [message].
void showSuccessToast({@required String message}) => _showToast(
      message: message,
      imageAsset: Images.ic_checkmark_filled_24,
      appearance: _ToastAppearance.success,
    );

/// Shows an error toast with the given [message].
void showErrorToast({@required String message}) => _showToast(
      message: message,
      imageAsset: Images.ic_error_24,
      appearance: _ToastAppearance.error,
    );

/// Shows an information toast with the given [message].
void showInfoToast({@required String message}) => _showToast(
      message: message,
      imageAsset: Images.ic_info_24,
      appearance: _ToastAppearance.info,
    );

void _showToast({
  @required String message,
  @required String imageAsset,
  @required _ToastAppearance appearance,
}) {
  final _ToastConfig config = _ToastConfig(
    imageAsset24: imageAsset,
    message: message,
    appearance: appearance,
    duration: Duration(seconds: 3),
  );

  _toastSubject.add(config);
}

/// A shared stream (subject) of toasts to avoid the need to pass a build context.
final PublishSubject<_ToastConfig> _toastSubject = PublishSubject();

/// A full screen overlay which renders toasts.
class ToastOverlay extends StatefulWidget {
  const ToastOverlay({Key key, @required this.child}) : super(key: key);

  final Widget child;

  static ToastOverlayState of(BuildContext context) {
    return context.findAncestorStateOfType<ToastOverlayState>();
  }

  /// Must be called before it can be used.
  static void init(BuildContext context) {
    ToastOverlay.of(context)._init(context);
  }

  @override
  State<StatefulWidget> createState() => ToastOverlayState();
}

class ToastOverlayState extends State<ToastOverlay> {
  final Queue<_ToastConfig> _toastQueue = Queue();

  OverlayState _overlay;
  OverlayEntry _overlayEntry;
  Timer _nextToastTimer;
  StreamSubscription<_ToastConfig> _toastSubscription;

  void _init(BuildContext context) {
    _overlay ??= Overlay.of(context, rootOverlay: true);
  }

  @override
  void initState() {
    super.initState();
    _toastSubscription = _toastSubject.listen(showToast);
  }

  @override
  void dispose() {
    _toastSubscription?.cancel();
    _toastSubscription = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  void showToast(_ToastConfig config) {
    assert(_overlay != null);

    _toastQueue.add(config);

    // avoid setState() during build().
    Timer.run(() {
      if (_nextToastTimer == null) {
        _popAndShowToastFromQueue(_overlay);
      }
    });
  }

  void _popAndShowToastFromQueue(OverlayState overlay) {
    _dismissShownToast();
    if (_toastQueue.isEmpty) return;

    final _ToastConfig config = _toastQueue.removeFirst();
    _showToast(overlay, config);
  }

  void _showToast(OverlayState overlay, _ToastConfig config) {
    _overlayEntry = _createOverlayEntry(overlay, config);
    overlay.insert(_overlayEntry);

    _nextToastTimer = Timer(
      config.duration,
      () => _popAndShowToastFromQueue(overlay),
    );
  }

  void _dismissShownToast() {
    _overlayEntry?.remove();
    _overlayEntry = null;
    _nextToastTimer?.cancel();
    _nextToastTimer = null;
  }

  OverlayEntry _createOverlayEntry(OverlayState overlay, _ToastConfig config) {
    return OverlayEntry(builder: (BuildContext context) {
      return _positionToast(
        _getPlacement(context),
        _ToastOverlayUI(
          config: config,
          onDismissToast: () => _popAndShowToastFromQueue(overlay),
        ),
      );
    });
  }

  Widget _positionToast(_ToastPlacement placement, Widget toast) {
    switch (placement) {
      case _ToastPlacement.top:
        return Positioned(top: 48, left: 16, right: 16, child: toast);
      case _ToastPlacement.topRight:
        return Positioned(top: 48, right: 40, child: toast);
    }
    throw ArgumentError('Unsupported placement: $placement');
  }

  _ToastPlacement _getPlacement(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    if (adaptive.isMobile || adaptive.isTablet) {
      return _ToastPlacement.top;
    } else {
      return _ToastPlacement.topRight;
    }
  }
}

enum _ToastPlacement { top, topRight }

enum _ToastAppearance { success, info, error }

class _ToastConfig extends Equatable {
  final String imageAsset24;
  final String message;
  final _ToastAppearance appearance;
  final Duration duration;

  const _ToastConfig({
    @required this.imageAsset24,
    @required this.message,
    @required this.appearance,
    @required this.duration,
  });

  @override
  List<Object> get props => [imageAsset24, message, appearance, duration];
}

/// Defines the look & feel of the toast.
class _ToastOverlayUI extends StatefulWidget {
  const _ToastOverlayUI({
    Key key,
    @required this.config,
    @required this.onDismissToast,
  }) : super(key: key);

  final _ToastConfig config;
  final VoidCallback onDismissToast;

  @override
  _ToastOverlayUIState createState() => _ToastOverlayUIState();
}

class _ToastOverlayUIState extends State<_ToastOverlayUI> with SingleTickerProviderStateMixin {
  static const Curve _curve = Curves.easeInOutBack;
  static const Duration _duration = Animations.mediumDuration;

  AnimationController _animationController;
  Animation<Offset> _slideFromTopAnim;
  Animation<double> _fadeAnim;
  Timer _reverseAnimTimer;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: _duration,
    );

    _slideFromTopAnim = _animationController.drive(
      Tween<Offset>(begin: Offset(0.0, -1.0), end: Offset.zero).chain(
        CurveTween(curve: _curve),
      ),
    );

    _fadeAnim = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: _curve,
        reverseCurve: _curve,
      ),
    );

    _reverseAnimTimer = Timer(
      widget.config.duration - _duration,
      () => _animationController.reverse(),
    );

    _animationController.forward();
  }

  @override
  void dispose() {
    _reverseAnimTimer.cancel();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget result = Row(children: [
      Image.asset(
        widget.config.imageAsset24,
        width: 24,
        height: 24,
        color: _getForegroundColor(context),
        excludeFromSemantics: true,
      ),
      Expanded(
        child: Padding(
          padding: EdgeInsets.only(left: Dimens.grid_2x),
          child: Text(
            widget.config.message,
            style: Theme.of(context).textTheme.headline6.copyWith(color: _getForegroundColor(context)),
          ),
        ),
      ),
    ]);

    result = GestureDetector(
      onTap: widget.onDismissToast,
      child: Center(
        child: IntrinsicWidth(
          child: Card(
            color: _getBackgroundColor(context),
            child: Padding(
              padding: EdgeInsets.all(Dimens.grid_2x),
              child: result,
            ),
          ),
        ),
      ),
    );

    result = SlideTransition(
      position: _slideFromTopAnim,
      child: FadeTransition(
        opacity: _fadeAnim,
        child: result,
      ),
    );

    return result;
  }

  Color _getForegroundColor(BuildContext context) {
    switch (widget.config.appearance) {
      case _ToastAppearance.success:
        return Theme.of(context).colorScheme.onSuccess(context);
      case _ToastAppearance.info:
        return Theme.of(context).colorScheme.onSecondary;
      case _ToastAppearance.error:
        return Theme.of(context).colorScheme.onError;
    }
    throw ArgumentError('Unsupported appearance: ${widget.config.appearance}');
  }

  Color _getBackgroundColor(BuildContext context) {
    switch (widget.config.appearance) {
      case _ToastAppearance.success:
        return Theme.of(context).colorScheme.success(context);
      case _ToastAppearance.info:
        return Theme.of(context).colorScheme.secondaryVariant;
      case _ToastAppearance.error:
        return Theme.of(context).colorScheme.error;
    }
    throw ArgumentError('Unsupported appearance: ${widget.config.appearance}');
  }
}
