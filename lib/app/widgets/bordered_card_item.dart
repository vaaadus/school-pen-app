import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/widgets/avatar.dart';
import 'package:school_pen/app/widgets/bordered_container.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/files/model/remote_file.dart';

/// Displays a thumbnail, title & label on a card.
class BorderedCardItem extends StatelessWidget {
  final double thumbnailWidth;
  final double thumbnailHeight;
  final BorderedCardItemData data;
  final GestureTapCallback onTap;

  const BorderedCardItem({
    Key key,
    this.thumbnailWidth = 40.0,
    this.thumbnailHeight = 40.0,
    @required this.data,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BorderedContainer(
      onTap: onTap,
      padding: EdgeInsets.only(
        top: Dimens.grid_2x,
        bottom: Dimens.grid_1_5x,
        left: Dimens.grid_1_5x,
        right: Dimens.grid_1_5x,
      ),
      child: Column(
        children: [
          _buildThumbnail(context),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_1_5x),
            child: _buildTitle(context),
          ),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_1x),
            child: _buildLabel(context),
          ),
        ],
      ),
    );
  }

  Widget _buildThumbnail(BuildContext context) {
    if (data.avatar != null) {
      return _buildAvatarThumbnail(context);
    } else {
      return _buildTextThumbnail(context);
    }
  }

  Widget _buildAvatarThumbnail(BuildContext context) {
    return Avatar(file: data.avatar, width: thumbnailWidth, height: thumbnailHeight);
  }

  Widget _buildTextThumbnail(BuildContext context) {
    final String letter = Strings.firstLetter(data.title) ?? ' ';

    return ExcludeSemantics(
      child: Container(
        width: thumbnailWidth,
        height: thumbnailHeight,
        decoration: BoxDecoration(
          color: AppColors.applyDarkModeSaturation(
              context, data.thumbnailColor ?? ResourceLocator.letterToColor(context, letter)),
          borderRadius: BorderRadius.all(Radius.circular(thumbnailWidth / 2)),
        ),
        child: Center(
          child: Text(
            letter,
            style: Theme.of(context).textTheme.headline6.copyWith(color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Text(
      data.title,
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.bodyText1,
    );
  }

  Widget _buildLabel(BuildContext context) {
    return Text(
      data.label,
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.caption.copyWith(
            color: data.labelColor ?? Theme.of(context).colorScheme.primaryVariant,
          ),
    );
  }
}

class BorderedCardItemData extends Equatable {
  final RemoteFile avatar;
  final Color thumbnailColor;
  final String title;
  final String label;
  final Color labelColor;

  const BorderedCardItemData({
    this.avatar,
    this.thumbnailColor,
    @required this.title,
    @required this.label,
    this.labelColor,
  });

  @override
  List<Object> get props => [avatar, thumbnailColor, title, label, labelColor];
}
