import 'package:diffutil_dart/diffutil.dart' as diffutil;
import 'package:flutter/material.dart';

typedef ListItemBuilder<T> = Widget Function(BuildContext context, List<T> items, int index);
typedef AnimatedChildBuilder = Widget Function(BuildContext context, Animation<double> animation, Widget child);
typedef AnimatedListItemBuilder = Widget Function(BuildContext context, int index, Animation<double> animation);
typedef EqualityChecker<T> = bool Function(T first, T second);

/// A widget that wraps [AnimatedList] and integrates diff util
/// functionality which animates item inserts & removals.
class AnimatedDiffUtilList<T> extends StatefulWidget {
  final List<T> items;
  final EdgeInsets padding;
  final bool shrinkWrap;
  final bool primary;
  final ListItemBuilder<T> itemBuilder;
  final AnimatedChildBuilder animationBuilder;
  final EqualityChecker<T> equalityChecker;

  const AnimatedDiffUtilList({
    Key key,
    @required this.items,
    @required this.itemBuilder,
    this.animationBuilder = _buildFadeResizeAnimation,
    this.equalityChecker = _defaultEquality,
    this.padding,
    this.shrinkWrap = false,
    this.primary = false,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AnimatedDiffUtilListState<T>();
}

class _AnimatedDiffUtilListState<T> extends State<AnimatedDiffUtilList<T>> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();

  @override
  void didUpdateWidget(AnimatedDiffUtilList oldWidget) {
    super.didUpdateWidget(oldWidget);

    final diffUpdates = diffutil
        .calculateListDiff<T>(oldWidget.items, widget.items, equalityChecker: widget.equalityChecker)
        .getUpdates();

    final _DispatchChangesCallback callback = _DispatchChangesCallback(
      updates: diffUpdates,
      listKey: _listKey,
      itemBuilder: (BuildContext context, int index, Animation<double> animation) {
        final Widget child = widget.itemBuilder(context, oldWidget.items, index);
        return widget.animationBuilder(context, animation, child);
      },
    );

    callback.dispatch();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedList(
      key: _listKey,
      padding: widget.items.isNotEmpty ? widget.padding : null,
      primary: widget.primary,
      shrinkWrap: widget.shrinkWrap,
      initialItemCount: widget.items.length,
      itemBuilder: (BuildContext context, int index, Animation<double> animation) {
        final Widget child = widget.itemBuilder(context, widget.items, index);
        return widget.animationBuilder(context, animation, child);
      },
    );
  }
}

Widget _buildFadeResizeAnimation(BuildContext context, Animation<double> animation, Widget child) {
  return FadeTransition(
    opacity: animation,
    child: SizeTransition(
      axis: Axis.vertical,
      sizeFactor: animation,
      child: child,
    ),
  );
}

bool _defaultEquality(dynamic first, dynamic second) {
  return first == second;
}

class _DispatchChangesCallback {
  final GlobalKey<AnimatedListState> listKey;
  final AnimatedListItemBuilder itemBuilder;
  final Iterable<diffutil.DiffUpdate> updates;

  const _DispatchChangesCallback({
    @required this.listKey,
    @required this.itemBuilder,
    @required this.updates,
  });

  void dispatch() {
    for (diffutil.DiffUpdate update in updates) {
      update.when(
        insert: (int position, int count) {
          listKey.currentState?.insertItem(position);
        },
        move: (int fromPosition, int toPosition) {
          listKey.currentState?.removeItem(fromPosition, (BuildContext context, Animation<double> animation) {
            return itemBuilder(context, fromPosition, animation);
          });

          listKey.currentState?.insertItem(toPosition);
        },
        change: (int position, Object object) {
          // do nothing
        },
        remove: (int position, int count) {
          listKey.currentState?.removeItem(position, (BuildContext context, Animation<double> animation) {
            return itemBuilder(context, position, animation);
          });
        },
      );
    }
  }
}
