import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/shadows.dart';
import 'package:school_pen/generated/l10n.dart';

import 'button.dart';

/// A widget which asks the user either to save or to discard the data.
class SaveOrDiscardFooter extends StatelessWidget {
  const SaveOrDiscardFooter({
    Key key,
    this.saveText,
    this.discardText,
    this.label = '',
    @required this.onSave,
    this.onDiscard,
  }) : super(key: key);

  final String saveText;
  final String discardText;
  final String label;
  final GestureTapCallback onSave;
  final GestureTapCallback onDiscard;

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);

    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: paddings.horizontal,
        vertical: Dimens.grid_1_5x,
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
        boxShadow: [Shadows.topMedium],
      ),
      child: Row(children: [
        Expanded(child: Text(label, style: Theme.of(context).textTheme.headline6)),
        if (onDiscard != null)
          SmallMaterialButton(
            child: SmallButtonText(
              discardText ?? S.of(context).common_discard,
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
            onPressed: onDiscard,
          ),
        SmallRaisedButton(
          child: SmallButtonText(saveText ?? S.of(context).common_save),
          onPressed: onSave,
        ),
      ]),
    );
  }
}
