import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// A [Dialog] widget which sizes itself to the
/// height of the children with width constraint.
///
/// The dialog is scrollable so the child can be any height it wants.
class SizedDialog extends StatelessWidget {
  const SizedDialog({
    Key key,
    @required this.child,
    this.padding,
  }) : super(key: key);

  final Widget child;
  final EdgeInsets padding;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      clipBehavior: Clip.antiAlias,
      child: Container(
        constraints: BoxConstraints(maxWidth: 320),
        child: IntrinsicHeight(
          child: SingleChildScrollView(
            padding: padding,
            child: child,
          ),
        ),
      ),
    );
  }
}
