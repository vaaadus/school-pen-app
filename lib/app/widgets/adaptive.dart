import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

/// A helper class which handles responsiveness & decides which screen size factor can be used.
class Adaptive extends Equatable {
  static Size _cachedSize;
  static Adaptive _cachedAdaptive;

  final bool isMobile;
  final bool isTablet;
  final bool isDesktop;

  const Adaptive._({
    this.isMobile = false,
    this.isTablet = false,
    this.isDesktop = false,
  });

  factory Adaptive.of(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    if (size != _cachedSize) {
      _cachedSize = size;
      _cachedAdaptive = _calculateAdaptive(size);
    }

    return _cachedAdaptive;
  }

  static Adaptive _calculateAdaptive(Size size) {
    final double diagonal = sqrt(pow(size.width, 2.0) + pow(size.height, 2.0));

    if (diagonal >= 1492.0) {
      // e.g. 1280x768
      return Adaptive._(isDesktop: true);
    } else if (diagonal >= 954.0 && size.shortestSide >= 500) {
      // e.g. 800x520
      return Adaptive._(isTablet: true);
    } else {
      return Adaptive._(isMobile: true);
    }
  }

  @override
  List<Object> get props => [isMobile, isTablet, isTablet];
}

/// A widget which helps to build adaptive widgets.
/// Ensure that you provide a widget builder for each screen class.
class AdaptiveWidgetBuilder extends StatelessWidget {
  const AdaptiveWidgetBuilder({
    Key key,
    @required this.mobile,
    @required this.tablet,
    @required this.desktop,
  }) : super(key: key);

  final WidgetBuilder mobile;
  final WidgetBuilder tablet;
  final WidgetBuilder desktop;

  @override
  Widget build(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    if (adaptive.isMobile) return mobile(context);
    if (adaptive.isTablet) return tablet(context);
    return desktop(context);
  }
}
