import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/typography.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

/// A widget suited to display subscription plans.
///
/// Consists of:
/// - Savings (e.g. SAVE 30%)
/// - Total period (e.g. Monthly)
/// - Total price for the period (e.g. PLN 30.90)
/// - Unit price (e.g. PLN 2.90)
/// - Unit period (e.g. Weekly)
class SubscriptionPlanWidget extends StatelessWidget {
  static const BoxConstraints _minConstraints = BoxConstraints(
    minWidth: 120,
    maxWidth: 120,
    minHeight: 160,
  );
  static const double _chipTopPosition = -16; // negative margin to pull chip over view bounds

  final String totalPeriod;
  final String totalPrice;
  final String unitPrice;
  final String unitPeriod;
  final String savings;
  final Color backgroundColor;
  final GestureTapCallback onTap;

  const SubscriptionPlanWidget({
    Key key,
    @required this.totalPeriod,
    this.totalPrice,
    @required this.unitPrice,
    @required this.unitPeriod,
    this.savings,
    this.backgroundColor,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          constraints: _minConstraints,
          child: InkWell(
            onTap: onTap,
            child: Card(
              color: Theme.of(context).colorScheme.surface,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  _buildTotalContainer(context),
                  _buildUnitContainer(context, _minConstraints),
                ],
              ),
            ),
          ),
        ),
        if (Strings.isNotBlank(savings))
          Positioned(
            top: _chipTopPosition,
            child: Container(
              constraints: _minConstraints,
              child: Align(
                alignment: Alignment.center,
                child: Chip(
                  label: Text(savings?.toUpperCase()),
                  labelStyle:
                      Theme.of(context).textTheme.overline.copyWith(color: Theme.of(context).colorScheme.onSecondary),
                  padding: EdgeInsets.zero,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  backgroundColor: Theme.of(context).colorScheme.secondary,
                ),
              ),
            ),
          ),
      ],
    );
  }

  Widget _buildTotalContainer(BuildContext context) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.all(Dimens.grid_0_5x),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildTotalPeriod(context),
            if (Strings.isNotBlank(totalPrice)) _buildTotalPrice(context),
          ],
        ),
      ),
    );
  }

  Widget _buildTotalPeriod(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: Strings.isNotBlank(totalPrice) ? Dimens.grid_2x : Dimens.grid_1x),
      child: Text(
        totalPeriod,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.headline5,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget _buildTotalPrice(BuildContext context) {
    return Text(totalPrice, textAlign: TextAlign.center, style: Theme.of(context).textTheme.caption2(context));
  }

  Widget _buildUnitContainer(BuildContext context, BoxConstraints constraints) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.all(Dimens.grid_0_5x),
        constraints: BoxConstraints(minWidth: constraints.minWidth),
        color: backgroundColor ?? Colors.transparent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildUnitPrice(context),
            _buildUnitPeriod(context),
          ],
        ),
      ),
    );
  }

  Widget _buildUnitPrice(BuildContext context) {
    return Text(
      unitPrice,
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.headline5.copyWith(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
    );
  }

  Widget _buildUnitPeriod(BuildContext context) {
    return Text(
      unitPeriod,
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.caption2(context).copyWith(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
    );
  }
}
