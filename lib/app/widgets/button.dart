import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/typography.dart';

/// A mini version of [RaisedButton] which can be used to fit into small placements.
class SmallRaisedButton extends StatelessWidget {
  final Widget child;
  final GestureTapCallback onPressed;
  final Color color;

  const SmallRaisedButton({
    Key key,
    @required this.child,
    @required this.onPressed,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxHeight: Dimens.smallButtonHeight),
      child: RaisedButton(
        child: child,
        onPressed: onPressed,
        color: color,
      ),
    );
  }
}

/// A mini version of [MaterialButton] which can be used to fit into small placements.
class SmallMaterialButton extends StatelessWidget {
  final Widget child;
  final GestureTapCallback onPressed;
  final ShapeBorder shape;
  final EdgeInsetsGeometry padding;

  const SmallMaterialButton({
    Key key,
    @required this.child,
    @required this.onPressed,
    this.shape,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxHeight: Dimens.smallButtonHeight),
      child: MaterialButton(
        child: child,
        onPressed: onPressed,
        shape: shape,
        padding: padding,
      ),
    );
  }
}

/// A mini material button with a border without a fill.
class SmallBorderedButton extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;

  const SmallBorderedButton({
    Key key,
    @required this.child,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmallMaterialButton(
      child: child,
      onPressed: onPressed,
      padding: EdgeInsets.symmetric(horizontal: Dimens.grid_1x),
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Theme.of(context).dividerColor),
        borderRadius: BorderRadius.all(Radius.circular(Dimens.grid_0_5x)),
      ),
    );
  }
}

/// A [Text] intended to be used inside a button.
/// Makes the text uppercase and applies default text theme (button).
class ButtonText extends StatelessWidget {
  final String text;
  final TextStyle textStyle;
  final Color color;

  const ButtonText(this.text, {Key key, this.textStyle, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text.toUpperCase(),
      textAlign: TextAlign.center,
      style: (textStyle ?? Theme.of(context).textTheme.button).copyWith(
        color: color ?? Theme.of(context).colorScheme.onSecondary,
      ),
    );
  }
}

/// A [Text] intended to be used inside a button.
/// Makes the text uppercase and applies default text theme (button2).
class SmallButtonText extends StatelessWidget {
  final String text;
  final TextStyle textStyle;
  final Color color;

  const SmallButtonText(this.text, {Key key, this.textStyle, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text.toUpperCase(),
      textAlign: TextAlign.center,
      style: (textStyle ?? Theme.of(context).textTheme.button2(context)).copyWith(
        color: color ?? Theme.of(context).colorScheme.onSecondary,
      ),
    );
  }
}

/// A mini version of [PopupMenuButton] which can be used to fit into small placements.
class SmallBorderedPopupButton<T> extends StatelessWidget {
  final PopupMenuItemBuilder<T> itemBuilder;
  final PopupMenuItemSelected<T> onSelected;
  final Widget child;

  const SmallBorderedPopupButton({
    Key key,
    @required this.itemBuilder,
    @required this.onSelected,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      itemBuilder: itemBuilder,
      onSelected: onSelected,
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          borderRadius: BorderRadius.circular(Dimens.grid_0_5x),
          border: Border.all(color: Theme.of(context).dividerColor),
        ),
        constraints: BoxConstraints(
          minHeight: Dimens.smallButtonHeight,
          maxHeight: Dimens.smallButtonHeight,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: Dimens.grid_1_5x,
          vertical: Dimens.grid_1x,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            child,
            Padding(
              padding: EdgeInsets.only(left: Dimens.grid_1x),
              child: Image.asset(
                Images.ic_dropdown_indicator_8,
                width: 8,
                height: 8,
                color: Theme.of(context).colorScheme.primary,
                excludeFromSemantics: true,
              ),
            )
          ],
        ),
      ),
    );
  }
}
