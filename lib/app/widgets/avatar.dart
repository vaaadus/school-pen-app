import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/widgets/remote_image.dart';
import 'package:school_pen/domain/files/model/remote_file.dart';

const double _imageSizeFactor = 0.285;

/// Renders a user image or placeholder if user image not set.
class Avatar extends StatelessWidget {
  const Avatar({
    Key key,
    this.file,
    this.width = 56.0,
    this.height = 56.0,
  }) : super(key: key);

  final RemoteFile file;
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return file != null ? _buildImage(context) : _buildPlaceholder(context);
  }

  Widget _buildImage(BuildContext context) {
    return ClipRRect(
      borderRadius: _borderRadius,
      child: RemoteImage(
        file: file,
        width: width,
        height: height,
        placeholderBuilder: _buildPlaceholder,
        errorBuilder: _buildPlaceholder,
      ),
    );
  }

  Widget _buildPlaceholder(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
        border: Border.all(color: Theme.of(context).dividerColor),
        borderRadius: _borderRadius,
      ),
      child: Center(
        child: Image.asset(
          Images.ic_person_16,
          width: width * _imageSizeFactor,
          height: height * _imageSizeFactor,
          fit: BoxFit.fill,
          color: Theme.of(context).colorScheme.primaryVariant,
        ),
      ),
    );
  }

  BorderRadius get _borderRadius => BorderRadius.circular((width + height) / 2);
}
