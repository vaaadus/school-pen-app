import 'package:flutter/material.dart';

import 'adaptive.dart';

/// Holds a master-detail layout in a row.
///
/// If the device resolution is not eligible for master-detail layout,
/// then only the master layout will be shown.
///
/// If the details layout resolves to null, then it will be
/// skipped and the master layout will take the whole space.
class MasterDetailContainer extends StatelessWidget {
  final WidgetBuilder masterBuilder;
  final WidgetBuilder detailBuilder;
  final int masterFlex;
  final int detailFlex;

  const MasterDetailContainer({
    Key key,
    @required this.masterBuilder,
    @required this.detailBuilder,
    this.masterFlex = 2,
    this.detailFlex = 3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isSupported(context)) {
      final Widget master = masterBuilder(context);
      final Widget detail = detailBuilder(context);

      return Row(children: [
        Expanded(child: master, flex: masterFlex),
        if (detail != null) VerticalDivider(),
        if (detail != null) Expanded(child: detail, flex: detailFlex),
      ]);
    } else {
      return masterBuilder(context);
    }
  }

  /// Whether a master-detail layout is supported according to the device screen size.
  static bool isSupported(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    final Orientation orientation = MediaQuery.of(context).orientation;
    return !adaptive.isMobile && orientation == Orientation.landscape;
  }
}
