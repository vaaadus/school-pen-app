import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

/// Draws a background for the [TextField].
class TextFieldContainer extends StatelessWidget {
  static const double normalHeight = 56.0;
  static const double miniHeight = 48.0;
  static const double microHeight = 32.0;

  const TextFieldContainer({
    Key key,
    @required this.child,
    this.error,
    this.width,
    this.maxHeight = normalHeight,
    this.showDropdown = false,
  }) : super(key: key);

  final Widget child;
  final TextFieldError error;
  final double width;
  final double maxHeight;
  final bool showDropdown;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = _resolveTheme(context);

    Widget result = Theme(
      data: theme,
      child: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: width ?? double.infinity),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: child,
              height: maxHeight,
              decoration: BoxDecoration(
                color: theme.colorScheme.surface,
                border: Border.all(color: theme.dividerColor),
                borderRadius: BorderRadius.circular(4.0),
              ),
            ),
            if (error != null) error,
          ],
        ),
      ),
    );

    if (showDropdown) {
      final Widget dropdown = Align(
        alignment: Alignment.centerLeft,
        child: Image.asset(
          Images.ic_dropdown_indicator_8,
          width: 8,
          height: 8,
          color: Theme.of(context).colorScheme.primaryVariant,
        ),
      );

      result = Stack(children: [
        result,
        Positioned(top: 0, bottom: 0, right: Dimens.grid_2x, child: dropdown),
      ]);
    }

    return result;
  }

  ThemeData _resolveTheme(BuildContext context) {
    if (maxHeight >= normalHeight) return Theme.of(context);

    return Theme.of(context).copyWith(
      inputDecorationTheme: Theme.of(context).inputDecorationTheme.copyWith(
            contentPadding: EdgeInsets.symmetric(
              vertical: 0,
              horizontal: maxHeight > microHeight ? Dimens.grid_1_5x : 0,
            ),
          ),
    );
  }
}

/// Renders the error under the text field.
/// One of [error] or [text] must be provided.
/// In case of error, the [ResourceLocator.errorToText] will be used to stringify the error.
class TextFieldError extends StatelessWidget {
  const TextFieldError({
    Key key,
    this.error,
    this.text,
  })  : assert(error == null || text == null),
        assert(error != null || text != null),
        super(key: key);

  factory TextFieldError.forError(Exception error) {
    if (error == null) return null;
    return TextFieldError(error: error);
  }

  factory TextFieldError.forText(String text) {
    if (Strings.isBlank(text)) return null;
    return TextFieldError(text: text);
  }

  final String text;
  final Exception error;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return Padding(
      padding: EdgeInsets.only(
        left: theme.inputDecorationTheme.contentPadding.horizontal / 2,
        top: Dimens.grid_0_5x,
      ),
      child: Text(
        text ?? ResourceLocator.errorToText(context, error),
        style: theme.textTheme.caption.copyWith(
          color: theme.colorScheme.error,
        ),
      ),
    );
  }
}
