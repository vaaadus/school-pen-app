import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/domain/themes/model/custom_theme.dart';
import 'package:school_pen/domain/themes/themes_listener.dart';
import 'package:school_pen/domain/themes/themes_manager.dart';

/// Provides access to app configuration.
/// Rebuilds the child whenever a config changes.
class SchoolPenOptions extends StatefulWidget {
  const SchoolPenOptions({Key key, @required this.builder}) : super(key: key);

  final WidgetBuilder builder;

  static SchoolPenOptionsState of(BuildContext context) {
    return context.findAncestorStateOfType();
  }

  @override
  State<StatefulWidget> createState() => SchoolPenOptionsState();
}

class SchoolPenOptionsState extends State<SchoolPenOptions> implements ThemesListener {
  final ThemesManager _themesManager = Injection.get();

  CustomTheme get customTheme => _themesManager.selectedTheme;

  @override
  void initState() {
    super.initState();
    _themesManager.addThemesListener(this, notifyOnAttach: false);
  }

  @override
  void dispose() {
    _themesManager.removeThemesListener(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => widget.builder(context);

  @override
  void onThemeSelected(CustomTheme theme) {
    setState(() {
      // themes manager has already this state, let the widget rebuild
    });
  }
}
