import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

import 'adaptive.dart';

/// The app bar.
class Toolbar extends StatelessWidget implements Equatable {
  static const double bottomPadding = Dimens.grid_1x;
  static const double contentHeight = NavButton.height;

  const Toolbar({
    Key key,
    this.pinned,
    @required this.vsync,
    this.navType = NavButtonType.back,
    this.onBack = Navigator.pop,
    this.title,
    this.prefix,
    this.actions,
    this.horizontalPadding,
  })  : assert(prefix == null || navType == null, 'Either nav button or prefix widget is supported'),
        assert(prefix == null || title == null, 'Either title or prefix are supported, not both'),
        super(key: key);

  /// Whether the toolbar should stay always on the top.
  final bool pinned;

  /// The vsync for the sliver persistent header.
  final TickerProvider vsync;

  /// Specifies what type of nav button to show: close, back or null.
  final NavButtonType navType;

  /// Callback invoked by nav button.
  final OnBackCallback onBack;

  /// A title displayed in the toolbar.
  final String title;

  /// A widget displayed just after the nav button.
  final Widget prefix;

  /// A widgets displayed at the end of the toolbar.
  final List<Widget> actions;

  /// Custom horizontal padding. Default is [Paddings.horizontal].
  final double horizontalPadding;

  @override
  Widget build(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    final Paddings paddings = Paddings.of(context);

    return SliverPersistentHeader(
      floating: true,
      pinned: pinned ?? !adaptive.isMobile,
      delegate: _SliverPersistentHeaderDelegate(
        toolbar: this,
        adaptive: adaptive,
        paddings: paddings,
      ),
    );
  }

  @override
  List<Object> get props => [vsync, navType, onBack, title, prefix, actions];

  @override
  bool get stringify => true;
}

class _SliverPersistentHeaderDelegate extends SliverPersistentHeaderDelegate {
  const _SliverPersistentHeaderDelegate({
    @required this.toolbar,
    @required this.adaptive,
    @required this.paddings,
  });

  final Toolbar toolbar;
  final Adaptive adaptive;
  final Paddings paddings;

  @override
  double get minExtent => _getExtent();

  @override
  double get maxExtent => _getExtent();

  @override
  bool shouldRebuild(_SliverPersistentHeaderDelegate oldDelegate) {
    return toolbar != oldDelegate.toolbar ||
        minExtent != oldDelegate.minExtent ||
        maxExtent != oldDelegate.maxExtent ||
        adaptive != oldDelegate.adaptive ||
        paddings != oldDelegate.paddings;
  }

  @override
  TickerProvider get vsync => toolbar.vsync;

  @override
  FloatingHeaderSnapConfiguration get snapConfiguration => FloatingHeaderSnapConfiguration();

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    final Paddings paddings = Paddings.of(context);
    return Container(
      padding: EdgeInsets.only(
        left: (toolbar.horizontalPadding ?? paddings.horizontal) - (_showNavButton ? NavButton.innerPadding : 0),
        right: toolbar.horizontalPadding ?? paddings.horizontal,
        bottom: Toolbar.bottomPadding,
        top: _getTopPadding(),
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: [0.0, 0.7, 0.8, 1.0],
          colors: [
            Theme.of(context).colorScheme.background.withAlpha(255),
            Theme.of(context).colorScheme.background.withAlpha(220),
            Theme.of(context).colorScheme.background.withAlpha(200),
            Theme.of(context).colorScheme.background.withAlpha(180),
          ],
        ),
      ),
      child: SizedBox(
        height: Toolbar.contentHeight,
        child: Row(children: [
          if (_showPrefix) toolbar.prefix,
          if (_showNavButton) _buildNavButton(),
          if (_showTitle) _buildTitle(context),
          if (!_showTitle) Spacer(),
          if (_showActions) _buildRowWithActions(),
        ]),
      ),
    );
  }

  Widget _buildNavButton() {
    return NavButton(
      type: toolbar.navType,
      onBack: toolbar.onBack,
      includeDefaultPadding: false,
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(left: toolbar.navType == null ? 0 : (Dimens.grid_3x - NavButton.innerPadding)),
        child: Text(toolbar.title, style: Theme.of(context).textTheme.headline6),
      ),
    );
  }

  Widget _buildRowWithActions() {
    return Row(children: [
      for (Widget action in toolbar.actions)
        Padding(
          padding: EdgeInsets.only(left: Dimens.grid_2x),
          child: action,
        ),
    ]);
  }

  double _getExtent() => _getTopPadding() + Toolbar.contentHeight + Toolbar.bottomPadding;

  double _getTopPadding() {
    if (_showPrefix || _showActions) return paddings.vertical - 2.0;
    if (_showNavButton) return paddings.vertical - NavButton.innerPadding;
    return paddings.vertical;
  }

  bool get _showPrefix => toolbar.prefix != null;

  bool get _showNavButton => toolbar.navType != null;

  bool get _showTitle => Strings.isNotBlank(toolbar.title);

  bool get _showActions => toolbar.actions != null && toolbar.actions.isNotEmpty;
}
