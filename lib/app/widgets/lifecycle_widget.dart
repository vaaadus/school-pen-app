import 'package:flutter/material.dart';
import 'package:school_pen/app/di/injection.dart';

/// A widget meant to be bound to a lifecycle (onResume, onPause).
/// Resumed: widget is visible and active
/// Paused: widget is not visible or not active
abstract class LifecycleWidget extends StatefulWidget {
  const LifecycleWidget({Key key}) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState();
}

@optionalTypeArgs
abstract class LifecycleWidgetState<T extends LifecycleWidget> extends State<T>
    with WidgetsBindingObserver, RouteAware {
  final RouteObserver _routeObserver = Injection.get();

  bool _started = false;
  bool _resumed = false;
  bool _visible = true;

  @mustCallSuper
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _dispatchStart();
  }

  @mustCallSuper
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @mustCallSuper
  @override
  void dispose() {
    _dispatchPause();
    _dispatchStop();
    _routeObserver.unsubscribe(this);
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @mustCallSuper
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (!_visible) return;

    if (state == AppLifecycleState.resumed) {
      _dispatchStart();
      _dispatchResume();
    } else if (state == AppLifecycleState.paused) {
      _dispatchPause();
      _dispatchStop();
    }
  }

  @mustCallSuper
  @override
  void didPush() {
    _dispatchResume();
    _visible = true;
  }

  @mustCallSuper
  @override
  void didPopNext() {
    _dispatchResume();
    _visible = true;
  }

  @mustCallSuper
  @override
  void didPop() {
    _dispatchPause();
    _visible = false;
  }

  @mustCallSuper
  @override
  void didPushNext() {
    _dispatchPause();
    _visible = false;
  }

  /// Called once the application is active.
  @mustCallSuper
  void onStart() {}

  /// Called when the application becomes inactive.
  @mustCallSuper
  void onStop() {}

  /// Called when the page state resumes.
  @mustCallSuper
  void onResume() {}

  /// Called when the page state pauses.
  @mustCallSuper
  void onPause() {}

  void _dispatchStart() {
    if (!_started) {
      _started = true;
      onStart();
    }
  }

  void _dispatchStop() {
    if (_started) {
      _started = false;
      onStop();
    }
  }

  void _dispatchResume() {
    if (!_resumed) {
      _resumed = true;
      onResume();
    }
  }

  void _dispatchPause() {
    if (_resumed) {
      _resumed = false;
      onPause();
    }
  }
}
