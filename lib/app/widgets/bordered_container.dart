import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';

/// A bordered container with inkwell.
/// [longPress] is an equivalent to force press on supported devices.
class BorderedContainer extends StatelessWidget {
  final Widget child;
  final double width;
  final double height;
  final GestureTapCallback onTap;
  final GestureTapCallback onLongPress;
  final Color color;
  final EdgeInsets padding;

  const BorderedContainer({
    Key key,
    @required this.child,
    this.width,
    this.height,
    this.onTap,
    this.onLongPress,
    this.color,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget result = child;

    if (padding != null) {
      result = Padding(padding: padding, child: result);
    }

    result = Container(
      child: result,
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: color,
        border: Border.fromBorderSide(BorderSide(color: Theme.of(context).dividerColor)),
        borderRadius: BorderRadius.all(Radius.circular(Dimens.cardRadius)),
      ),
    );

    if (onTap != null || onLongPress != null) {
      result = GestureDetector(
        onForcePressStart: (ForcePressDetails details) => onLongPress(),
        child: InkWell(
          onTap: onTap,
          onLongPress: onLongPress,
          borderRadius: BorderRadius.all(Radius.circular(Dimens.cardRadius)),
          child: result,
        ),
      );
    }

    return result;
  }
}
