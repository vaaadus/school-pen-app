import 'package:flutter/widgets.dart';

/// A single child scroll view that fills the available vertical space.
class FillViewportScrollView extends StatelessWidget {
  /// The child which fills the viewport.
  final Widget child;

  /// The constraints which provide the minimum width & height for the scroll view.
  /// Pass here null to automatically create [LayoutBuilder].
  final BoxConstraints constraints;

  /// The scroll view padding.
  final EdgeInsets padding;

  const FillViewportScrollView({
    Key key,
    @required this.child,
    this.constraints,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (constraints != null) {
      return _buildScrollView(context, constraints);
    }
    return LayoutBuilder(builder: _buildScrollView);
  }

  Widget _buildScrollView(BuildContext context, BoxConstraints constraints) {
    return SingleChildScrollView(
      padding: padding,
      child: ConstrainedBox(
        constraints: BoxConstraints(minHeight: _getMinHeightConstraint(constraints)),
        child: IntrinsicHeight(child: child),
      ),
    );
  }

  double _getMinHeightConstraint(BoxConstraints constraints) {
    return constraints.maxHeight - (padding?.top ?? 0) - (padding?.bottom ?? 0);
  }
}
