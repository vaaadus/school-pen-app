import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// A [Dialog] widget which sizes itself to the fit 75% of the screen height with a maximum width constraint.
///
/// The dialog won't be scrollable so the child must take care of it.
class ScaffoldDialog extends StatelessWidget {
  static const double wideWidth = 480.0;

  const ScaffoldDialog({
    Key key,
    @required this.child,
    this.maxWidth = wideWidth,
  }) : super(key: key);

  final Widget child;
  final double maxWidth;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      clipBehavior: Clip.antiAlias,
      child: Container(
        constraints: BoxConstraints(
          maxWidth: maxWidth,
          maxHeight: MediaQuery.of(context).size.height * 0.75,
        ),
        decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).colorScheme.surface),
        ),
        child: child,
      ),
    );
  }
}
