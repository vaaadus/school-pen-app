import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

const Size _defaultSize = Size(36, 36);

/// A clickable image.
class ImageButton extends StatelessWidget {
  final Image image;
  final String tooltip;
  final Size size;
  final Color fillColor;
  final GestureTapCallback onTap;
  final BorderSide borderSide;

  const ImageButton({
    Key key,
    @required this.image,
    this.tooltip,
    this.onTap,
    this.fillColor,
    this.size = _defaultSize,
    this.borderSide = BorderSide.none,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget result = RawMaterialButton(
      child: image,
      fillColor: fillColor ?? Theme.of(context).colorScheme.surface,
      onPressed: onTap,
      padding: EdgeInsets.zero,
      elevation: 0,
      focusElevation: 0,
      highlightElevation: 0,
      disabledElevation: 0,
      hoverElevation: 0,
      constraints: BoxConstraints(
        minWidth: size.width,
        minHeight: size.height,
        maxWidth: size.width,
        maxHeight: size.height,
      ),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
        side: borderSide,
      ),
    );

    if (Strings.isNotBlank(tooltip)) {
      result = Tooltip(
        message: tooltip,
        child: result,
      );
    }

    if (onTap != null) {
      result = MergeSemantics(child: result);
    } else {
      result = ExcludeSemantics(child: result);
    }

    return result;
  }
}

/// Most commonly used image button with border and default image size.
class BorderedImageButton extends StatelessWidget {
  final String imageAsset;
  final String tooltip;
  final double imageWidth;
  final double imageHeight;
  final Color imageTint;
  final bool applyImageTint;
  final Color fillColor;
  final GestureTapCallback onTap;

  const BorderedImageButton({
    Key key,
    @required this.imageAsset,
    this.tooltip,
    this.imageWidth = 20,
    this.imageHeight = 20,
    this.imageTint,
    this.applyImageTint = true,
    this.fillColor,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ImageButton(
      image: Image.asset(
        imageAsset,
        width: imageWidth,
        height: imageHeight,
        color: _tintColor(context),
        excludeFromSemantics: true,
      ),
      tooltip: tooltip,
      onTap: onTap,
      fillColor: fillColor,
      borderSide: BorderSide(color: Theme.of(context).dividerColor),
    );
  }

  Color _tintColor(BuildContext context) {
    if (!applyImageTint) return null;
    return imageTint ?? Theme.of(context).colorScheme.primary;
  }
}
