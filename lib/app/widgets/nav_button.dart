import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/generated/l10n.dart';

typedef OnBackCallback = void Function(BuildContext context);

/// Specifies the kind of the nav button.
enum NavButtonType {
  back,
  close,
}

/// Renders a navigation button such as back or close.
/// The button will include the top & left padding from [Paddings].
class NavButton extends StatelessWidget {
  static const double height = 40.0;
  static const double innerPadding = 8.0;

  final NavButtonType type;
  final OnBackCallback onBack;
  final bool includeDefaultPadding;

  const NavButton({
    Key key,
    this.type = NavButtonType.back,
    this.onBack = Navigator.pop,
    this.includeDefaultPadding = true,
  }) : super(key: key);

  const NavButton.back({
    Key key,
    OnBackCallback onBack = Navigator.pop,
    bool includeDefaultPadding = true,
  }) : this(
          type: NavButtonType.back,
          onBack: onBack,
          includeDefaultPadding: includeDefaultPadding,
        );

  const NavButton.close({
    Key key,
    OnBackCallback onBack = Navigator.pop,
    bool includeDefaultPadding = true,
  }) : this(
          type: NavButtonType.close,
          onBack: onBack,
          includeDefaultPadding: includeDefaultPadding,
        );

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);

    Widget result = Material(
      color: Colors.transparent,
      child: InkWell(
        borderRadius: BorderRadius.circular(height),
        onTap: () => onBack(context),
        child: Padding(
          padding: EdgeInsets.all(innerPadding),
          child: Image.asset(
            _getImageAsset24(),
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primary,
            semanticLabel: _getSemanticLabel(context),
          ),
        ),
      ),
    );

    if (includeDefaultPadding) {
      result = Padding(
        padding: EdgeInsets.only(
          top: paddings.vertical - innerPadding,
          left: paddings.horizontal - innerPadding,
        ),
        child: result,
      );
    }

    return MergeSemantics(child: result);
  }

  String _getImageAsset24() {
    if (type == NavButtonType.back) return Images.ic_back_24;
    if (type == NavButtonType.close) return Images.ic_close_24;
    return null;
  }

  String _getSemanticLabel(BuildContext context) {
    if (type == NavButtonType.back) return S.of(context).common_goBack;
    if (type == NavButtonType.close) return S.of(context).common_close;
    return null;
  }
}
