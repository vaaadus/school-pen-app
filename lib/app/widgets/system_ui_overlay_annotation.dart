import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:school_pen/app/common/extensions.dart';

/// Wraps a child and overrides the system UI overlays.
class SystemUiOverlayAnnotation extends StatelessWidget {
  const SystemUiOverlayAnnotation({
    Key key,
    @required this.child,
    this.statusBarColor,
    this.statusBarBrightness,
    this.statusBarIconBrightness,
    this.systemNavigationBarColor,
    this.systemNavigationBarIconBrightness,
  }) : super(key: key);

  final Widget child;
  final Color statusBarColor;
  final Brightness statusBarBrightness;
  final Brightness statusBarIconBrightness;
  final Color systemNavigationBarColor;
  final Brightness systemNavigationBarIconBrightness;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      child: child,
      value: SystemUiOverlayStyle(
        statusBarColor: statusBarColor ?? theme.colorScheme.background,
        statusBarBrightness: statusBarBrightness ?? theme.brightness,
        statusBarIconBrightness: statusBarIconBrightness ?? theme.brightness.inverted,
        systemNavigationBarColor: systemNavigationBarColor ?? theme.colorScheme.background,
        systemNavigationBarIconBrightness: systemNavigationBarIconBrightness ?? theme.brightness.inverted,
      ),
    );
  }
}
