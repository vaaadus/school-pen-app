import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

typedef ListItemsProvider = Future<List<ListItemData>> Function();
typedef ListItemCallback = void Function(ListItemData);

class ListItemData extends Equatable {
  final String title;
  final String icon;
  final String label;
  final Object customData;

  const ListItemData({
    @required this.title,
    this.icon,
    this.label,
    this.customData,
  });

  @override
  List<Object> get props => [icon, title, label, customData];
}

/// A list item consisting of an icon, a title and an optional label.
class ListItem extends StatelessWidget {
  static const double preferredMinHeight = 44.0;

  const ListItem({
    Key key,
    @required this.data,
    this.titleStyle,
    this.iconColor,
    this.showArrow = true,
    this.isActive = false,
  }) : super(key: key);

  final ListItemData data;
  final TextStyle titleStyle;
  final Color iconColor;
  final bool showArrow;
  final bool isActive;

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      if (_showIcon) _buildIcon(),
      _buildTexts(context),
      if (showArrow) _buildRightArrow(context),
    ]);
  }

  Widget _buildIcon() {
    return Image.asset(
      data.icon,
      color: iconColor,
      width: 36.0,
      height: 36.0,
      excludeFromSemantics: true,
    );
  }

  Widget _buildTexts(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(
          left: _showIcon ? Dimens.grid_3x : 0,
          right: Dimens.grid_3x,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: data.label != null ? [_buildTitle(context), _buildLabel(context)] : [_buildTitle(context)],
        ),
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Text(
      titleStyle == null ? data.title.toUpperCase() : data.title,
      style: (titleStyle ?? Theme.of(context).textTheme.button).copyWith(
        color: isActive ? Theme.of(context).colorScheme.secondary : Theme.of(context).colorScheme.primary,
      ),
    );
  }

  Widget _buildLabel(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: Dimens.grid_0_5x),
      child: Text(
        data.label,
        style: Theme.of(context).textTheme.subtitle2.copyWith(
              color: isActive ? Theme.of(context).colorScheme.secondary : Theme.of(context).colorScheme.primaryVariant,
            ),
      ),
    );
  }

  Widget _buildRightArrow(BuildContext context) {
    return Image.asset(
      Images.ic_chevron_right_16,
      width: 16,
      height: 16,
      color: isActive ? Theme.of(context).colorScheme.secondary : Theme.of(context).colorScheme.primaryVariant,
    );
  }

  bool get _showIcon => Strings.isNotBlank(data.icon);
}
