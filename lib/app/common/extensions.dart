import 'package:flutter/material.dart';

extension BrightnessExt on Brightness {
  Brightness get inverted {
    return this == Brightness.light ? Brightness.dark : Brightness.light;
  }
}

extension AnimationControllerExt on AnimationController {
  /// Plays the animation backwards then forwards.
  void reverseAndForward() {
    Function(AnimationStatus) listener;
    listener = (AnimationStatus status) {
      if (status == AnimationStatus.dismissed) {
        forward();
        removeStatusListener(listener);
      }
    };

    addStatusListener(listener);
    reverse();
  }

  /// Similar to [reset], however sets the animation as completed,
  /// meaning it transitions instantly to the [upperBound].
  void complete() {
    value = upperBound;
  }
}

extension TextEditingControllerExt on TextEditingController {
  /// Sets a new text if previous text was different and returns true,
  /// returns false otherwise if previous text was equal to the new one.
  bool setTextIfChanged(String newText) {
    if (text != newText) {
      text = newText;
      selection = TextSelection.fromPosition(TextPosition(offset: text.length));
      return true;
    }
    return false;
  }
}
