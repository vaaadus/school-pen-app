import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';

/// A formatter for [TimetableEvent] & [SingleTimetableEvent].
class TimetableEventFormatter {
  static Color getColor(SingleTimetableEvent event, Course course) {
    return ResourceLocator.colorEnumToColor(course?.color) ?? _generateColor(formatCourseName(event, course));
  }

  static Color _generateColor(String string) {
    final int hash = string.hashCode;

    final ColorEnum colorEnum = ColorEnum.values[hash % ColorEnum.values.length];

    final Color left =
        HSLColor.fromColor(ResourceLocator.colorEnumToColor(colorEnum)).withSaturation(0.5 + hash % 51 / 100).toColor();

    final Color right = Color(0xFF000000 | hash);

    return Color.lerp(left, right, hash % 11 / 10);
  }

  static String formatCourseName(SingleTimetableEvent event, Course course) {
    if (Strings.isNotBlank(event.customCourse)) return event.customCourse;

    return course?.name ?? '';
  }

  static String formatRoom(SingleTimetableEvent event, Course course) {
    if (Strings.isNotBlank(event.customRoom)) return event.customRoom;

    return course?.room ?? '';
  }

  static String formatStudent(SingleTimetableEvent event) {
    return event.customStudent ?? '';
  }

  static String formatTeachers(SingleTimetableEvent event, List<Teacher> teachers) {
    if (Strings.isNotBlank(event.customTeacher)) return event.customTeacher;

    return teachers.map((e) => e.name).join(' · ');
  }

  static String formatTimeRange(TimeRange timeRange) {
    return timeRange.start.toString() + ' - ' + timeRange.end.toString();
  }
}
