import 'package:flutter/cupertino.dart';
import 'package:school_pen/domain/common/notification_options.dart';
import 'package:school_pen/generated/l10n.dart';

/// Formats [NotificationOptions].
class NotificationFormatter {
  static String formatNotification(BuildContext context, NotificationOptions notification) {
    return notification.amount.toString() + ' ' + formatUnit(context, notification.unit, true);
  }

  static String formatUnit(BuildContext context, NotificationOptionsUnit unit, bool isActive) {
    switch (unit) {
      case NotificationOptionsUnit.minutes:
        return isActive ? S.of(context).common_minutesBefore : S.of(context).common_minutes;
      case NotificationOptionsUnit.hours:
        return isActive ? S.of(context).common_hoursBefore : S.of(context).common_hours;
      case NotificationOptionsUnit.days:
        return isActive ? S.of(context).common_daysBefore : S.of(context).common_days;
      case NotificationOptionsUnit.weeks:
        return isActive ? S.of(context).common_weeksBefore : S.of(context).common_weeks;
    }

    throw ArgumentError('Unsupported unit: $unit');
  }
}
