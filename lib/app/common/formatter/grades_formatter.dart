import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

/// Formats grades with units.
class GradesFormatter {
  /// Formats the grade name or returns the default value.
  static String formatName(BuildContext context, String name) {
    return Strings.isNotBlank(name) ? name : S.of(context).grades_unnamed;
  }

  /// Formats the grade value or returns the placeholder.
  static String formatGrade(BuildContext context, double grade, GradingSystem system) {
    if (grade == null || system == null) return _formatInvalid();

    return system.decorate(grade);
  }

  /// Formats the grade weight with short prefix or returns the placeholder.
  static String formatNamedWeight(BuildContext context, double weight) {
    return sprintf(S.of(context).grades_weight_short, [GradesFormatter.formatWeight(weight)]);
  }

  /// Formats the grade weight or returns null.
  static String formatWeight(double weight) {
    if (weight == null) return _formatInvalid();
    return NumberFormat('0.0#').format(weight);
  }

  /// Formats long name of the grading system. Unit with supported values range.
  static String formatGradingSystem(BuildContext context, GradingSystem system) {
    if (system == null) return _formatInvalid();
    if (system.type == GradingSystemType.percentage) return S.of(context).gradingSystem_percentage + ' (0-100%)';
    if (system.type == GradingSystemType.alphabetic) return S.of(context).gradingSystem_alphabetic + ' (A-F)';
    if (system.type == GradingSystemType.numeric) {
      return S.of(context).gradingSystem_numeric + sprintf(' (%.0f-%.0f)', [system.minValue, system.maxValue]);
    }
    throw ArgumentError('Unsupported grading system: $system');
  }

  /// Formats the grade category or returns the placeholder.
  static String formatGradeCategoryWithDefaults(BuildContext context, GradeCategory category) {
    return formatGradeCategory(context, category) ?? S.of(context).common_noCategory;
  }

  /// Formats the grade category or returns null.
  static String formatGradeCategory(BuildContext context, GradeCategory category) {
    if (category == null) return null;

    final S s = S.of(context);
    if (category.id == GradeCategory.written.id) return s.gradeCategory_written;
    if (category.id == GradeCategory.oral.id) return s.gradeCategory_oral;
    if (category.id == GradeCategory.practical.id) return s.gradeCategory_practical;
    return category.customName;
  }

  /// Assigns a color to a grade value. High grades are assigned green colors, low ones - the red.
  static Color getGradeColor(double value) {
    return Color.lerp(AppColors.red1, AppColors.green2, value / Grade.maxValue);
  }

  static String _formatInvalid() {
    return '--';
  }
}
