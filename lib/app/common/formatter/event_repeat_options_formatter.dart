import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

/// Formats [EventRepeatOptions].
class EventRepeatOptionsFormatter {
  static String format(BuildContext context, EventRepeatOptions options) {
    String message = _formatWithType(context, options);
    message += _formatEndsAfterOccurrences(context, options);
    message += _formatEndsAtDate(context, options);
    return message;
  }

  static String _formatWithType(BuildContext context, EventRepeatOptions options) {
    switch (options.type) {
      case EventRepeatOptionsType.single:
        return _formatSingle(context);
      case EventRepeatOptionsType.daily:
        return _formatDaily(context, options);
      case EventRepeatOptionsType.weekly:
        return _formatWeekly(context, options);
      case EventRepeatOptionsType.monthly:
        return _formatMonthly(context, options);
    }
    throw ArgumentError('Unsupported type: ${options.type}');
  }

  static String _formatSingle(BuildContext context) {
    return S.of(context).repeatOptions_doesNotRepeat;
  }

  static String _formatDaily(BuildContext context, EventRepeatOptions options) {
    if (options.repeatInterval == 1) {
      return S.of(context).repeatOptions_everyDay;
    } else {
      return sprintf(S.of(context).repeatOptions_everyNDays, [options.repeatInterval]);
    }
  }

  static String _formatWeekly(BuildContext context, EventRepeatOptions options) {
    String message;
    if (options.repeatInterval == 1) {
      message = S.of(context).repeatOptions_everyWeek;
    } else {
      message = sprintf(S.of(context).repeatOptions_everyNWeeks, [options.repeatInterval]);
    }

    if (options.weeklyRepeatsOn.length <= 1) {
      return message;
    } else {
      return message +
          ' (' +
          options.weeklyRepeatsOn.map((e) => ResourceLocator.weekdayToString(context, e)).join(', ') +
          ')';
    }
  }

  static String _formatMonthly(BuildContext context, EventRepeatOptions options) {
    if (options.repeatInterval == 1) {
      return S.of(context).repeatOptions_everyMonth;
    } else {
      return sprintf(S.of(context).repeatOptions_everyNMonths, [options.repeatInterval]);
    }
  }

  static String _formatEndsAfterOccurrences(BuildContext context, EventRepeatOptions options) {
    if (options.endsAfterOccurrences != null) {
      return ', ' + S.of(context).common_times + ': ' + options.endsAfterOccurrences.toString();
    }
    return '';
  }

  static String _formatEndsAtDate(BuildContext context, EventRepeatOptions options) {
    if (options.endsAt != null) {
      return ', ' + S.of(context).common_until + ': ' + DateFormat.yMd().format(options.endsAt);
    }
    return '';
  }
}
