import 'package:intl/intl.dart';

class NumberFormatter {
  /// Formats a number up to [precision] places after the comma.
  /// E.g.
  ///  - format(1.245, 2) == '1.25'
  ///  - format(1.2345, 0) == '1'
  static String format(double number, {int precision = 1}) {
    if (number == null) return null;
    return NumberFormat('0.' + ('#' * precision)).format(number);
  }
}
