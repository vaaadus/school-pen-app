import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

/// Formats dates & times.
class DateFormatter {
  /// Day of week, year, month, day, hour, minute.
  static String formatFull(DateTime dateTime) {
    if (dateTime == null) return '';
    return DateFormat.E().add_yMd().add_Hm().format(dateTime);
  }

  /// If [dateTime] is today, then: 'Today, H:mm'
  /// If [dateTime] is tomorrow, then: 'Tomorrow, H:mm'
  /// else: see [formatFull].
  static String formatRelativeOrFullDate(BuildContext context, DateTime dateTime) {
    if (dateTime == null) return '';

    final s = S.of(context);
    if (dateTime.isToday) {
      return sprintf(s.formatter_relativeDate, [s.common_today, DateFormat.Hm().format(dateTime)]);
    } else if (dateTime.isTomorrow) {
      return sprintf(s.formatter_relativeDate, [s.common_tomorrow, DateFormat.Hm().format(dateTime)]);
    } else if (dateTime.isYesterday) {
      return sprintf(s.formatter_relativeDate, [s.common_yesterday, DateFormat.Hm().format(dateTime)]);
    } else {
      return formatFull(dateTime);
    }
  }

  /// If [dateTime] is today, then: 'Today, H:mm'
  /// If [dateTime] is tomorrow, then: 'Tomorrow, H:mm'
  /// else: see [formatShortDayWithDate].
  static String formatFullRelativeOrShortDate(BuildContext context, DateTime dateTime) {
    if (dateTime == null) return '';

    final s = S.of(context);
    if (dateTime.isToday) {
      return sprintf(s.formatter_relativeDate, [s.common_today, DateFormat.Hm().format(dateTime)]);
    } else if (dateTime.isTomorrow) {
      return sprintf(s.formatter_relativeDate, [s.common_tomorrow, DateFormat.Hm().format(dateTime)]);
    } else if (dateTime.isYesterday) {
      return sprintf(s.formatter_relativeDate, [s.common_yesterday, DateFormat.Hm().format(dateTime)]);
    } else {
      return formatShortDayWithDate(dateTime);
    }
  }

  /// If [dateTime] is today, then: 'Today'
  /// If [dateTime] is tomorrow, then: 'Tomorrow'
  /// else: see [formatShortDayWithDate].
  static String formatRelativeOrShortDate(BuildContext context, DateTime dateTime) {
    if (dateTime == null) return '';

    final s = S.of(context);
    if (dateTime.isToday) {
      return s.common_today;
    } else if (dateTime.isTomorrow) {
      return s.common_tomorrow;
    } else if (dateTime.isYesterday) {
      return s.common_yesterday;
    } else {
      return formatShortDayWithDate(dateTime);
    }
  }

  /// Day of week, year, month, day.
  static String formatShortDayWithDate(DateTime dateTime) {
    if (dateTime == null) return '';
    return DateFormat.E().add_yMd().format(dateTime);
  }

  /// If the year is the current year: then day, month.
  /// Else: year, day, month.
  static String formatShortRecentDate(DateTime dateTime) {
    if (dateTime == null) return '';
    if (dateTime.year == DateTime.now().year) {
      return DateFormat.d().add_MMM().format(dateTime);
    } else {
      return DateFormat.yMd().format(dateTime);
    }
  }

  /// Year, month, day.
  static String formatDate(DateTime dateTime) {
    if (dateTime == null) return '';
    return DateFormat.yMd().format(dateTime);
  }

  /// Hour, minute.
  static String formatTime(DateTime dateTime) {
    if (dateTime == null) return '';
    return DateFormat.Hm().format(dateTime);
  }
}
