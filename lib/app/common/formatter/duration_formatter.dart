import 'package:duration/duration.dart';
import 'package:duration/locale.dart';
import 'package:flutter/cupertino.dart';

class DurationFormatter {
  /// Formats the duration with full units (e.g. 3 days),
  /// but skips less important parts if they are small.
  static String formatWhole(BuildContext context, Duration duration) {
    return prettyDuration(
      duration,
      locale: _getDurationLocale(context),
      tersity: _getDurationTersity(duration),
    );
  }

  static DurationLocale _getDurationLocale(BuildContext context) {
    Locale locale = Localizations.localeOf(context);
    return DurationLocale.fromLanguageCode(locale.languageCode) ?? DurationLocale.fromLanguageCode('en');
  }

  static DurationTersity _getDurationTersity(Duration duration) {
    if (duration.inDays >= 7) {
      return DurationTersity.week;
    } else if (duration.inDays >= 2) {
      return DurationTersity.day;
    } else if (duration.inHours >= 2) {
      return DurationTersity.hour;
    } else if (duration.inMinutes >= 1) {
      return DurationTersity.minute;
    } else {
      return DurationTersity.second;
    }
  }
}
