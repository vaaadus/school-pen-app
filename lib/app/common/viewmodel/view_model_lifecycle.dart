import 'package:flutter/cupertino.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';

/// Handles view model lifecycle.
@optionalTypeArgs
mixin ViewModelLifecycle<VM extends ViewModel, T extends LifecycleWidget> on LifecycleWidgetState<T> {
  ViewModel _viewModel;

  VM get viewModel => _viewModel;

  /// Provide the view model from client class to the mixin.
  VM provideViewModel();

  @override
  void initState() {
    _viewModel = provideViewModel();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _viewModel.dispose();
  }

  @override
  void onStart() {
    super.onStart();
    _viewModel.onStart();
  }

  @override
  void onStop() {
    _viewModel.onStop();
    super.onStop();
  }

  @override
  void onResume() {
    super.onResume();
    _viewModel.onResume();
  }

  @override
  void onPause() {
    _viewModel.onPause();
    super.onPause();
  }
}
