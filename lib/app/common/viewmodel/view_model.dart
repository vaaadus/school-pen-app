import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';

/// View Model base class.
abstract class ViewModel<Model, Signal> {
  final BehaviorSubject<Model> _models = BehaviorSubject<Model>();
  final PublishSubject<Signal> _signals = PublishSubject<Signal>();
  final List<Signal> _consumedSignals = [];

  /// Stream next view model states.
  Stream<Model> get models => _models.stream;

  /// Returns last model or null.
  Future<Model> get lastModel async {
    try {
      return _models.value;
    } catch (e) {
      return null;
    }
  }

  /// Streams next view model signals.
  Stream<Signal> get signals => _signals.stream;

  /// Called once the application is active.
  @mustCallSuper
  void onStart() {}

  /// Called when the application becomes inactive.
  @mustCallSuper
  void onStop() {}

  /// Called whenever a relevant page becomes visible & active.
  @mustCallSuper
  void onResume() {}

  /// Called whenever a relevant page becomes invisible & inactive.
  @mustCallSuper
  void onPause() {}

  /// Called whenever a relevant page is disposed.
  @mustCallSuper
  void dispose() {
    _models.close();
    _signals.close();
    _consumedSignals.clear();
  }

  /// Emit a new model to be rendered by the view.
  @protected
  void emitModel(Model model) {
    if (!_models.isClosed && model != null) {
      _models.add(model);
    }
  }

  /// Emit a new signal to be consumed by the view.
  @protected
  void emitSignal(Signal signal) {
    if (!_signals.isClosed && signal != null) {
      _signals.add(signal);
    }
  }

  /// Emit a new signal to be consumed by the view.
  /// If the signal was emitted in the past then it will be skipped.
  ///
  /// Useful for stateful entities, such as navigation, etc.
  @protected
  void emitSingleSignal(Signal signal) {
    if (!_signals.isClosed && signal != null && !_consumedSignals.contains(signal)) {
      _consumedSignals.add(signal);
      _signals.add(signal);
    }
  }
}
