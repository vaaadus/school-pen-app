import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';

typedef ModelBuilder<Model> = Widget Function(BuildContext context, Model model);
typedef ModelListener<Model> = void Function(BuildContext context, Model model);
typedef SignalConsumer<Signal> = void Function(BuildContext context, Signal signal);

/// Utility widget to ease subscribing & building widgets from view models & signals.
class ViewModelBuilder<Model, Signal> extends StatefulWidget {
  const ViewModelBuilder({
    Key key,
    @required this.viewModel,
    @required this.builder,
    this.listener,
    this.onSignal,
  }) : super(key: key);

  final ViewModel<Model, Signal> viewModel;
  final ModelBuilder<Model> builder;
  final ModelListener<Model> listener;
  final SignalConsumer<Signal> onSignal;

  @override
  State<StatefulWidget> createState() => _ViewModelBuilderState<Model, Signal>();
}

class _ViewModelBuilderState<Model, Signal> extends State<ViewModelBuilder<Model, Signal>> {
  StreamSubscription _signalSubscription;

  @override
  void initState() {
    super.initState();
    _subscribeSignals();
  }

  @override
  void didUpdateWidget(ViewModelBuilder oldWidget) {
    super.didUpdateWidget(oldWidget);
    _unsubscribeSignals();
    _subscribeSignals();
  }

  @override
  void dispose() {
    _unsubscribeSignals();
    super.dispose();
  }

  void _subscribeSignals() {
    _signalSubscription = widget.viewModel.signals.listen((Signal signal) {
      if (context != null && signal != null && widget.onSignal != null) {
        widget.onSignal(context, signal);
      }
    });
  }

  void _unsubscribeSignals() {
    _signalSubscription?.cancel();
    _signalSubscription = null;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: widget.viewModel.models,
      builder: (BuildContext context, AsyncSnapshot<Model> snapshot) {
        final Model model = snapshot.data;
        if (model != null) {
          widget.listener?.call(context, model);
          return widget.builder(context, model);
        } else {
          return Container();
        }
      },
    );
  }
}
