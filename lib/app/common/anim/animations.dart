import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

/// Common utils related to animations.
class Animations {
  static const Duration microDuration = Duration(milliseconds: 150);
  static const Duration shortDuration = Duration(milliseconds: 300);
  static const Duration mediumDuration = Duration(milliseconds: 450);
  static const Duration longDuration = Duration(milliseconds: 600);

  static const TransitionBounds scaleOnClickBounds = TransitionBounds(lower: 0.85, upper: 1.0);
}

class TransitionBounds extends Equatable {
  final double lower;
  final double upper;

  const TransitionBounds({@required this.lower, @required this.upper});

  @override
  List<Object> get props => [lower, upper];
}
