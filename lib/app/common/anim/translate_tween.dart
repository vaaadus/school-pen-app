import 'package:flutter/widgets.dart';

/// Transforms the interpolation signal <0.0, 1.0> in a way
/// that it renders range <0.0, 1.0> for the source signal between <start, end>.
class TranslateTween extends Animatable<double> {
  const TranslateTween({@required this.start, @required this.end})
      : assert(start <= end),
        assert(start >= 0),
        assert(start <= 1),
        assert(end >= 0),
        assert(end <= 1);

  final double start;
  final double end;

  @override
  double transform(double t) {
    if (start == end) return t <= start ? 0 : 1;

    final double value = (t - start) / (end - start);
    if (value >= 1) return 1;
    if (value <= 0) return 0;
    return value;
  }
}
