import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/domain/common/env_options.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:share/share.dart';
import 'package:store_redirect/store_redirect.dart';
import 'package:url_launcher/url_launcher.dart';

/// Common navigation intents to other apps/services.
class Intents {
  static const Logger _logger = Logger('Intents');

  static void openBrowser(String url) async {
    if (!url.startsWith(RegExp('https?://'))) {
      url = 'https://$url';
    }

    await _launchUrl(url);
  }

  static void openMap(String address) async {
    await MapsLauncher.launchQuery(address);
  }

  static void sendEmail({String email = '', String subject = '', String body = ''}) async {
    final String url = _buildMailtoUrl(email, subject, body);
    await _launchUrl(url);
  }

  static void callNumber(String number) async {
    final String url = 'tel://$number';
    await _launchUrl(url);
  }

  static bool isShareTextSupported() {
    return !kIsWeb && (Platform.isAndroid || Platform.isIOS);
  }

  static void shareText(String text) async {
    await Share.share(text);
  }

  static Future<bool> isStoreSupported() async {
    switch (EnvOptions.store) {
      case TargetStore.play:
      case TargetStore.iOS:
        return true;
      case TargetStore.amazon:
        return canLaunch(R.amazonAppUrl);
      case TargetStore.macOS:
      case TargetStore.windows:
      case TargetStore.linux:
      case TargetStore.web:
        return false;
    }

    throw ArgumentError('Unsupported store: ${EnvOptions.store}');
  }

  static Future<void> openStore() async {
    try {
      switch (EnvOptions.store) {
        case TargetStore.play:
        case TargetStore.iOS:
          await StoreRedirect.redirect();
          break;

        case TargetStore.amazon:
          await _launchUrl(R.amazonAppUrl);
          break;

        case TargetStore.macOS:
        case TargetStore.windows:
        case TargetStore.linux:
        case TargetStore.web:
          break;
      }
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  static void openFacebookPage() async {
    final String pageId = 'School-Timetable-100141361720881';
    final String websiteUrl = 'https://www.facebook.com/' + pageId;
    final String fbNewProtocolUrl = 'fb://facewebmodal/f?href=' + websiteUrl;
    final String fbProtocolUrl = 'fb://page?id=' + pageId;

    try {
      if (await _launchUrl(fbNewProtocolUrl)) {
        return;
      }

      if (await _launchUrl(fbProtocolUrl)) {
        return;
      }

      if (await _launchUrl(websiteUrl)) {
        return;
      }
    } catch (error) {
      await launch(websiteUrl, forceSafariVC: false);
    }
  }

  // TODO: amazon
  static void openManageInAppSubscriptionsPage() async {
    String url;
    if (Platform.isAndroid) {
      url = 'https://play.google.com/store/account/subscriptions';
    } else if (Platform.isIOS) {
      url = 'https://apps.apple.com/account/subscriptions';
    } else {
      throw ArgumentError('Not supported platform: ${Platform.operatingSystem}');
    }

    await _launchUrl(url);
  }

  static Future<bool> _launchUrl(String url) async {
    if (await canLaunch(url)) {
      return await launch(url, enableJavaScript: true);
    } else {
      _logger.log("Couldn't launch url: $url");
      return false;
    }
  }

  static String _buildMailtoUrl(String email, String subject, String body) {
    return 'mailto:$email?subject=$subject&body=$body';
  }
}
