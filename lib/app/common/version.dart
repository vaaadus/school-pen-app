import 'package:school_pen/domain/common/env_options.dart';

/// Provides the app version.
abstract class AppVersionProvider {
  Future<String> get version;
}

class EnvironmentVariableAppInfoProvider implements AppVersionProvider {
  @override
  Future<String> get version async => EnvOptions.appVersion;
}
