import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:injector/injector.dart';
import 'package:school_pen/app/common/version.dart';
import 'package:school_pen/app/service/applaunch/app_launch_manager.dart';
import 'package:school_pen/app/service/applaunch/app_launch_manager_impl.dart';
import 'package:school_pen/app/service/widget/agenda_widget.dart';
import 'package:school_pen/domain/ads/ad_manager.dart';
import 'package:school_pen/domain/ads/admob_manager.dart';
import 'package:school_pen/domain/ads/empty_ad_mnager.dart';
import 'package:school_pen/domain/ads/repository/adservice_location_repository.dart';
import 'package:school_pen/domain/ads/repository/location_repository.dart';
import 'package:school_pen/domain/agenda/agenda_manager.dart';
import 'package:school_pen/domain/agenda/agenda_manager_impl.dart';
import 'package:school_pen/domain/agenda/notification/agenda_notification_builder.dart';
import 'package:school_pen/domain/agenda/notification/timetable_notification_builder.dart';
import 'package:school_pen/domain/analytics/analytics_engine.dart';
import 'package:school_pen/domain/analytics/analytics_manager.dart';
import 'package:school_pen/domain/analytics/engines/facebook_analytics_engine.dart';
import 'package:school_pen/domain/analytics/engines/firebase_analytics_engine.dart';
import 'package:school_pen/domain/analytics/engines/logging_analytics_engine.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/common/env_options.dart';
import 'package:school_pen/domain/common/image_resizer.dart';
import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/course_manager_impl.dart';
import 'package:school_pen/domain/course/repository/course_repository.dart';
import 'package:school_pen/domain/course/repository/parse/parse_course_repository.dart';
import 'package:school_pen/domain/course/repository/sembast/sembast_course_repository.dart';
import 'package:school_pen/domain/diagnosis/diagnosis_manager.dart';
import 'package:school_pen/domain/exam/exam_manager.dart';
import 'package:school_pen/domain/exam/exam_manager_impl.dart';
import 'package:school_pen/domain/exam/notification/exam_notification_builder.dart';
import 'package:school_pen/domain/exam/repository/exam_repository.dart';
import 'package:school_pen/domain/exam/repository/parse/parse_exam_repository.dart';
import 'package:school_pen/domain/exam/repository/sembast/sembast_exam_repository.dart';
import 'package:school_pen/domain/files/file_manager.dart';
import 'package:school_pen/domain/files/filesystem_file_manager.dart';
import 'package:school_pen/domain/files/network_file_manager.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/grade/grade_manager_impl.dart';
import 'package:school_pen/domain/grade/repository/category/grade_category_repository.dart';
import 'package:school_pen/domain/grade/repository/category/parse/parse_grade_category_repository.dart';
import 'package:school_pen/domain/grade/repository/category/sembast/sembast_grade_category_repository.dart';
import 'package:school_pen/domain/grade/repository/grade/grade_repository.dart';
import 'package:school_pen/domain/grade/repository/grade/parse/parse_grade_repository.dart';
import 'package:school_pen/domain/grade/repository/grade/sembast/sembast_grade_repository.dart';
import 'package:school_pen/domain/homework/homework_manager.dart';
import 'package:school_pen/domain/homework/homework_manager_impl.dart';
import 'package:school_pen/domain/homework/notification/homework_notification_builder.dart';
import 'package:school_pen/domain/homework/repository/homework_repository.dart';
import 'package:school_pen/domain/homework/repository/parse/parse_homework_repository.dart';
import 'package:school_pen/domain/homework/repository/sembast/sembast_homework_repository.dart';
import 'package:school_pen/domain/inappsubscription/empty_inapp_subscription_manager.dart';
import 'package:school_pen/domain/inappsubscription/inapp_product_offer_mapper.dart';
import 'package:school_pen/domain/inappsubscription/inapp_subscription_manager.dart';
import 'package:school_pen/domain/inappsubscription/inapp_subscription_manager_impl.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager_impl.dart';
import 'package:school_pen/domain/network/network_manager.dart';
import 'package:school_pen/domain/network/network_manager_impl.dart';
import 'package:school_pen/domain/note/note_manager.dart';
import 'package:school_pen/domain/note/note_manager_impl.dart';
import 'package:school_pen/domain/note/notification/note_notification_builder.dart';
import 'package:school_pen/domain/note/repository/note_repository.dart';
import 'package:school_pen/domain/note/repository/parse/parse_note_repository.dart';
import 'package:school_pen/domain/note/repository/sembast/sembast_note_repository.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/notification/notification_manager_impl.dart';
import 'package:school_pen/domain/notification/repository/empty_notification_repository.dart';
import 'package:school_pen/domain/notification/repository/notification_channel_locator.dart';
import 'package:school_pen/domain/notification/repository/notification_history.dart';
import 'package:school_pen/domain/notification/repository/notification_repository.dart';
import 'package:school_pen/domain/notification/repository/notification_repository_impl.dart';
import 'package:school_pen/domain/notification/repository/notification_storage.dart';
import 'package:school_pen/domain/preferences/impl/shared_preferences_impl.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/pushnotification/engine/empty/empty_push_notification_engine.dart';
import 'package:school_pen/domain/pushnotification/engine/firebase/firebase_push_notification_engine.dart';
import 'package:school_pen/domain/pushnotification/engine/push_notification_engine.dart';
import 'package:school_pen/domain/pushnotification/push_notification_manager.dart';
import 'package:school_pen/domain/pushnotification/push_notification_manager_impl.dart';
import 'package:school_pen/domain/pushnotification/repository/parse_push_notification_repository.dart';
import 'package:school_pen/domain/pushnotification/repository/push_notification_repository.dart';
import 'package:school_pen/domain/quickactions/empty_quick_actions_manager.dart';
import 'package:school_pen/domain/quickactions/quick_actions_manager.dart';
import 'package:school_pen/domain/quickactions/quick_actions_manager_impl.dart';
import 'package:school_pen/domain/quickactions/repository/quick_actions_history.dart';
import 'package:school_pen/domain/quickactions/repository/quick_actions_locator.dart';
import 'package:school_pen/domain/quickactions/repository/quick_actions_repository.dart';
import 'package:school_pen/domain/rateapp/rate_app_manager.dart';
import 'package:school_pen/domain/rateapp/rate_app_manager_impl.dart';
import 'package:school_pen/domain/reminder/notification/reminder_notification_builder.dart';
import 'package:school_pen/domain/reminder/reminder_manager.dart';
import 'package:school_pen/domain/reminder/reminder_manager_impl.dart';
import 'package:school_pen/domain/reminder/repository/parse/parse_reminder_repository.dart';
import 'package:school_pen/domain/reminder/repository/reminder_repository.dart';
import 'package:school_pen/domain/reminder/repository/sembast/sembast_reminder_repository.dart';
import 'package:school_pen/domain/remoteconfig/parse_remote_config_manager.dart';
import 'package:school_pen/domain/remoteconfig/remote_config_manager.dart';
import 'package:school_pen/domain/schoolyear/repository/parse/parse_school_year_repository.dart';
import 'package:school_pen/domain/schoolyear/repository/school_year_repository.dart';
import 'package:school_pen/domain/schoolyear/repository/sembast/sembast_school_year_repository.dart';
import 'package:school_pen/domain/schoolyear/school_year_locator.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager_impl.dart';
import 'package:school_pen/domain/semester/repository/parse/parse_semester_repository.dart';
import 'package:school_pen/domain/semester/repository/sembast/sembast_semester_repository.dart';
import 'package:school_pen/domain/semester/repository/semester_repository.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:school_pen/domain/semester/semester_manager_impl.dart';
import 'package:school_pen/domain/smartnote/smart_note_manager.dart';
import 'package:school_pen/domain/smartnote/smart_note_manager_impl.dart';
import 'package:school_pen/domain/sync/parse_sync_manager.dart';
import 'package:school_pen/domain/sync/repository/parse_sync_repository.dart';
import 'package:school_pen/domain/sync/repository/sync_repository.dart';
import 'package:school_pen/domain/sync/sync_manager.dart';
import 'package:school_pen/domain/teacher/repository/parse/parse_teacher_repository.dart';
import 'package:school_pen/domain/teacher/repository/sembast/sembast_teacher_repository.dart';
import 'package:school_pen/domain/teacher/repository/teacher_repository.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';
import 'package:school_pen/domain/teacher/teacher_manager_impl.dart';
import 'package:school_pen/domain/themes/themes_manager.dart';
import 'package:school_pen/domain/themes/themes_manager_impl.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/custom_timetable_event_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/parse/parse_custom_timetable_event_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/sembast/sembast_custom_timetable_event_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/custom_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/parse/parse_custom_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/sembast/sembast_custom_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/public/parse/parse_public_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/public/public_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/public/public_timetable_repository_facade.dart';
import 'package:school_pen/domain/timetable/repository/public/sembast/sembast_public_timetable_repository.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';
import 'package:school_pen/domain/timetable/timetable_manager_impl.dart';
import 'package:school_pen/domain/user/repository/parse_user_repository.dart';
import 'package:school_pen/domain/user/repository/user_repository.dart';
import 'package:school_pen/domain/user/user_manager.dart';
import 'package:school_pen/domain/user/user_manager_impl.dart';
import 'package:school_pen/domain/userconfig/repository/parse/parse_user_config_repository.dart';
import 'package:school_pen/domain/userconfig/repository/sembast/sembast_user_config_repository.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:school_pen/domain/work/repository/android_work_repository.dart';
import 'package:school_pen/domain/work/repository/empty_work_repository.dart';
import 'package:school_pen/domain/work/repository/ios_work_repository.dart';
import 'package:school_pen/domain/work/repository/work_repository.dart';
import 'package:school_pen/domain/work/work_manager.dart';
import 'package:school_pen/domain/work/work_manager_impl.dart';

/// Dependency injection.
/// Usage:
///
///     Injection.get<Analytics>()
///     or:
///     Analytics analytics = Injection.get();
///
class Injection {
  static final Injector _injector = Injector.appInstance;

  static T get<T>() => _injector.get();

  /// Configure all dependencies in the app. Must be called before app startup.
  static Future<void> configureDependencies() => Future.wait([
        _registerPreferencesDependencies(),
        _registerSlidingWindowDependencies(),
        _registerUserDependencies(),
        _registerRemoteConfigDependencies(),
        _registerDateTimeDependencies(),
        _registerLifecycleDependencies(),
        _registerNetworkDependencies(),
        _registerTimetableDependencies(),
        _registerExamDependencies(),
        _registerHomeworkDependencies(),
        _registerReminderDependencies(),
        _registerPushNotificationDependencies(),
        _registerAdDependencies(),
        _registerInAppSubscriptionDependencies(),
        _registerRateAppDependencies(),
        _registerNotificationDependencies(),
        _registerSmartNoteDependencies(),
        _registerAnalyticsDependencies(),
        _registerDiagnosisDependencies(),
        _registerUserConfigDependencies(),
        _registerNoteDependencies(),
        _registerSchoolYearDependencies(),
        _registerSemesterDependencies(),
        _registerTeacherDependencies(),
        _registerCourseDependencies(),
        _registerGradeDependencies(),
        _registerQuickActionsDependencies(),
        _registerAppLaunchDependencies(),
        _registerSyncDependencies(),
        _registerFilesDependencies(),
        _registerThemesDependencies(),
        _registerAppVersionDependencies(),
        _registerWorkDependencies(),
        _registerAgendaDependencies(),
        _registerWidgetDependencies(),
      ]);

  static Future<void> _registerPreferencesDependencies() async {
    final SharedPreferencesImpl sharedPrefs = await SharedPreferencesImpl.create();
    _registerSingleton<Preferences>(() => sharedPrefs);
  }

  static Future<void> _registerSlidingWindowDependencies() async {
    _registerDependency<SlidingWindow>(() => PersistableSlidingWindow(get(), get()));
  }

  static Future<void> _registerUserDependencies() async {
    _registerSingleton<UserRepository>(() => ParseUserRepository(get(), get()));
    _registerSingleton<UserManager>(() => UserManagerImpl(get(), get(), get(), get(), ImageResizer()));
  }

  static Future<void> _registerRemoteConfigDependencies() async {
    _registerSingleton<RemoteConfigManager>(() => ParseRemoteConfigManager(get(), get(), !kReleaseMode));
  }

  static Future<void> _registerDateTimeDependencies() async {
    _registerSingleton<DateTimeProvider>(() => DefaultDateTimeProvider());
  }

  static Future<void> _registerLifecycleDependencies() async {
    _registerSingleton<LifecycleManager>(() => LifecycleManagerImpl());
  }

  static Future<void> _registerNetworkDependencies() async {
    _registerSingleton(() => Connectivity());
    _registerSingleton<NetworkManager>(() => NetworkManagerImpl(get()));
  }

  static Future<void> _registerTimetableDependencies() async {
    _registerSingleton(() => SembastPublicTimetableRepository());
    _registerSingleton(() => ParsePublicTimetableRepository(get()));
    _registerSingleton(() => PublicTimetableRepositoryFacade(get(), get()));
    _registerSingleton<PublicTimetableRepository>(() => get<PublicTimetableRepositoryFacade>());

    _registerSingleton(() => SembastCustomTimetableRepository());
    _registerSingleton(() => ParseCustomTimetableRepository(get<SembastCustomTimetableRepository>()));
    _registerSingleton<CustomTimetableRepository>(() => get<ParseCustomTimetableRepository>());

    _registerSingleton(() => SembastCustomTimetableEventRepository());
    _registerSingleton(() => ParseCustomTimetableEventRepository(get<SembastCustomTimetableEventRepository>()));
    _registerSingleton<CustomTimetableEventRepository>(() => get<ParseCustomTimetableEventRepository>());

    _registerSingleton<TimetableManager>(() => TimetableManagerImpl(get(), get(), get(), get(), get(), get(), get()));
  }

  static Future<void> _registerExamDependencies() async {
    _registerSingleton(() => SembastExamRepository());
    _registerSingleton(() => ParseExamRepository(get<SembastExamRepository>()));
    _registerSingleton<ExamRepository>(() => get<ParseExamRepository>());
    _registerDependency(() => ExamNotificationBuilder());
    _registerSingleton<ExamManager>(() => ExamManagerImpl(get(), get(), get(), get(), get(), get()));
  }

  static Future<void> _registerHomeworkDependencies() async {
    _registerSingleton(() => SembastHomeworkRepository());
    _registerSingleton(() => ParseHomeworkRepository(get<SembastHomeworkRepository>()));
    _registerSingleton<HomeworkRepository>(() => get<ParseHomeworkRepository>());
    _registerDependency(() => HomeworkNotificationBuilder());
    _registerSingleton<HomeworkManager>(() => HomeworkManagerImpl(get(), get(), get(), get(), get(), get()));
  }

  static Future<void> _registerReminderDependencies() async {
    _registerSingleton(() => SembastReminderRepository());
    _registerSingleton(() => ParseReminderRepository(get<SembastReminderRepository>()));
    _registerSingleton<ReminderRepository>(() => get<ParseReminderRepository>());
    _registerDependency(() => ReminderNotificationBuilder());
    _registerSingleton<ReminderManager>(() => ReminderManagerImpl(get(), get(), get(), get(), get()));
  }

  static Future<void> _registerPushNotificationDependencies() async {
    _registerSingleton<PushNotificationEngine>(() {
      if (EnvOptions.store == TargetStore.play) {
        return FirebasePushNotificationEngine();
      } else {
        // TODO
        return EmptyPushNotificationEngine();
      }
    });

    _registerSingleton<PushNotificationRepository>(() => ParsePushNotificationRepository());
    _registerSingleton<PushNotificationManager>(() {
      return PushNotificationManagerImpl(get(), get(), get(), get(), get(), get());
    });
  }

  static Future<void> _registerAdDependencies() async {
    _registerSingleton<LocationRepository>(() => AdServiceLocationRepository());

    _registerSingleton<AdmobConfig>(() {
      if (Platform.isAndroid && EnvOptions.useProductionEnv) return AndroidReleaseAdmobConfig();
      if (Platform.isAndroid && !EnvOptions.useProductionEnv) return AndroidDebugAdmobConfig();
      if (Platform.isIOS && EnvOptions.useProductionEnv) return IOSReleaseAdmobConfig();
      if (Platform.isIOS && !EnvOptions.useProductionEnv) return IOSDebugAdmobConfig();
      throw UnsupportedError('The platform or kReleaseMode not supported');
    });

    _registerSingleton<AdManager>(() {
      if (!kIsWeb && (Platform.isIOS || Platform.isAndroid)) {
        return AdmobManager(get(), get(), get(), get(), get());
      } else {
        return EmptyAdManager(get(), get(), get());
      }
    });
  }

  static Future<void> _registerInAppSubscriptionDependencies() async {
    _registerSingleton<InAppSubscriptionManager>(() {
      if (EnvOptions.store == TargetStore.play) {
        InAppPurchaseConnection.enablePendingPurchases();
        return InAppSubscriptionManagerImpl(InAppPurchaseConnection.instance, InAppProductOfferMapper(), get(), get());
      } else {
        // TODO
        return EmptyInAppSubscriptionManager();
      }
    });
  }

  static Future<void> _registerRateAppDependencies() async {
    _registerSingleton<RateAppManager>(() => RateAppManagerImpl(get(), get()));
  }

  static Future<void> _registerNotificationDependencies() async {
    _registerDependency(() => NotificationChannelLocator());
    _registerSingleton(() => NotificationHistory());
    _registerSingleton(() => NotificationStorage());
    _registerSingleton<NotificationRepository>(() {
      if (kIsWeb) {
        // TODO: implement
        return EmptyNotificationRepository();
      } else {
        return NotificationRepositoryImpl(get(), get());
      }
    });

    _registerSingleton<NotificationManager>(() {
      return NotificationManagerImpl(get(), get(), get(), get(), get());
    });
  }

  static Future<void> _registerSmartNoteDependencies() async {
    _registerSingleton<SmartNoteManager>(() {
      return SmartNoteManagerImpl(get(), get(), get(), get());
    });
  }

  static Future<void> _registerAnalyticsDependencies() async {
    _registerSingleton(() => FirebaseAnalytics());
    _registerSingleton(() => FacebookAppEvents());
    _registerSingleton(() => RouteObserver());

    final bool supportsFacebook = !kIsWeb && (Platform.isAndroid || Platform.isIOS);
    final bool supportsFirebase = kIsWeb || EnvOptions.store == TargetStore.play || Platform.isIOS;

    if (supportsFacebook) {
      final FacebookAppEvents facebook = get();
      await facebook.setAutoLogAppEventsEnabled(EnvOptions.useProductionEnv);
    }

    if (supportsFirebase) {
      final FirebaseAnalytics analytics = get();
      await analytics.setAnalyticsCollectionEnabled(EnvOptions.useProductionEnv);
    }

    _registerSingleton<List<AnalyticsEngine>>(() => [
          if (!EnvOptions.useProductionEnv) LoggingAnalyticsEngine(),
          if (EnvOptions.useProductionEnv && supportsFirebase) FirebaseAnalyticsEngine(get()),
          if (EnvOptions.useProductionEnv && supportsFacebook) FacebookAnalyticsEngine(get()),
        ]);

    _registerSingleton<AnalyticsManager>(() {
      return AnalyticsManager(get(), get(), get());
    });

    _registerDependency<List<NavigatorObserver>>(() => [
          if (kReleaseMode && supportsFirebase) FirebaseAnalyticsObserver(analytics: get()),
          get<RouteObserver>(),
        ]);
  }

  static Future<void> _registerDiagnosisDependencies() async {
    _registerSingleton<DiagnosisManager>(() => DiagnosisManager(get()));
  }

  static Future<void> _registerUserConfigDependencies() async {
    _registerSingleton(() => SembastUserConfigRepository(get()));
    _registerSingleton(() => ParseUserConfigRepository(get<SembastUserConfigRepository>()));
    _registerSingleton<UserConfigRepository>(() => get<ParseUserConfigRepository>());
  }

  static Future<void> _registerNoteDependencies() async {
    _registerSingleton(() => SembastNoteRepository());
    _registerSingleton(() => ParseNoteRepository(get<SembastNoteRepository>()));
    _registerSingleton<NoteRepository>(() => get<ParseNoteRepository>());
    _registerSingleton(() => NoteNotificationBuilder());

    _registerSingleton<NoteManager>(() => NoteManagerImpl(get(), get(), get(), get()));
  }

  static Future<void> _registerSchoolYearDependencies() async {
    _registerDependency(() => SchoolYearLocator());

    _registerSingleton(() => SembastSchoolYearRepository());
    _registerSingleton(() => ParseSchoolYearRepository(get<SembastSchoolYearRepository>()));
    _registerSingleton<SchoolYearRepository>(() => get<ParseSchoolYearRepository>());
    _registerSingleton<SchoolYearManager>(() => SchoolYearManagerImpl(get(), get(), get(), get()));
  }

  static Future<void> _registerSemesterDependencies() async {
    _registerSingleton(() => SembastSemesterRepository());
    _registerSingleton(() => ParseSemesterRepository(get<SembastSemesterRepository>()));
    _registerSingleton<SemesterRepository>(() => get<ParseSemesterRepository>());
    _registerDependency<DefaultSemesterNameGenerator>(() => DefaultSemesterNameGenerator());
    _registerSingleton<SemesterManager>(() => SemesterManagerImpl(get(), get(), get(), get(), get(), get()));
  }

  static Future<void> _registerTeacherDependencies() async {
    _registerSingleton(() => SembastTeacherRepository());
    _registerSingleton(() => ParseTeacherRepository(get<SembastTeacherRepository>()));
    _registerSingleton<TeacherRepository>(() => get<ParseTeacherRepository>());
    _registerSingleton<TeacherManager>(() => TeacherManagerImpl(get(), get()));
  }

  static Future<void> _registerCourseDependencies() async {
    _registerSingleton(() => SembastCourseRepository());
    _registerSingleton(() => ParseCourseRepository(get<SembastCourseRepository>()));
    _registerSingleton<CourseRepository>(() => get<ParseCourseRepository>());
    _registerSingleton<CourseManager>(() => CourseManagerImpl(get(), get(), get()));
  }

  static Future<void> _registerGradeDependencies() async {
    _registerSingleton(() => SembastGradeRepository());
    _registerSingleton(() => ParseGradeRepository(get<SembastGradeRepository>()));
    _registerSingleton<GradeRepository>(() => get<ParseGradeRepository>());

    _registerSingleton(() => SembastGradeCategoryRepository());
    _registerSingleton(() => ParseGradeCategoryRepository(get<SembastGradeCategoryRepository>()));
    _registerSingleton<GradeCategoryRepository>(() => get<ParseGradeCategoryRepository>());

    _registerSingleton<GradeManager>(() => GradeManagerImpl(get(), get(), get(), get(), get()));
  }

  static Future<void> _registerQuickActionsDependencies() async {
    _registerDependency(() => QuickActionsLocator());
    _registerSingleton(() => QuickActionsRepository(get()));
    _registerSingleton(() => QuickActionsHistory());

    _registerSingleton<QuickActionsManager>(() {
      if (!kIsWeb && (Platform.isAndroid || Platform.isIOS)) {
        return QuickActionsManagerImpl(get(), get(), get(), get());
      } else {
        return EmptyQuickActionsManager();
      }
    });
  }

  static Future<void> _registerAppLaunchDependencies() async {
    _registerDependency<AppLaunchManager>(() => AppLaunchManagerImpl(get(), get(), get(), get()));
  }

  static Future<void> _registerSyncDependencies() async {
    _registerSingleton<SyncRepository>(() => ParseSyncRepository(get()));
    _registerSingleton<SyncManager>(() => ParseSyncManager(get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(),
        get(), get(), get(), get(), get(), get(), get(), get(), get()));
  }

  static Future<void> _registerFilesDependencies() async {
    _registerSingleton<RemoteFileManager>(() {
      if (kIsWeb) {
        return NetworkFileManager(get());
      } else {
        return FileSystemFileManager(get(), get(), get());
      }
    });
  }

  static Future<void> _registerThemesDependencies() async {
    _registerSingleton<ThemesManager>(() => ThemesManagerImpl(get(), get()));
  }

  static Future<void> _registerAppVersionDependencies() async {
    _registerSingleton<AppVersionProvider>(() => EnvironmentVariableAppInfoProvider());
  }

  static Future<void> _registerWorkDependencies() async {
    _registerSingleton<WorkRepository>(() {
      if (kIsWeb) return EmptyWorkRepository();
      if (Platform.isAndroid) return AndroidWorkRepository(get());
      if (Platform.isIOS) return IosWorkRepository();
      return EmptyWorkRepository();
    });

    _registerSingleton<WorkManager>(() => WorkManagerImpl(get(), get(), get()));
  }

  static Future<void> _registerAgendaDependencies() async {
    _registerDependency(() => AgendaNotificationBuilder());
    _registerDependency(() => TimetableNotificationBuilder());
    _registerSingleton(() => AgendaNotificationsManager(get(), get(), get(), get(), get()));
    _registerSingleton<AgendaManager>(() {
      return AgendaManagerImpl(get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get());
    });
  }

  static Future<void> _registerWidgetDependencies() async {
    _registerSingleton(() => AgendaWidget(get()));
  }

  static void _registerSingleton<T>(T Function() builder) => _injector.registerSingleton(builder);

  static void _registerDependency<T>(T Function() builder) => _injector.registerDependency(builder);
}
