import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/homework/homework_listener.dart';
import 'package:school_pen/domain/homework/homework_manager.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';

class HomeworkModel extends Equatable {
  final bool showEmptyState;
  final Course course;
  final GradingSystem gradingSystem;
  final List<HomeworkData> overdueHomework;
  final List<HomeworkData> activeHomework;
  final List<HomeworkData> archivedHomework;

  const HomeworkModel({
    @required this.showEmptyState,
    this.course,
    @required this.gradingSystem,
    @required this.overdueHomework,
    @required this.activeHomework,
    @required this.archivedHomework,
  });

  @override
  List<Object> get props => [showEmptyState, course, gradingSystem, overdueHomework, activeHomework, archivedHomework];
}

class HomeworkData extends Equatable {
  final Homework homework;
  final Course course;
  final Grade grade;

  const HomeworkData(this.homework, this.course, this.grade);

  @override
  List<Object> get props => [homework, course, grade];
}

class HomeworkViewModel extends ViewModel<HomeworkModel, Object>
    implements SemesterListener, HomeworkListener, CourseListener, GradeListener {
  static const Logger _logger = Logger('HomeworkViewModel');

  final SemesterManager _semesterManager;
  final HomeworkManager _homeworkManager;
  final CourseManager _courseManager;
  final GradeManager _gradeManager;
  final String _courseId;

  HomeworkViewModel(
      this._semesterManager, this._homeworkManager, this._courseManager, this._gradeManager, this._courseId);

  @override
  void onStart() async {
    super.onStart();
    _semesterManager.addSemesterListener(this, notifyOnAttach: false);
    _homeworkManager.addHomeworkListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _gradeManager.addGradeListener(this, notifyOnAttach: false);

    await _tryEmitModel();
  }

  @override
  void onStop() {
    _semesterManager.removeSemesterListener(this);
    _homeworkManager.removeHomeworkListener(this);
    _courseManager.removeCourseListener(this);
    _gradeManager.removeGradeListener(this);
    super.onStop();
  }

  @override
  void onSemestersChanged(SemesterManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onHomeworkChanged(HomeworkManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onGradesChanged(GradeManager manager) async {
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<HomeworkModel> get _model async {
    final Semester semester = await _semesterManager.getActive();
    final Course course = await _courseManager.getById(_courseId);
    final GradingSystem gradingSystem = await _gradeManager.getGradingSystem();
    final List<HomeworkData> homework = await _fetchHomework(semester.schoolYearId, semester.id);
    final DateTime today = DateTime.now().withoutTime();

    return HomeworkModel(
      showEmptyState: homework.isEmpty,
      course: course,
      gradingSystem: gradingSystem,
      overdueHomework: homework.where((e) => !e.homework.isArchived && e.homework.dateTime.isBefore(today)).toList(),
      activeHomework: homework.where((e) => !e.homework.isArchived && e.homework.dateTime.isSameDateAs(today)).toList(),
      archivedHomework: homework.where((e) => e.homework.isArchived).toList(),
    );
  }

  Future<List<HomeworkData>> _fetchHomework(String schoolYearId, String semesterId) async {
    final List<Homework> homework = await _homeworkManager.getAll(semesterId: semesterId);
    final List<Homework> selectedHomework =
        homework.where((e) => e.courseId == _courseId || _courseId == null).toList();
    final List<Course> courses = await _courseManager.getAll(schoolYearId);
    final List<Grade> grades = await _gradeManager.getAll(semesterId: semesterId);

    final List<HomeworkData> result = [];
    for (Homework homework in selectedHomework) {
      final Course course = courses.firstWhereOrNull((e) => e.id == homework.courseId);
      final Grade grade = grades.firstWhereOrNull((e) => e.id == homework.gradeId);

      result.add(HomeworkData(homework, course, grade));
    }

    return result;
  }
}
