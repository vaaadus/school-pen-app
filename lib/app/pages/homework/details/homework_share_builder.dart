import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/homework/model/add_homework.dart';

class HomeworkShareBuilder {
  static String buildShareText(AddHomework homework, Course course) {
    assert(course != null);

    String result = course.name + ': ';

    if (Strings.isNotBlank(homework.title)) {
      result += homework.title;
    }

    if (Strings.isNotBlank(homework.note)) {
      result += '\n\n';
      result += homework.note;
    }

    result += '\n\n[${DateFormatter.formatFull(homework.dateTime)}]';

    return result.trim();
  }
}
