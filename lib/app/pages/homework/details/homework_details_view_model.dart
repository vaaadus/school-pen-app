import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/notification_options.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/homework/homework_listener.dart';
import 'package:school_pen/domain/homework/homework_manager.dart';
import 'package:school_pen/domain/homework/model/add_homework.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';

class HomeworkDetailsModel extends Equatable {
  final bool isEditing;
  final bool discardChangesPopup;
  final bool progressOverlay;
  final AddHomeworkInput homework;
  final Exception dateTimeError;
  final List<Course> availableCourses;
  final Course selectedCourse;
  final Exception courseError;
  final Grade selectedGrade;
  final List<Grade> availableGrades;
  final GradingSystem gradingSystem;

  const HomeworkDetailsModel({
    @required this.isEditing,
    @required this.discardChangesPopup,
    @required this.progressOverlay,
    @required this.homework,
    this.dateTimeError,
    @required this.availableCourses,
    this.selectedCourse,
    this.courseError,
    this.selectedGrade,
    @required this.availableGrades,
    @required this.gradingSystem,
  });

  @override
  List<Object> get props => [
        isEditing,
        discardChangesPopup,
        progressOverlay,
        homework,
        dateTimeError,
        availableCourses,
        selectedCourse,
        courseError,
        selectedGrade,
        availableGrades,
        gradingSystem,
      ];
}

enum HomeworkDetailsSignal {
  notifyHomeworkArchived,
  notifyHomeworkUnarchived,
  notifyHomeworkDeleted,
  notifyHomeworkSaved,
  pop,
}

class HomeworkDetailsViewModel extends ViewModel<HomeworkDetailsModel, HomeworkDetailsSignal>
    implements HomeworkListener, CourseListener, GradeListener {
  static const Logger _logger = Logger('HomeworkDetailsViewModel');

  final SemesterManager _semesterManager;
  final HomeworkManager _homeworkManager;
  final CourseManager _courseManager;
  final GradeManager _gradeManager;
  final String _homeworkId;
  final String _preselectedCourseId;

  bool _editModeActive = false;
  bool _progressOverlay = false;
  AddHomeworkInput _originalHomework;
  AddHomeworkInput _editedHomework;
  Exception _dateTimeError;
  Exception _courseError;

  HomeworkDetailsViewModel(this._semesterManager, this._homeworkManager, this._courseManager, this._gradeManager,
      this._homeworkId, this._preselectedCourseId) {
    _editModeActive = _homeworkId == null;
  }

  @override
  void onStart() async {
    super.onStart();
    _homeworkManager.addHomeworkListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _gradeManager.addGradeListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _homeworkManager.removeHomeworkListener(this);
    _courseManager.removeCourseListener(this);
    _gradeManager.removeGradeListener(this);
    super.onStop();
  }

  @override
  void onHomeworkChanged(HomeworkManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onGradesChanged(GradeManager manager) async {
    await _tryEmitModel();
  }

  void onEnableEditMode() async {
    _editModeActive = true;
    await _tryEmitModel();
  }

  void onArchiveHomework(bool archived) {
    _tryWithProgressOverlay(() async {
      if (_homeworkId == null) return;

      _editedHomework = _editedHomework.copyWith(isArchived: Optional(archived));
      await _homeworkManager.update(_homeworkId, _editedHomework?.toModel());
      emitSignal(
          archived ? HomeworkDetailsSignal.notifyHomeworkArchived : HomeworkDetailsSignal.notifyHomeworkUnarchived);
      if (archived) emitSignal(HomeworkDetailsSignal.pop);
    });
  }

  void onSaveHomework() {
    _tryWithProgressOverlay(() async {
      if (!_validate(_editedHomework)) return;

      if (_homeworkId != null) {
        await _homeworkManager.update(_homeworkId, _editedHomework?.toModel());
      } else {
        await _homeworkManager.create(_editedHomework?.toModel());
      }
      emitSignal(HomeworkDetailsSignal.notifyHomeworkSaved);
      emitSignal(HomeworkDetailsSignal.pop);
    });
  }

  Future<void> onEditTitle(String text) async {
    _editedHomework = _editedHomework.copyWith(title: Optional(text));

    await _tryEmitModel();
  }

  void onEditCourse(String courseId) async {
    if (_editedHomework.courseId == courseId) return;

    _editedHomework = _editedHomework.copyWith(
      courseId: Optional(courseId),
      gradeId: Optional(null),
    );

    await _tryEmitModel();
  }

  void onEditGrade(String gradeId) async {
    await _tryWithProgressOverlay(() async {
      if (gradeId == null && _editedHomework.gradeId != null) {
        await _gradeManager.delete(_editedHomework.gradeId);
      }

      _editedHomework = _editedHomework.copyWith(gradeId: Optional(gradeId));

      if (_homeworkId != null && !_editModeActive) {
        await _homeworkManager.update(_homeworkId, _editedHomework.toModel());
      }
    });
  }

  void onEditDateTime(DateTime dateTime) async {
    _editedHomework = _editedHomework.copyWith(dateTime: Optional(dateTime));

    await _tryEmitModel();
  }

  void onEditNote(String text) async {
    _editedHomework = _editedHomework.copyWith(note: Optional(text));

    await _tryEmitModel();
  }

  void onEditNotification(NotificationOptions notification) async {
    _editedHomework = _editedHomework.copyWith(notification: Optional(notification));

    await _tryEmitModel();
  }

  void onDeleteHomework() {
    _tryWithProgressOverlay(() async {
      await _homeworkManager.delete(_homeworkId);
      emitSignal(HomeworkDetailsSignal.notifyHomeworkDeleted);
      emitSignal(HomeworkDetailsSignal.pop);
    });
  }

  bool _validate(AddHomeworkInput homework) {
    _courseError = null;
    _dateTimeError = null;

    if (homework.courseId == null) {
      _courseError = FieldMustNotBeEmptyException();
      return false;
    }

    if (homework.dateTime == null) {
      _dateTimeError = FieldMustNotBeEmptyException();
      return false;
    }

    return true;
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<HomeworkDetailsModel> get _model async {
    final AddHomeworkInput homework = await _obtainHomework();
    if (homework == null) return null;

    final Semester semester = await _semesterManager.getById(homework.semesterId);
    final List<Course> courses = await _courseManager.getAll(semester.schoolYearId);
    final List<Grade> grades = await _gradeManager.getAll(semesterId: semester.id);
    final List<Grade> selectedGrades = grades.where((e) => e.courseId == homework.courseId).toList();
    final GradingSystem gradingSystem = await _gradeManager.getGradingSystem();

    return HomeworkDetailsModel(
      isEditing: _editModeActive,
      discardChangesPopup: _originalHomework == null || homework != _originalHomework,
      progressOverlay: _progressOverlay,
      homework: homework,
      dateTimeError: _dateTimeError,
      availableCourses: courses.where((e) => e.id != homework.courseId).toList(),
      selectedCourse: courses.firstWhereOrNull((e) => e.id == homework.courseId),
      courseError: _courseError,
      selectedGrade: selectedGrades.firstWhereOrNull((e) => e.id == homework.gradeId),
      availableGrades: selectedGrades,
      gradingSystem: gradingSystem,
    );
  }

  Future<AddHomeworkInput> _obtainHomework() async {
    if (_editedHomework != null) return _editedHomework;

    if (_homeworkId != null) {
      final Homework homework = await _homeworkManager.getById(_homeworkId);
      final AddHomework addHomework = homework?.toAddHomework()?.copyWith(updatedAt: Optional(DateTime.now()));
      _originalHomework = addHomework != null ? AddHomeworkInput.from(addHomework) : null;
      _editedHomework = _originalHomework;
    } else {
      final Semester semester = await _semesterManager.getActive();
      _editedHomework = AddHomeworkInput(
        semesterId: semester.id,
        courseId: _preselectedCourseId,
        gradeId: null,
        title: '',
        note: '',
        dateTime: null,
        notification: null,
        isArchived: false,
        updatedAt: DateTime.now(),
      );
    }

    return _editedHomework;
  }
}

class AddHomeworkInput extends Equatable {
  final String semesterId;
  final String courseId;
  final String gradeId;
  final String title;
  final String note;
  final DateTime dateTime;
  final NotificationOptions notification;
  final bool isArchived;
  final DateTime updatedAt;

  const AddHomeworkInput({
    @required this.semesterId,
    this.courseId,
    this.gradeId,
    this.title,
    this.note,
    this.dateTime,
    this.notification,
    @required this.isArchived,
    @required this.updatedAt,
  });

  factory AddHomeworkInput.from(AddHomework homework) {
    return AddHomeworkInput(
      semesterId: homework.semesterId,
      courseId: homework.courseId,
      gradeId: homework.gradeId,
      title: homework.title,
      note: homework.note,
      dateTime: homework.dateTime,
      notification: homework.notification,
      isArchived: homework.isArchived,
      updatedAt: homework.updatedAt,
    );
  }

  AddHomework toModel() {
    return AddHomework(
      semesterId: semesterId,
      courseId: courseId,
      gradeId: gradeId,
      title: title,
      note: note,
      dateTime: dateTime ?? DateTime.now(),
      notification: notification,
      isArchived: isArchived,
      updatedAt: updatedAt,
    );
  }

  AddHomeworkInput copyWith({
    Optional<String> semesterId,
    Optional<String> courseId,
    Optional<String> gradeId,
    Optional<String> title,
    Optional<String> note,
    Optional<DateTime> dateTime,
    Optional<NotificationOptions> notification,
    Optional<bool> isArchived,
    Optional<DateTime> updatedAt,
  }) {
    return AddHomeworkInput(
      semesterId: Optional.unwrapOrElse(semesterId, this.semesterId),
      courseId: Optional.unwrapOrElse(courseId, this.courseId),
      gradeId: Optional.unwrapOrElse(gradeId, this.gradeId),
      title: Optional.unwrapOrElse(title, this.title),
      note: Optional.unwrapOrElse(note, this.note),
      dateTime: Optional.unwrapOrElse(dateTime, this.dateTime),
      notification: Optional.unwrapOrElse(notification, this.notification),
      isArchived: Optional.unwrapOrElse(isArchived, this.isArchived),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  /// Whether the notification date is in the past.
  /// False when not set.
  bool isNotificationExpired(DateTime now) {
    if (notification == null) return false;
    return notification.getFireDateTime(dateTime ?? DateTime.now()).isBefore(now);
  }

  @override
  List<Object> get props => [semesterId, courseId, gradeId, title, note, dateTime, notification, isArchived, updatedAt];
}
