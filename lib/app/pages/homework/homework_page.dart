import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/homework/homework_view_model.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/widgets/bordered_list_item.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows to view/edit/create homework.
class HomeworkPage extends LifecycleWidget {
  const HomeworkPage({
    Key key,
    this.courseId,
    this.fullscreen = true,
    this.onBack = Navigator.pop,
  }) : super(key: key);

  final String courseId;
  final bool fullscreen;
  final OnBackCallback onBack;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _HomeworkPageState();
}

class _HomeworkPageState extends LifecycleWidgetState<HomeworkPage>
    with ViewModelLifecycle<HomeworkViewModel, HomeworkPage>, ProgressOverlayMixin, SingleTickerProviderStateMixin {
  @override
  HomeworkViewModel provideViewModel() =>
      HomeworkViewModel(Injection.get(), Injection.get(), Injection.get(), Injection.get(), widget.courseId);

  @override
  Widget build(BuildContext context) {
    Widget result = Scaffold(
      body: SafeArea(
        child: ViewModelBuilder(
          viewModel: viewModel,
          builder: _buildContent,
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        label: ButtonText(S.of(context).common_addHomework),
        onPressed: () => Nav.homework_add(courseId: widget.courseId),
      ),
    );

    if (widget.fullscreen) {
      result = SystemUiOverlayAnnotation(child: result);
    }

    return result;
  }

  Widget _buildContent(BuildContext context, HomeworkModel model) {
    return CustomScrollView(slivers: [
      Toolbar(
        pinned: !widget.fullscreen || model.showEmptyState,
        vsync: this,
        onBack: widget.onBack,
        navType: widget.fullscreen ? NavButtonType.back : NavButtonType.close,
        title: _getTitle(context, model),
      ),
      if (model.showEmptyState)
        SliverFillRemaining(
          child: _buildEmptyState(context),
        ),
      if (!model.showEmptyState)
        SliverPadding(
          padding: EdgeInsets.only(bottom: Dimens.grid_10x),
          sliver: SliverList(
            delegate: SliverChildListDelegate([
              if (model.overdueHomework.isNotEmpty) _buildErrorHeader(context, S.of(context).common_overdue),
              for (HomeworkData data in model.overdueHomework) _buildListItem(context, model, data),
              if (model.activeHomework.isNotEmpty) _buildHeader(context, S.of(context).common_active),
              for (HomeworkData data in model.activeHomework) _buildListItem(context, model, data),
              if (model.archivedHomework.isNotEmpty) _buildHeader(context, S.of(context).common_archived),
              for (HomeworkData data in model.archivedHomework) _buildListItem(context, model, data),
            ]),
          ),
        ),
    ]);
  }

  String _getTitle(BuildContext context, HomeworkModel model) {
    String title = '';
    if (widget.fullscreen && model.course != null) title += model.course.name + ' / ';
    title += S.of(context).common_homework;
    return title;
  }

  Widget _buildEmptyState(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(Dimens.grid_3x),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(Images.illustration_empty_box_128, width: 128, height: 128),
            Padding(
              padding: EdgeInsets.only(top: Dimens.grid_1_5x),
              child: Text(
                S.of(context).common_noActiveEvents,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline6.copyWith(
                      color: Theme.of(context).colorScheme.primaryVariant,
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildErrorHeader(BuildContext context, String text) {
    return _buildHeader(context, text + ' ⚠', color: Theme.of(context).colorScheme.error);
  }

  Widget _buildHeader(BuildContext context, String text, {Color color}) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        left: paddings.horizontal,
        right: paddings.horizontal,
        top: Dimens.grid_4x,
      ),
      child: Text(
        text.toUpperCase(),
        style: Theme.of(context).textTheme.overline.copyWith(
              color: color ?? Theme.of(context).colorScheme.primaryVariant,
            ),
      ),
    );
  }

  Widget _buildListItem(BuildContext context, HomeworkModel model, HomeworkData data) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        top: Dimens.grid_2x,
        left: paddings.horizontal,
        right: paddings.horizontal,
      ),
      child: BorderedListItem(
        data: _homeworkAsListData(context, model, data),
        onTap: () => Nav.homework_details(homeworkId: data.homework.id),
      ),
    );
  }

  BorderedListItemData _homeworkAsListData(BuildContext context, HomeworkModel model, HomeworkData data) {
    return BorderedListItemData(
      title: data.homework.title,
      label: _formatLabel(context, data),
      aside: _formatAside(context, model, data),
    );
  }

  String _formatLabel(BuildContext context, HomeworkData data) {
    String label = '';
    if (widget.courseId == null) label += data.course.name + ' · ';
    label += DateFormat.MMMMd().format(data.homework.dateTime);
    return label;
  }

  String _formatAside(BuildContext context, HomeworkModel model, HomeworkData data) {
    if (data.grade == null) return null;
    return model.gradingSystem.decorate(data.grade.value);
  }
}
