import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/intents.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/generated/l10n.dart';

/// Presents the user options how to get help.
class SupportPage extends StatefulWidget {
  const SupportPage({Key key, this.enableNavigation = true}) : super(key: key);

  final bool enableNavigation;

  @override
  _SupportPageState createState() => _SupportPageState();
}

class _SupportPageState extends State<SupportPage> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);

    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: CustomScrollView(slivers: [
            if (widget.enableNavigation) Toolbar(vsync: this),
            SliverList(
              delegate: SliverChildListDelegate([
                Padding(
                  padding: EdgeInsets.only(
                    top: widget.enableNavigation ? (Dimens.grid_2x - Toolbar.bottomPadding) : paddings.vertical,
                    bottom: Dimens.grid_2_5x,
                    left: paddings.horizontal,
                    right: paddings.horizontal,
                  ),
                  child: Text(
                    S.of(context).common_support,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                _SupportSection(
                  imageAsset: Images.ic_facebook_filled_36,
                  imageColor: Theme.of(context).colorScheme.secondary,
                  title: S.of(context).common_facebook,
                  subtitle: '@' + R.facebookUsername,
                  onTap: () => Nav.facebookPage(),
                ),
                _SupportSection(
                  imageAsset: Images.ic_mail_filled_36,
                  imageColor: Theme.of(context).colorScheme.secondaryVariant,
                  title: S.of(context).common_email,
                  subtitle: R.supportEmail,
                  onTap: () => Intents.sendEmail(email: R.supportEmail),
                ),
              ]),
            ),
          ]),
        ),
      ),
    );
  }
}

class _SupportSection extends StatelessWidget {
  final String imageAsset;
  final Color imageColor;
  final String title;
  final String subtitle;
  final GestureTapCallback onTap;

  const _SupportSection({
    Key key,
    @required this.imageAsset,
    @required this.imageColor,
    @required this.title,
    @required this.subtitle,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: paddings.horizontal,
          vertical: Dimens.grid_1_5x,
        ),
        child: Row(children: [
          Padding(
            padding: EdgeInsets.only(right: Dimens.grid_2x),
            child: Image.asset(imageAsset, width: 36, height: 36, color: imageColor, excludeFromSemantics: true),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title, style: Theme.of(context).textTheme.bodyText1),
                Padding(
                  padding: EdgeInsets.only(top: 2.0),
                  child: Text(
                    subtitle,
                    style: Theme.of(context).textTheme.caption.copyWith(
                          color: Theme.of(context).colorScheme.primaryVariant,
                        ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: Dimens.grid_2x),
            child: Image.asset(
              Images.ic_chevron_right_16,
              width: 16,
              height: 16,
              color: Theme.of(context).colorScheme.primaryVariant,
              excludeFromSemantics: true,
            ),
          ),
        ]),
      ),
    );
  }
}
