import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/sized_dialog.dart';
import 'package:school_pen/app/widgets/text_field_container.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/generated/l10n.dart';

/// Shows a dialog to create a semester.
/// The resulting future will resolve to a picked semester or to null if the dialog is cancelled.
Future<AddSemester> showSemesterCreateDialog({
  @required BuildContext context,
  @required String schoolYearId,
}) {
  return showDialog(
    context: context,
    routeSettings: RouteSettings(name: R.settings_semesters_add),
    builder: (BuildContext context) => _SemesterEditDialog(
      schoolYearId: schoolYearId,
    ),
  );
}

/// Shows a dialog to edit a semester.
/// The resulting future will resolve to a picked semester or to null if the dialog is cancelled.
Future<AddSemester> showSemesterEditDialog({
  @required BuildContext context,
  @required String schoolYearId,
  @required Semester initialSemester,
}) {
  return showDialog(
    context: context,
    routeSettings: RouteSettings(name: R.settings_semesters_edit),
    builder: (BuildContext context) => _SemesterEditDialog(
      schoolYearId: schoolYearId,
      initialSemester: initialSemester,
    ),
  );
}

class _SemesterEditDialog extends StatefulWidget {
  const _SemesterEditDialog({
    Key key,
    @required this.schoolYearId,
    this.initialSemester,
  }) : super(key: key);

  final String schoolYearId;
  final Semester initialSemester;

  @override
  _SemesterEditDialogState createState() => _SemesterEditDialogState();
}

class _SemesterEditDialogState extends State<_SemesterEditDialog> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _startDateController = TextEditingController();
  final TextEditingController _endDateController = TextEditingController();

  DateTime _startDate;
  DateTime _endDate;
  String _nameError;
  String _startDateError;

  @override
  void initState() {
    super.initState();
    _updateFields();
  }

  @override
  void didUpdateWidget(_SemesterEditDialog oldWidget) {
    super.didUpdateWidget(oldWidget);
    _updateFields();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _startDateController.dispose();
    _endDateController.dispose();
    super.dispose();
  }

  void _updateFields() {
    if (widget.initialSemester != null) {
      _startDate = widget.initialSemester?.startDate;
      _endDate = widget.initialSemester?.endDate;
    }

    _nameController.text = widget.initialSemester?.name ?? '';
  }

  @override
  Widget build(BuildContext context) {
    _startDateController.text = DateFormatter.formatDate(_startDate);
    _endDateController.text = DateFormatter.formatDate(_endDate);

    return SizedDialog(
      padding: EdgeInsets.all(Dimens.grid_2x),
      child: Column(children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            widget.initialSemester != null
                ? S.of(context).common_editSemesterTerm
                : S.of(context).common_newSemesterTerm,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
          child: Divider(),
        ),
        TextFieldContainer(
          maxHeight: TextFieldContainer.miniHeight,
          child: TextField(
            controller: _nameController,
            autofocus: true,
            style: Theme.of(context).textTheme.headline6,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(labelText: S.of(context).common_name),
            textInputAction: TextInputAction.done,
            onSubmitted: (String text) => _onSave(context),
          ),
          error: TextFieldError.forText(_nameError),
        ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x),
          child: GestureDetector(
            onTap: () => _onChangeStartDate(context),
            child: TextFieldContainer(
              maxHeight: TextFieldContainer.miniHeight,
              child: TextField(
                controller: _startDateController,
                style: Theme.of(context).textTheme.headline6,
                enabled: false,
                decoration: InputDecoration(labelText: S.of(context).common_startDate),
              ),
              error: TextFieldError.forText(_startDateError),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x),
          child: GestureDetector(
            onTap: () => _onChangeEndDate(context),
            child: TextFieldContainer(
              maxHeight: TextFieldContainer.miniHeight,
              child: TextField(
                controller: _endDateController,
                style: Theme.of(context).textTheme.headline6,
                enabled: false,
                decoration: InputDecoration(labelText: S.of(context).common_endDate),
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SmallMaterialButton(
                child: SmallButtonText(
                  S.of(context).common_cancel,
                  color: Theme.of(context).colorScheme.primaryVariant,
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              SmallRaisedButton(
                child: SmallButtonText(
                  widget.initialSemester != null ? S.of(context).common_save : S.of(context).common_create,
                ),
                onPressed: () => _onSave(context),
              ),
            ],
          ),
        ),
      ]),
    );
  }

  void _onChangeStartDate(BuildContext context) async {
    DateTime initialDate = (_startDate ?? DateTime.now()).withoutTime();
    DateTime startDate = initialDate.minusDays(31);
    if (initialDate.isBefore(startDate)) startDate = initialDate;

    final DateTime date = await showAdaptiveDatePicker(
      context: context,
      initialDate: initialDate,
      minimumDate: startDate,
    );

    if (date != null) {
      setState(() => _startDate = date);
    }
  }

  void _onChangeEndDate(BuildContext context) async {
    DateTime startDate = (_startDate ?? DateTime.now()).plusDays(1).withoutTime();
    DateTime initialDate = _endDate ?? DateTime.now().plusDays(150).withoutTime();
    if (initialDate.isBefore(startDate)) startDate = initialDate;

    final DateTime date = await showAdaptiveDatePicker(
      context: context,
      initialDate: initialDate,
      minimumDate: startDate,
    );

    if (date != null) {
      setState(() => _endDate = date);
    }
  }

  void _onSave(BuildContext context) {
    final String name = _nameController.text;
    final DateTime startDate = _startDate?.withoutTime();
    final DateTime endDate = _endDate?.withoutTime();

    if (_validate(name, startDate, endDate)) {
      final AddSemester semester = AddSemester(
        schoolYearId: widget.schoolYearId,
        name: name,
        startDate: startDate,
        endDate: endDate,
        updatedAt: DateTime.now(),
      );

      Navigator.of(context).pop(semester);
    }
  }

  bool _validate(String name, DateTime startDate, DateTime endDate) {
    _nameError = null;
    _startDateError = null;

    if (Strings.isBlank(name)) {
      setState(() => _nameError = S.of(context).error_mustNotBeEmpty);
      return false;
    } else if (_startDate == null && _endDate != null) {
      setState(() => _startDateError = S.of(context).error_mustNotBeEmpty);
      return false;
    } else if (_startDate != null && _endDate != null && _startDate.isAfterOrAt(_endDate)) {
      setState(() => _startDateError = S.of(context).error_startDateMustBeBeforeEndDate);
      return false;
    } else {
      return true;
    }
  }
}
