import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/options_dialog.dart';
import 'package:school_pen/app/pages/settings/semester/semester_edit_dialog.dart';
import 'package:school_pen/app/pages/settings/semester/semester_view_model.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows to view/edit/create semesters.
class SemesterPage extends LifecycleWidget {
  const SemesterPage({Key key, this.enableNavigation = true}) : super(key: key);

  final bool enableNavigation;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _SemesterPageState();
}

class _SemesterPageState extends LifecycleWidgetState<SemesterPage>
    with ViewModelLifecycle<SemesterViewModel, SemesterPage>, SingleTickerProviderStateMixin {
  @override
  SemesterViewModel provideViewModel() => SemesterViewModel(Injection.get());

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: (BuildContext context, SemesterModel model) => _buildContent(context, model),
            onSignal: (BuildContext context, SemesterSignal signal) {
              if (signal == SemesterSignal.notifySemesterSelected) {
                showSuccessToast(message: S.of(context).confirmation_saveSuccess);
              } else if (signal == SemesterSignal.notifySemesterCreated) {
                showSuccessToast(message: S.of(context).confirmation_saveSuccess);
              } else if (signal == SemesterSignal.notifySemesterUpdated) {
                showSuccessToast(message: S.of(context).confirmation_saveSuccess);
              } else if (signal == SemesterSignal.notifySemesterDeleted) {
                showSuccessToast(message: S.of(context).confirmation_deleted);
              }
            },
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          heroTag: null,
          label: ButtonText(S.of(context).common_newSemesterTerm),
          onPressed: () => _showSemesterCreateDialog(context),
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, SemesterModel model) {
    final Paddings paddings = Paddings.of(context);

    return CustomScrollView(slivers: [
      if (widget.enableNavigation) Toolbar(vsync: this),
      SliverList(
        delegate: SliverChildListDelegate([
          Padding(
            padding: EdgeInsets.only(
              top: widget.enableNavigation ? (Dimens.grid_2x - Toolbar.bottomPadding) : paddings.vertical,
              bottom: Dimens.grid_3x,
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: Text(
              S.of(context).common_semesterTerms,
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          Semantics(
            checked: model.automaticTermChange,
            child: InkWell(
              onTap: () => viewModel.onToggleAutomaticTermChange(!model.automaticTermChange),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: paddings.horizontal,
                  vertical: Dimens.grid_1x,
                ),
                child: Row(children: [
                  Expanded(
                    child: Text(
                      S.of(context).common_automaticSemesterTermChange.toUpperCase(),
                      style: Theme.of(context).textTheme.overline,
                    ),
                  ),
                  ExcludeSemantics(
                    child: Switch(
                      value: model.automaticTermChange,
                      onChanged: (bool value) => viewModel.onToggleAutomaticTermChange(value),
                    ),
                  ),
                ]),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: paddings.horizontal,
              right: paddings.horizontal,
              top: Dimens.grid_1x,
            ),
            child: Divider(),
          ),
          ..._buildSemesters(context, model.semesters),
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: Dimens.grid_4x,
              horizontal: paddings.horizontal,
            ),
            child: Text(
              S.of(context).settings_semesters_description,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText2.copyWith(
                    color: Theme.of(context).colorScheme.primaryVariant,
                  ),
            ),
          ),
        ]),
      ),
    ]);
  }

  Iterable<Widget> _buildSemesters(BuildContext context, List<SemesterData> semesters) sync* {
    final Paddings paddings = Paddings.of(context);

    for (int i = 0; i < semesters.length; i++) {
      yield _SemesterRow(
        key: ValueKey(semesters[i]),
        data: semesters[i],
        onTap: () => _showSemesterOptions(context, semesters[i]),
      );

      if (i + 1 < semesters.length) {
        yield Padding(
          padding: EdgeInsets.symmetric(horizontal: paddings.horizontal),
          child: Divider(),
        );
      }
    }
  }

  void _showSemesterOptions(BuildContext context, SemesterData data) async {
    final VoidCallback option = await showOptionsDialog(
      context: context,
      title: data.semester.name,
      itemBuilder: (BuildContext context) => [
        if (!data.isActive)
          OptionDialogItem(
            imageAsset24: Images.ic_checkmark_filled_24,
            text: S.of(context).settings_semesters_setAsCurrent,
            value: () => viewModel.onSelectSemester(data.semester.id),
          ),
        OptionDialogItem(
          imageAsset24: Images.ic_edit_field_24,
          text: S.of(context).common_edit,
          value: () => _showSemesterEditDialog(context, data.semester),
        ),
        if (data.canBeDeleted)
          OptionDialogItem(
            imageAsset24: Images.ic_trash_24,
            text: S.of(context).common_delete,
            customColor: Theme.of(context).colorScheme.error,
            value: () => _showSemesterDeleteConfirmation(context, data),
          )
      ],
    );

    option?.call();
  }

  void _showSemesterDeleteConfirmation(BuildContext context, SemesterData data) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_delete,
      message: S.of(context).settings_semesters_deleteDialog_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteSemester(data.semester.id),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }

  void _showSemesterCreateDialog(BuildContext context) async {
    final SemesterModel model = await viewModel.lastModel;
    if (model == null) return;

    final AddSemester newSemester = await showSemesterCreateDialog(
      context: context,
      schoolYearId: model.schoolYearId,
    );

    if (newSemester != null) {
      viewModel.onCreateSemester(newSemester);
    }
  }

  void _showSemesterEditDialog(BuildContext context, Semester semester) async {
    final AddSemester newSemester = await showSemesterEditDialog(
      context: context,
      schoolYearId: semester.schoolYearId,
      initialSemester: semester,
    );

    if (newSemester != null) {
      viewModel.onUpdateSemester(semester.id, newSemester);
    }
  }
}

class _SemesterRow extends StatelessWidget {
  const _SemesterRow({Key key, @required this.data, @required this.onTap}) : super(key: key);

  final SemesterData data;
  final GestureTapCallback onTap;

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);

    return MergeSemantics(
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: Dimens.grid_2x,
            horizontal: paddings.horizontal,
          ),
          child: Row(children: [
            Image.asset(
              Images.ic_item_arrow_24,
              width: 24,
              height: 24,
              color: Theme.of(context).colorScheme.primaryVariant,
              excludeFromSemantics: true,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Dimens.grid_2x),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(data.semester.name, style: Theme.of(context).textTheme.bodyText1),
                    if (_showDateRange)
                      Padding(
                        padding: EdgeInsets.only(top: Dimens.grid_0_5x),
                        child: Text(
                          _formattedDateRange,
                          style: Theme.of(context).textTheme.caption.copyWith(
                                color: Theme.of(context).colorScheme.primaryVariant,
                              ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
            if (data.isActive)
              Image.asset(
                Images.ic_checkmark_24,
                width: 24,
                height: 24,
                color: Theme.of(context).colorScheme.secondary,
                semanticLabel: S.of(context).common_active,
              ),
          ]),
        ),
      ),
    );
  }

  bool get _showDateRange {
    return data.semester.startDate != null || data.semester.endDate != null;
  }

  String get _formattedDateRange {
    String string = '';
    if (data.semester.startDate != null) string += DateFormatter.formatDate(data.semester.startDate);
    string += ' - ';
    if (data.semester.endDate != null) string += DateFormatter.formatDate(data.semester.endDate);
    return string;
  }
}
