import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';

class SemesterModel extends Equatable {
  final String schoolYearId;
  final bool automaticTermChange;
  final List<SemesterData> semesters;

  const SemesterModel({
    @required this.schoolYearId,
    @required this.automaticTermChange,
    @required this.semesters,
  });

  @override
  List<Object> get props => [schoolYearId, automaticTermChange, semesters];
}

class SemesterData extends Equatable {
  final Semester semester;
  final bool isActive;
  final bool canBeDeleted;

  const SemesterData({
    @required this.semester,
    @required this.isActive,
    @required this.canBeDeleted,
  });

  @override
  List<Object> get props => [semester, isActive, canBeDeleted];
}

enum SemesterSignal {
  notifySemesterSelected,
  notifySemesterCreated,
  notifySemesterUpdated,
  notifySemesterDeleted,
}

class SemesterViewModel extends ViewModel<SemesterModel, SemesterSignal> implements SemesterListener {
  static const Logger _logger = Logger('SemesterViewModel');
  final SemesterManager _semesterManager;

  SemesterViewModel(this._semesterManager);

  @override
  void onStart() async {
    super.onStart();
    _semesterManager.addSemesterListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _semesterManager.removeSemesterListener(this);
    super.onStop();
  }

  @override
  void onSemestersChanged(SemesterManager manager) async {
    await _tryEmitModel();
  }

  void onSelectSemester(String id) async {
    try {
      await _semesterManager.setActive(id);
      emitSignal(SemesterSignal.notifySemesterSelected);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    await _tryEmitModel();
  }

  void onCreateSemester(AddSemester semester) async {
    try {
      await _semesterManager.create(semester);
      emitSignal(SemesterSignal.notifySemesterCreated);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    await _tryEmitModel();
  }

  void onUpdateSemester(String id, AddSemester semester) async {
    try {
      await _semesterManager.update(id, semester);
      emitSignal(SemesterSignal.notifySemesterUpdated);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    await _tryEmitModel();
  }

  void onDeleteSemester(String id) async {
    try {
      await _semesterManager.delete(id);
      emitSignal(SemesterSignal.notifySemesterDeleted);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    await _tryEmitModel();
  }

  void onToggleAutomaticTermChange(bool value) async {
    try {
      await _semesterManager.setAutomaticSemesterChangeEnabled(value);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<SemesterModel> get _model async {
    final List<SemesterData> semesters = [];
    final Semester activeSemester = await _semesterManager.getActive();

    for (Semester semester in await _semesterManager.getAll()) {
      semesters.add(SemesterData(
        semester: semester,
        isActive: semester.id == activeSemester.id,
        canBeDeleted: await _semesterManager.canDelete(semester.id),
      ));
    }

    return SemesterModel(
      schoolYearId: activeSemester.schoolYearId,
      automaticTermChange: await _semesterManager.isAutomaticSemesterChangeEnabled(),
      semesters: semesters,
    );
  }
}
