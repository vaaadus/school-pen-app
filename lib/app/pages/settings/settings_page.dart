import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/intents.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/account/profile/profile_page.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/share_app_dialog.dart';
import 'package:school_pen/app/pages/settings/about/about_page.dart';
import 'package:school_pen/app/pages/settings/notifications/notifications_page.dart';
import 'package:school_pen/app/pages/settings/privacy/privacy_page.dart';
import 'package:school_pen/app/pages/settings/schoolyear/school_years_page.dart';
import 'package:school_pen/app/pages/settings/semester/semester_page.dart';
import 'package:school_pen/app/pages/settings/settings_page_enum.dart';
import 'package:school_pen/app/pages/settings/settings_view_model.dart';
import 'package:school_pen/app/pages/settings/support/support_page.dart';
import 'package:school_pen/app/pages/settings/themes/themes_page.dart';
import 'package:school_pen/app/widgets/avatar.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/list_item.dart';
import 'package:school_pen/app/widgets/master_detail_container.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

/// Displays a list of settings.
/// Intended to be used inside some wrapper as this page does not have its own scaffold.
class SettingsPage extends LifecycleWidget {
  const SettingsPage({Key key}) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _SettingsPageState();
}

class _SettingsPageState extends LifecycleWidgetState<SettingsPage>
    with ViewModelLifecycle<SettingsViewModel, SettingsPage> {
  SettingsPageEnum _selectedPage = SettingsPageEnum.account;

  @override
  SettingsViewModel provideViewModel() {
    return SettingsViewModel(Injection.get(), Injection.get(), Injection.get(), Injection.get());
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder(
      viewModel: viewModel,
      builder: (BuildContext context, SettingsModel model) {
        final Widget settings = _SettingsList(
          model: model,
          selectedPage: _selectedPage,
          onChangePage: (SettingsPageEnum page) => _onChangePage(context, page, model),
        );

        return MasterDetailContainer(
          masterBuilder: (BuildContext context) => settings,
          detailBuilder: (BuildContext context) => _buildDetailPage(context, model),
        );
      },
    );
  }

  void _onChangePage(BuildContext context, SettingsPageEnum page, SettingsModel model) {
    if (page.isSelectable) {
      setState(() => _selectedPage = page);
    }

    if (MasterDetailContainer.isSupported(context) && page.isSelectable) {
      return;
    }

    switch (page) {
      case SettingsPageEnum.account:
        if (model.user == null) {
          Nav.loginOptions();
        } else {
          Nav.profile();
        }
        break;
      case SettingsPageEnum.premium:
        // TODO: Handle this case.
        break;
      case SettingsPageEnum.pcOrWebVersion:
        Nav.appHomePage();
        break;
      case SettingsPageEnum.syncAndRestore:
        // TODO: Handle this case.
        break;
      case SettingsPageEnum.schoolYears:
        Nav.settings_schoolYears();
        break;
      case SettingsPageEnum.semesters:
        Nav.settings_semesters();
        break;
      case SettingsPageEnum.notifications:
        Nav.settings_notifications();
        break;
      case SettingsPageEnum.appearance:
        Nav.settings_themes();
        break;
      case SettingsPageEnum.privacy:
        Nav.settings_privacy();
        break;
      case SettingsPageEnum.support:
        Nav.settings_support();
        break;
      case SettingsPageEnum.integrations:
        // TODO: Handle this case.
        break;
      case SettingsPageEnum.rateUs:
        Intents.openStore();
        break;
      case SettingsPageEnum.share:
        showShareAppDialog(context);
        break;
      case SettingsPageEnum.about:
        Nav.settings_about();
        break;
    }
  }

  Widget _buildDetailPage(BuildContext context, SettingsModel model) {
    switch (_selectedPage) {
      case SettingsPageEnum.account:
        if (model.user == null) {
          return _CreateAccountPlaceholder();
        } else {
          return ProfilePage(enableNavigation: false);
        }
        break;
      case SettingsPageEnum.premium:
        // TODO: Handle this case.
        break;
      case SettingsPageEnum.pcOrWebVersion:
        throw ArgumentError('Page not selectable $_selectedPage');
      case SettingsPageEnum.syncAndRestore:
        // TODO: Handle this case.
        break;
      case SettingsPageEnum.schoolYears:
        return SchoolYearsPage(enableNavigation: false);
      case SettingsPageEnum.semesters:
        return SemesterPage(enableNavigation: false);
      case SettingsPageEnum.notifications:
        return NotificationsPage(enableNavigation: false);
      case SettingsPageEnum.appearance:
        return ThemesPage(
          enableNavigation: false,
          onShowPremium: () => _onChangePage(context, SettingsPageEnum.premium, model),
        );
      case SettingsPageEnum.privacy:
        return PrivacyPage(enableNavigation: false);
      case SettingsPageEnum.support:
        return SupportPage(enableNavigation: false);
      case SettingsPageEnum.integrations:
        // TODO: Handle this case.
        break;
      case SettingsPageEnum.rateUs:
        throw ArgumentError('This page is not selectable');
      case SettingsPageEnum.share:
        // TODO: Handle this case.
        break;
      case SettingsPageEnum.about:
        return AboutPage(enableNavigation: false);
    }

    // TODO remove
    return Container(child: Center(child: Text(_selectedPage.toString())));
  }
}

class _SettingsList extends StatelessWidget {
  const _SettingsList({
    Key key,
    @required this.model,
    @required this.onChangePage,
    @required this.selectedPage,
  }) : super(key: key);

  final SettingsModel model;
  final OnChangeSettingsPage onChangePage;
  final SettingsPageEnum selectedPage;

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    final S s = S.of(context);

    return ListView(
      padding: EdgeInsets.symmetric(vertical: paddings.vertical),
      children: [
        _UserWidget(user: model.user),
        Padding(
          padding: EdgeInsets.only(
            left: paddings.horizontal,
            right: paddings.horizontal,
            top: Dimens.grid_3x,
            bottom: Dimens.grid_2x,
          ),
          child: Divider(),
        ),
        _listItem(
          title: s.common_account,
          label: s.settings_account_label,
          page: SettingsPageEnum.account,
        ),
        if (kDebugMode)
          _listItem(
            title: s.common_premium,
            label: s.settings_premium_label,
            page: SettingsPageEnum.premium,
          ),
        _listItem(
          title: s.common_pcOrWebVersion,
          label: s.settings_pcOrWeb_label,
          page: SettingsPageEnum.pcOrWebVersion,
        ),
        if (kDebugMode)
          _listItem(
            title: s.common_syncAndRestore,
            label: s.settings_syncAndRestore_label,
            page: SettingsPageEnum.syncAndRestore,
          ),
        _listItem(
          title: s.common_schoolYears,
          label: s.settings_schoolYears_label,
          page: SettingsPageEnum.schoolYears,
        ),
        _listItem(
          title: s.common_semesterTerms,
          label: s.settings_semesters_label,
          page: SettingsPageEnum.semesters,
        ),
        if (model.supportsNotifications)
          _listItem(
            title: s.common_notification,
            label: s.settings_notifications_label,
            page: SettingsPageEnum.notifications,
          ),
        _listItem(
          title: s.common_appearance,
          label: ResourceLocator.themeToString(context, model.theme),
          page: SettingsPageEnum.appearance,
        ),
        _listItem(
          title: s.common_privacy,
          label: s.settings_privacy_label,
          page: SettingsPageEnum.privacy,
        ),
        _listItem(
          title: s.common_support,
          label: s.settings_support_label,
          page: SettingsPageEnum.support,
        ),
        if (kDebugMode)
          _listItem(
            title: s.common_integrations,
            label: s.settings_integrations_label,
            page: SettingsPageEnum.integrations,
          ),
        if (model.supportsStore)
          _listItem(
            title: s.common_rateUs,
            label: s.settings_rateUs_label,
            page: SettingsPageEnum.rateUs,
          ),
        if (isShareAppDialogSupported())
          _listItem(
            title: s.common_share,
            label: s.settings_share_label,
            page: SettingsPageEnum.share,
          ),
        _listItem(
          title: s.common_aboutApp,
          label: sprintf(s.settings_about_label, [model.versionName]),
          page: SettingsPageEnum.about,
        ),
      ],
    );
  }

  Widget _listItem({@required String title, @required String label, @required SettingsPageEnum page}) {
    return _ListItem(
      title: title,
      label: label,
      onTap: () => onChangePage(page),
      isSelected: selectedPage == page,
    );
  }
}

class _UserWidget extends StatelessWidget {
  const _UserWidget({
    Key key,
    @required this.user,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final Paddings paddings = Paddings.of(context);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: paddings.horizontal),
      child: Row(children: [
        Avatar(file: user?.avatar),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: Dimens.grid_2x),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: Dimens.grid_0_5x),
                  child: Text(_resolveUsername(context), style: theme.textTheme.headline6),
                ),
                Text(
                  _resolveEmail(context),
                  style: theme.textTheme.subtitle2.copyWith(color: theme.colorScheme.primaryVariant),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }

  String _resolveUsername(BuildContext context) {
    return user?.username ?? S.of(context).common_anonymousUser;
  }

  String _resolveEmail(BuildContext context) {
    if (user == null) return S.of(context).common_login;
    return user?.email ?? S.of(context).common_emailNotSet;
  }
}

class _ListItem extends StatelessWidget {
  const _ListItem({
    Key key,
    @required this.title,
    @required this.label,
    @required this.onTap,
    this.isSelected = false,
  }) : super(key: key);

  final String title;
  final String label;
  final GestureTapCallback onTap;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    final bool masterDetailLayout = MasterDetailContainer.isSupported(context);
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: Dimens.grid_1x,
          horizontal: Paddings.of(context).horizontal,
        ),
        child: ListItem(
          data: ListItemData(title: title, label: label),
          showArrow: !masterDetailLayout,
          isActive: isSelected && masterDetailLayout,
        ),
      ),
    );
  }
}

class _CreateAccountPlaceholder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(Dimens.grid_3x),
        child: Column(children: [
          Spacer(),
          Image.asset(Images.illustration_createAccount_256x152, width: 256, height: 152),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_5x, bottom: Dimens.grid_4x),
            child: Text(
              S.of(context).settings_accountPlaceholder_message,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline4.copyWith(
                    color: Theme.of(context).colorScheme.primaryVariant,
                  ),
            ),
          ),
          SmallRaisedButton(
            child: SmallButtonText(S.of(context).settings_accountPlaceholder_button),
            onPressed: () => Nav.loginOptions(),
          ),
          Spacer(),
        ]),
      ),
    );
  }
}
