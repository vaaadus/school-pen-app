import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/version.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';

class AboutModel extends Equatable {
  final String appVersion;

  const AboutModel({
    @required this.appVersion,
  });

  @override
  List<Object> get props => [appVersion];
}

class AboutViewModel extends ViewModel<AboutModel, Object> {
  final AppVersionProvider _versionProvider;

  AboutViewModel(this._versionProvider);

  @override
  void onStart() async {
    super.onStart();
    emitModel(await _model);
  }

  Future<AboutModel> get _model async {
    return AboutModel(
      appVersion: await _versionProvider.version,
    );
  }
}
