import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/settings/about/about_view_model.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/fill_viewport_scrollview.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/generated/l10n.dart';

/// Displays an info about the app.
class AboutPage extends LifecycleWidget {
  const AboutPage({Key key, this.enableNavigation = true}) : super(key: key);

  final bool enableNavigation;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _AboutPageState();
}

class _AboutPageState extends LifecycleWidgetState<AboutPage> with ViewModelLifecycle<AboutViewModel, AboutPage> {
  @override
  AboutViewModel provideViewModel() => AboutViewModel(Injection.get());

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: _buildContent,
          ),
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, AboutModel model) {
    final Paddings paddings = Paddings.of(context);

    return FillViewportScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (widget.enableNavigation)
            Align(
              alignment: Alignment.centerLeft,
              child: NavButton.back(),
            ),
          Spacer(flex: 3),
          Padding(
            padding: EdgeInsets.only(
              top: Dimens.grid_2x,
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: Image.asset(Images.ic_logo_100, width: 100, height: 100, excludeFromSemantics: true),
          ),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_3x),
            child: Text(
              S.of(context).app_name,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline3,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_1x),
            child: Text(
              'v' + model.appVersion,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          InkWell(
            onTap: () => Nav.developerPage(),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: Dimens.grid_1_5x),
              child: Text(
                S.of(context).common_byOurCompanyName,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                      color: Theme.of(context).colorScheme.primaryVariant,
                    ),
              ),
            ),
          ),
          Spacer(flex: 3),
          Padding(
            padding: EdgeInsets.only(
              top: Dimens.grid_2x,
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: RaisedButton(
              child: ButtonText(S.of(context).common_privacyPolicy),
              color: Theme.of(context).colorScheme.secondaryVariant,
              onPressed: () => Nav.privacyPolicy(),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: Dimens.grid_2x,
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: RaisedButton(
              child: ButtonText(S.of(context).common_credits),
              onPressed: () => Nav.settings_credits(),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: Dimens.grid_2x,
              bottom: Dimens.grid_4x,
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: RaisedButton(
              child: ButtonText(S.of(context).common_licenses),
              onPressed: () => showLicensePage(
                context: context,
                applicationName: S.of(context).app_name,
                applicationVersion: model.appVersion,
              ),
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
