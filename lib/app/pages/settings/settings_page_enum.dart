typedef OnChangeSettingsPage = void Function(SettingsPageEnum page);

enum SettingsPageEnum {
  account,
  premium,
  pcOrWebVersion,
  syncAndRestore,
  schoolYears,
  semesters,
  notifications,
  appearance,
  privacy,
  support,
  integrations,
  rateUs,
  share,
  about,
}

extension SettingsPageEnumExt on SettingsPageEnum {
  bool get isSelectable {
    switch (this) {
      case SettingsPageEnum.account:
      case SettingsPageEnum.premium:
      case SettingsPageEnum.syncAndRestore:
      case SettingsPageEnum.schoolYears:
      case SettingsPageEnum.semesters:
      case SettingsPageEnum.notifications:
      case SettingsPageEnum.appearance:
      case SettingsPageEnum.privacy:
      case SettingsPageEnum.support:
      case SettingsPageEnum.integrations:
      case SettingsPageEnum.about:
        return true;
      case SettingsPageEnum.pcOrWebVersion:
      case SettingsPageEnum.share:
      case SettingsPageEnum.rateUs:
        return false;
    }
    throw ArgumentError('Not support page: $this');
  }
}
