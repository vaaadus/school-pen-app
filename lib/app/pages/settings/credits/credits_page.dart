import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/generated/l10n.dart';

const String _flatIconUrl = 'https://www.flaticon.com/';

/// Shows legal staff for third-party works.
class CreditsPage extends StatefulWidget {
  const CreditsPage({Key key}) : super(key: key);

  @override
  _CreditsPageState createState() => _CreditsPageState();
}

class _CreditsPageState extends State<CreditsPage> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);

    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: CustomScrollView(slivers: [
            Toolbar(vsync: this),
            SliverList(
              delegate: SliverChildListDelegate([
                Padding(
                  padding: EdgeInsets.only(
                    top: (Dimens.grid_2x - Toolbar.bottomPadding),
                    bottom: Dimens.grid_2_5x,
                    left: paddings.horizontal,
                    right: paddings.horizontal,
                  ),
                  child: Text(
                    S.of(context).common_credits,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                _CreditsSection(
                  imageAsset: Images.ic_idea_36,
                  title: 'Pixel Perfect | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_image_24,
                  title: 'bqlqn | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_camera_24,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.illustration_notebook_192,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_tomorrow_24,
                  title: 'Pixel perfect | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_instagram_gradient_24,
                  title: 'Pixel perfect | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_whatsapp_green_24,
                  title: 'Pixel perfect | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_location_24,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_website_24,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.illustration_teacher_128,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_small_talk_24,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_teacher_blackboard_24,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.illustration_friend_128,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.illustration_abacus_192,
                  title: 'Dighital | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_book_36,
                  title: 'Dave Gandy | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_timetable_36,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.illustration_calendar_192,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.illustration_router_192,
                  title: 'Vectors Market | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.illustration_question_192,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.illustration_megaphone_128,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_exam_24,
                  title: 'Pixelmeetup | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.illustration_beach_128,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_homework_24,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.ic_percent_24,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
                _CreditsSection(
                  imageAsset: Images.illustration_empty_box_128,
                  title: 'Freepik | Flaticon',
                  url: _flatIconUrl,
                ),
              ]),
            ),
          ]),
        ),
      ),
    );
  }
}

class _CreditsSection extends StatelessWidget {
  const _CreditsSection({
    Key key,
    @required this.imageAsset,
    @required this.title,
    @required this.url,
  }) : super(key: key);

  final String imageAsset;
  final String title;
  final String url;

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return InkWell(
      onTap: () => Nav.url(url),
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: Dimens.grid_1_5x,
          horizontal: paddings.horizontal,
        ),
        child: Row(children: [
          Padding(
            padding: EdgeInsets.only(right: Dimens.grid_2x),
            child: Image.asset(imageAsset, width: 32, height: 32, fit: BoxFit.fill),
          ),
          Expanded(
            child: Text(title, style: Theme.of(context).textTheme.bodyText1),
          ),
          Padding(
            padding: EdgeInsets.only(left: Dimens.grid_2x),
            child: Image.asset(
              Images.ic_chevron_right_16,
              width: 16,
              height: 16,
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
          ),
        ]),
      ),
    );
  }
}
