import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/assets/themes.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/pages/settings/themes/themes_view_model.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows the user to configure the appearance (theme).
class ThemesPage extends LifecycleWidget {
  const ThemesPage({
    Key key,
    this.enableNavigation = true,
    @required this.onShowPremium,
  }) : super(key: key);

  final bool enableNavigation;
  final VoidCallback onShowPremium;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _ThemesPageState();
}

class _ThemesPageState extends LifecycleWidgetState<ThemesPage>
    with ViewModelLifecycle<ThemesViewModel, ThemesPage>, ProgressOverlayMixin, SingleTickerProviderStateMixin {
  @override
  ThemesViewModel provideViewModel() => ThemesViewModel(Injection.get());

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: (BuildContext context, ThemesModel model) {
              updateProgressOverlay(model.progressOverlay);
              return _buildContent(context, model);
            },
            onSignal: (BuildContext context, ThemesSignal signal) {
              if (signal == ThemesSignal.notifyThemeRewarded) {
                showSuccessToast(message: S.of(context).confirmation_themeUnlocked);
              } else if (signal == ThemesSignal.notifyUserMustRestoreInternet) {
                _showNoInternetDialog(context);
              } else if (signal == ThemesSignal.notifyAdFailedToLoad) {
                _showAdFailedToLoadDialog(context);
              }
            },
          ),
        ),
      ),
    );
  }

  void _showNoInternetDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_noInternetConnection,
      message: S.of(context).common_pleaseRestoreInternetConnection,
      primaryAction: DialogActionData(text: S.of(context).common_ok),
    );
  }

  void _showAdFailedToLoadDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      routeSettings: RouteSettings(name: R.settings_themes_adFailed),
      title: S.of(context).common_error,
      message: S.of(context).settings_themes_adFailedToLoad_message,
      primaryAction: DialogActionData(text: S.of(context).common_ok),
    );
  }

  Widget _buildContent(BuildContext context, ThemesModel model) {
    final Paddings paddings = Paddings.of(context);

    return CustomScrollView(slivers: [
      if (widget.enableNavigation) Toolbar(vsync: this),
      SliverList(
        delegate: SliverChildListDelegate([
          Padding(
            padding: EdgeInsets.only(
              top: widget.enableNavigation ? (Dimens.grid_2x - Toolbar.bottomPadding) : paddings.vertical,
              bottom: Dimens.grid_2_5x,
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: Text(
              S.of(context).common_appearance,
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          for (CustomThemeData data in model.themes)
            _CustomThemeWidget(
              data: data,
              onTap: () => viewModel.onSelectTheme(data),
            ),
          if (model.displayPremiumInfo && kDebugMode)
            InkWell(
              onTap: widget.onShowPremium,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: paddings.horizontal,
                  vertical: Dimens.grid_3x,
                ),
                child: Text(
                  S.of(context).settings_themes_unlockByAdsOrGoPremium,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Theme.of(context).colorScheme.primaryVariant,
                      ),
                ),
              ),
            ),
        ]),
      ),
    ]);
  }
}

class _CustomThemeWidget extends StatelessWidget {
  const _CustomThemeWidget({
    Key key,
    @required this.data,
    @required this.onTap,
  }) : super(key: key);

  final CustomThemeData data;
  final GestureTapCallback onTap;

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: paddings.horizontal,
          vertical: Dimens.grid_1_5x,
        ),
        child: Row(children: [
          SizedBox(
            width: 36,
            height: 36,
            child: Stack(children: [
              _buildBackgroundColorPreview(context),
              _buildSecondaryColorPreview(context),
            ]),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: Dimens.tens_2x),
              child: Text(
                ResourceLocator.themeToString(context, data.theme),
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
          ),
          if (data.isSelected)
            Image.asset(
              Images.ic_checkmark_24,
              width: 24,
              height: 24,
              color: Theme.of(context).colorScheme.secondary,
            ),
          if (!data.isSelected && !data.isUnlocked)
            Image.asset(
              Images.ic_rewarded_gift_32,
              width: 32,
              height: 32,
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
        ]),
      ),
    );
  }

  Widget _buildBackgroundColorPreview(BuildContext context) {
    return Positioned(
      left: 0,
      top: 0,
      width: 32,
      height: 32,
      child: Container(
        decoration: BoxDecoration(
          color: Themes.of(context, data.theme).colorScheme.background,
          shape: BoxShape.circle,
          border: Border.all(color: Theme.of(context).dividerColor),
        ),
      ),
    );
  }

  Widget _buildSecondaryColorPreview(BuildContext context) {
    return Positioned(
      left: 20,
      top: 20,
      width: 16,
      height: 16,
      child: Container(
        decoration: BoxDecoration(
          color: Themes.of(context, data.theme).colorScheme.secondary,
          shape: BoxShape.circle,
          border: Border.all(
            color: Theme.of(context).colorScheme.background,
            width: 2.0,
          ),
        ),
      ),
    );
  }
}
