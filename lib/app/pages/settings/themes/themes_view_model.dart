import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:pedantic/pedantic.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/ads/exception/ad_failed_to_load_exception.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/themes/model/custom_theme.dart';
import 'package:school_pen/domain/themes/themes_manager.dart';

class ThemesModel extends Equatable {
  final List<CustomThemeData> themes;
  final bool progressOverlay;
  final bool displayPremiumInfo;

  const ThemesModel({
    @required this.themes,
    @required this.progressOverlay,
    @required this.displayPremiumInfo,
  });

  @override
  List<Object> get props => [themes, progressOverlay, displayPremiumInfo];
}

class CustomThemeData extends Equatable {
  final CustomTheme theme;
  final bool isSelected;
  final bool isUnlocked;

  const CustomThemeData({@required this.theme, @required this.isSelected, @required this.isUnlocked});

  @override
  List<Object> get props => [theme, isSelected, isUnlocked];
}

enum ThemesSignal {
  notifyThemeRewarded,
  notifyUserMustRestoreInternet,
  notifyAdFailedToLoad,
}

class ThemesViewModel extends ViewModel<ThemesModel, ThemesSignal> {
  static const Logger _logger = Logger('ThemesViewModel');
  final ThemesManager _themesManager;
  bool _progressOverlay = false;

  ThemesViewModel(this._themesManager);

  @override
  void onStart() async {
    super.onStart();
    await _tryEmitModel();
    await _tryPreloadRewardedContent();
  }

  void onSelectTheme(CustomThemeData data) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      if (data.isUnlocked) {
        await _themesManager.setSelectedTheme(data.theme);
      } else {
        final bool unlocked = await _themesManager.playRewardedContentForTheme(data.theme);
        if (unlocked) {
          emitSignal(ThemesSignal.notifyThemeRewarded);
          await _themesManager.setSelectedTheme(data.theme);
        }
        unawaited(_tryPreloadRewardedContent());
      }
    } on NoInternetException {
      emitSignal(ThemesSignal.notifyUserMustRestoreInternet);
    } on AdFailedToLoadException {
      emitSignal(ThemesSignal.notifyAdFailedToLoad);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryPreloadRewardedContent() async {
    try {
      await _themesManager.preloadRewardedContent();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<ThemesModel> get _model async {
    final List<CustomThemeData> themes = [];

    final CustomTheme selectedTheme = _themesManager.selectedTheme;
    final List<CustomTheme> unlockedThemes = await _themesManager.unlockedThemes;

    for (CustomTheme theme in CustomTheme.values) {
      themes.add(CustomThemeData(
        theme: theme,
        isSelected: theme == selectedTheme,
        isUnlocked: unlockedThemes.contains(theme),
      ));
    }

    return ThemesModel(
      themes: themes,
      progressOverlay: _progressOverlay,
      displayPremiumInfo: _hasLockedThemes(themes),
    );
  }

  bool _hasLockedThemes(List<CustomThemeData> themes) {
    return themes.any((e) => !e.isUnlocked);
  }
}
