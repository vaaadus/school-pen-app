import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/picker/checkbox_dialog.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/pages/picker/radio_button_dialog.dart';
import 'package:school_pen/app/pages/settings/notifications/notifications_view_model.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/list_item.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

/// Allows the user to configure notification settings.
class NotificationsPage extends LifecycleWidget {
  const NotificationsPage({Key key, this.enableNavigation = true}) : super(key: key);

  final bool enableNavigation;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _NotificationsPageState();
}

class _NotificationsPageState extends LifecycleWidgetState<NotificationsPage>
    with ViewModelLifecycle<NotificationsViewModel, NotificationsPage>, SingleTickerProviderStateMixin {
  @override
  NotificationsViewModel provideViewModel() => NotificationsViewModel(Injection.get());

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: _buildContent,
          ),
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, NotificationsModel model) {
    final Paddings paddings = Paddings.of(context);

    return CustomScrollView(slivers: [
      if (widget.enableNavigation) Toolbar(vsync: this),
      SliverList(
        delegate: SliverChildListDelegate([
          Padding(
            padding: EdgeInsets.only(
              top: widget.enableNavigation ? (Dimens.grid_2x - Toolbar.bottomPadding) : paddings.vertical,
              bottom: Dimens.grid_1x,
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: Text(
              S.of(context).common_notifications,
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          _SwitchSection(
            title: S.of(context).common_agenda,
            enabled: model.agendaOptions.enabled,
            onToggle: (bool enabled) => viewModel.onSwitchAgenda(enabled),
          ),
          _Divider(),
          _ListItem(
            title: S.of(context).common_timeOfDay,
            label: model.agendaOptions.time.toTimeOfDay().format(context),
            onTap: () => _showTimePicker(context, model.agendaOptions.time),
          ),
          _ListItem(
            title: S.of(context).common_daysOfWeek,
            label: _formatWeekdays(context, model.agendaOptions.weekdays),
            onTap: () => _showWeekdaysPicker(context, model.agendaOptions.weekdays),
          ),
          _ListItem(
            title: S.of(context).common_options,
            label: model.agendaOptions.notifyIfNoEvents
                ? S.of(context).settings_notifications_notifyIfNoEvents
                : S.of(context).settings_notifications_doNotNotifyIfNoEvents,
            onTap: () => _showNotifyIfNoEventsDialog(context, model.agendaOptions.notifyIfNoEvents),
          ),
          _SwitchSection(
            title: S.of(context).common_timetable,
            enabled: model.timetableOptions.enabled,
            onToggle: (bool enabled) => viewModel.onSwitchTimetable(enabled),
          ),
          _Divider(),
          _ListItem(
            title: S.of(context).settings_notifications_reminderAboutUpcomingLessons,
            label: _formatUpcomingTimetableReminderInMinutes(
              context,
              model.timetableOptions.notifyBeforeLessonInMinutes,
            ),
            onTap: () => _showTimetableReminderOptions(context, model.timetableOptions.notifyBeforeLessonInMinutes),
          ),
        ]),
      ),
    ]);
  }

  String _formatWeekdays(BuildContext context, List<Weekday> weekdays) {
    if (weekdays.length == Weekday.values.length) return S.of(context).common_everyDay;
    if (weekdays.isEmpty) return S.of(context).common_never;

    String result = ResourceLocator.weekdayToString(context, weekdays.first);
    for (int i = 1; i < weekdays.length; i++) {
      result += ', ' + ResourceLocator.weekdayToString(context, weekdays[i]);
    }
    return result;
  }

  String _formatUpcomingTimetableReminderInMinutes(BuildContext context, int minutes) {
    if (minutes == 0) return S.of(context).settings_notifications_whenLessonStarts;

    return sprintf(S.of(context).settings_notifications_minutesBefore, [minutes]);
  }

  void _showTimePicker(BuildContext context, Time initialTime) async {
    final Time time = await showAdaptiveTimePicker(
      context: context,
      initialTime: initialTime,
    );

    if (time != null) {
      viewModel.onSelectAgendaTime(time);
    }
  }

  void _showWeekdaysPicker(BuildContext context, List<Weekday> initialWeekdays) async {
    final Map<Weekday, bool> result = await showCheckboxDialog(
      context: context,
      title: S.of(context).common_daysOfWeek,
      saveButtonText: S.of(context).common_save,
      itemBuilder: (BuildContext context) => [
        for (Weekday weekday in Weekday.values)
          CheckboxDialogItem(
            text: ResourceLocator.weekdayToString(context, weekday),
            defaultValue: initialWeekdays.contains(weekday),
            value: weekday,
          ),
      ],
    );

    if (result != null) {
      result.removeWhere((key, value) => !value);
      viewModel.onSelectAgendaDays(result.keys.toList());
    }
  }

  void _showNotifyIfNoEventsDialog(BuildContext context, bool enabled) async {
    final bool result = await showRadioButtonDialog(
      context: context,
      title: S.of(context).common_options,
      saveButtonText: S.of(context).common_save,
      itemBuilder: (BuildContext context) => [
        RadioButtonDialogItem(
          text: S.of(context).settings_notifications_notifyIfNoEvents,
          value: true,
          groupValue: enabled,
        ),
        RadioButtonDialogItem(
          text: S.of(context).settings_notifications_doNotNotifyIfNoEvents,
          value: false,
          groupValue: enabled,
        ),
      ],
    );

    if (result != null) {
      viewModel.onSwitchEmptyAgendaNotification(result);
    }
  }

  void _showTimetableReminderOptions(BuildContext context, int minutesBefore) async {
    final int result = await showRadioButtonDialog(
      context: context,
      title: S.of(context).settings_notifications_reminderAboutUpcomingLessons,
      saveButtonText: S.of(context).common_save,
      itemBuilder: (BuildContext context) => [
        RadioButtonDialogItem(
          text: S.of(context).settings_notifications_whenLessonStarts,
          value: 0,
          groupValue: minutesBefore,
        ),
        RadioButtonDialogItem(
          text: _formatUpcomingTimetableReminderInMinutes(context, 5),
          value: 5,
          groupValue: minutesBefore,
        ),
        RadioButtonDialogItem(
          text: _formatUpcomingTimetableReminderInMinutes(context, 15),
          value: 15,
          groupValue: minutesBefore,
        ),
        RadioButtonDialogItem(
          text: _formatUpcomingTimetableReminderInMinutes(context, 30),
          value: 30,
          groupValue: minutesBefore,
        ),
        RadioButtonDialogItem(
          text: _formatUpcomingTimetableReminderInMinutes(context, 60),
          value: 60,
          groupValue: minutesBefore,
        ),
      ],
    );

    if (result != null) {
      viewModel.onSelectTimetableReminder(result);
    }
  }
}

class _SwitchSection extends StatelessWidget {
  final String title;
  final bool enabled;
  final ValueChanged<bool> onToggle;

  const _SwitchSection({
    Key key,
    @required this.title,
    @required this.enabled,
    @required this.onToggle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);

    return Padding(
      padding: EdgeInsets.only(top: Dimens.grid_1x),
      child: Column(children: [
        InkWell(
          onTap: () => onToggle(!enabled),
          child: Padding(
            padding: EdgeInsets.only(
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    title.toUpperCase(),
                    style: Theme.of(context).textTheme.overline.copyWith(
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                  ),
                ),
                Semantics(
                  label: title,
                  checked: enabled,
                  child: Switch(value: enabled, onChanged: onToggle),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}

class _ListItem extends StatelessWidget {
  final String title;
  final String label;
  final VoidCallback onTap;

  const _ListItem({
    Key key,
    @required this.title,
    @required this.label,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Paddings.of(context).horizontal,
          vertical: Dimens.grid_1_5x,
        ),
        child: ListItem(
          titleStyle: Theme.of(context).textTheme.bodyText1,
          data: ListItemData(title: title, label: label),
        ),
      ),
    );
  }
}

class _Divider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        left: paddings.horizontal,
        right: paddings.horizontal,
      ),
      child: Divider(),
    );
  }
}
