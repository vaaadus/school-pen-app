import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/agenda/agenda_listener.dart';
import 'package:school_pen/domain/agenda/agenda_manager.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/lazy.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/userconfig/model/agenda_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/timetable_notification_options.dart';

class NotificationsModel extends Equatable {
  final AgendaNotificationOptions agendaOptions;
  final TimetableNotificationOptions timetableOptions;

  const NotificationsModel({
    @required this.agendaOptions,
    @required this.timetableOptions,
  });

  @override
  List<Object> get props => [agendaOptions, timetableOptions];
}

class NotificationsViewModel extends ViewModel<NotificationsModel, Object> implements AgendaListener {
  static const Logger _logger = Logger('NotificationsViewModel');
  final AgendaManager _agendaManager;

  NotificationsViewModel(this._agendaManager);

  @override
  void onStart() async {
    super.onStart();
    _agendaManager.addAgendaListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _agendaManager.removeAgendaListener(this);
    super.onStop();
  }

  @override
  void onAgendaChanged(AgendaManager manager) async {
    await _tryEmitModel();
  }

  void onSwitchAgenda(bool enabled) async {
    await _try(() async {
      AgendaNotificationOptions options = await _agendaManager.getAgendaOptions();
      options = options.copyWith(enabled: Optional(enabled));
      await _agendaManager.setAgendaOptions(options);
    });
  }

  void onSelectAgendaTime(Time time) async {
    await _try(() async {
      AgendaNotificationOptions options = await _agendaManager.getAgendaOptions();
      options = options.copyWith(time: Optional(time));
      await _agendaManager.setAgendaOptions(options);
    });
  }

  void onSelectAgendaDays(List<Weekday> weekdays) async {
    await _try(() async {
      AgendaNotificationOptions options = await _agendaManager.getAgendaOptions();
      options = options.copyWith(weekdays: Optional(weekdays));
      await _agendaManager.setAgendaOptions(options);
    });
  }

  void onSwitchEmptyAgendaNotification(bool enabled) async {
    await _try(() async {
      AgendaNotificationOptions options = await _agendaManager.getAgendaOptions();
      options = options.copyWith(notifyIfNoEvents: Optional(enabled));
      await _agendaManager.setAgendaOptions(options);
    });
  }

  void onSwitchTimetable(bool enabled) async {
    await _try(() async {
      TimetableNotificationOptions options = await _agendaManager.getTimetableOptions();
      options = options.copyWith(enabled: Optional(enabled));
      await _agendaManager.setTimetableOptions(options);
    });
  }

  void onSelectTimetableReminder(int minutesBefore) async {
    await _try(() async {
      TimetableNotificationOptions options = await _agendaManager.getTimetableOptions();
      options = options.copyWith(notifyBeforeLessonInMinutes: Optional(minutesBefore));
      await _agendaManager.setTimetableOptions(options);
    });
  }

  Future<void> _try(FutureProvider<void> provider) async {
    try {
      await provider();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<NotificationsModel> get _model async {
    return NotificationsModel(
      agendaOptions: await _agendaManager.getAgendaOptions(),
      timetableOptions: await _agendaManager.getTimetableOptions(),
    );
  }
}
