import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/settings/privacy/privacy_view_model.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows the user to configure privacy settings (analytics, diagnosis, ads).
class PrivacyPage extends LifecycleWidget {
  const PrivacyPage({Key key, this.enableNavigation = true}) : super(key: key);

  final bool enableNavigation;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _PrivacyPageState();
}

class _PrivacyPageState extends LifecycleWidgetState<PrivacyPage>
    with ViewModelLifecycle<PrivacyViewModel, PrivacyPage>, SingleTickerProviderStateMixin {
  @override
  PrivacyViewModel provideViewModel() => PrivacyViewModel(Injection.get(), Injection.get(), Injection.get());

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: _buildContent,
          ),
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, PrivacyModel model) {
    final Paddings paddings = Paddings.of(context);

    return CustomScrollView(slivers: [
      if (widget.enableNavigation) Toolbar(vsync: this),
      SliverList(
        delegate: SliverChildListDelegate([
          Padding(
            padding: EdgeInsets.only(
              top: widget.enableNavigation ? (Dimens.grid_2x - Toolbar.bottomPadding) : paddings.vertical,
              bottom: Dimens.grid_1x,
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: Text(
              S.of(context).common_privacy,
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          _PrivacySection(
            title: S.of(context).common_analytics,
            description: S.of(context).settings_privacy_analytics_description,
            enabled: model.enableAnalytics,
            onToggle: (bool enabled) => viewModel.onSwitchAnalytics(enabled),
          ),
          _Divider(),
          _PrivacySection(
            title: S.of(context).common_diagnosis,
            description: S.of(context).settings_privacy_diagnosis_description,
            enabled: model.enableDiagnosis,
            onToggle: (bool enabled) => viewModel.onSwitchDiagnosis(enabled),
          ),
          _Divider(),
          _PrivacySection(
            title: S.of(context).common_personalizedAds,
            description: S.of(context).settings_privacy_personalizedAds_description,
            enabled: model.enablePersonalizedAds,
            onToggle: (bool enabled) => viewModel.onSwitchPersonalizedAds(enabled),
          ),
          _Divider(),
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: paddings.horizontal,
                vertical: Dimens.grid_3x,
              ),
              child: SmallRaisedButton(
                onPressed: () => Nav.privacyPolicy(),
                color: Theme.of(context).colorScheme.primaryVariant,
                child: SmallButtonText(
                  S.of(context).common_privacyPolicy,
                  color: Theme.of(context).colorScheme.onPrimary,
                ),
              ),
            ),
          ),
        ]),
      ),
    ]);
  }
}

class _PrivacySection extends StatelessWidget {
  const _PrivacySection({
    Key key,
    @required this.title,
    @required this.description,
    @required this.enabled,
    @required this.onToggle,
  }) : super(key: key);

  final String title;
  final String description;
  final bool enabled;
  final ValueChanged<bool> onToggle;

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);

    return Padding(
      padding: EdgeInsets.only(top: Dimens.grid_1x),
      child: Column(children: [
        InkWell(
          onTap: () => onToggle(!enabled),
          child: Padding(
            padding: EdgeInsets.only(
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: Row(
              children: [
                Expanded(child: Text(title.toUpperCase(), style: Theme.of(context).textTheme.overline)),
                Semantics(
                  label: title,
                  checked: enabled,
                  child: Switch(value: enabled, onChanged: onToggle),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: paddings.horizontal),
          child: Text(
            description,
            style: Theme.of(context).textTheme.bodyText2.copyWith(
                  color: Theme.of(context).colorScheme.primaryVariant,
                ),
          ),
        ),
      ]),
    );
  }
}

class _Divider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        left: paddings.horizontal,
        right: paddings.horizontal,
        top: Dimens.grid_3x,
      ),
      child: Divider(),
    );
  }
}
