import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/ads/ad_manager.dart';
import 'package:school_pen/domain/ads/personalized_ads_consent_listener.dart';
import 'package:school_pen/domain/analytics/analytics_consent_listener.dart';
import 'package:school_pen/domain/analytics/analytics_manager.dart';
import 'package:school_pen/domain/diagnosis/diagnosis_consent_listener.dart';
import 'package:school_pen/domain/diagnosis/diagnosis_manager.dart';
import 'package:school_pen/domain/logger/logger.dart';

class PrivacyModel extends Equatable {
  final bool enableAnalytics;
  final bool enableDiagnosis;
  final bool enablePersonalizedAds;

  const PrivacyModel({
    @required this.enableAnalytics,
    @required this.enableDiagnosis,
    @required this.enablePersonalizedAds,
  });

  @override
  List<Object> get props => [enableAnalytics, enableDiagnosis, enablePersonalizedAds];
}

class PrivacyViewModel extends ViewModel<PrivacyModel, Object>
    implements AnalyticsConsentListener, DiagnosisConsentListener, PersonalizedAdsConsentListener {
  static const Logger _logger = Logger('PrivacyViewModel');
  final AnalyticsManager _analytics;
  final DiagnosisManager _diagnosis;
  final AdManager _ads;

  PrivacyViewModel(this._analytics, this._diagnosis, this._ads);

  @override
  void onStart() async {
    super.onStart();
    _analytics.addAnalyticsConsentListener(this);
    _diagnosis.addDiagnosisConsentListener(this);
    _ads.addPersonalizedAdsConsentListener(this);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _analytics.removeAnalyticsConsentListener(this);
    _diagnosis.removeDiagnosisConsentListener(this);
    _ads.removePersonalizedAdsConsentListener(this);
    super.onStop();
  }

  @override
  void onAnalyticsConsentChanged(bool enabled) async {
    await _tryEmitModel();
  }

  @override
  void onDiagnosisConsentChanged(bool enabled) async {
    await _tryEmitModel();
  }

  @override
  void onPersonalizedAdsConsentChanged(bool enabled) async {
    await _tryEmitModel();
  }

  void onSwitchAnalytics(bool enabled) async {
    try {
      await _analytics.setAnalyticsEnabled(enabled);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    await _tryEmitModel();
  }

  void onSwitchDiagnosis(bool enabled) async {
    try {
      await _diagnosis.setDiagnosisEnabled(enabled);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    await _tryEmitModel();
  }

  void onSwitchPersonalizedAds(bool enabled) async {
    try {
      await _ads.setPersonalizedAdsEnabled(enabled);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<PrivacyModel> get _model async {
    return PrivacyModel(
      enableAnalytics: _analytics.isAnalyticsEnabled,
      enableDiagnosis: _diagnosis.isDiagnosisEnabled,
      enablePersonalizedAds: _ads.arePersonalizedAdsEnabled,
    );
  }
}
