import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/intents.dart';
import 'package:school_pen/app/common/version.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/themes/model/custom_theme.dart';
import 'package:school_pen/domain/themes/themes_listener.dart';
import 'package:school_pen/domain/themes/themes_manager.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/user_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';

class SettingsModel extends Equatable {
  final User user;
  final CustomTheme theme;
  final String versionName;
  final bool supportsStore;
  final bool supportsNotifications;

  const SettingsModel({
    @required this.user,
    @required this.theme,
    @required this.versionName,
    @required this.supportsStore,
    @required this.supportsNotifications,
  });

  @override
  List<Object> get props => [user, theme, versionName, supportsStore];
}

class SettingsViewModel extends ViewModel<SettingsModel, dynamic> implements UserListener, ThemesListener {
  final UserManager _userManager;
  final ThemesManager _themesManager;
  final AppVersionProvider _versionProvider;
  final NotificationManager _notificationManager;

  SettingsViewModel(this._userManager, this._themesManager, this._versionProvider, this._notificationManager);

  @override
  void onResume() async {
    super.onResume();
    _userManager.addUserListener(this, notifyOnAttach: false);
    _themesManager.addThemesListener(this, notifyOnAttach: false);
    emitModel(await _model);
  }

  @override
  void onPause() {
    _userManager.removeUserListener(this);
    _themesManager.removeThemesListener(this);
    super.onPause();
  }

  @override
  void onUserChanged(User user) async {
    emitModel(await _model);
  }

  @override
  void onThemeSelected(CustomTheme theme) async {
    emitModel(await _model);
  }

  Future<SettingsModel> get _model async {
    return SettingsModel(
      user: await _userManager.getCurrentUser(),
      theme: _themesManager.selectedTheme,
      versionName: await _versionProvider.version,
      supportsStore: await Intents.isStoreSupported(),
      supportsNotifications: _notificationManager.isSupported,
    );
  }
}
