import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/schoolyear/model/add_school_year.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';

class SchoolYearsModel extends Equatable {
  final List<SchoolYearData> years;

  const SchoolYearsModel({
    @required this.years,
  });

  @override
  List<Object> get props => [years];
}

class SchoolYearData extends Equatable {
  final String id;
  final String name;
  final bool isActive;
  final bool canBeDeleted;

  const SchoolYearData({
    @required this.id,
    @required this.name,
    @required this.isActive,
    @required this.canBeDeleted,
  });

  @override
  List<Object> get props => [id, name, isActive, canBeDeleted];
}

enum SchoolYearsSignal {
  notifySchoolYearSelected,
  notifySchoolYearCreated,
  notifySchoolYearUpdated,
  notifySchoolYearDeleted,
}

class SchoolYearsViewModel extends ViewModel<SchoolYearsModel, SchoolYearsSignal> implements SchoolYearListener {
  static const Logger _logger = Logger('SchoolYearsViewModel');
  final SchoolYearManager _yearManager;

  SchoolYearsViewModel(this._yearManager);

  @override
  void onStart() async {
    super.onStart();
    _yearManager.addSchoolYearListener(this, notifyOnAttach: false);
    emitModel(await _model);
  }

  @override
  void onStop() {
    _yearManager.removeSchoolYearListener(this);
    super.onStop();
  }

  @override
  void onSchoolYearsChanged(SchoolYearManager manager) async {
    emitModel(await _model);
  }

  void onSelectSchoolYear(String id) async {
    try {
      await _yearManager.setActive(id);
      emitSignal(SchoolYearsSignal.notifySchoolYearSelected);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    emitModel(await _model);
  }

  void onCreateSchoolYear(AddSchoolYear year) async {
    try {
      await _yearManager.create(year);
      emitSignal(SchoolYearsSignal.notifySchoolYearCreated);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    emitModel(await _model);
  }

  void onUpdateSchoolYear(String id, AddSchoolYear year) async {
    try {
      await _yearManager.update(id, year);
      emitSignal(SchoolYearsSignal.notifySchoolYearUpdated);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    emitModel(await _model);
  }

  void onDeleteSchoolYear(String id) async {
    try {
      await _yearManager.delete(id);
      emitSignal(SchoolYearsSignal.notifySchoolYearDeleted);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    emitModel(await _model);
  }

  Future<SchoolYearsModel> get _model async {
    final List<SchoolYearData> years = [];
    try {
      final SchoolYear activeYear = await _yearManager.getActive();
      for (SchoolYear year in await _yearManager.getAll()) {
        years.add(SchoolYearData(
          id: year.id,
          name: year.name,
          isActive: year.id == activeYear.id,
          canBeDeleted: await _yearManager.canDelete(year.id),
        ));
      }
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    return SchoolYearsModel(years: years);
  }
}
