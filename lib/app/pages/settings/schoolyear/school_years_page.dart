import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/options_dialog.dart';
import 'package:school_pen/app/pages/picker/text_field_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/pages/settings/schoolyear/school_years_view_model.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/schoolyear/model/add_school_year.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows to view/edit/create school years.
class SchoolYearsPage extends LifecycleWidget {
  const SchoolYearsPage({Key key, this.enableNavigation = true}) : super(key: key);

  final bool enableNavigation;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _SchoolYearsPageState();
}

class _SchoolYearsPageState extends LifecycleWidgetState<SchoolYearsPage>
    with ViewModelLifecycle<SchoolYearsViewModel, SchoolYearsPage>, SingleTickerProviderStateMixin {
  @override
  SchoolYearsViewModel provideViewModel() => SchoolYearsViewModel(Injection.get());

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: _buildContent,
            onSignal: (BuildContext context, SchoolYearsSignal signal) {
              if (signal == SchoolYearsSignal.notifySchoolYearSelected) {
                showSuccessToast(message: S.of(context).confirmation_saveSuccess);
              } else if (signal == SchoolYearsSignal.notifySchoolYearCreated) {
                showSuccessToast(message: S.of(context).confirmation_saveSuccess);
              } else if (signal == SchoolYearsSignal.notifySchoolYearUpdated) {
                showSuccessToast(message: S.of(context).confirmation_saveSuccess);
              } else if (signal == SchoolYearsSignal.notifySchoolYearDeleted) {
                showSuccessToast(message: S.of(context).confirmation_deleted);
              }
            },
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          heroTag: null,
          label: ButtonText(S.of(context).common_newSchoolYear),
          onPressed: () => _showSchoolYearCreateDialog(context),
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, SchoolYearsModel model) {
    final Paddings paddings = Paddings.of(context);

    return CustomScrollView(slivers: [
      if (widget.enableNavigation) Toolbar(vsync: this),
      SliverList(
        delegate: SliverChildListDelegate([
          Padding(
            padding: EdgeInsets.only(
              top: widget.enableNavigation ? (Dimens.grid_2x - Toolbar.bottomPadding) : paddings.vertical,
              bottom: Dimens.grid_3x,
              left: paddings.horizontal,
              right: paddings.horizontal,
            ),
            child: Text(
              S.of(context).common_schoolYears,
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          ..._buildYears(context, model.years),
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: Dimens.grid_4x,
              horizontal: paddings.horizontal,
            ),
            child: Text(
              S.of(context).settings_schoolYears_description,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText2.copyWith(
                    color: Theme.of(context).colorScheme.primaryVariant,
                  ),
            ),
          )
        ]),
      ),
    ]);
  }

  Iterable<Widget> _buildYears(BuildContext context, List<SchoolYearData> years) sync* {
    final Paddings paddings = Paddings.of(context);

    for (int i = 0; i < years.length; i++) {
      yield _SchoolYearRow(
        key: ValueKey(years[i]),
        year: years[i],
        onTap: () => _showSchoolYearOptions(context, years[i]),
      );

      if (i + 1 < years.length) {
        yield Padding(
          padding: EdgeInsets.symmetric(horizontal: paddings.horizontal),
          child: Divider(),
        );
      }
    }
  }

  void _showSchoolYearOptions(BuildContext context, SchoolYearData year) async {
    final VoidCallback option = await showOptionsDialog(
      context: context,
      title: year.name,
      itemBuilder: (BuildContext context) => [
        if (!year.isActive)
          OptionDialogItem(
            imageAsset24: Images.ic_checkmark_filled_24,
            text: S.of(context).settings_schoolYears_setAsCurrent,
            value: () => viewModel.onSelectSchoolYear(year.id),
          ),
        OptionDialogItem(
          imageAsset24: Images.ic_edit_field_24,
          text: S.of(context).common_edit,
          value: () => _showSchoolYearEditDialog(context, year),
        ),
        if (year.canBeDeleted)
          OptionDialogItem(
            imageAsset24: Images.ic_trash_24,
            text: S.of(context).common_delete,
            customColor: Theme.of(context).colorScheme.error,
            value: () => _showSchoolYearDeleteConfirmation(context, year),
          )
      ],
    );

    option?.call();
  }

  void _showSchoolYearDeleteConfirmation(BuildContext context, SchoolYearData year) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_delete,
      message: S.of(context).settings_schoolYears_deleteDialog_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteSchoolYear(year.id),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }

  void _showSchoolYearCreateDialog(BuildContext context) {
    _showSchoolYearDialog(
      context: context,
      routeSettings: RouteSettings(name: R.settings_schoolYears_add),
      onSubmit: (AddSchoolYear newYear) => viewModel.onCreateSchoolYear(newYear),
    );
  }

  void _showSchoolYearEditDialog(BuildContext context, SchoolYearData initialYear) {
    assert(initialYear != null);

    _showSchoolYearDialog(
      context: context,
      routeSettings: RouteSettings(name: R.settings_schoolYears_edit),
      initialYear: initialYear,
      onSubmit: (AddSchoolYear newYear) => viewModel.onUpdateSchoolYear(initialYear.id, newYear),
    );
  }

  void _showSchoolYearDialog({
    @required BuildContext context,
    @required RouteSettings routeSettings,
    SchoolYearData initialYear,
    @required void Function(AddSchoolYear newYear) onSubmit,
  }) async {
    final String text = await showTextFieldDialog(
      context: context,
      routeSettings: routeSettings,
      initialText: initialYear?.name,
      title: initialYear != null ? S.of(context).common_editSchoolYear : S.of(context).common_newSchoolYear,
      placeholder: S.of(context).common_name,
      submitText: initialYear != null ? S.of(context).common_save : S.of(context).common_create,
      validator: (String text) {
        if (Strings.isBlank(text)) {
          return FieldMustNotBeEmptyException();
        }
        return null;
      },
    );

    if (text != null) {
      onSubmit(AddSchoolYear(name: text, updatedAt: DateTime.now()));
    }
  }
}

class _SchoolYearRow extends StatelessWidget {
  const _SchoolYearRow({Key key, @required this.year, @required this.onTap}) : super(key: key);

  final SchoolYearData year;
  final GestureTapCallback onTap;

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);

    return MergeSemantics(
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: Dimens.grid_2x,
            horizontal: paddings.horizontal,
          ),
          child: Row(children: [
            Image.asset(
              Images.ic_item_arrow_24,
              width: 24,
              height: 24,
              color: Theme.of(context).colorScheme.primaryVariant,
              excludeFromSemantics: true,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Dimens.grid_2x),
                child: Text(year.name, style: Theme.of(context).textTheme.bodyText1),
              ),
            ),
            if (year.isActive)
              Image.asset(
                Images.ic_checkmark_24,
                width: 24,
                height: 24,
                color: Theme.of(context).colorScheme.secondary,
                semanticLabel: S.of(context).common_active,
              ),
          ]),
        ),
      ),
    );
  }
}
