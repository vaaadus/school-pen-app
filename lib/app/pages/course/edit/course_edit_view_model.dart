import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/math.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/add_course.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/teacher_listener.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';

class CourseEditModel extends Equatable {
  final bool progressOverlay;
  final bool discardChangesPopup;
  final AddCourse course;
  final List<Teacher> availableTeachers;
  final List<Teacher> selectedTeachers;
  final String creditHours;
  final Exception nameError;

  const CourseEditModel({
    @required this.progressOverlay,
    @required this.discardChangesPopup,
    @required this.course,
    @required this.availableTeachers,
    @required this.selectedTeachers,
    @required this.creditHours,
    this.nameError,
  });

  @override
  List<Object> get props =>
      [progressOverlay, discardChangesPopup, course, availableTeachers, selectedTeachers, creditHours, nameError];
}

class CourseEditSignal extends Equatable {
  static const notifyCourseSaved = CourseEditSignal._('saved');

  final String tag;

  const CourseEditSignal._(this.tag);

  @override
  List<Object> get props => [tag];
}

class PopCourseEditSignal extends CourseEditSignal {
  final String courseId;

  const PopCourseEditSignal._([this.courseId]) : super._('pop');

  @override
  List<Object> get props => [...super.props, courseId];
}

class CourseEditViewModel extends ViewModel<CourseEditModel, CourseEditSignal>
    implements CourseListener, TeacherListener {
  static const Logger _logger = Logger('CourseEditViewModel');

  final SchoolYearManager _schoolYearManager;
  final CourseManager _courseManager;
  final TeacherManager _teacherManager;
  final String _courseId;

  bool _progressOverlay = false;
  AddCourse _originalCourse;
  AddCourse _editedCourse;
  String _creditHours;
  Exception _nameError;

  CourseEditViewModel(this._schoolYearManager, this._courseManager, this._teacherManager, this._courseId);

  @override
  void onStart() async {
    super.onStart();
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _teacherManager.addTeacherListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _courseManager.removeCourseListener(this);
    _teacherManager.removeTeacherListener(this);
    super.onStop();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onTeachersChanged(TeacherManager manager) async {
    await _tryEmitModel();
  }

  void onSaveCourse() async {
    final AddCourse course = _editedCourse;
    if (!await _validate(course)) return;

    await _tryWithProgressOverlay(() async {
      String courseIdResult;
      if (_courseId != null) {
        await _courseManager.update(_courseId, course);
        courseIdResult = _courseId;
      } else {
        courseIdResult = await _courseManager.create(course);
      }
      emitSignal(CourseEditSignal.notifyCourseSaved);
      emitSingleSignal(PopCourseEditSignal._(courseIdResult));
    });
  }

  Future<void> onEditName(String text) async {
    _editedCourse = _editedCourse.copyWith(name: Optional(text));

    await _tryEmitModel();
  }

  void onAddTeacher(String teacherId) async {
    _editedCourse = _editedCourse.copyWith(
      teachersIds: Optional(
        _editedCourse.teachersIds.plus(teacherId).skipDuplicates(),
      ),
    );

    await _tryEmitModel();
  }

  void onDeleteTeacher(String teacherId) async {
    _editedCourse = _editedCourse.copyWith(
      teachersIds: Optional(
        _editedCourse.teachersIds.minus(teacherId),
      ),
    );

    await _tryEmitModel();
  }

  void onEditRoom(String text) async {
    _editedCourse = _editedCourse.copyWith(room: Optional(text));

    await _tryEmitModel();
  }

  void onEditCreditHours(String creditHours) async {
    _creditHours = creditHours;
    _editedCourse = _editedCourse.copyWith(creditHours: Optional(tryParseDouble(creditHours)));

    await _tryEmitModel();
  }

  void onEditColor(ColorEnum color) async {
    _editedCourse = _editedCourse.copyWith(color: Optional(color));

    await _tryEmitModel();
  }

  void onEditNote(String text) async {
    _editedCourse = _editedCourse.copyWith(note: Optional(text));

    await _tryEmitModel();
  }

  Future<bool> _validate(AddCourse course) async {
    _nameError = null;
    await _tryEmitModel();

    if (Strings.isBlank(course.name)) {
      _nameError = FieldMustNotBeEmptyException();
      await _tryEmitModel();
      return false;
    } else {
      return true;
    }
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<CourseEditModel> get _model async {
    final AddCourse course = await _obtainCourse();
    if (course == null) return null;

    final List<Teacher> teachers = await _teacherManager.getAll(schoolYearId: course.schoolYearId);
    final List<Teacher> selectedTeachers = _toTeacherModels(teachers, course.teachersIds).toList();
    final List<Teacher> availableTeachers = teachers.minusAll(selectedTeachers);

    return CourseEditModel(
      progressOverlay: _progressOverlay,
      discardChangesPopup: _originalCourse == null || _editedCourse != _originalCourse,
      course: course,
      availableTeachers: availableTeachers,
      selectedTeachers: selectedTeachers,
      creditHours: _creditHours,
      nameError: _nameError,
    );
  }

  Iterable<Teacher> _toTeacherModels(List<Teacher> teachers, List<String> teachersIds) sync* {
    final Map<String, Teacher> teacherById = teachers.asMap().map((key, value) => MapEntry(value.id, value));
    for (String teacherId in teachersIds) {
      final Teacher teacher = teacherById[teacherId];
      if (teacher != null) yield teacher;
    }
  }

  Future<AddCourse> _obtainCourse() async {
    if (_editedCourse != null) return _editedCourse;

    if (_courseId != null) {
      final Course course = await _courseManager.getById(_courseId);
      _originalCourse = course?.toAddCourse()?.copyWith(updatedAt: Optional(DateTime.now()));
      _editedCourse = _originalCourse;
      _creditHours = _editedCourse?.creditHours?.toString();
    } else {
      final SchoolYear year = await _schoolYearManager.getActive();
      _editedCourse = AddCourse(
        schoolYearId: year.id,
        name: '',
        room: '',
        teachersIds: [],
        note: '',
        updatedAt: DateTime.now(),
      );
    }

    return _editedCourse;
  }
}
