import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/course/edit/course_edit_view_model.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/color_enum_picker_dialog.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/save_or_discard_footer.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/generated/l10n.dart';

/// A callback invoked upon the request to close/pop the details page.
/// The [courseId] is the id of created or updated course or null.
typedef OnCourseBackCallback = void Function(BuildContext context, String courseId);

/// Offers the user a possibility to edit a course (if [courseId] not null)
/// or create a new one if course is null.
class CourseEditPage extends LifecycleWidget {
  final String courseId;
  final OnCourseBackCallback onBack;

  const CourseEditPage({
    Key key,
    this.courseId,
    this.onBack = Navigator.pop,
  }) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _CourseEditPageState();
}

class _CourseEditPageState extends LifecycleWidgetState<CourseEditPage>
    with ViewModelLifecycle<CourseEditViewModel, CourseEditPage>, ProgressOverlayMixin, SingleTickerProviderStateMixin {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _roomController = TextEditingController();
  final TextEditingController _creditHoursController = TextEditingController();
  final TextEditingController _websiteController = TextEditingController();
  final TextEditingController _noteController = TextEditingController();

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _roomFocus = FocusNode();
  final FocusNode _creditHoursFocus = FocusNode();
  final FocusNode _websiteFocus = FocusNode();
  final FocusNode _noteFocus = FocusNode();

  @override
  CourseEditViewModel provideViewModel() {
    return CourseEditViewModel(Injection.get(), Injection.get(), Injection.get(), widget.courseId);
  }

  @override
  void dispose() {
    _nameController.dispose();
    _roomController.dispose();
    _creditHoursController.dispose();
    _websiteController.dispose();
    _noteController.dispose();
    _nameFocus.dispose();
    _roomFocus.dispose();
    _creditHoursFocus.dispose();
    _websiteFocus.dispose();
    _noteFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder(
      viewModel: viewModel,
      builder: (BuildContext context, CourseEditModel model) {
        updateProgressOverlay(model.progressOverlay);
        return _buildScaffold(context, model);
      },
      onSignal: (BuildContext context, CourseEditSignal signal) {
        if (signal == CourseEditSignal.notifyCourseSaved) {
          showSuccessToast(message: S.of(context).confirmation_saveSuccess);
        } else if (signal is PopCourseEditSignal) {
          widget.onBack(context, signal.courseId);
        }
      },
    );
  }

  Widget _buildScaffold(BuildContext context, CourseEditModel model) {
    return SystemUiOverlayAnnotation(
      systemNavigationBarColor: Theme.of(context).colorScheme.surface,
      child: Scaffold(
        body: SafeArea(
          child: _buildContent(context, model),
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, CourseEditModel model) {
    Widget result = CustomScrollView(slivers: [
      Toolbar(
        vsync: this,
        onBack: (BuildContext context) => _onWillPop(context, model),
        navType: NavButtonType.close,
        horizontalPadding: Dimens.grid_3x,
      ),
      SliverList(
        delegate: SliverChildListDelegate([
          _buildName(context, model),
          Divider(),
          _buildTeachers(context, model),
          Divider(),
          _buildRoom(context, model),
          Divider(),
          _buildCreditHours(context, model),
          Divider(),
          _buildColor(context, model),
          Divider(),
          _buildNote(context, model),
          Divider(),
          Container(height: Paddings.of(context).vertical),
        ]),
      ),
    ]);

    result = Column(children: [
      Expanded(child: result),
      SaveOrDiscardFooter(
        label: S.of(context).common_course,
        onSave: () => viewModel.onSaveCourse(),
        onDiscard: () => widget.onBack(context, null),
      ),
    ]);

    result = WillPopScope(
      onWillPop: () => _onWillPop(context, model),
      child: result,
    );

    return result;
  }

  Widget _buildName(BuildContext context, CourseEditModel model) {
    _nameController.setTextIfChanged(model.course.name);

    return Padding(
      padding: EdgeInsets.only(
        bottom: Dimens.grid_1x,
        left: Dimens.grid_7_5x,
        right: Dimens.grid_3x,
      ),
      child: TextField(
        controller: _nameController,
        focusNode: _nameFocus,
        autofocus: true,
        style: Theme.of(context).textTheme.headline5,
        maxLines: null,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: S.of(context).common_course,
          errorText: ResourceLocator.errorToText(context, model.nameError),
        ),
        textCapitalization: TextCapitalization.sentences,
        onChanged: (String text) => viewModel.onEditName(text),
        onSubmitted: (String text) => _unfocusTextFields(),
      ),
    );
  }

  Widget _buildTeachers(BuildContext context, CourseEditModel model) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            Images.ic_person_24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(
              left: Dimens.grid_2x,
              top: model.selectedTeachers.isNotEmpty ? Dimens.grid_1x : 0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Semantics(
                  label: S.of(context).common_teachers,
                  container: true,
                  child: Column(children: [
                    for (Teacher teacher in model.selectedTeachers) _buildTeacherRow(context, model, teacher),
                  ]),
                ),
                _buildAddTeacherRow(context, model),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildTeacherRow(BuildContext context, CourseEditModel model, Teacher teacher) {
    return Row(children: [
      Expanded(
        child: Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_1x,
            bottom: Dimens.grid_1x,
            left: Dimens.grid_1x,
          ),
          child: Text(
            teacher.name,
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ),
      MergeSemantics(
        child: InkWell(
          onTap: () {
            _unfocusTextFields();
            viewModel.onDeleteTeacher(teacher.id);
          },
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: Dimens.grid_1x, horizontal: Dimens.grid_3x),
            child: Image.asset(
              Images.ic_close_24,
              width: 16,
              height: 16,
              color: Theme.of(context).colorScheme.primaryVariant,
              semanticLabel: S.of(context).common_delete,
            ),
          ),
        ),
      )
    ]);
  }

  Widget _buildAddTeacherRow(BuildContext context, CourseEditModel model) {
    final String addTeacherId = 'add-teacher-id';
    return MergeSemantics(
      child: PopupMenuButton(
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          for (Teacher teacher in model.availableTeachers)
            PopupMenuItem(
              value: teacher.id,
              child: Text(teacher.name),
            ),
          PopupMenuDivider(),
          PopupMenuItem(
            value: addTeacherId,
            child: Text(
              S.of(context).common_addTeacher,
              style: Theme.of(context).popupMenuTheme.textStyle.copyWith(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
            ),
          ),
        ],
        onSelected: (String id) async {
          _unfocusTextFields();

          if (id == addTeacherId) {
            final String teacherId = await Nav.people_teacherAdd();
            if (teacherId != null) viewModel.onAddTeacher(teacherId);
          } else {
            viewModel.onAddTeacher(id);
          }
        },
        child: Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_1x,
          ),
          child: Text(
            S.of(context).common_pickTeacher,
            style: Theme.of(context).textTheme.bodyText1.copyWith(
                  color: Theme.of(context).colorScheme.primaryVariant,
                ),
          ),
        ),
      ),
    );
  }

  Widget _buildRoom(BuildContext context, CourseEditModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_location_24,
      value: model.course.room,
      placeholder: S.of(context).common_room,
      controller: _roomController,
      focus: _roomFocus,
      model: model,
      onChanged: (String text) => viewModel.onEditRoom(text),
      onSubmitted: (String text) => _creditHoursFocus.requestFocus(),
    );
  }

  Widget _buildCreditHours(BuildContext context, CourseEditModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_scale_24,
      value: model.creditHours,
      placeholder: S.of(context).common_creditHours,
      controller: _creditHoursController,
      focus: _creditHoursFocus,
      model: model,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      onChanged: (String text) => viewModel.onEditCreditHours(text),
      onSubmitted: (String text) => _unfocusTextFields(),
    );
  }

  Widget _buildColor(BuildContext context, CourseEditModel model) {
    return Row(children: [
      Expanded(
        child: InkWell(
          onTap: () => _showColorPicker(context, model),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
            child: Row(children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: Dimens.grid_3_5x),
                child: Container(
                  width: 16,
                  height: 16,
                  decoration: BoxDecoration(
                    color: ResourceLocator.colorEnumToColor(model.course.color),
                    border: model.course.color == null
                        ? Border.all(color: Theme.of(context).colorScheme.primaryVariant)
                        : null,
                    borderRadius: BorderRadius.circular(Dimens.grid_0_5x),
                  ),
                ),
              ),
              Expanded(
                child: Text(
                  ResourceLocator.colorEnumToString(context, model.course.color) ?? S.of(context).common_pickColor,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        color: model.course.color == null
                            ? Theme.of(context).colorScheme.primaryVariant
                            : Theme.of(context).colorScheme.primary,
                      ),
                ),
              ),
            ]),
          ),
        ),
      ),
      if (model.course.color != null)
        MergeSemantics(
          child: InkWell(
            onTap: () {
              _unfocusTextFields();
              viewModel.onEditColor(null);
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x, horizontal: Dimens.grid_3x),
              child: Image.asset(
                Images.ic_close_24,
                width: 16,
                height: 16,
                color: Theme.of(context).colorScheme.primaryVariant,
                semanticLabel: S.of(context).common_delete,
              ),
            ),
          ),
        ),
    ]);
  }

  Widget _buildNote(BuildContext context, CourseEditModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_description_24,
      value: model.course.note,
      placeholder: S.of(context).common_addNote,
      controller: _noteController,
      focus: _noteFocus,
      model: model,
      textInputAction: TextInputAction.newline,
      keyboardType: TextInputType.multiline,
      multiline: true,
      onChanged: (String text) => viewModel.onEditNote(text),
    );
  }

  Widget _buildTextFieldWithIcon({
    @required BuildContext context,
    @required String imageAsset24,
    @required String value,
    @required String placeholder,
    @required TextEditingController controller,
    @required FocusNode focus,
    @required CourseEditModel model,
    TextInputType keyboardType = TextInputType.text,
    TextInputAction textInputAction = TextInputAction.next,
    bool multiline = false,
    ValueChanged<String> onChanged,
    ValueChanged<String> onSubmitted,
  }) {
    controller.setTextIfChanged(value);

    Widget result = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            imageAsset24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_1_5x, vertical: Dimens.grid_0_5x),
            child: TextField(
              controller: controller,
              focusNode: focus,
              style: Theme.of(context).textTheme.bodyText1,
              textInputAction: textInputAction,
              keyboardType: keyboardType,
              maxLines: multiline ? null : 1,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: placeholder,
              ),
              onChanged: onChanged,
              onSubmitted: onSubmitted,
            ),
          ),
        ),
      ],
    );

    return result;
  }

  void _showColorPicker(BuildContext context, CourseEditModel model) async {
    _unfocusTextFields();

    final color = await showColorEnumPickerDialog(context: context);

    if (color != null) {
      viewModel.onEditColor(color);
    }
  }

  Future<bool> _onWillPop(BuildContext context, CourseEditModel model) async {
    if (model.discardChangesPopup) {
      _unfocusTextFields();
      _showSaveChangesDialog(context);
    } else {
      Navigator.of(context).pop();
    }
    return false;
  }

  void _showSaveChangesDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_discardChangesQuestion,
      primaryAction: DialogActionData(
        text: S.of(context).common_keepEditing,
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_discard,
        onTap: () => widget.onBack(context, null),
      ),
    );
  }

  void _unfocusTextFields() {
    _nameFocus.unfocus();
    _roomFocus.unfocus();
    _creditHoursFocus.unfocus();
    _websiteFocus.unfocus();
    _noteFocus.unfocus();
  }
}
