import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/typography.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/common/formatter/grades_formatter.dart';
import 'package:school_pen/app/widgets/bordered_container.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';

const double _iconWidth = 40;
const double _iconHeight = 40;

/// Displays a grade on a card.
class GradeCard extends StatelessWidget {
  final GradingSystem gradingSystem;
  final GradeCategory gradeCategory;
  final Grade grade;
  final GestureTapCallback onTap;
  final GestureTapCallback onLongPress;

  const GradeCard({
    Key key,
    @required this.gradingSystem,
    @required this.gradeCategory,
    @required this.grade,
    this.onTap,
    this.onLongPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicWidth(
      child: BorderedContainer(
        onTap: onTap,
        onLongPress: onLongPress,
        padding: EdgeInsets.all(Dimens.grid_1_5x),
        child: Column(children: [
          Row(children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  GradesFormatter.formatName(context, grade.name),
                  style: Theme.of(context).textTheme.headline6.copyWith(
                        color: Strings.isNotBlank(grade.name)
                            ? Theme.of(context).colorScheme.primary
                            : Theme.of(context).colorScheme.primaryVariant,
                      ),
                ),
                Text(
                  GradesFormatter.formatGradeCategoryWithDefaults(context, gradeCategory),
                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                        color: Theme.of(context).colorScheme.primaryVariant,
                      ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: Dimens.grid_2x),
              child: GradeIcon(
                gradingSystem: gradingSystem,
                grade: grade,
                width: _iconWidth,
                height: _iconHeight,
              ),
            ),
          ]),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_1x),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  child: Text(
                    DateFormatter.formatShortRecentDate(grade.dateTime),
                    style: Theme.of(context).textTheme.caption.copyWith(
                          color: Theme.of(context).colorScheme.primaryVariant,
                        ),
                  ),
                ),
                SizedBox(
                  width: _iconWidth,
                  child: Text(
                    GradesFormatter.formatNamedWeight(context, grade.weight),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.caption2(context).copyWith(
                          color: Theme.of(context).colorScheme.primaryVariant,
                        ),
                  ),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

class GradeIcon extends StatelessWidget {
  final GradingSystem gradingSystem;
  final Grade grade;
  final double width;
  final double height;

  const GradeIcon({
    Key key,
    @required this.gradingSystem,
    @required this.grade,
    this.width = 36,
    this.height = 36,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String text = gradingSystem.decorate(grade.value);
    return ExcludeSemantics(
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: GradesFormatter.getGradeColor(grade.value),
          borderRadius: BorderRadius.all(Radius.circular(width / 2)),
        ),
        child: Center(
          child: Text(text, style: _getTextStyle(context, text)),
        ),
      ),
    );
  }

  TextStyle _getTextStyle(BuildContext context, String text) {
    final TextStyle style = Theme.of(context).textTheme.headline6.copyWith(color: Colors.white);
    if (text.length == 3) return style.copyWith(fontSize: style.fontSize * 0.75);
    if (text.length == 4) return style.copyWith(fontSize: style.fontSize * 0.65);
    if (text.length > 4) return style.copyWith(fontSize: style.fontSize * 0.55);
    return style;
  }
}
