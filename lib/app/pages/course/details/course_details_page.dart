import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/typography.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/common/formatter/number_formatter.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/course/details/course_details_view_model.dart';
import 'package:school_pen/app/pages/course/grades/grade_page.dart';
import 'package:school_pen/app/pages/exam/exams_page.dart';
import 'package:school_pen/app/pages/homework/homework_page.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/reminder/reminders_page.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/bordered_container.dart';
import 'package:school_pen/app/widgets/bordered_list_item.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/master_detail_container.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/generated/l10n.dart';

import 'grade_card.dart';

const double _listItemHeight = 112;

/// Displays details of the course, related grades & files.
class CourseDetailsPage extends LifecycleWidget {
  final String courseId;
  final OnBackCallback onBack;

  const CourseDetailsPage({
    Key key,
    @required this.courseId,
    this.onBack = Navigator.pop,
  }) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _CourseDetailsPageState();
}

class _CourseDetailsPageState extends LifecycleWidgetState<CourseDetailsPage>
    with
        ViewModelLifecycle<CourseDetailsViewModel, CourseDetailsPage>,
        ProgressOverlayMixin,
        SingleTickerProviderStateMixin {
  Widget _asideWidget;

  @override
  CourseDetailsViewModel provideViewModel() => CourseDetailsViewModel(Injection.get(), Injection.get(), Injection.get(),
      Injection.get(), Injection.get(), Injection.get(), Injection.get(), widget.courseId);

  @override
  Widget build(BuildContext context) {
    final Widget scaffold = Scaffold(
      body: SafeArea(
        child: ViewModelBuilder(
          viewModel: viewModel,
          builder: (BuildContext context, CourseDetailsModel model) {
            updateProgressOverlay(model.progressOverlay);
            return _buildContent(context, model);
          },
          onSignal: (BuildContext context, CourseDetailsSignal signal) {
            if (signal == CourseDetailsSignal.notifyCourseDeleted) {
              showSuccessToast(message: S.of(context).confirmation_deleted);
            } else if (signal == CourseDetailsSignal.notifyGradeDeleted) {
              showSuccessToast(message: S.of(context).confirmation_deleted);
            } else if (signal == CourseDetailsSignal.pop) {
              widget.onBack(context);
            }
          },
        ),
      ),
    );

    return MasterDetailContainer(
      masterBuilder: (BuildContext context) => scaffold,
      detailBuilder: (BuildContext context) => _asideWidget,
      masterFlex: 3,
      detailFlex: 2,
    );
  }

  Widget _buildContent(BuildContext context, CourseDetailsModel model) {
    final bool mobileLayout = _mobileLayout(context);

    return CustomScrollView(slivers: [
      Toolbar(
        vsync: this,
        onBack: widget.onBack,
        title: model.course.name,
        actions: [
          BorderedImageButton(
            onTap: () => Nav.courses_edit(courseId: model.course.id),
            imageAsset: Images.ic_edit_pen_24,
            tooltip: S.of(context).common_edit,
          ),
          BorderedImageButton(
            onTap: () => _showDeleteConfirmation(context),
            imageAsset: Images.ic_trash_24,
            tooltip: S.of(context).common_delete,
          ),
        ],
      ),
      SliverList(
        delegate: SliverChildListDelegate([
          if (!_DetailsSection.isEmpty(model)) _buildDetailsSection(context, model),
          _buildEventsSection(context, model),
          _buildRecentGradesHeader(context),
          if (!mobileLayout || model.grades.isNotEmpty) _buildGradesList(context, model, !mobileLayout),
          if (mobileLayout) _buildAddGradeHorizontalButton(context, model),
          // TODO: show always
          if (kDebugMode) ...[
            _buildRecentFilesHeader(context),
            if (!mobileLayout || model.files.isNotEmpty) _buildFilesList(context, !mobileLayout),
            if (mobileLayout) _buildUploadFileHorizontalButton(context),
          ],
          Container(height: Paddings.of(context).vertical),
        ]),
      ),
    ]);
  }

  Widget _buildDetailsSection(BuildContext context, CourseDetailsModel model) {
    final Adaptive adaptive = Adaptive.of(context);
    final Paddings paddings = Paddings.of(context);

    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        padding: EdgeInsets.only(
          left: paddings.horizontal,
          right: paddings.horizontal,
          top: _getDetailsSectionTopPadding(adaptive) - Toolbar.bottomPadding,
        ),
        child: _mobileLayout(context) ? _MobileDetailsSection(model: model) : _DesktopDetailsSection(model: model),
      ),
    );
  }

  double _getDetailsSectionTopPadding(Adaptive adaptive) {
    if (adaptive.isMobile) return Dimens.grid_2x;
    if (adaptive.isTablet) return Dimens.grid_4x;
    return Dimens.grid_5x;
  }

  Widget _buildEventsSection(BuildContext context, CourseDetailsModel model) {
    return Padding(
      padding: EdgeInsets.only(top: _getEventsSectionTopPadding(context, model)),
      child: _mobileLayout(context)
          ? _MobileEventsSection(model: model)
          : _DesktopEventsSection(
              model: model,
              onViewExams: () => _onViewExams(model),
              onViewHomework: () => _onViewHomework(model),
              onViewReminders: () => _onViewReminders(model),
            ),
    );
  }

  void _onViewExams(CourseDetailsModel model) {
    final Widget widget = ExamsPage(
      courseId: model.course.id,
      fullscreen: false,
      onBack: (BuildContext context) {
        setState(() => _asideWidget = null);
      },
    );

    setState(() => _asideWidget = widget);
  }

  void _onViewHomework(CourseDetailsModel model) {
    final Widget widget = HomeworkPage(
      courseId: model.course.id,
      fullscreen: false,
      onBack: (BuildContext context) {
        setState(() => _asideWidget = null);
      },
    );

    setState(() => _asideWidget = widget);
  }

  void _onViewReminders(CourseDetailsModel model) {
    final Widget widget = RemindersPage(
      courseId: model.course.id,
      fullscreen: false,
      onBack: (BuildContext context) {
        setState(() => _asideWidget = null);
      },
    );

    setState(() => _asideWidget = widget);
  }

  double _getEventsSectionTopPadding(BuildContext context, CourseDetailsModel model) {
    final Adaptive adaptive = Adaptive.of(context);
    final double padding = adaptive.isMobile ? Dimens.grid_4x : Dimens.grid_6x;
    return padding - (_DetailsSection.isEmpty(model) ? 0 : Toolbar.bottomPadding);
  }

  Widget _buildRecentGradesHeader(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        left: paddings.horizontal,
        right: paddings.horizontal - _HeaderSection._innerPadding,
        top: _getHeaderSectionTopPadding(context),
      ),
      child: _HeaderSection(
        title: S.of(context).common_recentGrades,
        onViewMore: () {
          if (MasterDetailContainer.isSupported(context)) {
            final Widget page = GradePage(
              courseId: widget.courseId,
              fullscreen: false,
              onBack: (BuildContext context) {
                setState(() => _asideWidget = null);
              },
            );

            setState(() => _asideWidget = page);
          } else {
            Nav.grades_list(courseId: widget.courseId);
          }
        },
      ),
    );
  }

  Widget _buildRecentFilesHeader(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        left: paddings.horizontal,
        right: paddings.horizontal - _HeaderSection._innerPadding,
        top: _getHeaderSectionTopPadding(context),
      ),
      child: _HeaderSection(
        title: S.of(context).common_recentFiles,
        onViewMore: () {
          // TODO
        },
      ),
    );
  }

  double _getHeaderSectionTopPadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    return adaptive.isDesktop ? Dimens.grid_6x : Dimens.grid_5x;
  }

  Widget _buildAddGradeHorizontalButton(BuildContext context, CourseDetailsModel model) {
    return _buildHorizontalButton(
      context: context,
      text: S.of(context).common_addGrade,
      onPressed: () => Nav.grades_add(courseId: model.course.id),
    );
  }

  Widget _buildUploadFileHorizontalButton(BuildContext context) {
    return _buildHorizontalButton(
      context: context,
      text: S.of(context).common_uploadFile,
      onPressed: () {
        // TODO upload file
      },
    );
  }

  Widget _buildHorizontalButton({
    @required BuildContext context,
    @required String text,
    @required VoidCallback onPressed,
  }) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        left: paddings.horizontal,
        right: paddings.horizontal,
        top: Dimens.grid_2x,
      ),
      child: _HorizontalButton(
        text: text,
        onPressed: onPressed,
      ),
    );
  }

  Widget _buildGradesList(BuildContext context, CourseDetailsModel model, bool includeAddButton) {
    final int itemCount = model.grades.length + (includeAddButton ? 1 : 0);
    return _buildListView(
      context: context,
      itemCount: itemCount,
      itemBuilder: (BuildContext context, int position) {
        if (includeAddButton && position == 0) {
          return _AddButton(onTap: () => Nav.grades_add(courseId: model.course.id));
        }

        final GradeData data = model.grades[position - (includeAddButton ? 1 : 0)];
        return Padding(
          padding: EdgeInsets.only(
            left: includeAddButton ? Dimens.grid_2x : 0,
            right: includeAddButton || (position == itemCount - 1) ? 0 : Dimens.grid_2x,
          ),
          child: GradeCard(
            gradingSystem: model.gradingSystem,
            gradeCategory: data.category,
            grade: data.grade,
            onTap: () => Nav.grades_edit(gradeId: data.grade.id, courseId: data.grade.courseId),
            onLongPress: () => _showDeleteGradeConfirmation(context, data.grade),
          ),
        );
      },
    );
  }

  Widget _buildFilesList(BuildContext context, bool includeAddButton) {
    return _buildListView(
      context: context,
      itemCount: includeAddButton ? 1 : 0,
      itemBuilder: (BuildContext context, int position) {
        if (includeAddButton && position == 0) {
          return _AddButton(
            // TODO add file
            onTap: () {},
          );
        }
        throw ArgumentError('Cannot build a child at $position');
      },
    );
  }

  Widget _buildListView({
    @required BuildContext context,
    @required int itemCount,
    @required IndexedWidgetBuilder itemBuilder,
  }) {
    final Paddings paddings = Paddings.of(context);
    return SizedBox(
      height: _listItemHeight,
      child: ListView.builder(
        primary: false,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.only(
          top: Dimens.grid_2x,
          left: paddings.horizontal,
          right: paddings.vertical,
        ),
        itemCount: itemCount,
        itemBuilder: itemBuilder,
      ),
    );
  }

  void _showDeleteConfirmation(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_delete,
      message: S.of(context).courses_deleteDialog_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteCourse(),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }

  void _showDeleteGradeConfirmation(BuildContext context, Grade grade) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_delete,
      message: S.of(context).grades_deletedDialog_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteGrade(grade.id),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }
}

class _DetailsSection {
  static bool isEmpty(CourseDetailsModel model) {
    return model.teachers.isEmpty &&
        Strings.isBlank(model.course.room) &&
        model.course.creditHours == null &&
        model.gradesStats.gradesCount == 0 &&
        Strings.isBlank(model.course.note);
  }
}

class _MobileDetailsSection extends StatelessWidget {
  final CourseDetailsModel model;

  const _MobileDetailsSection({Key key, @required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BorderedContainer(
      color: Theme.of(context).colorScheme.surface,
      padding: EdgeInsets.all(Dimens.grid_2x).copyWith(top: 0),
      child: Column(children: [
        if (model.teachers.isNotEmpty)
          _buildRow(
            context: context,
            imageAsset24: Images.ic_person_24,
            semanticLabel: S.of(context).common_teachers,
            text: model.teachers.map((e) => e.name).join(' · '),
          ),
        if (Strings.isNotBlank(model.course.room))
          _buildRow(
            context: context,
            imageAsset24: Images.ic_location_24,
            semanticLabel: S.of(context).common_room,
            text: model.course.room,
          ),
        if (model.course.creditHours != null)
          _buildRow(
            context: context,
            imageAsset24: Images.ic_scale_24,
            semanticLabel: S.of(context).common_creditHours,
            text:
                NumberFormatter.format(model.course.creditHours) + ' ' + S.of(context).common_creditHours.toLowerCase(),
          ),
        if (model.gradesStats.gradesCount != 0)
          _buildRow(
            context: context,
            imageAsset24: Images.ic_percent_24,
            semanticLabel:
                S.of(context).common_gradeAverage + ': ' + model.gradingSystem.decorate(model.gradesStats.averageGrade),
            text: S.of(context).common_avg + ': ' + model.gradingSystem.decorate(model.gradesStats.averageGrade),
          ),
        if (Strings.isNotBlank(model.course.note))
          _buildRow(
            context: context,
            imageAsset24: Images.ic_description_24,
            semanticLabel: S.of(context).common_note,
            text: model.course.note,
          ),
      ]),
    );
  }

  Widget _buildRow({
    @required BuildContext context,
    @required String imageAsset24,
    @required String semanticLabel,
    @required String text,
  }) {
    return Padding(
      padding: EdgeInsets.only(top: Dimens.grid_2x),
      child: Row(children: [
        Image.asset(
          imageAsset24,
          width: 24,
          height: 24,
          color: Theme.of(context).colorScheme.primaryVariant,
          semanticLabel: semanticLabel,
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: Dimens.grid_2x),
            child: Text(text),
          ),
        ),
      ]),
    );
  }
}

class _DesktopDetailsSection extends StatelessWidget {
  final CourseDetailsModel model;

  const _DesktopDetailsSection({Key key, @required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: Dimens.grid_2x,
      runSpacing: Dimens.grid_2x,
      children: [
        if (model.teachers.isNotEmpty)
          _buildCard(
            context: context,
            imageAsset24: Images.ic_person_24,
            label: S.of(context).common_teachers,
            text: model.teachers.map((e) => e.name).join(' · '),
          ),
        if (Strings.isNotBlank(model.course.room))
          _buildCard(
            context: context,
            imageAsset24: Images.ic_location_24,
            label: S.of(context).common_room,
            text: model.course.room,
          ),
        if (model.course.creditHours != null)
          _buildCard(
            context: context,
            imageAsset24: Images.ic_scale_24,
            label: S.of(context).common_creditHours,
            text: NumberFormatter.format(model.course.creditHours),
          ),
        if (model.gradesStats.gradesCount != 0)
          _buildCard(
            context: context,
            imageAsset24: Images.ic_percent_24,
            label: S.of(context).common_gradeAverage,
            text: model.gradingSystem.decorate(model.gradesStats.averageGrade),
          ),
        if (Strings.isNotBlank(model.course.note))
          _buildCard(
            context: context,
            imageAsset24: Images.ic_description_24,
            text: model.course.note,
          ),
      ],
    );
  }

  Widget _buildCard({
    @required BuildContext context,
    @required String imageAsset24,
    String label,
    @required String text,
  }) {
    return IntrinsicWidth(
      child: ConstrainedBox(
        constraints: BoxConstraints(minHeight: 60),
        child: BorderedContainer(
          padding: EdgeInsets.all(Dimens.grid_1_5x),
          color: Theme.of(context).colorScheme.surface,
          child: Row(
            children: [
              Container(
                width: 36,
                height: 36,
                decoration: BoxDecoration(
                  color: Theme.of(context).dividerColor,
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: Image.asset(
                    imageAsset24,
                    width: 16,
                    height: 16,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: Dimens.grid_1_5x),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (Strings.isNotBlank(label))
                      Text(
                        label,
                        style: Theme.of(context).textTheme.caption2(context).copyWith(
                              color: Theme.of(context).colorScheme.primaryVariant,
                            ),
                      ),
                    Padding(
                      padding: EdgeInsets.only(top: Dimens.grid_0_5x),
                      child: Text(text, style: Theme.of(context).textTheme.caption),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _MobileEventsSection extends StatefulWidget {
  final CourseDetailsModel model;

  const _MobileEventsSection({Key key, @required this.model}) : super(key: key);

  @override
  _MobileEventsSectionState createState() => _MobileEventsSectionState();
}

class _MobileEventsSectionState extends State<_MobileEventsSection> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabController.addListener(() {
      setState(() {
        // rebuild
      });
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<Exam> activeExams = widget.model.exams.where((e) => !e.isArchived).toList();
    final List<Homework> activeHomework = widget.model.homework.where((e) => !e.isArchived).toList();
    final List<Reminder> activeReminders = widget.model.reminders.where((e) => !e.isArchived).toList();

    return Column(children: [
      TabBar(
        controller: _tabController,
        isScrollable: true,
        tabs: [
          _buildTab(
            context: context,
            text: S.of(context).common_exams,
            label: _EventsSection._buildTabLabel(context, activeExams.length, widget.model.exams.length),
            isActive: _tabController.index == 0,
          ),
          _buildTab(
            context: context,
            text: S.of(context).common_homework,
            label: _EventsSection._buildTabLabel(context, activeHomework.length, widget.model.homework.length),
            isActive: _tabController.index == 1,
          ),
          _buildTab(
            context: context,
            text: S.of(context).common_reminders,
            label: _EventsSection._buildTabLabel(context, activeReminders.length, widget.model.reminders.length),
            isActive: _tabController.index == 2,
          ),
        ],
      ),
      if (_tabController.index == 0)
        _ExamsList(
          exams: activeExams,
          grades: widget.model.grades,
          gradingSystem: widget.model.gradingSystem,
          onAddExam: () => Nav.exams_add(courseId: widget.model.course.id),
          onViewAll: () => Nav.exams_list(courseId: widget.model.course.id),
          onViewExam: (Exam exam) => Nav.exams_details(examId: exam.id),
        ),
      if (_tabController.index == 1)
        _HomeworkList(
          homework: activeHomework,
          grades: widget.model.grades,
          gradingSystem: widget.model.gradingSystem,
          onAddHomework: () => Nav.homework_add(courseId: widget.model.course.id),
          onViewAll: () => Nav.homework_list(courseId: widget.model.course.id),
          onViewHomework: (Homework homework) => Nav.homework_details(homeworkId: homework.id),
        ),
      if (_tabController.index == 2)
        _RemindersList(
          reminders: activeReminders,
          onAddReminder: () => Nav.reminders_add(courseId: widget.model.course.id),
          onViewAll: () => Nav.reminders_list(courseId: widget.model.course.id),
          onViewReminder: (Reminder reminder) => Nav.reminders_details(reminderId: reminder.id),
        ),
    ]);
  }

  Tab _buildTab({
    @required BuildContext context,
    @required String text,
    @required String label,
    @required bool isActive,
  }) {
    final Color color = isActive ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.primaryVariant;
    return Tab(
      child: Column(children: [
        Text(text, style: Theme.of(context).textTheme.headline6.copyWith(color: color)),
        Padding(
          padding: EdgeInsets.only(top: 2),
          child: Text(label, style: Theme.of(context).textTheme.caption2(context).copyWith(color: color)),
        ),
      ]),
    );
  }
}

class _DesktopEventsSection extends StatelessWidget {
  final CourseDetailsModel model;
  final VoidCallback onViewExams;
  final VoidCallback onViewHomework;
  final VoidCallback onViewReminders;

  const _DesktopEventsSection({
    Key key,
    @required this.model,
    @required this.onViewExams,
    @required this.onViewHomework,
    @required this.onViewReminders,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    final List<Exam> activeExams = model.exams.where((e) => !e.isArchived).toList();
    final List<Homework> activeHomework = model.homework.where((e) => !e.isArchived).toList();
    final List<Reminder> activeReminders = model.reminders.where((e) => !e.isArchived).toList();

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: paddings.horizontal - _EventsList._getHorizontalPadding(context),
      ),
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 2),
                  child: Text(S.of(context).common_exams, style: Theme.of(context).textTheme.headline6),
                ),
                Text(
                  _EventsSection._buildTabLabel(context, activeExams.length, model.exams.length),
                  style: Theme.of(context).textTheme.caption2(context),
                ),
                _ExamsList(
                  exams: activeExams,
                  grades: model.grades,
                  gradingSystem: model.gradingSystem,
                  onAddExam: () => Nav.exams_add(courseId: model.course.id),
                  onViewAll: onViewExams,
                  onViewExam: (Exam exam) => Nav.exams_details(examId: exam.id),
                ),
              ]),
            ),
            VerticalDivider(),
            Expanded(
              child: Column(children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 2),
                  child: Text(S.of(context).common_homework, style: Theme.of(context).textTheme.headline6),
                ),
                Text(
                  _EventsSection._buildTabLabel(context, activeHomework.length, model.homework.length),
                  style: Theme.of(context).textTheme.caption2(context),
                ),
                _HomeworkList(
                  homework: activeHomework,
                  grades: model.grades,
                  gradingSystem: model.gradingSystem,
                  onAddHomework: () => Nav.homework_add(courseId: model.course.id),
                  onViewAll: onViewHomework,
                  onViewHomework: (Homework homework) => Nav.homework_details(homeworkId: homework.id),
                ),
              ]),
            ),
            VerticalDivider(),
            Expanded(
              child: Column(children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 2),
                  child: Text(S.of(context).common_reminders, style: Theme.of(context).textTheme.headline6),
                ),
                Text(
                  _EventsSection._buildTabLabel(context, activeReminders.length, model.reminders.length),
                  style: Theme.of(context).textTheme.caption2(context),
                ),
                _RemindersList(
                  reminders: activeReminders,
                  onAddReminder: () => Nav.reminders_add(courseId: model.course.id),
                  onViewAll: onViewReminders,
                  onViewReminder: (Reminder reminder) => Nav.reminders_details(reminderId: reminder.id),
                ),
              ]),
            ),
          ],
        ),
      ),
    );
  }
}

class _EventsSection {
  static String _buildTabLabel(BuildContext context, int activeCount, int totalCount) {
    return activeCount.toString() + '/' + totalCount.toString() + ' ' + S.of(context).common_active.toLowerCase();
  }
}

class _ExamsList extends StatelessWidget {
  final List<Exam> exams;
  final List<GradeData> grades;
  final GradingSystem gradingSystem;
  final VoidCallback onAddExam;
  final VoidCallback onViewAll;
  final ValueChanged<Exam> onViewExam;

  const _ExamsList({
    Key key,
    @required this.exams,
    @required this.grades,
    @required this.gradingSystem,
    @required this.onAddExam,
    @required this.onViewAll,
    @required this.onViewExam,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _EventsList(
      events: [
        for (Exam exam in exams)
          BorderedListItem(
            data: _examAsData(context, exam),
            onTap: () => onViewExam(exam),
          ),
      ],
      addEventLabel: S.of(context).common_addExam,
      onAddEvent: onAddExam,
      onViewEvents: onViewAll,
    );
  }

  BorderedListItemData _examAsData(BuildContext context, Exam exam) {
    final GradeData data = grades.firstWhereOrNull((e) => e.grade.id == exam.gradeId);
    return BorderedListItemData(
      title: exam.headline,
      label: DateFormatter.formatRelativeOrFullDate(context, exam.dateTime),
      aside: data != null ? gradingSystem.decorate(data.grade.value) : null,
    );
  }
}

class _HomeworkList extends StatelessWidget {
  final List<Homework> homework;
  final List<GradeData> grades;
  final GradingSystem gradingSystem;
  final VoidCallback onAddHomework;
  final VoidCallback onViewAll;
  final ValueChanged<Homework> onViewHomework;

  const _HomeworkList({
    Key key,
    @required this.homework,
    @required this.grades,
    @required this.gradingSystem,
    @required this.onAddHomework,
    @required this.onViewAll,
    @required this.onViewHomework,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _EventsList(
      events: [
        for (Homework homework in homework)
          BorderedListItem(
            data: _homeworkAsData(context, homework),
            onTap: () => onViewHomework(homework),
          ),
      ],
      addEventLabel: S.of(context).common_addHomework,
      onAddEvent: onAddHomework,
      onViewEvents: onViewAll,
    );
  }

  BorderedListItemData _homeworkAsData(BuildContext context, Homework homework) {
    final GradeData data = grades.firstWhereOrNull((e) => e.grade.id == homework.gradeId);
    return BorderedListItemData(
      title: homework.headline,
      label: DateFormatter.formatRelativeOrFullDate(context, homework.dateTime),
      aside: data != null ? gradingSystem.decorate(data.grade.value) : null,
    );
  }
}

class _RemindersList extends StatelessWidget {
  final List<Reminder> reminders;
  final VoidCallback onAddReminder;
  final VoidCallback onViewAll;
  final ValueChanged<Reminder> onViewReminder;

  const _RemindersList({
    Key key,
    @required this.reminders,
    @required this.onAddReminder,
    @required this.onViewAll,
    @required this.onViewReminder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _EventsList(
      events: [
        for (Reminder reminder in reminders)
          BorderedListItem(
            data: _reminderAsData(context, reminder),
            onTap: () => onViewReminder(reminder),
          ),
      ],
      addEventLabel: S.of(context).common_addReminder,
      onAddEvent: onAddReminder,
      onViewEvents: onViewAll,
    );
  }

  BorderedListItemData _reminderAsData(BuildContext context, Reminder reminder) {
    return BorderedListItemData(
      title: reminder.headline,
      label: DateFormatter.formatRelativeOrFullDate(context, reminder.dateTime),
    );
  }
}

class _EventsList extends StatelessWidget {
  final List<BorderedListItem> events;
  final String addEventLabel;
  final VoidCallback onAddEvent;
  final VoidCallback onViewEvents;

  const _EventsList({
    Key key,
    @required this.events,
    @required this.addEventLabel,
    @required this.onAddEvent,
    @required this.onViewEvents,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: _getHorizontalPadding(context)),
      child: Column(
        children: [
          if (!_mobileLayout(context)) _buildButtons(context),
          if (events.isEmpty) _EventsEmptyBox(),
          for (BorderedListItem item in events)
            Padding(
              padding: EdgeInsets.only(top: Dimens.grid_2x),
              child: item,
            ),
          if (_mobileLayout(context)) _buildButtons(context),
        ],
      ),
    );
  }

  Widget _buildButtons(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: _getButtonTopPadding(context)),
      child: Row(children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(right: Dimens.grid_1x),
            child: SmallBorderedButton(
              child: SmallButtonText(
                addEventLabel,
                color: Theme.of(context).colorScheme.secondary,
              ),
              onPressed: onAddEvent,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: Dimens.grid_1x),
            child: SmallBorderedButton(
              child: SmallButtonText(
                S.of(context).common_viewAll,
                color: Theme.of(context).colorScheme.primaryVariant,
              ),
              onPressed: onViewEvents,
            ),
          ),
        ),
      ]),
    );
  }

  static double _getHorizontalPadding(BuildContext context) {
    if (_mobileLayout(context)) {
      return Paddings.of(context).horizontal;
    }
    return Dimens.grid_2x;
  }

  static double _getButtonTopPadding(BuildContext context) {
    if (_mobileLayout(context)) return Dimens.grid_2x;

    return Dimens.grid_4x;
  }
}

class _EventsEmptyBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(
          top: Dimens.grid_4x,
          bottom: Dimens.grid_3x,
          left: Dimens.grid_3x,
          right: Dimens.grid_3x,
        ),
        child: Column(children: [
          Image.asset(Images.illustration_empty_box_128, width: 64, height: 64),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_1_5x),
            child: Text(
              S.of(context).common_noActiveEvents,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline6.copyWith(
                    color: Theme.of(context).colorScheme.primaryVariant,
                  ),
            ),
          ),
        ]),
      ),
    );
  }
}

class _HeaderSection extends StatelessWidget {
  static const double _innerPadding = Dimens.grid_1x;

  final String title;
  final GestureTapCallback onViewMore;

  const _HeaderSection({
    Key key,
    @required this.title,
    @required this.onViewMore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(child: Text(title, style: Theme.of(context).textTheme.headline6)),
      Padding(
        padding: EdgeInsets.only(left: Dimens.grid_2x),
        child: InkWell(
          onTap: onViewMore,
          child: Padding(
            padding: EdgeInsets.all(_innerPadding),
            child: SmallButtonText(
              S.of(context).common_viewMore,
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
          ),
        ),
      ),
    ]);
  }
}

class _HorizontalButton extends StatelessWidget {
  final String text;
  final GestureTapCallback onPressed;

  const _HorizontalButton({
    Key key,
    @required this.text,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      child: ButtonText(text, color: Theme.of(context).colorScheme.secondary),
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Theme.of(context).dividerColor),
        borderRadius: BorderRadius.all(Radius.circular(Dimens.grid_0_5x)),
      ),
    );
  }
}

class _AddButton extends StatelessWidget {
  final GestureTapCallback onTap;

  const _AddButton({Key key, @required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BorderedContainer(
      width: 72,
      onTap: onTap,
      child: Center(
        child: Image.asset(
          Images.ic_add_32,
          width: 32,
          height: 32,
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
    );
  }
}

bool _mobileLayout(BuildContext context) {
  final Adaptive adaptive = Adaptive.of(context);
  return adaptive.isMobile || (adaptive.isTablet && MediaQuery.of(context).orientation == Orientation.portrait);
}
