import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/exam/exam_listener.dart';
import 'package:school_pen/domain/exam/exam_manager.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/grade/model/course_grades_stats.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/homework/homework_listener.dart';
import 'package:school_pen/domain/homework/homework_manager.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/reminder/reminder_listener.dart';
import 'package:school_pen/domain/reminder/reminder_manager.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/teacher_listener.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';

class CourseDetailsModel extends Equatable {
  final bool progressOverlay;
  final Course course;
  final List<Teacher> teachers;
  final GradingSystem gradingSystem;
  final List<GradeData> grades;
  final CourseGradesStats gradesStats;
  final List<Exam> exams;
  final List<Homework> homework;
  final List<Reminder> reminders;
  final List<Object> files;

  const CourseDetailsModel({
    @required this.progressOverlay,
    @required this.course,
    @required this.teachers,
    @required this.gradingSystem,
    @required this.grades,
    @required this.gradesStats,
    @required this.exams,
    @required this.homework,
    @required this.reminders,
    @required this.files,
  });

  @override
  List<Object> get props =>
      [progressOverlay, course, teachers, gradingSystem, grades, gradesStats, exams, homework, reminders, files];
}

class GradeData extends Equatable {
  final Grade grade;
  final GradeCategory category;

  const GradeData(this.grade, this.category);

  @override
  List<Object> get props => [grade, category];
}

enum CourseDetailsSignal {
  notifyCourseDeleted,
  notifyGradeDeleted,
  pop,
}

class CourseDetailsViewModel extends ViewModel<CourseDetailsModel, CourseDetailsSignal>
    implements
        SemesterListener,
        CourseListener,
        TeacherListener,
        GradeListener,
        ExamListener,
        HomeworkListener,
        ReminderListener {
  static const Logger _logger = Logger('CourseDetailsViewModel');

  final SemesterManager _semesterManager;
  final CourseManager _courseManager;
  final TeacherManager _teacherManager;
  final GradeManager _gradeManager;
  final ExamManager _examManager;
  final HomeworkManager _homeworkManager;
  final ReminderManager _reminderManager;
  final String _courseId;

  bool _progressOverlay = false;

  CourseDetailsViewModel(this._semesterManager, this._courseManager, this._teacherManager, this._gradeManager,
      this._examManager, this._homeworkManager, this._reminderManager, this._courseId);

  @override
  void onStart() async {
    super.onStart();
    _semesterManager.addSemesterListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _teacherManager.addTeacherListener(this, notifyOnAttach: false);
    _gradeManager.addGradeListener(this, notifyOnAttach: false);
    _examManager.addExamListener(this, notifyOnAttach: false);
    _homeworkManager.addHomeworkListener(this, notifyOnAttach: false);
    _reminderManager.addReminderListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _semesterManager.removeSemesterListener(this);
    _courseManager.removeCourseListener(this);
    _teacherManager.removeTeacherListener(this);
    _gradeManager.removeGradeListener(this);
    _examManager.removeExamListener(this);
    _homeworkManager.removeHomeworkListener(this);
    _reminderManager.removeReminderListener(this);
    super.onStop();
  }

  @override
  void onSemestersChanged(SemesterManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onTeachersChanged(TeacherManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onGradesChanged(GradeManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onExamsChanged(ExamManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onHomeworkChanged(HomeworkManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onRemindersChanged(ReminderManager manager) async {
    await _tryEmitModel();
  }

  void onDeleteCourse() async {
    await _tryWithProgressOverlay(() async {
      await _courseManager.delete(_courseId);
      emitSignal(CourseDetailsSignal.notifyCourseDeleted);
      emitSingleSignal(CourseDetailsSignal.pop);
    });
  }

  void onDeleteGrade(String id) async {
    await _tryWithProgressOverlay(() async {
      await _gradeManager.delete(id);
      emitSignal(CourseDetailsSignal.notifyGradeDeleted);
    });
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      final Course course = await _courseManager.getById(_courseId);
      if (course == null) return;

      emitModel(await _fetchModel(course));
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<CourseDetailsModel> _fetchModel(Course course) async {
    final Semester semester = await _semesterManager.getActive();
    final Future<List<Teacher>> teachers = _fetchTeachers(course);
    final Future<GradingSystem> gradingSystem = _gradeManager.getGradingSystem();
    final Future<List<GradeData>> grades = _fetchGrades(semester.id);
    final Future<CourseGradesStats> gradesStats =
        _gradeManager.getStatsById(courseId: _courseId, semesterId: semester.id);
    final Future<List<Exam>> exams = _examManager.getAll(semesterId: semester.id, courseId: _courseId);
    final Future<List<Homework>> homework = _homeworkManager.getAll(semesterId: semester.id, courseId: _courseId);
    final Future<List<Reminder>> reminders = _reminderManager.getAll(semesterId: semester.id, courseId: _courseId);

    return CourseDetailsModel(
      progressOverlay: _progressOverlay,
      course: course,
      teachers: await teachers,
      gradingSystem: await gradingSystem,
      grades: await grades,
      gradesStats: await gradesStats,
      exams: await exams,
      homework: await homework,
      reminders: await reminders,
      files: [],
    );
  }

  Future<List<Teacher>> _fetchTeachers(Course course) async {
    final List<Teacher> teachers = await _teacherManager.getAll(schoolYearId: course.schoolYearId);
    return teachers.where((e) => course.teachersIds.contains(e.id)).toList();
  }

  Future<List<GradeData>> _fetchGrades(String semesterId) async {
    final List<GradeCategory> categories = await _gradeManager.getCategories();
    final List<Grade> grades = await _gradeManager.getAll(courseId: _courseId, semesterId: semesterId);
    grades.sort();

    final List<GradeData> result = [];
    for (Grade grade in grades) {
      final GradeCategory category = categories.firstWhereOrNull((e) => e.id == grade.categoryId);
      result.add(GradeData(grade, category));
    }

    return result;
  }
}
