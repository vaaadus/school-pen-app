import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/typography.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/common/formatter/grades_formatter.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/course/details/grade_card.dart';
import 'package:school_pen/app/pages/course/grades/grade_view_model.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/radio_button_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/widgets/bordered_list_item.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows to view/edit/create grades.
class GradePage extends LifecycleWidget {
  const GradePage({
    Key key,
    @required this.courseId,
    this.fullscreen = true,
    this.onBack = Navigator.pop,
  }) : super(key: key);

  final String courseId;
  final bool fullscreen;
  final OnBackCallback onBack;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _GradePageState();
}

class _GradePageState extends LifecycleWidgetState<GradePage>
    with ViewModelLifecycle<GradeViewModel, GradePage>, ProgressOverlayMixin, SingleTickerProviderStateMixin {
  @override
  GradeViewModel provideViewModel() =>
      GradeViewModel(Injection.get(), Injection.get(), Injection.get(), Injection.get(), widget.courseId);

  @override
  Widget build(BuildContext context) {
    Widget result = Scaffold(
      body: SafeArea(
        child: ViewModelBuilder(
          viewModel: viewModel,
          builder: (BuildContext context, GradeModel model) {
            updateProgressOverlay(model.progressOverlay);
            return _buildContent(context, model);
          },
          onSignal: (BuildContext context, GradeSignal signal) {
            if (signal == GradeSignal.notifyGradeDeleted) {
              showSuccessToast(message: S.of(context).confirmation_deleted);
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        label: ButtonText(S.of(context).common_addGrade),
        onPressed: () => Nav.grades_add(courseId: widget.courseId),
      ),
    );

    if (widget.fullscreen) {
      result = SystemUiOverlayAnnotation(child: result);
    }

    return result;
  }

  Widget _buildContent(BuildContext context, GradeModel model) {
    final Paddings paddings = Paddings.of(context);

    return CustomScrollView(slivers: [
      Toolbar(
        pinned: !widget.fullscreen || model.showEmptyState,
        vsync: this,
        onBack: widget.onBack,
        navType: widget.fullscreen ? NavButtonType.back : NavButtonType.close,
        title: (widget.fullscreen ? '${model.course.name} / ' : '') + S.of(context).common_grades,
        actions: [
          if (!model.showEmptyState)
            BorderedImageButton(
              onTap: () => _showSortOptions(context, model),
              imageAsset: Images.ic_sort_24,
              tooltip: S.of(context).common_sort,
            ),
        ],
      ),
      if (model.showEmptyState)
        SliverFillRemaining(
          child: _buildEmptyState(context),
        ),
      if (!model.showEmptyState)
        SliverPadding(
          padding: EdgeInsets.only(bottom: Dimens.grid_10x),
          sliver: SliverList(
            delegate: SliverChildBuilderDelegate((BuildContext context, int position) {
              final GradeData data = model.grades[position];
              return Padding(
                padding: EdgeInsets.only(
                  top: position == 0 ? (paddings.vertical - Toolbar.bottomPadding) : Dimens.grid_2x,
                  left: paddings.horizontal,
                  right: paddings.horizontal,
                ),
                child: BorderedListItem(
                  data: _gradeAsListData(context, model, data),
                  onTap: () => Nav.grades_edit(gradeId: data.grade.id, courseId: data.grade.courseId),
                  onLongPress: () => _showDeleteGradeConfirmation(context, data.grade),
                ),
              );
            }, childCount: model.grades.length),
          ),
        ),
    ]);
  }

  Widget _buildEmptyState(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(Dimens.grid_3x),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(Images.illustration_empty_box_128, width: 128, height: 128),
            Padding(
              padding: EdgeInsets.only(top: Dimens.grid_1_5x),
              child: Text(
                S.of(context).common_noGradesAdded,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline6.copyWith(
                      color: Theme.of(context).colorScheme.primaryVariant,
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  BorderedListItemData _gradeAsListData(BuildContext context, GradeModel model, GradeData data) {
    return BorderedListItemData(
      thumbnail: GradeIcon(gradingSystem: model.gradingSystem, grade: data.grade),
      title: GradesFormatter.formatName(context, data.grade.name),
      titleColor: Strings.isNotBlank(data.grade.name)
          ? Theme.of(context).colorScheme.primary
          : Theme.of(context).colorScheme.primaryVariant,
      label: _formatLabel(context, data),
      aside: GradesFormatter.formatNamedWeight(context, data.grade.weight),
      asideTextStyle: Theme.of(context).textTheme.caption2(context).copyWith(
            color: Theme.of(context).colorScheme.primaryVariant,
          ),
      asideAlignment: CrossAxisAlignment.start,
    );
  }

  String _formatLabel(BuildContext context, GradeData data) {
    String text = DateFormatter.formatShortRecentDate(data.grade.dateTime);
    if (data.category != null) {
      text = GradesFormatter.formatGradeCategory(context, data.category) + ' · ' + text;
    }
    return text;
  }

  void _showSortOptions(BuildContext context, GradeModel model) async {
    final GradeSortOrder sortOrder = await showRadioButtonDialog(
      context: context,
      routeSettings: RouteSettings(name: R.grades_sort),
      title: S.of(context).common_sortBy,
      itemBuilder: (BuildContext context) => [
        RadioButtonDialogItem(
          text: S.of(context).common_sortBy_latest,
          groupValue: model.sortOrder,
          value: GradeSortOrder.latest,
        ),
        RadioButtonDialogItem(
          text: S.of(context).common_sortBy_oldest,
          groupValue: model.sortOrder,
          value: GradeSortOrder.oldest,
        ),
        RadioButtonDialogItem(
          text: S.of(context).common_sortBy_highest,
          groupValue: model.sortOrder,
          value: GradeSortOrder.highest,
        ),
        RadioButtonDialogItem(
          text: S.of(context).common_sortBy_lowest,
          groupValue: model.sortOrder,
          value: GradeSortOrder.lowest,
        ),
      ],
    );

    if (sortOrder != null) {
      viewModel.onSelectSortOrder(sortOrder);
    }
  }

  void _showDeleteGradeConfirmation(BuildContext context, Grade grade) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_delete,
      message: S.of(context).grades_deletedDialog_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteGrade(grade.id),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }
}
