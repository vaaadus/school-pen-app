import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/formatter/grades_formatter.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/exception/number_out_of_range_exception.dart';
import 'package:school_pen/domain/common/exception/value_out_of_constraints_exception.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/math.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/grade/model/add_grade.dart';
import 'package:school_pen/domain/grade/model/add_grade_category.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';

class GradeEditModel extends Equatable {
  final bool progressOverlay;
  final bool discardChangesPopup;
  final bool autofocusName;
  final GradingSystem gradingSystem;
  final String name;
  final String value;
  final Exception valueError;
  final String weight;
  final Exception weightError;
  final DateTime dateTime;
  final GradeCategory selectedCategory;
  final List<GradeCategory> availableCategories;
  final bool showCourse;
  final Course selectedCourse;
  final List<Course> availableCourses;
  final Exception courseError;

  const GradeEditModel({
    @required this.progressOverlay,
    @required this.discardChangesPopup,
    @required this.autofocusName,
    @required this.gradingSystem,
    this.name,
    this.value,
    this.valueError,
    this.weight,
    this.weightError,
    this.dateTime,
    this.selectedCategory,
    @required this.availableCategories,
    @required this.showCourse,
    this.selectedCourse,
    @required this.availableCourses,
    this.courseError,
  });

  @override
  List<Object> get props => [
        progressOverlay,
        discardChangesPopup,
        autofocusName,
        gradingSystem,
        name,
        value,
        valueError,
        weight,
        weightError,
        dateTime,
        selectedCategory,
        availableCategories,
        showCourse,
        selectedCourse,
        availableCourses,
        courseError,
      ];
}

class GradeEditSignal with EquatableMixin {
  static const GradeEditSignal notifyGradeSaved = GradeEditSignal('notifyGradeSaved');

  final String tag;

  const GradeEditSignal(this.tag);

  @override
  List<Object> get props => [tag];
}

class PopGradeEditSignal extends GradeEditSignal {
  final String gradeId;

  const PopGradeEditSignal(this.gradeId) : super('pop');

  @override
  List<Object> get props => [...super.props, gradeId];
}

class GradeEditViewModel extends ViewModel<GradeEditModel, GradeEditSignal> implements GradeListener, CourseListener {
  static const Logger _logger = Logger('GradeEditViewModel');

  final SemesterManager _semesterManager;
  final GradeManager _gradeManager;
  final CourseManager _courseManager;
  final String _gradeId;
  final String _preselectedCourseId;

  bool _progressOverlay = false;
  bool _dataInitialized = false;
  AddGradeInput _originalGrade;
  AddGradeInput _editedGrade;
  Exception _valueError;
  Exception _weightError;
  Exception _courseError;

  GradeEditViewModel(
    this._semesterManager,
    this._gradeManager,
    this._courseManager,
    this._gradeId,
    this._preselectedCourseId,
  );

  @override
  void onStart() async {
    super.onStart();
    _gradeManager.addGradeListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _gradeManager.removeGradeListener(this);
    _courseManager.removeCourseListener(this);
    super.onStop();
  }

  @override
  void onGradesChanged(GradeManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  void onSaveGrade() async {
    await _tryWithProgressOverlay(() async {
      if (!await _validate()) return;

      final AddGrade grade = await _buildGradeWithDefaults();
      String result;
      if (_gradeId != null) {
        await _gradeManager.update(_gradeId, grade);
        result = _gradeId;
      } else {
        result = await _gradeManager.create(grade);
      }
      emitSignal(GradeEditSignal.notifyGradeSaved);
      emitSingleSignal(PopGradeEditSignal(result));
    });
  }

  Future<void> onEditName(String text) async {
    _editedGrade = _editedGrade.copyWith(name: Optional(text));

    await _tryEmitModel();
  }

  void onEditGrade(String value) async {
    _editedGrade = _editedGrade.copyWith(value: Optional(value));

    await _tryEmitModel();
  }

  void onEditCourse(String courseId) async {
    _editedGrade = _editedGrade.copyWith(courseId: Optional(courseId));

    await _tryEmitModel();
  }

  void onEditDate(DateTime date) async {
    _editedGrade = _editedGrade.copyWith(dateTime: Optional(date));

    await _tryEmitModel();
  }

  void onAddCategory(AddGradeCategory category) async {
    try {
      final String categoryId = await _gradeManager.createCategory(category);
      _editedGrade = _editedGrade.copyWith(categoryId: Optional(categoryId));
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    await _tryEmitModel();
  }

  void onEditCategory(String categoryId) async {
    _editedGrade = _editedGrade.copyWith(categoryId: Optional(categoryId));

    await _tryEmitModel();
  }

  void onEditWeight(String weight) async {
    _editedGrade = _editedGrade.copyWith(weight: Optional(weight));

    await _tryEmitModel();
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<GradeEditModel> get _model async {
    if (!_dataInitialized) await _initializeData();

    final Semester semester = await _semesterManager.getById(_editedGrade.semesterId);
    return GradeEditModel(
      progressOverlay: _progressOverlay,
      discardChangesPopup: _originalGrade == null || _originalGrade != _editedGrade,
      autofocusName: _gradeId == null,
      gradingSystem: await _gradeManager.getGradingSystem(),
      name: _editedGrade.name,
      value: _editedGrade.value,
      valueError: _valueError,
      weight: _editedGrade.weight,
      weightError: _weightError,
      dateTime: _editedGrade.dateTime,
      selectedCategory: await _gradeManager.getCategoryById(_editedGrade.categoryId),
      availableCategories: await _gradeManager.getCategories(),
      showCourse: _preselectedCourseId == null,
      selectedCourse: await _courseManager.getById(_editedGrade.courseId),
      availableCourses: await _courseManager.getAll(semester.schoolYearId),
      courseError: _courseError,
    );
  }

  Future<bool> _validate() async {
    assert(_dataInitialized);

    _courseError = null;
    _valueError = null;
    _weightError = null;

    final GradingSystem gradingSystem = await _gradeManager.getGradingSystem();
    final double weight = tryParseDouble(_editedGrade.weight);

    if (gradingSystem.convert(_editedGrade.value) == null) {
      if (gradingSystem.type == GradingSystemType.alphabetic) {
        _valueError = ValueOutOfConstraintsException(allowedValues: gradingSystem.allowedValues());
      } else {
        _valueError = NumberOutOfRangeException(min: gradingSystem.minValue, max: gradingSystem.maxValue);
      }
      return false;
    } else if (weight == null && Strings.isNotBlank(_editedGrade.weight)) {
      _weightError = NumberOutOfRangeException(min: Grade.minWeight, max: Grade.maxWeight);
      return false;
    } else if (weight != null && (weight < Grade.minWeight || weight > Grade.maxWeight)) {
      _weightError = NumberOutOfRangeException(min: Grade.minWeight, max: Grade.maxWeight);
      return false;
    } else if (_editedGrade.courseId == null) {
      _courseError = FieldMustNotBeEmptyException();
      return false;
    }

    return true;
  }

  Future<AddGrade> _buildGradeWithDefaults() async {
    assert(_dataInitialized);

    final GradingSystem gradingSystem = await _gradeManager.getGradingSystem();
    return AddGrade(
      semesterId: _editedGrade.semesterId ?? ((await _semesterManager.getActive()).id),
      courseId: _editedGrade.courseId,
      categoryId: _editedGrade.categoryId,
      name: _editedGrade.name ?? '',
      value: gradingSystem.convert(_editedGrade.value),
      weight: tryParseDouble(_editedGrade.weight)?.abs() ?? 1.0,
      dateTime: _editedGrade.dateTime ?? DateTime.now(),
      updatedAt: DateTime.now(),
    );
  }

  Future<void> _initializeData() async {
    final GradingSystem gradingSystem = await _gradeManager.getGradingSystem();
    final AddGrade grade = await _obtainGrade();

    if (grade != null) {
      _originalGrade = AddGradeInput(
        semesterId: grade.semesterId,
        courseId: grade.courseId,
        categoryId: grade.categoryId,
        name: grade.name,
        value: gradingSystem.decorate(grade.value),
        weight: GradesFormatter.formatWeight(grade.weight),
        dateTime: grade.dateTime,
      );

      _editedGrade = _originalGrade;
    } else {
      final Semester semester = await _semesterManager.getActive();
      _editedGrade = AddGradeInput(
        semesterId: semester.id,
        courseId: _preselectedCourseId,
        categoryId: null,
        name: '',
        value: '',
        weight: '',
        dateTime: null,
      );
    }

    _dataInitialized = true;
  }

  Future<AddGrade> _obtainGrade() async {
    if (_gradeId != null) {
      final Grade grade = await _gradeManager.getById(_gradeId);
      return grade?.toAddGrade();
    }

    return null;
  }
}

class AddGradeInput extends Equatable {
  final String semesterId;
  final String courseId;
  final String categoryId;
  final String name;
  final String value;
  final String weight;
  final DateTime dateTime;

  const AddGradeInput({
    @required this.semesterId,
    @required this.courseId,
    @required this.categoryId,
    @required this.name,
    @required this.value,
    @required this.weight,
    @required this.dateTime,
  });

  AddGradeInput copyWith({
    Optional<String> semesterId,
    Optional<String> courseId,
    Optional<String> categoryId,
    Optional<String> name,
    Optional<String> value,
    Optional<String> weight,
    Optional<DateTime> dateTime,
  }) {
    return AddGradeInput(
      semesterId: Optional.unwrapOrElse(semesterId, this.semesterId),
      courseId: Optional.unwrapOrElse(courseId, this.courseId),
      categoryId: Optional.unwrapOrElse(categoryId, this.categoryId),
      name: Optional.unwrapOrElse(name, this.name),
      value: Optional.unwrapOrElse(value, this.value),
      weight: Optional.unwrapOrElse(weight, this.weight),
      dateTime: Optional.unwrapOrElse(dateTime, this.dateTime),
    );
  }

  @override
  List<Object> get props => [semesterId, courseId, categoryId, name, value, weight, dateTime];
}
