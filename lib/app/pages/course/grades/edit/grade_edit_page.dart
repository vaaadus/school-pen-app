import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/common/formatter/grades_formatter.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/course/grades/edit/grade_edit_view_model.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/pages/picker/text_field_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/save_or_discard_footer.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/grade/model/add_grade_category.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/generated/l10n.dart';

/// Callback called when grade edit page pops back.
/// The gradeId is the added or updated grade or null.
typedef OnGradeEditPop = void Function(BuildContext context, String gradeId);

void _pop(BuildContext context, String gradeId) => Navigator.of(context).pop(gradeId);

/// Offers the user a possibility to edit a grade (if [gradeId] not null)
/// or create a new one if grade is null.
class GradeEditPage extends LifecycleWidget {
  final String gradeId;
  final String courseId;
  final OnGradeEditPop onBack;

  const GradeEditPage({
    Key key,
    this.gradeId,
    this.courseId,
    this.onBack = _pop,
  }) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _GradeEditPageState();
}

class _GradeEditPageState extends LifecycleWidgetState<GradeEditPage>
    with ViewModelLifecycle<GradeEditViewModel, GradeEditPage>, ProgressOverlayMixin, SingleTickerProviderStateMixin {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _valueController = TextEditingController();
  final TextEditingController _weightController = TextEditingController();

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _valueFocus = FocusNode();
  final FocusNode _weightFocus = FocusNode();

  @override
  GradeEditViewModel provideViewModel() {
    return GradeEditViewModel(Injection.get(), Injection.get(), Injection.get(), widget.gradeId, widget.courseId);
  }

  @override
  void dispose() {
    _nameController.dispose();
    _valueController.dispose();
    _weightController.dispose();
    _nameFocus.dispose();
    _valueFocus.dispose();
    _weightFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder(
      viewModel: viewModel,
      builder: (BuildContext context, GradeEditModel model) {
        updateProgressOverlay(model.progressOverlay);
        return _buildScaffold(context, model);
      },
      onSignal: (BuildContext context, GradeEditSignal signal) {
        if (signal == GradeEditSignal.notifyGradeSaved) {
          showSuccessToast(message: S.of(context).confirmation_saveSuccess);
        } else if (signal is PopGradeEditSignal) {
          widget.onBack(context, signal.gradeId);
        }
      },
    );
  }

  Widget _buildScaffold(BuildContext context, GradeEditModel model) {
    return SystemUiOverlayAnnotation(
      systemNavigationBarColor: Theme.of(context).colorScheme.surface,
      child: Scaffold(
        body: SafeArea(
          child: _buildContent(context, model),
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, GradeEditModel model) {
    Widget result = CustomScrollView(slivers: [
      Toolbar(
        vsync: this,
        onBack: (BuildContext context) => _onWillPop(context, model),
        navType: NavButtonType.close,
        horizontalPadding: Dimens.grid_3x,
      ),
      SliverList(
        delegate: SliverChildListDelegate([
          _buildName(context, model),
          Divider(),
          _buildValue(context, model),
          Divider(),
          if (model.showCourse) ...[
            _buildCourse(context, model),
            Divider(),
          ],
          _buildDate(context, model),
          Divider(),
          _buildCategory(context, model),
          Divider(),
          _buildWeight(context, model),
          Divider(),
          Container(height: Paddings.of(context).vertical),
        ]),
      ),
    ]);

    result = Column(children: [
      Expanded(child: result),
      SaveOrDiscardFooter(
        label: S.of(context).common_grade,
        onSave: () => viewModel.onSaveGrade(),
        onDiscard: () => widget.onBack(context, null),
      ),
    ]);

    result = WillPopScope(
      onWillPop: () => _onWillPop(context, model),
      child: result,
    );

    return result;
  }

  Widget _buildName(BuildContext context, GradeEditModel model) {
    _nameController.setTextIfChanged(model.name);

    return Padding(
      padding: EdgeInsets.only(
        bottom: Dimens.grid_1x,
        left: Dimens.grid_7_5x,
        right: Dimens.grid_3x,
      ),
      child: TextField(
        controller: _nameController,
        focusNode: _nameFocus,
        autofocus: model.autofocusName,
        style: Theme.of(context).textTheme.headline5,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: S.of(context).common_name,
        ),
        textCapitalization: TextCapitalization.sentences,
        onChanged: (String text) => viewModel.onEditName(text),
        onSubmitted: (String text) => _valueFocus.requestFocus(),
      ),
    );
  }

  Widget _buildValue(BuildContext context, GradeEditModel model) {
    final TextInputType keyboardType =
        model.gradingSystem.type == GradingSystemType.alphabetic ? TextInputType.text : TextInputType.number;

    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_trophy_24,
      value: model.value,
      placeholder: S.of(context).common_grade,
      error: model.valueError,
      controller: _valueController,
      focus: _valueFocus,
      model: model,
      keyboardType: keyboardType,
      onChanged: (String text) => viewModel.onEditGrade(text),
      onSubmitted: (String text) => _unfocusTextFields(),
    );
  }

  Widget _buildCourse(BuildContext context, GradeEditModel model) {
    return _buildPopupMenu(
      context: context,
      entries: [
        for (Course course in model.availableCourses)
          PopupMenuItem(
            value: course.id,
            child: Text(course.name),
          ),
      ],
      newItemLabel: S.of(context).common_addCourse,
      newItemId: 'add-course-id',
      onItemSelected: (String id) => viewModel.onEditCourse(id),
      onAddNewItem: () async {
        final String courseId = await Nav.courses_add();
        if (courseId != null) viewModel.onEditCourse(courseId);
      },
      imageAsset24: Images.ic_book_24,
      text: model.selectedCourse?.name,
      placeholder: S.of(context).common_pickColor,
    );
  }

  Widget _buildDate(BuildContext context, GradeEditModel model) {
    return Row(children: [
      Expanded(
        child: InkWell(
          onTap: () => _showDatePicker(context, model),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
            child: Row(children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: Dimens.grid_3x),
                child: Image.asset(
                  Images.ic_timetable_24,
                  width: 24,
                  height: 24,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  excludeFromSemantics: true,
                ),
              ),
              Expanded(
                child: Text(
                  _formatDate(context, model),
                  semanticsLabel: _formatDateSemanticLabel(context, model),
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        color: model.dateTime == null
                            ? Theme.of(context).colorScheme.primaryVariant
                            : Theme.of(context).colorScheme.primary,
                      ),
                ),
              ),
            ]),
          ),
        ),
      ),
      if (model.dateTime != null)
        MergeSemantics(
          child: InkWell(
            onTap: () {
              _unfocusTextFields();
              viewModel.onEditDate(null);
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x, horizontal: Dimens.grid_3x),
              child: Image.asset(
                Images.ic_close_24,
                width: 16,
                height: 16,
                color: Theme.of(context).colorScheme.primaryVariant,
                semanticLabel: S.of(context).common_delete,
              ),
            ),
          ),
        ),
    ]);
  }

  String _formatDate(BuildContext context, GradeEditModel model) {
    if (model.dateTime == null) return S.of(context).common_pickDate;
    return DateFormatter.formatRelativeOrShortDate(context, model.dateTime);
  }

  String _formatDateSemanticLabel(BuildContext context, GradeEditModel model) {
    if (model.dateTime == null) return S.of(context).common_pickDate;
    return '${S.of(context).common_date}: ${_formatDate(context, model)}';
  }

  void _showDatePicker(BuildContext context, GradeEditModel model) async {
    _unfocusTextFields();

    final selectedDate = await showAdaptiveDatePicker(
      context: context,
      initialDate: model.dateTime,
    );

    if (selectedDate != null) {
      viewModel.onEditDate(selectedDate);
    }
  }

  Widget _buildCategory(BuildContext context, GradeEditModel model) {
    return _buildPopupMenu(
      context: context,
      entries: [
        for (GradeCategory category in model.availableCategories)
          PopupMenuItem(
            value: category.id,
            child: Text(GradesFormatter.formatGradeCategory(context, category)),
          ),
      ],
      newItemLabel: S.of(context).gradeCategory_addNew,
      newItemId: 'add-category-id',
      onItemSelected: (String id) => viewModel.onEditCategory(id),
      onAddNewItem: () => _showGradeCategoryDialog(context),
      imageAsset24: Images.ic_tag_24,
      text: GradesFormatter.formatGradeCategory(context, model.selectedCategory),
      placeholder: S.of(context).common_pickCategory,
    );
  }

  void _showGradeCategoryDialog(BuildContext context) async {
    final String text = await showTextFieldDialog(
      context: context,
      routeSettings: RouteSettings(name: R.grades_categoryAdd),
      title: S.of(context).common_gradeCategory,
      message: S.of(context).grades_gradeCategoryDialog_message,
      placeholder: S.of(context).common_category,
      submitText: S.of(context).common_save,
      validator: (String text) {
        if (Strings.isBlank(text)) {
          return FieldMustNotBeEmptyException();
        }
        return null;
      },
    );

    if (text != null) {
      viewModel.onAddCategory(AddGradeCategory(customName: text, updatedAt: DateTime.now()));
    }
  }

  Widget _buildWeight(BuildContext context, GradeEditModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_weight_24,
      value: model.weight,
      error: model.weightError,
      placeholder: S.of(context).grades_weight_placeholder,
      controller: _weightController,
      focus: _weightFocus,
      model: model,
      textInputAction: TextInputAction.done,
      keyboardType: TextInputType.number,
      onChanged: (String text) => viewModel.onEditWeight(text),
    );
  }

  Widget _buildTextFieldWithIcon({
    @required BuildContext context,
    @required String imageAsset24,
    @required String value,
    Exception error,
    @required String placeholder,
    @required TextEditingController controller,
    @required FocusNode focus,
    @required GradeEditModel model,
    TextInputType keyboardType = TextInputType.text,
    TextInputAction textInputAction = TextInputAction.next,
    ValueChanged<String> onChanged,
    ValueChanged<String> onSubmitted,
  }) {
    controller.setTextIfChanged(value);

    Widget result = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            imageAsset24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_1_5x, vertical: Dimens.grid_0_5x),
            child: TextField(
              controller: controller,
              focusNode: focus,
              style: Theme.of(context).textTheme.bodyText1,
              textInputAction: textInputAction,
              keyboardType: keyboardType,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: placeholder,
                errorText: ResourceLocator.errorToText(context, error),
              ),
              onChanged: onChanged,
              onSubmitted: onSubmitted,
            ),
          ),
        ),
      ],
    );

    return result;
  }

  Widget _buildPopupMenu({
    @required BuildContext context,
    @required List<PopupMenuEntry<String>> entries,
    @required String newItemLabel,
    @required String newItemId,
    @required ValueChanged<String> onItemSelected,
    @required VoidCallback onAddNewItem,
    @required String imageAsset24,
    @required String text,
    @required String placeholder,
  }) {
    return PopupMenuButton(
      itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
        ...entries,
        PopupMenuDivider(),
        PopupMenuItem(
          value: newItemId,
          child: Text(
            newItemLabel,
            style: Theme.of(context).popupMenuTheme.textStyle.copyWith(
                  color: Theme.of(context).colorScheme.secondary,
                ),
          ),
        ),
      ],
      onSelected: (String id) async {
        _unfocusTextFields();

        if (id == newItemId) {
          onAddNewItem();
        } else {
          onItemSelected(id);
        }
      },
      child: Padding(
        padding: EdgeInsets.only(
          top: Dimens.grid_2x,
          bottom: Dimens.grid_2x,
          left: Dimens.grid_3x,
          right: Dimens.grid_3x,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(right: Dimens.grid_3x),
              child: Image.asset(
                imageAsset24,
                width: 24,
                height: 24,
                color: Theme.of(context).colorScheme.primaryVariant,
                excludeFromSemantics: true,
              ),
            ),
            Expanded(
              child: Text(
                text ?? placeholder,
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                      color: text != null
                          ? Theme.of(context).colorScheme.primary
                          : Theme.of(context).colorScheme.primaryVariant,
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _onWillPop(BuildContext context, GradeEditModel model) async {
    if (model.discardChangesPopup) {
      _unfocusTextFields();
      _showSaveChangesDialog(context);
    } else {
      Navigator.of(context).pop();
    }

    return false;
  }

  void _showSaveChangesDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_discardChangesQuestion,
      primaryAction: DialogActionData(
        text: S.of(context).common_keepEditing,
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_discard,
        onTap: () => widget.onBack(context, null),
      ),
    );
  }

  void _unfocusTextFields() {
    _nameFocus.unfocus();
    _valueFocus.unfocus();
    _weightFocus.unfocus();
  }
}
