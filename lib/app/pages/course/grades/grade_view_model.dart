import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/preferences/preferences_listener.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';

class GradeModel extends Equatable {
  final bool progressOverlay;
  final bool showEmptyState;
  final Course course;
  final GradeSortOrder sortOrder;
  final GradingSystem gradingSystem;
  final List<GradeData> grades;

  const GradeModel({
    @required this.progressOverlay,
    @required this.showEmptyState,
    @required this.course,
    @required this.sortOrder,
    @required this.gradingSystem,
    @required this.grades,
  });

  @override
  List<Object> get props => [progressOverlay, showEmptyState, course, sortOrder, gradingSystem, grades];
}

class GradeData extends Equatable {
  final Grade grade;
  final GradeCategory category;

  const GradeData(this.grade, this.category);

  @override
  List<Object> get props => [grade, category];
}

enum GradeSortOrder {
  latest,
  oldest,
  highest,
  lowest,
}

extension GradeSortOrderExt on GradeSortOrder {
  static GradeSortOrder forTag(String tag) {
    for (GradeSortOrder order in GradeSortOrder.values) {
      if (order.tag == tag) return order;
    }

    return GradeSortOrder.latest;
  }

  String get tag {
    switch (this) {
      case GradeSortOrder.latest:
        return 'latest';
      case GradeSortOrder.oldest:
        return 'oldest';
      case GradeSortOrder.highest:
        return 'highest';
      case GradeSortOrder.lowest:
        return 'lowest';
    }
    throw ArgumentError('Unsupported grade sort order: $this');
  }

  int Function(Grade a, Grade b) get comparator {
    switch (this) {
      case GradeSortOrder.latest:
        return (Grade a, Grade b) => b.dateTime.compareTo(a.dateTime);
      case GradeSortOrder.oldest:
        return (Grade a, Grade b) => a.dateTime.compareTo(b.dateTime);
      case GradeSortOrder.highest:
        return (Grade a, Grade b) => b.value.compareTo(a.value);
      case GradeSortOrder.lowest:
        return (Grade a, Grade b) => a.value.compareTo(b.value);
    }
    throw ArgumentError('Unsupported grade sort order: $this');
  }
}

enum GradeSignal {
  notifyGradeDeleted,
}

class GradeViewModel extends ViewModel<GradeModel, GradeSignal>
    implements SemesterListener, CourseListener, GradeListener, PreferencesListener {
  static const Logger _logger = Logger('GradeViewModel');
  static const String _keySortOrder = 'key_gradeViewModel_sortOrder';

  final SemesterManager _semesterManager;
  final CourseManager _courseManager;
  final GradeManager _gradeManager;
  final Preferences _preferences;
  final String _courseId;

  bool _progressOverlay = false;

  GradeViewModel(this._semesterManager, this._courseManager, this._gradeManager, this._preferences, this._courseId);

  @override
  void onStart() async {
    super.onStart();
    _semesterManager.addSemesterListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _gradeManager.addGradeListener(this, notifyOnAttach: false);
    _preferences.addPreferencesListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _semesterManager.removeSemesterListener(this);
    _courseManager.removeCourseListener(this);
    _gradeManager.removeGradeListener(this);
    _preferences.removePreferencesListener(this);
    super.onStop();
  }

  @override
  void onSemestersChanged(SemesterManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onGradesChanged(GradeManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onPreferencesChanged(Preferences preferences, List<String> changedKeys) async {
    if (changedKeys.contains(_keySortOrder)) {
      await _tryEmitModel();
    }
  }

  void onSelectSortOrder(GradeSortOrder order) async {
    await _tryWithProgressOverlay(() async {
      _sortOrder = order;
    });
  }

  void onDeleteGrade(String id) async {
    await _tryWithProgressOverlay(() async {
      await _gradeManager.delete(id);
      emitSignal(GradeSignal.notifyGradeDeleted);
    });
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<GradeModel> get _model async {
    final Semester semester = await _semesterManager.getActive();
    final List<GradeData> grades = await _fetchGrades(semester.id);
    final Course course = await _courseManager.getById(_courseId);
    final GradingSystem gradingSystem = await _gradeManager.getGradingSystem();

    return GradeModel(
      progressOverlay: _progressOverlay,
      showEmptyState: grades.isEmpty,
      course: course,
      sortOrder: _sortOrder,
      gradingSystem: gradingSystem,
      grades: grades,
    );
  }

  Future<List<GradeData>> _fetchGrades(String semesterId) async {
    final List<GradeCategory> categories = await _gradeManager.getCategories();
    final List<Grade> grades = await _gradeManager.getAll(courseId: _courseId, semesterId: semesterId);
    grades.sort(_sortOrder.comparator);

    final List<GradeData> result = [];
    for (Grade grade in grades) {
      final GradeCategory category = categories.firstWhereOrNull((e) => e.id == grade.categoryId);
      result.add(GradeData(grade, category));
    }

    return result;
  }

  GradeSortOrder get _sortOrder => GradeSortOrderExt.forTag(_preferences.getString(_keySortOrder));

  set _sortOrder(GradeSortOrder order) => _preferences.setString(_keySortOrder, order.tag);
}
