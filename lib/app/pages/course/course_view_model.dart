import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/exam/exam_listener.dart';
import 'package:school_pen/domain/exam/exam_manager.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/grade/model/grades_stats.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/homework/homework_listener.dart';
import 'package:school_pen/domain/homework/homework_manager.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/preferences/preferences_listener.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/reminder/reminder_listener.dart';
import 'package:school_pen/domain/reminder/reminder_manager.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/timetable_listener.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';

class CourseModel extends Equatable {
  final String schoolYearId;
  final List<CourseData> courses;
  final Semester activeSemester;
  final List<Semester> availableSemesters;
  final GradingSystem gradingSystem;
  final bool showEmptyState;
  final bool isGradingSystemBannerConsumed;
  final GradesStats gradesStats;
  final int allExams;
  final int activeExams;
  final int allHomework;
  final int activeHomework;
  final int allReminders;
  final int activeReminders;
  final int overdueExams;
  final int overdueHomework;
  final int overdueReminders;

  const CourseModel({
    @required this.schoolYearId,
    @required this.courses,
    @required this.activeSemester,
    @required this.availableSemesters,
    @required this.gradingSystem,
    @required this.showEmptyState,
    @required this.isGradingSystemBannerConsumed,
    @required this.gradesStats,
    @required this.allExams,
    @required this.activeExams,
    @required this.allHomework,
    @required this.activeHomework,
    @required this.allReminders,
    @required this.activeReminders,
    @required this.overdueExams,
    @required this.overdueHomework,
    @required this.overdueReminders,
  });

  @override
  List<Object> get props => [
        schoolYearId,
        courses,
        activeSemester,
        availableSemesters,
        gradingSystem,
        showEmptyState,
        isGradingSystemBannerConsumed,
        gradesStats,
        allExams,
        activeExams,
        allHomework,
        activeHomework,
        allReminders,
        activeReminders,
        overdueExams,
        overdueHomework,
        overdueReminders,
      ];
}

class CourseData extends Equatable {
  final Course course;
  final DateTimeRange nextOccurrence;

  const CourseData(this.course, this.nextOccurrence);

  @override
  List<Object> get props => [course, nextOccurrence];
}

enum CourseSignal {
  notifySemesterCreated,
}

class CourseViewModel extends ViewModel<CourseModel, CourseSignal>
    implements
        SemesterListener,
        CourseListener,
        GradeListener,
        TimetableListener,
        ExamListener,
        HomeworkListener,
        ReminderListener,
        PreferencesListener {
  static const Logger _logger = Logger('CourseViewModel');
  static const String _keyGradingSystemBanner = 'key_courses_gradingSystemBannerConsumed';

  final SchoolYearManager _schoolYearManager;
  final SemesterManager _semesterManager;
  final CourseManager _courseManager;
  final GradeManager _gradeManager;
  final TimetableManager _timetableManager;
  final ExamManager _examManager;
  final HomeworkManager _homeworkManager;
  final ReminderManager _reminderManager;
  final Preferences _preferences;

  CourseViewModel(
    this._schoolYearManager,
    this._semesterManager,
    this._courseManager,
    this._gradeManager,
    this._timetableManager,
    this._examManager,
    this._homeworkManager,
    this._reminderManager,
    this._preferences,
  );

  @override
  void onStart() async {
    super.onStart();
    _semesterManager.addSemesterListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _gradeManager.addGradeListener(this, notifyOnAttach: false);
    _timetableManager.addTimetableListener(this, notifyOnAttach: false);
    _examManager.addExamListener(this, notifyOnAttach: false);
    _homeworkManager.addHomeworkListener(this, notifyOnAttach: false);
    _reminderManager.addReminderListener(this, notifyOnAttach: false);
    _preferences.addPreferencesListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _semesterManager.removeSemesterListener(this);
    _courseManager.removeCourseListener(this);
    _gradeManager.removeGradeListener(this);
    _timetableManager.removeTimetableListener(this);
    _examManager.removeExamListener(this);
    _homeworkManager.removeHomeworkListener(this);
    _reminderManager.removeReminderListener(this);
    _preferences.removePreferencesListener(this);
    super.onStop();
  }

  @override
  void onSemestersChanged(SemesterManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onGradesChanged(GradeManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onTimetablesChanged(TimetableManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onExamsChanged(ExamManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onHomeworkChanged(HomeworkManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onRemindersChanged(ReminderManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onPreferencesChanged(Preferences preferences, List<String> changedKeys) async {
    if (changedKeys.contains(_keyGradingSystemBanner)) {
      await _tryEmitModel();
    }
  }

  void onConsumeGradingSystemBanner() {
    _isGradingSystemBannerConsumed = true;
  }

  void onCreateSemester(AddSemester newSemester) async {
    try {
      final String id = await _semesterManager.create(newSemester);
      await _semesterManager.setActive(id);
      emitSignal(CourseSignal.notifySemesterCreated);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  void onSemesterSelected(String semesterId) {
    try {
      _semesterManager.setActive(semesterId);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  bool get _isGradingSystemBannerConsumed => _preferences.getBool(_keyGradingSystemBanner) ?? false;

  set _isGradingSystemBannerConsumed(bool consumed) => _preferences.setBool(_keyGradingSystemBanner, consumed);

  Future<CourseModel> get _model async {
    final SchoolYear year = await _schoolYearManager.getActive();
    final List<Course> courses = await _courseManager.getAll(year.id);
    final Semester activeSemester = await _semesterManager.getActive();
    final List<Semester> semesters = await _semesterManager.getAll();
    final GradingSystem gradingSystem = await _gradeManager.getGradingSystem();
    final List<SingleTimetableEvent> events = await _fetchEventsOfCourses(courses.map((e) => e.id).toList());
    final GradesStats gradesStats = await _gradeManager.getStats(semesterId: activeSemester.id);
    final List<Exam> exams = await _examManager.getAll(semesterId: activeSemester.id);
    final List<Homework> homework = await _homeworkManager.getAll(semesterId: activeSemester.id);
    final List<Reminder> reminders = await _reminderManager.getAll(semesterId: activeSemester.id);
    final DateTime today = DateTime.now().withoutTime();

    return CourseModel(
      schoolYearId: year.id,
      courses: _buildData(courses, events),
      activeSemester: activeSemester,
      availableSemesters: semesters,
      gradingSystem: gradingSystem,
      showEmptyState: courses.isEmpty,
      isGradingSystemBannerConsumed: _isGradingSystemBannerConsumed,
      gradesStats: gradesStats,
      allExams: exams.length,
      activeExams: exams.where((e) => !e.isArchived).length,
      overdueExams: exams.where((e) => !e.isArchived && e.dateTime.isBefore(today)).length,
      allHomework: homework.length,
      activeHomework: homework.where((e) => !e.isArchived).length,
      overdueHomework: homework.where((e) => !e.isArchived && e.dateTime.isBefore(today)).length,
      allReminders: reminders.length,
      activeReminders: reminders.where((e) => !e.isArchived).length,
      overdueReminders: reminders.where((e) => !e.isArchived && e.dateTime.isBefore(today)).length,
    );
  }

  List<CourseData> _buildData(List<Course> courses, List<SingleTimetableEvent> events) {
    final List<CourseData> result = [];
    for (Course course in courses) {
      final SingleTimetableEvent event = events.firstWhereOrNull((e) => e.courseId == course.id);
      result.add(CourseData(course, event?.reoccursAt));
    }
    return result;
  }

  Future<List<SingleTimetableEvent>> _fetchEventsOfCourses(List<String> coursesIds) async {
    final List<Timetable> timetables = await _timetableManager.getUserTimetables();
    final List<TimetableEvent> selectedEvents = [];

    for (Timetable timetable in timetables) {
      selectedEvents.addAll(timetable.events.where((e) => coursesIds.contains(e.courseId)));
    }

    final DateTimeRange dateRange = _buildRecentCoursesRange();
    final List<SingleTimetableEvent> singleEvents = Timetable.getSingleEventsOf(
      events: selectedEvents,
      range: dateRange.withoutTime(),
    );

    final List<SingleTimetableEvent> result =
        singleEvents.where((e) => dateRange.overlapsWith(_dateRangeOfEvent(e))).toList();

    result.sort(SingleTimetableEvent.compareByDate);
    return result;
  }

  DateTimeRange _dateRangeOfEvent(SingleTimetableEvent event) {
    return DateTimeRange(
      start: event.dateTime.withTime(event.timeRange.start),
      end: event.dateTime.withTime(event.timeRange.end),
    );
  }

  DateTimeRange _buildRecentCoursesRange() {
    final DateTime now = DateTimes.utcNow();
    return DateTimeRange(
      start: now,
      end: now.plusMonths(3),
    );
  }
}
