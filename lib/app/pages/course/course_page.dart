import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/assets/typography.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/common/formatter/grades_formatter.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/course/course_view_model.dart';
import 'package:school_pen/app/pages/exam/exams_page.dart';
import 'package:school_pen/app/pages/homework/homework_page.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/reminder/reminders_page.dart';
import 'package:school_pen/app/pages/settings/semester/semester_edit_dialog.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/bordered_card_item.dart';
import 'package:school_pen/app/widgets/bordered_container.dart';
import 'package:school_pen/app/widgets/bordered_list_item.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/master_detail_container.dart';
import 'package:school_pen/app/widgets/page_placeholder.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

/// Allows to view/edit/create course.
class CoursePage extends LifecycleWidget {
  const CoursePage({Key key, @required this.onCourseSelected}) : super(key: key);

  final ValueChanged<Course> onCourseSelected;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _CoursePageState();
}

class _CoursePageState extends LifecycleWidgetState<CoursePage>
    with ViewModelLifecycle<CourseViewModel, CoursePage>, SingleTickerProviderStateMixin {
  static const String _addSemesterId = 'add-semester-id';

  Widget _asideWidget;

  @override
  CourseViewModel provideViewModel() => CourseViewModel(Injection.get(), Injection.get(), Injection.get(),
      Injection.get(), Injection.get(), Injection.get(), Injection.get(), Injection.get(), Injection.get());

  @override
  Widget build(BuildContext context) {
    final Widget scaffold = Scaffold(
      body: SafeArea(
        child: ViewModelBuilder(
          viewModel: viewModel,
          builder: (BuildContext context, CourseModel model) {
            if (model.showEmptyState) {
              return _buildEmptyState(context, model);
            } else {
              return _buildContent(context, model);
            }
          },
          onSignal: (BuildContext context, CourseSignal signal) {
            if (signal == CourseSignal.notifySemesterCreated) {
              showSuccessToast(message: S.of(context).confirmation_saveSuccess);
            }
          },
        ),
      ),
      floatingActionButton: _buildFloatingButton(context),
    );

    return MasterDetailContainer(
      masterBuilder: (BuildContext context) => scaffold,
      detailBuilder: (BuildContext context) => _asideWidget,
      masterFlex: 3,
      detailFlex: 2,
    );
  }

  Widget _buildFloatingButton(BuildContext context) {
    return SpeedDial(
      animatedIcon: AnimatedIcons.add_event,
      animatedIconTheme: IconThemeData(size: Dimens.grid_3x),
      heroTag: null,
      tooltip: S.of(context).common_add,
      backgroundColor: Theme.of(context).colorScheme.secondary,
      foregroundColor: Theme.of(context).colorScheme.onSecondary,
      overlayColor: Theme.of(context).colorScheme.background,
      overlayOpacity: 0.85,
      curve: Curves.easeOutCubic,
      children: [
        _buildFloatingButtonChild(
          context: context,
          imageAsset: Images.ic_homework_24,
          label: S.of(context).common_homework,
          onTap: Nav.homework_add,
        ),
        _buildFloatingButtonChild(
          context: context,
          imageAsset: Images.ic_notification_24,
          label: S.of(context).common_reminder,
          onTap: Nav.reminders_add,
        ),
        _buildFloatingButtonChild(
          context: context,
          imageAsset: Images.ic_exam_24,
          label: S.of(context).common_exam,
          onTap: Nav.exams_add,
        ),
        _buildFloatingButtonChild(
          context: context,
          imageAsset: Images.ic_book_24,
          label: S.of(context).common_course,
          onTap: Nav.courses_add,
        ),
      ],
    );
  }

  SpeedDialChild _buildFloatingButtonChild({
    @required BuildContext context,
    @required String imageAsset,
    @required String label,
    @required VoidCallback onTap,
  }) {
    return SpeedDialChild(
      child: Center(
        child: Image.asset(
          imageAsset,
          color: Theme.of(context).colorScheme.secondary,
          width: 16,
          height: 16,
        ),
      ),
      label: label,
      labelStyle: Theme.of(context).textTheme.caption.copyWith(color: AppColors.gray1),
      labelBackgroundColor: AppColors.white,
      backgroundColor: AppColors.white,
      onTap: onTap,
    );
  }

  Widget _buildEmptyState(BuildContext context, CourseModel model) {
    return CustomScrollView(slivers: [
      _buildToolbar(context, model, pinned: true),
      SliverFillRemaining(
        child: PagePlaceholder(
          imageAsset: Images.illustration_abacus_192,
          imageWidth: 192,
          imageHeight: 192,
          message: S.of(context).courses_emptyState_message,
        ),
      ),
    ]);
  }

  Widget _buildContent(BuildContext context, CourseModel model) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final bool listViewLayout = _shouldShowListViewLayout(constraints);
      final bool showBanner = _showBanner(context, model);

      return CustomScrollView(slivers: [
        _buildToolbar(context, model),
        SliverList(
          delegate: SliverChildListDelegate([
            if (showBanner) _buildGradingSystemBanner(context, model),
            _buildStats(context, model),
            _buildEventsHeader(context),
            _buildEvents(context, model),
            _buildCoursesHeader(context),
            if (listViewLayout) _buildListView(context, model),
            if (!listViewLayout) _buildGridView(context, model),
            Container(height: Dimens.grid_10x),
          ]),
        ),
      ]);
    });
  }

  Widget _buildToolbar(BuildContext context, CourseModel model, {bool pinned}) {
    final bool useExpandedLayout = _showExpandedHeader(context);

    return Toolbar(
      vsync: this,
      navType: null,
      pinned: pinned,
      prefix: useExpandedLayout
          ? null
          : BorderedImageButton(
              imageAsset: Images.ic_scale_24,
              tooltip: S.of(context).common_gradingSystem,
              onTap: () {
                viewModel.onConsumeGradingSystemBanner();
                Nav.courses_gradingSystem();
              },
            ),
      actions: [
        if (useExpandedLayout)
          BorderedContainer(
            padding: EdgeInsets.symmetric(
              vertical: Dimens.grid_1x,
              horizontal: Dimens.grid_1_5x,
            ),
            color: Theme.of(context).colorScheme.surface,
            child: SmallButtonText(
              S.of(context).common_gradingSystem,
              color: Theme.of(context).colorScheme.primary,
            ),
            onTap: () {
              viewModel.onConsumeGradingSystemBanner();
              Nav.courses_gradingSystem();
            },
          ),
        SmallBorderedPopupButton(
          child: SmallButtonText(model.activeSemester.name, color: Theme.of(context).colorScheme.primary),
          itemBuilder: (BuildContext context) => _generatePopupMenuEntries(context, model),
          onSelected: (String semesterId) => _onSemesterSelected(context, model, semesterId),
        ),
      ],
    );
  }

  List<PopupMenuEntry<String>> _generatePopupMenuEntries(BuildContext context, CourseModel model) {
    return <PopupMenuEntry<String>>[
      for (Semester semester in model.availableSemesters) PopupMenuItem(value: semester.id, child: Text(semester.name)),
      PopupMenuDivider(),
      PopupMenuItem(
        value: _addSemesterId,
        child: Text(
          S.of(context).common_addNewSemesterTerm,
          style: Theme.of(context).popupMenuTheme.textStyle.copyWith(
                color: Theme.of(context).colorScheme.secondary,
              ),
        ),
      ),
    ];
  }

  void _onSemesterSelected(BuildContext context, CourseModel model, String semesterId) async {
    if (semesterId == _addSemesterId) {
      final AddSemester newSemester = await showSemesterCreateDialog(
        context: context,
        schoolYearId: model.schoolYearId,
      );
      if (newSemester != null) {
        viewModel.onCreateSemester(newSemester);
      }
    } else {
      viewModel.onSemesterSelected(semesterId);
    }
  }

  Widget _buildGradingSystemBanner(BuildContext context, CourseModel model) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        top: Dimens.grid_2x - Toolbar.bottomPadding,
        left: paddings.horizontal,
        right: paddings.horizontal,
      ),
      child: _GradingSystemBanner(viewModel: viewModel, model: model),
    );
  }

  Widget _buildStats(BuildContext context, CourseModel model) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final Paddings paddings = Paddings.of(context);
      final double screenWidth = constraints.maxWidth - paddings.horizontal * 2;

      return Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: EdgeInsets.only(
            left: paddings.horizontal,
            right: paddings.horizontal,
            top: _getHeaderTopPadding(context) - (_showBanner(context, model) ? 0 : Toolbar.bottomPadding),
          ),
          child: BorderedContainer(
            width: min(screenWidth, 360),
            padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
            color: Theme.of(context).colorScheme.surface,
            child: Row(children: [
              Expanded(
                child: Column(children: [
                  Text(
                    S.of(context).common_gradeAverage.toUpperCase(),
                    style: Theme.of(context)
                        .textTheme
                        .caption2(context)
                        .copyWith(color: Theme.of(context).colorScheme.primaryVariant),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.grid_1x),
                    child: Text(
                      model.gradesStats.averageGrade != null
                          ? model.gradingSystem.decorate(model.gradesStats.averageGrade)
                          : '--',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                ]),
              ),
              SizedBox(height: 52, child: VerticalDivider()),
              Expanded(
                child: Column(children: [
                  Text(
                    S.of(context).common_creditHours.toUpperCase(),
                    style: Theme.of(context)
                        .textTheme
                        .caption2(context)
                        .copyWith(color: Theme.of(context).colorScheme.primaryVariant),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.grid_1x),
                    child: Text(
                      model.gradesStats.totalCreditHours > 0
                          ? NumberFormat('#.##').format(model.gradesStats.totalCreditHours)
                          : '--',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                ]),
              ),
            ]),
          ),
        ),
      );
    });
  }

  Widget _buildEventsHeader(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: _getHeaderTopPadding(context),
        left: Paddings.of(context).horizontal + Dimens.grid_1x,
      ),
      child: _buildHeader(context, S.of(context).common_events),
    );
  }

  Widget _buildEvents(BuildContext context, CourseModel model) {
    final Adaptive adaptive = Adaptive.of(context);
    if (adaptive.isMobile) {
      return _MobileEventsSection(
        model: model,
        onViewExams: _onViewExams,
        onViewHomework: _onViewHomework,
        onViewReminders: _onViewReminders,
      );
    }

    return _DesktopEventsSection(
      model: model,
      onViewExams: _onViewExams,
      onViewHomework: _onViewHomework,
      onViewReminders: _onViewReminders,
    );
  }

  void _onViewExams(BuildContext context) {
    if (_shouldShowEventDetailsAsAside(context)) {
      Nav.exams_list();
    } else {
      final Widget widget = ExamsPage(
        fullscreen: false,
        onBack: (_) => setState(() => _asideWidget = null),
      );

      setState(() => _asideWidget = widget);
    }
  }

  void _onViewHomework(BuildContext context) {
    if (_shouldShowEventDetailsAsAside(context)) {
      Nav.homework_list();
    } else {
      final Widget widget = HomeworkPage(
        fullscreen: false,
        onBack: (_) => setState(() => _asideWidget = null),
      );

      setState(() => _asideWidget = widget);
    }
  }

  void _onViewReminders(BuildContext context) {
    if (_shouldShowEventDetailsAsAside(context)) {
      Nav.reminders_list();
    } else {
      final Widget widget = RemindersPage(
        fullscreen: false,
        onBack: (_) => setState(() => _asideWidget = null),
      );

      setState(() => _asideWidget = widget);
    }
  }

  bool _shouldShowEventDetailsAsAside(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    return adaptive.isMobile || (adaptive.isTablet && MediaQuery.of(context).orientation == Orientation.portrait);
  }

  Widget _buildCoursesHeader(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: _getHeaderTopPadding(context),
        left: Paddings.of(context).horizontal + Dimens.grid_1x,
      ),
      child: _buildHeader(context, S.of(context).common_courses),
    );
  }

  Widget _buildListView(BuildContext context, CourseModel model) {
    return Padding(
      padding: EdgeInsets.only(top: _getSectionTopPadding(context)),
      child: _ListView(
        courses: model.courses,
        onCourseTap: (Course course) => widget.onCourseSelected(course),
      ),
    );
  }

  Widget _buildGridView(BuildContext context, CourseModel model) {
    return Padding(
      padding: EdgeInsets.only(top: _getSectionTopPadding(context)),
      child: _GridView(
        courses: model.courses,
        onCourseTap: (Course course) => widget.onCourseSelected(course),
      ),
    );
  }
}

class _GradingSystemBanner extends StatelessWidget {
  final CourseViewModel viewModel;
  final CourseModel model;

  const _GradingSystemBanner({
    Key key,
    @required this.viewModel,
    @required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(Dimens.cardRadius)),
        color: Theme.of(context).colorScheme.success(context),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(Dimens.grid_2x),
                  child: Text(
                    sprintf(
                      S.of(context).courses_gradingSystemSet_message,
                      [GradesFormatter.formatGradingSystem(context, model.gradingSystem)],
                    ),
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                          color: Theme.of(context).colorScheme.onSuccess(context),
                        ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_1x, right: Dimens.grid_1x),
                child: MergeSemantics(
                  child: InkWell(
                    onTap: () => viewModel.onConsumeGradingSystemBanner(),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: Dimens.grid_0_5x, horizontal: Dimens.grid_0_5x),
                      child: Image.asset(
                        Images.ic_close_24,
                        width: 16,
                        height: 16,
                        color: Theme.of(context).colorScheme.onSuccess(context),
                        semanticLabel: S.of(context).common_close,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(right: Dimens.grid_2x, bottom: Dimens.grid_2x),
            child: SmallRaisedButton(
              color: Theme.of(context).colorScheme.onSuccess(context),
              child: SmallButtonText(
                S.of(context).common_change,
                color: Theme.of(context).colorScheme.success(context),
              ),
              onPressed: () {
                viewModel.onConsumeGradingSystemBanner();
                Nav.courses_gradingSystem();
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _MobileEventsSection extends StatelessWidget {
  final CourseModel model;
  final ValueChanged<BuildContext> onViewExams;
  final ValueChanged<BuildContext> onViewHomework;
  final ValueChanged<BuildContext> onViewReminders;

  const _MobileEventsSection({
    Key key,
    @required this.model,
    @required this.onViewExams,
    @required this.onViewHomework,
    @required this.onViewReminders,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    final ColorScheme colors = Theme.of(context).colorScheme;

    return Container(
      height: Dimens.smallButtonHeight + _getSectionTopPadding(context),
      child: ListView(
        shrinkWrap: true,
        primary: false,
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.only(
          left: paddings.horizontal,
          right: paddings.horizontal,
          top: _getSectionTopPadding(context),
        ),
        children: [
          SmallBorderedButton(
            child: SmallButtonText(
              S.of(context).common_exams + (model.overdueExams > 0 ? ' ⚠' : ''),
              color: model.overdueExams > 0 ? colors.error : colors.secondary,
            ),
            onPressed: () => onViewExams(context),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_1_5x),
            child: SmallBorderedButton(
              child: SmallButtonText(
                S.of(context).common_homework + (model.overdueHomework > 0 ? ' ⚠' : ''),
                color: model.overdueHomework > 0 ? colors.error : colors.secondary,
              ),
              onPressed: () => onViewHomework(context),
            ),
          ),
          SmallBorderedButton(
            child: SmallButtonText(
              S.of(context).common_reminders + (model.overdueReminders > 0 ? ' ⚠' : ''),
              color: model.overdueReminders > 0 ? colors.error : colors.secondary,
            ),
            onPressed: () => onViewReminders(context),
          ),
        ],
      ),
    );
  }
}

class _DesktopEventsSection extends StatelessWidget {
  final CourseModel model;
  final ValueChanged<BuildContext> onViewExams;
  final ValueChanged<BuildContext> onViewHomework;
  final ValueChanged<BuildContext> onViewReminders;

  const _DesktopEventsSection({
    Key key,
    @required this.model,
    @required this.onViewExams,
    @required this.onViewHomework,
    @required this.onViewReminders,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    final ColorScheme colors = Theme.of(context).colorScheme;

    return Container(
      height: 64 + _getSectionTopPadding(context),
      child: ListView(
        shrinkWrap: true,
        primary: false,
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.only(
          left: paddings.horizontal,
          right: paddings.horizontal,
          top: _getSectionTopPadding(context),
        ),
        children: [
          _DesktopEvent(
            imageAsset: Images.ic_exam_24,
            label: model.overdueExams > 0
                ? sprintf(S.of(context).courses_overdueEventsFormat, [model.overdueExams])
                : sprintf(S.of(context).courses_activeEventsFormat, [model.activeExams, model.allExams]),
            labelColor: model.overdueExams > 0 ? colors.error : colors.primaryVariant,
            title: S.of(context).common_allExams,
            onTap: () => onViewExams(context),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_2x),
            child: _DesktopEvent(
              imageAsset: Images.ic_homework_24,
              label: model.overdueHomework > 0
                  ? sprintf(S.of(context).courses_overdueEventsFormat, [model.overdueHomework])
                  : sprintf(S.of(context).courses_activeEventsFormat, [model.activeHomework, model.allHomework]),
              labelColor: model.overdueHomework > 0 ? colors.error : colors.primaryVariant,
              title: S.of(context).common_allHomework,
              onTap: () => onViewHomework(context),
            ),
          ),
          _DesktopEvent(
            imageAsset: Images.ic_notification_24,
            label: model.overdueReminders > 0
                ? sprintf(S.of(context).courses_overdueEventsFormat, [model.overdueReminders])
                : sprintf(S.of(context).courses_activeEventsFormat, [model.activeReminders, model.allReminders]),
            labelColor: model.overdueReminders > 0 ? colors.error : colors.primaryVariant,
            title: S.of(context).common_allReminders,
            onTap: () => onViewReminders(context),
          ),
        ],
      ),
    );
  }
}

class _DesktopEvent extends StatelessWidget {
  final String imageAsset;
  final String label;
  final Color labelColor;
  final String title;
  final VoidCallback onTap;

  const _DesktopEvent({
    Key key,
    @required this.imageAsset,
    @required this.label,
    @required this.labelColor,
    @required this.title,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BorderedContainer(
      onTap: onTap,
      color: Theme.of(context).colorScheme.surface,
      padding: EdgeInsets.all(Dimens.grid_1_5x),
      child: Row(children: [
        Padding(
          padding: EdgeInsets.only(right: Dimens.grid_1_5x),
          child: Container(
            width: 36,
            height: 36,
            decoration: BoxDecoration(
              color: Theme.of(context).dividerColor,
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Image.asset(imageAsset, width: 16, height: 16, color: Theme.of(context).colorScheme.primary),
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              label,
              style: Theme.of(context).textTheme.caption2(context).copyWith(color: labelColor),
            ),
            Padding(
              padding: EdgeInsets.only(top: Dimens.grid_0_5x),
              child: Text(title, style: Theme.of(context).textTheme.caption),
            ),
          ],
        ),
      ]),
    );
  }
}

class _GridView extends StatelessWidget {
  const _GridView({Key key, @required this.courses, @required this.onCourseTap}) : super(key: key);

  final List<CourseData> courses;
  final ValueChanged<Course> onCourseTap;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final int crossAxisCount = _getCrossAxisCount(constraints.maxWidth);
      return StaggeredGridView.countBuilder(
        key: ValueKey(crossAxisCount),
        primary: false,
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: Paddings.of(context).horizontal),
        mainAxisSpacing: Dimens.grid_2x,
        crossAxisSpacing: Dimens.grid_2x,
        crossAxisCount: crossAxisCount,
        itemCount: courses.length,
        itemBuilder: (BuildContext context, int index) => BorderedCardItem(
          data: _courseAsCardData(courses[index], context),
          onTap: () => onCourseTap(courses[index].course),
        ),
        staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
      );
    });
  }

  int _getCrossAxisCount(double availableWidth) {
    return (availableWidth / _getMinColumnWidth(availableWidth)).floor();
  }

  double _getMinColumnWidth(double availableWidth) {
    if (availableWidth < 640) return 160;
    if (availableWidth < 800) return 196;
    return 224;
  }

  BorderedCardItemData _courseAsCardData(CourseData data, BuildContext context) {
    return BorderedCardItemData(
      thumbnailColor: _getThumbnailColor(context, data),
      title: data.course.name,
      label: _formatLabel(context, data),
      labelColor: _getLabelColor(context, data),
    );
  }
}

class _ListView extends StatelessWidget {
  const _ListView({Key key, @required this.courses, @required this.onCourseTap}) : super(key: key);

  final List<CourseData> courses;
  final ValueChanged<Course> onCourseTap;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      primary: false,
      shrinkWrap: true,
      itemCount: courses.length,
      padding: EdgeInsets.symmetric(horizontal: Paddings.of(context).horizontal),
      itemBuilder: (BuildContext context, int index) => Padding(
        padding: EdgeInsets.only(bottom: Dimens.grid_2x),
        child: BorderedListItem(
          data: _courseAsListData(courses[index], context),
          onTap: () => onCourseTap(courses[index].course),
        ),
      ),
    );
  }

  BorderedListItemData _courseAsListData(CourseData data, BuildContext context) {
    return BorderedListItemData(
      thumbnailColor: _getThumbnailColor(context, data),
      title: data.course.name,
      label: _formatLabel(context, data),
      labelColor: _getLabelColor(context, data),
    );
  }
}

Color _getThumbnailColor(BuildContext context, CourseData data) {
  return ResourceLocator.colorEnumToColor(data.course.color) ?? Theme.of(context).colorScheme.secondaryVariant;
}

String _formatLabel(BuildContext context, CourseData data) {
  final DateTimeRange nextOccurrence = data.nextOccurrence;

  if (nextOccurrence == null) {
    return S.of(context).common_noLessons;
  }

  final DateTime now = DateTimes.utcNow();
  if (nextOccurrence.contains(now)) {
    return S.of(context).common_rightNow;
  }

  if (nextOccurrence.start.isToday) {
    return S.of(context).common_today + ', ' + DateFormatter.formatTime(nextOccurrence.start);
  }

  if (nextOccurrence.start.isTomorrow) {
    return S.of(context).common_tomorrow + ', ' + DateFormatter.formatTime(nextOccurrence.start);
  }

  if (DateTimeRanges.durationInDaysOf(start: now, end: nextOccurrence.start) < 6) {
    return DateFormat.EEEE().addPattern(',', '').add_Hm().format(nextOccurrence.start);
  }

  return DateFormatter.formatFull(nextOccurrence.start);
}

Color _getLabelColor(BuildContext context, CourseData data) {
  return (data.nextOccurrence != null && data.nextOccurrence.contains(DateTimes.utcNow()))
      ? Theme.of(context).colorScheme.secondary
      : Theme.of(context).colorScheme.primaryVariant;
}

bool _showBanner(BuildContext context, CourseModel model) {
  return !model.isGradingSystemBannerConsumed && !_showExpandedHeader(context);
}

bool _showExpandedHeader(BuildContext context) {
  return !Adaptive.of(context).isMobile;
}

double _getHeaderTopPadding(BuildContext context) {
  final Adaptive adaptive = Adaptive.of(context);
  if (adaptive.isMobile) return Dimens.grid_3x;
  if (adaptive.isTablet) return Dimens.grid_4x;
  return Dimens.grid_5x;
}

double _getSectionTopPadding(BuildContext context) {
  final Adaptive adaptive = Adaptive.of(context);
  if (adaptive.isMobile || adaptive.isTablet) return Dimens.grid_1_5x;
  return Dimens.grid_2x;
}

Widget _buildHeader(BuildContext context, String text) {
  return Text(
    text.toUpperCase(),
    style: Theme.of(context).textTheme.overline.copyWith(color: Theme.of(context).colorScheme.primaryVariant),
  );
}

bool _shouldShowListViewLayout(BoxConstraints constraints) {
  return constraints.maxWidth < 400;
}
