import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/save_or_discard_footer.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/text_field_container.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/math.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

import 'grading_system_view_model.dart';

const double _textFieldWidth = 56.0;
const double _textFieldBottomPadding = 14.0;
const int _maxTextFieldLength = 3;
const double _radioButtonWidth = 56.0;
const double _radioButtonHeight = 56.0;

/// Allows to change grading system.
class GradingSystemPage extends LifecycleWidget {
  const GradingSystemPage({Key key}) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _GradingSystemPageState();
}

class _GradingSystemPageState extends LifecycleWidgetState<GradingSystemPage>
    with
        ViewModelLifecycle<GradingSystemViewModel, GradingSystemPage>,
        ProgressOverlayMixin,
        SingleTickerProviderStateMixin {
  final TextEditingController _lowestGradeController = TextEditingController(text: '1');
  final TextEditingController _highestGradeController = TextEditingController(text: '100');
  final FocusNode _lowestGradeFocus = FocusNode();
  final FocusNode _highestGradeFocus = FocusNode();

  GradingSystemType _gradingSystemType;

  @override
  GradingSystemViewModel provideViewModel() => GradingSystemViewModel(Injection.get());

  @override
  void dispose() {
    _lowestGradeController.dispose();
    _highestGradeController.dispose();
    _lowestGradeFocus.dispose();
    _highestGradeFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      systemNavigationBarColor: Theme.of(context).colorScheme.surface,
      child: Scaffold(
        body: SafeArea(
          child: WillPopScope(
            onWillPop: () => _onWillPop(context),
            child: ViewModelBuilder(
              viewModel: viewModel,
              builder: (BuildContext context, GradingSystemModel model) {
                updateProgressOverlay(model.progressOverlay);
                return _buildContent(context, model);
              },
              onSignal: (BuildContext context, GradingSystemSignal signal) {
                if (signal == GradingSystemSignal.pop) {
                  Navigator.of(context).pop();
                } else if (signal == GradingSystemSignal.notifySaved) {
                  showSuccessToast(message: S.of(context).confirmation_saveSuccess);
                } else if (signal == GradingSystemSignal.notifyMinValueMissing) {
                  showAlertDialog(
                    context: context,
                    title: S.of(context).common_error,
                    message: S.of(context).gradingSystem_error_lowestGradeMustBeFilled,
                    primaryAction: DialogActionData(text: S.of(context).common_ok),
                  );
                } else if (signal == GradingSystemSignal.notifyMaxValueMissing) {
                  showAlertDialog(
                    context: context,
                    title: S.of(context).common_error,
                    message: S.of(context).gradingSystem_error_highestGradeMustBeFilled,
                    primaryAction: DialogActionData(text: S.of(context).common_ok),
                  );
                } else if (signal == GradingSystemSignal.notifyMinValueCannotBeEqualMaxValue) {
                  showAlertDialog(
                    context: context,
                    title: S.of(context).common_error,
                    message: S.of(context).gradingSystem_error_minValueCannotEqualMaxValue,
                    primaryAction: DialogActionData(text: S.of(context).common_ok),
                  );
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, GradingSystemModel model) {
    if (_gradingSystemType == null) {
      _gradingSystemType = model.gradingSystem.type;
      if (_gradingSystemType == GradingSystemType.numeric) {
        _lowestGradeController.setTextIfChanged(sprintf('%.0f', [model.gradingSystem.minValue]));
        _highestGradeController.setTextIfChanged(sprintf('%.0f', [model.gradingSystem.maxValue]));
      }
    }

    final Paddings paddings = Paddings.of(context);
    return Column(children: [
      Expanded(
        child: CustomScrollView(slivers: [
          Toolbar(
            vsync: this,
            onBack: _onWillPop,
            navType: NavButtonType.close,
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              Padding(
                padding: EdgeInsets.only(
                  top: Dimens.grid_2x - Toolbar.bottomPadding,
                  bottom: Dimens.grid_3x,
                  left: paddings.horizontal,
                  right: paddings.horizontal,
                ),
                child: Text(
                  S.of(context).common_gradingSystem,
                  style: Theme.of(context).textTheme.headline4,
                ),
              ),
              _buildRadio(
                context: context,
                type: GradingSystemType.percentage,
                selectedType: _gradingSystemType,
                label: S.of(context).gradingSystem_percentage + ' 0-100%',
              ),
              _buildDivider(context),
              _buildRadio(
                context: context,
                type: GradingSystemType.numeric,
                selectedType: _gradingSystemType,
                label: S.of(context).gradingSystem_numeric,
              ),
              _buildTextField(
                context: context,
                label: S.of(context).gradingSystem_lowestGrade,
                controller: _lowestGradeController,
                focus: _lowestGradeFocus,
                onSubmitted: (String text) => _highestGradeFocus.requestFocus(),
              ),
              _buildTextField(
                context: context,
                label: S.of(context).gradingSystem_highestGrade,
                controller: _highestGradeController,
                focus: _highestGradeFocus,
                onSubmitted: (String text) => _highestGradeFocus.unfocus(),
              ),
              _buildDivider(context),
              _buildRadio(
                context: context,
                type: GradingSystemType.alphabetic,
                selectedType: _gradingSystemType,
                label: S.of(context).gradingSystem_alphabetic + ' A-F',
              ),
            ]),
          ),
        ]),
      ),
      SaveOrDiscardFooter(
        onSave: () {
          viewModel.onSelectGradingSystem(
            _gradingSystemType,
            tryParseDouble(_lowestGradeController.text),
            tryParseDouble(_highestGradeController.text),
          );
        },
        onDiscard: () {
          Navigator.of(context).pop();
        },
      ),
    ]);
  }

  Widget _buildRadio({
    @required BuildContext context,
    @required GradingSystemType type,
    @required GradingSystemType selectedType,
    @required String label,
  }) {
    final Paddings paddings = Paddings.of(context);
    return InkWell(
      onTap: () => _updateGradingSystem(type),
      child: Padding(
        padding: EdgeInsets.only(
          left: paddings.horizontal - Dimens.grid_2x,
          right: paddings.horizontal,
        ),
        child: Row(children: [
          Container(
            constraints: BoxConstraints(
              minWidth: _radioButtonWidth,
              minHeight: _radioButtonHeight,
            ),
            child: Radio(
              value: type,
              groupValue: selectedType,
              onChanged: _updateGradingSystem,
            ),
          ),
          Text(
            label,
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ]),
      ),
    );
  }

  Widget _buildTextField({
    @required BuildContext context,
    @required String label,
    @required TextEditingController controller,
    @required FocusNode focus,
    @required ValueChanged<String> onSubmitted,
  }) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        left: paddings.horizontal + Dimens.grid_5x,
        right: paddings.horizontal,
        bottom: Dimens.grid_2x,
      ),
      child: Row(children: [
        Text(label, style: Theme.of(context).textTheme.bodyText1),
        Spacer(),
        TextFieldContainer(
          width: _textFieldWidth,
          maxHeight: TextFieldContainer.microHeight,
          child: TextField(
            controller: controller,
            focusNode: focus,
            style: Theme.of(context).textTheme.headline6,
            textAlign: TextAlign.center,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(bottom: _textFieldBottomPadding),
            ),
            scrollPadding: EdgeInsets.zero,
            onChanged: (String text) {
              text = Strings.retainDigits(text);
              text = Strings.substringUpTo(text, _maxTextFieldLength);
              controller.setTextIfChanged(text);

              _updateGradingSystem(_gradingSystemType);
            },
            onSubmitted: onSubmitted,
          ),
        ),
      ]),
    );
  }

  Widget _buildDivider(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Paddings.of(context).horizontal),
      child: Divider(),
    );
  }

  void _updateGradingSystem(GradingSystemType type) {
    setState(() {
      _gradingSystemType = type;
    });
  }

  Future<bool> _onWillPop(BuildContext context) async {
    _showSaveChangesDialog(context);
    return false;
  }

  void _showSaveChangesDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_discardChangesQuestion,
      primaryAction: DialogActionData(
        text: S.of(context).common_keepEditing,
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_discard,
        onTap: () => Navigator.of(context).pop(),
      ),
    );
  }
}
