import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/math.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/logger/logger.dart';

class GradingSystemModel extends Equatable {
  final GradingSystem gradingSystem;
  final bool progressOverlay;

  const GradingSystemModel({
    @required this.gradingSystem,
    @required this.progressOverlay,
  });

  @override
  List<Object> get props => [gradingSystem, progressOverlay];
}

enum GradingSystemSignal {
  pop,
  notifySaved,
  notifyMinValueMissing,
  notifyMaxValueMissing,
  notifyMinValueCannotBeEqualMaxValue,
}

class GradingSystemViewModel extends ViewModel<GradingSystemModel, GradingSystemSignal> implements GradeListener {
  static const Logger _logger = Logger('GradingSystemViewModel');
  final GradeManager _gradeManager;
  bool _progressOverlay = false;

  GradingSystemViewModel(this._gradeManager);

  @override
  void onStart() async {
    super.onStart();
    _gradeManager.addGradeListener(this, notifyOnAttach: false);
    emitModel(await _model);
  }

  @override
  void onStop() {
    _gradeManager.removeGradeListener(this);
    super.onStop();
  }

  @override
  void onGradesChanged(GradeManager manager) async {
    emitModel(await _model);
  }

  void onSelectGradingSystem(GradingSystemType type, double minValue, double maxValue) async {
    if (!_validate(type, minValue, maxValue)) return;

    _progressOverlay = true;
    emitModel(await _model);

    try {
      final GradingSystem system = GradingSystem.of(type, minValue: minValue, maxValue: maxValue);
      await _gradeManager.setGradingSystem(system);
      emitSignal(GradingSystemSignal.notifySaved);
      emitSingleSignal(GradingSystemSignal.pop);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    emitModel(await _model);
  }

  bool _validate(GradingSystemType type, double minValue, double maxValue) {
    if (type == GradingSystemType.numeric && minValue == null) {
      emitSignal(GradingSystemSignal.notifyMinValueMissing);
      return false;
    }
    if (type == GradingSystemType.numeric && maxValue == null) {
      emitSignal(GradingSystemSignal.notifyMaxValueMissing);
      return false;
    }
    if (type == GradingSystemType.numeric && minValue.isCloseTo(maxValue)) {
      emitSignal(GradingSystemSignal.notifyMinValueCannotBeEqualMaxValue);
      return false;
    }
    return true;
  }

  Future<GradingSystemModel> get _model async {
    return GradingSystemModel(
      gradingSystem: await _gradeManager.getGradingSystem(),
      progressOverlay: _progressOverlay,
    );
  }
}
