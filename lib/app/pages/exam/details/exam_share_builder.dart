import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/exam/model/add_exam.dart';

class ExamShareBuilder {
  static String buildShareText(AddExam exam, Course course) {
    assert(course != null);

    String result = course.name + ': ';

    if (Strings.isNotBlank(exam.title)) {
      result += exam.title;
    }

    if (Strings.isNotBlank(exam.note)) {
      result += '\n\n';
      result += exam.note;
    }

    result += '\n\n[${DateFormatter.formatFull(exam.dateTime)}]';

    return result.trim();
  }
}
