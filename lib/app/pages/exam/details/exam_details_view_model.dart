import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/notification_options.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/exam/exam_listener.dart';
import 'package:school_pen/domain/exam/exam_manager.dart';
import 'package:school_pen/domain/exam/model/add_exam.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';

class ExamDetailsModel extends Equatable {
  final bool isEditing;
  final bool discardChangesPopup;
  final bool progressOverlay;
  final AddExamInput exam;
  final Exception dateTimeError;
  final List<Course> availableCourses;
  final Course selectedCourse;
  final Exception courseError;
  final Grade selectedGrade;
  final List<Grade> availableGrades;
  final GradingSystem gradingSystem;

  const ExamDetailsModel({
    @required this.isEditing,
    @required this.discardChangesPopup,
    @required this.progressOverlay,
    @required this.exam,
    this.dateTimeError,
    @required this.availableCourses,
    this.selectedCourse,
    this.courseError,
    this.selectedGrade,
    @required this.availableGrades,
    @required this.gradingSystem,
  });

  @override
  List<Object> get props => [
        isEditing,
        discardChangesPopup,
        progressOverlay,
        exam,
        dateTimeError,
        availableCourses,
        selectedCourse,
        courseError,
        selectedGrade,
        availableGrades,
        gradingSystem,
      ];
}

enum ExamDetailsSignal {
  notifyExamArchived,
  notifyExamUnarchived,
  notifyExamDeleted,
  notifyExamSaved,
  pop,
}

class ExamDetailsViewModel extends ViewModel<ExamDetailsModel, ExamDetailsSignal>
    implements ExamListener, CourseListener, GradeListener {
  static const Logger _logger = Logger('ExamDetailsViewModel');

  final SemesterManager _semesterManager;
  final ExamManager _examManager;
  final CourseManager _courseManager;
  final GradeManager _gradeManager;
  final String _examId;
  final String _preselectedCourseId;

  bool _editModeActive = false;
  bool _progressOverlay = false;
  AddExamInput _originalExam;
  AddExamInput _editedExam;
  Exception _dateTimeError;
  Exception _courseError;

  ExamDetailsViewModel(this._semesterManager, this._examManager, this._courseManager, this._gradeManager, this._examId,
      this._preselectedCourseId) {
    _editModeActive = _examId == null;
  }

  @override
  void onStart() async {
    super.onStart();
    _examManager.addExamListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _gradeManager.addGradeListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _examManager.removeExamListener(this);
    _courseManager.removeCourseListener(this);
    _gradeManager.removeGradeListener(this);
    super.onStop();
  }

  @override
  void onExamsChanged(ExamManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onGradesChanged(GradeManager manager) async {
    await _tryEmitModel();
  }

  void onEnableEditMode() async {
    _editModeActive = true;
    await _tryEmitModel();
  }

  void onArchiveExam(bool archived) {
    _tryWithProgressOverlay(() async {
      if (_examId == null) return;

      _editedExam = _editedExam.copyWith(isArchived: Optional(archived));
      await _examManager.update(_examId, _editedExam?.toModel());
      emitSignal(archived ? ExamDetailsSignal.notifyExamArchived : ExamDetailsSignal.notifyExamUnarchived);
      if (archived) emitSignal(ExamDetailsSignal.pop);
    });
  }

  void onSaveExam() {
    _tryWithProgressOverlay(() async {
      if (!_validate(_editedExam)) return;

      if (_examId != null) {
        await _examManager.update(_examId, _editedExam?.toModel());
      } else {
        await _examManager.create(_editedExam?.toModel());
      }
      emitSignal(ExamDetailsSignal.notifyExamSaved);
      emitSignal(ExamDetailsSignal.pop);
    });
  }

  Future<void> onEditTitle(String text) async {
    _editedExam = _editedExam.copyWith(title: Optional(text));

    await _tryEmitModel();
  }

  void onEditCourse(String courseId) async {
    if (_editedExam.courseId == courseId) return;

    _editedExam = _editedExam.copyWith(
      courseId: Optional(courseId),
      gradeId: Optional(null),
    );

    await _tryEmitModel();
  }

  void onEditGrade(String gradeId) async {
    await _tryWithProgressOverlay(() async {
      if (gradeId == null && _editedExam.gradeId != null) {
        await _gradeManager.delete(_editedExam.gradeId);
      }

      _editedExam = _editedExam.copyWith(gradeId: Optional(gradeId));

      if (_examId != null && !_editModeActive) {
        await _examManager.update(_examId, _editedExam.toModel());
      }
    });
  }

  void onEditDateTime(DateTime dateTime) async {
    _editedExam = _editedExam.copyWith(dateTime: Optional(dateTime));

    await _tryEmitModel();
  }

  void onEditNote(String text) async {
    _editedExam = _editedExam.copyWith(note: Optional(text));

    await _tryEmitModel();
  }

  void onEditNotification(NotificationOptions notification) async {
    _editedExam = _editedExam.copyWith(notification: Optional(notification));

    await _tryEmitModel();
  }

  void onDeleteExam() {
    _tryWithProgressOverlay(() async {
      await _examManager.delete(_examId);
      emitSignal(ExamDetailsSignal.notifyExamDeleted);
      emitSignal(ExamDetailsSignal.pop);
    });
  }

  bool _validate(AddExamInput exam) {
    _courseError = null;
    _dateTimeError = null;

    if (exam.courseId == null) {
      _courseError = FieldMustNotBeEmptyException();
      return false;
    }

    if (exam.dateTime == null) {
      _dateTimeError = FieldMustNotBeEmptyException();
      return false;
    }

    return true;
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<ExamDetailsModel> get _model async {
    final AddExamInput exam = await _obtainExam();
    if (exam == null) return null;

    final Semester semester = await _semesterManager.getById(exam.semesterId);
    final List<Course> courses = await _courseManager.getAll(semester.schoolYearId);
    final List<Grade> grades = await _gradeManager.getAll(semesterId: exam.semesterId);
    final List<Grade> selectedGrades = grades.where((e) => e.courseId == exam.courseId).toList();
    final GradingSystem gradingSystem = await _gradeManager.getGradingSystem();

    return ExamDetailsModel(
      isEditing: _editModeActive,
      discardChangesPopup: _originalExam == null || exam != _originalExam,
      progressOverlay: _progressOverlay,
      exam: exam,
      dateTimeError: _dateTimeError,
      availableCourses: courses.where((e) => e.id != exam.courseId).toList(),
      selectedCourse: courses.firstWhereOrNull((e) => e.id == exam.courseId),
      courseError: _courseError,
      selectedGrade: selectedGrades.firstWhereOrNull((e) => e.id == exam.gradeId),
      availableGrades: selectedGrades,
      gradingSystem: gradingSystem,
    );
  }

  Future<AddExamInput> _obtainExam() async {
    if (_editedExam != null) return _editedExam;

    if (_examId != null) {
      final Exam exam = await _examManager.getById(_examId);
      final AddExam addExam = exam?.toAddExam()?.copyWith(updatedAt: Optional(DateTime.now()));
      _originalExam = addExam != null ? AddExamInput.from(addExam) : null;
      _editedExam = _originalExam;
    } else {
      final Semester semester = await _semesterManager.getActive();
      _editedExam = AddExamInput(
        semesterId: semester.id,
        courseId: _preselectedCourseId,
        gradeId: null,
        title: '',
        note: '',
        dateTime: null,
        notification: null,
        isArchived: false,
        updatedAt: DateTime.now(),
      );
    }

    return _editedExam;
  }
}

class AddExamInput extends Equatable {
  final String semesterId;
  final String courseId;
  final String gradeId;
  final String title;
  final String note;
  final DateTime dateTime;
  final NotificationOptions notification;
  final bool isArchived;
  final DateTime updatedAt;

  const AddExamInput({
    @required this.semesterId,
    this.courseId,
    this.gradeId,
    this.title,
    this.note,
    this.dateTime,
    this.notification,
    @required this.isArchived,
    @required this.updatedAt,
  });

  factory AddExamInput.from(AddExam exam) {
    return AddExamInput(
      semesterId: exam.semesterId,
      courseId: exam.courseId,
      gradeId: exam.gradeId,
      title: exam.title,
      note: exam.note,
      dateTime: exam.dateTime,
      notification: exam.notification,
      isArchived: exam.isArchived,
      updatedAt: exam.updatedAt,
    );
  }

  AddExam toModel() {
    return AddExam(
      semesterId: semesterId,
      courseId: courseId,
      gradeId: gradeId,
      title: title,
      note: note,
      dateTime: dateTime ?? DateTime.now(),
      notification: notification,
      isArchived: isArchived,
      updatedAt: updatedAt,
    );
  }

  AddExamInput copyWith({
    Optional<String> semesterId,
    Optional<String> courseId,
    Optional<String> gradeId,
    Optional<String> title,
    Optional<String> note,
    Optional<DateTime> dateTime,
    Optional<NotificationOptions> notification,
    Optional<bool> isArchived,
    Optional<DateTime> updatedAt,
  }) {
    return AddExamInput(
      semesterId: Optional.unwrapOrElse(semesterId, this.semesterId),
      courseId: Optional.unwrapOrElse(courseId, this.courseId),
      gradeId: Optional.unwrapOrElse(gradeId, this.gradeId),
      title: Optional.unwrapOrElse(title, this.title),
      note: Optional.unwrapOrElse(note, this.note),
      dateTime: Optional.unwrapOrElse(dateTime, this.dateTime),
      notification: Optional.unwrapOrElse(notification, this.notification),
      isArchived: Optional.unwrapOrElse(isArchived, this.isArchived),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  /// Whether the notification date is in the past.
  /// False when not set.
  bool isNotificationExpired(DateTime now) {
    if (notification == null) return false;
    return notification.getFireDateTime(dateTime ?? DateTime.now()).isBefore(now);
  }

  @override
  List<Object> get props => [semesterId, courseId, gradeId, title, note, dateTime, notification, isArchived, updatedAt];
}
