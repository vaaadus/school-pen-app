import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/common/formatter/notification_formatter.dart';
import 'package:school_pen/app/common/intents.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/exam/details/exam_details_view_model.dart';
import 'package:school_pen/app/pages/exam/details/exam_share_builder.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/pages/picker/notification_options_dialog.dart';
import 'package:school_pen/app/pages/picker/options_dialog.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/save_or_discard_footer.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/notification_options.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/generated/l10n.dart';

/// Offers the user a possibility to edit an exam (if [examId] not null)
/// or create a new one if exam is null.
class ExamDetailsPage extends LifecycleWidget {
  final String examId;
  final String courseId;
  final OnBackCallback onBack;
  final bool fullscreen;

  const ExamDetailsPage({
    Key key,
    this.examId,
    this.courseId,
    this.onBack = Navigator.pop,
    this.fullscreen = true,
  }) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _ExamDetailsPageState();
}

class _ExamDetailsPageState extends LifecycleWidgetState<ExamDetailsPage>
    with
        ViewModelLifecycle<ExamDetailsViewModel, ExamDetailsPage>,
        ProgressOverlayMixin,
        SingleTickerProviderStateMixin {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _noteController = TextEditingController();

  final FocusNode _titleFocus = FocusNode();
  final FocusNode _noteFocus = FocusNode();

  @override
  ExamDetailsViewModel provideViewModel() => ExamDetailsViewModel(
      Injection.get(), Injection.get(), Injection.get(), Injection.get(), widget.examId, widget.courseId);

  @override
  void dispose() {
    _titleController.dispose();
    _noteController.dispose();
    _titleFocus.dispose();
    _noteFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder(
      viewModel: viewModel,
      builder: (BuildContext context, ExamDetailsModel model) {
        updateProgressOverlay(model.progressOverlay);
        return _buildScaffold(context, model);
      },
      onSignal: (BuildContext context, ExamDetailsSignal signal) {
        if (signal == ExamDetailsSignal.notifyExamArchived) {
          showSuccessToast(message: S.of(context).confirmation_archived);
        } else if (signal == ExamDetailsSignal.notifyExamUnarchived) {
          showSuccessToast(message: S.of(context).confirmation_unarchived);
        } else if (signal == ExamDetailsSignal.notifyExamDeleted) {
          showSuccessToast(message: S.of(context).confirmation_deleted);
        } else if (signal == ExamDetailsSignal.notifyExamSaved) {
          showSuccessToast(message: S.of(context).confirmation_saveSuccess);
        } else if (signal == ExamDetailsSignal.pop) {
          widget.onBack(context);
        }
      },
    );
  }

  Widget _buildScaffold(BuildContext context, ExamDetailsModel model) {
    Widget result = Scaffold(
      body: SafeArea(
        child: _buildContent(context, model),
      ),
    );

    if (widget.fullscreen) {
      final ColorScheme colors = Theme.of(context).colorScheme;
      result = SystemUiOverlayAnnotation(
        child: result,
        systemNavigationBarColor: (model.isEditing || !model.exam.isArchived) ? colors.surface : colors.background,
      );
    }

    return result;
  }

  Widget _buildContent(BuildContext context, ExamDetailsModel model) {
    Widget result = CustomScrollView(slivers: [
      Toolbar(
        vsync: this,
        navType: NavButtonType.close,
        horizontalPadding: Dimens.grid_3x,
        onBack: (BuildContext context) => _onWillPop(context, model),
        actions: [
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => viewModel.onEnableEditMode(),
              imageAsset: Images.ic_edit_pen_24,
              tooltip: S.of(context).common_edit,
            ),
          if (!model.isEditing && Intents.isShareTextSupported())
            BorderedImageButton(
              onTap: () => Intents.shareText(ExamShareBuilder.buildShareText(
                model.exam.toModel(),
                model.selectedCourse,
              )),
              imageAsset: Images.ic_share_24,
              tooltip: S.of(context).common_share,
            ),
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => viewModel.onArchiveExam(!model.exam.isArchived),
              imageAsset: model.exam.isArchived ? Images.ic_unarchive_24 : Images.ic_archive_24,
              tooltip: model.exam.isArchived ? S.of(context).common_unarchive : S.of(context).common_archive,
            ),
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => _showDeleteConfirmation(context),
              imageAsset: Images.ic_trash_24,
              tooltip: S.of(context).common_delete,
            ),
        ],
      ),
      SliverList(
        delegate: SliverChildListDelegate([
          Container(height: Dimens.grid_3x - Toolbar.bottomPadding),
          if (Strings.isNotBlank(model.exam.title) || model.isEditing) ...[
            _buildTitle(context, model),
            Divider(),
          ],
          _buildCourse(context, model),
          Divider(),
          _buildGrade(context, model),
          Divider(),
          _buildDateTime(context, model),
          Divider(),
          if (model.exam.notification != null || model.isEditing) ...[
            _buildNotification(context, model),
            Divider(),
          ],
          if (Strings.isNotBlank(model.exam.note) || model.isEditing) ...[
            _buildNote(context, model),
            Divider(),
          ],
          Container(height: Paddings.of(context).vertical),
        ]),
      ),
    ]);

    if (model.isEditing) {
      result = Column(children: [
        Expanded(child: result),
        SaveOrDiscardFooter(
          label: S.of(context).common_exam,
          onSave: () => viewModel.onSaveExam(),
          onDiscard: () => widget.onBack(context),
        ),
      ]);
    } else if (!model.exam.isArchived) {
      result = Column(children: [
        Expanded(child: result),
        SaveOrDiscardFooter(
          label: S.of(context).common_exam,
          saveText: S.of(context).common_done,
          onSave: () => viewModel.onArchiveExam(true),
        ),
      ]);
    }

    result = WillPopScope(
      onWillPop: () => _onWillPop(context, model),
      child: result,
    );

    return result;
  }

  Widget _buildTitle(BuildContext context, ExamDetailsModel model) {
    _titleController.setTextIfChanged(model.exam.title);

    Widget result = Padding(
      padding: EdgeInsets.only(
        bottom: Dimens.grid_1x,
        left: Dimens.grid_7_5x,
        right: Dimens.grid_3x,
      ),
      child: TextField(
        controller: _titleController,
        focusNode: _titleFocus,
        enabled: model.isEditing,
        autofocus: model.isEditing,
        maxLines: null,
        style: Theme.of(context).textTheme.headline5,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: S.of(context).common_addTitle,
        ),
        textInputAction: TextInputAction.next,
        textCapitalization: TextCapitalization.sentences,
        onChanged: (String text) => viewModel.onEditTitle(text),
        onSubmitted: (String text) => _noteFocus.requestFocus(),
      ),
    );

    if (!model.isEditing) {
      result = Semantics(
        label: S.of(context).common_title + ': ' + model.exam.title,
        child: result,
      );
    }

    return result;
  }

  Widget _buildCourse(BuildContext context, ExamDetailsModel model) {
    final String addCourseId = 'add-course-id';

    return MergeSemantics(
      child: PopupMenuButton(
        enabled: model.isEditing,
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          for (Course course in model.availableCourses)
            PopupMenuItem(
              value: course.id,
              child: Text(course.name),
            ),
          PopupMenuDivider(),
          PopupMenuItem(
            value: addCourseId,
            child: Text(
              S.of(context).common_addCourse,
              style: Theme.of(context).popupMenuTheme.textStyle.copyWith(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
            ),
          ),
        ],
        onSelected: (String id) async {
          _unfocusTextFields();

          if (id == addCourseId) {
            final String courseId = await Nav.courses_add();
            if (courseId != null) viewModel.onEditCourse(courseId);
          } else {
            viewModel.onEditCourse(id);
          }
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                top: Dimens.grid_2x,
                bottom: Dimens.grid_2x,
                left: Dimens.grid_3x,
              ),
              child: Image.asset(
                Images.ic_book_24,
                width: 24,
                height: 24,
                color: Theme.of(context).colorScheme.primaryVariant,
                excludeFromSemantics: true,
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(
                  top: Dimens.grid_2x,
                  bottom: model.courseError == null ? Dimens.grid_2x : Dimens.grid_1x,
                  left: Dimens.grid_3x,
                  right: Dimens.grid_3x,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      model.selectedCourse?.name ?? S.of(context).common_pickCourse,
                      style: Theme.of(context).textTheme.bodyText1.copyWith(
                            color: model.selectedCourse?.name != null
                                ? Theme.of(context).colorScheme.primary
                                : Theme.of(context).colorScheme.primaryVariant,
                          ),
                    ),
                    if (model.courseError != null)
                      Padding(
                        padding: EdgeInsets.only(top: Dimens.grid_1x, bottom: Dimens.grid_1x),
                        child: Text(
                          ResourceLocator.errorToText(context, model.courseError),
                          style: Theme.of(context).textTheme.caption.copyWith(
                                color: Theme.of(context).colorScheme.error,
                              ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildGrade(BuildContext context, ExamDetailsModel model) {
    return InkWell(
      onTap: () => _showGradePicker(context, model),
      child: Row(children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
            child: Row(children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: Dimens.grid_3x),
                child: Image.asset(
                  Images.ic_trophy_24,
                  width: 24,
                  height: 24,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  excludeFromSemantics: true,
                ),
              ),
              Expanded(
                child: Text(
                  model.selectedGrade != null
                      ? model.selectedGrade.name + ' (${model.gradingSystem.decorate(model.selectedGrade.value)})'
                      : S.of(context).common_pickGrade,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        color: model.selectedGrade != null
                            ? Theme.of(context).colorScheme.primary
                            : Theme.of(context).colorScheme.primaryVariant,
                      ),
                ),
              ),
            ]),
          ),
        ),
      ]),
    );
  }

  Widget _buildDateTime(BuildContext context, ExamDetailsModel model) {
    return InkWell(
      onTap: model.isEditing ? () => _showDateTimePicker(context, model) : null,
      child: Row(children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
            child: Row(children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: Dimens.grid_3x),
                child: Image.asset(
                  Images.ic_timetable_24,
                  width: 24,
                  height: 24,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  excludeFromSemantics: true,
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      model.exam.dateTime != null
                          ? DateFormatter.formatRelativeOrFullDate(context, model.exam.dateTime)
                          : S.of(context).common_pickDateAndTime,
                      style: Theme.of(context).textTheme.bodyText1.copyWith(
                            color: model.exam.dateTime == null
                                ? Theme.of(context).colorScheme.primaryVariant
                                : Theme.of(context).colorScheme.primary,
                          ),
                    ),
                    if (model.dateTimeError != null)
                      Padding(
                        padding: EdgeInsets.only(top: Dimens.grid_1x),
                        child: Text(
                          ResourceLocator.errorToText(context, model.dateTimeError),
                          style: Theme.of(context).textTheme.caption.copyWith(
                                color: Theme.of(context).colorScheme.error,
                              ),
                        ),
                      ),
                  ],
                ),
              ),
            ]),
          ),
        ),
      ]),
    );
  }

  Widget _buildNotification(BuildContext context, ExamDetailsModel model) {
    return Row(children: [
      Expanded(
        child: InkWell(
          onTap: model.isEditing ? () => _showNotificationPicker(context, model) : null,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
            child: Row(children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: Dimens.grid_3x),
                child: Image.asset(
                  Images.ic_notification_24,
                  width: 24,
                  height: 24,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  excludeFromSemantics: true,
                ),
              ),
              Expanded(
                child: Text(
                  _formatNotificationText(context, model),
                  semanticsLabel: _formatNotificationSemanticLabel(context, model),
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        decoration: model.exam.isNotificationExpired(DateTime.now())
                            ? TextDecoration.lineThrough
                            : TextDecoration.none,
                        color: model.exam.notification == null
                            ? Theme.of(context).colorScheme.primaryVariant
                            : Theme.of(context).colorScheme.primary,
                      ),
                ),
              ),
            ]),
          ),
        ),
      ),
      if (model.isEditing && model.exam.notification != null)
        MergeSemantics(
          child: InkWell(
            onTap: () {
              _unfocusTextFields();
              viewModel.onEditNotification(null);
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x, horizontal: Dimens.grid_3x),
              child: Image.asset(
                Images.ic_close_24,
                width: 16,
                height: 16,
                color: Theme.of(context).colorScheme.primaryVariant,
                semanticLabel: S.of(context).common_delete,
              ),
            ),
          ),
        ),
    ]);
  }

  String _formatNotificationText(BuildContext context, ExamDetailsModel model) {
    if (model.exam.notification == null) return S.of(context).common_addNotification;
    return NotificationFormatter.formatNotification(context, model.exam.notification);
  }

  String _formatNotificationSemanticLabel(BuildContext context, ExamDetailsModel model) {
    if (model.exam.notification == null) return S.of(context).common_addNotification;
    return '${S.of(context).common_notification}: ${_formatNotificationText(context, model)}';
  }

  Widget _buildNote(BuildContext context, ExamDetailsModel model) {
    _noteController.setTextIfChanged(model.exam.note);

    Widget result = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            Images.ic_description_24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_1_5x, vertical: Dimens.grid_0_5x),
            child: TextField(
              controller: _noteController,
              focusNode: _noteFocus,
              enabled: model.isEditing,
              style: Theme.of(context).textTheme.bodyText1,
              maxLines: null,
              textInputAction: TextInputAction.newline,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: S.of(context).common_addNote,
              ),
              onChanged: (String text) => viewModel.onEditNote(text),
            ),
          ),
        ),
      ],
    );

    if (!model.isEditing) {
      result = Semantics(
        label: S.of(context).common_note + ': ' + model.exam.note,
        child: result,
      );
    }

    return result;
  }

  void _showGradePicker(BuildContext context, ExamDetailsModel model) async {
    _unfocusTextFields();

    final String addGradeId = 'add-grade-id';
    final String removeGradeId = 'remove-grade-id';
    String gradeId;

    if (model.availableGrades.isEmpty) {
      gradeId = await Nav.grades_add(courseId: model.selectedCourse.id);
    } else {
      gradeId = await showOptionsDialog(
        context: context,
        title: S.of(context).common_pickGrade,
        itemBuilder: (BuildContext context) => [
          for (Grade grade in model.availableGrades)
            OptionDialogItem(
              text: grade.name + ' (${model.gradingSystem.decorate(grade.value)})',
              label: DateFormatter.formatRelativeOrFullDate(context, grade.dateTime),
              value: grade.id,
            ),
          OptionDialogItem(
            text: S.of(context).common_addGrade,
            customColor: Theme.of(context).colorScheme.secondary,
            value: addGradeId,
          ),
          if (model.selectedGrade != null)
            OptionDialogItem(
              text: S.of(context).common_delete,
              customColor: Theme.of(context).colorScheme.error,
              value: removeGradeId,
            ),
        ],
      );
    }

    if (gradeId == addGradeId) {
      gradeId = await Nav.grades_add(courseId: model.selectedCourse.id);
    }

    if (gradeId == removeGradeId) {
      viewModel.onEditGrade(null);
    } else if (gradeId != null) {
      viewModel.onEditGrade(gradeId);
    }
  }

  void _showDateTimePicker(BuildContext context, ExamDetailsModel model) async {
    _unfocusTextFields();

    final DateTime dateTime = await showAdaptiveDateTimePicker(
      context: context,
      initialDate: model.exam.dateTime ?? DateTime.now().add(Duration(days: 1)).withMinutes(0),
    );

    if (dateTime != null) {
      viewModel.onEditDateTime(dateTime);
    }
  }

  void _showNotificationPicker(BuildContext context, ExamDetailsModel model) async {
    _unfocusTextFields();

    final NotificationOptions notification = await showNotificationOptionsDialog(
      context: context,
      initialNotification: model.exam.notification,
    );

    if (notification != null) {
      viewModel.onEditNotification(notification);
    }
  }

  Future<bool> _onWillPop(BuildContext context, ExamDetailsModel model) async {
    _unfocusTextFields();

    if (model.isEditing && model.discardChangesPopup) {
      _showSaveChangesDialog(context);
    } else {
      widget.onBack(context);
    }

    return false;
  }

  void _showDeleteConfirmation(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_delete,
      message: S.of(context).exams_deleteDialog_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteExam(),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }

  void _showSaveChangesDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_discardChangesQuestion,
      primaryAction: DialogActionData(
        text: S.of(context).common_keepEditing,
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_discard,
        onTap: () => widget.onBack(context),
      ),
    );
  }

  void _unfocusTextFields() {
    _titleFocus.unfocus();
    _noteFocus.unfocus();
  }
}
