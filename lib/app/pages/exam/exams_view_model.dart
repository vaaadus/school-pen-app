import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/exam/exam_listener.dart';
import 'package:school_pen/domain/exam/exam_manager.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';

class ExamsModel extends Equatable {
  final bool showEmptyState;
  final Course course;
  final GradingSystem gradingSystem;
  final List<ExamData> overdueExams;
  final List<ExamData> activeExams;
  final List<ExamData> archivedExams;

  const ExamsModel({
    @required this.showEmptyState,
    this.course,
    @required this.gradingSystem,
    @required this.overdueExams,
    @required this.activeExams,
    @required this.archivedExams,
  });

  @override
  List<Object> get props => [showEmptyState, course, gradingSystem, overdueExams, activeExams, archivedExams];
}

class ExamData extends Equatable {
  final Exam exam;
  final Course course;
  final Grade grade;

  const ExamData(this.exam, this.course, this.grade);

  @override
  List<Object> get props => [exam, course, grade];
}

class ExamsViewModel extends ViewModel<ExamsModel, Object>
    implements SemesterListener, ExamListener, CourseListener, GradeListener {
  static const Logger _logger = Logger('ExamViewModel');

  final SemesterManager _semesterManager;
  final ExamManager _examManager;
  final CourseManager _courseManager;
  final GradeManager _gradeManager;
  final String _courseId;

  ExamsViewModel(this._semesterManager, this._examManager, this._courseManager, this._gradeManager, this._courseId);

  @override
  void onStart() async {
    super.onStart();
    _semesterManager.addSemesterListener(this, notifyOnAttach: false);
    _examManager.addExamListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _gradeManager.addGradeListener(this, notifyOnAttach: false);

    await _tryEmitModel();
  }

  @override
  void onStop() {
    _semesterManager.removeSemesterListener(this);
    _examManager.removeExamListener(this);
    _courseManager.removeCourseListener(this);
    _gradeManager.removeGradeListener(this);
    super.onStop();
  }

  @override
  void onSemestersChanged(SemesterManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onExamsChanged(ExamManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onGradesChanged(GradeManager manager) async {
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<ExamsModel> get _model async {
    final Semester semester = await _semesterManager.getActive();
    final Course course = await _courseManager.getById(_courseId);
    final GradingSystem gradingSystem = await _gradeManager.getGradingSystem();
    final List<ExamData> exams = await _fetchExams(semester.schoolYearId, semester.id);
    final DateTime today = DateTime.now().withoutTime();

    return ExamsModel(
      showEmptyState: exams.isEmpty,
      course: course,
      gradingSystem: gradingSystem,
      overdueExams: exams.where((e) => !e.exam.isArchived && e.exam.dateTime.isBefore(today)).toList(),
      activeExams: exams.where((e) => !e.exam.isArchived && e.exam.dateTime.isSameDateAs(today)).toList(),
      archivedExams: exams.where((e) => e.exam.isArchived).toList(),
    );
  }

  Future<List<ExamData>> _fetchExams(String schoolYearId, String semesterId) async {
    final List<Exam> exams = await _examManager.getAll(semesterId: semesterId);
    final List<Exam> selectedExams = exams.where((e) => e.courseId == _courseId || _courseId == null).toList();
    final List<Course> courses = await _courseManager.getAll(schoolYearId);
    final List<Grade> grades = await _gradeManager.getAll(semesterId: semesterId);

    final List<ExamData> result = [];
    for (Exam exam in selectedExams) {
      final Course course = courses.firstWhereOrNull((e) => e.id == exam.courseId);
      final Grade grade = grades.firstWhereOrNull((e) => e.id == exam.gradeId);

      result.add(ExamData(exam, course, grade));
    }

    return result;
  }
}
