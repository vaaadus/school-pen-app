// class InAppSubscriptionPage extends LifecycleWidget {
//   const InAppSubscriptionPage({Key key}) : super(key: key);
//
//   @override
//   _InAppSubscriptionSnapshot createState() => _InAppSubscriptionSnapshot();
// }
//
// class _InAppSubscriptionSnapshot extends LifecycleWidgetState<InAppSubscriptionPage>
//     with BlocLifecycleMixin<InAppSubscriptionPage, InAppSubscriptionBloc> {
//   static const double alphaVisible = 1.0;
//   static const double alphaInvisible = 0.0;
//
//   @override
//   InAppSubscriptionBloc createBloc() => InAppSubscriptionBloc();
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: BlocBuilder(
//           bloc: bloc,
//           builder: (BuildContext context, Snapshot snapshot) {
//             if (snapshot == Snapshot.ServerError) {
//               return _buildServerError(context);
//             } else if (snapshot == Snapshot.Loading) {
//               return _buildLoadingState(context);
//             } else if (snapshot is SubscriptionAvailableSnapshot) {
//               return _buildSubscriptionAvailableState(context, snapshot);
//             } else {
//               return _buildSubscribedState(context, snapshot as SubscribedSnapshot);
//             }
//           },
//         ),
//       ),
//     );
//   }
//
//   Widget _buildServerError(BuildContext context) {
//     return Container(); // TODO
//   }
//
//   Widget _buildLoadingState(BuildContext context) {
//     return _buildContent(
//       context: context,
//       children: [
//         _buildSpace(),
//         _buildImage(),
//         _buildHeader(context, ''),
//         _buildMessage(context, ''),
//         _buildSpace(),
//         _buildSubscriptionPlans(context, []),
//         _buildSpace(),
//         _buildDisclaimer(context),
//       ],
//     );
//   }
//
//   Widget _buildSubscriptionAvailableState(BuildContext context, SubscriptionAvailableSnapshot snapshot) {
//     return _buildContent(
//       context: context,
//       children: [
//         _buildSpace(),
//         _buildImage(),
//         _buildHeader(context, 'S.of(context).page_premium_header'),
//         _buildMessage(context, 'S.of(context).page_premium_message'),
//         _buildSpace(),
//         _buildSubscriptionPlans(context, snapshot.subscriptions),
//         _buildSpace(),
//         _buildDisclaimer(context),
//       ],
//     );
//   }
//
//   Widget _buildSubscribedState(BuildContext context, SubscribedSnapshot snapshot) {
//     return _buildContent(
//       context: context,
//       children: [
//         _buildSpace(),
//         _buildImage(),
//         _buildHeader(context, snapshot.plan.title),
//         _buildMessage(context, 'S.of(context).page_premium_subscribedMessage'),
//         _buildSpace(),
//         _buildSubscriptionPlans(context, []),
//         _buildSpace(),
//         _buildManageSubscriptionsButton(context),
//       ],
//     );
//   }
//
//   Widget _buildContent({@required BuildContext context, @required List<Widget> children}) {
//     return Stack(children: [
//       Positioned.fill(
//         child: FillViewportScrollView(
//           child: Padding(
//             padding: EdgeInsets.symmetric(horizontal: Dimens.grid_4x),
//             child: Column(children: children),
//           ),
//         ),
//       ),
//     ]);
//   }
//
//   Widget _buildSpace() {
//     return Spacer();
//   }
//
//   Widget _buildImage() {
//     // TODO: new image
//     return Padding(
//       padding: EdgeInsets.only(top: Dimens.grid_4x),
//       child: Image.asset('TODO fix', width: 128, height: 128, excludeFromSemantics: true),
//     );
//   }
//
//   Widget _buildHeader(BuildContext context, String text) {
//     return AnimatedOpacity(
//       opacity: Strings.isNotBlank(text) ? alphaVisible : alphaInvisible,
//       duration: Animations.shortDuration,
//       child: Padding(
//         padding: EdgeInsets.only(top: Dimens.grid_4x),
//         child: Text(
//           text,
//           style: Theme.of(context).textTheme.headline4,
//           textAlign: TextAlign.center,
//         ),
//       ),
//     );
//   }
//
//   Widget _buildMessage(BuildContext context, String text) {
//     return AnimatedOpacity(
//       opacity: Strings.isNotBlank(text) ? alphaVisible : alphaInvisible,
//       duration: Animations.shortDuration,
//       child: Padding(
//         padding: EdgeInsets.only(top: Dimens.grid_2x),
//         child: Text(
//           text,
//           style: Theme.of(context).textTheme.bodyText1.copyWith(
//                 color: Theme.of(context).colorScheme.primaryVariant,
//               ),
//           textAlign: TextAlign.center,
//         ),
//       ),
//     );
//   }
//
//   Widget _buildSubscriptionPlans(BuildContext context, List<InAppSubscriptionOffer> plans) {
//     return AnimatedOpacity(
//       opacity: plans.isNotEmpty ? alphaVisible : alphaInvisible,
//       duration: Animations.mediumDuration,
//       child: IntrinsicHeight(
//         child: Padding(
//           padding: EdgeInsets.only(top: Dimens.grid_4x),
//           child: plans.isNotEmpty
//               ? _buildLoadedSubscriptionPlans(context, plans)
//               : _buildLoadingSubscriptionPlans(context),
//         ),
//       ),
//     );
//   }
//
//   Widget _buildLoadedSubscriptionPlans(BuildContext context, List<InAppSubscriptionOffer> plans) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceAround,
//       children: [
//         for (var plan in plans) _buildSubscriptionWidget(context, plan),
//       ],
//     );
//   }
//
//   Widget _buildSubscriptionWidget(BuildContext context, InAppSubscriptionOffer offer) {
//     return SubscriptionPlanWidget(
//       totalPeriod: _getTotalPeriod(context, offer.plan),
//       totalPrice: _getTotalPrice(offer),
//       unitPrice: offer.formattedMonthlyPrice,
//       unitPeriod: 'S.of(context).common_perMonth',
//       savings: _formatSavings(context, offer.savings),
//       backgroundColor: _getBackgroundColor(context, offer),
//       onTap: () {
//         bloc.dispatchEvent(BuySubscriptionEvent(offer));
//       },
//     );
//   }
//
//   Widget _buildLoadingSubscriptionPlans(BuildContext context) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceAround,
//       children: [
//         _buildLoadingSubscriptionWidget(context, Theme.of(context).colorScheme.surface),
//         _buildLoadingSubscriptionWidget(context, Theme.of(context).colorScheme.surface),
//       ],
//     );
//   }
//
//   Widget _buildLoadingSubscriptionWidget(BuildContext context, Color color) {
//     return SubscriptionPlanWidget(
//       totalPeriod: '',
//       totalPrice: '',
//       unitPeriod: '',
//       unitPrice: '',
//       backgroundColor: color,
//     );
//   }
//
//   String _getTotalPeriod(BuildContext context, InAppSubscriptionPlan plan) {
//     switch (plan) {
//       case InAppSubscriptionPlan.premiumMonthly:
//         return 'S.of(context).common_monthly';
//       case InAppSubscriptionPlan.premiumYearly:
//         return 'S.of(context).common_yearly';
//     }
//     throw ArgumentError('Subscription plan not supported: $plan');
//   }
//
//   String _formatSavings(BuildContext context, double savings) {
//     if (savings != null) {
//       return sprintf('S.of(context).common_save_xxx_percent', [(savings * 100.0).toInt()]);
//     } else {
//       return null;
//     }
//   }
//
//   String _getTotalPrice(InAppSubscriptionOffer offer) {
//     switch (offer.plan) {
//       case InAppSubscriptionPlan.premiumMonthly:
//         return null;
//       case InAppSubscriptionPlan.premiumYearly:
//         return offer.formattedPrice;
//     }
//     throw ArgumentError('Subscription plan not supported: ${offer.plan}');
//   }
//
//   Color _getBackgroundColor(BuildContext context, InAppSubscriptionOffer offer) {
//     return offer.savings != null ? Theme.of(context).colorScheme.surface : Theme.of(context).colorScheme.background;
//   }
//
//   Widget _buildDisclaimer(BuildContext context) {
//     return Padding(
//       padding: EdgeInsets.only(top: Dimens.grid_4x, bottom: Dimens.grid_4x),
//       child: Text(
//         'S.of(context).page_premium_subscription_disclaimer',
//         style: Theme.of(context).textTheme.bodyText2.copyWith(
//               color: Theme.of(context).colorScheme.primaryVariant,
//             ),
//       ),
//     );
//   }
//
//   Widget _buildManageSubscriptionsButton(BuildContext context) {
//     return Padding(
//       padding: EdgeInsets.only(top: Dimens.grid_4x, bottom: Dimens.grid_6x),
//       child: RaisedButton(
//         child: Text('S.of(context).common_manageSubscriptions'),
//         color: Theme.of(context).colorScheme.success(context),
//         // TODO:
//         splashColor: Theme.of(context).colorScheme.success(context).withAlpha(128),
//         onPressed: () => Nav.manageInAppSubscriptions(),
//       ),
//     );
//   }
// }
