// class InAppSubscriptionBloc extends Bloc<Event, Snapshot, BlocSignal> implements InAppSubscriptionListener {
//   static const Logger _logger = Logger('InAppSubscriptionBloc');
//   final InAppSubscriptionManager _subscriptionManager = Injection.get();
//
//   InAppSubscriptionBloc() {
//     dispatchEvent(Event.Refresh);
//     _subscriptionManager.addInAppSubscriptionListener(this);
//   }
//
//   @override
//   void dispose() {
//     _subscriptionManager.removeInAppSubscriptionListener(this);
//     super.dispose();
//   }
//
//   @override
//   void onResume() {
//     super.onResume();
//     dispatchEvent(Event.Refresh);
//   }
//
//   @override
//   void onInAppSubscriptionsChanged(InAppSubscriptionManager manager) {
//     dispatchEvent(Event.Refresh);
//   }
//
//   @override
//   Snapshot initialSnapshot() {
//     return Snapshot.Loading;
//   }
//
//   @override
//   Stream<Snapshot> mapEventToSnapshot(Event event) {
//     if (event == Event.Refresh) {
//       return _fetchData();
//     } else if (event is BuySubscriptionEvent) {
//       return _buySubscription(event.offer);
//     } else {
//       return null;
//     }
//   }
//
//   Stream<Snapshot> _fetchData() async* {
//     try {
//       var plan = await _subscriptionManager.subscribedPlan;
//       if (plan != null) {
//         yield SubscribedSnapshot(plan);
//       } else {
//         var subscriptions = await _subscriptionManager.availableSubscriptions;
//         yield SubscriptionAvailableSnapshot(subscriptions);
//       }
//     } catch (error, stackTrace) {
//       _logger.logError(error, stackTrace);
//       yield Snapshot.ServerError;
//     }
//   }
//
//   Stream<Snapshot> _buySubscription(InAppSubscriptionOffer offer) async* {
//     try {
//       await _subscriptionManager.buySubscription(offer);
//     } catch (error, stackTrace) {
//       _logger.logError(error, stackTrace);
//     }
//   }
// }
