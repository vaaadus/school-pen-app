import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/account/profile/profile_view_model.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/image_picker_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/widgets/avatar.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/save_or_discard_footer.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/text_field_container.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/generated/l10n.dart';

/// Displays details of the signed in user. Allows to edit profile, logout or delete account.
class ProfilePage extends LifecycleWidget {
  const ProfilePage({Key key, this.enableNavigation = true}) : super(key: key);

  final bool enableNavigation;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _ProfilePageState();
}

class _ProfilePageState extends LifecycleWidgetState<ProfilePage>
    with ViewModelLifecycle<ProfileViewModel, ProfilePage>, ProgressOverlayMixin, SingleTickerProviderStateMixin {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FocusNode _usernameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();

  @override
  ProfileViewModel provideViewModel() => ProfileViewModel(Injection.get());

  @override
  void dispose() {
    _usernameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _usernameFocus.dispose();
    _emailFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: _buildContent,
            listener: (BuildContext context, ProfileModel model) {
              updateProgressOverlay(model.progressOverlay);
            },
            onSignal: (BuildContext context, ProfileSignal signal) {
              if (signal == ProfileSignal.pop) {
                if (widget.enableNavigation) {
                  Navigator.of(context).pop();
                }
              } else if (signal == ProfileSignal.notifyUserMustRestoreInternet) {
                _showNoInternetDialog(context);
              } else if (signal == ProfileSignal.notifyOperationFailed) {
                _showOperationFailedDialog(context);
              } else if (signal == ProfileSignal.notifyAvatarSizeTooBig) {
                _showAvatarSizeTooBigDialog(context);
              } else if (signal == ProfileSignal.notifyAccountAlreadyLinked) {
                _showAccountAlreadyLinkedDialog(context);
              } else if (signal == ProfileSignal.notifyProfileSavedSuccessfully) {
                showSuccessToast(message: S.of(context).confirmation_saveSuccess);
              } else if (signal == ProfileSignal.notifyAvatarSavedSuccessfully) {
                showSuccessToast(message: S.of(context).confirmation_saveSuccess);
              } else if (signal == ProfileSignal.notifyAccountLinkedSuccessfully) {
                showSuccessToast(message: S.of(context).confirmation_accountConnected);
              } else if (signal == ProfileSignal.notifyAccountUnlinkedSuccessfully) {
                showSuccessToast(message: S.of(context).confirmation_accountDisconnected);
              }
            },
          ),
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, ProfileModel model) {
    final Paddings paddings = Paddings.of(context);

    Widget result = CustomScrollView(
      slivers: [
        if (widget.enableNavigation)
          Toolbar(
            vsync: this,
            onBack: (BuildContext context) => _onWillPop(context, model),
          ),
        SliverList(
          delegate: SliverChildListDelegate([
            if (!widget.enableNavigation) Container(height: Dimens.grid_7x),
            _AvatarSection(
              model: model,
              onChangeAvatar: () => _showAvatarPicker(context, model),
            ),
            Padding(
              padding: EdgeInsets.only(top: Dimens.grid_2x, left: paddings.horizontal, right: paddings.horizontal),
              child: Divider(),
            ),
            _ProfileSection(
              usernameController: _usernameController,
              emailController: _emailController,
              passwordController: _passwordController,
              usernameFocus: _usernameFocus,
              emailFocus: _emailFocus,
              onEditUsername: (String text) => viewModel.onUsernameEdited(text),
              onEditEmail: (String text) => viewModel.onEmailEdited(text),
              onChangePassword: () => Nav.changePassword(),
              model: model,
            ),
            Padding(
              padding: EdgeInsets.only(top: Dimens.grid_3x, left: paddings.horizontal, right: paddings.horizontal),
              child: Divider(),
            ),
            _AccountsSection(
              model: model,
              onLink: (AuthProvider provider) => viewModel.onLinkAccountWithProvider(provider),
              onUnlink: (AuthProvider provider) => viewModel.onUnlinkAccountFromProvider(provider),
            ),
            Padding(
              padding: EdgeInsets.only(top: Dimens.grid_2x, left: paddings.horizontal, right: paddings.horizontal),
              child: Divider(),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: Dimens.grid_7x),
              child: _DangerZoneSection(
                onLogout: () => _showLogoutDialog(context),
                onDeleteAccount: () => _showDeleteAccountDialog(context),
              ),
            ),
          ]),
        ),
      ],
    );

    result = Column(children: [
      Expanded(child: result),
      if (model.saveProfileOverlay)
        SaveOrDiscardFooter(
          onSave: _saveProfileChanges,
          onDiscard: _discardProfileChanges,
        ),
    ]);

    result = WillPopScope(
      child: result,
      onWillPop: () => _onWillPop(context, model),
    );

    return result;
  }

  Future<bool> _onWillPop(BuildContext context, ProfileModel model) async {
    _usernameFocus.unfocus();
    _emailFocus.unfocus();

    if (model.saveProfileOverlay) {
      _showSaveChangesDialog(context);
    } else {
      Navigator.of(context).pop();
    }

    return false;
  }

  void _showSaveChangesDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_discardChangesQuestion,
      primaryAction: DialogActionData(
        text: S.of(context).common_keepEditing,
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_discard,
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  void _showNoInternetDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_noInternetConnection,
      message: S.of(context).common_pleaseRestoreInternetConnection,
      primaryAction: DialogActionData(text: S.of(context).common_ok),
    );
  }

  void _showOperationFailedDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_somethingWentWrong,
      message: S.of(context).common_operationFailedTryAgainLater,
      primaryAction: DialogActionData(text: S.of(context).common_ok),
    );
  }

  void _showAvatarSizeTooBigDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_error,
      message: S.of(context).error_avatarSizeTooBig,
      primaryAction: DialogActionData(text: S.of(context).common_ok),
    );
  }

  void _showAccountAlreadyLinkedDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_error,
      message: S.of(context).error_accountAlreadyLinked,
      primaryAction: DialogActionData(text: S.of(context).common_ok),
    );
  }

  void _showAvatarPicker(BuildContext context, ProfileModel model) async {
    final ImagePickerResult result = await showImagePicker(
      context: context,
      routeSettings: RouteSettings(name: R.account_profile_avatar),
      showDeleteOption: model.user.avatar != null,
      cameraPreference: ImagePickerPreferredCamera.front,
    );

    if (result != null) {
      viewModel.onEditAvatar(result.file);
    }
  }

  void _saveProfileChanges() {
    _usernameFocus.unfocus();
    _emailFocus.unfocus();
    viewModel.onEditProfile(_usernameController.text, _emailController.text);
  }

  void _discardProfileChanges() {
    _usernameFocus.unfocus();
    _emailFocus.unfocus();
    viewModel.onDiscardChanges();
  }

  void _showLogoutDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      routeSettings: RouteSettings(name: R.account_profile_logout),
      title: S.of(context).common_logout,
      message: S.of(context).profile_logout_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_logout,
        isDestructive: true,
        onTap: () => viewModel.onLogout(),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }

  void _showDeleteAccountDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      routeSettings: RouteSettings(name: R.account_profile_deleteAccount),
      title: S.of(context).common_deleteAccount,
      message: S.of(context).profile_deleteAccount_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteAccount(),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }
}

class _AvatarSection extends StatelessWidget {
  const _AvatarSection({
    Key key,
    @required this.model,
    @required this.onChangeAvatar,
  }) : super(key: key);
  final ProfileModel model;
  final GestureTapCallback onChangeAvatar;

  @override
  Widget build(BuildContext context) {
    return MergeSemantics(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: Paddings.of(context).horizontal),
        child: Column(children: [
          InkWell(
            borderRadius: BorderRadius.all(Radius.circular(96)),
            child: Avatar(file: model.user.avatar, width: 96, height: 96),
            onTap: onChangeAvatar,
          ),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_1x),
            child: SmallMaterialButton(
              child: SmallButtonText(
                S.of(context).common_changeAvatar,
                color: Theme.of(context).colorScheme.primaryVariant,
              ),
              onPressed: onChangeAvatar,
            ),
          ),
        ]),
      ),
    );
  }
}

class _ProfileSection extends StatelessWidget {
  const _ProfileSection({
    Key key,
    @required this.usernameController,
    @required this.emailController,
    @required this.usernameFocus,
    @required this.emailFocus,
    @required this.passwordController,
    @required this.onEditUsername,
    @required this.onEditEmail,
    @required this.onChangePassword,
    @required this.model,
  }) : super(key: key);

  final TextEditingController usernameController;
  final TextEditingController emailController;
  final TextEditingController passwordController;
  final FocusNode usernameFocus;
  final FocusNode emailFocus;
  final ValueChanged<String> onEditUsername;
  final ValueChanged<String> onEditEmail;
  final GestureTapCallback onChangePassword;
  final ProfileModel model;

  @override
  Widget build(BuildContext context) {
    usernameController.setTextIfChanged(model.username);
    emailController.setTextIfChanged(model.email);
    passwordController.setTextIfChanged('Placeholder');

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Paddings.of(context).horizontal),
      child: Column(children: [
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_3x),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              S.of(context).common_profile.toUpperCase(),
              style: Theme.of(context).textTheme.overline,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_3x),
          child: TextFieldContainer(
            child: TextField(
              controller: usernameController,
              focusNode: usernameFocus,
              style: Theme.of(context).textTheme.headline6,
              keyboardType: TextInputType.name,
              autofillHints: {AutofillHints.username},
              decoration: InputDecoration(labelText: S.of(context).common_username),
              textInputAction: TextInputAction.next,
              onChanged: onEditUsername,
              onSubmitted: (String text) => emailFocus.requestFocus(),
            ),
            error: TextFieldError.forError(model.usernameError),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x),
          child: TextFieldContainer(
            child: TextField(
              controller: emailController,
              focusNode: emailFocus,
              style: Theme.of(context).textTheme.headline6,
              keyboardType: TextInputType.emailAddress,
              autofillHints: {AutofillHints.email},
              decoration: InputDecoration(labelText: S.of(context).common_email),
              textInputAction: TextInputAction.done,
              onChanged: onEditEmail,
              onSubmitted: (String text) => emailFocus.unfocus(),
            ),
            error: TextFieldError.forError(model.emailError),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x),
          child: Semantics(
            label: S.of(context).common_changePassword,
            button: true,
            child: GestureDetector(
              onTap: onChangePassword,
              child: TextFieldContainer(
                child: TextField(
                  controller: passwordController,
                  style: Theme.of(context).textTheme.headline6,
                  enabled: false,
                  obscureText: true,
                  decoration: InputDecoration(labelText: S.of(context).common_password),
                ),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}

typedef OnLinkAccountCallback = void Function(AuthProvider provider);
typedef OnUnlinkAccountCallback = void Function(AuthProvider provider);

class _AccountsSection extends StatelessWidget {
  const _AccountsSection({
    Key key,
    @required this.model,
    @required this.onLink,
    @required this.onUnlink,
  }) : super(key: key);

  final ProfileModel model;
  final OnLinkAccountCallback onLink;
  final OnUnlinkAccountCallback onUnlink;

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);

    return Column(children: [
      Padding(
        padding: EdgeInsets.only(
          top: Dimens.grid_3x,
          bottom: Dimens.grid_2x,
          left: paddings.horizontal,
        ),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            S.of(context).common_account_connections.toUpperCase(),
            style: Theme.of(context).textTheme.overline,
          ),
        ),
      ),
      for (AuthProvider provider in model.authProviders)
        _AuthProvider(
          provider: provider,
          isLinked: model.user.linkedProviders.contains(provider),
          onLink: onLink,
          onUnlink: onUnlink,
        ),
    ]);
  }
}

class _AuthProvider extends StatelessWidget {
  const _AuthProvider({
    Key key,
    @required this.provider,
    @required this.isLinked,
    @required this.onLink,
    @required this.onUnlink,
  }) : super(key: key);

  final AuthProvider provider;
  final bool isLinked;
  final OnLinkAccountCallback onLink;
  final OnUnlinkAccountCallback onUnlink;

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);

    return Padding(
      padding: EdgeInsets.only(
        top: Dimens.grid_1x,
        bottom: Dimens.grid_1x,
        left: paddings.horizontal,
        right: paddings.horizontal - Dimens.grid_2x,
      ),
      child: Row(children: [
        BorderedImageButton(
          imageAsset: ResourceLocator.authProviderToImageAsset(provider),
          imageWidth: 24,
          imageHeight: 24,
          imageTint: ResourceLocator.authProviderToImageColor(provider),
          applyImageTint: ResourceLocator.authProviderToImageColor(provider) != null,
          fillColor: ResourceLocator.authProviderToColor(provider),
        ),
        Padding(
          padding: EdgeInsets.only(left: Dimens.grid_2x),
          child: ExcludeSemantics(
            child: SmallButtonText(
              ResourceLocator.authProviderToName(context, provider),
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
          ),
        ),
        Spacer(),
        Semantics(
          label: _buildSemanticsLabel(context),
          button: true,
          excludeSemantics: true,
          child: InkWell(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: Dimens.grid_2x, vertical: Dimens.grid_1x),
              child: SmallButtonText(
                isLinked ? S.of(context).common_account_unlink : S.of(context).common_account_link,
                color: isLinked ? Theme.of(context).colorScheme.error : Theme.of(context).colorScheme.secondary,
              ),
            ),
            onTap: () {
              if (isLinked) {
                onUnlink(provider);
              } else {
                onLink(provider);
              }
            },
          ),
        )
      ]),
    );
  }

  String _buildSemanticsLabel(BuildContext context) {
    return (isLinked ? S.of(context).common_account_unlink : S.of(context).common_account_link) +
        ' ' +
        ResourceLocator.authProviderToName(context, provider);
  }
}

class _DangerZoneSection extends StatelessWidget {
  const _DangerZoneSection({
    Key key,
    @required this.onLogout,
    @required this.onDeleteAccount,
  }) : super(key: key);

  final GestureTapCallback onLogout;
  final GestureTapCallback onDeleteAccount;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Paddings.of(context).horizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_3x),
            child: RaisedButton(
              child: ButtonText(
                S.of(context).common_logout,
                color: Theme.of(context).colorScheme.onPrimary,
              ),
              color: Theme.of(context).colorScheme.primaryVariant,
              onPressed: onLogout,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_2x),
            child: RaisedButton(
              child: ButtonText(
                S.of(context).common_deleteAccount,
                color: Theme.of(context).colorScheme.onError,
              ),
              color: Theme.of(context).colorScheme.error,
              onPressed: onDeleteAccount,
            ),
          ),
        ],
      ),
    );
  }
}
