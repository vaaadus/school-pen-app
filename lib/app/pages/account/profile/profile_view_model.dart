import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/user/exception/account_already_linked_exception.dart';
import 'package:school_pen/domain/user/exception/avatar_size_too_big_exception.dart';
import 'package:school_pen/domain/user/exception/email_taken_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_email_exception.dart';
import 'package:school_pen/domain/user/exception/username_taken_exception.dart';
import 'package:school_pen/domain/user/exception/username_too_short_exception.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/domain/user/model/auth_result.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/plugin/auth_plugin.dart';
import 'package:school_pen/domain/user/user_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';

class ProfileModel extends Equatable {
  const ProfileModel({
    @required this.user,
    @required this.authProviders,
    @required this.username,
    @required this.email,
    @required this.progressOverlay,
    @required this.saveProfileOverlay,
    @required this.usernameError,
    @required this.emailError,
  });

  final User user;
  final List<AuthProvider> authProviders;
  final String username;
  final String email;
  final bool progressOverlay;
  final bool saveProfileOverlay;
  final Exception usernameError;
  final Exception emailError;

  @override
  List<Object> get props =>
      [user, authProviders, username, email, progressOverlay, saveProfileOverlay, usernameError, emailError];
}

enum ProfileSignal {
  pop,
  notifyUserMustRestoreInternet,
  notifyOperationFailed,
  notifyAvatarSizeTooBig,
  notifyAccountAlreadyLinked,
  notifyProfileSavedSuccessfully,
  notifyAvatarSavedSuccessfully,
  notifyAccountLinkedSuccessfully,
  notifyAccountUnlinkedSuccessfully,
}

class ProfileViewModel extends ViewModel<ProfileModel, ProfileSignal> implements UserListener {
  static const Logger _logger = Logger('ProfileViewModel');

  ProfileViewModel(this._userManager);

  final UserManager _userManager;
  bool _progressOverlay = false;
  String _editedUsername;
  String _editedEmail;
  Exception _usernameError;
  Exception _emailError;

  @override
  void onStart() async {
    super.onStart();
    try {
      await _userManager.refresh();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  @override
  void onResume() async {
    super.onResume();
    _userManager.addUserListener(this);
    await _tryEmitModelOrPop();
  }

  @override
  void onPause() {
    _userManager.removeUserListener(this);
    super.onPause();
  }

  @override
  void onUserChanged(User user) async {
    await _tryEmitModelOrPop();
  }

  void onUsernameEdited(String username) async {
    _editedUsername = username;
    await _tryEmitModelOrPop();
  }

  void onEmailEdited(String email) async {
    _editedEmail = email;
    await _tryEmitModelOrPop();
  }

  void onEditProfile(String username, String email) async {
    _usernameError = null;
    _emailError = null;
    _progressOverlay = true;
    await _tryEmitModelOrPop();

    try {
      await _userManager.editProfile(username: username, email: email);
      emitSignal(ProfileSignal.notifyProfileSavedSuccessfully);
      _editedEmail = null;
      _editedUsername = null;
    } on UsernameTooShortException catch (e) {
      _usernameError = e;
    } on UsernameTakenException catch (e) {
      _usernameError = e;
    } on InvalidEmailException catch (e) {
      _emailError = e;
    } on EmailTakenException catch (e) {
      _emailError = e;
    } catch (error, stackTrace) {
      _usernameError = error;
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModelOrPop();
  }

  void onDiscardChanges() async {
    _usernameError = null;
    _emailError = null;
    _editedEmail = null;
    _editedUsername = null;
    await _tryEmitModelOrPop();
  }

  void onEditAvatar(MemoryFile file) async {
    _progressOverlay = true;
    await _tryEmitModelOrPop();

    try {
      await _userManager.editAvatar(file: file);
      emitSignal(ProfileSignal.notifyAvatarSavedSuccessfully);
    } on AvatarSizeTooBigException {
      emitSignal(ProfileSignal.notifyAvatarSizeTooBig);
    } on NoInternetException {
      emitSignal(ProfileSignal.notifyUserMustRestoreInternet);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      emitSignal(ProfileSignal.notifyOperationFailed);
    }

    _progressOverlay = false;
    await _tryEmitModelOrPop();
  }

  void onLinkAccountWithProvider(AuthProvider provider) async {
    _progressOverlay = true;
    await _tryEmitModelOrPop();

    try {
      final AuthResult result = await AuthPlugin.of(provider).login();
      if (result != null) {
        await _userManager.linkWith(result);
        emitSignal(ProfileSignal.notifyAccountLinkedSuccessfully);
      }
    } on NoInternetException {
      emitSignal(ProfileSignal.notifyUserMustRestoreInternet);
    } on AccountAlreadyLinkedException {
      emitSignal(ProfileSignal.notifyAccountAlreadyLinked);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      emitSignal(ProfileSignal.notifyOperationFailed);
    }

    _progressOverlay = false;
    await _tryEmitModelOrPop();
  }

  void onUnlinkAccountFromProvider(AuthProvider provider) async {
    _progressOverlay = true;
    await _tryEmitModelOrPop();

    try {
      await _userManager.unlinkFrom(provider);
      emitSignal(ProfileSignal.notifyAccountUnlinkedSuccessfully);
    } on NoInternetException {
      emitSignal(ProfileSignal.notifyUserMustRestoreInternet);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      emitSignal(ProfileSignal.notifyOperationFailed);
    }

    _progressOverlay = false;
    await _tryEmitModelOrPop();
  }

  void onLogout() async {
    _progressOverlay = true;
    await _tryEmitModelOrPop();

    try {
      await _userManager.signOut();
      emitSingleSignal(ProfileSignal.pop);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      emitSignal(ProfileSignal.notifyOperationFailed);
    }

    _progressOverlay = false;
    await _tryEmitModelOrPop();
  }

  void onDeleteAccount() async {
    _progressOverlay = true;
    await _tryEmitModelOrPop();

    try {
      await _userManager.deleteUser();
      emitSingleSignal(ProfileSignal.pop);
    } on NoInternetException {
      emitSignal(ProfileSignal.notifyUserMustRestoreInternet);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      emitSignal(ProfileSignal.notifyOperationFailed);
    }

    _progressOverlay = false;
    await _tryEmitModelOrPop();
  }

  Future<void> _tryEmitModelOrPop() async {
    try {
      final User user = await _userManager.getCurrentUser();
      if (user == null) {
        emitSingleSignal(ProfileSignal.pop);
        return;
      }

      emitModel(await _model(user));
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      emitSingleSignal(ProfileSignal.pop);
    }
  }

  Future<ProfileModel> _model(User user) async {
    return ProfileModel(
      user: user,
      authProviders: await _userManager.getAvailableAuthProviders(),
      username: _editedUsername ?? user.username,
      email: _editedEmail ?? user.email,
      progressOverlay: _progressOverlay,
      saveProfileOverlay: _shouldShowSaveOverlay(user),
      usernameError: _usernameError,
      emailError: _emailError,
    );
  }

  bool _shouldShowSaveOverlay(User user) {
    if (_editedUsername != null && user.username != _editedUsername) return true;
    if (_editedEmail != null && user.email != _editedEmail) return true;
    return false;
  }
}
