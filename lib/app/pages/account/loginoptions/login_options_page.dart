import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/account/account_form_container.dart';
import 'package:school_pen/app/pages/account/loginoptions/login_options_view_model.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/fill_viewport_scrollview.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/generated/l10n.dart';

class LoginOptionsPage extends LifecycleWidget {
  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _LoginOptionsPageState();
}

class _LoginOptionsPageState extends LifecycleWidgetState<LoginOptionsPage>
    with ViewModelLifecycle<LoginOptionsViewModel, LoginOptionsPage>, ProgressOverlayMixin {
  @override
  LoginOptionsViewModel provideViewModel() => LoginOptionsViewModel(Injection.get());

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: (BuildContext context, LoginOptionsModel model) => AccountFormContainer(
              child: _LoginOptionsContent(viewModel: viewModel, model: model),
            ),
            listener: (BuildContext context, LoginOptionsModel model) {
              updateProgressOverlay(model.progressOverlay);
            },
            onSignal: (BuildContext context, LoginOptionsSignal signal) {
              if (signal == LoginOptionsSignal.pop) {
                Navigator.of(context).pop();
              } else if (signal == LoginOptionsSignal.notifyUserMustRestoreInternet) {
                _showNoInternetDialog(context);
              } else if (signal == LoginOptionsSignal.notifyLoginFailed) {
                _showLoginFailedDialog(context);
              }
            },
          ),
        ),
      ),
    );
  }

  void _showNoInternetDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_noInternetConnection,
      message: S.of(context).common_pleaseRestoreInternetConnection,
      primaryAction: DialogActionData(text: S.of(context).common_ok),
    );
  }

  void _showLoginFailedDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).loginOptions_loginFailed_title,
      message: S.of(context).loginOptions_loginFailed_message,
      primaryAction: DialogActionData(text: S.of(context).common_ok),
    );
  }
}

class _LoginOptionsContent extends StatelessWidget {
  const _LoginOptionsContent({
    Key key,
    @required this.viewModel,
    @required this.model,
  }) : super(key: key);

  final LoginOptionsViewModel viewModel;
  final LoginOptionsModel model;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final ThemeData theme = Theme.of(context);
      final Paddings paddings = Paddings.of(context);
      final double buttonWidth = min(328.0, constraints.maxWidth - paddings.horizontal * 2);

      return FillViewportScrollView(
        constraints: constraints,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(flex: 2),
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_4x),
                child: Image.asset(Images.ic_logo_100, width: 100, height: 100, excludeFromSemantics: true),
              ),
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_3x),
                child: Text(S.of(context).app_name, style: theme.textTheme.headline3),
              ),
              Spacer(flex: 2),
              for (AuthProvider provider in model.authProviders)
                Container(
                  width: buttonWidth,
                  padding: EdgeInsets.only(top: Dimens.grid_2x),
                  child: _AuthProviderButton(
                    provider: provider,
                    onTap: () => viewModel.onLoginWithProvider(provider),
                  ),
                ),
              Container(
                width: buttonWidth,
                padding: EdgeInsets.only(top: Dimens.grid_2x),
                child: Divider(),
              ),
              Container(
                width: buttonWidth,
                padding: EdgeInsets.only(top: Dimens.grid_2x),
                child: Row(children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(right: Dimens.grid_1x),
                      child: RaisedButton(
                        child: ButtonText(S.of(context).common_register),
                        color: Theme.of(context).colorScheme.secondaryVariant,
                        onPressed: () => Nav.register(),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: Dimens.grid_1x),
                      child: RaisedButton(
                        child: ButtonText(S.of(context).common_login),
                        onPressed: () => Nav.login(),
                      ),
                    ),
                  )
                ]),
              ),
              Spacer(flex: 1),
            ],
          ),
        ),
      );
    });
  }
}

class _AuthProviderButton extends StatelessWidget {
  final AuthProvider provider;
  final GestureTapCallback onTap;

  const _AuthProviderButton({
    Key key,
    @required this.provider,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: EdgeInsets.zero,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: Dimens.grid_2x),
        child: Row(children: [
          Image.asset(
            ResourceLocator.authProviderToImageAsset(provider),
            width: 24,
            height: 24,
            color: ResourceLocator.authProviderToImageColor(provider),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: Dimens.grid_3x),
              child: ButtonText(
                ResourceLocator.authProviderToSignInText(context, provider),
                color: ResourceLocator.authProviderTextColor(context, provider),
              ),
            ),
          ),
        ]),
      ),
      color: ResourceLocator.authProviderToColor(provider),
      onPressed: onTap,
    );
  }
}
