import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/domain/user/model/auth_result.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/plugin/auth_plugin.dart';
import 'package:school_pen/domain/user/user_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';

class LoginOptionsModel extends Equatable {
  const LoginOptionsModel({
    @required this.authProviders,
    @required this.progressOverlay,
  });

  final List<AuthProvider> authProviders;
  final bool progressOverlay;

  @override
  List<Object> get props => [authProviders, progressOverlay];
}

enum LoginOptionsSignal {
  pop,
  notifyUserMustRestoreInternet,
  notifyLoginFailed,
}

class LoginOptionsViewModel extends ViewModel<LoginOptionsModel, LoginOptionsSignal> implements UserListener {
  static const Logger _logger = Logger('LoginOptionsViewModel');

  LoginOptionsViewModel(this._userManager);

  final UserManager _userManager;
  bool _progressOverlay = false;

  @override
  void onResume() async {
    super.onResume();
    _userManager.addUserListener(this);
    emitModel(await _model);
  }

  @override
  void onPause() {
    _userManager.removeUserListener(this);
    super.onPause();
  }

  @override
  void onUserChanged(User user) {
    if (user != null) {
      emitSingleSignal(LoginOptionsSignal.pop);
    }
  }

  void onLoginWithProvider(AuthProvider provider) async {
    _progressOverlay = true;
    emitModel(await _model);

    try {
      final AuthResult result = await AuthPlugin.of(provider).login();
      if (result != null) {
        await _userManager.signInWith(result);
      }
    } on NoInternetException {
      emitSignal(LoginOptionsSignal.notifyUserMustRestoreInternet);
    } catch (error, stackTrace) {
      emitSignal(LoginOptionsSignal.notifyLoginFailed);
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    emitModel(await _model);
  }

  Future<LoginOptionsModel> get _model async {
    try {
      return LoginOptionsModel(
        authProviders: await _userManager.getAvailableAuthProviders(),
        progressOverlay: _progressOverlay,
      );
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      return LoginOptionsModel(authProviders: [], progressOverlay: _progressOverlay);
    }
  }
}
