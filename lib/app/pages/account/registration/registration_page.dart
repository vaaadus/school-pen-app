import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/account/account_form_container.dart';
import 'package:school_pen/app/pages/account/registration/registration_view_model.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/fill_viewport_scrollview.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/text_field_container.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows the user to sign up with email & password.
class RegistrationPage extends LifecycleWidget {
  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends LifecycleWidgetState<RegistrationPage>
    with ViewModelLifecycle<RegistrationViewModel, RegistrationPage>, ProgressOverlayMixin {
  @override
  RegistrationViewModel provideViewModel() => RegistrationViewModel(Injection.get());

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: (BuildContext context, RegistrationModel model) => AccountFormContainer(
              child: _RegistrationContent(viewModel: viewModel, model: model),
            ),
            listener: (BuildContext context, RegistrationModel model) {
              updateProgressOverlay(model.progressOverlay);
            },
            onSignal: (BuildContext context, RegistrationSignal signal) {
              if (signal == RegistrationSignal.pop) {
                Navigator.of(context).pop();
              }
            },
          ),
        ),
      ),
    );
  }
}

class _RegistrationContent extends StatefulWidget {
  const _RegistrationContent({
    Key key,
    @required this.viewModel,
    @required this.model,
  }) : super(key: key);

  final RegistrationViewModel viewModel;
  final RegistrationModel model;

  @override
  State<StatefulWidget> createState() => _RegistrationContentState();
}

class _RegistrationContentState extends State<_RegistrationContent> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FocusNode _usernameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  bool _passwordVisible = false;

  @override
  void initState() {
    super.initState();
    _usernameFocus.requestFocus();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _usernameFocus.dispose();
    _emailFocus.dispose();
    _passwordFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final ThemeData theme = Theme.of(context);
      final Paddings paddings = Paddings.of(context);
      final double rowWidth = min(328.0, constraints.maxWidth - paddings.horizontal * 2);

      return FillViewportScrollView(
        constraints: constraints,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(flex: 2),
              if (constraints.maxHeight >= 500)
                Padding(
                  padding: EdgeInsets.only(top: Dimens.grid_4x),
                  child: Image.asset(Images.ic_logo_100, width: 100, height: 100, excludeFromSemantics: true),
                ),
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_3x),
                child: Text(S.of(context).common_register, style: theme.textTheme.headline3),
              ),
              Spacer(flex: 2),
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_2x),
                child: TextFieldContainer(
                  width: rowWidth,
                  child: TextField(
                    controller: _usernameController,
                    focusNode: _usernameFocus,
                    style: theme.textTheme.headline6,
                    keyboardType: TextInputType.name,
                    autofillHints: {AutofillHints.newUsername},
                    decoration: InputDecoration(labelText: S.of(context).common_username),
                    textInputAction: TextInputAction.next,
                    onSubmitted: (String text) => _emailFocus.requestFocus(),
                  ),
                  error: TextFieldError.forError(widget.model.usernameError),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_2x),
                child: TextFieldContainer(
                  width: rowWidth,
                  child: TextField(
                    controller: _emailController,
                    focusNode: _emailFocus,
                    style: theme.textTheme.headline6,
                    keyboardType: TextInputType.emailAddress,
                    autofillHints: {AutofillHints.email},
                    decoration: InputDecoration(labelText: S.of(context).common_email),
                    textInputAction: TextInputAction.next,
                    onSubmitted: (String text) => _passwordFocus.requestFocus(),
                  ),
                  error: TextFieldError.forError(widget.model.emailError),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_2x),
                child: TextFieldContainer(
                  width: rowWidth,
                  child: TextField(
                    controller: _passwordController,
                    focusNode: _passwordFocus,
                    style: theme.textTheme.headline6,
                    obscureText: !_passwordVisible,
                    keyboardType: TextInputType.visiblePassword,
                    autofillHints: {AutofillHints.newPassword},
                    decoration: InputDecoration(
                      labelText: S.of(context).common_password,
                      suffixIcon: InkWell(
                        onTap: () => setState(() => _passwordVisible = !_passwordVisible),
                        child: Icon(_passwordVisible ? Icons.visibility_off : Icons.visibility),
                        excludeFromSemantics: true,
                      ),
                    ),
                    textInputAction: TextInputAction.done,
                    onSubmitted: (String text) => _register(),
                  ),
                  error: TextFieldError.forError(widget.model.passwordError),
                ),
              ),
              Spacer(),
              Container(
                padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
                constraints: BoxConstraints(minWidth: rowWidth),
                child: RaisedButton(
                  child: ButtonText(S.of(context).common_createAccount),
                  onPressed: _register,
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      );
    });
  }

  void _register() {
    _usernameFocus.unfocus();
    _emailFocus.unfocus();
    _passwordFocus.unfocus();
    widget.viewModel.onRegister(_usernameController.text, _emailController.text, _passwordController.text);
  }
}
