import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/user/exception/email_taken_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_email_exception.dart';
import 'package:school_pen/domain/user/exception/password_too_short_exception.dart';
import 'package:school_pen/domain/user/exception/username_taken_exception.dart';
import 'package:school_pen/domain/user/exception/username_too_short_exception.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/user_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';

class RegistrationModel extends Equatable {
  const RegistrationModel({
    @required this.progressOverlay,
    @required this.usernameError,
    @required this.emailError,
    @required this.passwordError,
  });

  final bool progressOverlay;
  final Exception usernameError;
  final Exception emailError;
  final Exception passwordError;

  @override
  List<Object> get props => [progressOverlay, usernameError, emailError, passwordError];
}

enum RegistrationSignal {
  pop,
}

class RegistrationViewModel extends ViewModel<RegistrationModel, RegistrationSignal> implements UserListener {
  static const Logger _logger = Logger('RegistrationViewModel');

  RegistrationViewModel(this._userManager);

  final UserManager _userManager;
  bool _progressOverlay = false;
  Exception _usernameError;
  Exception _emailError;
  Exception _passwordError;

  @override
  void onResume() async {
    super.onResume();
    _userManager.addUserListener(this);
    emitModel(await _model);
  }

  @override
  void onPause() {
    _userManager.removeUserListener(this);
    super.onPause();
  }

  @override
  void onUserChanged(User user) {
    if (user != null) {
      emitSingleSignal(RegistrationSignal.pop);
    }
  }

  void onRegister(String username, String email, String password) async {
    _usernameError = null;
    _emailError = null;
    _passwordError = null;
    _progressOverlay = true;
    emitModel(await _model);

    try {
      await _userManager.signUp(username: username, email: email, password: password);
    } on UsernameTooShortException catch (e) {
      _usernameError = e;
    } on UsernameTakenException catch (e) {
      _usernameError = e;
    } on InvalidEmailException catch (e) {
      _emailError = e;
    } on EmailTakenException catch (e) {
      _emailError = e;
    } on PasswordTooShortException catch (e) {
      _passwordError = e;
    } on NoInternetException catch (e) {
      _usernameError = e;
    } catch (error, stackTrace) {
      _usernameError = error;
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    emitModel(await _model);
  }

  Future<RegistrationModel> get _model async {
    return RegistrationModel(
      progressOverlay: _progressOverlay,
      usernameError: _usernameError,
      emailError: _emailError,
      passwordError: _passwordError,
    );
  }
}
