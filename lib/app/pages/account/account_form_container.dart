import 'package:flutter/material.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/nav_button.dart';

/// On tablets & desktop shows a centered card with the content, on mobile just the content.
class AccountFormContainer extends StatelessWidget {
  const AccountFormContainer({
    Key key,
    @required this.child,
    this.onWillPop,
  }) : super(key: key);

  final Widget child;
  final WillPopCallback onWillPop;

  @override
  Widget build(BuildContext context) {
    Widget result = child;

    if (_shouldDisplayFormInCard(context)) {
      final Adaptive adaptive = Adaptive.of(context);
      result = Center(
        child: ConstrainedBox(
          constraints: BoxConstraints(
            maxWidth: adaptive.isTablet ? 440 : 500,
            maxHeight: adaptive.isTablet ? 560 : 600,
          ),
          child: Card(child: result),
        ),
      );
    }

    result = Stack(children: [
      Positioned.fill(child: result),
      Positioned(top: 0, left: 0, child: NavButton.back(onBack: _onBack)),
    ]);

    return result;
  }

  bool _shouldDisplayFormInCard(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    final double height = MediaQuery.of(context).size.height;
    final Orientation orientation = MediaQuery.of(context).orientation;
    final bool cardLayoutOnDesktop = adaptive.isDesktop && height >= 600;
    final bool cardLayoutOnTablet = adaptive.isTablet && orientation == Orientation.landscape && height >= 600;
    return cardLayoutOnDesktop || cardLayoutOnTablet;
  }

  void _onBack(BuildContext context) {
    if (onWillPop != null) {
      onWillPop();
    } else {
      Navigator.of(context).pop();
    }
  }
}
