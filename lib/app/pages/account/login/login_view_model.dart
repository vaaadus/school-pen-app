import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/user_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';

class LoginModel extends Equatable {
  const LoginModel({
    @required this.progressOverlay,
    @required this.emailError,
  });

  final bool progressOverlay;
  final Exception emailError;

  @override
  List<Object> get props => [progressOverlay, emailError];
}

enum LoginSignal {
  pop,
  notifyPasswordResetInstructionsSent,
}

class LoginViewModel extends ViewModel<LoginModel, LoginSignal> implements UserListener {
  static const Logger _logger = Logger('LoginViewModel');

  LoginViewModel(this._userManager);

  final UserManager _userManager;
  bool _progressOverlay = false;
  Exception _emailError;

  @override
  void onResume() async {
    super.onResume();
    _userManager.addUserListener(this);
    emitModel(await _model);
  }

  @override
  void onPause() {
    _userManager.removeUserListener(this);
    super.onPause();
  }

  @override
  void onUserChanged(User user) {
    if (user != null) {
      emitSingleSignal(LoginSignal.pop);
    }
  }

  void onLogin(String email, String password) async {
    _emailError = null;
    _progressOverlay = true;
    emitModel(await _model);

    try {
      await _userManager.signIn(email: email, password: password);
    } catch (error, stackTrace) {
      _emailError = error;
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    emitModel(await _model);
  }

  void onShowResetPassword() async {
    _emailError = null;
    emitModel(await _model);
  }

  void onResetPassword(String email) async {
    _emailError = null;
    _progressOverlay = true;
    emitModel(await _model);

    try {
      await _userManager.requestPasswordReset(email: email);
      emitSignal(LoginSignal.notifyPasswordResetInstructionsSent);
    } catch (error, stackTrace) {
      _emailError = error;
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    emitModel(await _model);
  }

  Future<LoginModel> get _model async {
    return LoginModel(
      progressOverlay: _progressOverlay,
      emailError: _emailError,
    );
  }
}
