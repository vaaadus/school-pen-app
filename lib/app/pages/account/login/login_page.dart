import 'dart:math';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/account/account_form_container.dart';
import 'package:school_pen/app/pages/account/login/login_view_model.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/text_field_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/fill_viewport_scrollview.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/text_field_container.dart';
import 'package:school_pen/domain/user/exception/invalid_email_exception.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows the user to sign in with email & password.
class LoginPage extends LifecycleWidget {
  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _LoginPageState();
}

class _LoginPageState extends LifecycleWidgetState<LoginPage>
    with ViewModelLifecycle<LoginViewModel, LoginPage>, ProgressOverlayMixin {
  @override
  LoginViewModel provideViewModel() => LoginViewModel(Injection.get());

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: (BuildContext context, LoginModel model) => AccountFormContainer(
              child: _LoginContent(viewModel: viewModel, model: model),
            ),
            listener: (BuildContext context, LoginModel model) {
              updateProgressOverlay(model.progressOverlay);
            },
            onSignal: (BuildContext context, LoginSignal signal) {
              if (signal == LoginSignal.pop) {
                Navigator.of(context).pop();
              } else if (signal == LoginSignal.notifyPasswordResetInstructionsSent) {
                _showPasswordResetInstructionsDialog(context);
              }
            },
          ),
        ),
      ),
    );
  }

  void _showPasswordResetInstructionsDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      routeSettings: RouteSettings(name: R.account_changePassword_info),
      title: S.of(context).login_resetPassword_header,
      message: S.of(context).login_resetPassword_message,
      primaryAction: DialogActionData(text: S.of(context).common_ok),
    );
  }
}

class _LoginContent extends StatefulWidget {
  const _LoginContent({
    Key key,
    @required this.viewModel,
    @required this.model,
  }) : super(key: key);

  final LoginViewModel viewModel;
  final LoginModel model;

  @override
  State<StatefulWidget> createState() => _LoginContentState();
}

class _LoginContentState extends State<_LoginContent> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  bool _passwordVisible = false;

  @override
  void initState() {
    super.initState();
    _emailFocus.requestFocus();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _emailFocus.dispose();
    _passwordFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final ThemeData theme = Theme.of(context);
      final Paddings paddings = Paddings.of(context);
      final double rowWidth = min(328.0, constraints.maxWidth - paddings.horizontal * 2);

      return FillViewportScrollView(
        constraints: constraints,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(flex: 2),
              if (constraints.maxHeight >= 500)
                Padding(
                  padding: EdgeInsets.only(top: Dimens.grid_4x),
                  child: Image.asset(Images.ic_logo_100, width: 100, height: 100, excludeFromSemantics: true),
                ),
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_3x),
                child: Text(S.of(context).login_header, style: theme.textTheme.headline3),
              ),
              Spacer(flex: 2),
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_2x),
                child: TextFieldContainer(
                  width: rowWidth,
                  child: TextField(
                    controller: _emailController,
                    focusNode: _emailFocus,
                    style: theme.textTheme.headline6,
                    keyboardType: TextInputType.emailAddress,
                    autofillHints: {AutofillHints.email},
                    decoration: InputDecoration(labelText: S.of(context).common_email),
                    textInputAction: TextInputAction.next,
                    onSubmitted: (String text) => _passwordFocus.requestFocus(),
                  ),
                  error: TextFieldError.forError(widget.model.emailError),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_2x),
                child: TextFieldContainer(
                  width: rowWidth,
                  child: TextField(
                    controller: _passwordController,
                    focusNode: _passwordFocus,
                    style: theme.textTheme.headline6,
                    obscureText: !_passwordVisible,
                    keyboardType: TextInputType.visiblePassword,
                    autofillHints: {AutofillHints.password},
                    decoration: InputDecoration(
                      labelText: S.of(context).common_password,
                      suffixIcon: InkWell(
                        onTap: () => setState(() => _passwordVisible = !_passwordVisible),
                        child: Icon(_passwordVisible ? Icons.visibility_off : Icons.visibility),
                        excludeFromSemantics: true,
                      ),
                    ),
                    textInputAction: TextInputAction.done,
                    onSubmitted: (String text) => _login(),
                  ),
                ),
              ),
              Spacer(),
              Container(
                padding: EdgeInsets.only(top: Dimens.grid_2x),
                constraints: BoxConstraints(minWidth: rowWidth),
                child: RaisedButton(
                  child: ButtonText(S.of(context).common_login),
                  onPressed: _login,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: Dimens.grid_1x),
                child: MaterialButton(
                  child: SmallButtonText(
                    S.of(context).common_forgotPassword,
                    color: theme.colorScheme.primaryVariant,
                  ),
                  onPressed: () => _showResetPasswordDialog(context),
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      );
    });
  }

  void _login() {
    _emailFocus.unfocus();
    _passwordFocus.unfocus();
    widget.viewModel.onLogin(_emailController.text, _passwordController.text);
  }

  void _showResetPasswordDialog(BuildContext context) async {
    widget.viewModel.onShowResetPassword();

    final String email = await showTextFieldDialog(
      context: context,
      routeSettings: RouteSettings(name: R.account_forgotPassword),
      title: S.of(context).login_forgotPassword_header,
      message: S.of(context).login_forgotPassword_message,
      keyboardType: TextInputType.emailAddress,
      autofillHints: {AutofillHints.email},
      placeholder: S.of(context).common_email,
      validator: (String email) {
        if (!EmailValidator.validate(email)) {
          return InvalidEmailException();
        }
        return null;
      },
    );

    if (email != null) {
      widget.viewModel.onResetPassword(email);
    }
  }
}
