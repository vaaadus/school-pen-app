import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/user/exception/invalid_password_exception.dart';
import 'package:school_pen/domain/user/exception/password_too_short_exception.dart';
import 'package:school_pen/domain/user/exception/passwords_do_not_match_exception.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/user_listener.dart';
import 'package:school_pen/domain/user/user_manager.dart';

class PasswordModel extends Equatable {
  const PasswordModel({
    @required this.progressOverlay,
    @required this.oldPasswordError,
    @required this.newPasswordError,
    @required this.repeatPasswordError,
  });

  final bool progressOverlay;
  final Exception oldPasswordError;
  final Exception newPasswordError;
  final Exception repeatPasswordError;

  @override
  List<Object> get props => [progressOverlay, oldPasswordError, newPasswordError, repeatPasswordError];
}

enum PasswordSignal {
  pop,
  notifyPasswordChangedSuccessfully,
}

class PasswordViewModel extends ViewModel<PasswordModel, PasswordSignal> implements UserListener {
  static const Logger _logger = Logger('PasswordViewModel');

  PasswordViewModel(this._userManager);

  final UserManager _userManager;
  bool _progressOverlay = false;
  Exception _oldPasswordError;
  Exception _newPasswordError;
  Exception _repeatPasswordError;

  @override
  void onResume() async {
    super.onResume();
    _userManager.addUserListener(this);
    emitModel(await _model);
  }

  @override
  void onPause() {
    _userManager.removeUserListener(this);
    super.onPause();
  }

  @override
  void onUserChanged(User user) {
    if (user == null) {
      emitSingleSignal(PasswordSignal.pop);
    }
  }

  void onChangePassword(String oldPassword, String newPassword, String repeatPassword) async {
    _oldPasswordError = null;
    _newPasswordError = null;
    _repeatPasswordError = null;
    _progressOverlay = true;
    emitModel(await _model);

    if (newPassword != repeatPassword) {
      _repeatPasswordError = PasswordsDoNotMatchException();
    } else {
      try {
        await _userManager.changePassword(oldPassword: oldPassword, newPassword: newPassword);
        emitSignal(PasswordSignal.notifyPasswordChangedSuccessfully);
        emitSingleSignal(PasswordSignal.pop);
      } on PasswordTooShortException catch (e) {
        _newPasswordError = e;
      } on InvalidPasswordException catch (e) {
        _oldPasswordError = e;
      } catch (error, stackTrace) {
        _oldPasswordError = error;
        _logger.logError(error, stackTrace);
      }
    }

    _progressOverlay = false;
    emitModel(await _model);
  }

  Future<PasswordModel> get _model async {
    return PasswordModel(
      progressOverlay: _progressOverlay,
      oldPasswordError: _oldPasswordError,
      newPasswordError: _newPasswordError,
      repeatPasswordError: _repeatPasswordError,
    );
  }
}
