import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/account/account_form_container.dart';
import 'package:school_pen/app/pages/account/password/password_view_model.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/fill_viewport_scrollview.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/text_field_container.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows the user to change his password.
class PasswordPage extends LifecycleWidget {
  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _PasswordPageState();
}

class _PasswordPageState extends LifecycleWidgetState<PasswordPage>
    with ViewModelLifecycle<PasswordViewModel, PasswordPage>, ProgressOverlayMixin {
  @override
  PasswordViewModel provideViewModel() => PasswordViewModel(Injection.get());

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      child: Scaffold(
        body: SafeArea(
          child: ViewModelBuilder(
            viewModel: viewModel,
            builder: (BuildContext context, PasswordModel model) {
              return _PasswordContent(viewModel: viewModel, model: model);
            },
            listener: (BuildContext context, PasswordModel model) {
              updateProgressOverlay(model.progressOverlay);
            },
            onSignal: (BuildContext context, PasswordSignal signal) {
              if (signal == PasswordSignal.pop) {
                Navigator.of(context).pop();
              } else if (signal == PasswordSignal.notifyPasswordChangedSuccessfully) {
                showSuccessToast(message: S.of(context).confirmation_passwordChanged);
              }
            },
          ),
        ),
      ),
    );
  }
}

class _PasswordContent extends StatefulWidget {
  const _PasswordContent({
    Key key,
    @required this.viewModel,
    @required this.model,
  }) : super(key: key);

  final PasswordViewModel viewModel;
  final PasswordModel model;

  @override
  State<StatefulWidget> createState() => _PasswordContentState();
}

class _PasswordContentState extends State<_PasswordContent> {
  final TextEditingController _oldPasswordController = TextEditingController();
  final TextEditingController _newPasswordController = TextEditingController();
  final TextEditingController _repeatPasswordController = TextEditingController();

  final FocusNode _oldPasswordFocus = FocusNode();
  final FocusNode _newPasswordFocus = FocusNode();
  final FocusNode _repeatPasswordFocus = FocusNode();

  bool _oldPasswordVisible = false;
  bool _newPasswordVisible = false;
  bool _repeatPasswordVisible = false;

  @override
  void initState() {
    super.initState();
    _oldPasswordFocus.requestFocus();
  }

  @override
  void dispose() {
    _oldPasswordController.dispose();
    _newPasswordController.dispose();
    _repeatPasswordController.dispose();
    _oldPasswordFocus.dispose();
    _newPasswordFocus.dispose();
    _repeatPasswordFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onWillPop(context),
      child: AccountFormContainer(
        onWillPop: () => _onWillPop(context),
        child: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
          final ThemeData theme = Theme.of(context);
          final Paddings paddings = Paddings.of(context);
          final double rowWidth = min(328.0, constraints.maxWidth - paddings.horizontal * 2);

          return FillViewportScrollView(
            constraints: constraints,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Spacer(flex: 2),
                  if (constraints.maxHeight >= 500)
                    Padding(
                      padding: EdgeInsets.only(top: Dimens.grid_4x),
                      child: Image.asset(Images.ic_logo_100, width: 100, height: 100, excludeFromSemantics: true),
                    ),
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.grid_3x),
                    child: Text(S.of(context).common_changePassword, style: theme.textTheme.headline3),
                  ),
                  Spacer(flex: 2),
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.grid_2x),
                    child: TextFieldContainer(
                      width: rowWidth,
                      child: TextField(
                        controller: _oldPasswordController,
                        focusNode: _oldPasswordFocus,
                        style: theme.textTheme.headline6,
                        obscureText: !_oldPasswordVisible,
                        keyboardType: TextInputType.visiblePassword,
                        autofillHints: {AutofillHints.password},
                        decoration: InputDecoration(
                          labelText: S.of(context).common_oldPassword,
                          suffixIcon: InkWell(
                            onTap: () => setState(() => _oldPasswordVisible = !_oldPasswordVisible),
                            child: Icon(_oldPasswordVisible ? Icons.visibility_off : Icons.visibility),
                            excludeFromSemantics: true,
                          ),
                        ),
                        textInputAction: TextInputAction.next,
                        onSubmitted: (String text) => _newPasswordFocus.requestFocus(),
                      ),
                      error: TextFieldError.forError(widget.model.oldPasswordError),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.grid_2x),
                    child: TextFieldContainer(
                      width: rowWidth,
                      child: TextField(
                        controller: _newPasswordController,
                        focusNode: _newPasswordFocus,
                        style: theme.textTheme.headline6,
                        obscureText: !_newPasswordVisible,
                        keyboardType: TextInputType.visiblePassword,
                        autofillHints: {AutofillHints.newPassword},
                        decoration: InputDecoration(
                          labelText: S.of(context).common_newPassword,
                          suffixIcon: InkWell(
                            onTap: () => setState(() => _newPasswordVisible = !_newPasswordVisible),
                            child: Icon(_newPasswordVisible ? Icons.visibility_off : Icons.visibility),
                            excludeFromSemantics: true,
                          ),
                        ),
                        textInputAction: TextInputAction.next,
                        onSubmitted: (String text) => _repeatPasswordFocus.requestFocus(),
                      ),
                      error: TextFieldError.forError(widget.model.newPasswordError),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.grid_2x),
                    child: TextFieldContainer(
                      width: rowWidth,
                      child: TextField(
                        controller: _repeatPasswordController,
                        focusNode: _repeatPasswordFocus,
                        style: theme.textTheme.headline6,
                        obscureText: !_repeatPasswordVisible,
                        keyboardType: TextInputType.visiblePassword,
                        autofillHints: {AutofillHints.newPassword},
                        decoration: InputDecoration(
                          labelText: S.of(context).common_repeatPassword,
                          suffixIcon: InkWell(
                            onTap: () => setState(() => _repeatPasswordVisible = !_repeatPasswordVisible),
                            child: Icon(_repeatPasswordVisible ? Icons.visibility_off : Icons.visibility),
                            excludeFromSemantics: true,
                          ),
                        ),
                        textInputAction: TextInputAction.done,
                        onSubmitted: (String text) => _changePassword(),
                      ),
                      error: TextFieldError.forError(widget.model.repeatPasswordError),
                    ),
                  ),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
                    constraints: BoxConstraints(minWidth: rowWidth),
                    child: RaisedButton(
                      child: ButtonText(S.of(context).common_changePassword),
                      onPressed: _changePassword,
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }

  Future<bool> _onWillPop(BuildContext context) async {
    if (Strings.isNotBlank(_oldPasswordController.text) ||
        Strings.isNotBlank(_newPasswordController.text) ||
        Strings.isNotBlank(_repeatPasswordController.text)) {
      _showDiscardChangesDialog(context);
    } else {
      Navigator.of(context).pop();
    }

    return false;
  }

  void _showDiscardChangesDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_discardChangesQuestion,
      primaryAction: DialogActionData(
        text: S.of(context).common_keepEditing,
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_discard,
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  void _changePassword() {
    _oldPasswordFocus.unfocus();
    _newPasswordFocus.unfocus();
    _repeatPasswordFocus.unfocus();

    widget.viewModel.onChangePassword(
      _oldPasswordController.text,
      _newPasswordController.text,
      _repeatPasswordController.text,
    );
  }
}
