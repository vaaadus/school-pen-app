import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/anim/animations.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/common/intents.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/note/details/note_details_view_model.dart';
import 'package:school_pen/app/pages/note/details/note_share_builder.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/color_enum_picker_dialog.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/widgets/animated_diffutil_list.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/save_or_discard_footer.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/note/model/note_item.dart';
import 'package:school_pen/generated/l10n.dart';

/// Offers the user a possibility to edit a note (if [noteId] not null)
/// or create a new one if note is null.
class NoteDetailsPage extends LifecycleWidget {
  final String noteId;
  final OnBackCallback onBack;
  final bool fullscreen;

  const NoteDetailsPage({
    Key key,
    this.noteId,
    this.onBack = Navigator.pop,
    this.fullscreen = true,
  }) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _NoteDetailsPageState();
}

class _NoteDetailsPageState extends LifecycleWidgetState<NoteDetailsPage>
    with
        ViewModelLifecycle<NoteDetailsViewModel, NoteDetailsPage>,
        ProgressOverlayMixin,
        SingleTickerProviderStateMixin {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _noteController = TextEditingController();
  final Map<String, TextEditingController> _noteItemsController = {};

  final FocusNode _titleFocus = FocusNode();
  final FocusNode _noteFocus = FocusNode();
  final Map<String, FocusNode> _noteItemsFocus = {};

  @override
  NoteDetailsViewModel provideViewModel() => NoteDetailsViewModel(Injection.get(), Injection.get(), widget.noteId);

  @override
  void dispose() {
    _titleController.dispose();
    _noteController.dispose();
    _noteItemsController.forEach((key, value) => value.dispose());
    _titleFocus.dispose();
    _noteFocus.dispose();
    _noteItemsFocus.forEach((key, value) => value.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder(
      viewModel: viewModel,
      builder: (BuildContext context, NoteDetailsModel model) {
        updateProgressOverlay(model.progressOverlay);
        return _buildScaffold(context, model);
      },
      onSignal: (BuildContext context, NoteDetailsSignal signal) {
        if (signal == NoteDetailsSignal.notifyNoteArchived) {
          showSuccessToast(message: S.of(context).confirmation_archived);
        } else if (signal == NoteDetailsSignal.notifyNoteUnarchived) {
          showSuccessToast(message: S.of(context).confirmation_unarchived);
        } else if (signal == NoteDetailsSignal.notifyNoteDeleted) {
          showSuccessToast(message: S.of(context).confirmation_deleted);
        } else if (signal == NoteDetailsSignal.notifyNoteSaved) {
          showSuccessToast(message: S.of(context).confirmation_saveSuccess);
        } else if (signal == NoteDetailsSignal.pop) {
          widget.onBack(context);
        }
      },
    );
  }

  Widget _buildScaffold(BuildContext context, NoteDetailsModel model) {
    Widget result = Scaffold(
      body: SafeArea(
        child: _buildContent(context, model),
      ),
    );

    if (widget.fullscreen) {
      final ColorScheme colors = Theme.of(context).colorScheme;
      result = SystemUiOverlayAnnotation(
        child: result,
        systemNavigationBarColor: model.isEditing ? colors.surface : colors.background,
      );
    }

    return result;
  }

  Widget _buildContent(BuildContext context, NoteDetailsModel model) {
    Widget result = CustomScrollView(slivers: [
      Toolbar(
        vsync: this,
        navType: NavButtonType.close,
        horizontalPadding: Dimens.grid_3x,
        onBack: (BuildContext context) => _onWillPop(context, model),
        actions: [
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => viewModel.onEnableEditMode(),
              imageAsset: Images.ic_edit_pen_24,
              tooltip: S.of(context).common_edit,
            ),
          if (!model.isEditing && Intents.isShareTextSupported())
            BorderedImageButton(
              onTap: () => Intents.shareText(NoteShareBuilder.buildShareText(model.note)),
              imageAsset: Images.ic_share_24,
              tooltip: S.of(context).common_share,
            ),
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => viewModel.onArchiveNote(!model.note.isArchived),
              imageAsset: model.note.isArchived ? Images.ic_unarchive_24 : Images.ic_archive_24,
              tooltip: model.note.isArchived ? S.of(context).common_unarchive : S.of(context).common_archive,
            ),
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => _showDeleteConfirmation(context),
              imageAsset: Images.ic_trash_24,
              tooltip: S.of(context).common_delete,
            ),
        ],
      ),
      SliverList(
        delegate: SliverChildListDelegate([
          Container(height: Dimens.grid_3x - Toolbar.bottomPadding),
          if (Strings.isNotBlank(model.note.title) || model.isEditing) ...[
            _buildTitle(context, model),
            Divider(),
          ],
          if (Strings.isNotBlank(model.note.note) || model.isEditing) ...[
            _buildNote(context, model),
            Divider(),
          ],
          if (model.note.items.isNotEmpty || model.isEditing) ...[
            _buildNoteItems(context, model),
            if (model.isEditing) _buildAddNoteItem(context, model),
            Divider(),
          ],
          if (model.note.color != null || model.isEditing) ...[
            _buildColor(context, model),
            Divider(),
          ],
          if (model.note.notificationDate != null || model.isEditing) ...[
            _buildNotification(context, model),
            Divider(),
          ],
          Container(height: Paddings.of(context).vertical),
        ]),
      ),
    ]);

    if (model.isEditing) {
      result = Column(children: [
        Expanded(child: result),
        SaveOrDiscardFooter(
          label: S.of(context).common_note,
          onSave: () => viewModel.onSaveNote(),
          onDiscard: () => widget.onBack(context),
        ),
      ]);
    }

    result = WillPopScope(
      onWillPop: () => _onWillPop(context, model),
      child: result,
    );

    return result;
  }

  Widget _buildTitle(BuildContext context, NoteDetailsModel model) {
    _titleController.setTextIfChanged(model.note.title);

    Widget result = Padding(
      padding: EdgeInsets.only(
        bottom: Dimens.grid_1x,
        left: Dimens.grid_7_5x,
        right: Dimens.grid_3x,
      ),
      child: TextField(
        controller: _titleController,
        focusNode: _titleFocus,
        enabled: model.isEditing,
        autofocus: model.isEditing,
        maxLines: null,
        style: Theme.of(context).textTheme.headline5,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: S.of(context).common_addTitle,
        ),
        textInputAction: TextInputAction.next,
        textCapitalization: TextCapitalization.sentences,
        onChanged: (String text) => viewModel.onEditTitle(text),
        onSubmitted: (String text) => _noteFocus.requestFocus(),
      ),
    );

    if (!model.isEditing) {
      result = Semantics(
        label: S.of(context).common_title + ': ' + model.note.title,
        child: result,
      );
    }

    return result;
  }

  Widget _buildNote(BuildContext context, NoteDetailsModel model) {
    _noteController.setTextIfChanged(model.note.note);

    Widget result = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            Images.ic_description_24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_1_5x, vertical: Dimens.grid_0_5x),
            child: TextField(
              controller: _noteController,
              focusNode: _noteFocus,
              enabled: model.isEditing,
              style: Theme.of(context).textTheme.bodyText1,
              maxLines: null,
              textInputAction: TextInputAction.newline,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: S.of(context).common_addNote,
              ),
              onChanged: (String text) => viewModel.onEditNote(text),
            ),
          ),
        ),
      ],
    );

    if (!model.isEditing) {
      result = Semantics(
        label: S.of(context).common_note + ': ' + model.note.note,
        child: result,
      );
    }

    return result;
  }

  Widget _buildNoteItems(BuildContext context, NoteDetailsModel model) {
    for (var item in model.note.items) {
      _obtainNoteItemController(item.id).setTextIfChanged(item.text);
    }

    return AnimatedDiffUtilList(
      primary: false,
      shrinkWrap: true,
      padding: EdgeInsets.only(
        top: Dimens.grid_1x,
        bottom: model.isEditing ? 0 : Dimens.grid_1x,
      ),
      items: model.note.items,
      itemBuilder: (BuildContext context, List<NoteItem> items, int index) {
        return _buildNoteItem(context, model, items[index], items.getOrNull(index + 1));
      },
      equalityChecker: (NoteItem first, NoteItem second) {
        return first.id == second.id;
      },
    );
  }

  TextEditingController _obtainNoteItemController(String id) {
    var controller = _noteItemsController[id] ?? TextEditingController();
    _noteItemsController[id] = controller;
    return controller;
  }

  Widget _buildNoteItem(BuildContext context, NoteDetailsModel model, NoteItem item, NoteItem nextItem) {
    final Widget checkbox = MergeSemantics(
      child: Padding(
        padding: EdgeInsets.fromLTRB(Dimens.grid_3x, Dimens.grid_1x, Dimens.grid_1_5x, Dimens.grid_1x),
        child: SizedBox(
          width: Dimens.checkboxWidth,
          height: Dimens.checkboxHeight,
          child: Checkbox(
            value: item.isCompleted,
            onChanged: (bool newState) => viewModel.onEditNoteItem(item.id, isCompleted: newState),
          ),
        ),
      ),
    );

    Widget textField = TextField(
      controller: _obtainNoteItemController(item.id),
      focusNode: _obtainNoteItemFocus(item.id),
      enabled: model.isEditing,
      textInputAction: TextInputAction.next,
      style: Theme.of(context).textTheme.bodyText1.copyWith(
            decoration: item.isCompleted && !model.isEditing ? TextDecoration.lineThrough : TextDecoration.none,
          ),
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: S.of(context).common_yourListItem,
      ),
      onChanged: (String text) {
        viewModel.onEditNoteItem(item.id, text: text);
      },
      onSubmitted: (String text) async {
        if (nextItem != null) {
          _obtainNoteItemFocus(nextItem.id).requestFocus();
        } else {
          // catch focus on the current item to prevent keyboard collapse
          _obtainNoteItemFocus(item.id).requestFocus();

          // focus next item
          final String id = await viewModel.onAddNoteItem();
          _obtainNoteItemFocus(id).requestFocus();
        }
      },
    );

    if (!model.isEditing) {
      textField = Semantics(
        label: S.of(context).common_listItem + ': ' + item.text,
        child: textField,
      );
    }

    Widget result = Row(
      key: ValueKey(item.id + 'row'),
      children: [
        checkbox,
        Expanded(child: textField),
        if (model.isEditing) _buildDeleteNoteItemButton(context, item),
      ],
    );

    if (!model.isEditing) {
      result = InkWell(
        key: ValueKey(item.id + 'ink-well'),
        onTap: () => viewModel.onEditNoteItem(item.id, isCompleted: !item.isCompleted),
        child: result,
      );
    }

    return result;
  }

  Widget _buildDeleteNoteItemButton(BuildContext context, NoteItem item) {
    return MergeSemantics(
      child: InkWell(
        onTap: () {
          _obtainNoteItemFocus(item.id).unfocus();
          viewModel.onDeleteNoteItem(item.id);
        },
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: Dimens.grid_1_5x, horizontal: Dimens.grid_3x),
          child: Image.asset(
            Images.ic_close_24,
            width: 16,
            height: 16,
            color: Theme.of(context).colorScheme.primaryVariant,
            semanticLabel: S.of(context).common_delete,
          ),
        ),
      ),
    );
  }

  FocusNode _obtainNoteItemFocus(String id) {
    var focus = _noteItemsFocus[id] ?? FocusNode();
    _noteItemsFocus[id] = focus;
    return focus;
  }

  Widget _buildAddNoteItem(BuildContext context, NoteDetailsModel model) {
    return MergeSemantics(
      child: InkWell(
        onTap: () async {
          final String id = await viewModel.onAddNoteItem();
          _obtainNoteItemFocus(id).requestFocus();
        },
        child: AnimatedContainer(
          duration: Animations.shortDuration,
          padding: EdgeInsets.only(
            left: Dimens.grid_3x,
            right: Dimens.grid_3x,
            bottom: Dimens.grid_2x,
            top: Dimens.grid_2x,
          ),
          child: Row(children: [
            Padding(
              padding: EdgeInsets.only(right: Dimens.grid_3x),
              child: Image.asset(
                Images.ic_plus_24,
                width: 24,
                height: 24,
                color: Theme.of(context).colorScheme.primaryVariant,
              ),
            ),
            Expanded(
              child: Text(
                S.of(context).common_addListItem,
                semanticsLabel: S.of(context).common_addListItem,
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                      color: Theme.of(context).colorScheme.primaryVariant,
                    ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  Widget _buildColor(BuildContext context, NoteDetailsModel model) {
    return Row(children: [
      Expanded(
        child: InkWell(
          onTap: model.isEditing ? () => _showColorPicker(context, model) : null,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
            child: Row(children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: Dimens.grid_3_5x),
                child: Container(
                  width: 16,
                  height: 16,
                  decoration: BoxDecoration(
                    color: ResourceLocator.colorEnumToColor(model.note.color),
                    border: model.note.color == null
                        ? Border.all(color: Theme.of(context).colorScheme.primaryVariant)
                        : null,
                    borderRadius: BorderRadius.circular(Dimens.grid_0_5x),
                  ),
                ),
              ),
              Expanded(
                child: Text(
                  ResourceLocator.colorEnumToString(context, model.note.color) ?? S.of(context).common_pickColor,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        color: model.note.color == null
                            ? Theme.of(context).colorScheme.primaryVariant
                            : Theme.of(context).colorScheme.primary,
                      ),
                ),
              ),
            ]),
          ),
        ),
      ),
      if (model.isEditing && model.note.color != null)
        MergeSemantics(
          child: InkWell(
            onTap: () {
              _unfocusTextFields();
              viewModel.onEditColor(null);
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x, horizontal: Dimens.grid_3x),
              child: Image.asset(
                Images.ic_close_24,
                width: 16,
                height: 16,
                color: Theme.of(context).colorScheme.primaryVariant,
                semanticLabel: S.of(context).common_delete,
              ),
            ),
          ),
        ),
    ]);
  }

  Widget _buildNotification(BuildContext context, NoteDetailsModel model) {
    return Row(children: [
      Expanded(
        child: InkWell(
          onTap: model.isEditing ? () => _showNotificationPicker(context, model) : null,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
            child: Row(children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: Dimens.grid_3x),
                child: Image.asset(
                  Images.ic_notification_24,
                  width: 24,
                  height: 24,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  excludeFromSemantics: true,
                ),
              ),
              Expanded(
                child: Text(
                  _formatNotificationText(context, model),
                  semanticsLabel: _formatNotificationSemanticLabel(context, model),
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        decoration: model.note.isNotificationExpired(DateTime.now())
                            ? TextDecoration.lineThrough
                            : TextDecoration.none,
                        color: model.note.notificationDate == null
                            ? Theme.of(context).colorScheme.primaryVariant
                            : Theme.of(context).colorScheme.primary,
                      ),
                ),
              ),
            ]),
          ),
        ),
      ),
      if (model.isEditing && model.note.notificationDate != null)
        MergeSemantics(
          child: InkWell(
            onTap: () {
              _unfocusTextFields();
              viewModel.onEditNotificationDate(null);
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x, horizontal: Dimens.grid_3x),
              child: Image.asset(
                Images.ic_close_24,
                width: 16,
                height: 16,
                color: Theme.of(context).colorScheme.primaryVariant,
                semanticLabel: S.of(context).common_delete,
              ),
            ),
          ),
        ),
    ]);
  }

  String _formatNotificationText(BuildContext context, NoteDetailsModel model) {
    if (model.note.notificationDate == null) return S.of(context).common_addNotification;
    return DateFormatter.formatRelativeOrFullDate(context, model.note.notificationDate);
  }

  String _formatNotificationSemanticLabel(BuildContext context, NoteDetailsModel model) {
    if (model.note.notificationDate == null) return S.of(context).common_addNotification;
    return '${S.of(context).common_notification}: ${_formatNotificationText(context, model)}';
  }

  void _showColorPicker(BuildContext context, NoteDetailsModel model) async {
    _unfocusTextFields();

    final color = await showColorEnumPickerDialog(context: context);

    if (color != null) {
      viewModel.onEditColor(color);
    }
  }

  void _showNotificationPicker(BuildContext context, NoteDetailsModel model) async {
    _unfocusTextFields();

    final now = DateTime.now();
    final initialDate = model.note.suggestedNotificationDate(now);
    final constraints = model.note.notificationConstraints(now: now, notificationDate: model.note.notificationDate);

    final selectedDateTime = await showAdaptiveDateTimePickerWithSuggestions(
      context: context,
      initialDate: initialDate,
      minimumDate: constraints.minimumDate,
      maximumDate: constraints.maximumDate,
    );

    if (selectedDateTime != null) {
      viewModel.onEditNotificationDate(selectedDateTime);
    }
  }

  Future<bool> _onWillPop(BuildContext context, NoteDetailsModel model) async {
    _unfocusTextFields();

    if (model.isEditing && model.discardChangesPopup) {
      _showSaveChangesDialog(context);
    } else {
      widget.onBack(context);
    }

    return false;
  }

  void _showDeleteConfirmation(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_delete,
      message: S.of(context).notes_deleteDialog_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteNote(),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }

  void _showSaveChangesDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_discardChangesQuestion,
      primaryAction: DialogActionData(
        text: S.of(context).common_keepEditing,
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_discard,
        onTap: () => widget.onBack(context),
      ),
    );
  }

  void _unfocusTextFields() {
    _titleFocus.unfocus();
    _noteFocus.unfocus();
    _noteItemsFocus.values.forEach((e) => e.unfocus());
  }
}
