import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/note/model/add_note.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/model/note_item.dart';
import 'package:school_pen/domain/note/note_listener.dart';
import 'package:school_pen/domain/note/note_manager.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';

class NoteDetailsModel extends Equatable {
  final bool isEditing;
  final bool discardChangesPopup;
  final bool progressOverlay;
  final AddNote note;

  const NoteDetailsModel({
    @required this.isEditing,
    @required this.discardChangesPopup,
    @required this.progressOverlay,
    @required this.note,
  });

  @override
  List<Object> get props => [isEditing, discardChangesPopup, progressOverlay, note];
}

enum NoteDetailsSignal {
  notifyNoteArchived,
  notifyNoteUnarchived,
  notifyNoteDeleted,
  notifyNoteSaved,
  pop,
}

class NoteDetailsViewModel extends ViewModel<NoteDetailsModel, NoteDetailsSignal> implements NoteListener {
  static const Duration _backgroundSyncDelay = Duration(seconds: 5);
  static const Logger _logger = Logger('NoteDetailsViewModel');

  final SchoolYearManager _schoolYearManager;
  final NoteManager _noteManager;
  final String _noteId;

  bool _editModeActive = false;
  bool _progressOverlay = false;
  AddNote _originalNote;
  AddNote _editedNote;
  Timer _saveChangesTimer;

  NoteDetailsViewModel(this._schoolYearManager, this._noteManager, this._noteId) {
    _editModeActive = _noteId == null;
  }

  @override
  void onStart() async {
    super.onStart();
    _noteManager.addNoteListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _noteManager.removeNoteListener(this);

    if (_saveChangesTimer != null) {
      _saveChangesTimer.cancel();
      _saveChangesTimer = null;
      _saveNoteInBackground();
    }

    super.onStop();
  }

  @override
  void onNotesChanged(NoteManager manager) async {
    await _tryEmitModel();
  }

  void onEnableEditMode() async {
    _editModeActive = true;
    await _tryEmitModel();
  }

  void onArchiveNote(bool archived) {
    _tryWithProgressOverlay(() async {
      if (_noteId == null) return;

      _editedNote = _editedNote.copyWith(isArchived: Optional(archived));
      await _noteManager.update(_noteId, _editedNote);
      emitSignal(archived ? NoteDetailsSignal.notifyNoteArchived : NoteDetailsSignal.notifyNoteUnarchived);
      if (archived) emitSignal(NoteDetailsSignal.pop);
    });
  }

  void onSaveNote() {
    _tryWithProgressOverlay(() async {
      if (_noteId != null) {
        await _noteManager.update(_noteId, _editedNote);
      } else {
        await _noteManager.create(_editedNote);
      }
      emitSignal(NoteDetailsSignal.notifyNoteSaved);
      emitSignal(NoteDetailsSignal.pop);
    });
  }

  Future<void> onEditTitle(String text) async {
    _editedNote = _editedNote.copyWith(title: Optional(text));

    await _tryEmitModel();
  }

  void onEditNote(String text) async {
    _editedNote = _editedNote.copyWith(note: Optional(text));

    await _tryEmitModel();
  }

  Future<String> onAddNoteItem() async {
    final String id = Id.random();

    _editedNote = _editedNote.copyWith(
      items: Optional([
        ..._editedNote.items,
        NoteItem(id: id, text: '', isCompleted: false),
      ]),
    );

    await _tryEmitModel();
    return id;
  }

  void onEditNoteItem(String id, {String text, bool isCompleted}) async {
    _editedNote = _editedNote.copyWith(
      items: Optional(_editedNote.items.map((NoteItem item) {
        if (item.id == id) {
          return NoteItem(id: id, text: text ?? item.text, isCompleted: isCompleted ?? item.isCompleted);
        } else {
          return item;
        }
      }).toList()),
    );

    if (isCompleted != null && !_editModeActive) {
      _saveChangesTimer?.cancel();
      _saveChangesTimer = Timer(_backgroundSyncDelay, _saveNoteInBackground);
    }

    await _tryEmitModel();
  }

  void onDeleteNoteItem(String id) async {
    _editedNote = _editedNote.copyWith(
      items: Optional(_editedNote.items.where((e) => e.id != id).toList()),
    );

    await _tryEmitModel();
  }

  void onEditColor(ColorEnum color) async {
    _editedNote = _editedNote.copyWith(color: Optional(color));

    await _tryEmitModel();
  }

  void onEditNotificationDate(DateTime notificationDate) async {
    _editedNote = _editedNote.copyWith(notificationDate: Optional(notificationDate));

    await _tryEmitModel();
  }

  void onDeleteNote() {
    _tryWithProgressOverlay(() async {
      await _noteManager.delete(_noteId);
      emitSignal(NoteDetailsSignal.notifyNoteDeleted);
      emitSignal(NoteDetailsSignal.pop);
    });
  }

  Future<void> _saveNoteInBackground() async {
    try {
      if (_noteId != null && !_editModeActive) {
        await _noteManager.update(_noteId, _editedNote);
      }
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<NoteDetailsModel> get _model async {
    final AddNote note = await _obtainNote();
    if (note == null) return null;

    return NoteDetailsModel(
      isEditing: _editModeActive,
      discardChangesPopup: _originalNote == null || note != _originalNote,
      progressOverlay: _progressOverlay,
      note: note,
    );
  }

  Future<AddNote> _obtainNote() async {
    if (_editedNote != null) return _editedNote;

    if (_noteId != null) {
      final Note note = await _noteManager.getById(_noteId);
      _originalNote = note?.toAddNote()?.copyWith(updatedAt: Optional(DateTime.now()));
      _editedNote = _originalNote;
    } else {
      final SchoolYear year = await _schoolYearManager.getActive();
      _editedNote = AddNote(
        schoolYearId: year.id,
        title: '',
        note: '',
        items: [],
        notificationDate: null,
        isArchived: false,
        updatedAt: DateTime.now(),
      );
    }

    return _editedNote;
  }
}
