import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/note/model/add_note.dart';
import 'package:school_pen/domain/note/model/note_item.dart';

class NoteShareBuilder {
  static String buildShareText(AddNote note) {
    String result = '';

    if (Strings.isNotBlank(note.title)) {
      result += note.title;
    }

    if (Strings.isNotBlank(note.note)) {
      result += '\n\n';
      result += note.note;
    }

    for (NoteItem item in note.items) {
      result += '\n - ${item.text}';
    }

    if (note.notificationDate != null) {
      result += '\n\n[${DateFormatter.formatFull(note.notificationDate)}]';
    }

    return result.trim();
  }
}
