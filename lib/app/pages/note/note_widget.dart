import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/assets/typography.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/widgets/bordered_container.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/model/note_item.dart';
import 'package:school_pen/generated/l10n.dart';

/// Displays a read-only note on a card.
class NoteWidget extends StatelessWidget {
  static final Color _fgColorOnColoredNote = Colors.white.withAlpha(240);

  final Key _titleKey = UniqueKey();
  final Key _noteKey = UniqueKey();
  final Key _footerKey = UniqueKey();
  final Key _archivedKey = UniqueKey();
  final Key _alarmKey = UniqueKey();
  final Key _attachmentsKey = UniqueKey();

  final Note note;
  final GestureTapCallback onTap;

  NoteWidget({Key key, @required this.note, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BorderedContainer(
      onTap: onTap,
      color: AppColors.applyDarkModeSaturation(context, ResourceLocator.colorEnumToColor(note.color)),
      padding: EdgeInsets.all(Dimens.grid_1_5x),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (_showTitle) _buildTitle(context),
          if (_showNote) _buildNote(context),
          ..._buildNoteItems(context),
          _buildFooter(context),
        ],
      ),
    );
  }

  bool get _showTitle => Strings.isNotBlank(note.title);

  bool get _showNote => Strings.isNotBlank(note.note);

  Widget _buildTitle(BuildContext context) {
    return Text(
      note.title,
      key: _titleKey,
      semanticsLabel: '${S.of(context).common_title}: ${note.title}',
      style: Theme.of(context).textTheme.headline6.copyWith(
            color: note.color != null ? _fgColorOnColoredNote : Theme.of(context).colorScheme.primary,
          ),
    );
  }

  Widget _buildNote(BuildContext context) {
    return Padding(
      key: _noteKey,
      padding: EdgeInsets.only(top: _showTitle ? Dimens.grid_0_5x : 0),
      child: Text(
        note.note,
        semanticsLabel: '${S.of(context).common_note}: ${note.note}',
        style: Theme.of(context).textTheme.bodyText2.copyWith(
              color: note.color != null ? _fgColorOnColoredNote : Theme.of(context).colorScheme.primary,
            ),
      ),
    );
  }

  List<Widget> _buildNoteItems(BuildContext context) {
    return note.items.mapIndexed((NoteItem item, int index) {
      double topPadding;
      if (index == 0) {
        topPadding = (_showTitle || _showNote) ? Dimens.grid_1_5x : 0;
      } else {
        topPadding = Dimens.grid_1x;
      }

      return Padding(
        key: ValueKey(item),
        padding: EdgeInsets.only(top: topPadding),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: Dimens.grid_0_5x),
              child: Image.asset(
                item.isCompleted ? Images.ic_checkbox_checked_16 : Images.ic_checkbox_unchecked_16,
                width: 16,
                height: 16,
                color: note.color != null ? _fgColorOnColoredNote : Theme.of(context).colorScheme.primary,
                excludeFromSemantics: true,
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: Dimens.grid_1_5x),
                child: Text(
                  item.text,
                  semanticsLabel: _buildNoteItemSemanticLabel(context, item),
                  style: Theme.of(context).textTheme.bodyText2.copyWith(
                        decoration: item.isCompleted ? TextDecoration.lineThrough : TextDecoration.none,
                        color: note.color != null ? _fgColorOnColoredNote : Theme.of(context).colorScheme.primary,
                      ),
                ),
              ),
            ),
          ],
        ),
      );
    });
  }

  String _buildNoteItemSemanticLabel(BuildContext context, NoteItem item) {
    final S s = S.of(context);
    final String completionSemantics = item.isCompleted ? s.common_completed : s.common_notCompleted;
    return '${s.common_listItem}: $completionSemantics - ${item.text}';
  }

  Widget _buildFooter(BuildContext context) {
    final Color color =
        note.color != null ? _fgColorOnColoredNote.withAlpha(220) : Theme.of(context).colorScheme.primaryVariant;

    return Padding(
      key: _footerKey,
      padding: EdgeInsets.only(top: Dimens.grid_2x),
      child: Row(children: [
        Expanded(
          child: Text(
            DateFormatter.formatFullRelativeOrShortDate(context, note.updatedAt),
            style: Theme.of(context).textTheme.caption2(context).copyWith(color: color),
          ),
        ),
        if (note.isArchived)
          Padding(
            padding: EdgeInsets.only(left: Dimens.grid_1x),
            key: _archivedKey,
            child: Image.asset(Images.ic_archived_12, width: 12, height: 12, color: color),
          ),
        if (note.notificationDate != null)
          Padding(
            padding: EdgeInsets.only(left: Dimens.grid_1x),
            key: _alarmKey,
            child: Image.asset(Images.ic_notification_12, width: 12, height: 12, color: color),
          ),
        // TODO attachments
        // if (note.attachments.isNotEmpty)
        // Padding(
        //   padding: EdgeInsets.only(left: Dimens.grid_1x),
        //   key: _attachmentsKey,
        //   child: Image.asset(Images.ic_attachment_12, width: 12, height: 12, color: color),
        // ),
        // TODO attachments
        // if (note.attachments.isNotEmpty)
        // Padding(
        //   padding: EdgeInsets.only(left: Dimens.grid_0_5x),
        //   child: Text(
        //     '3',
        //     style: Theme.of(context).textTheme.caption2(context).copyWith(color: color),
        //   ),
        // ),
      ]),
    );
  }
}
