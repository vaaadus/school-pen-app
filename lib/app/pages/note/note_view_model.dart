import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/note_listener.dart';
import 'package:school_pen/domain/note/note_manager.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/preferences/preferences_listener.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';

class NoteModel extends Equatable {
  final bool showEmptyState;
  final bool showArchivedNotes;
  final NoteViewMode viewMode;
  final List<Note> recentNotes;
  final List<Note> pastNotes;

  const NoteModel({
    @required this.showEmptyState,
    @required this.showArchivedNotes,
    @required this.viewMode,
    @required this.recentNotes,
    @required this.pastNotes,
  });

  @override
  List<Object> get props => [showEmptyState, showArchivedNotes, viewMode, recentNotes, pastNotes];
}

enum NoteViewMode {
  grid,
  list,
}

class NoteViewModel extends ViewModel<NoteModel, Object>
    implements NoteListener, SchoolYearListener, PreferencesListener {
  static const String _keyShowInListView = 'notePage_showInListView';
  static const String _keyShowArchivedNotes = 'notePage_showArchivedNotes';
  static const Logger _logger = Logger('NoteViewModel');
  final SchoolYearManager _schoolYearManager;
  final NoteManager _noteManager;
  final Preferences _preferences;

  NoteViewModel(this._schoolYearManager, this._noteManager, this._preferences);

  @override
  void onStart() async {
    super.onStart();
    _noteManager.addNoteListener(this, notifyOnAttach: false);
    _schoolYearManager.addSchoolYearListener(this, notifyOnAttach: false);
    _preferences.addPreferencesListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _noteManager.removeNoteListener(this);
    _schoolYearManager.removeSchoolYearListener(this);
    _preferences.removePreferencesListener(this);
    super.onStop();
  }

  @override
  void onNotesChanged(NoteManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onSchoolYearsChanged(SchoolYearManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onPreferencesChanged(Preferences preferences, List<String> changedKeys) async {
    if (changedKeys.contains(_keyShowArchivedNotes) || changedKeys.contains(_keyShowInListView)) {
      await _tryEmitModel();
    }
  }

  void onFilterArchivedNotes(bool filter) async {
    _preferences.setBool(_keyShowArchivedNotes, filter);
  }

  void onChangeViewMode(NoteViewMode viewMode) async {
    _preferences.setBool(_keyShowInListView, viewMode == NoteViewMode.list);
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<NoteModel> get _model async {
    final bool showArchivedNotes = _preferences.getBool(_keyShowArchivedNotes) ?? false;
    final bool showInListView = _preferences.getBool(_keyShowInListView) ?? false;

    final SchoolYear year = await _schoolYearManager.getActive();
    final List<Note> notes = await _noteManager.getAll(year.id);

    final List<Note> recentNotes = [];
    final List<Note> pastNotes = [];
    for (Note note in notes) {
      if (note.isArchived && !showArchivedNotes) continue;

      if (_isUpdatedLessThanDaysAgo(note, 30)) {
        recentNotes.add(note);
      } else {
        pastNotes.add(note);
      }
    }

    return NoteModel(
      showEmptyState: notes.isEmpty,
      showArchivedNotes: showArchivedNotes,
      viewMode: showInListView ? NoteViewMode.list : NoteViewMode.grid,
      recentNotes: recentNotes,
      pastNotes: pastNotes,
    );
  }

  bool _isUpdatedLessThanDaysAgo(Note note, int days) {
    return note.updatedAt.isBefore(DateTime.now().plusDays(days));
  }
}
