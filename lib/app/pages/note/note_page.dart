import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/note/details/note_details_page.dart';
import 'package:school_pen/app/pages/note/note_view_model.dart';
import 'package:school_pen/app/pages/note/note_widget.dart';
import 'package:school_pen/app/pages/picker/checkbox_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/master_detail_container.dart';
import 'package:school_pen/app/widgets/page_placeholder.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows to view/edit/create notes.
class NotePage extends LifecycleWidget {
  const NotePage({
    Key key,
    @required this.onSearchNotes,
    this.selectedNoteId,
  }) : super(key: key);

  final VoidCallback onSearchNotes;
  final String selectedNoteId;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _NotePageState();
}

class _NotePageState extends LifecycleWidgetState<NotePage>
    with ViewModelLifecycle<NoteViewModel, NotePage>, SingleTickerProviderStateMixin {
  final List<String> _consumedNotesIds = [];
  Widget _asideWidget;

  @override
  NoteViewModel provideViewModel() => NoteViewModel(Injection.get(), Injection.get(), Injection.get());

  @override
  Widget build(BuildContext context) {
    _consumeSelectedNoteId(context);

    final Widget scaffold = Scaffold(
      body: SafeArea(
        child: ViewModelBuilder(
          viewModel: viewModel,
          builder: (BuildContext context, NoteModel model) {
            if (model.showEmptyState) {
              return _buildEmptyState(context);
            } else {
              return _buildContent(context, model);
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        label: ButtonText(S.of(context).common_addNote),
        onPressed: () => _showNoteDetails(context),
      ),
    );

    return MasterDetailContainer(
      masterBuilder: (BuildContext context) => scaffold,
      detailBuilder: (BuildContext context) => _asideWidget,
    );
  }

  void _consumeSelectedNoteId(BuildContext context) {
    final String noteId = widget.selectedNoteId;
    if (noteId == null) return;

    // avoid setState() during build().
    Timer.run(() {
      if (mounted && !_consumedNotesIds.contains(noteId)) {
        _consumedNotesIds.add(noteId);
        _showNoteDetails(context, noteId);
      }
    });
  }

  Widget _buildEmptyState(BuildContext context) {
    return PagePlaceholder(
      imageAsset: Images.illustration_notebook_192,
      imageWidth: 192,
      imageHeight: 192,
      message: S.of(context).notes_emptyState_message,
    );
  }

  Widget _buildContent(BuildContext context, NoteModel model) {
    return CustomScrollView(slivers: [
      _buildToolbar(context, model),
      SliverList(
        delegate: SliverChildListDelegate([
          if (model.recentNotes.isNotEmpty)
            _buildSectionHeader(
              context: context,
              title: S.of(context).common_recent,
              topPadding: Dimens.grid_2x - Toolbar.bottomPadding,
            ),
          if (model.viewMode == NoteViewMode.grid) _buildGridView(context, model.recentNotes),
          if (model.viewMode == NoteViewMode.list) _buildListView(context, model.recentNotes),
          if (model.pastNotes.isNotEmpty)
            _buildSectionHeader(
              context: context,
              title: S.of(context).common_past,
              topPadding: Dimens.grid_4x,
            ),
          if (model.viewMode == NoteViewMode.grid) _buildGridView(context, model.pastNotes),
          if (model.viewMode == NoteViewMode.list) _buildListView(context, model.pastNotes),
          Container(height: Dimens.grid_10x),
        ]),
      ),
    ]);
  }

  Widget _buildToolbar(BuildContext context, NoteModel model) {
    final bool listView = model.viewMode == NoteViewMode.list;

    return Toolbar(
      vsync: this,
      navType: null,
      actions: [
        if (kDebugMode)
          BorderedImageButton(
            imageAsset: Images.ic_search_24,
            tooltip: S.of(context).common_search,
            onTap: widget.onSearchNotes,
          ),
        BorderedImageButton(
          imageAsset: Images.ic_filter_24,
          tooltip: S.of(context).common_filter,
          onTap: () => _showFilterOptions(context, model),
        ),
        BorderedImageButton(
          imageAsset: listView ? Images.ic_grid_view_24 : Images.ic_list_view_24,
          tooltip: listView ? S.of(context).common_gridView : S.of(context).common_listView,
          onTap: () => _changeViewMode(model),
        ),
      ],
    );
  }

  Widget _buildSectionHeader({@required BuildContext context, @required String title, @required double topPadding}) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        top: topPadding,
        left: paddings.horizontal,
        right: paddings.horizontal,
      ),
      child: _SectionHeader(text: title),
    );
  }

  Widget _buildGridView(BuildContext context, List<Note> notes) {
    return _GridView(
      notes: notes,
      onNoteTap: (Note note) => _showNoteDetails(context, note.id),
    );
  }

  Widget _buildListView(BuildContext context, List<Note> notes) {
    return _ListView(
      notes: notes,
      onNoteTap: (Note note) => _showNoteDetails(context, note.id),
    );
  }

  void _showNoteDetails(BuildContext context, [String noteId]) {
    if (MasterDetailContainer.isSupported(context)) {
      final Widget page = NoteDetailsPage(
        key: ValueKey(noteId),
        noteId: noteId,
        fullscreen: false,
        onBack: (BuildContext context) {
          setState(() => _asideWidget = null);
        },
      );

      setState(() => _asideWidget = page);
    } else if (noteId != null) {
      Nav.notes_details(noteId: noteId);
    } else {
      Nav.notes_add();
    }
  }

  void _showFilterOptions(BuildContext context, NoteModel model) async {
    final String keyShowArchivedNotes = 'showArchivedNotes';

    final Map<String, bool> result = await showCheckboxDialog(
      context: context,
      routeSettings: RouteSettings(name: R.notes_filter),
      title: S.of(context).common_filter,
      itemBuilder: (BuildContext context) => [
        CheckboxDialogItem(
          text: S.of(context).notes_filter_showArchivedNotes,
          defaultValue: model.showArchivedNotes,
          value: keyShowArchivedNotes,
        ),
      ],
    );

    if (result != null && result[keyShowArchivedNotes] != null) {
      viewModel.onFilterArchivedNotes(result[keyShowArchivedNotes]);
    }
  }

  void _changeViewMode(NoteModel model) {
    final NoteViewMode newViewMode = model.viewMode == NoteViewMode.list ? NoteViewMode.grid : NoteViewMode.list;
    viewModel.onChangeViewMode(newViewMode);
  }
}

class _SectionHeader extends StatelessWidget {
  const _SectionHeader({Key key, @required this.text}) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text.toUpperCase(),
      style: Theme.of(context).textTheme.overline.copyWith(color: Theme.of(context).colorScheme.primaryVariant),
    );
  }
}

class _GridView extends StatelessWidget {
  const _GridView({Key key, @required this.notes, @required this.onNoteTap}) : super(key: key);

  final List<Note> notes;
  final ValueChanged<Note> onNoteTap;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final int crossAxisCount = _getCrossAxisCount(constraints.maxWidth);
      final Paddings paddings = Paddings.of(context);
      return StaggeredGridView.countBuilder(
        key: ValueKey(crossAxisCount),
        primary: false,
        shrinkWrap: true,
        padding: EdgeInsets.only(
          top: Dimens.grid_2x,
          left: paddings.horizontal,
          right: paddings.horizontal,
        ),
        mainAxisSpacing: Dimens.grid_2x,
        crossAxisSpacing: Dimens.grid_2x,
        crossAxisCount: crossAxisCount,
        itemCount: notes.length,
        itemBuilder: (BuildContext context, int index) => NoteWidget(
          note: notes[index],
          onTap: () => onNoteTap(notes[index]),
        ),
        staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
      );
    });
  }

  int _getCrossAxisCount(double availableWidth) {
    return (availableWidth / _getMinColumnWidth(availableWidth)).floor();
  }

  double _getMinColumnWidth(double availableWidth) {
    if (availableWidth < 320) return 128;
    if (availableWidth < 640) return 160;
    return 200;
  }
}

class _ListView extends StatelessWidget {
  const _ListView({Key key, @required this.notes, @required this.onNoteTap}) : super(key: key);

  final List<Note> notes;
  final ValueChanged<Note> onNoteTap;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      primary: false,
      shrinkWrap: true,
      itemCount: notes.length,
      padding: EdgeInsets.symmetric(horizontal: Paddings.of(context).horizontal),
      itemBuilder: (BuildContext context, int index) => Padding(
        padding: EdgeInsets.only(top: Dimens.grid_2x),
        child: NoteWidget(
          note: notes[index],
          onTap: () => onNoteTap(notes[index]),
        ),
      ),
    );
  }
}
