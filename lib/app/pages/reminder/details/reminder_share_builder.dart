import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/reminder/model/add_reminder.dart';

class ReminderShareBuilder {
  static String buildShareText(AddReminder reminder, Course course) {
    String result = '';

    if (course != null) {
      result += course.name + ': ';
    }

    if (Strings.isNotBlank(reminder.title)) {
      result += reminder.title;
    }

    if (Strings.isNotBlank(reminder.note)) {
      result += '\n\n';
      result += reminder.note;
    }

    result += '\n\n[${DateFormatter.formatFull(reminder.dateTime)}]';

    return result.trim();
  }
}
