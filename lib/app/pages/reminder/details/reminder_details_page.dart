import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/common/intents.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/pages/reminder/details/reminder_details_view_model.dart';
import 'package:school_pen/app/pages/reminder/details/reminder_share_builder.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/save_or_discard_footer.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/generated/l10n.dart';

/// Offers the user a possibility to edit a reminder (if [reminderId] not null)
/// or create a new one if reminder is null.
class ReminderDetailsPage extends LifecycleWidget {
  final String reminderId;
  final String courseId;
  final OnBackCallback onBack;
  final bool fullscreen;

  const ReminderDetailsPage({
    Key key,
    this.reminderId,
    this.courseId,
    this.onBack = Navigator.pop,
    this.fullscreen = true,
  }) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _ReminderDetailsPageState();
}

class _ReminderDetailsPageState extends LifecycleWidgetState<ReminderDetailsPage>
    with
        ViewModelLifecycle<ReminderDetailsViewModel, ReminderDetailsPage>,
        ProgressOverlayMixin,
        SingleTickerProviderStateMixin {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _noteController = TextEditingController();

  final FocusNode _titleFocus = FocusNode();
  final FocusNode _noteFocus = FocusNode();

  @override
  ReminderDetailsViewModel provideViewModel() =>
      ReminderDetailsViewModel(Injection.get(), Injection.get(), Injection.get(), widget.reminderId, widget.courseId);

  @override
  void dispose() {
    _titleController.dispose();
    _noteController.dispose();
    _titleFocus.dispose();
    _noteFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder(
      viewModel: viewModel,
      builder: (BuildContext context, ReminderDetailsModel model) {
        updateProgressOverlay(model.progressOverlay);
        return _buildScaffold(context, model);
      },
      onSignal: (BuildContext context, ReminderDetailsSignal signal) {
        if (signal == ReminderDetailsSignal.notifyReminderArchived) {
          showSuccessToast(message: S.of(context).confirmation_archived);
        } else if (signal == ReminderDetailsSignal.notifyReminderUnarchived) {
          showSuccessToast(message: S.of(context).confirmation_unarchived);
        } else if (signal == ReminderDetailsSignal.notifyReminderDeleted) {
          showSuccessToast(message: S.of(context).confirmation_deleted);
        } else if (signal == ReminderDetailsSignal.notifyReminderSaved) {
          showSuccessToast(message: S.of(context).confirmation_saveSuccess);
        } else if (signal == ReminderDetailsSignal.pop) {
          widget.onBack(context);
        }
      },
    );
  }

  Widget _buildScaffold(BuildContext context, ReminderDetailsModel model) {
    Widget result = Scaffold(
      body: SafeArea(
        child: _buildContent(context, model),
      ),
    );

    if (widget.fullscreen) {
      final ColorScheme colors = Theme.of(context).colorScheme;
      result = SystemUiOverlayAnnotation(
        child: result,
        systemNavigationBarColor: (model.isEditing || !model.reminder.isArchived) ? colors.surface : colors.background,
      );
    }

    return result;
  }

  Widget _buildContent(BuildContext context, ReminderDetailsModel model) {
    Widget result = CustomScrollView(slivers: [
      Toolbar(
        vsync: this,
        navType: NavButtonType.close,
        horizontalPadding: Dimens.grid_3x,
        onBack: (BuildContext context) => _onWillPop(context, model),
        actions: [
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => viewModel.onEnableEditMode(),
              imageAsset: Images.ic_edit_pen_24,
              tooltip: S.of(context).common_edit,
            ),
          if (!model.isEditing && Intents.isShareTextSupported())
            BorderedImageButton(
              onTap: () => Intents.shareText(ReminderShareBuilder.buildShareText(
                model.reminder.toModel(),
                model.selectedCourse,
              )),
              imageAsset: Images.ic_share_24,
              tooltip: S.of(context).common_share,
            ),
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => viewModel.onArchiveReminder(!model.reminder.isArchived),
              imageAsset: model.reminder.isArchived ? Images.ic_unarchive_24 : Images.ic_archive_24,
              tooltip: model.reminder.isArchived ? S.of(context).common_unarchive : S.of(context).common_archive,
            ),
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => _showDeleteConfirmation(context),
              imageAsset: Images.ic_trash_24,
              tooltip: S.of(context).common_delete,
            ),
        ],
      ),
      SliverList(
        delegate: SliverChildListDelegate([
          Container(height: Dimens.grid_3x - Toolbar.bottomPadding),
          if (Strings.isNotBlank(model.reminder.title) || model.isEditing) ...[
            _buildTitle(context, model),
            Divider(),
          ],
          if (Strings.isNotBlank(model.reminder.note) || model.isEditing) ...[
            _buildNote(context, model),
            Divider(),
          ],
          if (model.selectedCourse != null || model.isEditing) ...[
            _buildCourse(context, model),
            Divider(),
          ],
          _buildDateTime(context, model),
          Divider(),
          Container(height: Paddings.of(context).vertical),
        ]),
      ),
    ]);

    if (model.isEditing) {
      result = Column(children: [
        Expanded(child: result),
        SaveOrDiscardFooter(
          label: S.of(context).common_reminder,
          onSave: () => viewModel.onSaveReminder(),
          onDiscard: () => widget.onBack(context),
        ),
      ]);
    } else if (!model.reminder.isArchived) {
      result = Column(children: [
        Expanded(child: result),
        SaveOrDiscardFooter(
          label: S.of(context).common_reminder,
          saveText: S.of(context).common_done,
          onSave: () => viewModel.onArchiveReminder(true),
        ),
      ]);
    }

    result = WillPopScope(
      onWillPop: () => _onWillPop(context, model),
      child: result,
    );

    return result;
  }

  Widget _buildTitle(BuildContext context, ReminderDetailsModel model) {
    _titleController.setTextIfChanged(model.reminder.title);

    Widget result = Padding(
      padding: EdgeInsets.only(
        bottom: Dimens.grid_1x,
        left: Dimens.grid_7_5x,
        right: Dimens.grid_3x,
      ),
      child: TextField(
        controller: _titleController,
        focusNode: _titleFocus,
        enabled: model.isEditing,
        autofocus: model.isEditing,
        maxLines: null,
        style: Theme.of(context).textTheme.headline5,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: S.of(context).common_addTitle,
        ),
        textInputAction: TextInputAction.next,
        textCapitalization: TextCapitalization.sentences,
        onChanged: (String text) => viewModel.onEditTitle(text),
        onSubmitted: (String text) => _noteFocus.requestFocus(),
      ),
    );

    if (!model.isEditing) {
      result = Semantics(
        label: S.of(context).common_title + ': ' + model.reminder.title,
        child: result,
      );
    }

    return result;
  }

  Widget _buildNote(BuildContext context, ReminderDetailsModel model) {
    _noteController.setTextIfChanged(model.reminder.note);

    Widget result = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            Images.ic_description_24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_1_5x, vertical: Dimens.grid_0_5x),
            child: TextField(
              controller: _noteController,
              focusNode: _noteFocus,
              enabled: model.isEditing,
              style: Theme.of(context).textTheme.bodyText1,
              maxLines: null,
              textInputAction: TextInputAction.newline,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: S.of(context).common_addNote,
              ),
              onChanged: (String text) => viewModel.onEditNote(text),
            ),
          ),
        ),
      ],
    );

    if (!model.isEditing) {
      result = Semantics(
        label: S.of(context).common_note + ': ' + model.reminder.note,
        child: result,
      );
    }

    return result;
  }

  Widget _buildCourse(BuildContext context, ReminderDetailsModel model) {
    final String addCourseId = 'add-course-id';

    return MergeSemantics(
      child: PopupMenuButton(
        enabled: model.isEditing,
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          for (Course course in model.availableCourses)
            PopupMenuItem(
              value: course.id,
              child: Text(course.name),
            ),
          PopupMenuDivider(),
          PopupMenuItem(
            value: addCourseId,
            child: Text(
              S.of(context).common_addCourse,
              style: Theme.of(context).popupMenuTheme.textStyle.copyWith(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
            ),
          ),
        ],
        onSelected: (String id) async {
          _unfocusTextFields();

          if (id == addCourseId) {
            final String courseId = await Nav.courses_add();
            if (courseId != null) viewModel.onEditCourse(courseId);
          } else {
            viewModel.onEditCourse(id);
          }
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                top: Dimens.grid_2x,
                bottom: Dimens.grid_2x,
                left: Dimens.grid_3x,
              ),
              child: Image.asset(
                Images.ic_book_24,
                width: 24,
                height: 24,
                color: Theme.of(context).colorScheme.primaryVariant,
                excludeFromSemantics: true,
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(
                  top: Dimens.grid_2x,
                  bottom: Dimens.grid_2x,
                  left: Dimens.grid_3x,
                  right: Dimens.grid_3x,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      model.selectedCourse?.name ?? S.of(context).common_pickCourse,
                      style: Theme.of(context).textTheme.bodyText1.copyWith(
                            color: model.selectedCourse?.name != null
                                ? Theme.of(context).colorScheme.primary
                                : Theme.of(context).colorScheme.primaryVariant,
                          ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDateTime(BuildContext context, ReminderDetailsModel model) {
    return InkWell(
      onTap: model.isEditing ? () => _showDateTimePicker(context, model) : null,
      child: Row(children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
            child: Row(children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: Dimens.grid_3x),
                child: Image.asset(
                  Images.ic_timetable_24,
                  width: 24,
                  height: 24,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  excludeFromSemantics: true,
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      model.reminder.dateTime != null
                          ? DateFormatter.formatRelativeOrFullDate(context, model.reminder.dateTime)
                          : S.of(context).common_pickDateAndTime,
                      style: Theme.of(context).textTheme.bodyText1.copyWith(
                            color: model.reminder.dateTime == null
                                ? Theme.of(context).colorScheme.primaryVariant
                                : Theme.of(context).colorScheme.primary,
                          ),
                    ),
                    if (model.dateTimeError != null)
                      Padding(
                        padding: EdgeInsets.only(top: Dimens.grid_1x),
                        child: Text(
                          ResourceLocator.errorToText(context, model.dateTimeError),
                          style: Theme.of(context).textTheme.caption.copyWith(
                                color: Theme.of(context).colorScheme.error,
                              ),
                        ),
                      ),
                  ],
                ),
              ),
            ]),
          ),
        ),
      ]),
    );
  }

  void _showDateTimePicker(BuildContext context, ReminderDetailsModel model) async {
    _unfocusTextFields();

    final DateTime dateTime = await showAdaptiveDateTimePicker(
      context: context,
      initialDate: model.reminder.dateTime ?? DateTime.now().add(Duration(days: 1)).withMinutes(0),
    );

    if (dateTime != null) {
      viewModel.onEditDateTime(dateTime);
    }
  }

  Future<bool> _onWillPop(BuildContext context, ReminderDetailsModel model) async {
    _unfocusTextFields();

    if (model.isEditing && model.discardChangesPopup) {
      _showSaveChangesDialog(context);
    } else {
      widget.onBack(context);
    }

    return false;
  }

  void _showDeleteConfirmation(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_delete,
      message: S.of(context).reminders_deleteDialog_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteReminder(),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }

  void _showSaveChangesDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_discardChangesQuestion,
      primaryAction: DialogActionData(
        text: S.of(context).common_keepEditing,
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_discard,
        onTap: () => widget.onBack(context),
      ),
    );
  }

  void _unfocusTextFields() {
    _titleFocus.unfocus();
    _noteFocus.unfocus();
  }
}
