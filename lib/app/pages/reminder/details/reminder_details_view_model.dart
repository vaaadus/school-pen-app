import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/reminder/model/add_reminder.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/reminder/reminder_listener.dart';
import 'package:school_pen/domain/reminder/reminder_manager.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';

class ReminderDetailsModel extends Equatable {
  final bool isEditing;
  final bool discardChangesPopup;
  final bool progressOverlay;
  final AddReminderInput reminder;
  final Exception dateTimeError;
  final List<Course> availableCourses;
  final Course selectedCourse;

  const ReminderDetailsModel({
    @required this.isEditing,
    @required this.discardChangesPopup,
    @required this.progressOverlay,
    @required this.reminder,
    this.dateTimeError,
    @required this.availableCourses,
    this.selectedCourse,
  });

  @override
  List<Object> get props => [
        isEditing,
        discardChangesPopup,
        progressOverlay,
        reminder,
        dateTimeError,
        availableCourses,
        selectedCourse,
      ];
}

enum ReminderDetailsSignal {
  notifyReminderArchived,
  notifyReminderUnarchived,
  notifyReminderDeleted,
  notifyReminderSaved,
  pop,
}

class ReminderDetailsViewModel extends ViewModel<ReminderDetailsModel, ReminderDetailsSignal>
    implements ReminderListener, CourseListener {
  static const Logger _logger = Logger('ReminderDetailsViewModel');

  final SemesterManager _semesterManager;
  final ReminderManager _reminderManager;
  final CourseManager _courseManager;
  final String _reminderId;
  final String _preselectedCourseId;

  bool _editModeActive = false;
  bool _progressOverlay = false;
  AddReminderInput _originalReminder;
  AddReminderInput _editedReminder;
  Exception _dateTimeError;

  ReminderDetailsViewModel(
      this._semesterManager, this._reminderManager, this._courseManager, this._reminderId, this._preselectedCourseId) {
    _editModeActive = _reminderId == null;
  }

  @override
  void onStart() async {
    super.onStart();
    _reminderManager.addReminderListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _reminderManager.removeReminderListener(this);
    _courseManager.removeCourseListener(this);
    super.onStop();
  }

  @override
  void onRemindersChanged(ReminderManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  void onEnableEditMode() async {
    _editModeActive = true;
    await _tryEmitModel();
  }

  void onArchiveReminder(bool archived) {
    _tryWithProgressOverlay(() async {
      if (_reminderId == null) return;

      _editedReminder = _editedReminder.copyWith(isArchived: Optional(archived));
      await _reminderManager.update(_reminderId, _editedReminder?.toModel());
      emitSignal(
          archived ? ReminderDetailsSignal.notifyReminderArchived : ReminderDetailsSignal.notifyReminderUnarchived);
      if (archived) emitSignal(ReminderDetailsSignal.pop);
    });
  }

  void onSaveReminder() {
    _tryWithProgressOverlay(() async {
      if (!_validate(_editedReminder)) return;

      if (_reminderId != null) {
        await _reminderManager.update(_reminderId, _editedReminder?.toModel());
      } else {
        await _reminderManager.create(_editedReminder?.toModel());
      }
      emitSignal(ReminderDetailsSignal.notifyReminderSaved);
      emitSignal(ReminderDetailsSignal.pop);
    });
  }

  Future<void> onEditTitle(String text) async {
    _editedReminder = _editedReminder.copyWith(title: Optional(text));

    await _tryEmitModel();
  }

  void onEditCourse(String courseId) async {
    _editedReminder = _editedReminder.copyWith(courseId: Optional(courseId));

    await _tryEmitModel();
  }

  void onEditDateTime(DateTime dateTime) async {
    _editedReminder = _editedReminder.copyWith(dateTime: Optional(dateTime));

    await _tryEmitModel();
  }

  void onEditNote(String text) async {
    _editedReminder = _editedReminder.copyWith(note: Optional(text));

    await _tryEmitModel();
  }

  void onDeleteReminder() {
    _tryWithProgressOverlay(() async {
      await _reminderManager.delete(_reminderId);
      emitSignal(ReminderDetailsSignal.notifyReminderDeleted);
      emitSignal(ReminderDetailsSignal.pop);
    });
  }

  bool _validate(AddReminderInput reminder) {
    _dateTimeError = null;

    if (reminder.dateTime == null) {
      _dateTimeError = FieldMustNotBeEmptyException();
      return false;
    }

    return true;
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<ReminderDetailsModel> get _model async {
    final AddReminderInput reminder = await _obtainReminder();
    if (reminder == null) return null;

    final Semester semester = await _semesterManager.getById(reminder.semesterId);
    final List<Course> courses = await _courseManager.getAll(semester.schoolYearId);

    return ReminderDetailsModel(
      isEditing: _editModeActive,
      discardChangesPopup: _originalReminder == null || reminder != _originalReminder,
      progressOverlay: _progressOverlay,
      reminder: reminder,
      dateTimeError: _dateTimeError,
      availableCourses: courses.where((e) => e.id != reminder.courseId).toList(),
      selectedCourse: courses.firstWhereOrNull((e) => e.id == reminder.courseId),
    );
  }

  Future<AddReminderInput> _obtainReminder() async {
    if (_editedReminder != null) return _editedReminder;

    if (_reminderId != null) {
      final Reminder reminder = await _reminderManager.getById(_reminderId);
      final AddReminder addReminder = reminder?.toAddReminder()?.copyWith(updatedAt: Optional(DateTime.now()));
      _originalReminder = addReminder != null ? AddReminderInput.from(addReminder) : null;
      _editedReminder = _originalReminder;
    } else {
      final Semester semester = await _semesterManager.getActive();
      _editedReminder = AddReminderInput(
        semesterId: semester.id,
        courseId: _preselectedCourseId,
        title: '',
        note: '',
        dateTime: null,
        isArchived: false,
        updatedAt: DateTime.now(),
      );
    }

    return _editedReminder;
  }
}

class AddReminderInput extends Equatable {
  final String semesterId;
  final String courseId;
  final String title;
  final String note;
  final DateTime dateTime;
  final bool isArchived;
  final DateTime updatedAt;

  const AddReminderInput({
    @required this.semesterId,
    this.courseId,
    this.title,
    this.note,
    this.dateTime,
    @required this.isArchived,
    @required this.updatedAt,
  });

  factory AddReminderInput.from(AddReminder reminder) {
    return AddReminderInput(
      semesterId: reminder.semesterId,
      courseId: reminder.courseId,
      title: reminder.title,
      note: reminder.note,
      dateTime: reminder.dateTime,
      isArchived: reminder.isArchived,
      updatedAt: reminder.updatedAt,
    );
  }

  AddReminder toModel() {
    return AddReminder(
      semesterId: semesterId,
      courseId: courseId,
      title: title,
      note: note,
      dateTime: dateTime ?? DateTime.now(),
      isArchived: isArchived,
      updatedAt: updatedAt,
    );
  }

  AddReminderInput copyWith({
    Optional<String> semesterId,
    Optional<String> courseId,
    Optional<String> title,
    Optional<String> note,
    Optional<DateTime> dateTime,
    Optional<bool> isArchived,
    Optional<DateTime> updatedAt,
  }) {
    return AddReminderInput(
      semesterId: Optional.unwrapOrElse(semesterId, this.semesterId),
      courseId: Optional.unwrapOrElse(courseId, this.courseId),
      title: Optional.unwrapOrElse(title, this.title),
      note: Optional.unwrapOrElse(note, this.note),
      dateTime: Optional.unwrapOrElse(dateTime, this.dateTime),
      isArchived: Optional.unwrapOrElse(isArchived, this.isArchived),
      updatedAt: Optional.unwrapOrElse(updatedAt, this.updatedAt),
    );
  }

  @override
  List<Object> get props => [semesterId, courseId, title, note, dateTime, isArchived, updatedAt];
}
