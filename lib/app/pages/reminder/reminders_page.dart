import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/reminder/reminders_view_model.dart';
import 'package:school_pen/app/widgets/bordered_list_item.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows to view/edit/create reminders.
class RemindersPage extends LifecycleWidget {
  const RemindersPage({
    Key key,
    this.courseId,
    this.fullscreen = true,
    this.onBack = Navigator.pop,
  }) : super(key: key);

  final String courseId;
  final bool fullscreen;
  final OnBackCallback onBack;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _RemindersPageState();
}

class _RemindersPageState extends LifecycleWidgetState<RemindersPage>
    with ViewModelLifecycle<RemindersViewModel, RemindersPage>, ProgressOverlayMixin, SingleTickerProviderStateMixin {
  @override
  RemindersViewModel provideViewModel() =>
      RemindersViewModel(Injection.get(), Injection.get(), Injection.get(), widget.courseId);

  @override
  Widget build(BuildContext context) {
    Widget result = Scaffold(
      body: SafeArea(
        child: ViewModelBuilder(
          viewModel: viewModel,
          builder: _buildContent,
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        label: ButtonText(S.of(context).common_addReminder),
        onPressed: () => Nav.reminders_add(courseId: widget.courseId),
      ),
    );

    if (widget.fullscreen) {
      result = SystemUiOverlayAnnotation(child: result);
    }

    return result;
  }

  Widget _buildContent(BuildContext context, RemindersModel model) {
    return CustomScrollView(slivers: [
      Toolbar(
        pinned: !widget.fullscreen || model.showEmptyState,
        vsync: this,
        onBack: widget.onBack,
        navType: widget.fullscreen ? NavButtonType.back : NavButtonType.close,
        title: _getTitle(context, model),
      ),
      if (model.showEmptyState)
        SliverFillRemaining(
          child: _buildEmptyState(context),
        ),
      if (!model.showEmptyState)
        SliverPadding(
          padding: EdgeInsets.only(bottom: Dimens.grid_10x),
          sliver: SliverList(
            delegate: SliverChildListDelegate([
              if (model.overdueReminders.isNotEmpty) _buildErrorHeader(context, S.of(context).common_overdue),
              for (ReminderData data in model.overdueReminders) _buildListItem(context, model, data),
              if (model.activeReminders.isNotEmpty) _buildHeader(context, S.of(context).common_active),
              for (ReminderData data in model.activeReminders) _buildListItem(context, model, data),
              if (model.archivedReminders.isNotEmpty) _buildHeader(context, S.of(context).common_archived),
              for (ReminderData data in model.archivedReminders) _buildListItem(context, model, data),
            ]),
          ),
        ),
    ]);
  }

  String _getTitle(BuildContext context, RemindersModel model) {
    String title = '';
    if (widget.fullscreen && model.course != null) title += model.course.name + ' / ';
    title += S.of(context).common_reminders;
    return title;
  }

  Widget _buildEmptyState(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(Dimens.grid_3x),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(Images.illustration_empty_box_128, width: 128, height: 128),
            Padding(
              padding: EdgeInsets.only(top: Dimens.grid_1_5x),
              child: Text(
                S.of(context).common_noActiveEvents,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline6.copyWith(
                      color: Theme.of(context).colorScheme.primaryVariant,
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildErrorHeader(BuildContext context, String text) {
    return _buildHeader(context, text + ' ⚠', color: Theme.of(context).colorScheme.error);
  }

  Widget _buildHeader(BuildContext context, String text, {Color color}) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        left: paddings.horizontal,
        right: paddings.horizontal,
        top: Dimens.grid_4x,
      ),
      child: Text(
        text.toUpperCase(),
        style: Theme.of(context).textTheme.overline.copyWith(
              color: color ?? Theme.of(context).colorScheme.primaryVariant,
            ),
      ),
    );
  }

  Widget _buildListItem(BuildContext context, RemindersModel model, ReminderData data) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        top: Dimens.grid_2x,
        left: paddings.horizontal,
        right: paddings.horizontal,
      ),
      child: BorderedListItem(
        data: _reminderAsListData(context, model, data),
        onTap: () => Nav.reminders_details(reminderId: data.reminder.id),
      ),
    );
  }

  BorderedListItemData _reminderAsListData(BuildContext context, RemindersModel model, ReminderData data) {
    return BorderedListItemData(
      title: data.reminder.title,
      label: _formatLabel(context, data),
    );
  }

  String _formatLabel(BuildContext context, ReminderData data) {
    String label = '';
    if (widget.courseId == null && data.course != null) label += data.course.name + ' · ';
    label += DateFormat.MMMMd().format(data.reminder.dateTime);
    return label;
  }
}
