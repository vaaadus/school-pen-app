import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/reminder/reminder_listener.dart';
import 'package:school_pen/domain/reminder/reminder_manager.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_listener.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';

class RemindersModel extends Equatable {
  final bool showEmptyState;
  final Course course;
  final List<ReminderData> overdueReminders;
  final List<ReminderData> activeReminders;
  final List<ReminderData> archivedReminders;

  const RemindersModel({
    @required this.showEmptyState,
    this.course,
    @required this.overdueReminders,
    @required this.activeReminders,
    @required this.archivedReminders,
  });

  @override
  List<Object> get props => [showEmptyState, course, overdueReminders, activeReminders, archivedReminders];
}

class ReminderData extends Equatable {
  final Reminder reminder;
  final Course course;

  const ReminderData(this.reminder, this.course);

  @override
  List<Object> get props => [reminder, course];
}

class RemindersViewModel extends ViewModel<RemindersModel, Object>
    implements SemesterListener, ReminderListener, CourseListener {
  static const Logger _logger = Logger('ReminderViewModel');

  final SemesterManager _semesterManager;
  final ReminderManager _reminderManager;
  final CourseManager _courseManager;
  final String _courseId;

  RemindersViewModel(this._semesterManager, this._reminderManager, this._courseManager, this._courseId);

  @override
  void onStart() async {
    super.onStart();
    _semesterManager.addSemesterListener(this, notifyOnAttach: false);
    _reminderManager.addReminderListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);

    await _tryEmitModel();
  }

  @override
  void onStop() {
    _semesterManager.removeSemesterListener(this);
    _reminderManager.removeReminderListener(this);
    _courseManager.removeCourseListener(this);
    super.onStop();
  }

  @override
  void onSemestersChanged(SemesterManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onRemindersChanged(ReminderManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<RemindersModel> get _model async {
    final Semester semester = await _semesterManager.getActive();
    final Course course = await _courseManager.getById(_courseId);
    final List<ReminderData> reminders = await _fetchReminders(semester.schoolYearId, semester.id);
    final DateTime today = DateTime.now().withoutTime();

    return RemindersModel(
      showEmptyState: reminders.isEmpty,
      course: course,
      overdueReminders: reminders.where((e) => !e.reminder.isArchived && e.reminder.dateTime.isBefore(today)).toList(),
      activeReminders:
          reminders.where((e) => !e.reminder.isArchived && e.reminder.dateTime.isSameDateAs(today)).toList(),
      archivedReminders: reminders.where((e) => e.reminder.isArchived).toList(),
    );
  }

  Future<List<ReminderData>> _fetchReminders(String schoolYearId, String semesterId) async {
    final List<Reminder> reminders = await _reminderManager.getAll(semesterId: semesterId);
    final List<Reminder> selectedReminders =
        reminders.where((e) => e.courseId == _courseId || _courseId == null).toList();
    final List<Course> courses = await _courseManager.getAll(schoolYearId);

    final List<ReminderData> result = [];
    for (Reminder reminder in selectedReminders) {
      final Course course = courses.firstWhereOrNull((e) => e.id == reminder.courseId);
      result.add(ReminderData(reminder, course));
    }

    return result;
  }
}
