import 'dart:async';

import 'package:flutter/material.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/course/course_page.dart';
import 'package:school_pen/app/pages/course/details/course_details_page.dart';
import 'package:school_pen/app/pages/gateway/desktop_gateway_scaffold.dart';
import 'package:school_pen/app/pages/gateway/mobile_gateway_scaffold.dart';
import 'package:school_pen/app/pages/gateway/tablet_gateway_scaffold.dart';
import 'package:school_pen/app/pages/home/home_page.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/note/note_page.dart';
import 'package:school_pen/app/pages/people/teachers/teacher_page.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/pages/settings/settings_page.dart';
import 'package:school_pen/app/pages/timetable/timetable_page.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/domain/analytics/analytics.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/notification/local_notification_listener.dart';
import 'package:school_pen/domain/notification/model/local_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/quickactions/model/quick_actions.dart';
import 'package:school_pen/domain/quickactions/quick_actions_listener.dart';
import 'package:school_pen/domain/quickactions/quick_actions_manager.dart';

import 'gateway_page_enum.dart';

class GatewayPage extends StatefulWidget {
  const GatewayPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _GatewayPageState();
}

class _GatewayPageState extends State<GatewayPage> implements LocalNotificationListener, QuickActionsListener {
  final _GatewayPageStack _stack = _GatewayPageStack(initial: GatewayPageEnum.home, onPageChanged: _trackScreenName);
  final NotificationManager _notifications = Injection.get();
  final QuickActionsManager _quickActions = Injection.get();

  String _selectedNoteId;
  String _selectedCourseId;
  bool _showFriendsAsPeople = false;
  Widget _body;

  @override
  void initState() {
    super.initState();
    _notifications.addNotificationListener(this);
    _quickActions.addQuickActionsListener(this);

    // Run in timer to avoid setState() when the widget tree is not stable.
    Timer.run(() {
      if (mounted) ToastOverlay.init(context);
    });
  }

  @override
  void dispose() {
    _notifications.removeNotificationListener(this);
    _quickActions.removeQuickActionsListener(this);
    super.dispose();
  }

  @override
  void onLocalNotification(LocalNotification notification) {
    switch (notification.channel) {
      case NotificationChannel.note:
        _selectedNoteId = notification.payload.objectId;
        _onChangePage(GatewayPageEnum.notes);
        break;
      case NotificationChannel.exam:
        Nav.exams_details(examId: notification.payload.objectId);
        break;
      case NotificationChannel.homework:
        Nav.homework_details(homeworkId: notification.payload.objectId);
        break;
      case NotificationChannel.reminder:
        Nav.reminders_details(reminderId: notification.payload.objectId);
        break;
      case NotificationChannel.agenda:
        _onChangePage(GatewayPageEnum.home);
        break;
      case NotificationChannel.upcomingLessons:
        _onChangePage(GatewayPageEnum.timetable);
        break;
    }
  }

  @override
  void onQuickAction(QuickAction action) {
    switch (action) {
      case QuickAction.addReminder:
        Nav.reminders_add();
        break;
      case QuickAction.addHomework:
        Nav.homework_add();
        break;
      case QuickAction.addExam:
        Nav.exams_add();
        break;
      case QuickAction.addNote:
        Nav.notes_add();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AdaptiveWidgetBuilder(
      mobile: _buildMobileLayout,
      tablet: _buildTabletLayout,
      desktop: _buildDesktopLayout,
    );
  }

  Widget _buildMobileLayout(BuildContext context) {
    return MobileGatewayScaffold(
      body: _getBody(context),
      selectedPage: _selectedPage,
      onChangePage: _onChangePage,
      onPopPage: _onPopPage,
    );
  }

  Widget _buildTabletLayout(BuildContext context) {
    return TabletGatewayScaffold(
      body: _getBody(context),
      selectedPage: _selectedPage,
      onChangePage: _onChangePage,
      onPopPage: _onPopPage,
    );
  }

  Widget _buildDesktopLayout(BuildContext context) {
    return DesktopGatewayScaffold(
      body: _getBody(context),
      selectedPage: _selectedPage,
      onChangePage: _onChangePage,
      onPopPage: _onPopPage,
    );
  }

  Widget _getBody(BuildContext context) {
    _body ??= _buildBody(context);
    return _body;
  }

  Widget _buildBody(BuildContext context) {
    switch (_selectedPage) {
      case GatewayPageEnum.home:
        return HomePage(
          onManageTimetables: () => _onChangePage(GatewayPageEnum.timetable),
          onSearch: () => _onChangePage(GatewayPageEnum.search),
          onViewCourses: () => _onChangePage(GatewayPageEnum.courses),
        );
      case GatewayPageEnum.courses:
        if (_selectedCourseId != null) {
          return CourseDetailsPage(
            courseId: _selectedCourseId,
            onBack: (BuildContext context) => _rebuildWith(() => _selectedCourseId = null),
          );
        }
        return CoursePage(
          onCourseSelected: (Course course) => _rebuildWith(() => _selectedCourseId = course.id),
        );
      case GatewayPageEnum.notes:
        // TODO: preselect notes filter on search page by default
        return NotePage(
          onSearchNotes: () => _onChangePage(GatewayPageEnum.search),
          selectedNoteId: _consumeSelectedNoteId(),
        );
      case GatewayPageEnum.timetable:
        return TimetablePage();
      case GatewayPageEnum.attendance:
        // TODO: Handle this case.
        break;
      case GatewayPageEnum.people:
        return TeacherPage(onSelectFriends: () {
          _rebuildWith(() => _showFriendsAsPeople = true);
        });
      // TODO: else show friends
      case GatewayPageEnum.files:
        // TODO: Handle this case.
        break;
      case GatewayPageEnum.holidays:
        // TODO: Handle this case.
        break;
      case GatewayPageEnum.search:
        // TODO: Handle this case.
        break;
      case GatewayPageEnum.settings:
        return SettingsPage();
    }

    // TODO remove and throw exception
    return Padding(
      padding: EdgeInsets.only(top: 100, bottom: 100, left: 32, right: 32),
      child: Column(
        children: [
          Text('Welcome'),
          Padding(
            padding: EdgeInsets.only(top: 16.0),
            child: Text('The google_fonts package will automatically use matching '
                'font files in your pubspec.yamls assets (rather than '
                'fetching them at runtime via HTTP). Once youve settled '
                'on the fonts you want to use:'),
          ),
        ],
      ),
    );
  }

  GatewayPageEnum get _selectedPage => _stack.peek();

  bool _onPopPage(GatewayPageEnum page) {
    if (page == GatewayPageEnum.courses && _selectedCourseId != null) {
      _rebuildWith(() => _selectedCourseId = null);
      return false;
    }

    if (page != _selectedPage) return true;
    if (_stack.length == 1) return true;

    _rebuildWith(() {
      _stack.pop();
    });

    return false;
  }

  void _onChangePage(GatewayPageEnum page, [VoidCallback callback]) {
    _rebuildWith(() {
      callback?.call();
      _stack.push(page);
    });
  }

  void _rebuildWith(VoidCallback callback) {
    setState(() {
      _body = null;
      callback();
    });
  }

  String _consumeSelectedNoteId() {
    final String noteId = _selectedNoteId;
    _selectedNoteId = null;
    return noteId;
  }

  static void _trackScreenName(GatewayPageEnum page) {
    Analytics.setScreenName(_mapScreenName(page));
  }

  static String _mapScreenName(GatewayPageEnum page) {
    switch (page) {
      case GatewayPageEnum.home:
        return R.gateway_home;
      case GatewayPageEnum.courses:
        return R.gateway_courses;
      case GatewayPageEnum.notes:
        return R.gateway_notes;
      case GatewayPageEnum.timetable:
        return R.gateway_timetable;
      case GatewayPageEnum.attendance:
        return R.gateway_attendance;
      case GatewayPageEnum.people:
        return R.gateway_people;
      case GatewayPageEnum.files:
        return R.gateway_files;
      case GatewayPageEnum.holidays:
        return R.gateway_holidays;
      case GatewayPageEnum.search:
        return R.gateway_search;
      case GatewayPageEnum.settings:
        return R.gateway_settings;
    }

    throw Exception('Unsupported page: $page');
  }
}

/// Manages the navigation stack. Allows for up to 2 pages to
/// be in the history, the 3rd one is the current page.
class _GatewayPageStack {
  static const int _stackSize = 3;
  final List<GatewayPageEnum> _stack = [];
  final ValueChanged<GatewayPageEnum> onPageChanged;

  _GatewayPageStack({@required GatewayPageEnum initial, @required this.onPageChanged}) {
    push(initial);
  }

  void push(GatewayPageEnum page) {
    if (_stack.isNotEmpty && _stack.last == page) return;
    if (_stack.length >= _stackSize) _stack.removeAt(0);
    _stack.add(page);
    onPageChanged(page);
  }

  GatewayPageEnum pop() {
    if (_stack.isEmpty) return null;
    final GatewayPageEnum result = _stack.removeLast();
    if (_stack.isNotEmpty) {
      onPageChanged(_stack.first);
    }
    return result;
  }

  GatewayPageEnum peek() {
    if (_stack.isEmpty) return null;
    return _stack.last;
  }

  int get length => _stack.length;
}
