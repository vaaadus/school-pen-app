import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/generated/l10n.dart';

typedef OnChangeGatewayPage = void Function(GatewayPageEnum page);
typedef OnPopGatewayPage = bool Function(GatewayPageEnum page);

enum GatewayPageEnum {
  home,
  courses,
  notes,
  timetable,
  attendance,
  people,
  files,
  holidays,
  search,
  settings,
}

extension GatewayPageEnumExt on GatewayPageEnum {
  static List<GatewayPageEnum> get mobilePages {
    return [
      GatewayPageEnum.home,
      GatewayPageEnum.timetable,
      GatewayPageEnum.courses,
      GatewayPageEnum.notes,
      if (kDebugMode) GatewayPageEnum.attendance,
      GatewayPageEnum.people,
      if (kDebugMode) GatewayPageEnum.files,
      if (kDebugMode) GatewayPageEnum.holidays,
      if (kDebugMode) GatewayPageEnum.search,
      GatewayPageEnum.settings,
    ];
  }

  static List<GatewayPageEnum> get tabletPages {
    return [
      GatewayPageEnum.home,
      GatewayPageEnum.timetable,
      GatewayPageEnum.notes,
      GatewayPageEnum.courses,
      if (kDebugMode) GatewayPageEnum.attendance,
      GatewayPageEnum.people,
      if (kDebugMode) GatewayPageEnum.files,
      if (kDebugMode) GatewayPageEnum.holidays,
      if (kDebugMode) GatewayPageEnum.search,
      GatewayPageEnum.settings,
    ];
  }

  static List<GatewayPageEnum> get desktopPages {
    return tabletPages;
  }

  static String imageAsset36(GatewayPageEnum page) {
    switch (page) {
      case GatewayPageEnum.home:
        return Images.ic_home_36;
      case GatewayPageEnum.courses:
        return Images.ic_book_36;
      case GatewayPageEnum.notes:
        return Images.ic_idea_36;
      case GatewayPageEnum.timetable:
        return Images.ic_timetable_36;
      case GatewayPageEnum.attendance:
        return Images.ic_calendar_check_36;
      case GatewayPageEnum.people:
        return Images.ic_people_36;
      case GatewayPageEnum.files:
        return Images.ic_folder_36;
      case GatewayPageEnum.holidays:
        return Images.ic_plane_36;
      case GatewayPageEnum.search:
        return Images.ic_search_36;
      case GatewayPageEnum.settings:
        return Images.ic_settings_36;
    }
    throw ArgumentError('Unsupported page: $page');
  }

  static String imageAsset24(GatewayPageEnum page) {
    switch (page) {
      case GatewayPageEnum.home:
        return Images.ic_home_24;
      case GatewayPageEnum.courses:
        return Images.ic_book_24;
      case GatewayPageEnum.notes:
        return Images.ic_idea_24;
      case GatewayPageEnum.timetable:
        return Images.ic_timetable_24;
      case GatewayPageEnum.attendance:
        return Images.ic_calendar_check_24;
      case GatewayPageEnum.people:
        return Images.ic_people_24;
      case GatewayPageEnum.files:
        return Images.ic_folder_24;
      case GatewayPageEnum.holidays:
        return Images.ic_plane_24;
      case GatewayPageEnum.search:
        return Images.ic_search_24;
      case GatewayPageEnum.settings:
        return Images.ic_settings_24;
    }
    throw ArgumentError('Unsupported page: $page');
  }

  static String label(BuildContext context, GatewayPageEnum page) {
    switch (page) {
      case GatewayPageEnum.home:
        return S.of(context).nav_home;
      case GatewayPageEnum.courses:
        return S.of(context).nav_courses;
      case GatewayPageEnum.notes:
        return S.of(context).nav_notes;
      case GatewayPageEnum.timetable:
        return S.of(context).nav_timetable;
      case GatewayPageEnum.attendance:
        return S.of(context).nav_attendance;
      case GatewayPageEnum.people:
        return S.of(context).nav_people;
      case GatewayPageEnum.files:
        return S.of(context).nav_files;
      case GatewayPageEnum.holidays:
        return S.of(context).nav_holidays;
      case GatewayPageEnum.search:
        return S.of(context).nav_search;
      case GatewayPageEnum.settings:
        return S.of(context).nav_settings;
    }
    throw ArgumentError('Unsupported page: $page');
  }
}
