import 'dart:math';
import 'dart:ui';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/anim/animations.dart';
import 'package:school_pen/app/common/anim/translate_tween.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/generated/l10n.dart';

import 'gateway_page_enum.dart';

/// A widget which holds a bottom navigation and the current page.
class MobileGatewayScaffold extends StatefulWidget {
  const MobileGatewayScaffold({
    Key key,
    @required this.body,
    @required this.selectedPage,
    @required this.onChangePage,
    @required this.onPopPage,
  }) : super(key: key);

  final Widget body;
  final GatewayPageEnum selectedPage;
  final OnChangeGatewayPage onChangePage;
  final OnPopGatewayPage onPopPage;

  @override
  State<StatefulWidget> createState() => _MobileGatewayScaffoldState();
}

class _MobileGatewayScaffoldState extends State<MobileGatewayScaffold> with TickerProviderStateMixin {
  static final CurveTween animationTween = CurveTween(curve: Curves.fastOutSlowIn);

  AnimationController _expandController;
  AnimationController _scaleController;
  AnimationController _bodyFadeController;
  Widget _previousBody;

  @override
  void initState() {
    super.initState();
    _expandController = AnimationController(
      vsync: this,
      duration: Animations.mediumDuration,
    );

    _scaleController = AnimationController(
      vsync: this,
      lowerBound: Animations.scaleOnClickBounds.lower,
      upperBound: Animations.scaleOnClickBounds.upper,
      duration: Animations.microDuration,
    );

    _bodyFadeController = AnimationController(
      vsync: this,
      duration: Animations.microDuration,
    );

    _scaleController.complete();
    _bodyFadeController.complete();
  }

  @override
  void didUpdateWidget(MobileGatewayScaffold oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.body != widget.body) {
      _previousBody = oldWidget.body;
      _animateNextBody();
    }
  }

  @override
  void dispose() {
    _expandController.dispose();
    _scaleController.dispose();
    _bodyFadeController.dispose();
    _previousBody = null;
    super.dispose();
  }

  void _animateNextBody() {
    _bodyFadeController.reset();
    _bodyFadeController.forward();

    AnimationStatusListener _listener;
    _listener = (AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _bodyFadeController.removeStatusListener(_listener);
        setState(() {
          _previousBody = null;
        });
      }
    };

    _bodyFadeController.addStatusListener(_listener);
  }

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.landscape) {
      SystemChrome.setEnabledSystemUIOverlays([]);
    } else {
      SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    }

    return SystemUiOverlayAnnotation(
      systemNavigationBarColor: Theme.of(context).colorScheme.surface,
      child: Scaffold(
        body: SafeArea(
          child: WillPopScope(
            onWillPop: () async => _onWillPop(),
            child: AnimatedBuilder(
              animation: _expandController,
              builder: (BuildContext context, Widget child) {
                if (MediaQuery.of(context).orientation == Orientation.portrait) {
                  return _buildVerticalLayout(context);
                } else {
                  return _buildHorizontalLayout(context);
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildVerticalLayout(BuildContext context) {
    final Widget bottomNav = _MobileGatewayVerticalNav(
      animationProgress: _animationProgress,
      scaleAnimation: _scaleController,
      onToggle: _onToggleNav,
      selectedPage: widget.selectedPage,
      onChangePage: _onChangePage,
    );

    return Stack(
      children: [
        Positioned.fill(bottom: _MobileGatewayVerticalNav.collapsedHeight, child: _buildBody()),
        if (!_expandController.isDismissed) Positioned.fill(child: _buildBarrier()),
        Positioned(bottom: 0, left: 0, right: 0, child: bottomNav),
      ],
    );
  }

  Widget _buildHorizontalLayout(BuildContext context) {
    final Widget bottomNav = _MobileGatewayHorizontalNav(
      animationProgress: _animationProgress,
      scaleAnimation: _scaleController,
      onToggle: _onToggleNav,
      selectedPage: widget.selectedPage,
      onChangePage: _onChangePage,
    );

    return Stack(
      children: [
        Positioned.fill(left: _MobileGatewayHorizontalNav.collapsedWidth, child: _buildBody()),
        if (!_expandController.isDismissed) Positioned.fill(child: _buildBarrier()),
        Positioned(top: 0, bottom: 0, left: 0, child: bottomNav),
      ],
    );
  }

  Widget _buildBody() {
    return Stack(children: [
      if (_previousBody != null)
        Positioned.fill(
          key: ObjectKey(_previousBody),
          child: FadeTransition(opacity: ReverseAnimation(_bodyFadeController), child: _previousBody),
        ),
      Positioned.fill(
        key: ObjectKey(widget.body),
        child: FadeTransition(opacity: _bodyFadeController, child: widget.body),
      ),
    ]);
  }

  Widget _buildBarrier() {
    return GestureDetector(
      onTap: () => _expandController.reverse(),
      child: Container(
        color: Color.lerp(AppColors.transparent, AppColors.barrierColor, _animationProgress),
      ),
    );
  }

  double get _animationProgress {
    return animationTween.transform(_expandController.value);
  }

  bool _onWillPop() {
    if (_expandController.isCompleted || _expandController.isAnimating) {
      _expandController.reverse();
      return false;
    }

    return widget.onPopPage(widget.selectedPage);
  }

  void _onToggleNav() {
    if (_expandController.isCompleted) {
      _expandController.reverse();
    } else {
      _expandController.forward();
    }
  }

  void _onChangePage(GatewayPageEnum page) {
    if (_expandController.isDismissed) {
      _scaleController.complete();
      _scaleController.reverseAndForward();
    }

    _expandController.reverse();
    widget.onChangePage(page);
  }
}

class _MobileGatewayVerticalNav extends StatelessWidget {
  static const double collapsedHeight = 56.0;
  static const double collapsedLeftPadding = 8.0;
  static const double collapsedRightPadding = 8.0;
  static const double expandedLeftPadding = 24.0;
  static const double expandedRightPadding = 24.0;
  static const int columnsInRow = 5;
  static const double collapsedBottomPadding = 4.0;
  static const double expandedBottomPadding = 40.0;
  static const double expandedTopPadding = 16.0;
  static const double expandedMiniBottomPadding = 16.0;
  static const double collapsedItemHeight = 44.0;
  static const double expandedMiniItemHeight = 36.0;
  static const double expandedItemHeight = 48.0;
  static final int itemsCount = GatewayPageEnumExt.mobilePages.length;

  const _MobileGatewayVerticalNav({
    Key key,
    @required this.selectedPage,
    @required this.animationProgress,
    @required this.onToggle,
    @required this.onChangePage,
    @required this.scaleAnimation,
  }) : super(key: key);

  final GatewayPageEnum selectedPage;
  final double animationProgress;
  final GestureTapCallback onToggle;
  final OnChangeGatewayPage onChangePage;
  final Animation<double> scaleAnimation;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    final Widget toggle = Positioned(
      top: _lerp(7.0, 16.0),
      right: collapsedRightPadding,
      width: _collapsedItemWidth(context),
      child: _MenuToggle(
        startAngle: 0,
        endAngle: -pi,
        animationProgress: animationProgress,
        isActive: selectedPage.index >= GatewayPageEnum.attendance.index,
        onToggle: onToggle,
      ),
    );

    final List<_VerticalPosition> positions = [
      _VerticalPosition(col: 0, row: 0),
      _VerticalPosition(col: 1, row: 1),
      _VerticalPosition(col: 2, row: 2),
      _VerticalPosition(col: 3, row: 3),
      _VerticalPosition(col: 5, row: 4),
      _VerticalPosition(col: 6, row: 5),
      _VerticalPosition(col: 7, row: 6),
      _VerticalPosition(col: 8, row: 7),
      _VerticalPosition(col: 9, row: 8),
      _VerticalPosition(col: 10, row: 9),
    ];

    final List<GatewayPageEnum> supportedPages = GatewayPageEnumExt.mobilePages;

    final Widget stack = Stack(
      clipBehavior: Clip.none,
      children: [
        for (int i = 0; i < supportedPages.length; i++)
          _buildMenuItem(context: context, page: supportedPages[i], col: positions[i].col, row: positions[i].row),
        toggle,
      ],
    );

    return Container(
      height: _lerp(collapsedHeight, _expandedHeight(context)),
      decoration: BoxDecoration(
        color: theme.dividerColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(_lerp(0.0, 16.0)),
          topRight: Radius.circular(_lerp(0.0, 16.0)),
        ),
      ),
      child: Container(
        margin: EdgeInsetsDirectional.only(top: 1),
        decoration: BoxDecoration(
          color: theme.colorScheme.surface,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(_lerp(0, 15.0)),
            topRight: Radius.circular(_lerp(0, 15.0)),
          ), // BorderRadius
        ),
        child: stack,
      ),
    );
  }

  Positioned _buildMenuItem({
    @required BuildContext context,
    @required GatewayPageEnum page,
    @required int col,
    @required int row,
  }) {
    final double startBottom = collapsedBottomPadding;
    final double endBottomPadding = _useMiniHeight(context) ? expandedMiniBottomPadding : expandedBottomPadding;
    final double endItemHeight = _useMiniHeight(context) ? expandedMiniItemHeight : expandedItemHeight;
    final double endBottom = endBottomPadding + (itemsCount - 1 - row) * endItemHeight;

    final double startLeft = collapsedLeftPadding + col * _collapsedItemWidth(context);
    final double endLeft = expandedLeftPadding;

    final double verticalAnimStart = row / itemsCount;
    final double verticalValue = TranslateTween(start: verticalAnimStart, end: 1.0).transform(animationProgress);

    final double horizontalAnimEnd = max(verticalAnimStart, 0.1);
    final double horizontalValue = TranslateTween(start: 0.0, end: horizontalAnimEnd).transform(animationProgress);

    final double startWidth = _collapsedItemWidth(context);
    final double endWidth = MediaQuery.of(context).size.width - expandedLeftPadding - expandedRightPadding;

    Widget result = InkWell(
      onTap: () => onChangePage(page),
      child: _MenuItem(
        imageAsset: GatewayPageEnumExt.imageAsset24(page),
        label: GatewayPageEnumExt.label(context, page),
        isActive: selectedPage == page,
        animationProgress: verticalValue,
        animateOpacity: row >= (columnsInRow - 1),
      ),
    );

    if (selectedPage == page) {
      result = ScaleTransition(
        scale: scaleAnimation,
        child: result,
      );
    }

    return Positioned(
      bottom: animationProgress > verticalAnimStart ? lerpDouble(startBottom, endBottom, verticalValue) : startBottom,
      left: lerpDouble(startLeft, endLeft, horizontalValue),
      width: _lerp(startWidth, endWidth),
      height: _lerp(collapsedItemHeight, expandedItemHeight),
      child: result,
    );
  }

  double _lerp(double start, double end) {
    return lerpDouble(start, end, animationProgress);
  }

  double _collapsedItemWidth(BuildContext context) {
    return (MediaQuery.of(context).size.width - collapsedLeftPadding - collapsedRightPadding) / columnsInRow;
  }

  bool _useMiniHeight(BuildContext context) {
    return MediaQuery.of(context).size.height < _fullHeight;
  }

  double _expandedHeight(BuildContext context) {
    if (_useMiniHeight(context)) {
      return _miniHeight;
    } else {
      return _fullHeight;
    }
  }

  double get _fullHeight {
    return expandedTopPadding + expandedBottomPadding + itemsCount * expandedItemHeight;
  }

  double get _miniHeight {
    return expandedTopPadding + expandedMiniBottomPadding + itemsCount * expandedMiniItemHeight;
  }
}

class _VerticalPosition extends Equatable {
  final int col;
  final int row;

  const _VerticalPosition({@required this.col, @required this.row});

  @override
  List<Object> get props => [col, row];
}

class _MobileGatewayHorizontalNav extends StatelessWidget {
  static const double collapsedWidth = 72.0;
  static const double expandedWidth = 480.0;
  static const double collapsedTopPadding = 24.0;
  static const double collapsedBottomPadding = 24.0;
  static const int rowsInColumn = 5;
  static const double expandedRightPadding = 24.0;
  static const double expandedTopPadding = 12.0;
  static const double collapsedItemWidth = 72.0;
  static const double expandedItemWidth = 200.0;
  static const double collapsedItemHeight = 44.0;
  static const double expandedItemHeight = 48.0;
  static final int itemsCount = GatewayPageEnumExt.mobilePages.length;

  const _MobileGatewayHorizontalNav({
    Key key,
    @required this.selectedPage,
    @required this.animationProgress,
    @required this.onToggle,
    @required this.onChangePage,
    @required this.scaleAnimation,
  }) : super(key: key);

  final GatewayPageEnum selectedPage;
  final double animationProgress;
  final GestureTapCallback onToggle;
  final OnChangeGatewayPage onChangePage;
  final Animation<double> scaleAnimation;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    final Widget toggle = Positioned(
      bottom: collapsedBottomPadding,
      right: 0,
      width: collapsedItemWidth,
      height: collapsedItemHeight,
      child: _MenuToggle(
        startAngle: 0.5 * pi,
        endAngle: 1.5 * pi,
        animationProgress: animationProgress,
        isActive: selectedPage.index >= GatewayPageEnum.attendance.index,
        onToggle: onToggle,
      ),
    );

    final List<_HorizontalPosition> positions = [
      _HorizontalPosition(startRow: 0, endCol: 0, endRow: 0),
      _HorizontalPosition(startRow: 1, endCol: 0, endRow: 1),
      _HorizontalPosition(startRow: 2, endCol: 0, endRow: 2),
      _HorizontalPosition(startRow: 3, endCol: 0, endRow: 3),
      _HorizontalPosition(startRow: 5, endCol: 0, endRow: 4),
      _HorizontalPosition(startRow: 6, endCol: 1, endRow: 0),
      _HorizontalPosition(startRow: 7, endCol: 1, endRow: 1),
      _HorizontalPosition(startRow: 8, endCol: 1, endRow: 2),
      _HorizontalPosition(startRow: 9, endCol: 1, endRow: 3),
      _HorizontalPosition(startRow: 10, endCol: 1, endRow: 4),
    ];

    final List<GatewayPageEnum> supportedPages = GatewayPageEnumExt.mobilePages;

    final Widget stack = Stack(
      clipBehavior: Clip.none,
      children: [
        for (int i = 0; i < supportedPages.length; i++)
          _buildMenuItem(
            context: context,
            page: supportedPages[i],
            startRow: positions[i].startRow,
            endCol: positions[i].endCol,
            endRow: positions[i].endRow,
          ),
        toggle,
      ],
    );

    return Container(
      width: _lerp(collapsedWidth, expandedWidth),
      decoration: BoxDecoration(
        color: theme.dividerColor,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(_lerp(0.0, 16.0)),
          bottomRight: Radius.circular(_lerp(0.0, 16.0)),
        ),
      ),
      child: Container(
        margin: EdgeInsetsDirectional.only(end: 1),
        decoration: BoxDecoration(
          color: theme.colorScheme.surface,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(_lerp(0, 15.0)),
            bottomRight: Radius.circular(_lerp(0, 15.0)),
          ), // BorderRadius
        ),
        child: stack,
      ),
    );
  }

  Positioned _buildMenuItem({
    @required BuildContext context,
    @required GatewayPageEnum page,
    @required int startRow,
    @required int endCol,
    @required int endRow,
  }) {
    final double heightAvailableForDividers = MediaQuery.of(context).size.height -
        collapsedTopPadding -
        collapsedBottomPadding -
        collapsedItemHeight * rowsInColumn;

    final double dividerHeight = heightAvailableForDividers / (rowsInColumn - 1);

    final double startTop = collapsedTopPadding + (collapsedItemHeight + dividerHeight) * startRow;

    final double endTopPadding = expandedTopPadding;
    final double endItemHeight = expandedItemHeight;
    final double endTop = endTopPadding + endRow * endItemHeight;

    final double startRight = 0;
    final double endRight = expandedRightPadding + endCol * expandedItemWidth;

    final double verticalAnimStart = endRow / itemsCount;
    final double verticalValue = TranslateTween(start: verticalAnimStart, end: 1.0).transform(animationProgress);

    final double horizontalAnimEnd = max(verticalAnimStart, 0.1);
    final double horizontalValue = TranslateTween(start: 0.0, end: horizontalAnimEnd).transform(animationProgress);

    Widget result = InkWell(
      onTap: () => onChangePage(page),
      child: _MenuItem(
        imageAsset: GatewayPageEnumExt.imageAsset24(page),
        label: GatewayPageEnumExt.label(context, page),
        isActive: selectedPage == page,
        animationProgress: verticalValue,
        animateOpacity: startRow >= (rowsInColumn - 1),
      ),
    );

    if (selectedPage == page) {
      result = ScaleTransition(
        scale: scaleAnimation,
        child: result,
      );
    }

    return Positioned(
      top: animationProgress > verticalAnimStart ? lerpDouble(startTop, endTop, verticalValue) : startTop,
      right: lerpDouble(startRight, endRight, horizontalValue),
      width: _lerp(collapsedItemWidth, expandedItemWidth),
      height: _lerp(collapsedItemHeight, expandedItemHeight),
      child: result,
    );
  }

  double _lerp(double start, double end) {
    return lerpDouble(start, end, animationProgress);
  }
}

class _HorizontalPosition extends Equatable {
  final int startRow;
  final int endCol;
  final int endRow;

  const _HorizontalPosition({@required this.startRow, @required this.endCol, @required this.endRow});

  @override
  List<Object> get props => [startRow, endCol, endRow];
}

class _MenuItem extends StatelessWidget {
  static const double imageSize = 24.0;
  static const double textCollapsedTopPadding = 4.0;
  static const double textExpandedLeftPadding = 24.0;
  static const TranslateTween opacityTransform = TranslateTween(start: 0.2, end: 0.7);
  static const TranslateTween alignTransform = TranslateTween(start: 0.0, end: 0.1);
  static const TranslateTween textBoundsTransform = TranslateTween(start: 0.3, end: 0.6);
  static const TranslateTween textSizeTransform = TranslateTween(start: 0.4, end: 0.9);

  const _MenuItem({
    Key key,
    @required this.imageAsset,
    @required this.label,
    @required this.isActive,
    @required this.animationProgress,
    this.animateOpacity = false,
  }) : super(key: key);

  final String imageAsset;
  final String label;
  final bool isActive;
  final double animationProgress;
  final bool animateOpacity;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final TextTheme textTheme = theme.textTheme;

    final Widget image = Align(
      alignment: Alignment.lerp(
        Alignment.topCenter,
        Alignment.centerLeft,
        alignTransform.transform(animationProgress),
      ),
      child: Image.asset(
        imageAsset,
        width: imageSize,
        height: imageSize,
        color: isActive ? theme.colorScheme.secondary : theme.unselectedWidgetColor,
      ),
    );

    final Widget text = Align(
      alignment: Alignment.lerp(
        Alignment.bottomCenter,
        Alignment.centerLeft,
        alignTransform.transform(animationProgress),
      ),
      child: Text(
        label,
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.center,
        style: TextStyle.lerp(
          textTheme.caption,
          textTheme.headline6,
          textSizeTransform.transform(animationProgress),
        ).copyWith(color: isActive ? theme.colorScheme.secondary : theme.unselectedWidgetColor),
      ),
    );

    Widget result = Stack(
      clipBehavior: Clip.none,
      children: [
        Positioned(child: image, top: 0, bottom: 0, left: 0, right: 0),
        Positioned(
          child: text,
          left: lerpDouble(0, imageSize + textExpandedLeftPadding, textBoundsTransform.transform(animationProgress)),
          right: 0,
          top: lerpDouble(imageSize + textCollapsedTopPadding, 0, textBoundsTransform.transform(animationProgress)),
          bottom: 0,
        ),
      ],
    );

    if (animateOpacity) {
      result = Opacity(
        opacity: opacityTransform.transform(animationProgress),
        child: result,
      );
    }

    return result;
  }
}

class _MenuToggle extends StatelessWidget {
  const _MenuToggle({
    Key key,
    @required this.startAngle,
    @required this.endAngle,
    @required this.animationProgress,
    @required this.isActive,
    @required this.onToggle,
  }) : super(key: key);

  final double startAngle;
  final double endAngle;
  final double animationProgress;
  final bool isActive;
  final GestureTapCallback onToggle;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final Color color = isActive ? theme.colorScheme.secondary : theme.unselectedWidgetColor;

    return InkWell(
      onTap: onToggle,
      child: Column(children: [
        Transform.rotate(
          angle: lerpDouble(startAngle, endAngle, animationProgress),
          child: Image.asset(
            Images.ic_chevron_up_24,
            width: _MenuItem.imageSize,
            height: _MenuItem.imageSize,
            color: color,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: _MenuItem.textCollapsedTopPadding),
          child: Text(
            animationProgress < 0.5 ? S.of(context).common_more : S.of(context).common_less,
            style: theme.textTheme.caption.copyWith(color: color),
            textAlign: TextAlign.center,
          ),
        ),
      ]),
    );
  }
}
