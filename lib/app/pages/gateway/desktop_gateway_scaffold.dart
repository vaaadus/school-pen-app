import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';

import 'gateway_page_enum.dart';

/// A widget which holds a rail navigation and the current page.
class DesktopGatewayScaffold extends StatelessWidget {
  const DesktopGatewayScaffold({
    Key key,
    @required this.body,
    @required this.selectedPage,
    @required this.onChangePage,
    @required this.onPopPage,
  }) : super(key: key);

  final Widget body;
  final GatewayPageEnum selectedPage;
  final OnChangeGatewayPage onChangePage;
  final OnPopGatewayPage onPopPage;

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      statusBarColor: Theme.of(context).colorScheme.surface,
      child: Scaffold(
        body: SafeArea(
          child: WillPopScope(
            onWillPop: () async => onPopPage(selectedPage),
            child: Row(children: [
              _NavigationRail(selectedPage: selectedPage, onChangePage: onChangePage),
              Expanded(child: body),
            ]),
          ),
        ),
      ),
    );
  }
}

class _NavigationRail extends StatelessWidget {
  const _NavigationRail({
    Key key,
    @required this.selectedPage,
    @required this.onChangePage,
  }) : super(key: key);

  final GatewayPageEnum selectedPage;
  final OnChangeGatewayPage onChangePage;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final ThemeData theme = Theme.of(context);
      return Container(
        width: 280,
        height: constraints.maxHeight,
        decoration: BoxDecoration(
          color: theme.colorScheme.surface,
          border: Border(right: BorderSide(color: theme.dividerColor)),
        ),
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: Dimens.grid_4x),
          children: GatewayPageEnumExt.desktopPages.map((page) {
            return _MenuItem(
              page: page,
              onChangePage: onChangePage,
              isSelected: selectedPage == page,
            );
          }).toList(),
        ),
      );
    });
  }
}

class _MenuItem extends StatelessWidget {
  const _MenuItem({
    Key key,
    @required this.page,
    @required this.onChangePage,
    @required this.isSelected,
  }) : super(key: key);

  final GatewayPageEnum page;
  final OnChangeGatewayPage onChangePage;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final double height = MediaQuery.of(context).size.height;

    final Color color = isSelected ? theme.colorScheme.secondary : theme.unselectedWidgetColor;
    return Material(
      color: AppColors.transparent,
      child: InkWell(
        onTap: () => onChangePage(page),
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Dimens.grid_4x,
            vertical: height > 700 ? Dimens.grid_2x : Dimens.grid_1_5x,
          ),
          child: Row(
            children: [
              Image.asset(GatewayPageEnumExt.imageAsset36(page), color: color, width: 28, height: 28),
              Padding(
                padding: EdgeInsets.only(left: Dimens.grid_4x),
                child: Text(
                  GatewayPageEnumExt.label(context, page),
                  style: theme.textTheme.headline6.copyWith(color: color),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
