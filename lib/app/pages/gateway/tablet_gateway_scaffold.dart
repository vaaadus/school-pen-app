import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/common/anim/animations.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';

import 'gateway_page_enum.dart';

/// A widget which holds a rail navigation and the current page.
class TabletGatewayScaffold extends StatelessWidget {
  const TabletGatewayScaffold({
    Key key,
    @required this.body,
    @required this.selectedPage,
    @required this.onChangePage,
    @required this.onPopPage,
  }) : super(key: key);

  final Widget body;
  final GatewayPageEnum selectedPage;
  final OnChangeGatewayPage onChangePage;
  final OnPopGatewayPage onPopPage;

  @override
  Widget build(BuildContext context) {
    return SystemUiOverlayAnnotation(
      statusBarColor: Theme.of(context).colorScheme.surface,
      child: Scaffold(
        body: SafeArea(
          child: WillPopScope(
            onWillPop: () async => onPopPage(selectedPage),
            child: Row(children: [
              _NavigationRail(selectedPage: selectedPage, onChangePage: onChangePage),
              Expanded(child: body),
            ]),
          ),
        ),
      ),
    );
  }
}

class _NavigationRail extends StatefulWidget {
  const _NavigationRail({
    Key key,
    @required this.selectedPage,
    @required this.onChangePage,
  }) : super(key: key);

  final GatewayPageEnum selectedPage;
  final OnChangeGatewayPage onChangePage;

  @override
  State<StatefulWidget> createState() => _NavigationRailState();
}

class _NavigationRailState extends State<_NavigationRail> with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      lowerBound: Animations.scaleOnClickBounds.lower,
      upperBound: Animations.scaleOnClickBounds.upper,
      duration: Animations.microDuration,
    );

    _controller.complete();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final ThemeData theme = Theme.of(context);
      return Container(
        width: 96,
        height: constraints.maxHeight,
        decoration: BoxDecoration(
          color: theme.colorScheme.surface,
          border: Border(right: BorderSide(color: theme.dividerColor)),
        ),
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
          children: GatewayPageEnumExt.tabletPages.map(_buildMenuItem).toList(),
        ),
      );
    });
  }

  Widget _buildMenuItem(GatewayPageEnum page) {
    final bool isSelected = widget.selectedPage == page;

    Widget result = _MenuItem(
      page: page,
      onChangePage: (GatewayPageEnum page) {
        _controller.complete();
        _controller.reverseAndForward();
        widget.onChangePage(page);
      },
      isSelected: isSelected,
    );

    if (isSelected) {
      result = ScaleTransition(child: result, scale: _controller);
    }

    return result;
  }
}

class _MenuItem extends StatelessWidget {
  const _MenuItem({
    Key key,
    @required this.page,
    @required this.onChangePage,
    @required this.isSelected,
  }) : super(key: key);

  final GatewayPageEnum page;
  final OnChangeGatewayPage onChangePage;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final bool useMiniHeight = MediaQuery.of(context).size.height < 770;
    final Color color = isSelected ? theme.colorScheme.secondary : theme.unselectedWidgetColor;

    return Material(
      color: AppColors.transparent,
      child: InkWell(
        onTap: () => onChangePage(page),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: useMiniHeight ? Dimens.grid_1x : Dimens.grid_1_5x),
          child: Column(
            children: [
              Image.asset(GatewayPageEnumExt.imageAsset24(page), color: color, width: 24, height: 24),
              Padding(
                padding: EdgeInsets.only(top: useMiniHeight ? Dimens.grid_0_5x : Dimens.grid_1x),
                child: Text(
                  GatewayPageEnumExt.label(context, page),
                  style: theme.textTheme.caption.copyWith(color: color),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
