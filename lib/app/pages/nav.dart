import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/intents.dart';
import 'package:school_pen/app/pages/account/login/login_page.dart';
import 'package:school_pen/app/pages/account/loginoptions/login_options_page.dart';
import 'package:school_pen/app/pages/account/password/password_page.dart';
import 'package:school_pen/app/pages/account/profile/profile_page.dart';
import 'package:school_pen/app/pages/account/registration/registration_page.dart';
import 'package:school_pen/app/pages/course/edit/course_edit_page.dart';
import 'package:school_pen/app/pages/course/grades/edit/grade_edit_page.dart';
import 'package:school_pen/app/pages/course/grades/grade_page.dart';
import 'package:school_pen/app/pages/course/gradingsystem/grading_system_page.dart';
import 'package:school_pen/app/pages/exam/details/exam_details_page.dart';
import 'package:school_pen/app/pages/exam/exams_page.dart';
import 'package:school_pen/app/pages/home/notification/notification_page.dart';
import 'package:school_pen/app/pages/homework/details/homework_details_page.dart';
import 'package:school_pen/app/pages/homework/homework_page.dart';
import 'package:school_pen/app/pages/note/details/note_details_page.dart';
import 'package:school_pen/app/pages/people/teachers/details/teacher_details_page.dart';
import 'package:school_pen/app/pages/picker/event_repeat_options_page.dart';
import 'package:school_pen/app/pages/reminder/details/reminder_details_page.dart';
import 'package:school_pen/app/pages/reminder/reminders_page.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/pages/settings/about/about_page.dart';
import 'package:school_pen/app/pages/settings/credits/credits_page.dart';
import 'package:school_pen/app/pages/settings/notifications/notifications_page.dart';
import 'package:school_pen/app/pages/settings/privacy/privacy_page.dart';
import 'package:school_pen/app/pages/settings/schoolyear/school_years_page.dart';
import 'package:school_pen/app/pages/settings/semester/semester_page.dart';
import 'package:school_pen/app/pages/settings/support/support_page.dart';
import 'package:school_pen/app/pages/settings/themes/themes_page.dart';
import 'package:school_pen/app/pages/timetable/event/timetable_event_page.dart';
import 'package:school_pen/app/pages/timetable/subscribe/school/select_school_page.dart';
import 'package:school_pen/app/pages/timetable/subscribe/timetable/select_timetable_page.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/scaffold_dialog.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';

/// Aggregates pages (screens) and manages the global navigation flow.
/// Routes must be synchronized with routes.dart names.
class Nav {
  static final GlobalKey<NavigatorState> key = GlobalKey();

  static NavigatorState get navigator => key.currentState;

  // account

  static void loginOptions() {
    navigator.push(_pageRoute(name: R.account_loginOptions, page: LoginOptionsPage()));
  }

  static void register() {
    navigator.push(_pageRoute(name: R.account_register, page: RegistrationPage()));
  }

  static void login() {
    navigator.push(_pageRoute(name: R.account_login, page: LoginPage()));
  }

  static void profile() {
    navigator.push(_pageRoute(name: R.account_profile, page: ProfilePage()));
  }

  static void changePassword() {
    navigator.push(_pageRoute(name: R.account_changePassword, page: PasswordPage()));
  }

  // home

  static void home_notifications() {
    navigator.push(_pageRoute(name: R.home_notifications, page: NotificationPage()));
  }

  // courses

  /// Shows a page to add a course.
  /// Returns the courseId or null if not added.
  static Future<String> courses_add() {
    return _showAdaptivePage(name: R.courses_add, page: CourseEditPage());
  }

  static void courses_edit({@required String courseId}) {
    _showAdaptivePage(name: R.courses_edit, page: CourseEditPage(courseId: courseId));
  }

  static void courses_gradingSystem() {
    _showAdaptivePage(name: R.courses_gradingSystem, page: GradingSystemPage());
  }

  // grades

  static void grades_list({@required String courseId}) {
    navigator.push(_pageRoute(name: R.grades_list, page: GradePage(courseId: courseId)));
  }

  /// Shows a page to add a grade.
  /// Returns the gradeId or null if not added.
  static Future<String> grades_add({String courseId}) {
    return _showAdaptivePage(name: R.grades_add, page: GradeEditPage(courseId: courseId));
  }

  static void grades_edit({@required String gradeId, String courseId}) {
    _showAdaptivePage(name: R.grades_edit, page: GradeEditPage(gradeId: gradeId, courseId: courseId));
  }

  // notes

  static void notes_add() {
    navigator.push(_pageRoute(name: R.notes_add, page: NoteDetailsPage()));
  }

  static void notes_details({@required String noteId}) {
    _showAdaptivePage(name: R.notes_details, page: NoteDetailsPage(noteId: noteId));
  }

  // exams

  static void exams_add({String courseId}) {
    _showAdaptivePage(
      name: R.exams_add,
      builder: (bool dialog) => ExamDetailsPage(courseId: courseId, fullscreen: !dialog),
    );
  }

  static void exams_details({@required String examId}) {
    _showAdaptivePage(
      name: R.exams_details,
      builder: (bool dialog) => ExamDetailsPage(examId: examId, fullscreen: !dialog),
    );
  }

  static void exams_list({String courseId}) {
    navigator.push(_pageRoute(name: R.exams_list, page: ExamsPage(courseId: courseId)));
  }

  // homework

  static void homework_add({String courseId}) {
    _showAdaptivePage(
      name: R.homework_add,
      builder: (bool dialog) => HomeworkDetailsPage(courseId: courseId, fullscreen: !dialog),
    );
  }

  static void homework_details({@required String homeworkId}) {
    _showAdaptivePage(
      name: R.homework_details,
      builder: (bool dialog) => HomeworkDetailsPage(homeworkId: homeworkId, fullscreen: !dialog),
    );
  }

  static void homework_list({String courseId}) {
    navigator.push(_pageRoute(name: R.homework_list, page: HomeworkPage(courseId: courseId)));
  }

  // reminders

  static void reminders_add({String courseId}) {
    _showAdaptivePage(
      name: R.reminders_add,
      builder: (bool dialog) => ReminderDetailsPage(courseId: courseId, fullscreen: !dialog),
    );
  }

  static void reminders_details({@required String reminderId}) {
    _showAdaptivePage(
      name: R.reminders_details,
      builder: (bool dialog) => ReminderDetailsPage(reminderId: reminderId, fullscreen: !dialog),
    );
  }

  static void reminders_list({String courseId}) {
    navigator.push(_pageRoute(name: R.reminders_list, page: RemindersPage(courseId: courseId)));
  }

  // timetable

  static void timetable_subscribe_selectSchool() {
    _showAdaptivePage(name: R.timetable_subscribe_selectSchool, page: SelectSchoolPage());
  }

  static void timetable_subscribe_selectTimetable({@required String schoolId}) {
    _showAdaptivePage(name: R.timetable_subscribe_selectTimetable, page: SelectTimetablePage(schoolId: schoolId));
  }

  static void timetable_eventAdd({@required String timetableId}) {
    _showAdaptivePage(name: R.timetable_eventAdd, page: TimetableEventPage(timetableId: timetableId));
  }

  static void timetable_eventEdit({@required String eventId, @required DateTimeRange reoccursAt}) {
    _showAdaptivePage(
      name: R.timetable_eventEdit,
      page: TimetableEventPage(eventId: eventId, reoccursAt: reoccursAt),
    );
  }

  static void timetable_eventDuplicate({@required String copyFromEventId, @required DateTimeRange reoccursAt}) {
    _showAdaptivePage(
      name: R.timetable_eventDuplicate,
      page: TimetableEventPage(copyFromEventId: copyFromEventId, reoccursAt: reoccursAt),
    );
  }

  // people

  /// Shows a page to add a teacher.
  /// Returns the teacherId or null if not added.
  static Future<String> people_teacherAdd() {
    return _showAdaptivePage(name: R.people_teacherAdd, page: TeacherDetailsPage());
  }

  static void people_teacherDetails({@required String teacherId}) {
    navigator.push(_pageRoute(name: R.people_teacherDetails, page: TeacherDetailsPage(teacherId: teacherId)));
  }

  // settings

  static void settings_premium() {
    // TODO
  }

  static void settings_schoolYears() {
    navigator.push(_pageRoute(name: R.settings_schoolYears, page: SchoolYearsPage()));
  }

  static void settings_semesters() {
    navigator.push(_pageRoute(name: R.settings_semesters, page: SemesterPage()));
  }

  static void settings_notifications() {
    navigator.push(_pageRoute(name: R.settings_notifications, page: NotificationsPage()));
  }

  static void settings_themes() {
    navigator.push(_pageRoute(
      name: R.settings_themes,
      page: ThemesPage(onShowPremium: () => Nav.settings_premium()),
    ));
  }

  static void settings_privacy() {
    navigator.push(_pageRoute(name: R.settings_privacy, page: PrivacyPage()));
  }

  static void settings_support() {
    navigator.push(_pageRoute(name: R.settings_support, page: SupportPage()));
  }

  static void settings_about() {
    navigator.push(_pageRoute(name: R.settings_about, page: AboutPage()));
  }

  static void settings_credits() {
    navigator.push(_pageRoute(name: R.settings_credits, page: CreditsPage()));
  }

  static void settings_licenses(BuildContext context) {
    showLicensePage(context: context);
  }

  // common

  /// Shows a page to select event repeat options.
  /// Returns future which resolves to the picked options or null if cancelled.
  static Future<EventRepeatOptions> event_repeatOptions({@required EventRepeatOptions initialOptions}) {
    return _showAdaptivePage(
      name: R.event_repeatOptions,
      builder: (bool dialog) => EventRepeatOptionsPage(fullscreen: !dialog, initialOptions: initialOptions),
    );
  }

  static void privacyPolicy() {
    Intents.openBrowser(R.privacyPolicyUrl);
  }

  static void developerPage() {
    Intents.openBrowser(R.developerPageUrl);
  }

  static void appHomePage() {
    Intents.openBrowser(R.appHomePageUrl);
  }

  static void manageInAppSubscriptions() {
    Intents.openManageInAppSubscriptionsPage();
  }

  static void facebookPage() {
    Intents.openFacebookPage();
  }

  static void url(String url) {
    Intents.openBrowser(url);
  }

  /// Shows the page as a full screen material route on small screens and as a dialog on larger screen.
  static Future<T> _showAdaptivePage<T>({
    @required String name,
    Widget page,
    Widget Function(bool dialog) builder,
  }) {
    final Adaptive adaptive = Adaptive.of(navigator.context);
    if ((adaptive.isDesktop || adaptive.isTablet) && MediaQuery.of(navigator.context).size.height > 600) {
      return showDialog(
        context: navigator.context,
        routeSettings: RouteSettings(name: name),
        builder: (BuildContext context) => ScaffoldDialog(child: page ?? builder(true)),
      );
    } else {
      return navigator.push(_pageRoute(name: name, page: page ?? builder(false)));
    }
  }

  static PageRoute<T> _pageRoute<T>({@required String name, @required Widget page}) {
    return MaterialPageRoute(
      settings: RouteSettings(name: name),
      builder: (BuildContext context) => page,
    );
  }
}
