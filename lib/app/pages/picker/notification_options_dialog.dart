import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/common/formatter/notification_formatter.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/sized_dialog.dart';
import 'package:school_pen/app/widgets/text_field_container.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/notification_options.dart';
import 'package:school_pen/generated/l10n.dart';

/// Shows a dialog with a picker to select the [NotificationOptions].
///
/// Returns a future which resolves to the selected options.
/// Returns null when cancelled.
Future<NotificationOptions> showNotificationOptionsDialog({
  @required BuildContext context,
  RouteSettings routeSettings,
  NotificationOptions initialNotification,
}) {
  return showDialog(
    context: context,
    routeSettings: routeSettings,
    builder: (BuildContext context) => _NotificationOptionsDialog(initialNotification: initialNotification),
  );
}

class _NotificationOptionsDialog extends StatefulWidget {
  const _NotificationOptionsDialog({
    Key key,
    this.initialNotification,
  }) : super(key: key);

  final NotificationOptions initialNotification;

  @override
  _NotificationOptionsDialogState createState() => _NotificationOptionsDialogState();
}

class _NotificationOptionsDialogState extends State<_NotificationOptionsDialog> {
  final TextEditingController _controller = TextEditingController();
  Exception _error;
  NotificationOptionsUnit _unit;

  @override
  void initState() {
    super.initState();
    if (widget.initialNotification != null) {
      _controller.text = widget.initialNotification.amount.toString();
      _unit = widget.initialNotification.unit;
    } else {
      _controller.text = '30';
      _unit = NotificationOptionsUnit.minutes;
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedDialog(
      padding: EdgeInsets.all(Dimens.grid_2x),
      child: Column(children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            S.of(context).common_notification,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x),
          child: Divider(),
        ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x, bottom: Dimens.grid_1x),
          child: TextFieldContainer(
            width: 280,
            maxHeight: TextFieldContainer.miniHeight,
            child: TextField(
              controller: _controller,
              style: Theme.of(context).textTheme.headline6,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: S.of(context).common_notifyMe),
              textInputAction: TextInputAction.done,
            ),
            error: TextFieldError.forError(_error),
          ),
        ),
        for (NotificationOptionsUnit unit in NotificationOptionsUnit.values) _buildUnitRow(context, unit),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SmallMaterialButton(
                child: SmallButtonText(
                  S.of(context).common_cancel,
                  color: Theme.of(context).colorScheme.primaryVariant,
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              SmallRaisedButton(
                child: SmallButtonText(S.of(context).common_save),
                onPressed: () => _onSubmit(context),
              ),
            ],
          ),
        ),
      ]),
    );
  }

  Widget _buildUnitRow(BuildContext context, NotificationOptionsUnit unit) {
    return InkWell(
      onTap: () => setState(() => _unit = unit),
      child: Row(children: [
        Radio(
          value: unit,
          groupValue: _unit,
          onChanged: (NotificationOptionsUnit unit) => setState(() => _unit = unit),
        ),
        Padding(
          padding: EdgeInsets.only(left: Dimens.grid_1x),
          child: Text(
            NotificationFormatter.formatUnit(context, unit, unit == _unit),
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ]),
    );
  }

  void _onSubmit(BuildContext context) {
    try {
      final NotificationOptions options = NotificationOptions(
        amount: int.parse(_controller.text),
        unit: _unit,
      );

      Navigator.of(context).pop(options);
    } on FormatException {
      setState(() {
        _error = FieldMustNotBeEmptyException();
      });
    }
  }
}
