import 'dart:math';

import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/save_or_discard_footer.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/text_field_container.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/exception/at_least_one_value_must_be_selected_exception.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/exception/number_out_of_range_exception.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/generated/l10n.dart';

const double _textFieldBottomPadding = 10.0;
const double _radioButtonWidth = 56.0;
const double _radioButtonHeight = 56.0;
const double _weekdayTileSize = 36.0;
const double _weekdayTilePadding = 8.0;

enum _EventRepeatEndOptions {
  never,
  onDate,
  afterOccurrences,
}

/// Allows to pick [EventRepeatOptions].
class EventRepeatOptionsPage extends StatefulWidget {
  final bool fullscreen;
  final EventRepeatOptions initialOptions;

  const EventRepeatOptionsPage({
    Key key,
    this.fullscreen = true,
    @required this.initialOptions,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _EventRepeatOptionsPageState();
}

class _EventRepeatOptionsPageState extends State<EventRepeatOptionsPage> with SingleTickerProviderStateMixin {
  static const List<EventRepeatOptionsType> _selectableRepeatTypes = [
    EventRepeatOptionsType.daily,
    EventRepeatOptionsType.weekly,
    EventRepeatOptionsType.monthly,
  ];

  final TextEditingController _repeatIntervalController = TextEditingController();
  final TextEditingController _endsAtController = TextEditingController();
  final TextEditingController _endsAfterOccurrencesController = TextEditingController();

  final FocusNode _repeatIntervalFocus = FocusNode();
  final FocusNode _endsAtFocus = FocusNode();
  final FocusNode _endsAfterOccurrencesFocus = FocusNode();

  final List<Weekday> _weeklyRepeatsOn = [];
  EventRepeatOptionsType _repeatType;
  _EventRepeatEndOptions _endOptions;
  DateTime _endsAt;
  Exception _repeatIntervalError;
  Exception _weeklyRepeatsOnError;
  Exception _endsAtError;
  Exception _endsAfterOccurrencesError;

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void didUpdateWidget(EventRepeatOptionsPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.initialOptions != widget.initialOptions) {
      _init();
    }
  }

  void _init() {
    final EventRepeatOptions options = widget.initialOptions;

    _repeatType = options.type;
    if (!_selectableRepeatTypes.contains(_repeatType)) {
      _repeatType = _selectableRepeatTypes.first;
    }

    _endsAt = options.endsAt;
    _endOptions = _calculateEndOptions(options.endsAt, options.endsAfterOccurrences);
    _weeklyRepeatsOn.clear();
    _weeklyRepeatsOn.addAll(options.weeklyRepeatsOn);

    _repeatIntervalError = null;
    _weeklyRepeatsOnError = null;
    _endsAtError = null;
    _endsAfterOccurrencesError = null;

    _repeatIntervalController.text = max(options.repeatInterval, 1).toString();
    _endsAtController.text = DateFormatter.formatDate(options.endsAt);
    _endsAfterOccurrencesController.text = options.endsAfterOccurrences?.toString() ?? '';
  }

  @override
  void dispose() {
    _repeatIntervalController.dispose();
    _endsAtController.dispose();
    _endsAfterOccurrencesController.dispose();
    _repeatIntervalFocus.dispose();
    _endsAtFocus.dispose();
    _endsAfterOccurrencesFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    final EdgeInsets horizontal = EdgeInsets.symmetric(horizontal: paddings.horizontal);

    Widget result = CustomScrollView(slivers: [
      Toolbar(
        vsync: this,
        navType: NavButtonType.close,
      ),
      SliverList(
        delegate: SliverChildListDelegate([
          Padding(
            padding: horizontal.copyWith(top: Dimens.grid_4x - Toolbar.bottomPadding),
            child: _buildHeader(context, S.of(context).repeatOptionsPage_repeatsEvery),
          ),
          Padding(
            padding: horizontal.copyWith(top: Dimens.grid_3x),
            child: _buildRepeatIntervalRow(context),
          ),
          if (_repeatIntervalError != null)
            Padding(
              padding: horizontal.copyWith(top: Dimens.grid_1x),
              child: _buildError(context, ResourceLocator.errorToText(context, _repeatIntervalError)),
            ),
          Padding(
            padding: horizontal.copyWith(top: Dimens.grid_2x),
            child: Divider(),
          ),
          if (_repeatType == EventRepeatOptionsType.weekly) ...[
            Padding(
              padding: horizontal.copyWith(top: Dimens.grid_4x),
              child: _buildHeader(context, S.of(context).repeatOptionsPage_repeatsOn),
            ),
            Padding(
              padding: horizontal.copyWith(top: Dimens.grid_3x),
              child: _buildWeeklyRepeatsOnRow(context),
            ),
            if (_weeklyRepeatsOnError != null)
              Padding(
                padding: horizontal.copyWith(top: Dimens.grid_1x),
                child: _buildError(context, ResourceLocator.errorToText(context, _weeklyRepeatsOnError)),
              ),
            Padding(
              padding: horizontal.copyWith(top: Dimens.grid_2x),
              child: Divider(),
            ),
          ],
          Padding(
            padding: horizontal.copyWith(top: Dimens.grid_4x),
            child: _buildHeader(context, S.of(context).repeatOptionsPage_ends),
          ),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_1x),
            child: _buildEndsNeverRow(context),
          ),
          Padding(
            padding: horizontal,
            child: Divider(),
          ),
          _buildEndsAtRow(context),
          if (_endsAtError != null)
            Padding(
              padding: horizontal.copyWith(
                left: paddings.horizontal + Dimens.grid_5x,
                bottom: Dimens.grid_1x,
              ),
              child: _buildError(context, ResourceLocator.errorToText(context, _endsAtError)),
            ),
          Padding(
            padding: horizontal,
            child: Divider(),
          ),
          _buildEndsAfterOccurrencesRow(context),
          if (_endsAfterOccurrencesError != null)
            Padding(
              padding: horizontal.copyWith(
                left: paddings.horizontal + Dimens.grid_5x,
                bottom: Dimens.grid_1x,
              ),
              child: _buildError(context, ResourceLocator.errorToText(context, _endsAfterOccurrencesError)),
            ),
          Container(height: paddings.vertical),
        ]),
      ),
    ]);

    result = Scaffold(
      body: SafeArea(
        child: Column(children: [
          Expanded(child: result),
          SaveOrDiscardFooter(
            onSave: () => _onSave(context),
            onDiscard: () => _onDiscard(context),
          ),
        ]),
      ),
    );

    if (widget.fullscreen) {
      result = SystemUiOverlayAnnotation(
        child: result,
        systemNavigationBarColor: Theme.of(context).colorScheme.surface,
      );
    }

    return result;
  }

  Widget _buildHeader(BuildContext context, String text) {
    return Text(
      text.toUpperCase(),
      style: Theme.of(context).textTheme.overline.copyWith(color: Theme.of(context).colorScheme.primaryVariant),
    );
  }

  Widget _buildError(BuildContext context, String text) {
    return Text(
      text,
      style: Theme.of(context).textTheme.caption.copyWith(color: Theme.of(context).colorScheme.error),
    );
  }

  Widget _buildRepeatIntervalRow(BuildContext context) {
    return Row(children: [
      _buildTextField(
        context: context,
        textFieldWidth: 56.0,
        controller: _repeatIntervalController,
        focus: _repeatIntervalFocus,
        placeholder: '1',
        onChanged: (String text) {
          setState(() {
            // reformat value in popup, text controller has already this state
          });
        },
      ),
      Padding(
        padding: EdgeInsets.only(left: Dimens.grid_2x),
        child: SmallBorderedPopupButton(
          child: SmallButtonText(
            _mapTypeToString(context, _repeatType, _repeatInterval ?? 0),
            color: Theme.of(context).colorScheme.primary,
          ),
          itemBuilder: (BuildContext context) => [
            for (EventRepeatOptionsType type in _selectableRepeatTypes)
              PopupMenuItem(
                value: type,
                child: Text(_mapTypeToString(context, type, _repeatInterval ?? 0)),
              ),
          ],
          onSelected: (EventRepeatOptionsType type) {
            _unfocusTextFields();
            setState(() => _repeatType = type);
          },
        ),
      ),
    ]);
  }

  Widget _buildWeeklyRepeatsOnRow(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      if (constraints.maxWidth >= (Weekday.values.length * (_weekdayTileSize + 2 * _weekdayTilePadding))) {
        return Row(
          children: _generateWeeklyTiles(context)
              .map((e) => Padding(padding: EdgeInsets.symmetric(horizontal: _weekdayTilePadding), child: e))
              .toList(),
        );
      } else {
        return Wrap(
          alignment: WrapAlignment.spaceBetween,
          children: _generateWeeklyTiles(context).toList(),
        );
      }
    });
  }

  Iterable<Widget> _generateWeeklyTiles(BuildContext context) sync* {
    final BorderRadius borderRadius = BorderRadius.all(Radius.circular(12));
    Weekday weekday = Weekday.firstOfWeek();

    for (int i = 0; i < Weekday.values.length; i++) {
      final bool active = _weeklyRepeatsOn.contains(weekday);
      final Weekday weekdayClosure = weekday;
      yield InkWell(
        onTap: () => setState(() {
          _unfocusTextFields();

          if (_weeklyRepeatsOn.contains(weekdayClosure)) {
            _weeklyRepeatsOn.remove(weekdayClosure);
          } else {
            _weeklyRepeatsOn.add(weekdayClosure);
          }
        }),
        borderRadius: borderRadius,
        child: Container(
          width: 36,
          height: 36,
          decoration: BoxDecoration(
            color: active ? Theme.of(context).colorScheme.secondary : Theme.of(context).colorScheme.surface,
            borderRadius: borderRadius,
            border: Border.all(color: Theme.of(context).dividerColor),
          ),
          child: Center(
            child: Text(
              Strings.firstLetter(ResourceLocator.weekdayToString(context, weekdayClosure)),
              style: Theme.of(context).textTheme.button.copyWith(
                    color: active ? Theme.of(context).colorScheme.onSecondary : Theme.of(context).colorScheme.primary,
                  ),
            ),
          ),
        ),
      );

      weekday = weekday.nextDay();
    }
  }

  Widget _buildEndsNeverRow(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return InkWell(
      onTap: () {
        _unfocusTextFields();
        setState(() => _endOptions = _EventRepeatEndOptions.never);
      },
      child: Padding(
        padding: EdgeInsets.only(
          left: paddings.horizontal - Dimens.grid_2x,
          right: paddings.horizontal,
        ),
        child: Row(children: [
          Container(
            constraints: BoxConstraints(
              minWidth: _radioButtonWidth,
              minHeight: _radioButtonHeight,
            ),
            child: Radio(
              value: _EventRepeatEndOptions.never,
              groupValue: _endOptions,
              onChanged: (_EventRepeatEndOptions endOptions) {
                setState(() => _endOptions = endOptions);
              },
            ),
          ),
          Text(S.of(context).common_never, style: Theme.of(context).textTheme.bodyText1),
        ]),
      ),
    );
  }

  Widget _buildEndsAtRow(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return InkWell(
      onTap: () {
        _unfocusTextFields();
        setState(() => _endOptions = _EventRepeatEndOptions.onDate);
        _showEndsAtPicker(context);
      },
      child: Padding(
        padding: EdgeInsets.only(
          left: paddings.horizontal - Dimens.grid_2x,
          right: paddings.horizontal,
        ),
        child: Row(children: [
          Container(
            constraints: BoxConstraints(
              minWidth: _radioButtonWidth,
              minHeight: _radioButtonHeight,
            ),
            child: Radio(
              value: _EventRepeatEndOptions.onDate,
              groupValue: _endOptions,
              onChanged: (_EventRepeatEndOptions endOptions) {
                setState(() => _endOptions = endOptions);
              },
            ),
          ),
          Text(S.of(context).repeatOptionsPage_onDate, style: Theme.of(context).textTheme.bodyText1),
          Padding(
            padding: EdgeInsets.only(left: Dimens.grid_1_5x),
            child: _buildTextField(
              context: context,
              textFieldWidth: 160,
              controller: _endsAtController,
              focus: _endsAtFocus,
              enabled: false,
              onChanged: null,
            ),
          ),
        ]),
      ),
    );
  }

  void _showEndsAtPicker(BuildContext context) async {
    final DateTime now = DateTime.now().withoutTime();
    final DateTime initialDate = _endsAt ?? now.plusDays(1);

    DateTime dateTime = await showAdaptiveDatePicker(
      context: context,
      initialDate: _endsAt ?? now.plusDays(1),
      minimumDate: _endsAt == null || initialDate.isBefore(_endsAt) ? initialDate : _endsAt,
    );

    if (dateTime != null) {
      dateTime = dateTime.asUtc();
      setState(() {
        _endsAt = dateTime;
        _endsAtController.text = DateFormatter.formatDate(dateTime);
      });
    }
  }

  Widget _buildEndsAfterOccurrencesRow(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return InkWell(
      onTap: () {
        _endsAfterOccurrencesFocus.requestFocus();
        setState(() => _endOptions = _EventRepeatEndOptions.afterOccurrences);
      },
      child: Padding(
        padding: EdgeInsets.only(
          left: paddings.horizontal - Dimens.grid_2x,
          right: paddings.horizontal,
        ),
        child: Row(children: [
          Container(
            constraints: BoxConstraints(
              minWidth: _radioButtonWidth,
              minHeight: _radioButtonHeight,
            ),
            child: Radio(
              value: _EventRepeatEndOptions.afterOccurrences,
              groupValue: _endOptions,
              onChanged: (_EventRepeatEndOptions endOptions) {
                setState(() => _endOptions = endOptions);
              },
            ),
          ),
          Text(
            _getEndsAfterOccurrencesPrefix(context, _endsAfterOccurrences ?? 0),
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_1_5x),
            child: _buildTextField(
              context: context,
              textFieldWidth: 44,
              controller: _endsAfterOccurrencesController,
              focus: _endsAfterOccurrencesFocus,
              placeholder: '1',
              onChanged: (String text) {
                setState(() {
                  // reformat, text controller already has this state.
                });
              },
            ),
          ),
          Text(
            _getEndsAfterOccurrencesSuffix(context, _endsAfterOccurrences ?? 0),
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ]),
      ),
    );
  }

  String _getEndsAfterOccurrencesPrefix(BuildContext context, int occurrences) {
    final String text = occurrences <= 1
        ? S.of(context).repeatOptionsPage_afterOneOccurrence
        : S.of(context).repeatOptionsPage_afterNOccurrences;

    return text.substring(0, text.indexOf(RegExp(r'%s'))).trim();
  }

  String _getEndsAfterOccurrencesSuffix(BuildContext context, int occurrences) {
    final String text = occurrences <= 1
        ? S.of(context).repeatOptionsPage_afterOneOccurrence
        : S.of(context).repeatOptionsPage_afterNOccurrences;

    return text.substring(text.indexOf(RegExp(r'%s')) + 2, text.length).trim();
  }

  Widget _buildTextField({
    @required BuildContext context,
    @required double textFieldWidth,
    @required TextEditingController controller,
    @required FocusNode focus,
    String placeholder = '',
    bool enabled = true,
    @required ValueChanged<String> onChanged,
  }) {
    return TextFieldContainer(
      width: textFieldWidth,
      maxHeight: TextFieldContainer.microHeight,
      child: TextField(
        controller: controller,
        focusNode: focus,
        style: Theme.of(context).textTheme.headline6,
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.done,
        enabled: enabled,
        decoration: InputDecoration(
          hintText: placeholder,
          contentPadding: EdgeInsets.only(bottom: _textFieldBottomPadding),
        ),
        scrollPadding: EdgeInsets.zero,
        onChanged: onChanged,
      ),
    );
  }

  int get _repeatInterval => int.tryParse(_repeatIntervalController.text);

  int get _endsAfterOccurrences => int.tryParse(_endsAfterOccurrencesController.text);

  _EventRepeatEndOptions _calculateEndOptions(DateTime endsAt, int endsAfterOccurrences) {
    if (endsAt != null) return _EventRepeatEndOptions.onDate;
    if (endsAfterOccurrences != null) return _EventRepeatEndOptions.afterOccurrences;
    return _EventRepeatEndOptions.never;
  }

  String _mapTypeToString(BuildContext context, EventRepeatOptionsType type, int repeatInterval) {
    final S s = S.of(context);

    switch (type) {
      case EventRepeatOptionsType.single:
        throw ArgumentError('Single does not repeat!');
      case EventRepeatOptionsType.daily:
        return repeatInterval <= 1 ? s.repeatOptionsPage_day : s.repeatOptionsPage_days;
      case EventRepeatOptionsType.weekly:
        return repeatInterval <= 1 ? s.repeatOptionsPage_week : s.repeatOptionsPage_weeks;
      case EventRepeatOptionsType.monthly:
        return repeatInterval <= 1 ? s.repeatOptionsPage_month : s.repeatOptionsPage_months;
    }
    throw ArgumentError('Unsupported type: $type');
  }

  void _unfocusTextFields() {
    _repeatIntervalFocus.unfocus();
    _endsAtFocus.unfocus();
    _endsAfterOccurrencesFocus.unfocus();
  }

  bool _validate(BuildContext context) {
    _repeatIntervalError = null;
    _weeklyRepeatsOnError = null;
    _endsAtError = null;
    _endsAfterOccurrencesError = null;

    if (_repeatInterval == null || _repeatInterval < 1) {
      setState(() => _repeatIntervalError = NumberOutOfRangeException(min: 1, max: 1000));
      return false;
    }

    if (_repeatType == EventRepeatOptionsType.weekly && _weeklyRepeatsOn.isEmpty) {
      setState(() => _weeklyRepeatsOnError = AtLeastOneValueMustBeSelectedException());
      return false;
    }

    if (_endOptions == _EventRepeatEndOptions.onDate && _endsAt == null) {
      setState(() => _endsAtError = FieldMustNotBeEmptyException());
      return false;
    }

    if (_endOptions == _EventRepeatEndOptions.afterOccurrences &&
        (_endsAfterOccurrences == null || _endsAfterOccurrences < 1)) {
      setState(() => _endsAfterOccurrencesError = NumberOutOfRangeException(min: 1, max: 10000));
      return false;
    }

    setState(() {
      // fields are already set
    });

    return true;
  }

  void _onSave(BuildContext context) {
    if (_validate(context)) {
      final EventRepeatOptions options = EventRepeatOptions(
        type: _repeatType,
        dateRange: _repeatType == EventRepeatOptionsType.weekly
            ? _getDateRangeAdjustedForWeeklyType()
            : widget.initialOptions.dateRange,
        repeatInterval: _repeatInterval,
        weeklyRepeatsOn: _repeatType == EventRepeatOptionsType.weekly ? _weeklyRepeatsOn : [],
        endsAt: _endOptions == _EventRepeatEndOptions.onDate ? _endsAt : null,
        endsAfterOccurrences: _endOptions == _EventRepeatEndOptions.afterOccurrences ? _endsAfterOccurrences : null,
      );

      Navigator.of(context).pop(options);
    }
  }

  DateTimeRange _getDateRangeAdjustedForWeeklyType() {
    DateTime startDate = widget.initialOptions.dateRange.start;
    DateTime endDate = widget.initialOptions.dateRange.end;

    final Weekday earliest = Weekday.earliest(_weeklyRepeatsOn);

    if (Weekday.fromDateTime(startDate) != earliest) {
      startDate = earliest.isBefore(Weekday.fromDateTime(startDate))
          ? startDate.adjustToPreviousWeekday(earliest)
          : startDate.adjustToNextWeekday(earliest);

      endDate = endDate.plusDays(DateTimeRanges.durationInDaysOf(
        start: widget.initialOptions.dateRange.start,
        end: startDate,
      ));
    }

    return DateTimeRange(start: startDate, end: endDate);
  }

  void _onDiscard(BuildContext context) {
    Navigator.of(context).pop();
  }
}
