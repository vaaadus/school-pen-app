import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/sized_dialog.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

typedef OptionsDialogBuilder<T> = List<OptionDialogItem<T>> Function(BuildContext context);

/// Shows a dialog with options to select.
/// Returns the selected option or null if cancelled.
Future<T> showOptionsDialog<T>({
  @required BuildContext context,
  RouteSettings routeSettings,
  String title,
  @required OptionsDialogBuilder<T> itemBuilder,
}) {
  final Adaptive adaptive = Adaptive.of(context);
  if (adaptive.isMobile) {
    return _showBottomSheet(context, routeSettings, title, itemBuilder);
  } else {
    return _showDialog(context, routeSettings, title, itemBuilder);
  }
}

Future<T> _showBottomSheet<T>(
  BuildContext context,
  RouteSettings routeSettings,
  String title,
  OptionsDialogBuilder<T> itemBuilder,
) {
  return showModalBottomSheet(
    context: context,
    routeSettings: routeSettings,
    isScrollControlled: true,
    builder: (BuildContext context) => SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height / 2),
        child: _OptionsDialogContent(
          title: title,
          itemBuilder: itemBuilder,
          showCloseButton: true,
        ),
      ),
    ),
  );
}

Future<T> _showDialog<T>(
  BuildContext context,
  RouteSettings routeSettings,
  String title,
  OptionsDialogBuilder<T> itemBuilder,
) {
  return showDialog(
    context: context,
    routeSettings: routeSettings,
    builder: (BuildContext context) => SizedDialog(
      child: _OptionsDialogContent(
        title: title,
        itemBuilder: itemBuilder,
      ),
    ),
  );
}

/// Defines what to display in the dialog.
///
/// Use [Colors.transparent] if you do not want to apply a tint color to an image.
class OptionDialogItem<T> extends Equatable {
  final String imageAsset24;
  final String text;
  final String label;
  final Color customColor;
  final Color imageColor;
  final bool enabled;
  final T value;

  const OptionDialogItem({
    this.imageAsset24,
    @required this.text,
    this.label,
    this.customColor,
    this.imageColor,
    this.enabled = true,
    @required this.value,
  });

  @override
  List<Object> get props => [imageAsset24, text, label, customColor, imageColor, enabled, value];
}

class _OptionsDialogContent<T> extends StatelessWidget {
  const _OptionsDialogContent({
    Key key,
    this.title,
    this.showCloseButton = false,
    @required this.itemBuilder,
  }) : super(key: key);

  final String title;
  final bool showCloseButton;
  final OptionsDialogBuilder<T> itemBuilder;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: showCloseButton ? Dimens.grid_0_5x : Dimens.grid_1_5x,
        bottom: _showHeader ? Dimens.grid_0_5x : Dimens.grid_1_5x,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (_showHeader) ...[
            _buildHeader(context),
            _buildDivider(context),
          ],
          for (OptionDialogItem<T> item in itemBuilder(context)) _OptionDialogItemWidget(item: item),
        ],
      ),
    );
  }

  Widget _buildHeader(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: Dimens.grid_0_5x,
        left: Dimens.grid_2x,
        right: Dimens.grid_2x,
      ),
      child: Row(children: [
        Expanded(child: Text(title, style: Theme.of(context).textTheme.headline6)),
        if (showCloseButton) NavButton.close(includeDefaultPadding: false)
      ]),
    );
  }

  Widget _buildDivider(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: showCloseButton ? Dimens.grid_1x : Dimens.grid_2x,
        bottom: Dimens.grid_0_5x,
        left: Dimens.grid_2x,
        right: Dimens.grid_2x,
      ),
      child: Divider(),
    );
  }

  bool get _showHeader => Strings.isNotBlank(title);
}

class _OptionDialogItemWidget<T> extends StatelessWidget {
  const _OptionDialogItemWidget({Key key, @required this.item}) : super(key: key);

  final OptionDialogItem<T> item;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _getCallback(context),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: Dimens.grid_1_5x, horizontal: Dimens.grid_3x),
        child: Row(children: [
          if (_showImage)
            Padding(
              padding: EdgeInsets.only(right: Dimens.grid_3x),
              child: Image.asset(item.imageAsset24, width: 24, height: 24, color: _getImageColor(context)),
            ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item.text,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(color: _getTextColor(context)),
                ),
                if (Strings.isNotBlank(item.label))
                  Padding(
                    padding: EdgeInsets.only(top: Dimens.grid_0_5x),
                    child: Text(
                      item.label,
                      style: Theme.of(context).textTheme.bodyText2.copyWith(color: _getLabelColor(context)),
                    ),
                  ),
              ],
            ),
          ),
        ]),
      ),
    );
  }

  bool get _showImage => Strings.isNotBlank(item.imageAsset24);

  GestureTapCallback _getCallback(BuildContext context) {
    if (!item.enabled) return null;

    return () => Navigator.of(context).pop(item.value);
  }

  Color _getTextColor(BuildContext context) {
    if (!item.enabled) return Theme.of(context).colorScheme.primaryVariant;
    if (item.customColor != null) return item.customColor;
    return Theme.of(context).colorScheme.primary;
  }

  Color _getLabelColor(BuildContext context) {
    if (!item.enabled) return Theme.of(context).colorScheme.primaryVariant;
    if (item.customColor != null) return item.customColor;
    return Theme.of(context).colorScheme.primaryVariant;
  }

  Color _getImageColor(BuildContext context) {
    if (!item.enabled) return Theme.of(context).colorScheme.primaryVariant;
    if (item.imageColor == Colors.transparent) return null;
    if (item.imageColor != null) return item.imageColor;
    if (item.customColor != null) return item.customColor;
    return Theme.of(context).colorScheme.primaryVariant;
  }
}
