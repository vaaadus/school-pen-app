import 'package:flutter/material.dart';
import 'package:school_pen/app/pages/picker/radio_button_dialog.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/generated/l10n.dart';

/// Asks the user how he would like to save the event.
Future<EventRepeatOptionsEditMode> showEventRepeatOptionsEditModeDialog({
  @required BuildContext context,
  RouteSettings routeSettings,
  List<EventRepeatOptionsEditMode> editModes = const [],
}) async {
  if (editModes.length == 1) return editModes.first;

  final EventRepeatOptionsEditMode groupValue = editModes.firstOrNull;
  return showRadioButtonDialog(
    context: context,
    routeSettings: routeSettings,
    title: S.of(context).common_saveRecurringEvent,
    saveButtonText: S.of(context).common_save,
    itemBuilder: (BuildContext context) => [
      if (editModes.contains(EventRepeatOptionsEditMode.thisAndFollowingEvents))
        RadioButtonDialogItem(
          text: S.of(context).recurringEvent_thisAndFollowingEvents,
          value: EventRepeatOptionsEditMode.thisAndFollowingEvents,
          groupValue: groupValue,
        ),
      if (editModes.contains(EventRepeatOptionsEditMode.allEvents))
        RadioButtonDialogItem(
          text: S.of(context).recurringEvent_allEvents,
          value: EventRepeatOptionsEditMode.allEvents,
          groupValue: groupValue,
        ),
      if (editModes.contains(EventRepeatOptionsEditMode.thisEvent))
        RadioButtonDialogItem(
          text: S.of(context).recurringEvent_thisEvent,
          value: EventRepeatOptionsEditMode.thisEvent,
          groupValue: groupValue,
        ),
    ],
  );
}
