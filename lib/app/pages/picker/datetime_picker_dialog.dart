import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/pages/picker/options_dialog.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/numbers.dart';
import 'package:school_pen/generated/l10n.dart';

final DateTime _minimumDateTime = DateTime(1970);
final DateTime _maximumDateTime = DateTime(2199);

/// Shows a multi step date time picker with suggestions, on mobile a bottom sheet, else a dialog.
/// - in an hour
/// - tomorrow at 8:00
/// - next week at 8:00
/// - pick date & time
///
/// Returns a [Future] which completes when a user picks a date time.
/// If the dialog is cancelled, the future is completed with null.
Future<DateTime> showAdaptiveDateTimePickerWithSuggestions({
  @required BuildContext context,
  DateTime initialDate,
  DateTime minimumDate,
  DateTime maximumDate,
}) async {
  initialDate = initialDate ?? DateTime.now();
  minimumDate = minimumDate ?? _minimumDateTime;
  maximumDate = maximumDate ?? _maximumDateTime;

  final DateTime now = DateTime.now();
  final DateTime inHour = now.add(Duration(hours: 1)).withMinutes(Numbers.floorToTens(now.minute));
  final DateTime tomorrow = now.plusDays(1).withTime(Time(hour: 8, minute: 0));
  final DateTime nextWeek = now.plusDays(7).withTime(Time(hour: 8, minute: 0));
  final DateTime pickDate = DateTime.fromMillisecondsSinceEpoch(0);

  final DateTime dateTime = await showOptionsDialog(
    context: context,
    itemBuilder: (BuildContext context) => [
      OptionDialogItem(
        imageAsset24: Images.ic_clock_24,
        text: S.of(context).common_inHour,
        label: DateFormatter.formatTime(inHour),
        value: inHour,
      ),
      OptionDialogItem(
        imageAsset24: Images.ic_tomorrow_24,
        text: S.of(context).common_tomorrow,
        label: DateFormatter.formatFull(tomorrow),
        value: tomorrow,
      ),
      OptionDialogItem(
        imageAsset24: Images.ic_next_week_24,
        text: S.of(context).common_nextWeek,
        label: DateFormatter.formatFull(nextWeek),
        value: nextWeek,
      ),
      OptionDialogItem(
        imageAsset24: Images.ic_custom_date_24,
        text: S.of(context).common_pickDateAndTime,
        value: pickDate,
      ),
    ],
  );

  if (dateTime != pickDate) return dateTime;

  return showAdaptiveDateTimePicker(
    context: context,
    initialDate: initialDate,
    minimumDate: minimumDate,
    maximumDate: maximumDate,
  );
}

/// Shows a date time picker, on mobile a bottom sheet, else a dialog.
///
/// Returns a [Future] which completes when a user picks a date time.
/// If the dialog is cancelled, the future is completed with null.
Future<DateTime> showAdaptiveDateTimePicker({
  @required BuildContext context,
  DateTime initialDate,
  DateTime minimumDate,
  DateTime maximumDate,
}) async {
  initialDate = initialDate ?? DateTime.now();
  minimumDate = minimumDate ?? _minimumDateTime;
  maximumDate = maximumDate ?? _maximumDateTime;

  final Adaptive adaptive = Adaptive.of(context);
  if (adaptive.isMobile) {
    return _showBottomSheetPicker(
      context: context,
      initialDate: initialDate,
      minimumDate: minimumDate,
      maximumDate: maximumDate,
      mode: CupertinoDatePickerMode.dateAndTime,
    );
  } else {
    return _showDateTimeDialogPicker(
      context: context,
      initialDate: initialDate,
      minimumDate: minimumDate,
      maximumDate: maximumDate,
    );
  }
}

/// Shows a date picker, on mobile a bottom sheet, else a dialog.
///
/// Returns a [Future] which completes when a user picks a date.
/// If the dialog is cancelled, the future is completed with null.
Future<DateTime> showAdaptiveDatePicker({
  @required BuildContext context,
  DateTime initialDate,
  DateTime minimumDate,
  DateTime maximumDate,
}) async {
  initialDate = (initialDate ?? DateTime.now()).withoutTime();
  minimumDate = (minimumDate ?? _minimumDateTime).withoutTime();
  maximumDate = (maximumDate ?? _maximumDateTime).withoutTime();

  final Adaptive adaptive = Adaptive.of(context);
  if (adaptive.isMobile) {
    return _showBottomSheetPicker(
      context: context,
      initialDate: initialDate,
      minimumDate: minimumDate,
      maximumDate: maximumDate,
      mode: CupertinoDatePickerMode.date,
    );
  } else {
    return showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: minimumDate,
      lastDate: maximumDate,
    );
  }
}

/// Shows a time picker, on mobile a bottom sheet, else a dialog.
///
/// Returns a [Future] which completes when a user picks a time.
/// If the dialog is cancelled, the future is completed with null.
Future<Time> showAdaptiveTimePicker({
  @required BuildContext context,
  Time initialTime,
  Time minimumTime,
  Time maximumTime,
  int minuteInterval,
}) async {
  initialTime ??= Time.now();

  final Adaptive adaptive = Adaptive.of(context);
  if (adaptive.isMobile) {
    return _showTimePickerBottomSheet(
      context: context,
      initialTime: initialTime,
      minimumTime: minimumTime,
      maximumTime: maximumTime,
      minuteInterval: minuteInterval,
    );
  } else {
    final TimeOfDay timeOfDay = await showTimePicker(
      context: context,
      initialTime: initialTime.toTimeOfDay(),
    );
    if (timeOfDay == null) return null;

    final Time time = Time.fromTimeOfDay(timeOfDay);
    if (minimumTime != null && time.isBefore(minimumTime)) return minimumTime;
    if (maximumTime != null && time.isAfter(maximumTime)) return maximumTime;
    return time;
  }
}

Future<DateTime> _showDateTimeDialogPicker({
  @required BuildContext context,
  @required DateTime initialDate,
  @required DateTime minimumDate,
  @required DateTime maximumDate,
}) async {
  final DateTime dateTime = await showDatePicker(
    context: context,
    initialDate: initialDate,
    firstDate: minimumDate,
    lastDate: maximumDate,
  );
  if (dateTime == null) return null;

  final TimeOfDay time = await showTimePicker(
    context: context,
    initialTime: initialDate.timeOfDay,
  );
  if (time == null) return null;

  return dateTime.withTime(Time.fromTimeOfDay(time));
}

Future<DateTime> _showBottomSheetPicker({
  @required BuildContext context,
  DateTime initialDate,
  DateTime minimumDate,
  DateTime maximumDate,
  int minuteInterval,
  CupertinoDatePickerMode mode,
}) {
  initialDate = (initialDate ?? DateTime.now()).withoutSeconds();
  minimumDate = minimumDate?.withoutSeconds();
  maximumDate = maximumDate?.withoutSeconds();
  minuteInterval = minuteInterval ?? 1;
  mode = mode ?? CupertinoDatePickerMode.dateAndTime;

  if (minimumDate != null && initialDate.isBefore(minimumDate)) {
    minimumDate = initialDate;
  }

  if (maximumDate != null && initialDate.isAfter(maximumDate)) {
    maximumDate = initialDate;
  }

  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    builder: (BuildContext context) => _DateTimePickerBottomSheetContent(
      initialDate: initialDate,
      firstDate: minimumDate,
      lastDate: maximumDate,
      minuteInterval: minuteInterval,
      mode: mode,
    ),
  );
}

Future<Time> _showTimePickerBottomSheet({
  @required BuildContext context,
  Time initialTime,
  Time minimumTime,
  Time maximumTime,
  int minuteInterval,
}) async {
  final now = DateTime.now();
  final initialDate = initialTime != null ? now.withTime(initialTime) : null;
  final minimumDate = minimumTime != null ? now.withTime(minimumTime) : null;
  final maximumDate = maximumTime != null ? now.withTime(maximumTime) : null;

  final dateTime = await _showBottomSheetPicker(
    context: context,
    initialDate: initialDate,
    minimumDate: minimumDate,
    maximumDate: maximumDate,
    minuteInterval: minuteInterval,
    mode: CupertinoDatePickerMode.time,
  );

  return dateTime != null ? Time.fromDateTime(dateTime) : null;
}

class _DateTimePickerBottomSheetContent extends StatefulWidget {
  final DateTime initialDate;
  final DateTime firstDate;
  final DateTime lastDate;
  final int minuteInterval;
  final CupertinoDatePickerMode mode;

  _DateTimePickerBottomSheetContent({
    @required this.initialDate,
    @required this.firstDate,
    @required this.lastDate,
    @required this.minuteInterval,
    @required this.mode,
  });

  @override
  State<StatefulWidget> createState() => _DateTimePickerBottomSheetContentState();
}

class _DateTimePickerBottomSheetContentState extends State<_DateTimePickerBottomSheetContent> {
  static const double _dialogHeightPortrait = 320.0;
  static const double _dialogHeightLandscape = 250.0;
  static const double _pickerHeightPortrait = 200.0;
  static const double _pickerHeightLandscape = 120.0;

  DateTime _selectedDateTime;

  @override
  void initState() {
    super.initState();
    _selectedDateTime = widget.initialDate;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _selectedDateTime = widget.initialDate;
  }

  @override
  void didUpdateWidget(_DateTimePickerBottomSheetContent oldWidget) {
    super.didUpdateWidget(oldWidget);
    _selectedDateTime = widget.initialDate;
  }

  @override
  Widget build(BuildContext context) {
    final isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    final dialogHeight = isPortrait ? _dialogHeightPortrait : _dialogHeightLandscape;

    return Container(
      height: dialogHeight,
      child: Column(children: [
        Padding(
          padding: EdgeInsets.fromLTRB(Dimens.grid_2x, Dimens.grid_1x, Dimens.grid_2x, Dimens.grid_2x),
          child: Row(children: [
            _buildCancelButton(context),
            Spacer(),
            _buildSaveButton(context),
          ]),
        ),
        _buildDateTimePicker(context),
      ]),
    );
  }

  Widget _buildCancelButton(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.of(context).pop(),
      child: Padding(
        padding: EdgeInsets.all(Dimens.grid_1x),
        child: SmallButtonText(
          S.of(context).common_cancel,
          color: Theme.of(context).colorScheme.primaryVariant,
        ),
      ),
    );
  }

  Widget _buildSaveButton(BuildContext context) {
    return SmallRaisedButton(
      onPressed: () => Navigator.of(context).pop(_selectedDateTime),
      child: SmallButtonText(S.of(context).common_save),
    );
  }

  Widget _buildDateTimePicker(BuildContext context) {
    final isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    final pickerHeight = isPortrait ? _pickerHeightPortrait : _pickerHeightLandscape;

    return Container(
      height: pickerHeight,
      child: CupertinoDatePicker(
        mode: widget.mode,
        initialDateTime: widget.initialDate,
        minimumDate: widget.firstDate,
        maximumDate: widget.lastDate,
        onDateTimeChanged: (DateTime newDateTime) => _selectedDateTime = newDateTime,
        use24hFormat: MediaQuery.of(context).alwaysUse24HourFormat,
        minuteInterval: widget.minuteInterval,
      ),
    );
  }
}
