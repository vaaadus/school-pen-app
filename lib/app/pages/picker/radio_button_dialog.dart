import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/sized_dialog.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/generated/l10n.dart';

typedef RadioButtonDialogItemBuilder<T> = List<RadioButtonDialogItem<T>> Function(BuildContext context);

const double _radioButtonWidth = 24.0;
const double _radioButtonHeight = 24.0;

/// Shows a dialog with multiple radio buttons to select a single value.
/// Returns the selected value or null if cancelled.
Future<T> showRadioButtonDialog<T>({
  @required BuildContext context,
  RouteSettings routeSettings,
  String title,
  String saveButtonText,
  Color saveButtonTextColor,
  Color saveButtonColor,
  @required RadioButtonDialogItemBuilder<T> itemBuilder,
}) {
  return showDialog(
    context: context,
    routeSettings: routeSettings,
    builder: (BuildContext context) => _RadioButtonDialogContent(
      title: title,
      saveButtonText: saveButtonText,
      saveButtonTextColor: saveButtonTextColor,
      saveButtonColor: saveButtonColor,
      itemBuilder: itemBuilder,
    ),
  );
}

class RadioButtonDialogItem<T> extends Equatable {
  final String text;
  final T value;
  final T groupValue;

  const RadioButtonDialogItem({
    @required this.text,
    @required this.value,
    @required this.groupValue,
  });

  @override
  List<Object> get props => [text, value, groupValue];
}

class _RadioButtonDialogContent<T> extends StatefulWidget {
  final String title;
  final String saveButtonText;
  final Color saveButtonTextColor;
  final Color saveButtonColor;
  final RadioButtonDialogItemBuilder<T> itemBuilder;

  const _RadioButtonDialogContent({
    Key key,
    this.title,
    this.saveButtonText,
    this.saveButtonTextColor,
    this.saveButtonColor,
    @required this.itemBuilder,
  }) : super(key: key);

  @override
  _RadioButtonDialogContentState<T> createState() => _RadioButtonDialogContentState<T>();
}

class _RadioButtonDialogContentState<T> extends State<_RadioButtonDialogContent<T>> {
  final List<RadioButtonDialogItem<T>> _items = [];
  T _selectedValue;

  @override
  Widget build(BuildContext context) {
    _initValues(context);

    return SizedDialog(
      padding: EdgeInsets.only(
        top: _showTitle ? Dimens.grid_2x : Dimens.grid_1_5x,
        bottom: Dimens.grid_2x,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (_showTitle) _buildTitle(context),
          if (_showTitle) _buildDivider(context),
          for (RadioButtonDialogItem<T> item in _items)
            _RadioButtonDialogItemWidget(
              text: item.text,
              value: item.value,
              groupValue: _selectedValue,
              onSelected: (T value) {
                setState(() {
                  _selectedValue = value;
                });
              },
            ),
          _buildActions(context),
        ],
      ),
    );
  }

  void _initValues(BuildContext context) {
    _items.clear();
    _items.addAll(widget.itemBuilder(context));

    final List<T> allValues = _items.map((e) => e.value).toList();
    if (!allValues.contains(_selectedValue) || _selectedValue == null) {
      _selectedValue = _items.firstOrNull?.groupValue;
    }
  }

  Widget _buildTitle(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Dimens.grid_2x),
      child: Text(widget.title, style: Theme.of(context).textTheme.headline6),
    );
  }

  Widget _buildDivider(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: Dimens.grid_2x,
        bottom: Dimens.grid_0_5x,
        left: Dimens.grid_2x,
        right: Dimens.grid_2x,
      ),
      child: Divider(),
    );
  }

  Widget _buildActions(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: Dimens.grid_2x,
        left: Dimens.grid_2x,
        right: Dimens.grid_2x,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SmallMaterialButton(
            child: SmallButtonText(
              S.of(context).common_cancel,
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          SmallRaisedButton(
            child: SmallButtonText(
              widget.saveButtonText ?? S.of(context).common_apply,
              color: widget.saveButtonTextColor ?? Theme.of(context).colorScheme.onSecondary,
            ),
            color: widget.saveButtonColor ?? Theme.of(context).colorScheme.secondary,
            onPressed: () => Navigator.of(context).pop(_selectedValue),
          ),
        ],
      ),
    );
  }

  bool get _showTitle => Strings.isNotBlank(widget.title);
}

class _RadioButtonDialogItemWidget<T> extends StatelessWidget {
  final String text;
  final T value;
  final T groupValue;
  final ValueChanged<T> onSelected;

  const _RadioButtonDialogItemWidget({
    Key key,
    @required this.text,
    @required this.value,
    @required this.groupValue,
    @required this.onSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onSelected(value),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: Dimens.grid_1_5x, horizontal: Dimens.grid_3x),
        child: Row(children: [
          Padding(
            padding: EdgeInsets.only(right: Dimens.grid_2x),
            child: Container(
              constraints: BoxConstraints(
                maxWidth: _radioButtonWidth,
                maxHeight: _radioButtonHeight,
              ),
              child: Radio(
                value: value,
                groupValue: groupValue,
                onChanged: (T value) => onSelected(value),
              ),
            ),
          ),
          Expanded(child: Text(text, style: Theme.of(context).textTheme.subtitle1)),
        ]),
      ),
    );
  }
}
