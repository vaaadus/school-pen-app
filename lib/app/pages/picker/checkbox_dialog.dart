import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/sized_dialog.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/generated/l10n.dart';

typedef CheckboxDialogItemBuilder<T> = List<CheckboxDialogItem<T>> Function(BuildContext context);

/// Shows a dialog with multiple checkboxes to select.
/// Returns a map with [CheckboxDialogItem.value] as a key
/// and boolean as a value with picker result or null if cancelled.
Future<Map<T, bool>> showCheckboxDialog<T>({
  @required BuildContext context,
  RouteSettings routeSettings,
  String title,
  String saveButtonText,
  @required CheckboxDialogItemBuilder<T> itemBuilder,
}) {
  return showDialog(
    context: context,
    routeSettings: routeSettings,
    builder: (BuildContext context) => _CheckboxDialogContent(
      title: title,
      saveButtonText: saveButtonText,
      itemBuilder: itemBuilder,
    ),
  );
}

class CheckboxDialogItem<T> extends Equatable {
  final String text;
  final bool defaultValue;
  final T value;

  const CheckboxDialogItem({
    @required this.text,
    this.defaultValue,
    @required this.value,
  });

  @override
  List<Object> get props => [text, defaultValue, value];
}

class _CheckboxDialogContent<T> extends StatefulWidget {
  final String title;
  final String saveButtonText;
  final CheckboxDialogItemBuilder<T> itemBuilder;

  const _CheckboxDialogContent({
    Key key,
    this.title,
    this.saveButtonText,
    @required this.itemBuilder,
  }) : super(key: key);

  @override
  _CheckboxDialogContentState<T> createState() => _CheckboxDialogContentState<T>();
}

class _CheckboxDialogContentState<T> extends State<_CheckboxDialogContent<T>> {
  final Map<T, bool> _values = {};
  final List<CheckboxDialogItem<T>> _items = [];

  @override
  Widget build(BuildContext context) {
    _initValues(context);

    return SizedDialog(
      padding: EdgeInsets.only(
        top: _showTitle ? Dimens.grid_2x : Dimens.grid_1_5x,
        bottom: Dimens.grid_2x,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (_showTitle) _buildTitle(context),
          if (_showTitle) _buildDivider(context),
          for (CheckboxDialogItem<T> item in _items)
            _CheckboxDialogItemWidget(
              text: item.text,
              value: _values[item.value],
              onChange: (bool newValue) {
                setState(() {
                  _values[item.value] = newValue;
                });
              },
            ),
          _buildActions(context),
        ],
      ),
    );
  }

  void _initValues(BuildContext context) {
    _items.clear();
    _items.addAll(widget.itemBuilder(context));

    for (CheckboxDialogItem<T> item in _items) {
      _values[item.value] ??= item.defaultValue ?? false;
    }
  }

  Widget _buildTitle(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Dimens.grid_2x),
      child: Text(widget.title, style: Theme.of(context).textTheme.headline6),
    );
  }

  Widget _buildDivider(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: Dimens.grid_2x,
        bottom: Dimens.grid_0_5x,
        left: Dimens.grid_2x,
        right: Dimens.grid_2x,
      ),
      child: Divider(),
    );
  }

  Widget _buildActions(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: Dimens.grid_4x,
        left: Dimens.grid_2x,
        right: Dimens.grid_2x,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SmallMaterialButton(
            child: SmallButtonText(
              S.of(context).common_cancel,
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          SmallRaisedButton(
            child: SmallButtonText(
              widget.saveButtonText ?? S.of(context).common_apply,
              color: Theme.of(context).colorScheme.onSecondary,
            ),
            color: Theme.of(context).colorScheme.secondary,
            onPressed: () => Navigator.of(context).pop(_values),
          ),
        ],
      ),
    );
  }

  bool get _showTitle => Strings.isNotBlank(widget.title);
}

class _CheckboxDialogItemWidget<T> extends StatelessWidget {
  const _CheckboxDialogItemWidget({
    Key key,
    @required this.text,
    @required this.value,
    @required this.onChange,
  }) : super(key: key);

  final String text;
  final bool value;
  final ValueChanged<bool> onChange;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onChange(!value),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: Dimens.grid_1_5x, horizontal: Dimens.grid_3x),
        child: Row(children: [
          Padding(
            padding: EdgeInsets.only(right: Dimens.grid_2x),
            child: SizedBox(
              width: Dimens.checkboxWidth,
              height: Dimens.checkboxHeight,
              child: Checkbox(value: value, onChanged: onChange),
            ),
          ),
          Expanded(child: Text(text, style: Theme.of(context).textTheme.subtitle1)),
        ]),
      ),
    );
  }
}
