import 'package:flutter/material.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/options_dialog.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/generated/l10n.dart';

/// Shows a dialog to pick a course.
Future<String> showCoursePickerDialog({
  @required BuildContext context,
  RouteSettings routeSettings,
  @required List<Course> courses,
}) async {
  final String addCourseId = 'add_course_id';

  final String result = await showOptionsDialog(
    context: context,
    title: S.of(context).common_pickCourse,
    itemBuilder: (BuildContext context) => [
      for (Course course in courses) OptionDialogItem(text: course.name, value: course.id),
      OptionDialogItem(
        text: S.of(context).common_addCourse,
        value: addCourseId,
        customColor: Theme.of(context).colorScheme.secondary,
      ),
    ],
  );

  final String courseId = result == addCourseId ? (await Nav.courses_add()) : result;
  return courseId;
}
