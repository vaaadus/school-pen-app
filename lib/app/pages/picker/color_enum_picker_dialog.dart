import 'package:flutter/cupertino.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/domain/common/color_enum.dart';

import 'options_dialog.dart';

/// Shows a dialog with colors to select.
/// Returns the selected color or null if cancelled.
Future<ColorEnum> showColorEnumPickerDialog({@required BuildContext context}) {
  return showOptionsDialog(
    context: context,
    itemBuilder: (BuildContext context) => ColorEnum.values
        .map((e) => OptionDialogItem(
              imageAsset24: Images.ic_color_placeholder_24,
              text: ResourceLocator.colorEnumToString(context, e),
              imageColor: ResourceLocator.colorEnumToColor(e),
              value: e,
            ))
        .toList(),
  );
}
