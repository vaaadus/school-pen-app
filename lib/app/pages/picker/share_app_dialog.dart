import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/pages/picker/options_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:share/share.dart';
import 'package:social_share_plugin/social_share_plugin.dart';

typedef _Callback = Future<void> Function(BuildContext context);

/// Whether the current device supports sharing the app.
bool isShareAppDialogSupported() {
  return !kIsWeb && (Platform.isAndroid || Platform.isIOS);
}

/// Shows a dialog which offers the user to share the app with his friends.
Future<void> showShareAppDialog(BuildContext context) async {
  final _Callback result = await showOptionsDialog(
    context: context,
    routeSettings: RouteSettings(name: R.settings_shareApp),
    title: S.of(context).common_shareApp,
    itemBuilder: (BuildContext context) => [
      OptionDialogItem(
        imageAsset24: Images.ic_facebook_blue_24,
        text: S.of(context).common_facebook,
        imageColor: Colors.transparent,
        value: _shareOnFacebook,
      ),
      OptionDialogItem(
        imageAsset24: Images.ic_twitter_blue_24,
        text: S.of(context).common_twitter,
        imageColor: Colors.transparent,
        value: _shareOnTwitter,
      ),
      if (kDebugMode)
        OptionDialogItem(
          imageAsset24: Images.ic_instagram_gradient_24,
          text: S.of(context).common_instagram,
          imageColor: Colors.transparent,
          value: _shareOnInstagram,
        ),
      if (kDebugMode)
        OptionDialogItem(
          imageAsset24: Images.ic_whatsapp_green_24,
          text: S.of(context).common_whatsapp,
          imageColor: Colors.transparent,
          value: _shareOnWhatsApp,
        ),
      OptionDialogItem(
        imageAsset24: Images.ic_external_24,
        text: S.of(context).common_otherApps,
        value: _shareWithOtherApps,
      ),
    ],
  );

  if (result != null) {
    await result.call(context);
  }
}

Future<void> _shareOnFacebook(BuildContext context) async {
  await SocialSharePlugin.shareToFeedFacebookLink(
    url: R.appHomePageUrl,
    quote: S.of(context).shareAppOptions_quote,
  );
}

Future<void> _shareOnTwitter(BuildContext context) async {
  await SocialSharePlugin.shareToTwitterLink(
    url: R.appHomePageUrl,
    text: S.of(context).shareAppOptions_quote,
  );
}

Future<void> _shareOnInstagram(BuildContext context) async {
  // TODO prepare an image with qr code to our website, localize it and share.
}

Future<void> _shareOnWhatsApp(BuildContext context) async {
  // TODO share on whatsapp
}

Future<void> _shareWithOtherApps(BuildContext context) async {
  await Share.share(_buildShareText(context));
}

String _buildShareText(BuildContext context) {
  return S.of(context).shareAppOptions_quote + '\n\n' + R.appHomePageUrl;
}
