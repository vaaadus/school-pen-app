import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/sized_dialog.dart';
import 'package:school_pen/app/widgets/text_field_container.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/generated/l10n.dart';

/// Callback which is called when a user submits the text field.
/// Return an error from here to handle the validation.
/// The resulting exception will be mapped to string using [ResourceLocator.errorToText].
typedef StringValidator = Exception Function(String text);

/// Shows a dialog with title, message and a text field to enter the text.
/// Returns a future which resolves to the entered text if it is valid, see: [StringValidator].
/// Returns null if cancelled.
Future<String> showTextFieldDialog({
  @required BuildContext context,
  RouteSettings routeSettings,
  String initialText,
  String title,
  String message,
  StringValidator validator = alwaysValid,
  String submitText,
  TextInputType keyboardType,
  Set<String> autofillHints,
  String placeholder,
}) {
  return showDialog(
    context: context,
    routeSettings: routeSettings,
    builder: (BuildContext context) => _TextFieldDialog(
      initialText: initialText,
      title: title,
      message: message,
      validator: validator,
      submitText: submitText,
      keyboardType: keyboardType,
      autofillHints: autofillHints,
      placeholder: placeholder,
    ),
  );
}

class _TextFieldDialog extends StatefulWidget {
  const _TextFieldDialog({
    Key key,
    this.initialText,
    this.title,
    this.message,
    @required this.validator,
    this.submitText,
    this.keyboardType,
    this.autofillHints,
    this.placeholder,
  }) : super(key: key);

  final String initialText;
  final String title;
  final String message;
  final StringValidator validator;
  final String submitText;
  final TextInputType keyboardType;
  final Set<String> autofillHints;
  final String placeholder;

  @override
  _TextFieldDialogState createState() => _TextFieldDialogState();
}

class _TextFieldDialogState extends State<_TextFieldDialog> {
  final TextEditingController _controller = TextEditingController();
  Exception _error;

  @override
  void initState() {
    super.initState();
    _controller.text = widget.initialText ?? '';
  }

  @override
  void didUpdateWidget(_TextFieldDialog oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.initialText != widget.initialText) {
      _controller.text = widget.initialText ?? '';
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedDialog(
      padding: EdgeInsets.all(Dimens.grid_2x),
      child: Column(children: [
        if (Strings.isNotBlank(widget.title))
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              widget.title,
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
        if (Strings.isNotBlank(widget.title))
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_2x),
            child: Divider(),
          ),
        if (Strings.isNotBlank(widget.message))
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_2x),
            child: Text(
              widget.message,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x),
          child: TextFieldContainer(
            width: 280,
            maxHeight: TextFieldContainer.miniHeight,
            child: TextField(
              controller: _controller,
              autofocus: true,
              style: Theme.of(context).textTheme.headline6,
              keyboardType: widget.keyboardType ?? TextInputType.text,
              autofillHints: widget.autofillHints,
              decoration: InputDecoration(labelText: widget.placeholder),
              textInputAction: TextInputAction.done,
              onSubmitted: (String text) => _onSubmit(context),
            ),
            error: TextFieldError.forError(_error),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SmallMaterialButton(
                child: SmallButtonText(
                  S.of(context).common_cancel,
                  color: Theme.of(context).colorScheme.primaryVariant,
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              SmallRaisedButton(
                child: SmallButtonText(widget.submitText ?? S.of(context).common_send),
                onPressed: () => _onSubmit(context),
              ),
            ],
          ),
        ),
      ]),
    );
  }

  void _onSubmit(BuildContext context) {
    final String text = _controller.text;
    final Exception error = widget.validator(text);
    if (error != null) {
      setState(() => _error = error);
    } else {
      Navigator.of(context).pop(text);
    }
  }
}

Exception alwaysValid(String text) => null;
