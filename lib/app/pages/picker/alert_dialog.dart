import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/sized_dialog.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

/// Shows a default alert dialog with title, message and actions (buttons).
Future<T> showAlertDialog<T>({
  @required BuildContext context,
  RouteSettings routeSettings,
  String title,
  String message,
  @required DialogActionData primaryAction,
  DialogActionData secondaryAction,
}) {
  return showDialog(
    context: context,
    routeSettings: routeSettings,
    builder: (BuildContext context) {
      return _AlertDialogContent(
        title: title,
        message: message,
        primaryAction: primaryAction,
        secondaryAction: secondaryAction,
      );
    },
  );
}

class DialogActionData extends Equatable {
  final String text;
  final GestureTapCallback onTap;
  final bool isDestructive;

  const DialogActionData({
    @required this.text,
    this.onTap,
    this.isDestructive = false,
  });

  @override
  List<Object> get props => [text, onTap, isDestructive];
}

class _AlertDialogContent extends StatelessWidget {
  const _AlertDialogContent({
    Key key,
    this.title,
    this.message,
    @required this.primaryAction,
    this.secondaryAction,
  }) : super(key: key);

  final String title;
  final String message;
  final DialogActionData primaryAction;
  final DialogActionData secondaryAction;

  @override
  Widget build(BuildContext context) {
    return SizedDialog(
      padding: EdgeInsets.all(Dimens.grid_2x),
      child: Column(children: [
        if (_showTitle) _buildTitle(context),
        if (_showDivider) _buildDivider(context),
        if (_showMessage) _buildMessage(context),
        _buildActions(context),
      ]),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(title, style: Theme.of(context).textTheme.headline6),
    );
  }

  Widget _buildDivider(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
      child: Divider(),
    );
  }

  Widget _buildMessage(BuildContext context) {
    return Text(message, style: Theme.of(context).textTheme.bodyText2);
  }

  Widget _buildActions(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: _showMessage ? Dimens.grid_2x : Dimens.grid_4x),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          if (secondaryAction != null) _buildSecondaryAction(context),
          if (primaryAction != null) _buildPrimaryAction(context),
        ],
      ),
    );
  }

  Widget _buildSecondaryAction(BuildContext context) {
    final ColorScheme colors = Theme.of(context).colorScheme;

    return SmallMaterialButton(
      child: SmallButtonText(
        secondaryAction.text,
        color: secondaryAction.isDestructive ? colors.error : colors.primaryVariant,
      ),
      onPressed: () {
        Navigator.of(context).pop();
        secondaryAction.onTap?.call();
      },
    );
  }

  Widget _buildPrimaryAction(BuildContext context) {
    final ColorScheme colors = Theme.of(context).colorScheme;

    return SmallRaisedButton(
      child: SmallButtonText(
        primaryAction.text,
        color: primaryAction.isDestructive ? colors.onError : colors.onSecondary,
      ),
      color: primaryAction.isDestructive ? colors.error : colors.secondary,
      onPressed: () {
        Navigator.of(context).pop();
        primaryAction.onTap?.call();
      },
    );
  }

  bool get _showTitle => Strings.isNotBlank(title);

  bool get _showDivider => Strings.isNotBlank(title) && Strings.isNotBlank(message);

  bool get _showMessage => Strings.isNotBlank(message);
}
