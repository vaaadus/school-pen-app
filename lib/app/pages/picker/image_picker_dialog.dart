import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:path/path.dart' as path;
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/generated/l10n.dart';

import 'options_dialog.dart';

typedef ImageProvider = Future<ImagePickerResult> Function();

const Logger _logger = Logger('ImagePicker');

enum ImagePickerPreferredCamera {
  front,
  rear,
}

/// Whether the current device/platform supports the camera.
bool isCameraSupported() {
  return !kIsWeb && (Platform.isAndroid || Platform.isIOS);
}

/// Shows the flow to pick/take an image. If the result of the
/// future is null, then it means user cancelled the picker.
/// If the result does not contain the image path, then it means user decided to remove the image.
Future<ImagePickerResult> showImagePicker({
  @required BuildContext context,
  RouteSettings routeSettings,
  bool showDeleteOption = false,
  ImagePickerPreferredCamera cameraPreference = ImagePickerPreferredCamera.rear,
}) async {
  // just a single option available
  if (!isCameraSupported() && !showDeleteOption) {
    return _chooseFromGallery();
  }

  final ImageProvider imageProvider = await showOptionsDialog(
    context: context,
    routeSettings: routeSettings,
    title: S.of(context).common_avatar,
    itemBuilder: (BuildContext context) => [
      if (isCameraSupported())
        OptionDialogItem(
          imageAsset24: Images.ic_camera_24,
          text: S.of(context).common_takePhoto,
          value: () => _takePhoto(cameraPreference),
        ),
      OptionDialogItem(
        imageAsset24: Images.ic_image_24,
        text: S.of(context).common_gallery,
        value: _chooseFromGallery,
      ),
      if (showDeleteOption)
        OptionDialogItem(
          imageAsset24: Images.ic_trash_24,
          text: S.of(context).common_deleteCurrentPhoto,
          customColor: Theme.of(context).colorScheme.error,
          value: _deletePhoto,
        ),
    ],
  );

  return imageProvider?.call();
}

Future<ImagePickerResult> _takePhoto(ImagePickerPreferredCamera preferredCamera) async {
  final PickedFile file = await ImagePicker().getImage(
    source: ImageSource.camera,
    preferredCameraDevice: _asCameraDevice(preferredCamera),
  );

  return _readImagePickerResult(file);
}

Future<ImagePickerResult> _chooseFromGallery() async {
  try {
    final FilePickerCross file = await FilePickerCross.importFromStorage(type: FileTypeCross.image);
    final ImagePickerResult result = await _readFilePickerResult(file);
    if (_mimeTypeMatchesImage(result?.file)) return result;
    return null;
  } on FileSelectionCanceledError {
    return null;
  } catch (error, stackTrace) {
    _logger.logError(error, stackTrace);
    return null;
  }
}

Future<ImagePickerResult> _deletePhoto() async {
  return ImagePickerResult(file: null);
}

Future<ImagePickerResult> _readImagePickerResult(PickedFile file) async {
  if (file == null) return null;

  final MemoryFile memoryFile = MemoryFile(
    name: path.basename(file.path),
    localPath: file.path,
    bytes: await file.readAsBytes(),
  );

  return ImagePickerResult(file: memoryFile);
}

Future<ImagePickerResult> _readFilePickerResult(FilePickerCross file) async {
  if (file == null) return null;
  if (file.type != FileTypeCross.image) return null;

  final MemoryFile memoryFile = MemoryFile(
    name: path.basename(file.path),
    localPath: file.path,
    bytes: file.toUint8List(),
  );

  return ImagePickerResult(file: memoryFile);
}

bool _mimeTypeMatchesImage(MemoryFile file) {
  if (file == null) return false;

  final String mimeType = lookupMimeType(file.localPath, headerBytes: file.bytes);
  if (Strings.isBlank(mimeType)) return false;

  return RegExp('image/.+').hasMatch(mimeType);
}

CameraDevice _asCameraDevice(ImagePickerPreferredCamera camera) {
  switch (camera) {
    case ImagePickerPreferredCamera.front:
      return CameraDevice.front;
    case ImagePickerPreferredCamera.rear:
      return CameraDevice.rear;
  }
  throw ArgumentError('Unsupported camera: $camera');
}

class ImagePickerResult extends Equatable {
  final MemoryFile file;

  const ImagePickerResult({this.file});

  @override
  List<Object> get props => [file];
}
