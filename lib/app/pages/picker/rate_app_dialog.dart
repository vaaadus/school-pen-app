import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:school_pen/app/common/intents.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/domain/common/env_options.dart';
import 'package:school_pen/generated/l10n.dart';

bool isRateAppDialogSupported() {
  return [
    TargetStore.play,
    TargetStore.iOS,
    TargetStore.amazon,
  ].contains(EnvOptions.store);
}

/// Shows a dialog to rate the app or redirects to the store.
/// Returns a [Future] which resolves to true if user should not be prompted again to rate the app, false otherwise.
Future<bool> showRateAppDialog(BuildContext context) async {
  switch (EnvOptions.store) {
    case TargetStore.play:
    case TargetStore.iOS:
      await InAppReview.instance.requestReview();
      return false;

    case TargetStore.amazon:
      final bool openStore = await _showCustomDialog(context);
      if (openStore) {
        await Intents.openStore();
        return true;
      }
      return false;

    case TargetStore.macOS:
    case TargetStore.windows:
    case TargetStore.linux:
    case TargetStore.web:
      return false;
  }

  throw Exception('Store not supported: ${EnvOptions.store}');
}

/// Returns true if user would like to rate the app, false otherwise.
Future<bool> _showCustomDialog(BuildContext context) async {
  bool result = false;

  await showAlertDialog(
    context: context,
    title: S.of(context).rateApp_title,
    message: S.of(context).rateApp_message,
    primaryAction: DialogActionData(
      text: S.of(context).common_rateUs,
      onTap: () => result = true,
    ),
    secondaryAction: DialogActionData(
      text: S.of(context).common_dismiss,
      onTap: () => result = false,
    ),
  );

  return result;
}
