import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/agenda/agenda_listener.dart';
import 'package:school_pen/domain/agenda/agenda_manager.dart';
import 'package:school_pen/domain/agenda/model/agenda_event.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/smartnote/smart_note.dart';
import 'package:school_pen/domain/smartnote/smart_note_listener.dart';
import 'package:school_pen/domain/smartnote/smart_note_manager.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/teacher_listener.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/timetable_listener.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';

class HomeModel extends Equatable {
  final SmartNote smartNote;
  final bool showEmptyState;
  final bool showDoneState;
  final DateTime dateTime;
  final TimetableInfo activeTimetable;
  final List<Course> courses;
  final List<Teacher> teachers;
  final List<AgendaEvent> eventsRightNow;
  final List<AgendaEvent> eventsLaterThisDay;
  final List<AgendaEvent> eventsDone;

  const HomeModel({
    @required this.smartNote,
    @required this.showEmptyState,
    @required this.showDoneState,
    @required this.dateTime,
    this.activeTimetable,
    @required this.courses,
    @required this.teachers,
    @required this.eventsRightNow,
    @required this.eventsLaterThisDay,
    @required this.eventsDone,
  });

  @override
  List<Object> get props => [
        smartNote,
        showEmptyState,
        showDoneState,
        dateTime,
        activeTimetable,
        courses,
        teachers,
        eventsRightNow,
        eventsLaterThisDay,
        eventsDone,
      ];
}

class HomeViewModel extends ViewModel<HomeModel, Object>
    implements
        SmartNoteListener,
        AgendaListener,
        SchoolYearListener,
        CourseListener,
        TeacherListener,
        TimetableListener {
  static const Logger _logger = Logger('HomeViewModel');

  final SmartNoteManager _smartNoteManager;
  final AgendaManager _agendaManager;
  final SchoolYearManager _schoolYearManager;
  final CourseManager _courseManager;
  final TeacherManager _teacherManager;
  final TimetableManager _timetableManager;

  DateTime _dateTime = DateTime.now();

  HomeViewModel(
    this._smartNoteManager,
    this._agendaManager,
    this._schoolYearManager,
    this._courseManager,
    this._teacherManager,
    this._timetableManager,
  );

  @override
  void onStart() async {
    super.onStart();
    _smartNoteManager.addSmartNoteListener(this, notifyOnAttach: false);
    _agendaManager.addAgendaListener(this, notifyOnAttach: false);
    _schoolYearManager.addSchoolYearListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _teacherManager.addTeacherListener(this, notifyOnAttach: false);
    _timetableManager.addTimetableListener(this, notifyOnAttach: false);

    _dateTime = DateTime.now();
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _smartNoteManager.removeSmartNoteListener(this);
    _agendaManager.removeAgendaListener(this);
    _schoolYearManager.removeSchoolYearListener(this);
    _courseManager.removeCourseListener(this);
    _teacherManager.removeTeacherListener(this);
    _timetableManager.removeTimetableListener(this);
    super.onStop();
  }

  @override
  void onSmartNotesChanged(SmartNoteManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onAgendaChanged(AgendaManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onSchoolYearsChanged(SchoolYearManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onTeachersChanged(TeacherManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onTimetablesChanged(TimetableManager manager) async {
    await _tryEmitModel();
  }

  void onSelectDate(DateTime dateTime) async {
    _dateTime = dateTime;

    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<HomeModel> get _model async {
    final SmartNote smartNote = await _smartNoteManager.smartNote;
    final SchoolYear year = await _schoolYearManager.getActive();

    final Future<TimetableInfo> activeTimetableFuture = _timetableManager.getActiveTimetableInfo();
    final Future<List<Course>> coursesFuture = _courseManager.getAll(year.id);
    final Future<List<Teacher>> teachersFuture = _teacherManager.getAll(schoolYearId: year.id);

    final TimetableInfo activeTimetable = await activeTimetableFuture;
    final List<Course> courses = await coursesFuture;
    final List<Teacher> teachers = await teachersFuture;
    final List<AgendaEvent> events = await _agendaManager.getEvents(_dateTime);

    final DateTime now = DateTime.now();
    final List<AgendaEvent> eventsRightNow = [];
    final List<AgendaEvent> eventsLaterThisDay = [];
    final List<AgendaEvent> eventsDone = [];

    _assignEventsInto(events, eventsRightNow, eventsLaterThisDay, eventsDone, now);

    eventsRightNow.sort();
    eventsLaterThisDay.sort();
    eventsDone.sort();

    final bool showEmptyState = eventsRightNow.isEmpty && eventsLaterThisDay.isEmpty && eventsDone.isEmpty;
    final bool showDoneState = !showEmptyState && eventsRightNow.isEmpty && eventsLaterThisDay.isEmpty;

    return HomeModel(
      smartNote: smartNote,
      showEmptyState: showEmptyState,
      showDoneState: showDoneState,
      dateTime: _dateTime,
      activeTimetable: activeTimetable,
      courses: courses,
      teachers: teachers,
      eventsRightNow: eventsRightNow,
      eventsLaterThisDay: eventsLaterThisDay,
      eventsDone: eventsDone,
    );
  }

  void _assignEventsInto(List<AgendaEvent> events, List<AgendaEvent> eventsRightNow,
      List<AgendaEvent> eventsLaterThisDay, List<AgendaEvent> eventsDone, DateTime now) {
    final DateTimeRange range = DateTimeRange(
      start: now.subtract(Duration(minutes: 5)),
      end: now.add(Duration(minutes: 10)),
    );

    for (AgendaEvent event in events) {
      if (event.isDoneAt(range.start)) {
        eventsDone.add(event);
      } else if (event.dateTimeRange.start.isAfter(range.end)) {
        eventsLaterThisDay.add(event);
      } else {
        eventsRightNow.add(event);
      }
    }
  }
}
