import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:school_pen/app/common/intents.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/domain/smartnote/smart_note.dart';
import 'package:school_pen/domain/smartnote/smart_note_manager.dart';
import 'package:school_pen/generated/l10n.dart';

class SmartNoteData extends Equatable {
  final String title;
  final String body;
  final String negativeButtonText;
  final String positiveButtonText;
  final ValueChanged<BuildContext> onNegativeAction;
  final ValueChanged<BuildContext> onPositiveAction;

  const SmartNoteData({
    @required this.title,
    @required this.body,
    this.negativeButtonText,
    @required this.positiveButtonText,
    this.onNegativeAction,
    @required this.onPositiveAction,
  });

  @override
  List<Object> get props => [title, body, negativeButtonText, positiveButtonText, onNegativeAction, onPositiveAction];
}

class SmartNoteMapper {
  static SmartNoteManager get _manager => Injection.get();

  static SmartNoteData map({
    @required BuildContext context,
    @required SmartNote smartNote,
    @required VoidCallback onViewCourses,
  }) {
    switch (smartNote) {
      case SmartNote.pendingBilling:
        return SmartNoteData(
          title: S.of(context).smartNote_pendingBilling_title,
          body: S.of(context).smartNote_pendingBilling_body,
          positiveButtonText: S.of(context).common_manage,
          negativeButtonText: S.of(context).common_dismiss,
          onPositiveAction: (BuildContext context) => Intents.openManageInAppSubscriptionsPage(),
          onNegativeAction: (BuildContext context) => _manager.onPostponePendingBillingNote(),
        );
      case SmartNote.loginOffer:
        return SmartNoteData(
          title: S.of(context).smartNote_loginOffer_title,
          body: S.of(context).smartNote_loginOffer_body,
          positiveButtonText: S.of(context).common_login,
          negativeButtonText: S.of(context).common_dismiss,
          onPositiveAction: (BuildContext context) => Nav.loginOptions(),
          onNegativeAction: (BuildContext context) => _manager.onPostponeLoginOfferNote(),
        );
      case SmartNote.missedWork:
        return SmartNoteData(
          title: S.of(context).smartNote_missedWork_title,
          body: S.of(context).smartNote_missedWork_body,
          positiveButtonText: S.of(context).common_viewMore,
          onPositiveAction: (BuildContext context) => onViewCourses(),
        );
    }

    throw ArgumentError('Unsupported smart note: $smartNote');
  }
}
