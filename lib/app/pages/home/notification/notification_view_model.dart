import 'package:equatable/equatable.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/logger/logger.dart';

class NotificationModel extends Equatable {
  const NotificationModel();

  @override
  List<Object> get props => [];
}

class NotificationViewModel extends ViewModel<NotificationModel, Object> {
  static const Logger _logger = Logger('NotificationViewModel');

  @override
  void onStart() async {
    super.onStart();
    await _tryEmitModel();
  }

  @override
  void onStop() {
    super.onStop();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<NotificationModel> get _model async {
    return NotificationModel();
  }
}
