import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/pages/home/notification/notification_view_model.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/page_placeholder.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/generated/l10n.dart';

/// Presents a list of notifications to the user.
class NotificationPage extends LifecycleWidget {
  final bool fullscreen;

  const NotificationPage({Key key, this.fullscreen = true}) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _NotificationPageState();
}

class _NotificationPageState extends LifecycleWidgetState<NotificationPage>
    with ViewModelLifecycle<NotificationViewModel, NotificationPage>, SingleTickerProviderStateMixin {
  @override
  NotificationViewModel provideViewModel() => NotificationViewModel();

  @override
  Widget build(BuildContext context) {
    Widget result = Scaffold(
      body: SafeArea(
        child: ViewModelBuilder(
          viewModel: viewModel,
          builder: _buildContent,
        ),
      ),
    );

    if (widget.fullscreen) {
      result = SystemUiOverlayAnnotation(child: result);
    }

    return result;
  }

  Widget _buildContent(BuildContext context, NotificationModel model) {
    return CustomScrollView(slivers: [
      _buildToolbar(context, model),
      SliverFillRemaining(
        child: PagePlaceholder(
          imageAsset: Images.illustration_megaphone_128,
          imageWidth: 128,
          imageHeight: 128,
          message: S.of(context).notifications_emptyState_message,
        ),
      ),
    ]);
  }

  Widget _buildToolbar(BuildContext context, NotificationModel model) {
    return Toolbar(
      vsync: this,
      navType: widget.fullscreen ? NavButtonType.close : null,
      title: S.of(context).common_notifications,
    );
  }
}
