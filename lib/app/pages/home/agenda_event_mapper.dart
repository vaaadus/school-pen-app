import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/formatter/timetable_event_formatter.dart';
import 'package:school_pen/app/widgets/bordered_list_item.dart';
import 'package:school_pen/domain/agenda/model/agenda_event.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';

class AgendaEventDataMapper {
  static BorderedListItemData map(AgendaEvent event, List<Course> courses, bool inactive) {
    if (event is AgendaTimetableEvent) {
      return _mapTimetableEvent(event, courses, inactive);
    } else if (event is AgendaNoteEvent) {
      return _mapNoteEvent(event, inactive);
    } else if (event is AgendaExamEvent) {
      return _mapExamEvent(event, courses, inactive);
    } else if (event is AgendaHomeworkEvent) {
      return _mapHomeworkEvent(event, courses, inactive);
    } else if (event is AgendaReminderEvent) {
      return _mapReminderEvent(event, courses, inactive);
    }

    throw ArgumentError('Unsupported event type: $event');
  }

  static BorderedListItemData _mapTimetableEvent(AgendaTimetableEvent event, List<Course> courses, bool inactive) {
    final Course course = courses.firstWhereOrNull((e) => e.id == event.raw.courseId);

    return BorderedListItemData(
      thumbnailImageAsset16: Images.ic_timetable_24,
      thumbnailColor: TimetableEventFormatter.getColor(event.raw, course),
      title: TimetableEventFormatter.formatCourseName(event.raw, course),
      label: TimetableEventFormatter.formatTimeRange(event.raw.timeRange),
      aside: TimetableEventFormatter.formatRoom(event.raw, course),
      inactive: inactive,
    );
  }

  static BorderedListItemData _mapNoteEvent(AgendaNoteEvent event, bool inactive) {
    return BorderedListItemData(
      thumbnailImageAsset16: Images.ic_idea_24,
      thumbnailColor: ResourceLocator.colorEnumToColor(event.note.color) ?? AppColors.orange1,
      title: Strings.substringUpTo(event.note.headline, 30),
      label: event.note.notificationDate.time.toString(),
      inactive: inactive,
    );
  }

  static BorderedListItemData _mapExamEvent(AgendaExamEvent event, List<Course> courses, bool inactive) {
    final Course course = courses.firstWhereOrNull((e) => e.id == event.exam.courseId);

    return BorderedListItemData(
      thumbnailImageAsset16: Images.ic_exam_24,
      thumbnailColor: ResourceLocator.colorEnumToColor(course?.color),
      title: Strings.substringUpTo(event.exam.headline, 30),
      label: event.exam.dateTime.time.toString(),
      inactive: inactive,
    );
  }

  static BorderedListItemData _mapHomeworkEvent(AgendaHomeworkEvent event, List<Course> courses, bool inactive) {
    final Course course = courses.firstWhereOrNull((e) => e.id == event.homework.courseId);

    return BorderedListItemData(
      thumbnailImageAsset16: Images.ic_homework_24,
      thumbnailColor: ResourceLocator.colorEnumToColor(course?.color),
      title: Strings.substringUpTo(event.homework.headline, 30),
      label: event.homework.dateTime.time.toString(),
      inactive: inactive,
    );
  }

  static BorderedListItemData _mapReminderEvent(AgendaReminderEvent event, List<Course> courses, bool inactive) {
    final Course course = courses.firstWhereOrNull((e) => e.id == event.reminder.courseId);

    return BorderedListItemData(
      thumbnailImageAsset16: Images.ic_notification_24,
      thumbnailColor: ResourceLocator.colorEnumToColor(course?.color),
      title: Strings.substringUpTo(event.reminder.headline, 30),
      label: event.reminder.dateTime.time.toString(),
      inactive: inactive,
    );
  }
}
