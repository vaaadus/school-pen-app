import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/anim/animations.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/home/agenda_event_mapper.dart';
import 'package:school_pen/app/pages/home/home_view_model.dart';
import 'package:school_pen/app/pages/home/notification/notification_page.dart';
import 'package:school_pen/app/pages/home/smart_note_mapper.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/pages/picker/options_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/pages/timetable/timetable_event_details_dialog.dart';
import 'package:school_pen/app/pages/timetable/timetable_event_widget.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/bordered_container.dart';
import 'package:school_pen/app/widgets/bordered_list_item.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/horizontal_progress_bar.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/master_detail_container.dart';
import 'package:school_pen/app/widgets/page_placeholder.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/agenda/model/agenda_event.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/smartnote/smart_note.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

const double _dayItemWidth = 56.0;
const double _dayItemHeight = 64.0;
const double _dayItemPadding = Dimens.grid_2x;

/// Presents the dashboard to the user.
class HomePage extends LifecycleWidget {
  final VoidCallback onManageTimetables;
  final VoidCallback onSearch;
  final VoidCallback onViewCourses;

  const HomePage({
    Key key,
    @required this.onManageTimetables,
    @required this.onSearch,
    @required this.onViewCourses,
  }) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _HomePageState();
}

class _HomePageState extends LifecycleWidgetState<HomePage>
    with ViewModelLifecycle<HomeViewModel, HomePage>, SingleTickerProviderStateMixin {
  static const int _initialDayPosition = 100; // allow to go that many days back

  final ScrollController _daysController = ScrollController(initialScrollOffset: _calculateDayScrollOffset(_initialDayPosition));

  @override
  HomeViewModel provideViewModel() {
    return HomeViewModel(Injection.get(), Injection.get(), Injection.get(), Injection.get(), Injection.get(), Injection.get());
  }

  @override
  void dispose() {
    _daysController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Widget scaffold = Scaffold(
      body: SafeArea(
        child: ViewModelBuilder(
          viewModel: viewModel,
          builder: _buildContent,
        ),
      ),
      floatingActionButton: _buildFloatingButton(context),
    );

    if (_shouldShowDesktopLayout(context)) {
      return MasterDetailContainer(
        masterFlex: Adaptive.of(context).isDesktop ? 7 : 6,
        detailFlex: 4,
        masterBuilder: (BuildContext context) => scaffold,
        detailBuilder: (BuildContext context) => NotificationPage(fullscreen: false),
      );
    } else {
      return scaffold;
    }
  }

  Widget _buildFloatingButton(BuildContext context) {
    return SpeedDial(
      animatedIcon: AnimatedIcons.add_event,
      animatedIconTheme: IconThemeData(size: Dimens.grid_3x),
      heroTag: null,
      tooltip: S.of(context).common_add,
      backgroundColor: Theme.of(context).colorScheme.secondary,
      foregroundColor: Theme.of(context).colorScheme.onSecondary,
      overlayColor: Theme.of(context).colorScheme.background,
      overlayOpacity: 0.85,
      curve: Curves.easeOutCubic,
      children: [
        _buildFloatingButtonChild(
          context: context,
          imageAsset: Images.ic_homework_24,
          label: S.of(context).common_homework,
          onTap: _onAddHomework,
        ),
        _buildFloatingButtonChild(
          context: context,
          imageAsset: Images.ic_exam_24,
          label: S.of(context).common_exam,
          onTap: _onAddExam,
        ),
        _buildFloatingButtonChild(
          context: context,
          imageAsset: Images.ic_notification_24,
          label: S.of(context).common_reminder,
          onTap: _onAddReminder,
        ),
        _buildFloatingButtonChild(
          context: context,
          imageAsset: Images.ic_timetable_24,
          label: S.of(context).common_lesson,
          onTap: _onAddLesson,
        ),
      ],
    );
  }

  SpeedDialChild _buildFloatingButtonChild({
    @required BuildContext context,
    @required String imageAsset,
    @required String label,
    @required VoidCallback onTap,
  }) {
    return SpeedDialChild(
      child: Center(
        child: Image.asset(
          imageAsset,
          color: Theme.of(context).colorScheme.secondary,
          width: 16,
          height: 16,
        ),
      ),
      label: label,
      labelStyle: Theme.of(context).textTheme.caption.copyWith(color: AppColors.gray1),
      labelBackgroundColor: AppColors.white,
      backgroundColor: AppColors.white,
      onTap: onTap,
    );
  }

  void _onAddLesson() async {
    final HomeModel model = await viewModel.lastModel;
    if (model == null) return;

    if (model.activeTimetable == null || model.activeTimetable.type != TimetableType.custom) {
      widget.onManageTimetables();
    } else {
      Nav.timetable_eventAdd(timetableId: model.activeTimetable.id);
    }
  }

  void _onAddHomework() {
    Nav.homework_add();
  }

  void _onAddExam() {
    Nav.exams_add();
  }

  void _onAddReminder() {
    Nav.reminders_add();
  }

  Widget _buildContent(BuildContext context, HomeModel model) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final Paddings paddings = Paddings.of(context);
      final bool expandedHeader = _shouldShowExpandedHeader(context);

      return CustomScrollView(slivers: [
        _buildToolbar(
          context: context,
          model: model,
          constraints: constraints,
          includeHeader: expandedHeader,
        ),
        if (model.smartNote != null)
          SliverToBoxAdapter(
            child: _buildSmartNote(context, model.smartNote),
          ),
        if (!expandedHeader)
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(
                left: paddings.horizontal + Dimens.grid_0_5x,
                right: paddings.horizontal + Dimens.grid_0_5x,
                top: model.smartNote != null ? Dimens.grid_2x : 0,
              ),
              child: _buildHeader(context, model),
            ),
          ),
        SliverToBoxAdapter(
          child: _buildDaysList(context, model, constraints),
        ),
        if (model.showEmptyState)
          SliverFillRemaining(
            child: PagePlaceholder(
              imageAsset: Images.illustration_beach_128,
              imageWidth: 128,
              imageHeight: 128,
              message: S.of(context).home_emptyState_message,
            ),
          ),
        if (!model.showEmptyState)
          SliverList(
            delegate: SliverChildListDelegate([
              if (!model.showDoneState) _buildProgressSection(context, model),
              if (model.showDoneState) _buildDoneSection(context),
              if (model.eventsRightNow.isNotEmpty) ...[
                _buildEventsHeader(context, S.of(context).common_rightNow),
                _buildEvents(context: context, model: model, events: model.eventsRightNow),
              ],
              if (model.eventsLaterThisDay.isNotEmpty) ...[
                _buildEventsHeader(context, S.of(context).common_laterThisDay),
                _buildEvents(context: context, model: model, events: model.eventsLaterThisDay),
              ],
              if (model.eventsDone.isNotEmpty) ...[
                _buildEventsHeader(context, S.of(context).common_done),
                _buildEvents(context: context, model: model, events: model.eventsDone, inactive: true),
              ],
              Container(height: Dimens.grid_10x),
            ]),
          ),
      ]);
    });
  }

  Widget _buildToolbar({
    @required BuildContext context,
    @required HomeModel model,
    @required BoxConstraints constraints,
    @required bool includeHeader,
  }) {
    final Adaptive adaptive = Adaptive.of(context);
    final bool desktopLayout = _shouldShowDesktopLayout(context);

    return Toolbar(
      vsync: this,
      navType: null,
      title: includeHeader ? _getHeaderText(context, model) : null,
      actions: [
        if (desktopLayout)
          BorderedContainer(
            padding: EdgeInsets.symmetric(
              vertical: Dimens.grid_1x,
              horizontal: Dimens.grid_1_5x,
            ),
            color: Theme.of(context).colorScheme.surface,
            child: SmallButtonText(
              S.of(context).common_today,
              color: Theme.of(context).colorScheme.primary,
            ),
            onTap: () => viewModel.onSelectDate(DateTime.now()),
          ),
        if (adaptive.isDesktop)
          BorderedImageButton(
            imageAsset: Images.ic_arrow_left_24,
            tooltip: S.of(context).common_previousPage,
            onTap: () => _animateDaysListBy(-constraints.maxWidth * 0.75),
          ),
        if (adaptive.isDesktop)
          BorderedImageButton(
            imageAsset: Images.ic_arrow_right_24,
            tooltip: S.of(context).common_nextPage,
            onTap: () => _animateDaysListBy(constraints.maxWidth * 0.75),
          ),
        BorderedImageButton(
          imageAsset: Images.ic_calendar_today_24,
          tooltip: S.of(context).common_goToDate,
          onTap: () {
            if (desktopLayout) {
              _showGoToDateCalendarPicker(context, model);
            } else {
              _showGoToDateDialog(context, model);
            }
          },
        ),
        if (kDebugMode)
          BorderedImageButton(
            imageAsset: Images.ic_search_24,
            tooltip: S.of(context).common_search,
            onTap: () => widget.onSearch(),
          ),
        if (!desktopLayout)
          BorderedImageButton(
            imageAsset: Images.ic_notification_24,
            tooltip: S.of(context).common_notifications,
            onTap: () => Nav.home_notifications(),
          ),
      ],
    );
  }

  Widget _buildSmartNote(BuildContext context, SmartNote smartNote) {
    final SmartNoteData data = SmartNoteMapper.map(
      context: context,
      smartNote: smartNote,
      onViewCourses: widget.onViewCourses,
    );

    return Padding(
      padding: EdgeInsets.only(top: Dimens.grid_2x - Toolbar.bottomPadding),
      child: _SmartNote(data: data),
    );
  }

  Widget _buildHeader(BuildContext context, HomeModel model) {
    return Text(
      _getHeaderText(context, model),
      style: _shouldShowExpandedHeader(context) ? Theme.of(context).textTheme.headline6 : Theme.of(context).textTheme.caption,
    );
  }

  String _getHeaderText(BuildContext context, HomeModel model) {
    String result = '';
    if (model.dateTime.isToday) {
      result += S.of(context).common_today + ', ';
    }
    result += DateFormat.yMMMMd().format(model.dateTime);
    return result;
  }

  Widget _buildDaysList(BuildContext context, HomeModel model, BoxConstraints constraints) {
    _scrollToSelectedDate(constraints.maxWidth, model);

    final Paddings paddings = Paddings.of(context);
    final double topPadding = _getDaysListTopPadding(context);

    return ConstrainedBox(
      constraints: BoxConstraints(maxHeight: _dayItemHeight + topPadding),
      child: ListView.builder(
        controller: _daysController,
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.only(
          top: topPadding,
          left: paddings.horizontal,
          right: paddings.horizontal,
        ),
        itemBuilder: (BuildContext context, int position) {
          return _buildDay(context, model, position);
        },
      ),
    );
  }

  double _getDaysListTopPadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    if (adaptive.isDesktop || adaptive.isTablet) return Dimens.grid_3x;
    return Dimens.grid_2x;
  }

  Widget _buildDay(BuildContext context, HomeModel model, int position) {
    final DateTime dateTime = DateTime.now().plusDays(position - _initialDayPosition);
    return _DayView(
      key: ValueKey(dateTime),
      dateTime: dateTime,
      onTap: () => viewModel.onSelectDate(dateTime),
      isSelected: dateTime.isSameDateAs(model.dateTime),
    );
  }

  Widget _buildProgressSection(BuildContext context, HomeModel model) {
    final Paddings paddings = Paddings.of(context);
    final int totalEvents = model.eventsRightNow.length + model.eventsLaterThisDay.length + model.eventsDone.length;
    final double progress = model.eventsDone.length / totalEvents;

    return Padding(
      padding: EdgeInsets.only(
        top: _getListHeaderTopPadding(context),
        left: paddings.horizontal,
        right: paddings.horizontal,
      ),
      child: Column(children: [
        Row(children: [
          Expanded(
            child: Text(
              sprintf(S.of(context).home_eventsDone, [model.eventsDone.length, totalEvents]),
              style: Theme.of(context).textTheme.caption.copyWith(
                    color: Theme.of(context).colorScheme.primaryVariant,
                  ),
            ),
          ),
          Text(
            NumberFormat('#.#').format(progress * 100.0) + '%',
            style: Theme.of(context).textTheme.caption.copyWith(
                  color: Theme.of(context).colorScheme.primaryVariant,
                ),
          ),
        ]),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_1_5x),
          child: HorizontalProgressBar(progress: progress),
        ),
      ]),
    );
  }

  Widget _buildDoneSection(BuildContext context) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        top: Dimens.grid_6x,
        bottom: Dimens.grid_2x,
        left: paddings.horizontal + Dimens.grid_3x,
        right: paddings.horizontal + Dimens.grid_3x,
      ),
      child: Column(children: [
        Image.asset(Images.illustration_beach_128, width: 128, height: 128),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_3x),
          child: Text(S.of(context).home_doneState_message, style: Theme.of(context).textTheme.bodyText1),
        ),
      ]),
    );
  }

  Widget _buildEventsHeader(BuildContext context, String text) {
    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        top: _getListHeaderTopPadding(context),
        left: paddings.horizontal + Dimens.grid_0_5x,
        right: paddings.horizontal + Dimens.grid_0_5x,
      ),
      child: Text(
        text.toUpperCase(),
        style: Theme.of(context).textTheme.overline.copyWith(
              color: Theme.of(context).colorScheme.secondary,
            ),
      ),
    );
  }

  double _getListHeaderTopPadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);

    if (adaptive.isDesktop || adaptive.isTablet && MediaQuery.of(context).orientation == Orientation.portrait) {
      return Dimens.grid_4x;
    }

    return Dimens.grid_3x;
  }

  Widget _buildEvents({
    @required BuildContext context,
    @required HomeModel model,
    @required List<AgendaEvent> events,
    bool inactive = false,
  }) {
    return Padding(
      padding: EdgeInsets.only(top: _getListViewTopPadding(context)),
      child: _EventsColumn(
        events: events,
        courses: model.courses,
        teachers: model.teachers,
        inactive: inactive,
        onEventTap: (AgendaEvent event) => _handleEventTap(context, model, event),
      ),
    );
  }

  double _getListViewTopPadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);

    if (adaptive.isDesktop || adaptive.isTablet && MediaQuery.of(context).orientation == Orientation.portrait) {
      return Dimens.grid_1_5x;
    }

    return Dimens.grid_1x;
  }

  void _handleEventTap(BuildContext context, HomeModel model, AgendaEvent event) {
    if (event is AgendaTimetableEvent) {
      showTimetableEventDetailsDialog(
        context: context,
        data: TimetableEventWidgetData.from(
          context: context,
          timetableType: event.timetableType,
          event: event.raw,
          courses: model.courses,
          teachers: model.teachers,
        ),
      );
    } else if (event is AgendaNoteEvent) {
      Nav.notes_details(noteId: event.note.id);
    } else if (event is AgendaExamEvent) {
      Nav.exams_details(examId: event.exam.id);
    } else if (event is AgendaHomeworkEvent) {
      Nav.homework_details(homeworkId: event.homework.id);
    } else if (event is AgendaReminderEvent) {
      Nav.reminders_details(reminderId: event.reminder.id);
    }
  }

  void _scrollToSelectedDate(double availableWidth, HomeModel model) {
    if (_daysController.hasClients && _daysController.position.hasPixels) {
      final DateTime start = DateTime.now().minusDays(_initialDayPosition);
      final int daysDiff = DateTimeRanges.durationInDaysOf(start: start, end: model.dateTime);
      final double minOffset = _daysController.position.pixels;
      final double targetOffset = _calculateDayScrollOffset(daysDiff);
      final double maxOffset = minOffset + availableWidth;

      if (targetOffset < minOffset || targetOffset > maxOffset) {
        _animateDaysListTo(targetOffset);
      }
    }
  }

  void _animateDaysListBy(double offset) {
    if (_daysController.hasClients && _daysController.position.hasPixels) {
      _animateDaysListTo(_daysController.position.pixels + offset);
    }
  }

  void _animateDaysListTo(double offset) {
    if (_daysController.hasClients) {
      _daysController.animateTo(
        offset,
        duration: Animations.mediumDuration,
        curve: Curves.fastOutSlowIn,
      );
    }
  }

  void _showGoToDateDialog(BuildContext context, HomeModel model) async {
    final callback = await showOptionsDialog(
      context: context,
      routeSettings: RouteSettings(name: R.home_goToDate),
      title: S.of(context).common_goToDate,
      itemBuilder: (BuildContext context) => [
        OptionDialogItem(
          imageAsset24: Images.ic_next_event_24,
          text: S.of(context).common_today,
          value: () => viewModel.onSelectDate(DateTime.now()),
        ),
        OptionDialogItem(
          imageAsset24: Images.ic_custom_date_24,
          text: S.of(context).common_pickDate,
          value: () => _showGoToDateCalendarPicker(context, model),
        ),
      ],
    );

    callback?.call();
  }

  void _showGoToDateCalendarPicker(BuildContext context, HomeModel model) async {
    final DateTime dateTime = await showAdaptiveDatePicker(
      context: context,
      initialDate: DateTime.now(),
      minimumDate: DateTime.now().minusDays(_initialDayPosition),
    );

    if (dateTime != null) {
      viewModel.onSelectDate(dateTime);
    }
  }

  static bool _shouldShowDesktopLayout(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    return adaptive.isDesktop || (adaptive.isTablet && MediaQuery.of(context).orientation == Orientation.landscape);
  }

  static bool _shouldShowExpandedHeader(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    return adaptive.isDesktop || adaptive.isTablet;
  }

  static double _calculateDayScrollOffset(int position) {
    return position * (_dayItemWidth + _dayItemPadding);
  }
}

class _SmartNote extends StatelessWidget {
  final SmartNoteData data;

  const _SmartNote({
    Key key,
    @required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Paddings.of(context).horizontal),
      child: BorderedContainer(
        color: Theme.of(context).colorScheme.surface,
        padding: EdgeInsets.all(Dimens.grid_2x),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(data.title, style: Theme.of(context).textTheme.headline6),
            Padding(
              padding: EdgeInsets.only(top: Dimens.grid_1x, bottom: Dimens.grid_2x),
              child: Text(data.body, style: Theme.of(context).textTheme.bodyText2),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                if (data.negativeButtonText != null)
                  SmallMaterialButton(
                    child: SmallButtonText(
                      data.negativeButtonText,
                      color: Theme.of(context).colorScheme.primaryVariant,
                    ),
                    onPressed: () => data.onNegativeAction(context),
                  ),
                SmallRaisedButton(
                  color: Theme.of(context).colorScheme.secondary,
                  child: SmallButtonText(
                    data.positiveButtonText,
                    color: Theme.of(context).colorScheme.onSecondary,
                  ),
                  onPressed: () => data.onPositiveAction(context),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _DayView extends StatelessWidget {
  final DateTime dateTime;
  final VoidCallback onTap;
  final bool isSelected;

  const _DayView({
    Key key,
    @required this.dateTime,
    @required this.onTap,
    @required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: Dimens.grid_2x),
      child: BorderedContainer(
        width: 56,
        color: isSelected ? Theme.of(context).colorScheme.secondaryVariant : null,
        onTap: onTap,
        child: Column(children: [
          Spacer(),
          Text(
            DateFormat.d().format(dateTime),
            style: Theme.of(context).textTheme.button.copyWith(
                  color: isSelected ? Theme.of(context).colorScheme.onSecondary : Theme.of(context).colorScheme.primary,
                ),
          ),
          Padding(
            padding: EdgeInsets.only(top: Dimens.grid_0_5x),
            child: Text(
              DateFormat.E().format(dateTime),
              style: Theme.of(context)
                  .textTheme
                  .caption
                  .copyWith(color: isSelected ? Theme.of(context).colorScheme.onSecondary : Theme.of(context).colorScheme.primaryVariant),
            ),
          ),
          Spacer(),
        ]),
      ),
    );
  }
}

class _EventsColumn extends StatelessWidget {
  final List<AgendaEvent> events;
  final List<Course> courses;
  final List<Teacher> teachers;
  final bool inactive;
  final ValueChanged<AgendaEvent> onEventTap;

  const _EventsColumn({
    Key key,
    @required this.events,
    @required this.courses,
    @required this.teachers,
    @required this.inactive,
    @required this.onEventTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Paddings.of(context).horizontal),
      child: Column(children: [
        for (AgendaEvent event in events) _buildAgendaEventWidget(context, event),
      ]),
    );
  }

  Widget _buildAgendaEventWidget(BuildContext context, AgendaEvent event) {
    final BorderedListItemData data = AgendaEventDataMapper.map(event, courses, inactive);

    return Padding(
      key: ValueKey(event),
      padding: EdgeInsets.only(bottom: _getEventBottomPadding(context)),
      child: BorderedListItem(
        data: data,
        onTap: () => onEventTap(event),
      ),
    );
  }

  double _getEventBottomPadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);

    if (adaptive.isDesktop || adaptive.isTablet && MediaQuery.of(context).orientation == Orientation.portrait) {
      return Dimens.grid_1_5x;
    }

    return Dimens.grid_1x;
  }
}
