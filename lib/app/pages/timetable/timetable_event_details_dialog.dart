import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/formatter/event_repeat_options_formatter.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/event_repeat_options_delete_mode_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/pages/timetable/timetable_event_widget.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/sized_dialog.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';
import 'package:school_pen/generated/l10n.dart';

/// Presents the details of the [TimetableEventWidgetData].
Future<void> showTimetableEventDetailsDialog({
  @required BuildContext context,
  @required TimetableEventWidgetData data,
}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) => _TimetableEventDetailsDialog(data: data),
  );
}

class _TimetableEventDetailsDialog extends StatelessWidget {
  static const Logger _logger = Logger('_TimetableEventDetailsDialog');

  final TimetableEventWidgetData data;

  const _TimetableEventDetailsDialog({Key key, @required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedDialog(
      padding: EdgeInsets.all(Dimens.grid_2x),
      child: Column(children: [
        _buildTitle(context),
        _buildDivider(context),
        _buildDate(context),
        if (_isSingleDay) _buildTime(context),
        if (data.event.repeatOptions.type != EventRepeatOptionsType.single) _buildRepeatOptions(context),
        if (Strings.isNotBlank(data.room)) _buildRoom(context),
        if (Strings.isNotBlank(data.teacher)) _buildTeachers(context),
        if (Strings.isNotBlank(data.event.note)) _buildNote(context),
        _buildActions(context),
      ]),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Row(children: [
      Text(data.course, style: Theme.of(context).textTheme.headline6),
      Spacer(),
      if (data.type == TimetableType.custom)
        Padding(
          padding: EdgeInsets.only(left: Dimens.grid_2x),
          child: BorderedImageButton(
            imageAsset: Images.ic_edit_pen_24,
            tooltip: S.of(context).common_edit,
            onTap: () {
              Navigator.of(context).pop();
              Nav.timetable_eventEdit(eventId: data.event.id, reoccursAt: data.event.reoccursAt);
            },
          ),
        ),
      if (data.type == TimetableType.custom)
        Padding(
          padding: EdgeInsets.only(left: Dimens.grid_2x),
          child: BorderedImageButton(
            imageAsset: Images.ic_duplicate_24,
            tooltip: S.of(context).common_copy,
            onTap: () {
              Navigator.of(context).pop();
              Nav.timetable_eventDuplicate(copyFromEventId: data.event.id, reoccursAt: data.event.reoccursAt);
            },
          ),
        ),
      if (data.type == TimetableType.custom)
        Padding(
          padding: EdgeInsets.only(left: Dimens.grid_2x),
          child: BorderedImageButton(
            imageAsset: Images.ic_trash_24,
            tooltip: S.of(context).common_delete,
            onTap: () {
              _showDeleteOptionsDialog(context);
            },
          ),
        ),
    ]);
  }

  Widget _buildDivider(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
      child: Divider(),
    );
  }

  Widget _buildDate(BuildContext context) {
    String text;
    if (_isSingleDay) {
      text = DateFormat.EEEE().addPattern(',', '').add_MMMMd().format(data.dateTime);
    } else {
      text = DateFormat.MMMMd().addPattern(',', '').add_Hm().format(data.event.reoccursAt.start) +
          ' - ' +
          DateFormat.MMMMd().addPattern(',', '').add_Hm().format(data.event.reoccursAt.end);
    }

    return _buildRow(
      context: context,
      imageAsset24: Images.ic_timetable_24,
      text: text,
    );
  }

  Widget _buildTime(BuildContext context) {
    return _buildRow(
      context: context,
      imageAsset24: Images.ic_clock_24,
      text: _formatTime(context, data.timeRange.start) + ' - ' + _formatTime(context, data.timeRange.end),
    );
  }

  String _formatTime(BuildContext context, Time time) {
    return time.toTimeOfDay().format(context);
  }

  Widget _buildRepeatOptions(BuildContext context) {
    return _buildRow(
      context: context,
      imageAsset24: Images.ic_loop_24,
      text: EventRepeatOptionsFormatter.format(context, data.event.repeatOptions),
    );
  }

  Widget _buildRoom(BuildContext context) {
    return _buildRow(
      context: context,
      imageAsset24: Images.ic_location_24,
      text: data.room,
    );
  }

  Widget _buildTeachers(BuildContext context) {
    return _buildRow(
      context: context,
      imageAsset24: Images.ic_person_24,
      text: data.teacher,
    );
  }

  Widget _buildNote(BuildContext context) {
    return _buildRow(
      context: context,
      imageAsset24: Images.ic_description_24,
      text: data.event.note,
    );
  }

  Widget _buildRow({@required BuildContext context, @required String imageAsset24, @required String text}) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: Dimens.tens_1x,
        horizontal: Dimens.grid_1x,
      ),
      child: Row(children: [
        Image.asset(
          imageAsset24,
          width: 24,
          height: 24,
          color: Theme.of(context).colorScheme.primaryVariant,
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: Dimens.grid_2x),
            child: Text(
              text,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
        ),
      ]),
    );
  }

  Widget _buildActions(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: Dimens.grid_2x),
      child: Align(
        alignment: Alignment.centerRight,
        child: SmallRaisedButton(
          child: SmallButtonText(S.of(context).common_close),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
    );
  }

  void _showDeleteOptionsDialog(BuildContext context) async {
    // store before popping to avoid accessing context which is disposed
    final String confirmationText = S.of(context).confirmation_deleted;
    Navigator.of(context).pop();

    final EventRepeatOptionsEditMode editMode = await showEventRepeatOptionsDeleteModeDialog(
      context: context,
      routeSettings: RouteSettings(name: R.timetable_delete),
      editModes: data.event.repeatOptions.getAllowedDeleteModes(data.event.reoccursAt),
    );

    if (editMode != null) {
      try {
        final TimetableManager manager = Injection.get();
        await manager.deleteTimetableEvent(data.event.id, data.event.reoccursAt, editMode);
        showSuccessToast(message: confirmationText);
      } catch (error, stackTrace) {
        _logger.logError(error, stackTrace);
      }
    }
  }

  bool get _isSingleDay => data.event.reoccursAt.start.isSameDateAs(data.event.reoccursAt.end);
}
