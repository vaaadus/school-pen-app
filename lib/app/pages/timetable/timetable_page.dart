import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/anim/animations.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/pages/picker/options_dialog.dart';
import 'package:school_pen/app/pages/picker/radio_button_dialog.dart';
import 'package:school_pen/app/pages/picker/text_field_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/pages/timetable/timetable_header.dart';
import 'package:school_pen/app/pages/timetable/timetable_view_model.dart';
import 'package:school_pen/app/pages/timetable/timetable_widget.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/bordered_container.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/page_placeholder.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows to view/edit/create timetables.
class TimetablePage extends LifecycleWidget {
  const TimetablePage({Key key}) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _TimetablePageState();
}

class _TimetablePageState extends LifecycleWidgetState<TimetablePage>
    with ViewModelLifecycle<TimetableViewModel, TimetablePage>, ProgressOverlayMixin, SingleTickerProviderStateMixin {
  // Menus
  static const String _createTimetablePopupId = 'create-timetable-popup-id';
  static const String _subscribeTimetablePopupId = 'subscribe-timetable-popup-id';

  // PageView
  static const int _initialPage = 1000000; // allow to go back that amount of spans
  static const Duration _changePageDuration = Animations.mediumDuration;
  static const Curve _changePageCurve = Curves.fastOutSlowIn;

  final PageController _controller = PageController(initialPage: _initialPage);
  final FocusNode _focus = FocusNode();
  TimetableViewMode _previousViewMode;
  DateTime _selectedDateTime;

  @override
  TimetableViewModel provideViewModel() {
    return TimetableViewModel(Injection.get(), Injection.get(), Injection.get(), Injection.get(), Injection.get());
  }

  @override
  void dispose() {
    _controller.dispose();
    _focus.dispose();
    _previousViewMode = null;
    _selectedDateTime = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder(
      viewModel: viewModel,
      builder: (BuildContext context, TimetableModel model) {
        updateProgressOverlay(model.progressOverlay);
        return _buildScaffold(context, model);
      },
      onSignal: (BuildContext context, TimetableSignal signal) {
        if (signal == TimetableSignal.notifyUnsubscribedFromTimetable) {
          showSuccessToast(message: S.of(context).confirmation_unsubscribed);
        } else if (signal == TimetableSignal.notifyTimetableCreated) {
          showSuccessToast(message: S.of(context).confirmation_saveSuccess);
        } else if (signal == TimetableSignal.notifyTimetableDuplicated) {
          showSuccessToast(message: S.of(context).confirmation_saveSuccess);
        } else if (signal == TimetableSignal.notifyTimetableRenamed) {
          showSuccessToast(message: S.of(context).confirmation_saveSuccess);
        } else if (signal == TimetableSignal.notifyTimetableDeleted) {
          showSuccessToast(message: S.of(context).confirmation_deleted);
        }
      },
    );
  }

  Widget _buildScaffold(BuildContext context, TimetableModel model) {
    Widget result;
    Widget floatingButton;

    if (model.showEmptyState) {
      result = _buildEmptyState(context, model);
    } else if (model.showNoInternetState) {
      result = _buildNoInternetState(context);
    } else if (model.showNotFoundState) {
      result = _buildNotFoundState(context);
    } else {
      result = _buildContent(context, model);

      if (model.timetable.type == TimetableType.custom) {
        floatingButton = FloatingActionButton(
          heroTag: null,
          child: Icon(Icons.add, color: Theme.of(context).colorScheme.onSecondary),
          tooltip: S.of(context).common_addLesson,
          onPressed: () => _showCreateTimetableEventDialog(context, model),
        );
      }
    }

    result = Scaffold(
      body: SafeArea(child: result),
      floatingActionButton: floatingButton,
    );

    return result;
  }

  Widget _buildEmptyState(BuildContext context, TimetableModel model) {
    return PagePlaceholder(
      imageAsset: Images.illustration_calendar_192,
      imageWidth: 192,
      imageHeight: 192,
      message: S.of(context).timetables_emptyState_message +
          ' ' +
          (model.subscribeAvailable ? S.of(context).timetables_emptyState_subscribeMessage : ''),
      buttons: [
        RaisedButton(
          child: ButtonText(S.of(context).common_create),
          onPressed: () => _showCreateTimetableDialog(context, model),
        ),
        if (model.subscribeAvailable)
          RaisedButton(
            color: Theme.of(context).colorScheme.secondaryVariant,
            child: ButtonText(S.of(context).common_subscribe),
            onPressed: () => Nav.timetable_subscribe_selectSchool(),
          ),
      ],
    );
  }

  Widget _buildNoInternetState(BuildContext context) {
    return PagePlaceholder(
      imageAsset: Images.illustration_router_192,
      imageWidth: 192,
      imageHeight: 192,
      message: S.of(context).common_noInternet_message,
      buttons: [
        RaisedButton(
          child: ButtonText(S.of(context).common_refresh),
          onPressed: () => viewModel.onRefresh(),
        ),
      ],
    );
  }

  Widget _buildNotFoundState(BuildContext context) {
    return PagePlaceholder(
      imageAsset: Images.illustration_question_192,
      imageWidth: 192,
      imageHeight: 192,
      message: S.of(context).timetables_notFoundState_message,
      buttons: [
        RaisedButton(
          color: Theme.of(context).colorScheme.error,
          child: ButtonText(
            S.of(context).common_unsubscribe,
            color: Theme.of(context).colorScheme.onError,
          ),
          onPressed: () => viewModel.onUnsubscribeFromActiveTimetable(),
        ),
      ],
    );
  }

  Widget _buildContent(BuildContext context, TimetableModel model) {
    return RawKeyboardListener(
      focusNode: _focus,
      onKey: _onKeyboardKey,
      child: CustomScrollView(slivers: [
        _buildToolbar(context, model),
        SliverFillRemaining(
          child: PageView.builder(
            controller: _controller,
            itemBuilder: (BuildContext context, int index) {
              _repositionPageViewAfterChangeInViewMode(context, model, index);
              if (_selectedDateTime != null) {
                _jumpToDateTime(_selectedDateTime, model.viewMode);
                _selectedDateTime = null;
              }
              return _buildPage(context, model, index);
            },
          ),
        ),
      ]),
    );
  }

  Widget _buildToolbar(BuildContext context, TimetableModel model) {
    final Adaptive adaptive = Adaptive.of(context);
    final Orientation orientation = MediaQuery.of(context).orientation;
    final bool mobileLayout = adaptive.isMobile || (adaptive.isTablet && orientation == Orientation.portrait);

    return Toolbar(
      vsync: this,
      navType: null,
      pinned: false,
      prefix: Row(children: [
        _buildTimetableSwitcher(context, model),
        if (!mobileLayout)
          Padding(
            padding: EdgeInsets.only(left: Dimens.grid_2x),
            child: _buildViewModeSwitcher(context, model),
          ),
      ]),
      actions: [
        if (!mobileLayout)
          BorderedContainer(
            padding: EdgeInsets.symmetric(
              vertical: Dimens.grid_1x,
              horizontal: Dimens.grid_1_5x,
            ),
            color: Theme.of(context).colorScheme.surface,
            child: SmallButtonText(
              S.of(context).common_today,
              color: Theme.of(context).colorScheme.primary,
            ),
            onTap: () => _animateToDateTime(DateTimes.utcNow(), model.viewMode),
          ),
        if (!mobileLayout)
          BorderedImageButton(
            imageAsset: Images.ic_arrow_left_24,
            tooltip: S.of(context).common_previousPage,
            onTap: _showPreviousPage,
          ),
        if (!mobileLayout)
          BorderedImageButton(
            imageAsset: Images.ic_arrow_right_24,
            tooltip: S.of(context).common_nextPage,
            onTap: _showNextPage,
          ),
        BorderedImageButton(
          imageAsset: Images.ic_calendar_today_24,
          tooltip: S.of(context).common_goToDate,
          onTap: () => mobileLayout ? _showGoToDateDialog(context, model) : _showGoToDateCalendarPicker(context, model),
        ),
        if (mobileLayout)
          BorderedImageButton(
            imageAsset: _viewModeToImageAsset(model.viewMode),
            tooltip: S.of(context).common_viewMode,
            onTap: () => _showViewModeDialog(context, model),
          ),
        BorderedImageButton(
          imageAsset: Images.ic_config_24,
          tooltip: S.of(context).common_options,
          onTap: () => _showSettings(context, model),
        ),
      ],
    );
  }

  String _viewModeToImageAsset(TimetableViewMode mode) {
    switch (mode) {
      case TimetableViewMode.day:
        return Images.ic_view_day_24;
      case TimetableViewMode.threeDays:
        return Images.ic_view_days_24;
      case TimetableViewMode.week:
        return Images.ic_view_week_24;
    }
    throw ArgumentError('Unsupported view mode: $mode');
  }

  Widget _buildTimetableSwitcher(BuildContext context, TimetableModel model) {
    return SmallBorderedPopupButton(
      child: SmallButtonText(model.timetable.name, color: Theme.of(context).colorScheme.primary),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
        for (TimetableInfo timetable in model.availableTimetables)
          PopupMenuItem(value: timetable.id, child: Text(timetable.name)),
        PopupMenuDivider(),
        _popupMenuItem(context, S.of(context).common_createTimetable, _createTimetablePopupId),
        if (model.subscribeAvailable)
          _popupMenuItem(context, S.of(context).common_subscribeOtherTimetable, _subscribeTimetablePopupId),
      ],
      onSelected: (String timetableId) {
        if (timetableId == _createTimetablePopupId) {
          _showCreateTimetableDialog(context, model);
        } else if (timetableId == _subscribeTimetablePopupId) {
          Nav.timetable_subscribe_selectSchool();
        } else {
          viewModel.onSelectActiveTimetable(timetableId);
        }
      },
    );
  }

  PopupMenuItem<String> _popupMenuItem(BuildContext context, String text, String id) {
    return PopupMenuItem(
      value: id,
      child: Text(
        text,
        style: Theme.of(context).popupMenuTheme.textStyle.copyWith(
              color: Theme.of(context).colorScheme.secondary,
            ),
      ),
    );
  }

  Widget _buildViewModeSwitcher(BuildContext context, TimetableModel model) {
    return SmallBorderedPopupButton(
      child: SmallButtonText(
        ResourceLocator.timetableViewModeToText(context, model.viewMode),
        color: Theme.of(context).colorScheme.primary,
      ),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<TimetableViewMode>>[
        for (TimetableViewMode viewMode in TimetableViewMode.values)
          PopupMenuItem(
            value: viewMode,
            child: Text(ResourceLocator.timetableViewModeToText(context, viewMode)),
          ),
      ],
      onSelected: (TimetableViewMode viewMode) => viewModel.onChangeViewMode(viewMode),
    );
  }

  Widget _buildPage(BuildContext context, TimetableModel model, int pageIndex) {
    final DateTimeRange dateRange = _calculateDateRange(pageIndex, model.viewMode);

    return Column(children: [
      Padding(
        padding: EdgeInsets.only(
          left: TimetableWidget.leftTimelinePadding(context) +
              TimetableWidget.timelineWidth(context) +
              TimetableWidget.scheduleHorizontalPadding(context),
          right: TimetableWidget.scheduleHorizontalPadding(context),
        ),
        child: TimetableHeader(
          dateRange: dateRange,
          reduceTopPaddingBy: Toolbar.bottomPadding,
          onDateSelected: (DateTime dateTime) {
            _selectedDateTime = dateTime;
            viewModel.onChangeViewMode(TimetableViewMode.day);
          },
        ),
      ),
      Divider(),
      Expanded(
        child: TimetableWidget(
          dateRange: dateRange,
          timetable: model.timetable,
          courses: model.courses,
          teachers: model.teachers,
          onAddEvent: (AddTimetableEvent event) => viewModel.onCreateTimetableEvent(event),
        ),
      )
    ]);
  }

  void _onKeyboardKey(RawKeyEvent event) {
    if (event is RawKeyDownEvent) {
      final LogicalKeyboardKey key = event.logicalKey;
      if (key == LogicalKeyboardKey.arrowLeft || key == LogicalKeyboardKey.arrowDown) {
        _showPreviousPage();
      } else if (key == LogicalKeyboardKey.arrowRight || key == LogicalKeyboardKey.arrowUp) {
        _showNextPage();
      }
    }
  }

  void _showPreviousPage() {
    _controller.previousPage(duration: _changePageDuration, curve: _changePageCurve);
  }

  void _showNextPage() {
    _controller.nextPage(duration: _changePageDuration, curve: _changePageCurve);
  }

  void _animateToDateTime(DateTime dateTime, TimetableViewMode viewMode) {
    _controller.animateToPage(
      _calculatePageIndex(dateTime, viewMode),
      duration: _changePageDuration,
      curve: _changePageCurve,
    );
  }

  void _jumpToDateTime(DateTime dateTime, TimetableViewMode viewMode) {
    _controller.jumpToPage(_calculatePageIndex(dateTime, viewMode));
  }

  /// Reposition the page view so that the start date selected by the user still is the same.
  void _repositionPageViewAfterChangeInViewMode(BuildContext context, TimetableModel model, int pageIndex) {
    final TimetableViewMode previousViewMode = _previousViewMode;
    _previousViewMode = model.viewMode;

    if (previousViewMode == null || previousViewMode == model.viewMode) {
      return;
    }

    final DateTime previousStartDate = _calculateDateRange(pageIndex, previousViewMode).start;
    final int increment = _calculateDateRange(pageIndex, model.viewMode).start.isBefore(previousStartDate) ? 1 : -1;

    while (true) {
      final DateTimeRange range = _calculateDateRange(pageIndex, model.viewMode);
      if (range.contains(previousStartDate)) {
        break;
      } else {
        pageIndex += increment;
      }
    }

    _controller.jumpToPage(max(0, pageIndex));
  }

  int _calculatePageIndex(DateTime dateTime, TimetableViewMode viewMode) {
    final int increment = dateTime.isBefore(DateTimes.utcNow()) ? -1 : 1;
    int index = _initialPage;
    while (true) {
      final DateTimeRange range = _calculateDateRange(index, viewMode);
      if (range.contains(dateTime)) return index;

      index += increment;
    }
  }

  DateTimeRange _calculateDateRange(int pageIndex, TimetableViewMode viewMode) {
    final int daysToAdd = (pageIndex - _initialPage) * viewMode.daysSpan;

    DateTime start = DateTimes.utcNow().plusDays(daysToAdd).withoutTime();
    if (viewMode == TimetableViewMode.week) {
      start = start.adjustToStartOfWeek();
    }

    return DateTimeRange(
      start: start,
      end: start.plusDays(viewMode.daysSpan - 1).atEndOfDay(),
    );
  }

  void _showGoToDateDialog(BuildContext context, TimetableModel model) async {
    final callback = await showOptionsDialog(
      context: context,
      routeSettings: RouteSettings(name: R.timetable_goToDate),
      title: S.of(context).common_goToDate,
      itemBuilder: (BuildContext context) => [
        OptionDialogItem(
          imageAsset24: Images.ic_next_event_24,
          text: S.of(context).common_today,
          value: () => _animateToDateTime(DateTimes.utcNow(), model.viewMode),
        ),
        OptionDialogItem(
          imageAsset24: Images.ic_custom_date_24,
          text: S.of(context).common_pickDate,
          value: () => _showGoToDateCalendarPicker(context, model),
        ),
      ],
    );

    callback?.call();
  }

  void _showGoToDateCalendarPicker(BuildContext context, TimetableModel model) async {
    final DateTime dateTime = await showAdaptiveDatePicker(
      context: context,
      initialDate: DateTimes.utcNow(),
      minimumDate: DateTimes.utcNow().minusDays(_initialPage),
    );

    if (dateTime != null) {
      _animateToDateTime(dateTime.asUtc(), model.viewMode);
    }
  }

  void _showViewModeDialog(BuildContext context, TimetableModel model) async {
    final TimetableViewMode viewMode = await showRadioButtonDialog(
      context: context,
      routeSettings: RouteSettings(name: R.timetable_viewMode),
      title: S.of(context).common_viewMode,
      itemBuilder: (BuildContext context) => [
        for (TimetableViewMode viewMode in TimetableViewMode.values)
          RadioButtonDialogItem(
            text: ResourceLocator.timetableViewModeToShortText(context, viewMode),
            value: viewMode,
            groupValue: model.viewMode,
          ),
      ],
    );

    if (viewMode != null) {
      viewModel.onChangeViewMode(viewMode);
    }
  }

  void _showSettings(BuildContext context, TimetableModel model) {
    if (model.timetable.type == TimetableType.custom) {
      _showCustomSettings(context, model);
    } else {
      _showPublicSettings(context, model);
    }
  }

  void _showCustomSettings(BuildContext context, TimetableModel model) async {
    final Function(BuildContext context, TimetableModel model) option = await showOptionsDialog(
      context: context,
      routeSettings: RouteSettings(name: R.timetable_settings),
      title: S.of(context).common_options,
      itemBuilder: (BuildContext context) => [
        OptionDialogItem(
          imageAsset24: Images.ic_create_document_24,
          text: S.of(context).common_createTimetable,
          value: _showCreateTimetableDialog,
        ),
        OptionDialogItem(
          imageAsset24: Images.ic_duplicate_24,
          text: S.of(context).common_duplicateTimetable,
          value: _showDuplicateTimetableDialog,
        ),
        OptionDialogItem(
          imageAsset24: Images.ic_edit_field_24,
          text: S.of(context).common_renameTimetable,
          value: _showRenameTimetableDialog,
        ),
        OptionDialogItem(
          imageAsset24: Images.ic_trash_24,
          text: S.of(context).common_deleteTimetable,
          customColor: Theme.of(context).colorScheme.error,
          value: _showDeleteTimetableDialog,
        ),
      ],
    );

    option?.call(context, model);
  }

  void _showPublicSettings(BuildContext context, TimetableModel model) async {
    final Function(BuildContext context, TimetableModel model) option = await showOptionsDialog(
      context: context,
      routeSettings: RouteSettings(name: R.timetable_settings),
      title: S.of(context).common_options,
      itemBuilder: (BuildContext context) => [
        OptionDialogItem(
          imageAsset24: Images.ic_create_document_24,
          text: S.of(context).common_subscribeOtherTimetable,
          value: _showSubscribePage,
        ),
        OptionDialogItem(
          imageAsset24: Images.ic_trash_24,
          text: S.of(context).common_unsubscribeFromTimetable,
          customColor: Theme.of(context).colorScheme.error,
          value: _unsubscribeFromTimetable,
        ),
      ],
    );

    option?.call(context, model);
  }

  void _showCreateTimetableDialog(BuildContext context, TimetableModel model) async {
    final String text = await showTextFieldDialog(
      context: context,
      routeSettings: RouteSettings(name: R.timetable_create),
      title: S.of(context).common_createTimetable,
      placeholder: S.of(context).common_name,
      submitText: S.of(context).common_create,
      validator: (String text) {
        if (Strings.isBlank(text)) {
          return FieldMustNotBeEmptyException();
        }
        return null;
      },
    );

    if (text != null) {
      viewModel.onCreateTimetable(AddTimetable(
        schoolYearId: model.schoolYear.id,
        name: text,
        type: TimetableType.custom,
        updatedAt: DateTime.now(),
      ));
    }
  }

  void _showDuplicateTimetableDialog(BuildContext context, TimetableModel model) async {
    final String text = await showTextFieldDialog(
      context: context,
      routeSettings: RouteSettings(name: R.timetable_duplicate),
      title: S.of(context).common_duplicateTimetable,
      placeholder: S.of(context).common_newName,
      submitText: S.of(context).common_duplicate,
      validator: (String text) {
        if (Strings.isBlank(text)) {
          return FieldMustNotBeEmptyException();
        }
        return null;
      },
    );

    if (text != null) {
      viewModel.onDuplicateTimetable(model.timetable, text);
    }
  }

  void _showRenameTimetableDialog(BuildContext context, TimetableModel model) async {
    final String text = await showTextFieldDialog(
      context: context,
      routeSettings: RouteSettings(name: R.timetable_rename),
      initialText: model.timetable.name,
      title: S.of(context).common_renameTimetable,
      placeholder: S.of(context).common_newName,
      submitText: S.of(context).common_save,
      validator: (String text) {
        if (Strings.isBlank(text)) {
          return FieldMustNotBeEmptyException();
        }
        return null;
      },
    );

    if (text != null) {
      viewModel.onRenameTimetable(model.timetable, text);
    }
  }

  void _showDeleteTimetableDialog(BuildContext context, TimetableModel model) {
    showAlertDialog(
      context: context,
      routeSettings: RouteSettings(name: R.timetable_delete),
      title: S.of(context).common_delete,
      message: S.of(context).timetables_deleteDialog_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteTimetable(model.timetable),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }

  void _showCreateTimetableEventDialog(BuildContext context, TimetableModel model) {
    Nav.timetable_eventAdd(timetableId: model.timetable.id);
  }

  void _showSubscribePage(BuildContext context, TimetableModel model) {
    Nav.timetable_subscribe_selectSchool();
  }

  void _unsubscribeFromTimetable(BuildContext context, TimetableModel model) {
    viewModel.onUnsubscribe(model.timetable.id);
  }
}
