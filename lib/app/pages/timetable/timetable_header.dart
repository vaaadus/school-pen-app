import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/typography.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';

/// Displays a list of weekdays for the timetable.
class TimetableHeader extends StatelessWidget {
  /// The date time range to display in this widget.
  final DateTimeRange dateRange;

  /// The amount by which the top padding should be reduced.
  final double reduceTopPaddingBy;

  /// The callback which is called when a user taps on some date.
  final ValueChanged<DateTime> onDateSelected;

  const TimetableHeader({
    Key key,
    @required this.dateRange,
    this.reduceTopPaddingBy = 0,
    this.onDateSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final int daysSpan = dateRange.durationInDays();
    if (daysSpan == 1) {
      return _buildSingleDayHeader(context);
    } else {
      return _buildMultiDaysHeader(context, daysSpan);
    }
  }

  Widget _buildSingleDayHeader(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: _getSingleDayHeaderTopPadding(context) - reduceTopPaddingBy,
        bottom: Dimens.grid_1_5x - 2.0,
        left: Dimens.grid_3x,
        right: Dimens.grid_3x,
      ),
      child: Text(
        _formatSingleDayText(),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.headline6.copyWith(
              color: dateRange.start.isToday
                  ? Theme.of(context).colorScheme.secondary
                  : Theme.of(context).colorScheme.primary,
            ),
      ),
    );
  }

  double _getSingleDayHeaderTopPadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    if (adaptive.isMobile || adaptive.isTablet) return Dimens.grid_3x;
    return Dimens.grid_4x;
  }

  String _formatSingleDayText() {
    return DateFormat.EEEE().addPattern(',', '').add_MMMMd().format(dateRange.start);
  }

  Widget _buildMultiDaysHeader(BuildContext context, int daysSpan) {
    return Padding(
      padding: EdgeInsets.only(
        top: _getMultiDayHeaderTopPadding(context) - reduceTopPaddingBy,
        bottom: Dimens.grid_1x,
      ),
      child: Row(children: [
        for (int i = 0; i < daysSpan; i++)
          Expanded(
            child: _buildMultiDayColumn(context, i),
          ),
      ]),
    );
  }

  Widget _buildMultiDayColumn(BuildContext context, int position) {
    final DateTime dateTime = dateRange.start.plusDays(position);
    return InkWell(
      onTap: () => onDateSelected?.call(dateTime),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _buildMultiDayText(context, dateTime),
          _buildMultiDayCaption(context, dateTime, mustIncludeMonth: position == 0),
        ],
      ),
    );
  }

  double _getMultiDayHeaderTopPadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    if (adaptive.isMobile || adaptive.isTablet) return Dimens.grid_2x;
    return Dimens.grid_3x;
  }

  Widget _buildMultiDayText(BuildContext context, DateTime date) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      return _wrapInHighlightedContainer(
        context: context,
        highlight: date.isToday,
        child: Text(
          _formatMultiDayText(date, constraints),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.button.copyWith(
                color: date.isToday ? Theme.of(context).colorScheme.onSecondary : Theme.of(context).colorScheme.primary,
              ),
        ),
      );
    });
  }

  Widget _wrapInHighlightedContainer({@required BuildContext context, @required Widget child, bool highlight = true}) {
    return Center(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
        decoration: BoxDecoration(
          color: highlight ? Theme.of(context).colorScheme.secondaryVariant : null,
          borderRadius: BorderRadius.all(Radius.circular(Dimens.grid_0_5x)),
        ),
        child: child,
      ),
    );
  }

  String _formatMultiDayText(DateTime date, BoxConstraints constraints) {
    if (constraints.maxWidth < 60) {
      final String text = DateFormat.E().format(date).toUpperCase();
      return Strings.substringUpTo(text, 2);
    }

    if (constraints.maxWidth < 120) {
      final String text = DateFormat.E().format(date).toUpperCase();
      return Strings.substringUpTo(text, 3);
    }

    return DateFormat.EEEE().format(date).toUpperCase();
  }

  Widget _buildMultiDayCaption(BuildContext context, DateTime date, {bool mustIncludeMonth = false}) {
    return Padding(
      padding: EdgeInsets.only(top: 2.0),
      child: Text(
        _formatMultiDayCaption(date, mustIncludeMonth: mustIncludeMonth),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.caption2(context).copyWith(
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
      ),
    );
  }

  String _formatMultiDayCaption(DateTime date, {bool mustIncludeMonth = false}) {
    if (date.minusDays(1).month != date.month) {
      mustIncludeMonth = true;
    }

    if (mustIncludeMonth) {
      return DateFormat.MMMd().format(date);
    } else {
      return DateFormat.d().format(date);
    }
  }
}
