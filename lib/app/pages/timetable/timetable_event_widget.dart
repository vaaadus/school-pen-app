import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/typography.dart';
import 'package:school_pen/app/common/formatter/timetable_event_formatter.dart';
import 'package:school_pen/app/pages/timetable/timetable_event_details_dialog.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';

/// Defines possible layout adaptations to different screen sizes.
enum _LayoutSize {
  big,
  medium,
  small,
}

/// Data to show in the [TimetableEventWidget].
class TimetableEventWidgetData extends Equatable {
  final SingleTimetableEvent event;
  final TimetableType type;
  final DateTime dateTime;
  final TimeRange timeRange;
  final Color color;
  final String course;
  final String room;
  final String student;
  final String teacher;

  const TimetableEventWidgetData({
    @required this.event,
    @required this.type,
    @required this.dateTime,
    @required this.timeRange,
    @required this.color,
    @required this.course,
    @required this.room,
    @required this.student,
    @required this.teacher,
  });

  static TimetableEventWidgetData from({
    @required BuildContext context,
    @required TimetableType timetableType,
    @required SingleTimetableEvent event,
    @required List<Course> courses,
    @required List<Teacher> teachers,
  }) {
    final Course course = _findCourse(courses, event);
    final List<Teacher> selectedTeachers = _findTeachers(teachers, course);

    return TimetableEventWidgetData(
      event: event,
      type: timetableType,
      dateTime: event.dateTime,
      timeRange: event.timeRange,
      color: TimetableEventFormatter.getColor(event, course),
      course: TimetableEventFormatter.formatCourseName(event, course),
      room: TimetableEventFormatter.formatRoom(event, course),
      student: TimetableEventFormatter.formatStudent(event),
      teacher: TimetableEventFormatter.formatTeachers(event, selectedTeachers),
    );
  }

  static Course _findCourse(List<Course> courses, SingleTimetableEvent event) {
    if (event.courseId == null) return null;
    return courses.firstWhereOrNull((e) => e.id == event.courseId);
  }

  static List<Teacher> _findTeachers(List<Teacher> teachers, Course course) {
    if (course == null) return [];
    return teachers.where((e) => course.teachersIds.contains(e.id)).toList();
  }

  /// Whether this [TimetableEventWidgetData] has any common parts in time with [other].
  bool overlapsWith(TimetableEventWidgetData other) {
    if (!dateTime.isSameDateAs(other.dateTime)) return false;
    return timeRange.overlapsWith(other.timeRange);
  }

  @override
  List<Object> get props => [event, type, dateTime, timeRange, color, course, room, student, teacher];
}

/// Displays [TimetableEventWidgetData].
class TimetableEventWidget extends StatelessWidget {
  final TimetableEventWidgetData data;

  const TimetableEventWidget({Key key, @required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final TextTheme textTheme = Theme.of(context).textTheme;
      final _LayoutSize size = _calculateLayoutSize(constraints);

      Widget result = Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(top: _getVerticalPadding(size, constraints)),
            child: Text(
              data.course,
              style: _getTitleStyle(context, textTheme, size, constraints).copyWith(color: _getTextColor()),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: size == _LayoutSize.small ? 2.0 : 4.0),
            child: Text(
              size == _LayoutSize.big ? data.teacher : data.room,
              style: textTheme.caption2(context).copyWith(color: _getTextColor(), fontWeight: FontWeight.w500),
            ),
          ),
          Spacer(),
          Padding(
            padding: EdgeInsets.only(
              top: size == _LayoutSize.small ? 2.0 : 4.0,
              bottom: _getVerticalPadding(size, constraints),
            ),
            child: Text(
              TimetableEventFormatter.formatTimeRange(data.timeRange),
              style: textTheme.caption.copyWith(
                fontSize: size == _LayoutSize.small ? 8.0 : textTheme.caption.fontSize,
                color: _getTextColor(),
              ),
            ),
          ),
        ],
      );

      if (size == _LayoutSize.big) {
        result = Row(children: [
          Expanded(child: result),
          Text(data.room, style: textTheme.headline6.copyWith(color: _getTextColor())),
        ]);
      }

      final BorderRadius borderRadius = BorderRadius.all(Radius.circular(
        size == _LayoutSize.small ? Dimens.grid_0_5x : Dimens.grid_1x,
      ));

      result = Container(
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(
          color: AppColors.applyDarkModeSaturation(context, data.color),
          borderRadius: borderRadius,
        ),
        padding: EdgeInsets.symmetric(horizontal: _getHorizontalPadding(size)),
        child: result,
      );

      result = InkWell(
        child: result,
        borderRadius: borderRadius,
        onTap: () => showTimetableEventDetailsDialog(context: context, data: data),
      );

      return result;
    });
  }

  _LayoutSize _calculateLayoutSize(BoxConstraints constraints) {
    final double diagonal = sqrt(pow(constraints.maxWidth, 2.0) + pow(constraints.maxHeight, 2.0));
    if (diagonal >= 300 && constraints.maxWidth > 80) return _LayoutSize.big;
    if (diagonal >= 120) return _LayoutSize.medium;
    return _LayoutSize.small;
  }

  double _getVerticalPadding(_LayoutSize size, BoxConstraints constraints) {
    switch (size) {
      case _LayoutSize.big:
        return constraints.maxHeight > 100 ? 8.0 : 4.0;
      case _LayoutSize.medium:
        return 4.0;
      case _LayoutSize.small:
        return 2.0;
    }

    throw ArgumentError('Unsupported Layout size: $size');
  }

  double _getHorizontalPadding(_LayoutSize size) {
    switch (size) {
      case _LayoutSize.big:
        return 12.0;
      case _LayoutSize.medium:
        return 6.0;
      case _LayoutSize.small:
        return 4.0;
    }

    throw ArgumentError('Unsupported Layout size: $size');
  }

  TextStyle _getTitleStyle(BuildContext context, TextTheme textTheme, _LayoutSize size, BoxConstraints constraints) {
    switch (size) {
      case _LayoutSize.big:
        return constraints.maxHeight > 70
            ? textTheme.headline6
            : textTheme.caption.copyWith(fontWeight: FontWeight.bold);
      case _LayoutSize.medium:
        return textTheme.caption.copyWith(fontWeight: FontWeight.bold);
      case _LayoutSize.small:
        return textTheme.caption2(context).copyWith(fontWeight: FontWeight.bold);
    }

    throw ArgumentError('Unsupported Layout size: $size');
  }

  Color _getTextColor() {
    return Colors.white;
  }
}
