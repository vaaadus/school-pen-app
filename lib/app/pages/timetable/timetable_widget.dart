import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/typography.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/picker/course_picker_dialog.dart';
import 'package:school_pen/app/pages/timetable/timetable_event_widget.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';

/// Draws the timetable schedule.
class TimetableWidget extends StatefulWidget {
  /// The date time range to display in this widget.
  final DateTimeRange dateRange;

  /// The timetable to show in this widget.
  final Timetable timetable;

  /// The list of courses related to the timetable.
  final List<Course> courses;

  /// The list of teachers related to the timetable.
  final List<Teacher> teachers;

  /// Called when users adds a timetable event.
  final ValueChanged<AddTimetableEvent> onAddEvent;

  const TimetableWidget({
    Key key,
    @required this.dateRange,
    @required this.timetable,
    @required this.courses,
    @required this.teachers,
    @required this.onAddEvent,
  }) : super(key: key);

  static double timelineWidth(BuildContext context) {
    return 40.0;
  }

  static double leftTimelinePadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    if (adaptive.isMobile) return 0;
    if (adaptive.isTablet) return Dimens.grid_1x;
    return Dimens.grid_2x;
  }

  static double scheduleHorizontalPadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    if (adaptive.isMobile) return Dimens.grid_0_5x;
    if (adaptive.isTablet) return Dimens.grid_1_5x;
    return Dimens.grid_2x;
  }

  static double verticalPadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    if (adaptive.isMobile) return 10;
    if (adaptive.isTablet) return Dimens.grid_2x;
    return Dimens.grid_3x;
  }

  static double eventSidePadding(BuildContext context, int daysSpan) {
    final Adaptive adaptive = Adaptive.of(context);

    if (adaptive.isMobile) {
      if (daysSpan <= 1) return 4;
      if (daysSpan <= 3) return 2.0;
      return 1.5;
    }

    if (adaptive.isTablet) {
      if (daysSpan <= 1) return 8;
      if (daysSpan <= 3) return 4.0;
      return 2.0;
    }

    // desktop
    if (daysSpan <= 1) return 10;
    if (daysSpan <= 3) return 5;
    return 2.5;
  }

  @override
  _TimetableWidgetState createState() => _TimetableWidgetState();
}

class _TimetableWidgetState extends State<TimetableWidget> {
  static const double minHeightPerHour = 100.0;

  static final TimeRange minimumTimeRange = TimeRange(
    start: Time(hour: 8, minute: 0),
    end: Time(hour: 14, minute: 0),
  );

  final TimetableManager _timetableManager = Injection.get();
  List<SingleTimetableEvent> _events = [];
  TimeRange _timeRange = minimumTimeRange;

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void didUpdateWidget(TimetableWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.dateRange != widget.dateRange || oldWidget.timetable != widget.timetable) {
      _init();
    }
  }

  @override
  void dispose() {
    _events = null;
    _timeRange = null;
    super.dispose();
  }

  void _init() async {
    final DateTimeRange dateRange = widget.dateRange;
    final Timetable timetable = widget.timetable;

    if (timetable == null) {
      return;
    }

    final List<SingleTimetableEvent> events = await _timetableManager.getSingleEventsOf(timetable, dateRange);
    final TimeRange timeRange = Timetable.getTimeRangeOfEventsOf(events: events, minimumRange: minimumTimeRange);
    final TimeRange viewportTimeRange = _calculateViewportTimeRange(dateRange, timeRange);

    if (mounted && dateRange == widget.dateRange && timetable == widget.timetable) {
      setState(() {
        _events = events;
        _timeRange = viewportTimeRange;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final Size viewportSize = Size(
        constraints.maxWidth,
        max(minHeightPerHour * _timeRange.duration.inHours, constraints.maxHeight),
      );

      Widget result = TimetableContent(
        viewportSize: viewportSize,
        dateRange: widget.dateRange,
        timeRange: _timeRange,
        timetable: widget.timetable,
        events: _events,
        courses: widget.courses,
        teachers: widget.teachers,
        onAddEvent: widget.onAddEvent,
      );

      if (viewportSize.height > constraints.maxHeight) {
        result = SingleChildScrollView(
          primary: false,
          child: result,
        );
      }
      return result;
    });
  }

  TimeRange _calculateViewportTimeRange(DateTimeRange dateRange, TimeRange timeRange) {
    return TimeRange(
      start: Time(hour: timeRange.start.hour, minute: 0),
      end: Time(hour: timeRange.end.hour + (timeRange.end.minute == 0 ? 0 : 1), minute: 0),
    );
  }
}

class TimetableContent extends StatefulWidget {
  final Size viewportSize;
  final DateTimeRange dateRange;
  final TimeRange timeRange;
  final Timetable timetable;
  final List<SingleTimetableEvent> events;
  final List<Course> courses;
  final List<Teacher> teachers;
  final ValueChanged<AddTimetableEvent> onAddEvent;

  const TimetableContent({
    Key key,
    @required this.viewportSize,
    @required this.dateRange,
    @required this.timeRange,
    @required this.timetable,
    @required this.events,
    @required this.courses,
    @required this.teachers,
    @required this.onAddEvent,
  }) : super(key: key);

  @override
  TimetableContentState createState() => TimetableContentState();
}

class TimetableContentState extends State<TimetableContent> {
  bool _isMouseActive = false;
  DateTime _tapToCreatePointedDate;
  TimeRange _tapToCreatePointedRange;

  DateTime _tapToCreateSelectedDate;
  TimeRange _tapToCreateSelectedRange;

  @override
  void didUpdateWidget(TimetableContent oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.dateRange != widget.dateRange ||
        oldWidget.timeRange != widget.timeRange ||
        oldWidget.timetable != widget.timetable ||
        oldWidget.events != widget.events) {
      _removeTapToCreateBlock();
    }
  }

  @override
  void dispose() {
    _removeTapToCreateBlock();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<List<TimetableEventWidgetData>> overlappingTimetableEvents = _OverlappingEventCalculator.calculate(
      context: context,
      timetable: widget.timetable,
      events: widget.events,
      courses: widget.courses,
      teachers: widget.teachers,
    );

    final double leftTimelinePadding = TimetableWidget.leftTimelinePadding(context);
    final double scheduleHorizontalPadding = TimetableWidget.scheduleHorizontalPadding(context);
    final double verticalPadding = TimetableWidget.verticalPadding(context);
    final double timelineWidth = TimetableWidget.timelineWidth(context);
    final double scheduleWidth =
        widget.viewportSize.width - leftTimelinePadding - timelineWidth - (2 * scheduleHorizontalPadding);
    final int daysSpan = widget.dateRange.durationInDays();
    final double eventSidePadding = TimetableWidget.eventSidePadding(context, daysSpan);
    final double widthPerDay = scheduleWidth / daysSpan;

    final double scheduleHeight = widget.viewportSize.height - (2 * verticalPadding);
    final int hoursSpan = widget.timeRange.duration.inHours;
    final double heightPerHour = scheduleHeight / hoursSpan;

    final _Viewport viewport = _Viewport(
      timelineWidth: timelineWidth,
      daysSpan: daysSpan,
      hoursSpan: hoursSpan,
      scheduleWidth: scheduleWidth,
      scheduleHeight: scheduleHeight,
      widthPerDay: widthPerDay,
      heightPerHour: heightPerHour,
      eventSidePadding: eventSidePadding,
      leftTimelinePadding: leftTimelinePadding,
      scheduleHorizontalPadding: scheduleHorizontalPadding,
      verticalPadding: verticalPadding,
    );

    return SizedBox(
      width: widget.viewportSize.width,
      height: widget.viewportSize.height,
      child: Stack(children: [
        ..._buildVerticalGrid(context, viewport),
        ..._buildHorizontalGrid(context, viewport),
        if (widget.timetable.type == TimetableType.custom && _tapToCreatePointedRange != null)
          _buildTapToCreatePointedBlock(context, viewport),
        if (widget.timetable.type == TimetableType.custom) _buildTapToCreateGrid(context, viewport),
        ..._buildTimeline(context, viewport),
        ..._buildTimetableEvents(context, viewport, overlappingTimetableEvents),
        if (widget.timetable.type == TimetableType.custom && _tapToCreateSelectedRange != null)
          _buildTapToCreateSelectedBlock(context, viewport),
        if (widget.dateRange.contains(DateTime.now()) && widget.timeRange.contains(Time.now()))
          _buildCurrentTimeIndicator(context, viewport),
      ]),
    );
  }

  Widget _buildTapToCreateGrid(BuildContext context, _Viewport vp) {
    Widget result = GestureDetector(
      onTapUp: (TapUpDetails details) {
        final Offset offset = details.localPosition;
        final Time tapTime = _convertTopOffsetToTime(offset.dy, vp);

        final DateTime tapToCreateSelectedDate = _convertLeftOffsetToDate(offset.dx, vp);
        final TimeRange tapToCreateSelectedRange = TimeRange(
          start: Time(hour: tapTime.hour, minute: 0),
          end: Time(hour: tapTime.hour + 1, minute: 0),
        );

        if (_isMouseActive) {
          if (tapToCreateSelectedDate == _tapToCreatePointedDate &&
              tapToCreateSelectedRange == _tapToCreatePointedRange) {
            _showCreateTimetableQuickDialog(context, tapToCreateSelectedDate, tapToCreateSelectedRange);
            setState(() => _removeTapToCreateBlock());
          }
        } else {
          setState(() {
            _tapToCreateSelectedDate = tapToCreateSelectedDate;
            _tapToCreateSelectedRange = tapToCreateSelectedRange;
          });
        }
      },
    );

    result = MouseRegion(
      cursor: SystemMouseCursors.click,
      onEnter: (PointerEvent details) {
        setState(() {
          _isMouseActive = true;
        });
      },
      onExit: (PointerEvent details) {
        setState(() {
          _isMouseActive = false;
          _tapToCreatePointedDate = null;
          _tapToCreatePointedRange = null;
        });
      },
      onHover: (PointerEvent details) {
        if (!_isMouseActive) return;

        setState(() {
          final Offset offset = details.localPosition;
          final Time tapTime = _convertTopOffsetToTime(offset.dy, vp);

          _tapToCreatePointedDate = _convertLeftOffsetToDate(offset.dx, vp);
          _tapToCreatePointedRange = TimeRange(
            start: Time(hour: tapTime.hour, minute: 0),
            end: Time(hour: tapTime.hour + 1, minute: 0),
          );
        });
      },
      child: result,
    );

    result = Positioned.fill(child: result);
    return result;
  }

  Widget _buildTapToCreateSelectedBlock(BuildContext context, _Viewport vp) {
    final double top = _convertTimeToTopOffset(_tapToCreateSelectedRange.start, vp);
    final double bottom = _convertTimeToTopOffset(_tapToCreateSelectedRange.end, vp);

    return Positioned(
      top: top,
      height: bottom - top,
      left: _convertDateToLeftOffset(_tapToCreateSelectedDate, vp),
      width: vp.widthPerDay - 2 * vp.eventSidePadding,
      child: InkWell(
        onTap: () {
          _showCreateTimetableQuickDialog(context, _tapToCreateSelectedDate, _tapToCreateSelectedRange);
          setState(() => _removeTapToCreateBlock());
        },
        child: Opacity(opacity: 0.8, child: _AddBlock()),
      ),
    );
  }

  Widget _buildTapToCreatePointedBlock(BuildContext context, _Viewport vp) {
    final double top = _convertTimeToTopOffset(_tapToCreatePointedRange.start, vp);
    final double bottom = _convertTimeToTopOffset(_tapToCreatePointedRange.end, vp);

    return Positioned(
      top: top,
      height: bottom - top,
      left: _convertDateToLeftOffset(_tapToCreatePointedDate, vp),
      width: vp.widthPerDay - 2 * vp.eventSidePadding,
      child: Opacity(opacity: 0.4, child: _AddBlock()),
    );
  }

  Iterable<Widget> _buildVerticalGrid(BuildContext context, _Viewport vp) sync* {
    final double leftPadding = vp.leftTimelinePadding + vp.timelineWidth + vp.scheduleHorizontalPadding;
    for (int i = 1; i < vp.daysSpan; i++) {
      yield Positioned(
        left: leftPadding + i * vp.widthPerDay,
        top: 0,
        bottom: 0,
        child: VerticalDivider(color: _getSecondaryDividerColor(context)),
      );
    }
  }

  Iterable<Widget> _buildHorizontalGrid(BuildContext context, _Viewport vp) sync* {
    for (int i = 0; i <= vp.hoursSpan; i++) {
      yield Positioned(
        left: vp.leftTimelinePadding + vp.timelineWidth,
        right: 0,
        top: vp.verticalPadding + vp.heightPerHour * i,
        child: Divider(color: i == 0 ? _getSecondaryDividerColor(context) : _getPrimaryDividerColor(context)),
      );

      if (i + 1 <= vp.hoursSpan) {
        yield Positioned(
          left: vp.leftTimelinePadding + vp.timelineWidth,
          right: 0,
          top: vp.verticalPadding + vp.heightPerHour * (i + 0.5),
          child: Divider(color: _getSecondaryDividerColor(context)),
        );
      }
    }
  }

  Iterable<Widget> _buildTimeline(BuildContext context, _Viewport vp) sync* {
    for (int i = 0; i <= vp.hoursSpan; i++) {
      final String text =
          _formatTime(Time(hour: widget.timeRange.start.hour + i, minute: widget.timeRange.start.minute));
      final double halfTextHeight = _measureTimelineHeight(context, text) / 2;

      yield Positioned(
        left: vp.leftTimelinePadding,
        width: vp.timelineWidth,
        top: vp.verticalPadding + vp.heightPerHour * i - halfTextHeight,
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: Theme.of(context)
              .textTheme
              .caption2(context)
              .copyWith(color: Theme.of(context).colorScheme.primaryVariant),
        ),
      );
    }
  }

  Iterable<Widget> _buildTimetableEvents(
      BuildContext context, _Viewport vp, List<List<TimetableEventWidgetData>> events) sync* {
    final double leftPadding = vp.leftTimelinePadding + vp.timelineWidth + vp.scheduleHorizontalPadding;

    for (List<TimetableEventWidgetData> group in events) {
      for (int i = 0; i < group.length; i++) {
        final TimetableEventWidgetData data = group[i];
        final int dayIndex = DateTimeRanges.durationInDaysOf(start: widget.dateRange.start, end: data.dateTime);

        final double left =
            leftPadding + dayIndex * vp.widthPerDay + vp.eventSidePadding + (i / group.length * vp.widthPerDay);

        final right = left + vp.widthPerDay / group.length - (2 * vp.eventSidePadding);
        final top = _convertTimeToTopOffset(data.timeRange.start, vp) + vp.eventSidePadding;
        final bottom = _convertTimeToTopOffset(data.timeRange.end, vp) - vp.eventSidePadding;

        if (top < bottom && left < right) {
          yield Positioned(
            key: ValueKey(data),
            left: left,
            top: top,
            width: right - left,
            height: bottom - top,
            child: TimetableEventWidget(data: data),
          );
        }
      }
    }
  }

  Widget _buildCurrentTimeIndicator(BuildContext context, _Viewport viewport) {
    final double bubbleSide = 6.0;
    return Positioned(
      left: viewport.leftTimelinePadding + viewport.timelineWidth - bubbleSide / 2,
      right: 0,
      top: _convertTimeToTopOffset(Time.now(), viewport) - bubbleSide / 2,
      child: Row(children: [
        Container(
          width: bubbleSide,
          height: bubbleSide,
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.secondary,
            shape: BoxShape.circle,
          ),
        ),
        Expanded(child: Divider(color: Theme.of(context).colorScheme.secondary)),
      ]),
    );
  }

  double _convertTimeToTopOffset(Time time, _Viewport viewport) {
    if (time.isBefore(widget.timeRange.start)) time = widget.timeRange.start;
    if (time.isAfter(widget.timeRange.end)) time = widget.timeRange.end;

    final int totalMinutesSpan = widget.timeRange.duration.inMinutes;
    final int minutesSpan = time.minus(widget.timeRange.start).inMinutes;
    return viewport.verticalPadding + minutesSpan / totalMinutesSpan * viewport.scheduleHeight;
  }

  Time _convertTopOffsetToTime(double topOffset, _Viewport viewport) {
    final int totalMinutesSpan = widget.timeRange.duration.inMinutes;
    final double minutesSpan = (topOffset - viewport.verticalPadding) * totalMinutesSpan / viewport.scheduleHeight;
    final Time time = widget.timeRange.start.plus(Duration(minutes: minutesSpan.toInt()));

    if (time.isBefore(widget.timeRange.start)) return widget.timeRange.start;
    if (time.isAfter(widget.timeRange.end)) return widget.timeRange.end;
    return time;
  }

  double _convertDateToLeftOffset(DateTime dateTime, _Viewport viewport) {
    final double leftPadding =
        viewport.leftTimelinePadding + viewport.timelineWidth + viewport.scheduleHorizontalPadding;
    final int dayIndex = DateTimeRanges.durationInDaysOf(start: widget.dateRange.start, end: dateTime);
    return leftPadding + dayIndex * viewport.widthPerDay + viewport.eventSidePadding;
  }

  DateTime _convertLeftOffsetToDate(double leftOffset, _Viewport viewport) {
    final double leftPadding =
        viewport.leftTimelinePadding + viewport.timelineWidth + viewport.scheduleHorizontalPadding;
    final double dayIndex = (leftOffset - leftPadding - viewport.eventSidePadding) / viewport.widthPerDay;
    final DateTime dateTime = widget.dateRange.start.plusDays(dayIndex.toInt());

    if (dateTime.isBefore(widget.dateRange.start)) return widget.dateRange.start.withoutTime();
    if (dateTime.isAfter(widget.dateRange.end)) return widget.dateRange.end.withoutTime();
    return dateTime;
  }

  double _measureTimelineHeight(BuildContext context, String text) {
    final TextPainter paint = TextPainter(
      text: TextSpan(
        text: text,
        style: Theme.of(context).textTheme.caption2(context),
      ),
      maxLines: 1,
      textScaleFactor: MediaQuery.of(context).textScaleFactor,
      textDirection: Directionality.of(context),
    );

    paint.layout();
    return paint.size.height;
  }

  String _formatTime(Time time) {
    return DateFormatter.formatTime(DateTime.now().withTime(time));
  }

  Color _getPrimaryDividerColor(BuildContext context) {
    return Theme.of(context).dividerColor.withAlpha(220);
  }

  Color _getSecondaryDividerColor(BuildContext context) {
    return Theme.of(context).dividerColor.withAlpha(160);
  }

  void _showCreateTimetableQuickDialog(BuildContext context, DateTime dateTime, TimeRange timeRange) async {
    final String courseId = await showCoursePickerDialog(context: context, courses: widget.courses);
    if (courseId == null) return;

    final AddTimetableEvent event = AddTimetableEvent(
      timetableId: widget.timetable.id,
      courseId: courseId,
      repeatOptions: EventRepeatOptions(
        type: EventRepeatOptionsType.weekly,
        dateRange: DateTimeRange(
          start: dateTime.withTime(timeRange.start),
          end: dateTime.withTime(timeRange.end),
        ),
        weeklyRepeatsOn: [Weekday.fromDateTime(dateTime)],
      ),
      updatedAt: DateTime.now(),
    );

    widget.onAddEvent(event);
  }

  void _removeTapToCreateBlock() {
    _isMouseActive = false;
    _tapToCreatePointedDate = null;
    _tapToCreatePointedRange = null;
    _tapToCreateSelectedDate = null;
    _tapToCreateSelectedRange = null;
  }
}

class _OverlappingEventCalculator {
  static List<List<TimetableEventWidgetData>> calculate({
    @required BuildContext context,
    @required Timetable timetable,
    @required List<SingleTimetableEvent> events,
    @required List<Course> courses,
    @required List<Teacher> teachers,
  }) {
    final List<List<TimetableEventWidgetData>> overlappingEvents = [];

    for (SingleTimetableEvent event in events) {
      final TimetableEventWidgetData data = TimetableEventWidgetData.from(
        context: context,
        timetableType: timetable.type,
        event: event,
        courses: courses,
        teachers: teachers,
      );

      final List<int> indices = overlappingEvents.indicesWhere((group) => group.any((e) => e.overlapsWith(data)));

      if (indices.isEmpty) {
        overlappingEvents.add([data]);
      } else if (indices.length == 1) {
        final List<TimetableEventWidgetData> list = overlappingEvents[indices.first];
        list.add(data);
        overlappingEvents[indices.first] = list;
      } else {
        final List<TimetableEventWidgetData> list = [data];
        for (int index in indices) {
          list.addAll(overlappingEvents[index]);
        }
        for (int index in indices.reversed) {
          overlappingEvents.removeAt(index);
        }
        overlappingEvents.add(list);
      }
    }

    return overlappingEvents;
  }
}

class _AddBlock extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.secondaryVariant,
        borderRadius: BorderRadius.all(Radius.circular(Dimens.grid_0_5x)),
      ),
      child: Icon(Icons.add, color: Theme.of(context).colorScheme.onSecondary),
    );
  }
}

class _Viewport extends Equatable {
  final double timelineWidth;
  final int daysSpan;
  final int hoursSpan;
  final double scheduleWidth;
  final double scheduleHeight;
  final double widthPerDay;
  final double heightPerHour;
  final double eventSidePadding;
  final double leftTimelinePadding;
  final double scheduleHorizontalPadding;
  final double verticalPadding;

  const _Viewport({
    @required this.timelineWidth,
    @required this.daysSpan,
    @required this.hoursSpan,
    @required this.scheduleWidth,
    @required this.scheduleHeight,
    @required this.widthPerDay,
    @required this.heightPerHour,
    @required this.eventSidePadding,
    @required this.leftTimelinePadding,
    @required this.scheduleHorizontalPadding,
    @required this.verticalPadding,
  });

  @override
  List<Object> get props => [
        timelineWidth,
        daysSpan,
        hoursSpan,
        scheduleWidth,
        scheduleHeight,
        widthPerDay,
        heightPerHour,
        eventSidePadding,
        leftTimelinePadding,
        scheduleHorizontalPadding,
        verticalPadding,
      ];
}
