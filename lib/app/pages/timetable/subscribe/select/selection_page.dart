import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/anim/animations.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/pages/timetable/subscribe/select/selection_view_model.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/list_item.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/page_placeholder.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/generated/l10n.dart';

typedef ListItemDataProvider = Future<List<ListItemData>> Function();
typedef ListItemDataCallback = void Function(ListItemData item);

/// Allows to select one [ListItemData].
class SelectionPage extends LifecycleWidget {
  const SelectionPage({
    Key key,
    @required this.provider,
    @required this.callback,
    @required this.title,
  }) : super(key: key);

  final ListItemDataProvider provider;
  final ListItemDataCallback callback;
  final String title;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _SelectionPageState();
}

class _SelectionPageState extends LifecycleWidgetState<SelectionPage>
    with ViewModelLifecycle<SelectionViewModel, SelectionPage>, SingleTickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();
  final TextEditingController _textController = TextEditingController();
  final FocusNode _textFocus = FocusNode();

  @override
  SelectionViewModel provideViewModel() => SelectionViewModel(widget.provider);

  @override
  void dispose() {
    _scrollController.dispose();
    _textController.dispose();
    _textFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ViewModelBuilder(
          viewModel: viewModel,
          builder: (BuildContext context, SelectionModel model) {
            if (model.showNoInternetState) {
              return _buildNoInternetState(context);
            } else if (model.showServerErrorState) {
              return _buildServerErrorState(context);
            } else {
              return _buildContent(context, model);
            }
          },
        ),
      ),
    );
  }

  Widget _buildNoInternetState(BuildContext context) {
    return PagePlaceholder(
      navType: NavButtonType.back,
      imageAsset: Images.illustration_router_192,
      imageWidth: 192,
      imageHeight: 192,
      message: S.of(context).common_noInternet_message,
      buttons: [
        RaisedButton(
          child: ButtonText(S.of(context).common_refresh),
          onPressed: () => viewModel.onRefresh(),
        ),
      ],
    );
  }

  Widget _buildServerErrorState(BuildContext context) {
    return PagePlaceholder(
      navType: NavButtonType.back,
      imageAsset: Images.illustration_router_192,
      imageWidth: 192,
      imageHeight: 192,
      message: S.of(context).common_somethingWentWrong,
      buttons: [
        RaisedButton(
          child: ButtonText(S.of(context).common_refresh),
          onPressed: () => viewModel.onRefresh(),
        ),
      ],
    );
  }

  Widget _buildContent(BuildContext context, SelectionModel model) {
    return CustomScrollView(
      controller: _scrollController,
      slivers: [
        _buildToolbar(context, model),
        SliverToBoxAdapter(child: _buildSearchField(context, model)),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int position) => _buildListItem(context, model.items[position]),
            childCount: model.items.length,
          ),
        ),
      ],
    );
  }

  Widget _buildToolbar(BuildContext context, SelectionModel model) {
    return Toolbar(
      vsync: this,
      actions: [
        BorderedImageButton(
          imageAsset: Images.ic_search_24,
          tooltip: S.of(context).common_search,
          onTap: () => viewModel.onEnableSearch(),
        ),
      ],
    );
  }

  Widget _buildSearchField(BuildContext context, SelectionModel model) {
    if (model.searchEnabled) {
      _scrollController.animateTo(0, duration: Animations.shortDuration, curve: Curves.fastOutSlowIn);
    }
    if (model.searchEnabled) {
      _textFocus.requestFocus();
    }

    final Paddings paddings = Paddings.of(context);
    return Padding(
      padding: EdgeInsets.only(
        left: paddings.horizontal - Dimens.grid_1_5x,
        right: paddings.horizontal - Dimens.grid_1_5x,
        bottom: Dimens.grid_3x,
      ),
      child: TextField(
        controller: _textController,
        focusNode: _textFocus,
        readOnly: !model.searchEnabled,
        keyboardType: TextInputType.name,
        textInputAction: TextInputAction.search,
        style: Theme.of(context).textTheme.headline4,
        decoration: InputDecoration(
          hintText: model.searchEnabled ? (S.of(context).common_search + '…') : widget.title,
          hintStyle: Theme.of(context).textTheme.headline4.copyWith(
                color: model.searchEnabled ? Theme.of(context).hintColor : Theme.of(context).colorScheme.primary,
              ),
        ),
        onChanged: (String text) => viewModel.onSearch(text),
      ),
    );
  }

  Widget _buildListItem(BuildContext context, ListItemData data) {
    return InkWell(
      key: ValueKey(data),
      onTap: () => widget.callback(data),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Paddings.of(context).horizontal,
          vertical: Dimens.grid_1x,
        ),
        child: ListItem(
          titleStyle: Theme.of(context).textTheme.bodyText1,
          data: data,
        ),
      ),
    );
  }
}
