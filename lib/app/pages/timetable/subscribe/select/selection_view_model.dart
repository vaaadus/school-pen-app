import 'package:equatable/equatable.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/app/pages/timetable/subscribe/select/selection_page.dart';
import 'package:school_pen/app/widgets/list_item.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/logger/logger.dart';

class SelectionModel extends Equatable {
  final List<ListItemData> items;
  final bool searchEnabled;
  final bool showNoInternetState;
  final bool showServerErrorState;

  const SelectionModel({
    this.items,
    this.searchEnabled = false,
    this.showNoInternetState = false,
    this.showServerErrorState = false,
  });

  @override
  List<Object> get props => [items, searchEnabled, showNoInternetState, showServerErrorState];
}

class SelectionViewModel extends ViewModel<SelectionModel, Object> {
  static const Logger _logger = Logger('SelectSchoolViewModel');
  final ListItemDataProvider _itemsProvider;

  List<ListItemData> _items;
  bool _searchEnabled = false;
  String _query;

  SelectionViewModel(this._itemsProvider);

  @override
  void onStart() async {
    super.onStart();
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _items = null;
    super.onStop();
  }

  void onEnableSearch() async {
    _searchEnabled = true;
    await _tryEmitModel();
  }

  void onSearch(String text) async {
    _query = text;

    await _tryEmitModel();
  }

  void onRefresh() async {
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } on NoInternetException {
      emitModel(SelectionModel(showNoInternetState: true));
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
      emitModel(SelectionModel(showServerErrorState: true));
    }
  }

  Future<SelectionModel> get _model async {
    _items ??= await _itemsProvider();

    return SelectionModel(
      items: _findItems(_query, _items),
      searchEnabled: _searchEnabled,
    );
  }

  List<ListItemData> _findItems(String query, List<ListItemData> items) {
    if (Strings.isBlank(query)) return items;
    query = query.toLowerCase();

    return items.where((e) => _matchesQuery(query, e)).toList();
  }

  bool _matchesQuery(String query, ListItemData item) {
    return (item.title?.toLowerCase()?.contains(RegExp(query)) ?? false) ||
        (item.label?.toLowerCase()?.contains(RegExp(query)) ?? false);
  }
}
