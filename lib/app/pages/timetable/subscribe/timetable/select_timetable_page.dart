import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/pages/timetable/subscribe/select/selection_page.dart';
import 'package:school_pen/app/widgets/list_item.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';
import 'package:school_pen/generated/l10n.dart';

/// Presents the second step to subscribe to a timetable: timetable selection.
class SelectTimetablePage extends StatefulWidget {
  final String schoolId;

  const SelectTimetablePage({Key key, @required this.schoolId}) : super(key: key);

  @override
  _SelectTimetablePageState createState() => _SelectTimetablePageState();
}

class _SelectTimetablePageState extends State<SelectTimetablePage> with ProgressOverlayMixin {
  final TimetableManager _manager = Injection.get();

  @override
  Widget build(BuildContext context) {
    return SelectionPage(
      provider: () => _provideItems(context),
      callback: (ListItemData data) => _callback(context, data),
      title: S.of(context).common_selectYourTimetable,
    );
  }

  Future<List<ListItemData>> _provideItems(BuildContext context) async {
    updateProgressOverlay(true);

    try {
      final List<TimetableInfo> timetables = await _fetchTimetables();
      return timetables.map((timetable) {
        return ListItemData(
          title: timetable.name,
          label: ResourceLocator.timetableTypeToText(context, timetable.type),
          customData: timetable,
        );
      }).toList();
    } finally {
      updateProgressOverlay(false);
    }
  }

  Future<List<TimetableInfo>> _fetchTimetables() async {
    final List<TimetableInfo> timetables = await _manager.getTimetablesInfoOfSchool(widget.schoolId);
    final List<TimetableInfo> userTimetables = await _manager.getUserTimetablesInfo();
    final List<String> userTimetablesIds = userTimetables.map((e) => e.id).toList();
    return timetables.where((e) => !userTimetablesIds.contains(e.id)).toList();
  }

  void _callback(BuildContext context, ListItemData data) async {
    updateProgressOverlay(true);

    try {
      final TimetableInfo timetable = data.customData as TimetableInfo;
      await _manager.subscribeToPublicTimetable(timetable.id);
      await _manager.setActiveTimetable(timetable.id);
      showSuccessToast(message: S.of(context).confirmation_subscribed);

      Navigator.of(context).popUntil((route) => ![
            R.timetable_subscribe_selectSchool,
            R.timetable_subscribe_selectTimetable,
          ].contains(route?.settings?.name));
    } finally {
      updateProgressOverlay(false);
    }
  }
}
