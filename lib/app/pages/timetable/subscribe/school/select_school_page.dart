import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/timetable/subscribe/select/selection_page.dart';
import 'package:school_pen/app/widgets/list_item.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/domain/timetable/model/school.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';
import 'package:school_pen/generated/l10n.dart';

/// Presents the first step to subscribe to a timetable: school selection.
class SelectSchoolPage extends StatefulWidget {
  const SelectSchoolPage({Key key}) : super(key: key);

  @override
  _SelectSchoolPageState createState() => _SelectSchoolPageState();
}

class _SelectSchoolPageState extends State<SelectSchoolPage> with ProgressOverlayMixin {
  final TimetableManager _manager = Injection.get();

  @override
  Widget build(BuildContext context) {
    return SelectionPage(
      provider: _provideItems,
      callback: _callback,
      title: S.of(context).common_selectYourSchool,
    );
  }

  Future<List<ListItemData>> _provideItems() async {
    updateProgressOverlay(true);

    try {
      final List<School> schools = await _manager.getSchools();

      return schools.map((school) {
        return ListItemData(
          title: school.name,
          label: '${school.address.city}, ${school.address.country}',
          customData: school,
        );
      }).toList();
    } finally {
      updateProgressOverlay(false);
    }
  }

  void _callback(ListItemData data) {
    Nav.timetable_subscribe_selectTimetable(schoolId: (data.customData as School).id);
  }
}
