import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/common/formatter/date_formatter.dart';
import 'package:school_pen/app/common/formatter/event_repeat_options_formatter.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/pages/picker/event_repeat_options_edit_mode_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/pages/timetable/event/timetable_event_view_model.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/save_or_discard_footer.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/generated/l10n.dart';

/// Offers the user a possibility to edit a timetable event (if [eventId] not null)
/// or create a new one if event is null.
class TimetableEventPage extends LifecycleWidget {
  /// The timetable id to which the event will be linked. Must be null if the [eventId] is specified.
  final String timetableId;

  /// The id of the event which will be edited.
  final String eventId;

  /// The id of the event from which all data should be copied and saved as a new event.
  final String copyFromEventId;

  /// The date time range when the event reoccurs.
  final DateTimeRange reoccursAt;

  const TimetableEventPage({
    Key key,
    this.timetableId,
    this.eventId,
    this.copyFromEventId,
    this.reoccursAt,
  })  : assert(
          ((timetableId != null ? 1 : 0) + (eventId != null ? 1 : 0) + (copyFromEventId != null ? 1 : 0)) == 1,
          'Only one property can be non-null',
        ),
        assert(eventId == null || reoccursAt != null),
        assert(copyFromEventId == null || reoccursAt != null),
        super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _TimetableEventPageState();
}

class _TimetableEventPageState extends LifecycleWidgetState<TimetableEventPage>
    with
        ViewModelLifecycle<TimetableEventViewModel, TimetableEventPage>,
        ProgressOverlayMixin,
        SingleTickerProviderStateMixin {
  static const String _repeatOption_everyDay = 'repeatOption_everyDay';
  static const String _repeatOption_everyWeek = 'repeatOption_everyWeek';
  static const String _repeatOption_everyMonth = 'repeatOption_everyMonth';
  static const String _repeatOption_custom = 'repeatOption_custom';
  static const String _repeatOption_doesNotRepeat = 'repeatOption_doesNotRepeat';

  final TextEditingController _roomController = TextEditingController();
  final TextEditingController _teacherController = TextEditingController();
  final TextEditingController _noteController = TextEditingController();

  final FocusNode _roomFocus = FocusNode();
  final FocusNode _teacherFocus = FocusNode();
  final FocusNode _noteFocus = FocusNode();

  @override
  TimetableEventViewModel provideViewModel() {
    return TimetableEventViewModel(Injection.get(), Injection.get(), Injection.get(), widget.timetableId,
        widget.eventId, widget.copyFromEventId, widget.reoccursAt);
  }

  @override
  void dispose() {
    _roomController.dispose();
    _teacherController.dispose();
    _noteController.dispose();
    _roomFocus.dispose();
    _teacherFocus.dispose();
    _noteFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder(
      viewModel: viewModel,
      builder: (BuildContext context, TimetableEventModel model) {
        updateProgressOverlay(model.progressOverlay);
        return _buildScaffold(context, model);
      },
      onSignal: (BuildContext context, TimetableEventSignal signal) {
        if (signal == TimetableEventSignal.notifyEventSaved) {
          showSuccessToast(message: S.of(context).confirmation_saveSuccess);
        } else if (signal == TimetableEventSignal.pop) {
          Navigator.of(context).pop();
        }
      },
    );
  }

  Widget _buildScaffold(BuildContext context, TimetableEventModel model) {
    return SystemUiOverlayAnnotation(
      systemNavigationBarColor: Theme.of(context).colorScheme.surface,
      child: Scaffold(
        body: SafeArea(child: _buildContent(context, model)),
      ),
    );
  }

  Widget _buildContent(BuildContext context, TimetableEventModel model) {
    Widget result = CustomScrollView(slivers: [
      Toolbar(
        vsync: this,
        onBack: (BuildContext context) => _onWillPop(context, model),
        navType: NavButtonType.close,
        horizontalPadding: Dimens.grid_3x,
      ),
      SliverList(
        delegate: SliverChildListDelegate([
          _buildCourse(context, model),
          Divider(),
          _buildDateTime(context, model),
          Divider(),
          _buildRepeatOptions(context, model),
          Divider(),
          _buildRoom(context, model),
          Divider(),
          _buildTeachers(context, model),
          Divider(),
          _buildNote(context, model),
          Divider(),
          Container(height: Paddings.of(context).vertical),
        ]),
      ),
    ]);

    result = Column(children: [
      Expanded(child: result),
      SaveOrDiscardFooter(
        label: S.of(context).common_lesson,
        onSave: () => _onSave(context, model),
        onDiscard: () => Navigator.of(context).pop(),
      ),
    ]);

    result = WillPopScope(
      onWillPop: () => _onWillPop(context, model),
      child: result,
    );

    return result;
  }

  Widget _buildCourse(BuildContext context, TimetableEventModel model) {
    final String addCourseId = 'add-course-id';

    return Padding(
      padding: EdgeInsets.only(top: Dimens.grid_2x),
      child: MergeSemantics(
        child: PopupMenuButton(
          itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
            for (Course course in model.availableCourses)
              PopupMenuItem(
                value: course.id,
                child: Text(course.name),
              ),
            PopupMenuDivider(),
            PopupMenuItem(
              value: addCourseId,
              child: Text(
                S.of(context).common_addCourse,
                style: Theme.of(context).popupMenuTheme.textStyle.copyWith(
                      color: Theme.of(context).colorScheme.secondary,
                    ),
              ),
            ),
          ],
          onSelected: (String id) async {
            _unfocusTextFields();

            if (id == addCourseId) {
              final String courseId = await Nav.courses_add();
              if (courseId != null) viewModel.onEditCourse(courseId);
            } else {
              viewModel.onEditCourse(id);
            }
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                  top: Dimens.grid_2x,
                  bottom: Dimens.grid_2x,
                  left: Dimens.grid_3x,
                ),
                child: Image.asset(
                  Images.ic_book_24,
                  width: 24,
                  height: 24,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  excludeFromSemantics: true,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(
                    top: Dimens.grid_2x,
                    bottom: model.courseError == null ? Dimens.grid_2x : Dimens.grid_1x,
                    left: Dimens.grid_3x,
                    right: Dimens.grid_3x,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        model.selectedCourse?.name ?? S.of(context).common_pickCourse,
                        style: Theme.of(context).textTheme.bodyText1.copyWith(
                              color: model.selectedCourse?.name != null
                                  ? Theme.of(context).colorScheme.primary
                                  : Theme.of(context).colorScheme.primaryVariant,
                            ),
                      ),
                      if (model.courseError != null)
                        Padding(
                          padding: EdgeInsets.only(top: Dimens.grid_1x, bottom: Dimens.grid_1x),
                          child: Text(
                            ResourceLocator.errorToText(context, model.courseError),
                            style: Theme.of(context).textTheme.caption.copyWith(
                                  color: Theme.of(context).colorScheme.error,
                                ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildDateTime(BuildContext context, TimetableEventModel model) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            Images.ic_timetable_24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: Dimens.grid_1x),
                child: Row(children: [
                  Expanded(
                    child: InkWell(
                      onTap: () => _showStartDatePicker(context, model),
                      child: Padding(
                        padding: EdgeInsets.only(top: Dimens.grid_1x, bottom: Dimens.grid_1x, left: Dimens.grid_3x),
                        child: Text(
                          _formatDate(model.dateRange.start),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () => _showStartTimePicker(context, model),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: Dimens.grid_1x, horizontal: Dimens.grid_3x),
                      child: Text(
                        DateFormatter.formatTime(model.dateRange.start),
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  )
                ]),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: model.endDateError == null ? Dimens.grid_1x : 0),
                child: Row(children: [
                  Expanded(
                    child: InkWell(
                      onTap: () => _showEndDatePicker(context, model),
                      child: Padding(
                        padding: EdgeInsets.only(top: Dimens.grid_1x, bottom: Dimens.grid_1x, left: Dimens.grid_3x),
                        child: Text(
                          _formatDate(model.dateRange.end),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () => _showEndTimePicker(context, model),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: Dimens.grid_1x, horizontal: Dimens.grid_3x),
                      child: Text(
                        DateFormatter.formatTime(model.dateRange.end),
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  )
                ]),
              ),
              if (model.endDateError != null)
                Padding(
                  padding: EdgeInsets.only(left: Dimens.grid_3x, bottom: Dimens.grid_1x),
                  child: Text(
                    ResourceLocator.errorToText(context, model.endDateError),
                    style: Theme.of(context).textTheme.caption.copyWith(
                          color: Theme.of(context).colorScheme.error,
                        ),
                  ),
                ),
            ],
          ),
        ),
      ],
    );
  }

  String _formatDate(DateTime date) {
    return DateFormat.E().addPattern(',', '').add_MMMMd().format(date);
  }

  Widget _buildRepeatOptions(BuildContext context, TimetableEventModel model) {
    return MergeSemantics(
      child: PopupMenuButton(
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          PopupMenuItem(value: _repeatOption_everyDay, child: Text(S.of(context).repeatOptions_everyDay)),
          PopupMenuItem(value: _repeatOption_everyWeek, child: Text(S.of(context).repeatOptions_everyWeek)),
          PopupMenuItem(value: _repeatOption_everyMonth, child: Text(S.of(context).repeatOptions_everyMonth)),
          PopupMenuItem(value: _repeatOption_custom, child: Text(S.of(context).repeatOptions_custom)),
          PopupMenuDivider(),
          PopupMenuItem(
            value: _repeatOption_doesNotRepeat,
            child: Text(
              S.of(context).repeatOptions_doesNotRepeat,
              style: Theme.of(context).popupMenuTheme.textStyle.copyWith(
                    color: Theme.of(context).colorScheme.error,
                  ),
            ),
          ),
        ],
        onSelected: (String id) async {
          _unfocusTextFields();

          if (id == _repeatOption_everyDay) {
            viewModel.onSelectRepeatsDaily();
          } else if (id == _repeatOption_everyWeek) {
            viewModel.onSelectRepeatsWeekly();
          } else if (id == _repeatOption_everyMonth) {
            viewModel.onSelectRepeatsMonthly();
          } else if (id == _repeatOption_custom) {
            _showRepeatOptionsPicker(context, model);
          } else if (id == _repeatOption_doesNotRepeat) {
            viewModel.onSelectDoesNotRepeat();
          }
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                top: Dimens.grid_2x,
                bottom: Dimens.grid_2x,
                left: Dimens.grid_3x,
              ),
              child: Image.asset(
                Images.ic_loop_24,
                width: 24,
                height: 24,
                color: Theme.of(context).colorScheme.primaryVariant,
                excludeFromSemantics: true,
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(
                  top: Dimens.grid_2x,
                  bottom: Dimens.grid_2x,
                  left: Dimens.grid_3x,
                  right: Dimens.grid_3x,
                ),
                child: Text(
                  EventRepeatOptionsFormatter.format(context, model.event.repeatOptions),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRoom(BuildContext context, TimetableEventModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_location_24,
      value: model.event.customRoom,
      placeholder:
          Strings.isNotBlank(model.selectedCourse?.room) ? model.selectedCourse.room : S.of(context).common_room,
      controller: _roomController,
      focus: _roomFocus,
      model: model,
      onChanged: (String text) => viewModel.onEditRoom(text),
      onSubmitted: (String text) => _teacherFocus.requestFocus(),
    );
  }

  Widget _buildTeachers(BuildContext context, TimetableEventModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_person_24,
      value: model.event.customTeacher,
      placeholder: model.selectedTeachers.isNotEmpty
          ? model.selectedTeachers.map((e) => e.name).join(' · ')
          : S.of(context).common_teacher,
      controller: _teacherController,
      focus: _teacherFocus,
      model: model,
      textCapitalization: TextCapitalization.words,
      onChanged: (String text) => viewModel.onEditTeacher(text),
      onSubmitted: (String text) => _noteFocus.requestFocus(),
    );
  }

  Widget _buildNote(BuildContext context, TimetableEventModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_description_24,
      value: model.event.note,
      placeholder: S.of(context).common_note,
      controller: _noteController,
      focus: _noteFocus,
      model: model,
      textInputAction: TextInputAction.newline,
      keyboardType: TextInputType.multiline,
      multiline: true,
      onChanged: (String text) => viewModel.onEditNote(text),
    );
  }

  Widget _buildTextFieldWithIcon({
    @required BuildContext context,
    @required String imageAsset24,
    @required String value,
    @required String placeholder,
    @required TextEditingController controller,
    @required FocusNode focus,
    @required TimetableEventModel model,
    TextInputType keyboardType = TextInputType.text,
    TextInputAction textInputAction = TextInputAction.next,
    TextCapitalization textCapitalization = TextCapitalization.none,
    bool multiline = false,
    ValueChanged<String> onChanged,
    ValueChanged<String> onSubmitted,
  }) {
    controller.setTextIfChanged(value);

    Widget result = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            imageAsset24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_1_5x, vertical: Dimens.grid_0_5x),
            child: TextField(
              controller: controller,
              focusNode: focus,
              style: Theme.of(context).textTheme.bodyText1,
              textInputAction: textInputAction,
              keyboardType: keyboardType,
              textCapitalization: textCapitalization,
              maxLines: multiline ? null : 1,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: placeholder,
              ),
              onChanged: onChanged,
              onSubmitted: onSubmitted,
            ),
          ),
        ),
      ],
    );

    return result;
  }

  void _onSave(BuildContext context, TimetableEventModel model) async {
    final EventRepeatOptionsEditMode mode = await showEventRepeatOptionsEditModeDialog(
      context: context,
      routeSettings: RouteSettings(name: R.timetable_eventSave),
      editModes: model.event.repeatOptions.getAllowedUpdateModes(
        model.originalEvent?.repeatOptions,
        model.reoccursAt,
      ),
    );

    if (mode != null) {
      viewModel.onSaveEvent(mode);
    }
  }

  Future<bool> _onWillPop(BuildContext context, TimetableEventModel model) async {
    if (model.discardChangesPopup) {
      _unfocusTextFields();
      _showSaveChangesDialog(context);
    } else {
      Navigator.of(context).pop();
    }
    return false;
  }

  void _showSaveChangesDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_discardChangesQuestion,
      primaryAction: DialogActionData(
        text: S.of(context).common_keepEditing,
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_discard,
        onTap: () => Navigator.of(context).pop(),
      ),
    );
  }

  void _showStartDatePicker(BuildContext context, TimetableEventModel model) async {
    DateTime dateTime = await showAdaptiveDatePicker(
      context: context,
      initialDate: model.dateRange.start,
    );

    if (dateTime != null) {
      dateTime = dateTime.asUtc();
      dateTime = dateTime.withTime(model.dateRange.start.time);
      viewModel.onEditStartDate(dateTime);
    }
  }

  void _showStartTimePicker(BuildContext context, TimetableEventModel model) async {
    final Time time = await showAdaptiveTimePicker(
      context: context,
      initialTime: model.dateRange.start.time,
    );

    if (time != null) {
      final DateTime dateTime = model.dateRange.start.withTime(time);
      viewModel.onEditStartDate(dateTime);
    }
  }

  void _showEndDatePicker(BuildContext context, TimetableEventModel model) async {
    DateTime dateTime = await showAdaptiveDatePicker(
      context: context,
      initialDate: model.dateRange.end,
    );

    if (dateTime != null) {
      dateTime = dateTime.asUtc();
      dateTime = dateTime.withTime(model.dateRange.end.time);
      viewModel.onEditEndDate(dateTime);
    }
  }

  void _showEndTimePicker(BuildContext context, TimetableEventModel model) async {
    final Time time = await showAdaptiveTimePicker(
      context: context,
      initialTime: model.dateRange.end.time,
    );

    if (time != null) {
      final DateTime dateTime = model.dateRange.end.withTime(time);
      viewModel.onEditEndDate(dateTime);
    }
  }

  void _showRepeatOptionsPicker(BuildContext context, TimetableEventModel model) async {
    final EventRepeatOptions options = await Nav.event_repeatOptions(initialOptions: model.event.repeatOptions);
    if (options != null) {
      viewModel.onEditRepeatOptions(options);
    }
  }

  void _unfocusTextFields() {
    _roomFocus.unfocus();
    _teacherFocus.unfocus();
    _noteFocus.unfocus();
  }
}
