import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/exception/date_range_start_after_end_exception.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/teacher_listener.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/timetable_listener.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';

class TimetableEventModel extends Equatable {
  final bool progressOverlay;
  final bool discardChangesPopup;
  final TimetableEvent originalEvent;
  final AddTimetableEvent event;
  final Course selectedCourse;
  final List<Course> availableCourses;
  final Exception courseError;
  final DateTimeRange dateRange;
  final DateTimeRange reoccursAt;
  final Exception endDateError;
  final List<Teacher> selectedTeachers;

  const TimetableEventModel({
    @required this.progressOverlay,
    @required this.discardChangesPopup,
    this.originalEvent,
    @required this.event,
    this.selectedCourse,
    @required this.availableCourses,
    this.courseError,
    @required this.dateRange,
    this.reoccursAt,
    this.endDateError,
    @required this.selectedTeachers,
  });

  @override
  List<Object> get props => [
        progressOverlay,
        discardChangesPopup,
        originalEvent,
        event,
        selectedCourse,
        availableCourses,
        courseError,
        dateRange,
        reoccursAt,
        endDateError,
        selectedTeachers,
      ];
}

enum TimetableEventSignal {
  pop,
  notifyEventSaved,
}

class TimetableEventViewModel extends ViewModel<TimetableEventModel, TimetableEventSignal>
    implements TimetableListener, CourseListener, TeacherListener {
  static const Logger _logger = Logger('TimetableEventViewModel');

  final TimetableManager _timetableManager;
  final CourseManager _courseManager;
  final TeacherManager _teacherManager;
  final String _timetableId;
  final String _eventId;
  final String _copyFromEventId;
  final DateTimeRange _reoccursAt;

  String _schoolYearId;
  bool _progressOverlay = false;
  TimetableEvent _originalEvent;
  AddTimetableEvent _editedEvent;
  DateTimeRange _dateRange;
  Exception _courseError;
  Exception _endDateError;

  TimetableEventViewModel(
    this._timetableManager,
    this._courseManager,
    this._teacherManager,
    this._timetableId,
    this._eventId,
    this._copyFromEventId,
    this._reoccursAt,
  ) {
    _dateRange = _reoccursAt;
  }

  @override
  void onStart() async {
    super.onStart();
    _timetableManager.addTimetableListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _teacherManager.addTeacherListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _timetableManager.removeTimetableListener(this);
    _courseManager.removeCourseListener(this);
    _teacherManager.removeTeacherListener(this);
    super.onStop();
  }

  @override
  void onTimetablesChanged(TimetableManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onTeachersChanged(TeacherManager manager) async {
    await _tryEmitModel();
  }

  void onSaveEvent(EventRepeatOptionsEditMode mode) async {
    final AddTimetableEvent event = _editedEvent;
    if (!await _validate(event)) return;

    await _tryWithProgressOverlay(() async {
      if (event.repeatOptions.isNotEmpty) {
        if (_eventId != null) {
          await _timetableManager.updateTimetableEvent(_eventId, event, _reoccursAt, mode);
        } else {
          await _timetableManager.createTimetableEvent(event);
        }
      } else if (_eventId != null) {
        await _timetableManager.deleteTimetableEvent(_eventId, _reoccursAt, EventRepeatOptionsEditMode.allEvents);
      }
      emitSignal(TimetableEventSignal.notifyEventSaved);
      emitSingleSignal(TimetableEventSignal.pop);
    });
  }

  void onEditCourse(String courseId) async {
    _editedEvent = _editedEvent.copyWith(courseId: Optional(courseId));

    if (await _validate(_editedEvent)) {
      await _tryEmitModel();
    }
  }

  void onEditStartDate(DateTime dateTime) async {
    DateTime endDate = _dateRange.end.add(dateTime.difference(_dateRange.start));
    if (endDate.isBeforeOrAt(dateTime)) endDate = dateTime.add(Duration(hours: 1));

    _dateRange = DateTimeRange(start: dateTime, end: endDate);
    _updateRepeatOptions(_dateRange);

    if (await _validate(_editedEvent)) {
      await _tryEmitModel();
    }
  }

  void onEditEndDate(DateTime dateTime) async {
    _dateRange = DateTimeRange(start: _dateRange.start, end: dateTime);
    _updateRepeatOptions(_dateRange);

    if (await _validate(_editedEvent)) {
      await _tryEmitModel();
    }
  }

  void _updateRepeatOptions(DateTimeRange dateRange) {
    EventRepeatOptions options;

    if (_originalEvent == null || _reoccursAt == null || _reoccursAt != dateRange) {
      List<Weekday> weeklyRepeatsOn = _editedEvent.repeatOptions.weeklyRepeatsOn;

      if (_editedEvent.repeatOptions.type == EventRepeatOptionsType.weekly &&
          !weeklyRepeatsOn.contains(Weekday.fromDateTime(dateRange.start))) {
        weeklyRepeatsOn = [Weekday.fromDateTime(_dateRange.start)];
      }

      options = _editedEvent.repeatOptions.copyWith(
        dateRange: Optional(dateRange),
        weeklyRepeatsOn: Optional(weeklyRepeatsOn),
      );
    } else {
      options = _originalEvent.repeatOptions;
    }

    _editedEvent = _editedEvent.copyWith(repeatOptions: Optional(options));
  }

  void onSelectRepeatsDaily() async {
    final EventRepeatOptions options = EventRepeatOptions(
      type: EventRepeatOptionsType.daily,
      dateRange: _editedEvent.repeatOptions.dateRange,
    );

    _editedEvent = _editedEvent.copyWith(repeatOptions: Optional(options));

    await _tryEmitModel();
  }

  void onSelectRepeatsWeekly() async {
    final EventRepeatOptions options = EventRepeatOptions(
      type: EventRepeatOptionsType.weekly,
      dateRange: _editedEvent.repeatOptions.dateRange,
      weeklyRepeatsOn: [Weekday.fromDateTime(_editedEvent.repeatOptions.dateRange.start)],
    );

    _editedEvent = _editedEvent.copyWith(repeatOptions: Optional(options));

    await _tryEmitModel();
  }

  void onSelectRepeatsMonthly() async {
    final EventRepeatOptions options = EventRepeatOptions(
      type: EventRepeatOptionsType.monthly,
      dateRange: _editedEvent.repeatOptions.dateRange,
    );

    _editedEvent = _editedEvent.copyWith(repeatOptions: Optional(options));

    await _tryEmitModel();
  }

  void onSelectDoesNotRepeat() async {
    final EventRepeatOptions options = EventRepeatOptions.single(_editedEvent.repeatOptions.dateRange);

    _editedEvent = _editedEvent.copyWith(repeatOptions: Optional(options));

    await _tryEmitModel();
  }

  void onEditRepeatOptions(EventRepeatOptions options) async {
    if (_editedEvent.repeatOptions.dateRange != options.dateRange) {
      _dateRange = options.dateRange;
    }

    _editedEvent = _editedEvent.copyWith(repeatOptions: Optional(options));

    await _tryEmitModel();
  }

  void onEditRoom(String text) async {
    _editedEvent = _editedEvent.copyWith(customRoom: Optional(text));

    await _tryEmitModel();
  }

  void onEditTeacher(String text) async {
    _editedEvent = _editedEvent.copyWith(customTeacher: Optional(text));

    await _tryEmitModel();
  }

  void onEditNote(String text) async {
    _editedEvent = _editedEvent.copyWith(note: Optional(text));

    await _tryEmitModel();
  }

  Future<bool> _validate(AddTimetableEvent event) async {
    _courseError = null;
    _endDateError = null;
    await _tryEmitModel();

    if (Strings.isBlank(event.courseId)) {
      _courseError = FieldMustNotBeEmptyException();
      await _tryEmitModel();
      return false;
    } else if (event.repeatOptions.dateRange.start.isAfterOrAt(event.repeatOptions.dateRange.end)) {
      _endDateError = DateRangeStartAfterEndException();
      await _tryEmitModel();
      return false;
    } else {
      return true;
    }
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<TimetableEventModel> get _model async {
    final AddTimetableEvent event = await _obtainEvent();
    final List<Course> courses = await _courseManager.getAll(await _obtainSchoolYearId());
    final Course course = await _courseManager.getById(event.courseId);
    final List<Teacher> teachers = course == null ? [] : await _teacherManager.getAll(ids: course.teachersIds);

    final DateTime now = DateTime.now();
    final AddTimetableEvent eventNow = event.copyWith(updatedAt: Optional(now));
    final AddTimetableEvent originalNow = _originalEvent?.toAddTimetableEvent()?.copyWith(updatedAt: Optional(now));

    return TimetableEventModel(
      progressOverlay: _progressOverlay,
      discardChangesPopup: _originalEvent == null || eventNow != originalNow,
      originalEvent: _originalEvent,
      event: event,
      selectedCourse: course,
      availableCourses: courses.where((e) => e.id != course?.id).toList(),
      courseError: _courseError,
      dateRange: _dateRange,
      reoccursAt: _reoccursAt,
      endDateError: _endDateError,
      selectedTeachers: teachers,
    );
  }

  Future<String> _obtainSchoolYearId() async {
    if (_schoolYearId != null) return _schoolYearId;

    if (_timetableId != null) {
      final TimetableInfo timetable = await _timetableManager.getTimetableInfo(_timetableId);
      _schoolYearId = timetable.schoolYearId;
    } else if (_eventId != null) {
      final TimetableEvent event = await _timetableManager.getTimetableEventById(_eventId);
      final TimetableInfo timetable = await _timetableManager.getTimetableInfo(event.timetableId);
      _schoolYearId = timetable.schoolYearId;
    } else if (_copyFromEventId != null) {
      final TimetableEvent event = await _timetableManager.getTimetableEventById(_copyFromEventId);
      final TimetableInfo timetable = await _timetableManager.getTimetableInfo(event.timetableId);
      _schoolYearId = timetable.schoolYearId;
    }

    return _schoolYearId;
  }

  Future<AddTimetableEvent> _obtainEvent() async {
    if (_editedEvent != null) return _editedEvent;

    if (_eventId != null) {
      _originalEvent = await _timetableManager.getTimetableEventById(_eventId);
      _editedEvent = _originalEvent?.toAddTimetableEvent()?.copyWith(updatedAt: Optional(DateTime.now()));
      assert(_dateRange != null);
    } else if (_copyFromEventId != null) {
      final TimetableEvent event = await _timetableManager.getTimetableEventById(_copyFromEventId);
      _editedEvent = event?.toAddTimetableEvent()?.copyWith(updatedAt: Optional(DateTime.now()));
      assert(_dateRange != null);
    } else {
      final DateTime now = DateTimes.utcNow();

      _dateRange ??= DateTimeRange(
        start: now,
        end: now.add(Duration(hours: 1)),
      );

      _editedEvent = AddTimetableEvent(
        timetableId: _timetableId,
        courseId: null,
        customRoom: '',
        customTeacher: '',
        customCourse: '',
        customStudent: '',
        repeatOptions: EventRepeatOptions(
          type: EventRepeatOptionsType.weekly,
          dateRange: _dateRange,
          weeklyRepeatsOn: [Weekday.fromDateTime(_dateRange.start)],
        ),
        note: '',
        updatedAt: DateTime.now(),
      );
    }

    return _editedEvent;
  }
}
