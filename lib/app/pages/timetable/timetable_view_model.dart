import 'dart:ui';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/preferences/preferences_listener.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/teacher_listener.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';
import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/timetable_listener.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';

class TimetableModel extends Equatable {
  final bool progressOverlay;
  final bool showEmptyState;
  final bool showNoInternetState;
  final bool showNotFoundState;
  final bool subscribeAvailable;
  final SchoolYear schoolYear;
  final TimetableViewMode viewMode;
  final Timetable timetable;
  final List<TimetableInfo> availableTimetables;
  final List<Course> courses;
  final List<Teacher> teachers;

  const TimetableModel({
    this.progressOverlay = false,
    this.showEmptyState = false,
    this.showNoInternetState = false,
    this.showNotFoundState = false,
    this.subscribeAvailable = false,
    this.schoolYear,
    this.viewMode,
    this.timetable,
    this.availableTimetables,
    this.courses,
    this.teachers,
  });

  @override
  List<Object> get props => [
        progressOverlay,
        showEmptyState,
        showNoInternetState,
        showNotFoundState,
        subscribeAvailable,
        schoolYear,
        viewMode,
        timetable,
        availableTimetables,
        courses,
        teachers,
      ];
}

enum TimetableSignal {
  notifyUnsubscribedFromTimetable,
  notifyTimetableCreated,
  notifyTimetableDuplicated,
  notifyTimetableRenamed,
  notifyTimetableDeleted,
}

enum TimetableViewMode {
  day,
  threeDays,
  week,
}

extension TimetableViewModeExt on TimetableViewMode {
  static TimetableViewMode forTag(String tag) {
    for (TimetableViewMode mode in TimetableViewMode.values) {
      if (mode.tag == tag) return mode;
    }
    return null;
  }

  String get tag {
    switch (this) {
      case TimetableViewMode.day:
        return 'day';
      case TimetableViewMode.threeDays:
        return '3_days';
      case TimetableViewMode.week:
        return 'week';
    }
    throw ArgumentError('Unsupported view mode: $this');
  }

  int get daysSpan {
    switch (this) {
      case TimetableViewMode.day:
        return 1;
      case TimetableViewMode.threeDays:
        return 3;
      case TimetableViewMode.week:
        return 7;
    }

    throw ArgumentError('Unsupported view mode: $this');
  }
}

class TimetableViewModel extends ViewModel<TimetableModel, TimetableSignal>
    implements TimetableListener, SchoolYearListener, CourseListener, TeacherListener, PreferencesListener {
  static const Logger _logger = Logger('TimetableViewModel');
  static const String _keyViewMode = 'timetable_viewMode';

  final TimetableManager _timetableManager;
  final SchoolYearManager _schoolYearManager;
  final CourseManager _courseManager;
  final TeacherManager _teacherManager;
  final Preferences _preferences;

  bool _progressOverlay = false;

  TimetableViewModel(
    this._timetableManager,
    this._schoolYearManager,
    this._courseManager,
    this._teacherManager,
    this._preferences,
  );

  @override
  void onStart() async {
    super.onStart();
    _timetableManager.addTimetableListener(this, notifyOnAttach: false);
    _schoolYearManager.addSchoolYearListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    _teacherManager.addTeacherListener(this, notifyOnAttach: false);
    _preferences.addPreferencesListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _timetableManager.removeTimetableListener(this);
    _schoolYearManager.removeSchoolYearListener(this);
    _courseManager.removeCourseListener(this);
    _teacherManager.removeTeacherListener(this);
    _preferences.removePreferencesListener(this);
    super.onStop();
  }

  @override
  void onTimetablesChanged(TimetableManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onSchoolYearsChanged(SchoolYearManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onTeachersChanged(TeacherManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onPreferencesChanged(Preferences preferences, List<String> changedKeys) async {
    if (changedKeys.contains(_keyViewMode)) {
      await _tryEmitModel();
    }
  }

  void onSelectActiveTimetable(String timetableId) async {
    await _timetableManager.setActiveTimetable(timetableId);
  }

  void onChangeViewMode(TimetableViewMode viewMode) async {
    _timetableViewMode = viewMode;
    await _tryEmitModel();
  }

  void onUnsubscribeFromActiveTimetable() async {
    await _tryWithProgressOverlay(() async {
      await _timetableManager.unsubscribeAndResolveNewActiveTimetable();
      emitSignal(TimetableSignal.notifyUnsubscribedFromTimetable);
    });
  }

  void onUnsubscribe(String timetableId) async {
    await _tryWithProgressOverlay(() async {
      await _timetableManager.unsubscribeFromPublicTimetable(timetableId);
      emitSignal(TimetableSignal.notifyUnsubscribedFromTimetable);
    });
  }

  void onRefresh() async {
    await _tryWithProgressOverlay(() async {
      // refreshing...
    });
  }

  void onCreateTimetable(AddTimetable timetable) async {
    await _tryWithProgressOverlay(() async {
      final String id = await _timetableManager.createTimetable(timetable);
      await _timetableManager.setActiveTimetable(id);
      emitSignal(TimetableSignal.notifyTimetableCreated);
    });
  }

  void onDuplicateTimetable(Timetable timetable, String newName) async {
    await _tryWithProgressOverlay(() async {
      final String id = await _timetableManager.duplicateTimetable(timetable, DateTime.now(), newName: newName);
      await _timetableManager.setActiveTimetable(id);
      emitSignal(TimetableSignal.notifyTimetableDuplicated);
    });
  }

  void onRenameTimetable(Timetable timetable, String newName) async {
    await _tryWithProgressOverlay(() async {
      await _timetableManager.renameTimetable(timetable.id, newName, DateTime.now());
      emitSignal(TimetableSignal.notifyTimetableRenamed);
    });
  }

  void onDeleteTimetable(Timetable timetable) async {
    await _tryWithProgressOverlay(() async {
      await _timetableManager.deleteAndResolveNewActiveTimetable(timetable.id);
      emitSignal(TimetableSignal.notifyTimetableDeleted);
    });
  }

  void onCreateTimetableEvent(AddTimetableEvent event) async {
    try {
      await _timetableManager.createTimetableEvent(event);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<TimetableModel> get _model async {
    try {
      final SchoolYear schoolYear = await _schoolYearManager.getActive();

      final Future<Timetable> activeTimetableFuture = _timetableManager.getActiveTimetable();
      final Future<List<TimetableInfo>> timetablesFuture = _timetableManager.getUserTimetablesInfo();
      final Future<List<Course>> coursesFuture = _courseManager.getAll(schoolYear.id);
      final Future<List<Teacher>> teachersFuture = _teacherManager.getAll(schoolYearId: schoolYear.id);

      final Timetable activeTimetable = await activeTimetableFuture;
      final List<TimetableInfo> timetables = await timetablesFuture;
      final List<Course> courses = await coursesFuture;
      final List<Teacher> teachers = await teachersFuture;

      return TimetableModel(
        progressOverlay: _progressOverlay,
        showEmptyState: timetables.isEmpty,
        showNotFoundState: activeTimetable == null && timetables.isNotEmpty,
        subscribeAvailable: _isSubscribeAvailable,
        schoolYear: schoolYear,
        viewMode: _timetableViewMode,
        timetable: activeTimetable,
        availableTimetables: timetables,
        courses: courses,
        teachers: teachers,
      );
    } on NoInternetException {
      return TimetableModel(showNoInternetState: true);
    }
  }

  TimetableViewMode get _timetableViewMode {
    return TimetableViewModeExt.forTag(_preferences.getString(_keyViewMode)) ?? TimetableViewMode.week;
  }

  set _timetableViewMode(TimetableViewMode mode) {
    _preferences.setString(_keyViewMode, mode?.tag);
  }

  bool get _isSubscribeAvailable {
    return kDebugMode || window.locale.toLanguageTag().toLowerCase().contains(RegExp('pl'));
  }
}
