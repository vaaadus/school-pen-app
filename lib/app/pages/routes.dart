/// The class aggregates all names of routes to avoid typos.
/// R means Route. Used as a shortcut to skip repeating names.
class R {
  // common urls
  static const String privacyPolicyUrl = 'https://schoolpen.app/privacy.html';
  static const String developerPageUrl = 'https://apps.maroon-bells.com';
  static const String appHomePageUrl = 'https://schoolpen.app';
  static const String facebookUsername = 'schoolpenapp';
  static const String supportEmail = 'support@maroon-bells.com';
  static const String amazonAppUrl = 'amzn://apps/android?p=com.shemhazai.school.timetable';

  // account
  static const String account_loginOptions = '/account/loginOptions';
  static const String account_login = '/account/login';
  static const String account_register = '/account/register';
  static const String account_profile = '/account/profile';
  static const String account_profile_avatar = '/account/profile/avatar';
  static const String account_profile_logout = '/account/profile/logout';
  static const String account_profile_deleteAccount = '/account/profile/deleteAccount';
  static const String account_forgotPassword = '/account/forgotPassword';
  static const String account_changePassword = '/account/changePassword';
  static const String account_changePassword_info = '/account/changePassword/info';

  // gateway
  static const String gateway_home = '/gateway/home';
  static const String gateway_timetable = '/gateway/timetable';
  static const String gateway_courses = '/gateway/courses';
  static const String gateway_notes = '/gateway/notes';
  static const String gateway_attendance = '/gateway/attendance';
  static const String gateway_people = '/gateway/people';
  static const String gateway_files = '/gateway/files';
  static const String gateway_holidays = '/gateway/holidays';
  static const String gateway_search = '/gateway/search';
  static const String gateway_settings = '/gateway/settings';

  // home
  static const String home_goToDate = '/home/goToDate';
  static const String home_notifications = '/home/notifications';

  // courses
  static const String courses_add = '/courses/add';
  static const String courses_edit = '/courses/edit';
  static const String courses_gradingSystem = '/courses/gradingSystem';

  // grades
  static const String grades_list = '/grades/list';
  static const String grades_add = '/grades/add';
  static const String grades_edit = '/grades/edit';
  static const String grades_sort = '/grades/sort';
  static const String grades_categoryAdd = '/grades/categoryAdd';

  // notes
  static const String notes_add = '/notes/add';
  static const String notes_details = '/notes/details';
  static const String notes_filter = '/notes/filter';

  // exams
  static const String exams_add = '/exams/add';
  static const String exams_details = '/exams/details';
  static const String exams_list = '/exams/list';

  // homework
  static const String homework_add = '/homework/add';
  static const String homework_details = '/homework/details';
  static const String homework_list = '/homework/list';

  // reminders
  static const String reminders_add = '/reminders/add';
  static const String reminders_details = '/reminders/details';
  static const String reminders_list = '/reminders/list';

  // timetable
  static const String timetable_viewMode = '/timetable/viewMode';
  static const String timetable_settings = '/timetable/settings';
  static const String timetable_create = '/timetable/create';
  static const String timetable_duplicate = '/timetable/duplicate';
  static const String timetable_rename = '/timetable/rename';
  static const String timetable_delete = '/timetable/delete';
  static const String timetable_goToDate = '/timetable/goToDate';
  static const String timetable_eventAdd = '/timetable/eventAdd';
  static const String timetable_eventEdit = '/timetable/eventEdit';
  static const String timetable_eventDuplicate = '/timetable/eventDuplicate';
  static const String timetable_eventSave = '/timetable/eventSave';
  static const String timetable_subscribe_selectSchool = '/timetable/subscribe/selectSchool';
  static const String timetable_subscribe_selectTimetable = '/timetable/subscribe/selectTimetable';

  // people
  static const String people_teacherAdd = '/people/teacherAdd';
  static const String people_teacherDetails = '/people/teacherDetails';
  static const String people_teacher_officeHours = '/people/teacher/officeHours';

  // settings
  static const String settings_schoolYears = '/settings/schoolYears';
  static const String settings_schoolYears_add = '/settings/schoolYears/add';
  static const String settings_schoolYears_edit = '/settings/schoolYears/edit';
  static const String settings_semesters = '/settings/semesters';
  static const String settings_notifications = '/settings/notifications';
  static const String settings_semesters_add = '/settings/semesters/add';
  static const String settings_semesters_edit = '/settings/semesters/edit';
  static const String settings_themes = '/settings/themes';
  static const String settings_themes_adFailed = '/settings/themes/adFailed';
  static const String settings_privacy = '/settings/privacy';
  static const String settings_support = '/settings/support';
  static const String settings_about = '/settings/about';
  static const String settings_credits = '/settings/credits';
  static const String settings_shareApp = '/settings/shareApp';

  // common
  static const String event_repeatOptions = '/event/repeatOptions';
}
