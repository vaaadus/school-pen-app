import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/teacher_listener.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';

class TeacherModel extends Equatable {
  final bool showEmptyState;
  final List<TeacherData> teachers;

  const TeacherModel({@required this.showEmptyState, @required this.teachers});

  @override
  List<Object> get props => [showEmptyState, teachers];
}

class TeacherData extends Equatable {
  final Teacher teacher;
  final List<Course> courses;

  const TeacherData(this.teacher, this.courses);

  @override
  List<Object> get props => [teacher, courses];
}

class TeacherViewModel extends ViewModel<TeacherModel, Object>
    implements SchoolYearListener, TeacherListener, CourseListener {
  static const Logger _logger = Logger('TeacherViewModel');
  final SchoolYearManager _schoolYearManager;
  final TeacherManager _teacherManager;
  final CourseManager _courseManager;

  TeacherViewModel(this._schoolYearManager, this._teacherManager, this._courseManager);

  @override
  void onStart() async {
    super.onStart();
    _schoolYearManager.addSchoolYearListener(this, notifyOnAttach: false);
    _teacherManager.addTeacherListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _schoolYearManager.removeSchoolYearListener(this);
    _teacherManager.removeTeacherListener(this);
    _courseManager.removeCourseListener(this);
    super.onStop();
  }

  @override
  void onSchoolYearsChanged(SchoolYearManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onTeachersChanged(TeacherManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<TeacherModel> get _model async {
    final List<TeacherData> teachers = await _fetchAllTeacherData();
    return TeacherModel(
      showEmptyState: teachers.isEmpty,
      teachers: teachers,
    );
  }

  Future<List<TeacherData>> _fetchAllTeacherData() async {
    final SchoolYear year = await _schoolYearManager.getActive();
    final List<Teacher> teachers = await _teacherManager.getAll(schoolYearId: year.id);

    final List<Future<TeacherData>> futures = [];
    for (Teacher teacher in teachers) {
      futures.add(_fetchTeacherData(teacher));
    }

    return Future.wait(futures);
  }

  Future<TeacherData> _fetchTeacherData(Teacher teacher) async {
    return TeacherData(teacher, await _courseManager.getAllByTeacherId(teacher.id));
  }
}
