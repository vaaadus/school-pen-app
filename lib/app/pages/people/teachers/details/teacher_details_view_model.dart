import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/common/viewmodel/view_model.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/common/types.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/teacher/model/add_teacher.dart';
import 'package:school_pen/domain/teacher/model/office_hours.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/teacher_listener.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';

class TeacherDetailsModel extends Equatable {
  final bool isEditing;
  final bool discardChangesPopup;
  final bool progressOverlay;
  final AddTeacher teacher;
  final List<Course> courses;
  final Exception nameError;

  const TeacherDetailsModel({
    @required this.isEditing,
    @required this.discardChangesPopup,
    @required this.progressOverlay,
    @required this.teacher,
    @required this.courses,
    this.nameError,
  });

  @override
  List<Object> get props => [isEditing, discardChangesPopup, progressOverlay, teacher, courses, nameError];
}

class TeacherDetailsSignal extends Equatable {
  static const notifyTeacherDeleted = TeacherDetailsSignal._('deleted');
  static const notifyTeacherSaved = TeacherDetailsSignal._('saved');

  final String tag;

  const TeacherDetailsSignal._(this.tag);

  @override
  List<Object> get props => [tag];
}

class PopTeacherDetailsSignal extends TeacherDetailsSignal {
  final String teacherId;

  const PopTeacherDetailsSignal._([this.teacherId]) : super._('pop');

  @override
  List<Object> get props => [...super.props, teacherId];
}

class TeacherDetailsViewModel extends ViewModel<TeacherDetailsModel, TeacherDetailsSignal>
    implements TeacherListener, CourseListener {
  static const Logger _logger = Logger('TeacherDetailsViewModel');

  final SchoolYearManager _schoolYearManager;
  final TeacherManager _teacherManager;
  final CourseManager _courseManager;
  final String _teacherId;

  bool _editModeActive = false;
  bool _progressOverlay = false;
  AddTeacher _originalTeacher;
  AddTeacher _editedTeacher;
  List<Course> _courses;
  Exception _nameError;

  TeacherDetailsViewModel(this._schoolYearManager, this._teacherManager, this._courseManager, this._teacherId) {
    _editModeActive = _teacherId == null;
  }

  @override
  void onStart() async {
    super.onStart();
    _teacherManager.addTeacherListener(this, notifyOnAttach: false);
    _courseManager.addCourseListener(this, notifyOnAttach: false);
    await _tryEmitModel();
  }

  @override
  void onStop() {
    _teacherManager.removeTeacherListener(this);
    _courseManager.removeCourseListener(this);
    super.onStop();
  }

  @override
  void onTeachersChanged(TeacherManager manager) async {
    await _tryEmitModel();
  }

  @override
  void onCoursesChanged(CourseManager manager) async {
    _courses = null;
    await _tryEmitModel();
  }

  void onEnableEditMode() async {
    _editModeActive = true;
    await _tryEmitModel();
  }

  void onSaveTeacher() async {
    final AddTeacher teacher = _editedTeacher;
    if (!await _validate(teacher)) return;

    await _tryWithProgressOverlay(() async {
      String teacherIdResult;
      if (_teacherId != null) {
        await _teacherManager.update(_teacherId, teacher);
        teacherIdResult = _teacherId;
      } else {
        teacherIdResult = await _teacherManager.create(teacher);
      }
      emitSignal(TeacherDetailsSignal.notifyTeacherSaved);
      emitSingleSignal(PopTeacherDetailsSignal._(teacherIdResult));
    });
  }

  Future<void> onEditName(String text) async {
    _editedTeacher = _editedTeacher.copyWith(name: Optional(text));

    await _tryEmitModel();
  }

  void onEditPhone(String text) async {
    _editedTeacher = _editedTeacher.copyWith(phone: Optional(text));

    await _tryEmitModel();
  }

  void onEditEmail(String text) async {
    _editedTeacher = _editedTeacher.copyWith(email: Optional(text));

    await _tryEmitModel();
  }

  void onEditWebsite(String text) async {
    _editedTeacher = _editedTeacher.copyWith(website: Optional(text));

    await _tryEmitModel();
  }

  void onEditAddress(String text) async {
    _editedTeacher = _editedTeacher.copyWith(address: Optional(text));

    await _tryEmitModel();
  }

  void onAddOfficeHours(Weekday weekday, TimeRange range) async {
    _editedTeacher = _editedTeacher.copyWith(
      officeHours: Optional(
        _editedTeacher.officeHours.plus(weekday, range),
      ),
    );

    await _tryEmitModel();
  }

  void onDeleteOfficeHours(Weekday weekday, TimeRange range) async {
    _editedTeacher = _editedTeacher.copyWith(
      officeHours: Optional(
        _editedTeacher.officeHours.minus(weekday, range),
      ),
    );

    await _tryEmitModel();
  }

  void onDeleteTeacher() {
    _tryWithProgressOverlay(() async {
      await _teacherManager.delete(_teacherId);
      emitSignal(TeacherDetailsSignal.notifyTeacherDeleted);
      emitSingleSignal(PopTeacherDetailsSignal._());
    });
  }

  Future<bool> _validate(AddTeacher teacher) async {
    _nameError = null;
    await _tryEmitModel();

    if (Strings.isBlank(teacher.name)) {
      _nameError = FieldMustNotBeEmptyException();
      await _tryEmitModel();
      return false;
    } else {
      return true;
    }
  }

  Future<void> _tryWithProgressOverlay(VoidFutureBuilder builder) async {
    _progressOverlay = true;
    await _tryEmitModel();

    try {
      await builder();
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }

    _progressOverlay = false;
    await _tryEmitModel();
  }

  Future<void> _tryEmitModel() async {
    try {
      emitModel(await _model);
    } catch (error, stackTrace) {
      _logger.logError(error, stackTrace);
    }
  }

  Future<TeacherDetailsModel> get _model async {
    final AddTeacher teacher = await _obtainTeacher();
    if (teacher == null) return null;

    if (_courses == null && _teacherId != null) {
      _courses = await _courseManager.getAllByTeacherId(_teacherId);
    }

    return TeacherDetailsModel(
      isEditing: _editModeActive,
      discardChangesPopup: _originalTeacher == null || teacher != _originalTeacher,
      progressOverlay: _progressOverlay,
      teacher: teacher,
      courses: _courses ?? [],
      nameError: _nameError,
    );
  }

  Future<AddTeacher> _obtainTeacher() async {
    if (_editedTeacher != null) return _editedTeacher;

    if (_teacherId != null) {
      final Teacher teacher = await _teacherManager.getById(_teacherId);
      _originalTeacher = teacher?.toAddTeacher()?.copyWith(updatedAt: Optional(DateTime.now()));
      _editedTeacher = _originalTeacher;
    } else {
      final SchoolYear year = await _schoolYearManager.getActive();
      _editedTeacher = AddTeacher(
        schoolYearId: year.id,
        name: '',
        phone: '',
        email: '',
        website: '',
        address: '',
        officeHours: OfficeHours.empty(),
        updatedAt: DateTime.now(),
      );
    }

    return _editedTeacher;
  }
}
