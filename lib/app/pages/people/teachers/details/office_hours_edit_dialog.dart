import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/pages/picker/datetime_picker_dialog.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/sized_dialog.dart';
import 'package:school_pen/app/widgets/text_field_container.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/teacher/model/office_hours.dart';
import 'package:school_pen/generated/l10n.dart';

/// A dialog to edit [OfficeHours].
class OfficeHoursEditDialog extends StatefulWidget {
  const OfficeHoursEditDialog({
    Key key,
    @required this.onSave,
  }) : super(key: key);

  final Function(Weekday weekday, TimeRange timeRange) onSave;

  @override
  _OfficeHoursEditDialogState createState() => _OfficeHoursEditDialogState();
}

class _OfficeHoursEditDialogState extends State<OfficeHoursEditDialog> {
  final TextEditingController _weekdayController = TextEditingController();
  final TextEditingController _startTimeController = TextEditingController();
  final TextEditingController _endTimeController = TextEditingController();

  Weekday _weekday = Weekday.monday;
  Time _startTime;
  Time _endTime;
  String _startTimeError;
  String _endTimeError;

  @override
  void dispose() {
    _weekdayController.dispose();
    _startTimeController.dispose();
    _endTimeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _weekdayController.text = ResourceLocator.weekdayToString(context, _weekday);
    _startTimeController.text = _startTime?.toTimeOfDay()?.format(context) ?? '';
    _endTimeController.text = _endTime?.toTimeOfDay()?.format(context) ?? '';

    return SizedDialog(
      padding: EdgeInsets.all(Dimens.grid_2x),
      child: Column(children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            S.of(context).common_officeHours,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
          child: Divider(),
        ),
        Semantics(
          label: S.of(context).common_weekday + ': ' + ResourceLocator.weekdayToString(context, _weekday),
          child: PopupMenuButton(
            itemBuilder: (BuildContext context) => [
              for (Weekday weekday in Weekday.values)
                PopupMenuItem(
                  value: weekday,
                  child: Text(ResourceLocator.weekdayToString(context, weekday)),
                ),
            ],
            onSelected: (Weekday weekday) => setState(() => _weekday = weekday),
            child: TextFieldContainer(
              maxHeight: TextFieldContainer.miniHeight,
              showDropdown: true,
              child: TextField(
                controller: _weekdayController,
                style: Theme.of(context).textTheme.headline6,
                enabled: false,
                decoration: InputDecoration(labelText: S.of(context).common_weekday),
              ),
            ),
          ),
        ),
        Semantics(
          label: S.of(context).common_begin + ': ' + _startTimeController.text,
          child: Padding(
            padding: EdgeInsets.only(top: Dimens.grid_2x),
            child: GestureDetector(
              onTap: () => _onChangeStartTime(context),
              child: TextFieldContainer(
                maxHeight: TextFieldContainer.miniHeight,
                child: TextField(
                  controller: _startTimeController,
                  style: Theme.of(context).textTheme.headline6,
                  enabled: false,
                  decoration: InputDecoration(labelText: S.of(context).common_begin),
                ),
                error: TextFieldError.forText(_startTimeError),
              ),
            ),
          ),
        ),
        Semantics(
          label: S.of(context).common_end + ': ' + _endTimeController.text,
          child: Padding(
            padding: EdgeInsets.only(top: Dimens.grid_2x),
            child: GestureDetector(
              onTap: () => _onChangeEndTime(context),
              child: TextFieldContainer(
                maxHeight: TextFieldContainer.miniHeight,
                child: TextField(
                  controller: _endTimeController,
                  style: Theme.of(context).textTheme.headline6,
                  enabled: false,
                  decoration: InputDecoration(labelText: S.of(context).common_end),
                ),
                error: TextFieldError.forText(_endTimeError),
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: Dimens.grid_2x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SmallMaterialButton(
                child: SmallButtonText(
                  S.of(context).common_cancel,
                  color: Theme.of(context).colorScheme.primaryVariant,
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              SmallRaisedButton(
                child: SmallButtonText(S.of(context).common_create),
                onPressed: () => _onSave(context),
              ),
            ],
          ),
        ),
      ]),
    );
  }

  void _onChangeStartTime(BuildContext context) async {
    final Time time = await showAdaptiveTimePicker(
      context: context,
      initialTime: _startTime ?? Time(hour: 16, minute: 0),
    );

    if (time != null) {
      setState(() => _startTime = time);
    }
  }

  void _onChangeEndTime(BuildContext context) async {
    final Time time = await showAdaptiveTimePicker(
      context: context,
      initialTime: _endTime ?? Time(hour: 18, minute: 0),
    );

    if (time != null) {
      setState(() => _endTime = time);
    }
  }

  void _onSave(BuildContext context) {
    _startTimeError = null;
    _endTimeError = null;

    final Weekday weekday = _weekday;
    final Time startTime = _startTime;
    final Time endTime = _endTime;

    if (_validate(startTime, endTime)) {
      Navigator.of(context).pop();
      widget.onSave(weekday, TimeRange(start: startTime, end: endTime));
    }
  }

  bool _validate(Time startTime, Time endTime) {
    if (startTime == null) {
      setState(() => _startTimeError = S.of(context).error_mustNotBeEmpty);
      return false;
    }
    if (endTime == null) {
      setState(() => _endTimeError = S.of(context).error_mustNotBeEmpty);
      return false;
    }
    return true;
  }
}
