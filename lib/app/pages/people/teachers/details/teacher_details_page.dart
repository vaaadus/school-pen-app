import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/assets/resource_locator.dart';
import 'package:school_pen/app/common/extensions.dart';
import 'package:school_pen/app/common/intents.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/people/teachers/details/teacher_details_view_model.dart';
import 'package:school_pen/app/pages/picker/alert_dialog.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/app/widgets/image_button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/nav_button.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/save_or_discard_footer.dart';
import 'package:school_pen/app/widgets/system_ui_overlay_annotation.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/app/widgets/toolbar.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/extensions/strings_ext.dart';
import 'package:school_pen/domain/teacher/model/office_hours.dart';
import 'package:school_pen/generated/l10n.dart';

import 'office_hours_edit_dialog.dart';

/// A callback invoked upon the request to close/pop the details page.
/// The [teacherId] is the id of created or updated teacher or null.
typedef OnTeacherDetailsBackCallback = void Function(BuildContext context, String teacherId);

/// Offers the user a possibility to edit a teacher (if [teacherId] not null)
/// or create a new one if teacher is null.
class TeacherDetailsPage extends LifecycleWidget {
  final String teacherId;
  final OnTeacherDetailsBackCallback onBack;
  final bool fullscreen;

  const TeacherDetailsPage({
    Key key,
    this.teacherId,
    this.onBack = Navigator.pop,
    this.fullscreen = true,
  }) : super(key: key);

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _TeacherDetailsPageState();
}

class _TeacherDetailsPageState extends LifecycleWidgetState<TeacherDetailsPage>
    with
        ViewModelLifecycle<TeacherDetailsViewModel, TeacherDetailsPage>,
        ProgressOverlayMixin,
        SingleTickerProviderStateMixin {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _websiteController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _websiteFocus = FocusNode();
  final FocusNode _addressFocus = FocusNode();

  @override
  TeacherDetailsViewModel provideViewModel() {
    return TeacherDetailsViewModel(Injection.get(), Injection.get(), Injection.get(), widget.teacherId);
  }

  @override
  void dispose() {
    _nameController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    _websiteController.dispose();
    _addressController.dispose();
    _nameFocus.dispose();
    _phoneFocus.dispose();
    _emailFocus.dispose();
    _websiteFocus.dispose();
    _addressFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder(
      viewModel: viewModel,
      builder: (BuildContext context, TeacherDetailsModel model) {
        updateProgressOverlay(model.progressOverlay);
        return _buildScaffold(context, model);
      },
      onSignal: (BuildContext context, TeacherDetailsSignal signal) {
        if (signal == TeacherDetailsSignal.notifyTeacherDeleted) {
          showSuccessToast(message: S.of(context).confirmation_deleted);
        } else if (signal == TeacherDetailsSignal.notifyTeacherSaved) {
          showSuccessToast(message: S.of(context).confirmation_saveSuccess);
        } else if (signal is PopTeacherDetailsSignal) {
          widget.onBack(context, signal.teacherId);
        }
      },
    );
  }

  Widget _buildScaffold(BuildContext context, TeacherDetailsModel model) {
    Widget result = Scaffold(
      body: SafeArea(
        child: _buildContent(context, model),
      ),
    );

    if (widget.fullscreen) {
      final ColorScheme colors = Theme.of(context).colorScheme;
      result = SystemUiOverlayAnnotation(
        child: result,
        systemNavigationBarColor: model.isEditing ? colors.surface : colors.background,
      );
    }

    return result;
  }

  Widget _buildContent(BuildContext context, TeacherDetailsModel model) {
    Widget result = CustomScrollView(slivers: [
      Toolbar(
        vsync: this,
        onBack: (BuildContext context) => _onWillPop(context, model),
        navType: NavButtonType.close,
        horizontalPadding: Dimens.grid_3x,
        actions: [
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => viewModel.onEnableEditMode(),
              imageAsset: Images.ic_edit_pen_24,
              tooltip: S.of(context).common_edit,
            ),
          if (!model.isEditing)
            BorderedImageButton(
              onTap: () => _showDeleteConfirmation(context),
              imageAsset: Images.ic_trash_24,
              tooltip: S.of(context).common_delete,
            ),
        ],
      ),
      SliverList(
        delegate: SliverChildListDelegate([
          Container(height: Dimens.grid_3x - Toolbar.bottomPadding),
          if (Strings.isNotBlank(model.teacher.name) || model.isEditing) ...[
            _buildName(context, model),
            Divider(),
          ],
          if (model.courses.isNotEmpty && !model.isEditing) ...[
            _buildCourses(context, model),
            Divider(),
          ],
          if (Strings.isNotBlank(model.teacher.phone) || model.isEditing) ...[
            _buildPhone(context, model),
            Divider(),
          ],
          if (Strings.isNotBlank(model.teacher.email) || model.isEditing) ...[
            _buildEmail(context, model),
            Divider(),
          ],
          if (Strings.isNotBlank(model.teacher.website) || model.isEditing) ...[
            _buildWebsite(context, model),
            Divider(),
          ],
          if (Strings.isNotBlank(model.teacher.address) || model.isEditing) ...[
            _buildAddress(context, model),
            Divider(),
          ],
          if (model.teacher.officeHours.isNotEmpty || model.isEditing) ...[
            _buildOfficeHours(context, model),
            Divider(),
          ],
          Container(height: Paddings.of(context).vertical),
        ]),
      ),
    ]);

    if (model.isEditing) {
      result = Column(children: [
        Expanded(child: result),
        SaveOrDiscardFooter(
          label: S.of(context).common_teacher,
          onSave: () => viewModel.onSaveTeacher(),
          onDiscard: () => widget.onBack(context, null),
        ),
      ]);
    }

    result = WillPopScope(
      onWillPop: () => _onWillPop(context, model),
      child: result,
    );

    return result;
  }

  Widget _buildName(BuildContext context, TeacherDetailsModel model) {
    _nameController.setTextIfChanged(model.teacher.name);

    Widget result = Padding(
      padding: EdgeInsets.only(
        bottom: Dimens.grid_1x,
        left: Dimens.grid_7_5x,
        right: Dimens.grid_3x,
      ),
      child: TextField(
        controller: _nameController,
        focusNode: _nameFocus,
        enabled: model.isEditing,
        autofocus: model.isEditing,
        maxLines: null,
        style: Theme.of(context).textTheme.headline5,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: S.of(context).common_name,
          errorText: ResourceLocator.errorToText(context, model.nameError),
        ),
        textInputAction: TextInputAction.next,
        textCapitalization: TextCapitalization.sentences,
        onChanged: (String text) => viewModel.onEditName(text),
        onSubmitted: (String text) => _phoneFocus.requestFocus(),
      ),
    );

    if (!model.isEditing) {
      result = Semantics(
        label: S.of(context).common_name + ': ' + model.teacher.name,
        child: result,
        container: true,
        excludeSemantics: true,
      );
    }

    return result;
  }

  Widget _buildCourses(BuildContext context, TeacherDetailsModel model) {
    final String text =
        model.courses.isEmpty ? S.of(context).common_noCourses : model.courses.map((e) => e.name).join(' · ');

    return Semantics(
      label: S.of(context).common_courses + ': ' + text,
      container: true,
      excludeSemantics: true,
      child: Row(children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            Images.ic_book_24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_3x),
            child: Text(text, style: Theme.of(context).textTheme.bodyText1),
          ),
        ),
      ]),
    );
  }

  Widget _buildPhone(BuildContext context, TeacherDetailsModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_phone_24,
      value: model.teacher.phone,
      placeholder: S.of(context).common_phone,
      controller: _phoneController,
      focus: _phoneFocus,
      model: model,
      keyboardType: TextInputType.phone,
      onChanged: (String text) => viewModel.onEditPhone(text),
      onSubmitted: (String text) => _emailFocus.requestFocus(),
      onTapWhenNotEditing: () => Intents.callNumber(model.teacher.phone),
    );
  }

  Widget _buildEmail(BuildContext context, TeacherDetailsModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_mail_outline_24,
      value: model.teacher.email,
      placeholder: S.of(context).common_email,
      controller: _emailController,
      focus: _emailFocus,
      model: model,
      keyboardType: TextInputType.emailAddress,
      onChanged: (String text) => viewModel.onEditEmail(text),
      onSubmitted: (String text) => _websiteFocus.requestFocus(),
      onTapWhenNotEditing: () => Intents.sendEmail(email: model.teacher.email),
    );
  }

  Widget _buildWebsite(BuildContext context, TeacherDetailsModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_website_24,
      value: model.teacher.website,
      placeholder: S.of(context).common_website,
      controller: _websiteController,
      focus: _websiteFocus,
      model: model,
      keyboardType: TextInputType.url,
      onChanged: (String text) => viewModel.onEditWebsite(text),
      onSubmitted: (String text) => _addressFocus.requestFocus(),
      onTapWhenNotEditing: () => Intents.openBrowser(model.teacher.website),
    );
  }

  Widget _buildAddress(BuildContext context, TeacherDetailsModel model) {
    return _buildTextFieldWithIcon(
      context: context,
      imageAsset24: Images.ic_location_24,
      value: model.teacher.address,
      placeholder: S.of(context).common_address,
      controller: _addressController,
      focus: _addressFocus,
      model: model,
      keyboardType: TextInputType.streetAddress,
      onChanged: (String text) => viewModel.onEditAddress(text),
      onSubmitted: (String text) => _unfocusTextFields(),
      onTapWhenNotEditing: () => Intents.openMap(model.teacher.address),
    );
  }

  Widget _buildOfficeHours(BuildContext context, TeacherDetailsModel model) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            Images.ic_work_hours_24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(
              left: Dimens.grid_2x,
              top: model.teacher.officeHours.isNotEmpty ? Dimens.grid_1x : 0,
              bottom: model.isEditing ? 0 : Dimens.grid_1x,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Semantics(
                  label: S.of(context).common_officeHours,
                  container: true,
                  child: Column(children: [
                    for (OfficeHoursTime officeHour in model.teacher.officeHours.sortedTimes())
                      _buildOfficeHoursTimeRow(context, model, officeHour),
                  ]),
                ),
                if (model.isEditing) _buildAddOfficeHoursRow(context),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildOfficeHoursTimeRow(BuildContext context, TeacherDetailsModel model, OfficeHoursTime officeHour) {
    return Row(children: [
      Expanded(
        child: Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_1x,
            bottom: Dimens.grid_1x,
            left: Dimens.grid_1x,
          ),
          child: Text(
            ResourceLocator.weekdayToString(context, officeHour.weekday) + ' · ' + officeHour.timeRange.toString(),
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ),
      if (model.isEditing)
        MergeSemantics(
          child: InkWell(
            onTap: () {
              _unfocusTextFields();
              viewModel.onDeleteOfficeHours(officeHour.weekday, officeHour.timeRange);
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: Dimens.grid_1x, horizontal: Dimens.grid_3x),
              child: Image.asset(
                Images.ic_close_24,
                width: 16,
                height: 16,
                color: Theme.of(context).colorScheme.primaryVariant,
                semanticLabel: S.of(context).common_delete,
              ),
            ),
          ),
        )
    ]);
  }

  Widget _buildAddOfficeHoursRow(BuildContext context) {
    return MergeSemantics(
      child: InkWell(
        onTap: () => _showOfficeHoursDialog(context),
        child: Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_1x,
          ),
          child: Text(
            S.of(context).common_addOfficeHours,
            style: Theme.of(context).textTheme.bodyText1.copyWith(
                  color: Theme.of(context).colorScheme.primaryVariant,
                ),
          ),
        ),
      ),
    );
  }

  void _showOfficeHoursDialog(BuildContext context) async {
    _unfocusTextFields();

    await showDialog(
      context: context,
      routeSettings: RouteSettings(name: R.people_teacher_officeHours),
      builder: (BuildContext context) => OfficeHoursEditDialog(
        onSave: (Weekday weekday, TimeRange range) => viewModel.onAddOfficeHours(weekday, range),
      ),
    );
  }

  Widget _buildTextFieldWithIcon({
    @required BuildContext context,
    @required String imageAsset24,
    @required String value,
    @required String placeholder,
    @required TextEditingController controller,
    @required FocusNode focus,
    @required TeacherDetailsModel model,
    TextInputType keyboardType = TextInputType.text,
    ValueChanged<String> onChanged,
    ValueChanged<String> onSubmitted,
    GestureTapCallback onTapWhenNotEditing,
  }) {
    controller.setTextIfChanged(value);

    Widget result = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: Dimens.grid_2x,
            bottom: Dimens.grid_2x,
            left: Dimens.grid_3x,
          ),
          child: Image.asset(
            imageAsset24,
            width: 24,
            height: 24,
            color: Theme.of(context).colorScheme.primaryVariant,
            excludeFromSemantics: true,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Dimens.grid_1_5x, vertical: Dimens.grid_0_5x),
            child: TextField(
              controller: controller,
              focusNode: focus,
              enabled: model.isEditing,
              style: Theme.of(context).textTheme.bodyText1,
              textInputAction: TextInputAction.next,
              maxLines: null,
              keyboardType: keyboardType,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: placeholder,
              ),
              onChanged: onChanged,
              onSubmitted: onSubmitted,
            ),
          ),
        ),
      ],
    );

    if (!model.isEditing && onTapWhenNotEditing != null) {
      result = InkWell(onTap: onTapWhenNotEditing, child: result);
    }

    if (!model.isEditing) {
      result = Semantics(label: '$placeholder: $value', child: result, excludeSemantics: true);
    }

    return result;
  }

  Future<bool> _onWillPop(BuildContext context, TeacherDetailsModel model) async {
    _unfocusTextFields();

    if (model.isEditing && model.discardChangesPopup) {
      _showSaveChangesDialog(context);
    } else {
      widget.onBack(context, null);
    }

    return false;
  }

  void _showDeleteConfirmation(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_delete,
      message: S.of(context).teachers_deleteDialog_message,
      primaryAction: DialogActionData(
        text: S.of(context).common_delete,
        isDestructive: true,
        onTap: () => viewModel.onDeleteTeacher(),
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_cancel,
      ),
    );
  }

  void _showSaveChangesDialog(BuildContext context) {
    showAlertDialog(
      context: context,
      title: S.of(context).common_discardChangesQuestion,
      primaryAction: DialogActionData(
        text: S.of(context).common_keepEditing,
      ),
      secondaryAction: DialogActionData(
        text: S.of(context).common_discard,
        onTap: () => widget.onBack(context, null),
      ),
    );
  }

  void _unfocusTextFields() {
    _nameFocus.unfocus();
    _phoneFocus.unfocus();
    _emailFocus.unfocus();
    _websiteFocus.unfocus();
    _addressFocus.unfocus();
  }
}
