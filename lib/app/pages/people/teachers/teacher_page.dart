import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/viewmodel/view_model_builder.dart';
import 'package:school_pen/app/common/viewmodel/view_model_lifecycle.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/pages/people/teachers/details/teacher_details_page.dart';
import 'package:school_pen/app/pages/people/teachers/teacher_view_model.dart';
import 'package:school_pen/app/pages/people/teachers_or_friends_switch.dart';
import 'package:school_pen/app/widgets/adaptive.dart';
import 'package:school_pen/app/widgets/bordered_card_item.dart';
import 'package:school_pen/app/widgets/bordered_list_item.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/app/widgets/lifecycle_widget.dart';
import 'package:school_pen/app/widgets/master_detail_container.dart';
import 'package:school_pen/app/widgets/page_placeholder.dart';
import 'package:school_pen/generated/l10n.dart';

/// Allows to view/edit/create teacher.
class TeacherPage extends LifecycleWidget {
  const TeacherPage({Key key, @required this.onSelectFriends}) : super(key: key);

  final GestureTapCallback onSelectFriends;

  @override
  LifecycleWidgetState<LifecycleWidget> createState() => _TeacherPageState();
}

class _TeacherPageState extends LifecycleWidgetState<TeacherPage>
    with ViewModelLifecycle<TeacherViewModel, TeacherPage> {
  Widget _asideWidget;

  @override
  TeacherViewModel provideViewModel() => TeacherViewModel(Injection.get(), Injection.get(), Injection.get());

  @override
  Widget build(BuildContext context) {
    final Widget scaffold = Scaffold(
      body: SafeArea(
        child: ViewModelBuilder(
          viewModel: viewModel,
          builder: (BuildContext context, TeacherModel model) {
            if (model.showEmptyState) {
              return _buildEmptyState(context);
            } else {
              return _buildContent(context, model);
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: null,
        label: ButtonText(S.of(context).common_addTeacher),
        onPressed: () => _showDetails(context),
      ),
    );

    return MasterDetailContainer(
      masterBuilder: (BuildContext context) => scaffold,
      detailBuilder: (BuildContext context) => _asideWidget,
    );
  }

  Widget _buildEmptyState(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      return Column(children: [
        if (kDebugMode)
          Padding(
            padding: Paddings.of(context).edgeInsets.copyWith(top: 0),
            child: _buildTeachersOrFriendsSwitch(context, _shouldShowListViewLayout(constraints)),
          ),
        Expanded(
          child: PagePlaceholder(
            imageAsset: Images.illustration_teacher_128,
            imageWidth: 128,
            imageHeight: 128,
            message: S.of(context).teachers_emptyState_message,
          ),
        ),
      ]);
    });
  }

  Widget _buildContent(BuildContext context, TeacherModel model) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final Paddings paddings = Paddings.of(context);
      final bool listViewLayout = _shouldShowListViewLayout(constraints);
      return ListView(
        padding: EdgeInsets.only(
          left: paddings.horizontal,
          right: paddings.horizontal,
          bottom: Dimens.grid_10x,
        ),
        children: [
          if (kDebugMode) _buildTeachersOrFriendsSwitch(context, listViewLayout),
          if (listViewLayout) _buildListView(context, model.teachers),
          if (!listViewLayout) _buildGridView(context, model.teachers),
        ],
      );
    });
  }

  Widget _buildTeachersOrFriendsSwitch(BuildContext context, bool expandedLayout) {
    final Adaptive adaptive = Adaptive.of(context);
    return Align(
      alignment: expandedLayout ? Alignment.center : Alignment.topLeft,
      child: Padding(
        padding: EdgeInsets.only(top: (adaptive.isMobile || adaptive.isTablet) ? Dimens.grid_2x : Dimens.grid_5x),
        child: TeachersOrFriendsSwitch(
          isTeacherSelected: true,
          expandedLayout: expandedLayout,
          onSelectFriends: widget.onSelectFriends,
        ),
      ),
    );
  }

  Widget _buildGridView(BuildContext context, List<TeacherData> teachers) {
    return Padding(
      padding: EdgeInsets.only(top: _getTeachersTopPadding(context)),
      child: _GridView(
        teachers: teachers,
        onTeacherTap: (TeacherData data) => _showDetails(context, data.teacher.id),
      ),
    );
  }

  Widget _buildListView(BuildContext context, List<TeacherData> teachers) {
    return Padding(
      padding: EdgeInsets.only(top: _getTeachersTopPadding(context)),
      child: _ListView(
        teachers: teachers,
        onTeacherTap: (TeacherData data) => _showDetails(context, data.teacher.id),
      ),
    );
  }

  double _getTeachersTopPadding(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    if (adaptive.isMobile) return 0;
    if (adaptive.isTablet) return Dimens.grid_1x;
    return Dimens.grid_2x;
  }

  void _showDetails(BuildContext context, [String teacherId]) {
    if (MasterDetailContainer.isSupported(context)) {
      final Widget page = TeacherDetailsPage(
        key: ValueKey(teacherId),
        teacherId: teacherId,
        fullscreen: false,
        onBack: (BuildContext context, String teacherId) {
          setState(() => _asideWidget = null);
        },
      );

      setState(() => _asideWidget = page);
    } else if (teacherId != null) {
      Nav.people_teacherDetails(teacherId: teacherId);
    } else {
      Nav.people_teacherAdd();
    }
  }

  bool _shouldShowListViewLayout(BoxConstraints constraints) {
    return constraints.maxWidth < 400;
  }
}

class _GridView extends StatelessWidget {
  const _GridView({Key key, @required this.teachers, @required this.onTeacherTap}) : super(key: key);

  final List<TeacherData> teachers;
  final ValueChanged<TeacherData> onTeacherTap;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final int crossAxisCount = _getCrossAxisCount(constraints.maxWidth);
      return StaggeredGridView.countBuilder(
        key: ValueKey(crossAxisCount),
        primary: false,
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(vertical: Dimens.grid_2x),
        mainAxisSpacing: Dimens.grid_2x,
        crossAxisSpacing: Dimens.grid_2x,
        crossAxisCount: crossAxisCount,
        itemCount: teachers.length,
        itemBuilder: (BuildContext context, int index) => BorderedCardItem(
          data: _teacherAsCardData(teachers[index], context),
          onTap: () => onTeacherTap(teachers[index]),
        ),
        staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
      );
    });
  }

  int _getCrossAxisCount(double availableWidth) {
    return (availableWidth / _getMinColumnWidth(availableWidth)).floor();
  }

  double _getMinColumnWidth(double availableWidth) {
    if (availableWidth < 640) return 160;
    if (availableWidth < 800) return 196;
    return 224;
  }

  BorderedCardItemData _teacherAsCardData(TeacherData data, BuildContext context) {
    return BorderedCardItemData(
      title: data.teacher.name,
      label: _formatLabel(context, data),
    );
  }
}

class _ListView extends StatelessWidget {
  const _ListView({Key key, @required this.teachers, @required this.onTeacherTap}) : super(key: key);

  final List<TeacherData> teachers;
  final ValueChanged<TeacherData> onTeacherTap;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      primary: false,
      shrinkWrap: true,
      itemCount: teachers.length,
      itemBuilder: (BuildContext context, int index) => Padding(
        padding: EdgeInsets.only(top: Dimens.grid_2x),
        child: BorderedListItem(
          data: _teacherAsListData(teachers[index], context),
          onTap: () => onTeacherTap(teachers[index]),
        ),
      ),
    );
  }

  BorderedListItemData _teacherAsListData(TeacherData data, BuildContext context) {
    return BorderedListItemData(
      title: data.teacher.name,
      label: _formatLabel(context, data),
    );
  }
}

String _formatLabel(BuildContext context, TeacherData data) {
  return data.courses.isEmpty ? S.of(context).common_noCourses : data.courses.map((e) => e.name).join(' · ');
}
