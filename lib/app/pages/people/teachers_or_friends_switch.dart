import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/dimens.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/widgets/button.dart';
import 'package:school_pen/generated/l10n.dart';

/// A widget which allows to switch the view between friends & teachers.
class TeachersOrFriendsSwitch extends StatelessWidget {
  final bool isTeacherSelected;
  final bool expandedLayout;
  final GestureTapCallback onSelectTeachers;
  final GestureTapCallback onSelectFriends;

  const TeachersOrFriendsSwitch({
    Key key,
    @required this.isTeacherSelected,
    @required this.expandedLayout,
    this.onSelectTeachers = _doNothing,
    this.onSelectFriends = _doNothing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _buildRow(
          context: context,
          onTap: onSelectTeachers,
          isSelected: isTeacherSelected,
          imageAsset: Images.ic_teacher_blackboard_24,
          text: S.of(context).common_teachers,
          isExpanded: expandedLayout,
          prependSpacer: expandedLayout,
          rightPadding: Dimens.grid_3x,
        ),
        SizedBox(height: 32, child: VerticalDivider()),
        _buildRow(
          context: context,
          onTap: onSelectFriends,
          isSelected: !isTeacherSelected,
          imageAsset: Images.ic_small_talk_24,
          text: S.of(context).common_friends,
          isExpanded: expandedLayout,
          appendSpacer: expandedLayout,
          leftPadding: Dimens.grid_3x,
        ),
      ],
    );
  }

  Widget _buildRow({
    @required BuildContext context,
    @required GestureTapCallback onTap,
    @required bool isSelected,
    @required String imageAsset,
    @required String text,
    @required bool isExpanded,
    double leftPadding = Dimens.grid_1x,
    double rightPadding = Dimens.grid_1x,
    bool prependSpacer = false,
    bool appendSpacer = false,
  }) {
    final ColorScheme colors = Theme.of(context).colorScheme;

    Widget result = InkWell(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.only(
          top: Dimens.grid_1x,
          bottom: Dimens.grid_1x,
          left: leftPadding,
          right: rightPadding,
        ),
        child: Row(children: [
          if (prependSpacer) Spacer(),
          Image.asset(
            imageAsset,
            width: 24,
            height: 24,
            color: isSelected ? colors.primary : colors.primaryVariant,
          ),
          Padding(
            padding: EdgeInsets.only(left: Dimens.grid_1_5x),
            child: ButtonText(
              text,
              color: isSelected ? colors.primary : colors.primaryVariant,
            ),
          ),
          if (appendSpacer) Spacer(),
        ]),
      ),
    );

    if (isExpanded) {
      result = Expanded(child: result);
    }

    return result;
  }
}

void _doNothing() {}
