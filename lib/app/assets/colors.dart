import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/widgets/custom_theme_provider.dart';
import 'package:school_pen/domain/themes/model/custom_theme.dart';

/// App colors palette
class AppColors {
  static const Color black = Color(0xFF000000);
  static const Color gray1 = Color(0xFF303030);
  static const Color gray2 = Color(0xFF444444);
  static const Color gray3 = Color(0xFF606060);
  static const Color gray4 = Color(0xFF808080);
  static const Color gray5 = Color(0xFFA0A0A0);
  static const Color gray6 = Color(0xFFCECECE);
  static const Color gray7 = Color(0xFFE7E7E7);
  static const Color gray8 = Color(0xFFF4F4F4);
  static const Color white = Color(0xFFFFFFFF);

  static const Color red1 = Color(0xFFEB2626);
  static const Color orange1 = Color(0xFFFFA726);
  static const Color orange2 = Color(0xFFE98634);
  static const Color yellow = Color(0xFFEEC23C);
  static const Color green1 = Color(0xFF219653);
  static const Color green2 = Color(0xFF27AE60);
  static const Color green3 = Color(0xFF6FCF97);
  static const Color teal1 = Color(0xFF008080);
  static const Color blue1 = Color(0xFF2E72DF);
  static const Color blue2 = Color(0xFF2D9CDB);
  static const Color blue3 = Color(0xFF56CCF2);
  static const Color purple1 = Color(0xFF9B51E0);
  static const Color purple2 = Color(0xFFBB6BD9);
  static const Color pink1 = Color(0xFFFD8BAF);

  static const Color overlayColor = Color(0xCC000000);
  static const Color barrierColor = Color(0x80000000);
  static const Color transparent = Color(0x00000000);
  static const Color facebookBlue = Color(0xFF4267B2);
  static const Color appleBlack = Color(0xFF000000);

  /// Returns the final color how a [base] color would be displayed.
  /// An in dark mode the colors should be darker.
  static Color applyDarkModeSaturation(BuildContext context, Color base) {
    if (base == null) {
      return Colors.transparent;
    } else if (Theme.of(context).brightness == Brightness.light) {
      return base;
    } else {
      return HSLColor.fromColor(base).withSaturation(0.5).withLightness(0.45).toColor();
    }
  }
}

/// Extensions to standard material design color palette.
extension ColorSchemes on ColorScheme {
  static const CustomColorScheme light = CustomColorScheme(
    brightness: Brightness.light,
    primary: AppColors.gray1,
    primaryVariant: AppColors.gray4,
    onPrimary: AppColors.white,
    secondary: AppColors.blue1,
    secondaryVariant: AppColors.blue2,
    onSecondary: AppColors.white,
    background: AppColors.white,
    dialogBackground: AppColors.white,
    onBackground: AppColors.gray1,
    surface: AppColors.gray8,
    onSurface: AppColors.gray1,
    success: AppColors.green2,
    onSuccess: AppColors.white,
    error: AppColors.red1,
    onError: AppColors.white,
    divider: AppColors.gray7,
    unselectedWidget: Color(0xFFA1A6B3),
  );

  static const CustomColorScheme amber = CustomColorScheme(
    brightness: Brightness.light,
    primary: AppColors.gray1,
    primaryVariant: AppColors.gray4,
    onPrimary: AppColors.white,
    secondary: Color(0xFFA6494D),
    secondaryVariant: Color(0xFFD27254),
    onSecondary: AppColors.white,
    background: AppColors.white,
    dialogBackground: AppColors.white,
    onBackground: AppColors.gray1,
    surface: AppColors.gray8,
    onSurface: AppColors.gray1,
    success: AppColors.green2,
    onSuccess: AppColors.white,
    error: AppColors.red1,
    onError: AppColors.white,
    divider: AppColors.gray7,
    unselectedWidget: Color(0xFFC4B49F),
  );

  static const CustomColorScheme red = CustomColorScheme(
    brightness: Brightness.light,
    primary: AppColors.gray1,
    primaryVariant: AppColors.gray4,
    onPrimary: AppColors.white,
    secondary: AppColors.red1,
    secondaryVariant: Color(0xFFD10F0F),
    onSecondary: AppColors.white,
    background: AppColors.white,
    dialogBackground: AppColors.white,
    onBackground: AppColors.gray1,
    surface: AppColors.gray8,
    onSurface: AppColors.gray1,
    success: AppColors.green2,
    onSuccess: AppColors.white,
    error: Color(0xFF4B3A9F),
    onError: AppColors.white,
    divider: AppColors.gray7,
    unselectedWidget: Color(0xFFCCA5A5),
  );

  static const CustomColorScheme green = CustomColorScheme(
    brightness: Brightness.light,
    primary: AppColors.gray1,
    primaryVariant: AppColors.gray4,
    onPrimary: AppColors.white,
    secondary: AppColors.green2,
    secondaryVariant: AppColors.green1,
    onSecondary: AppColors.white,
    background: AppColors.white,
    dialogBackground: AppColors.white,
    onBackground: AppColors.gray1,
    surface: AppColors.gray8,
    onSurface: AppColors.gray1,
    success: AppColors.orange1,
    onSuccess: AppColors.white,
    error: AppColors.red1,
    onError: AppColors.white,
    divider: AppColors.gray7,
    unselectedWidget: Color(0xFFAFC9BA),
  );

  static const CustomColorScheme dark = CustomColorScheme(
    brightness: Brightness.dark,
    primary: AppColors.white,
    primaryVariant: AppColors.gray5,
    onPrimary: AppColors.gray1,
    secondary: AppColors.orange1,
    secondaryVariant: AppColors.orange2,
    onSecondary: AppColors.white,
    background: AppColors.gray1,
    dialogBackground: AppColors.gray1,
    onBackground: AppColors.white,
    surface: AppColors.gray2,
    onSurface: AppColors.white,
    success: AppColors.green2,
    onSuccess: AppColors.white,
    error: AppColors.red1,
    onError: AppColors.white,
    divider: AppColors.gray3,
    unselectedWidget: AppColors.gray5,
  );

  static const CustomColorScheme black = CustomColorScheme(
    brightness: Brightness.dark,
    primary: AppColors.gray7,
    primaryVariant: AppColors.gray4,
    onPrimary: AppColors.black,
    secondary: AppColors.orange1,
    secondaryVariant: AppColors.orange2,
    onSecondary: AppColors.white,
    background: AppColors.black,
    dialogBackground: Color(0xFF1B1B1B),
    onBackground: AppColors.gray7,
    surface: Color(0xFF262626),
    onSurface: AppColors.gray7,
    success: AppColors.green2,
    onSuccess: AppColors.white,
    error: AppColors.red1,
    onError: AppColors.white,
    divider: Color(0xFF363636),
    unselectedWidget: AppColors.gray4,
  );

  static const CustomColorScheme navyBlue = CustomColorScheme(
    brightness: Brightness.dark,
    primary: AppColors.white,
    primaryVariant: AppColors.gray6,
    onPrimary: Color(0xFF1A2E41),
    secondary: Color(0xFFFFC905),
    secondaryVariant: Color(0xFFDFB431),
    onSecondary: Color(0xFFFFFFFF),
    background: Color(0xFF1A2E41),
    dialogBackground: Color(0xFF253b50),
    onBackground: Color(0xFFFFFFFF),
    surface: Color(0xFF2D4259),
    onSurface: Color(0xFFFFFFFF),
    success: AppColors.green2,
    onSuccess: AppColors.white,
    error: AppColors.red1,
    onError: AppColors.white,
    divider: Color(0xFF4A5E73),
    unselectedWidget: AppColors.gray6,
  );

  Color success(BuildContext context) => _colorSchemeOf(context).success;

  Color onSuccess(BuildContext context) => _colorSchemeOf(context).onSuccess;
}

class CustomColorScheme extends Equatable {
  final Brightness brightness;
  final Color primary;
  final Color primaryVariant;
  final Color onPrimary;
  final Color secondary;
  final Color secondaryVariant;
  final Color onSecondary;
  final Color background;
  final Color dialogBackground;
  final Color onBackground;
  final Color surface;
  final Color onSurface;
  final Color success;
  final Color onSuccess;
  final Color error;
  final Color onError;
  final Color divider;
  final Color unselectedWidget;

  const CustomColorScheme({
    @required this.brightness,
    @required this.primary,
    @required this.primaryVariant,
    @required this.onPrimary,
    @required this.secondary,
    @required this.secondaryVariant,
    @required this.onSecondary,
    @required this.background,
    @required this.dialogBackground,
    @required this.onBackground,
    @required this.surface,
    @required this.onSurface,
    @required this.success,
    @required this.onSuccess,
    @required this.error,
    @required this.onError,
    @required this.divider,
    @required this.unselectedWidget,
  });

  ColorScheme asFramework() => ColorScheme(
        brightness: brightness,
        primary: primary,
        primaryVariant: primaryVariant,
        onPrimary: onPrimary,
        secondary: secondary,
        secondaryVariant: secondaryVariant,
        onSecondary: onSecondary,
        background: background,
        onBackground: onBackground,
        surface: surface,
        onSurface: onSurface,
        error: error,
        onError: onError,
      );

  @override
  List<Object> get props => [
        brightness,
        primary,
        primaryVariant,
        onPrimary,
        secondary,
        secondaryVariant,
        onSecondary,
        background,
        dialogBackground,
        onBackground,
        surface,
        onSurface,
        success,
        onSurface,
        error,
        onError,
        divider,
        unselectedWidget,
      ];
}

CustomColorScheme _colorSchemeOf(BuildContext context) {
  final CustomTheme theme = SchoolPenOptions.of(context).customTheme;
  switch (theme) {
    case CustomTheme.system:
      if (MediaQuery.of(context).platformBrightness == Brightness.light) return ColorSchemes.light;
      return ColorSchemes.dark;
    case CustomTheme.light:
      return ColorSchemes.light;
    case CustomTheme.amber:
      return ColorSchemes.amber;
    case CustomTheme.red:
      return ColorSchemes.red;
    case CustomTheme.green:
      return ColorSchemes.green;
    case CustomTheme.dark:
      return ColorSchemes.dark;
    case CustomTheme.black:
      return ColorSchemes.black;
    case CustomTheme.navyBlue:
      return ColorSchemes.navyBlue;
  }
  throw ArgumentError('Unsupported theme: $theme');
}
