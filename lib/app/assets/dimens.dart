import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:school_pen/app/widgets/adaptive.dart';

/// Commonly used dimensions, sizes
class Dimens {
  static const double grid_0_5x = 4.0;
  static const double grid_1x = 8.0;
  static const double grid_1_5x = 12.0;
  static const double grid_2x = 16.0;
  static const double grid_2_5x = 20.0;
  static const double grid_3x = 24.0;
  static const double grid_3_5x = 28.0;
  static const double grid_4x = 32.0;
  static const double grid_4_5x = 36.0;
  static const double grid_5x = 40.0;
  static const double grid_5_5x = 44.0;
  static const double grid_6x = 48.0;
  static const double grid_6_5x = 52.0;
  static const double grid_7x = 56.0;
  static const double grid_7_5x = 60.0;
  static const double grid_8x = 64.0;
  static const double grid_8_5x = 68.0;
  static const double grid_9x = 72.0;
  static const double grid_9_5x = 76.0;
  static const double grid_10x = 80.0;

  static const double tens_1x = 10.0;
  static const double tens_2x = 20.0;
  static const double tens_3x = 30.0;
  static const double tens_4x = 40.0;
  static const double tens_5x = 50.0;
  static const double tens_6x = 60.0;

  static const double cardElevation = 2.0;
  static const double cardRadius = grid_1x;

  static const double checkboxWidth = 24.0;
  static const double checkboxHeight = 24.0;

  static const double smallButtonHeight = 32.0;
}

/// Defines horizontal & vertical page paddings handling adaptive screen sizes.
class Paddings extends Equatable {
  static Paddings mobilePortrait = Paddings(vertical: Dimens.grid_2x, horizontal: Dimens.grid_2x);
  static Paddings mobileLandscape = Paddings(vertical: Dimens.grid_2x, horizontal: Dimens.grid_4x);
  static Paddings tabletPortrait = Paddings(vertical: Dimens.grid_3x, horizontal: Dimens.grid_3x);
  static Paddings tabletLandscape = Paddings(vertical: Dimens.grid_3x, horizontal: Dimens.grid_4x);
  static Paddings desktop = Paddings(vertical: Dimens.grid_6x, horizontal: Dimens.grid_5x);

  const Paddings({@required this.vertical, @required this.horizontal});

  static Paddings of(BuildContext context) {
    final Adaptive adaptive = Adaptive.of(context);
    final Orientation orientation = MediaQuery.of(context).orientation;

    if (adaptive.isMobile)
      return orientation == Orientation.portrait ? Paddings.mobilePortrait : Paddings.mobileLandscape;
    if (adaptive.isTablet)
      return orientation == Orientation.portrait ? Paddings.tabletPortrait : Paddings.tabletLandscape;
    return Paddings.desktop;
  }

  final double vertical;
  final double horizontal;

  EdgeInsets get edgeInsets => EdgeInsets.symmetric(vertical: vertical, horizontal: horizontal);

  @override
  List<Object> get props => [vertical, horizontal];
}
