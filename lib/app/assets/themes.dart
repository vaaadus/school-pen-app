import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/domain/themes/model/custom_theme.dart';

import 'colors.dart';
import 'dimens.dart';
import 'typography.dart';

/// App themes.
extension Themes on Theme {
  static ThemeData of(BuildContext context, CustomTheme theme) {
    if (MediaQuery.of(context).platformBrightness == Brightness.light) {
      return Themes.lightOf(theme);
    } else {
      return Themes.darkOf(theme);
    }
  }

  static ThemeData lightOf(CustomTheme theme) {
    if (theme == CustomTheme.system) {
      return _light;
    } else {
      return _themeDataOf(theme);
    }
  }

  static ThemeData darkOf(CustomTheme theme) {
    if (theme == CustomTheme.system) {
      return _dark;
    } else {
      return _themeDataOf(theme);
    }
  }

  static ThemeData _themeDataOf(CustomTheme theme) {
    assert(theme != CustomTheme.system);

    switch (theme) {
      case CustomTheme.light:
        return _light;
      case CustomTheme.amber:
        return _amber;
      case CustomTheme.red:
        return _red;
      case CustomTheme.green:
        return _green;
      case CustomTheme.dark:
        return _dark;
      case CustomTheme.black:
        return _black;
      case CustomTheme.navyBlue:
        return _navyBlue;
      case CustomTheme.system:
        // Must be handled before this method, see assert at the top.
        break;
    }
    throw ArgumentError('Unsupported theme: $theme');
  }

  static final ThemeData _light = _theme(ColorSchemes.light);
  static final ThemeData _dark = _theme(ColorSchemes.dark);
  static final ThemeData _black = _theme(ColorSchemes.black);
  static final ThemeData _amber = _theme(ColorSchemes.amber);
  static final ThemeData _navyBlue = _theme(ColorSchemes.navyBlue);
  static final ThemeData _red = _theme(ColorSchemes.red);
  static final ThemeData _green = _theme(ColorSchemes.green);

  static ThemeData _theme(CustomColorScheme colorScheme) {
    final TextTheme textTheme = TextThemes.of(colorScheme);

    return ThemeData(
      brightness: colorScheme.brightness,
      textTheme: textTheme,
      primaryColor: colorScheme.primary,
      primaryColorDark: colorScheme.primary,
      primaryColorLight: colorScheme.primary,
      accentColor: colorScheme.secondary,
      toggleableActiveColor: colorScheme.secondaryVariant,
      unselectedWidgetColor: colorScheme.unselectedWidget,
      scaffoldBackgroundColor: colorScheme.background,
      canvasColor: colorScheme.background,
      dividerColor: colorScheme.divider,
      hintColor: colorScheme.primaryVariant,
      colorScheme: colorScheme.asFramework(),
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: colorScheme.secondaryVariant,
        selectionColor: colorScheme.secondaryVariant,
        selectionHandleColor: colorScheme.secondaryVariant,
      ),
      appBarTheme: AppBarTheme(
        brightness: colorScheme.brightness,
        color: colorScheme.background,
        textTheme: textTheme,
        iconTheme: IconThemeData(color: colorScheme.primaryVariant),
      ),
      cardTheme: CardTheme(
        color: colorScheme.surface,
        clipBehavior: Clip.antiAlias,
        elevation: Dimens.cardElevation,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(Dimens.cardRadius)),
      ),
      dialogTheme: DialogTheme(
        backgroundColor: colorScheme.dialogBackground,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(Dimens.grid_0_5x)),
      ),
      bottomSheetTheme: BottomSheetThemeData(
        backgroundColor: colorScheme.surface,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(Dimens.grid_2x)),
        ),
      ),
      popupMenuTheme: PopupMenuThemeData(
        color: colorScheme.surface,
        textStyle: textTheme.bodyText1,
      ),
      inputDecorationTheme: InputDecorationTheme(
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        focusedErrorBorder: InputBorder.none,
        contentPadding: EdgeInsets.symmetric(
          vertical: Dimens.grid_1x,
          horizontal: Dimens.grid_1_5x,
        ),
        focusColor: colorScheme.secondary,
        labelStyle: textTheme.headline6.copyWith(color: colorScheme.primaryVariant),
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        foregroundColor: colorScheme.onSecondary,
        backgroundColor: colorScheme.secondary,
        splashColor: colorScheme.secondaryVariant,
      ),
      buttonTheme: ButtonThemeData(
        height: 44.0,
        textTheme: ButtonTextTheme.primary,
        buttonColor: colorScheme.secondary,
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      ),
      dividerTheme: DividerThemeData(
        color: colorScheme.divider,
        thickness: 1.0,
        space: 1.0,
      ),
      cupertinoOverrideTheme: CupertinoThemeData(
        brightness: colorScheme.brightness,
        primaryColor: colorScheme.primary,
        textTheme: TextThemes.cupertinoOf(colorScheme),
      ),
    );
  }
}
