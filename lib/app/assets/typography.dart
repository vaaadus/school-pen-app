import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_pen/app/assets/colors.dart';

const TextStyle _montserrat = TextStyle(fontFamily: 'Montserrat');
const TextStyle _raleway = TextStyle(fontFamily: 'Raleway');

extension TextThemes on TextTheme {
  static TextTheme of(CustomColorScheme scheme) => TextTheme(
        headline1: _raleway.copyWith(
          fontSize: 60.0,
          height: 1.40,
          letterSpacing: -1.50,
          fontWeight: FontWeight.w500,
          color: scheme.primary,
        ),
        headline2: _raleway.copyWith(
          fontSize: 48.0,
          height: 1.25,
          letterSpacing: -0.50,
          fontWeight: FontWeight.w500,
          color: scheme.primary,
        ),
        headline3: _raleway.copyWith(
          fontSize: 34.0,
          height: 1.40,
          letterSpacing: 0.00,
          fontWeight: FontWeight.w500,
          color: scheme.primary,
        ),
        headline4: _raleway.copyWith(
          fontSize: 24.0,
          height: 1.33,
          letterSpacing: 0.25,
          fontWeight: FontWeight.w600,
          color: scheme.primary,
        ),
        headline5: _raleway.copyWith(
          fontSize: 22.0,
          height: 1.45,
          letterSpacing: 0.00,
          fontWeight: FontWeight.w600,
          color: scheme.primary,
        ),
        headline6: _raleway.copyWith(
          fontSize: 18.0,
          height: 1.33,
          letterSpacing: 0.15,
          fontWeight: FontWeight.w600,
          color: scheme.primary,
        ),
        bodyText1: _montserrat.copyWith(
          fontSize: 16.0,
          height: 1.50,
          letterSpacing: 0.00,
          fontWeight: FontWeight.w500,
          color: scheme.primary,
        ),
        bodyText2: _montserrat.copyWith(
          fontSize: 14.0,
          height: 1.40,
          letterSpacing: 0.00,
          fontWeight: FontWeight.w500,
          color: scheme.primary,
        ),
        subtitle1: _raleway.copyWith(
          fontSize: 16.0,
          height: 1.25,
          letterSpacing: 0.15,
          fontWeight: FontWeight.w500,
          color: scheme.primary,
        ),
        subtitle2: _raleway.copyWith(
          fontSize: 14.0,
          height: 1.33,
          letterSpacing: 0.10,
          fontWeight: FontWeight.w500,
          color: scheme.primary,
        ),
        button: _raleway.copyWith(
          fontSize: 14.0,
          height: 1.40,
          letterSpacing: 0.75,
          fontWeight: FontWeight.bold,
          color: scheme.primary,
        ),
        caption: _raleway.copyWith(
          fontSize: 12.0,
          height: 1.33,
          letterSpacing: 0.40,
          fontWeight: FontWeight.w600,
          color: scheme.primary,
        ),
        overline: _raleway.copyWith(
          fontSize: 12.0,
          height: 1.33,
          letterSpacing: 1.5,
          fontWeight: FontWeight.bold,
          color: scheme.primary,
        ),
      );

  static CupertinoTextThemeData cupertinoOf(CustomColorScheme scheme) => CupertinoTextThemeData(
        primaryColor: scheme.primary,
        dateTimePickerTextStyle: of(scheme).headline6,
      );

  TextStyle button2(BuildContext context) => _button2.copyWith(color: Theme.of(context).colorScheme.primary);

  TextStyle caption2(BuildContext context) => _caption2.copyWith(color: Theme.of(context).colorScheme.primary);

  static final TextStyle _button2 = _raleway.copyWith(
    fontSize: 12.0,
    height: 1.33,
    letterSpacing: 0.60,
    fontWeight: FontWeight.bold,
  );

  static final TextStyle _caption2 = _raleway.copyWith(
    fontSize: 10.0,
    height: 1.20,
    letterSpacing: 0.30,
    fontWeight: FontWeight.w600,
  );
}
