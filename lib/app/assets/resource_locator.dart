import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:school_pen/app/assets/colors.dart';
import 'package:school_pen/app/assets/images.dart';
import 'package:school_pen/app/common/formatter/number_formatter.dart';
import 'package:school_pen/app/pages/timetable/timetable_view_model.dart';
import 'package:school_pen/domain/common/color_enum.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/exception/at_least_one_value_must_be_selected_exception.dart';
import 'package:school_pen/domain/common/exception/date_range_start_after_end_exception.dart';
import 'package:school_pen/domain/common/exception/field_must_not_be_empty_exception.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/common/exception/number_out_of_range_exception.dart';
import 'package:school_pen/domain/common/exception/value_out_of_constraints_exception.dart';
import 'package:school_pen/domain/themes/model/custom_theme.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';
import 'package:school_pen/domain/user/exception/account_already_linked_exception.dart';
import 'package:school_pen/domain/user/exception/avatar_size_too_big_exception.dart';
import 'package:school_pen/domain/user/exception/email_taken_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_credentials_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_email_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_password_exception.dart';
import 'package:school_pen/domain/user/exception/password_cannot_contain_username_exception.dart';
import 'package:school_pen/domain/user/exception/password_too_short_exception.dart';
import 'package:school_pen/domain/user/exception/passwords_do_not_match_exception.dart';
import 'package:school_pen/domain/user/exception/too_many_failed_login_attempts_exception.dart';
import 'package:school_pen/domain/user/exception/username_taken_exception.dart';
import 'package:school_pen/domain/user/exception/username_too_short_exception.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/generated/l10n.dart';
import 'package:sprintf/sprintf.dart';

class ResourceLocator {
  static String weekdayToString(BuildContext context, Weekday weekday) {
    final DateTime dateTime = DateTime.now().adjustToNextWeekday(weekday);
    return DateFormat.EEEE().format(dateTime);
  }

  static String authProviderToName(BuildContext context, AuthProvider authProvider) {
    switch (authProvider) {
      case AuthProvider.apple:
        return S.of(context).common_account_apple;
      case AuthProvider.google:
        return S.of(context).common_account_google;
      case AuthProvider.facebook:
        return S.of(context).common_account_facebook;
    }
    throw ArgumentError('Unsupported auth provider: $authProvider');
  }

  static String authProviderToSignInText(BuildContext context, AuthProvider authProvider) {
    switch (authProvider) {
      case AuthProvider.apple:
        return S.of(context).common_account_signInWith_apple;
      case AuthProvider.google:
        return S.of(context).common_account_signInWith_google;
      case AuthProvider.facebook:
        return S.of(context).common_account_signInWith_facebook;
    }
    throw ArgumentError('Unsupported auth provider: $authProvider');
  }

  static String authProviderToImageAsset(AuthProvider authProvider) {
    switch (authProvider) {
      case AuthProvider.apple:
        return Images.ic_apple_24;
      case AuthProvider.google:
        return Images.ic_google_24;
      case AuthProvider.facebook:
        return Images.ic_facebook_24;
    }
    throw ArgumentError('Unsupported auth provider: $authProvider');
  }

  static Color authProviderToImageColor(AuthProvider authProvider) {
    switch (authProvider) {
      case AuthProvider.apple:
      case AuthProvider.facebook:
        return AppColors.white;
      case AuthProvider.google:
        return null;
    }
    throw ArgumentError('Unsupported auth provider: $authProvider');
  }

  static Color authProviderTextColor(BuildContext context, AuthProvider authProvider) {
    switch (authProvider) {
      case AuthProvider.apple:
      case AuthProvider.facebook:
        return AppColors.white;
      case AuthProvider.google:
        return AppColors.black;
    }
    throw ArgumentError('Unsupported auth provider: $authProvider');
  }

  static Color authProviderToColor(AuthProvider authProvider) {
    switch (authProvider) {
      case AuthProvider.apple:
        return AppColors.black;
      case AuthProvider.google:
        return AppColors.white;
      case AuthProvider.facebook:
        return AppColors.facebookBlue;
    }
    throw ArgumentError('Unsupported auth provider: $authProvider');
  }

  static String timetableTypeToText(BuildContext context, TimetableType type) {
    switch (type) {
      case TimetableType.student:
        return S.of(context).common_students;
      case TimetableType.teacher:
        return S.of(context).common_teacher;
      case TimetableType.room:
        return S.of(context).common_room;
      case TimetableType.custom:
        return S.of(context).common_custom;
      case TimetableType.unknown:
        return S.of(context).common_unknown;
    }
    throw ArgumentError('Unsupported timetable type: $type');
  }

  static String timetableViewModeToShortText(BuildContext context, TimetableViewMode viewMode) {
    switch (viewMode) {
      case TimetableViewMode.day:
        return S.of(context).timetable_viewMode_day;
      case TimetableViewMode.threeDays:
        return S.of(context).timetable_viewMode_threeDays;
      case TimetableViewMode.week:
        return S.of(context).timetable_viewMode_week;
    }

    throw ArgumentError('Unsupported timetable view mode: $viewMode');
  }

  static String timetableViewModeToText(BuildContext context, TimetableViewMode viewMode) {
    switch (viewMode) {
      case TimetableViewMode.day:
        return S.of(context).timetable_viewMode_dayView;
      case TimetableViewMode.threeDays:
        return S.of(context).timetable_viewMode_threeDaysView;
      case TimetableViewMode.week:
        return S.of(context).timetable_viewMode_weekView;
    }

    throw ArgumentError('Unsupported timetable view mode: $viewMode');
  }

  static String errorToText(BuildContext context, Object error) {
    if (error == null) return null;
    if (error is FieldMustNotBeEmptyException) return S.of(context).error_mustNotBeEmpty;
    if (error is PasswordsDoNotMatchException) return S.of(context).error_passwordsDoNotMatch;
    if (error is InvalidPasswordException) return S.of(context).error_invalidPassword;
    if (error is AccountAlreadyLinkedException) return S.of(context).error_accountAlreadyLinked;
    if (error is AvatarSizeTooBigException) return S.of(context).error_avatarSizeTooBig;
    if (error is PasswordCannotContainUsernameException) return S.of(context).error_passwordCannotContainUsername;
    if (error is TooManyFailedLoginAttemptsException) return S.of(context).error_tooManyFailedLoginAttempts;
    if (error is UsernameTakenException) return S.of(context).error_usernameTaken;
    if (error is UsernameTooShortException) return sprintf(S.of(context).error_usernameTooShort, [error.minLength]);
    if (error is InvalidEmailException) return S.of(context).error_invalidEmail;
    if (error is InvalidCredentialsException) return S.of(context).error_invalidCredentials;
    if (error is EmailTakenException) return S.of(context).error_emailTaken;
    if (error is PasswordTooShortException) return sprintf(S.of(context).error_passwordTooShort, [error.minLength]);
    if (error is NoInternetException) return S.of(context).error_noInternet;
    if (error is DateRangeStartAfterEndException) return S.of(context).error_startDateMustBeBeforeEndDate;
    if (error is AtLeastOneValueMustBeSelectedException) return S.of(context).error_atLeastOneValueMustBeSelected;

    if (error is NumberOutOfRangeException) {
      return sprintf(
        S.of(context).error_valueMustBeBetween,
        [NumberFormatter.format(error.min), NumberFormatter.format(error.max)],
      );
    }

    if (error is ValueOutOfConstraintsException) {
      return sprintf(S.of(context).error_valueMustBeOneOf, [error.allowedValues.join(', ')]);
    }

    return S.of(context).error_somethingWentWrong;
  }

  static String themeToString(BuildContext context, CustomTheme theme) {
    final S s = S.of(context);
    switch (theme) {
      case CustomTheme.system:
        final Brightness brightness = MediaQuery.of(context).platformBrightness;
        return brightness == Brightness.light ? s.common_theme_systemLight : s.common_theme_systemDark;
      case CustomTheme.light:
        return s.common_theme_light;
      case CustomTheme.amber:
        return s.common_theme_amber;
      case CustomTheme.red:
        return s.common_theme_red;
      case CustomTheme.green:
        return s.common_theme_green;
      case CustomTheme.dark:
        return s.common_theme_dark;
      case CustomTheme.black:
        return s.common_theme_black;
      case CustomTheme.navyBlue:
        return s.common_theme_navyBlue;
    }
    throw ArgumentError('Unsupported theme: $theme');
  }

  static Color colorEnumToColor(ColorEnum color) {
    if (color == null) return null;
    switch (color) {
      case ColorEnum.red:
        return AppColors.red1;
      case ColorEnum.orange:
        return AppColors.orange1;
      case ColorEnum.yellow:
        return AppColors.yellow;
      case ColorEnum.green:
        return AppColors.green2;
      case ColorEnum.teal:
        return AppColors.teal1;
      case ColorEnum.blue:
        return AppColors.blue1;
      case ColorEnum.lightBlue:
        return AppColors.blue3;
      case ColorEnum.purple:
        return AppColors.purple2;
      case ColorEnum.pink:
        return AppColors.pink1;
      case ColorEnum.black:
        return AppColors.black;
    }
    throw ArgumentError('Unsupported color: $color');
  }

  static String colorEnumToString(BuildContext context, ColorEnum color) {
    if (color == null) return null;
    switch (color) {
      case ColorEnum.red:
        return S.of(context).color_red;
      case ColorEnum.orange:
        return S.of(context).color_orange;
      case ColorEnum.yellow:
        return S.of(context).color_yellow;
      case ColorEnum.green:
        return S.of(context).color_green;
      case ColorEnum.teal:
        return S.of(context).color_teal;
      case ColorEnum.blue:
        return S.of(context).color_blue;
      case ColorEnum.lightBlue:
        return S.of(context).color_lightBlue;
      case ColorEnum.purple:
        return S.of(context).color_purple;
      case ColorEnum.pink:
        return S.of(context).color_pink;
      case ColorEnum.black:
        return S.of(context).color_black;
    }
    throw ArgumentError('Unsupported color: $color');
  }

  static Color letterToColor(BuildContext context, String letter) {
    assert(letter.runes.length == 1);
    return _resolveColor(context, letter.runes.first);
  }

  static Color _resolveColor(BuildContext context, int rune) {
    final ColorEnum color = ColorEnum.values[rune % ColorEnum.values.length];
    if (color == ColorEnum.black) return _resolveColor(context, rune + 1);
    return colorEnumToColor(color);
  }
}
