import 'package:flutter/material.dart';

/// Aggregates commonly used shadows.
class Shadows {
  static final BoxShadow topMedium = BoxShadow(
    color: Colors.black.withOpacity(0.25),
    spreadRadius: 2,
    blurRadius: 8,
    offset: Offset(0, -2),
  );
}
