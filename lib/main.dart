import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:school_pen/app/service/applaunch/app_launch_manager.dart';
import 'package:school_pen/domain/ads/ad_manager.dart';
import 'package:school_pen/domain/quickactions/quick_actions_manager.dart';

import 'app/di/injection.dart';
import 'domain/logger/logger.dart';
import 'main_app.dart';
import 'main_common.dart';

const Logger _logger = Logger('main');

void main() async {
  await App.ensureInitialized();

  await Future.wait([
    _enforceUiDependenciesCreated(),
    _initializeAds(),
    _initializeGoogleFonts(),
  ]);

  _runApp();
}

Future<void> _enforceUiDependenciesCreated() async {
  Injection.get<QuickActionsManager>();
  Injection.get<AppLaunchManager>();
}

Future<void> _initializeAds() async {
  final AdManager manager = Injection.get();
  await manager.initialize();
}

Future<void> _initializeGoogleFonts() async {
  LicenseRegistry.addLicense(() async* {
    final List<String> files = ['assets/fonts/Montserrat/OFL.txt', 'assets/fonts/Raleway/OFL.txt'];
    for (String file in files) {
      final String license = await rootBundle.loadString(file);
      yield LicenseEntryWithLineBreaks(['google_fonts'], license);
    }
  });
}

void _runApp() {
  runZonedGuarded(() {
    runApp(SchoolPenApp());
  }, (error, stackTrace) {
    _logger.logCrash(error, stackTrace);
  });
}
