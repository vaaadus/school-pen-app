import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:school_pen/app/assets/themes.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/pages/gateway/gateway_page.dart';
import 'package:school_pen/app/pages/nav.dart';
import 'package:school_pen/app/widgets/custom_theme_provider.dart';
import 'package:school_pen/app/widgets/progress_overlay.dart';
import 'package:school_pen/app/widgets/toast_overlay.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/themes/model/custom_theme.dart';
import 'package:school_pen/generated/l10n.dart';

/// Main application.
class SchoolPenApp extends StatefulWidget {
  const SchoolPenApp({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => SchoolPenAppState();
}

class SchoolPenAppState extends State<SchoolPenApp> with WidgetsBindingObserver {
  final LifecycleManager _lifecycleManager = Injection.get();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _lifecycleManager.resume();
  }

  @override
  void dispose() {
    _lifecycleManager.pause();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _lifecycleManager.resume();
    } else if (state == AppLifecycleState.paused) {
      _lifecycleManager.pause();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SchoolPenOptions(builder: (BuildContext context) {
      return ProgressOverlay(
        child: ToastOverlay(
          child: Builder(builder: _buildApp),
        ),
      );
    });
  }

  Widget _buildApp(BuildContext context) {
    final CustomTheme theme = SchoolPenOptions.of(context).customTheme;

    return MaterialApp(
      onGenerateTitle: (BuildContext context) => S.of(context).app_name,
      supportedLocales: S.delegate.supportedLocales,
      localizationsDelegates: [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      theme: Themes.lightOf(theme),
      darkTheme: Themes.darkOf(theme),
      home: _buildHome(),
      navigatorKey: Nav.key,
      navigatorObservers: Injection.get(),
    );
  }

  /// Home widget performs some initialization logic which requires fully built material app.
  /// See the implementation of initState from [GatewayPage].
  Widget _buildHome() {
    return GatewayPage();
  }
}
