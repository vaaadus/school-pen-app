import 'dart:async';
import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_sign_in_dartio/google_sign_in_dartio.dart';
import 'package:school_pen/app/di/injection.dart';
import 'package:school_pen/app/service/widget/agenda_widget.dart';
import 'package:school_pen/domain/agenda/agenda_manager.dart';
import 'package:school_pen/domain/analytics/analytics.dart';
import 'package:school_pen/domain/analytics/analytics_manager.dart';
import 'package:school_pen/domain/common/env_options.dart';
import 'package:school_pen/domain/common/parse/parse_sdk_metadata.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/diagnosis/diagnosis.dart';
import 'package:school_pen/domain/diagnosis/diagnosis_manager.dart';
import 'package:school_pen/domain/exam/exam_manager.dart';
import 'package:school_pen/domain/files/file_manager.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/homework/homework_manager.dart';
import 'package:school_pen/domain/inappsubscription/inapp_subscription_manager.dart';
import 'package:school_pen/domain/logger/appenders/console_logger_appender.dart';
import 'package:school_pen/domain/logger/appenders/sentry_logger_appender.dart';
import 'package:school_pen/domain/logger/logger.dart';
import 'package:school_pen/domain/note/note_manager.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/pushnotification/push_notification_manager.dart';
import 'package:school_pen/domain/reminder/reminder_manager.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:school_pen/domain/sync/sync_manager.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';
import 'package:school_pen/domain/user/user_manager.dart';
import 'package:school_pen/domain/work/work_manager.dart';
import 'package:synchronized/synchronized.dart';
import 'package:time_machine/time_machine.dart';

/// Main initialization logic which should be executed regardless
/// of how the app is launched, either in foreground or in background.
class App {
  static final Lock _lock = Lock();
  static bool _initialized = false;

  static Future<void> ensureInitialized() {
    return _lock.synchronized(() async {
      if (!_initialized) {
        _initialized = true;
        await _initialize();
      }
    });
  }
}

Future<void> _initialize() async {
  WidgetsFlutterBinding.ensureInitialized();
  EquatableConfig.stringify = true;

  await Future.wait([
    _configureErrorHandler(),
    _initializeLogger(),
    _configureTimeMachine(),
    _configureDependencyInjection(),
    _configureParseSdk(),
    _configureFirebase(),
    _configureGoogleSignInForDesktop(),
  ]);

  await Future.wait([
    _initializeWidgets(),
    _initializeDiagnosis(),
    _initializeAnalytics(),
    _initializeNotifications(),
    _initializePushNotifications(),
    _initializeBackgroundWork(),
    _enforceDependenciesCreated(),
  ]);
}

Future<void> _configureErrorHandler() async {
  FlutterError.onError = (FlutterErrorDetails details) {
    if (kReleaseMode) {
      // In production mode report to the application zone to report to Logger.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    } else {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    }
  };
}

Future<void> _initializeLogger() async {
  if (EnvOptions.useProductionEnv) {
    Logger.addAppender(SentryLoggerAppender());
  } else {
    Logger.addAppender(ConsoleLoggerAppender());
  }
}

Future<void> _configureTimeMachine() async {
  // for rrule
  // TODO fix after this one is closed https://github.com/flutter/flutter/issues/14815
  if (kIsWeb || !Platform.isMacOS) {
    await TimeMachine.initialize({'rootBundle': rootBundle});
  }
}

Future<void> _configureDependencyInjection() async {
  await Injection.configureDependencies();
}

Future<void> _configureParseSdk() async {
  await ParseSdkMetadata.initialize();
}

Future<void> _configureFirebase() async {
  await Firebase.initializeApp();
}

Future<void> _configureGoogleSignInForDesktop() async {
  if (!kIsWeb && (Platform.isMacOS || Platform.isWindows || Platform.isWindows)) {
    await GoogleSignInDart.register(clientId: EnvOptions.googleClientId);
  }
}

Future<void> _initializeWidgets() async {
  final AgendaWidget widget = Injection.get();
  await widget.initialize();
}

Future<void> _initializeDiagnosis() async {
  final DiagnosisManager manager = Injection.get();
  Diagnosis.setImplementation(manager);
}

Future<void> _initializeAnalytics() async {
  final AnalyticsManager manager = Injection.get();
  Analytics.setImplementation(manager);
}

Future<void> _initializeNotifications() async {
  final NotificationManager repository = Injection.get();
  await repository.initialize();
}

Future<void> _initializePushNotifications() async {
  final PushNotificationManager manager = Injection.get();
  await manager.initialize();
}

Future<void> _initializeBackgroundWork() async {
  final WorkManager manager = Injection.get();
  await manager.initialize();
}

/// By default, dependencies are created lazily,
/// however these must be created as soon as possible.
Future<void> _enforceDependenciesCreated() async {
  Injection.get<InAppSubscriptionManager>();
  Injection.get<RemoteFileManager>();
  Injection.get<UserManager>();
  Injection.get<SyncManager>();
  Injection.get<SchoolYearManager>();
  Injection.get<NoteManager>();
  Injection.get<SemesterManager>();
  Injection.get<TeacherManager>();
  Injection.get<CourseManager>();
  Injection.get<GradeManager>();
  Injection.get<TimetableManager>();
  Injection.get<ExamManager>();
  Injection.get<HomeworkManager>();
  Injection.get<ReminderManager>();
  Injection.get<AgendaManager>();
  Injection.get<AgendaWidget>();
}
