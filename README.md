# school_timetable

School Timetable - a flutter app for the students.

## Required Plugins

Flutter intl (https://plugins.jetbrains.com/plugin/13666-flutter-intl)

## Useful commands

### Run tests
flutter test

### Building apk

#### All versions / architectures
flutter build appbundle

flutter build apk --split-per-abi

#### 64 bit
flutter build apk --target-platform=android-arm64 --split-per-abi

#### 32 bit
flutter build apk --target-platform=android-arm --split-per-abi

#### obfuscation
flutter build apk --obfuscate --split-debug-info=/tmp/schoolpen/

#### deobfuscation
flutter symbolize -i <stack trace file> -d /out/android/app.android-arm64.symbols 

### One-time code generation
flutter pub run build_runner build --delete-conflicting-outputs

### Generating code continuously
flutter pub run build_runner watch --delete-conflicting-outputs

### Generate mappings for .arb files (string resources)
flutter pub global run intl_utils:generate