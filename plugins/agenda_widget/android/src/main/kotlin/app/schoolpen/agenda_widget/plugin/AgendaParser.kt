package app.schoolpen.agenda_widget.plugin

import app.schoolpen.agenda_widget.model.AgendaEvent
import app.schoolpen.agenda_widget.model.AgendaEventKind

object AgendaParser {
    fun parse(args: List<Map<String, Any>>): List<AgendaEvent> {
        return args.map {
            AgendaEvent(
                    kind = AgendaEventKind.forTag(it["kind"] as String),
                    title = it["title"] as String,
                    subtitle = it["subtitle"] as String?,
                    label = it["label"] as String?,
                    color = (it["color"] as Long?)?.toInt()
            )
        }
    }
}