package app.schoolpen.agenda_widget.model


enum class AgendaEventKind(val tag: String) {
    Note("note"),
    Exam("exam"),
    Homework("homework"),
    Reminder("reminder"),
    Timetable("timetable");

    companion object {
        fun forTag(tag: String): AgendaEventKind {
            for (kind in values()) {
                if (kind.tag == tag) return kind
            }
            throw IllegalArgumentException("Unsupported tag: $tag")
        }
    }
}