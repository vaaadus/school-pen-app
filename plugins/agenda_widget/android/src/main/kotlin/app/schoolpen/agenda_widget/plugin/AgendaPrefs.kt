package app.schoolpen.agenda_widget.plugin

import android.content.Context
import android.content.SharedPreferences

class AgendaPrefs(private val prefs: SharedPreferences) {

    companion object {
        private const val prefsName = "agenda_plugin"
        private const val keyDartEntrypoint = "key_dart_entrypoint"

        fun of(context: Context): AgendaPrefs {
            return AgendaPrefs(context.getSharedPreferences(prefsName, Context.MODE_PRIVATE))
        }
    }

    fun getDartEntrypoint(): Long {
        return prefs.getLong(keyDartEntrypoint, 0)
    }

    fun setDartEntrypoint(entrypoint: Long) {
        prefs.edit().putLong(keyDartEntrypoint, entrypoint).apply()
    }
}