package app.schoolpen.agenda_widget.model

data class AgendaEvent(
        val kind: AgendaEventKind,
        val title: String,
        val subtitle: String?,
        val label: String?,
        val color: Int?
)