package app.schoolpen.agenda_widget

import android.content.Context
import android.util.Log
import app.schoolpen.agenda_widget.plugin.AgendaPrefs
import app.schoolpen.agenda_widget.widget.AgendaWidgetProvider
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

class AgendaWidgetPlugin : FlutterPlugin, MethodChannel.MethodCallHandler {

    companion object {
        const val channelName = "app.schoolpen/agenda"
    }

    private var channel: MethodChannel? = null
    private var context: Context? = null

    override fun onAttachedToEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        this.context = binding.applicationContext
        channel = MethodChannel(binding.binaryMessenger, channelName)
        channel?.setMethodCallHandler(this)
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        context = null
        channel?.setMethodCallHandler(null)
        channel = null
    }

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        Log.d("AgendaWidgetPlugin", "onMethodCall, call: " + call.method)

        val context = context!!
        when (call.method) {
            "initialize" -> {
                AgendaPrefs.of(context).setDartEntrypoint(call.arguments())
                result.success(null)
            }
            "updateWidget" -> {
                val intent = AgendaWidgetProvider.newUpdateIntent(context)
                context.sendBroadcast(intent)
                result.success(null)
            }
            else -> {
                result.notImplemented()
            }
        }
    }
}