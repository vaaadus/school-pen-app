package app.schoolpen.agenda_widget.widget

import android.content.Context
import android.content.Intent
import android.widget.RemoteViewsService

class AgendaWidgetService : RemoteViewsService() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, AgendaWidgetService::class.java)
        }
    }

    override fun onGetViewFactory(intent: Intent?): RemoteViewsFactory {
        return AgendaRemoteViewsFactory(applicationContext)
    }
}