package app.schoolpen.agenda_widget.widget

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import app.schoolpen.agenda_widget.AgendaWidgetPlugin
import app.schoolpen.agenda_widget.R
import app.schoolpen.agenda_widget.model.AgendaEvent
import app.schoolpen.agenda_widget.model.AgendaEventKind
import app.schoolpen.agenda_widget.plugin.AgendaParser
import app.schoolpen.agenda_widget.plugin.AgendaPrefs
import io.flutter.FlutterInjector
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.dart.DartExecutor
import io.flutter.plugin.common.MethodChannel
import io.flutter.view.FlutterCallbackInformation
import kotlinx.coroutines.runBlocking
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class AgendaRemoteViewsFactory(private val context: Context) : RemoteViewsService.RemoteViewsFactory {
    private val handler = Handler(Looper.getMainLooper())
    private val events = ArrayList<AgendaEvent>()
    private var engine: FlutterEngine? = null
    private var channel: MethodChannel? = null

    override fun onCreate() {
        val loader = FlutterInjector.instance().flutterLoader()
        loader.startInitialization(context)
        loader.ensureInitializationComplete(context, null)

        val entrypoint = AgendaPrefs.of(context).getDartEntrypoint()
        if (entrypoint == 0L) return

        val callbackInfo = FlutterCallbackInformation.lookupCallbackInformation(entrypoint)
        val dartBundlePath = loader.findAppBundlePath()
        val dartCallback = DartExecutor.DartCallback(context.assets, dartBundlePath, callbackInfo)

        engine = FlutterEngine(context)
        engine!!.dartExecutor.executeDartCallback(dartCallback)

        channel = MethodChannel(engine!!.dartExecutor.binaryMessenger, AgendaWidgetPlugin.channelName)
    }

    override fun onDestroy() {
        handler.post {
            events.clear()
            engine?.destroy()
            engine = null
            channel = null
        }
    }

    override fun onDataSetChanged() {
        val newEvents = runBlocking { getAgenda() }
        events.clear()
        events += newEvents
    }

    override fun getLoadingView(): RemoteViews? = null

    override fun getViewAt(position: Int): RemoteViews {
        val event = events[position]

        val views = RemoteViews(context.packageName, R.layout.item_widget_agenda)
        views.setOnClickFillInIntent(R.id.widget_item_container, Intent())
        views.setTextViewText(R.id.title, event.title)
        views.setViewVisibility(R.id.subtitle, if (event.subtitle.isNullOrBlank()) View.GONE else View.VISIBLE)
        views.setTextViewText(R.id.subtitle, event.subtitle ?: "")
        views.setViewVisibility(R.id.label, if (event.label.isNullOrBlank()) View.GONE else View.VISIBLE)
        views.setTextViewText(R.id.label, event.label ?: "")
        views.setImageViewResource(R.id.event_icon, mapKindToDrawable(event.kind))
        views.setInt(R.id.event_background, "setColorFilter", event.color
                ?: ContextCompat.getColor(context, R.color.widget_secondary))
        return views
    }

    override fun getItemId(position: Int) = position.toLong()

    override fun hasStableIds() = true

    override fun getCount() = events.size

    override fun getViewTypeCount() = 1

    @DrawableRes
    private fun mapKindToDrawable(kind: AgendaEventKind): Int = when (kind) {
        AgendaEventKind.Note -> R.drawable.ic_notification_16
        AgendaEventKind.Exam -> R.drawable.ic_exam_16
        AgendaEventKind.Homework -> R.drawable.ic_homework_16
        AgendaEventKind.Reminder -> R.drawable.ic_reminder_16
        AgendaEventKind.Timetable -> R.drawable.ic_timetable_16
    }


    private suspend fun getAgenda(): List<AgendaEvent> {
        assert(channel != null)

        return suspendCoroutine { continuation ->
            handler.post {
                channel?.invokeMethod("getAgenda", null, object : MethodChannel.Result {

                    @Suppress("UNCHECKED_CAST")
                    override fun success(result: Any?) {
                        if (result != null) {
                            continuation.resume(AgendaParser.parse(result as List<Map<String, Any>>))
                        } else {
                            continuation.resumeWithException(IllegalArgumentException("Not valid args, $result"))
                        }
                    }

                    override fun error(errorCode: String?, errorMessage: String?, errorDetails: Any?) {
                        continuation.resumeWithException(Exception(errorMessage))
                    }

                    override fun notImplemented() {
                        continuation.resumeWithException(InterruptedException())
                    }
                })
            }
        }
    }
}