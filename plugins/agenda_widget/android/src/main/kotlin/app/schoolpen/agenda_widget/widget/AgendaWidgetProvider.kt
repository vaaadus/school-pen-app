package app.schoolpen.agenda_widget.widget

import android.app.PendingIntent
import android.app.PendingIntent.FLAG_CANCEL_CURRENT
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.RemoteViews
import app.schoolpen.agenda_widget.R

class AgendaWidgetProvider : AppWidgetProvider() {

    companion object {
        fun newUpdateIntent(context: Context): Intent {
            val intent = Intent(context, AgendaWidgetProvider::class.java)
            intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, getWidgetIds(context))
            return intent
        }

        private fun getWidgetIds(context: Context): IntArray {
            val componentName = ComponentName(context, AgendaWidgetProvider::class.java)
            return AppWidgetManager.getInstance(context).getAppWidgetIds(componentName)
        }
    }

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        for (appWidgetId in appWidgetIds) {
            val intent = AgendaWidgetService.newIntent(context)
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
            intent.data = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME))

            val activityIntent = createMainActivityIntent(context)

            val remoteViews = RemoteViews(context.packageName, R.layout.widget_agenda)
            remoteViews.setOnClickPendingIntent(R.id.widget_container, activityIntent)
            remoteViews.setOnClickPendingIntent(R.id.add_event, activityIntent)
            remoteViews.setPendingIntentTemplate(R.id.list_view, activityIntent)
            remoteViews.setRemoteAdapter(R.id.list_view, intent)
            remoteViews.setEmptyView(R.id.list_view, R.id.empty_view)

            appWidgetManager.updateAppWidget(appWidgetId, remoteViews)
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.list_view)
        }
    }

    private fun createMainActivityIntent(context: Context): PendingIntent {
        val intent = Intent(context, Class.forName("com.shemhazai.school.timetable.MainActivity"))
        return PendingIntent.getActivity(context, 0, intent, FLAG_CANCEL_CURRENT)
    }
}