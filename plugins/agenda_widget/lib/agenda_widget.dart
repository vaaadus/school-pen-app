import 'dart:io';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

/// Whether the system app widget is supported.
bool isAgendaWidgetSupported() {
  return !kIsWeb && Platform.isAndroid;
}

/// Allows the native part to fetch agenda data from flutter.
class AgendaWidgetPlugin {
  static const MethodChannel _channel = MethodChannel('app.schoolpen/agenda');

  static Future<void> initialize({
    @required void Function() initializer,
    @required Future<List<Map<String, dynamic>>> Function() getEvents,
  }) async {
    if (isAgendaWidgetSupported()) {
      final CallbackHandle callback = PluginUtilities.getCallbackHandle(initializer);
      await _channel.invokeMethod('initialize', callback.toRawHandle());

      _channel.setMethodCallHandler((MethodCall call) async {
        if (call.method == 'getAgenda') {
          return getEvents();
        }
      });
    }
  }

  static Future<void> updateWidget() async {
    await _channel.invokeMethod('updateWidget');
  }
}
