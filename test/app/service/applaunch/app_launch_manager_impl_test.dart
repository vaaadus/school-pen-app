// ignore_for_file: must_be_immutable

import 'package:mockito/mockito.dart';
import 'package:school_pen/app/service/applaunch/app_launch_manager_impl.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification.dart';
import 'package:school_pen/domain/pushnotification/push_notification_manager.dart';
import 'package:school_pen/domain/rateapp/rate_app_manager.dart';
import 'package:school_pen/domain/user/user_manager.dart';
import 'package:test/test.dart';

class TestLifecycleManager extends Mock implements LifecycleManager {}

class TestPushNotificationManager extends Mock implements PushNotificationManager {}

class TestPushNotification extends Mock implements PushNotification {}

class TestUserManager extends Mock implements UserManager {}

class TestRateAppManager extends Mock implements RateAppManager {}

void main() {
  group('AppLaunchManagerImpl', () {
    AppLaunchManagerImpl manager;

    final LifecycleManager lifecycleManager = TestLifecycleManager();
    final PushNotificationManager pushNotificationManager = TestPushNotificationManager();
    final UserManager userManager = TestUserManager();
    final PushNotification pushNotification = TestPushNotification();
    final RateAppManager rateAppManager = TestRateAppManager();

    setUp(() {
      manager = AppLaunchManagerImpl(
        lifecycleManager,
        pushNotificationManager,
        userManager,
        rateAppManager,
      );
    });

    tearDown(() {
      reset(lifecycleManager);
      reset(pushNotificationManager);
      reset(userManager);
      reset(pushNotification);
      reset(rateAppManager);
    });

    // TODO: implement support for push notifications
    // test('onPushNotification() skips unknown notifications', () async {
    //   when(pushNotification.type).thenReturn(PushNotificationType.unknown);
    //   await manager.onPushNotification(pushNotification);
    //   verifyNever(timetableManager.onSchoolTimetableChanged());
    // });
    //
    // test('onPushNotification() notifies timetable manager school changed', () async {
    //   when(pushNotification.type).thenReturn(PushNotificationType.schoolTimetableChanged);
    //   await manager.onPushNotification(pushNotification);
    //   verify(timetableManager.onSchoolTimetableChanged());
    // });
  });
}
