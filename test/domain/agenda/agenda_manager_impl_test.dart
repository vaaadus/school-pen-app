import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:school_pen/domain/agenda/agenda_manager.dart';
import 'package:school_pen/domain/agenda/agenda_manager_impl.dart';
import 'package:school_pen/domain/agenda/model/agenda_event.dart';
import 'package:school_pen/domain/agenda/notification/agenda_notification_builder.dart';
import 'package:school_pen/domain/agenda/notification/timetable_notification_builder.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';
import 'package:school_pen/domain/notification/model/scheduled_notification.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';
import 'package:school_pen/domain/userconfig/model/agenda_notification_options.dart';
import 'package:school_pen/domain/userconfig/model/timetable_notification_options.dart';

class TestNotificationManager extends Mock implements NotificationManager {}

class TestAgendaNotificationBuilder extends Mock implements AgendaNotificationBuilder {}

class TestTimetableNotificationBuilder extends Mock implements TimetableNotificationBuilder {}

class TestCourseManager extends Mock implements CourseManager {}

class TestAgendaManager extends Mock implements AgendaManager {}

// ignore: must_be_immutable
class TestAddNotification extends Mock implements AddNotification {}

class TestAgendaEvent extends Mock implements AgendaEvent {}

class TestAgendaTimetableEvent extends Mock implements AgendaTimetableEvent {}

// ignore: must_be_immutable
class TestSingleTimetableEvent extends Mock implements SingleTimetableEvent {}

class TestDateTimeProvider extends Mock implements DateTimeProvider {}

void main() {
  group('AgendaManagerImpl', () {
    final NotificationManager notifications = TestNotificationManager();
    final AgendaNotificationBuilder agendaBuilder = TestAgendaNotificationBuilder();
    final TimetableNotificationBuilder timetableBuilder = TestTimetableNotificationBuilder();
    final CourseManager courseManager = TestCourseManager();
    final AgendaManager agendaManager = TestAgendaManager();
    final DateTimeProvider dateTimeProvider = TestDateTimeProvider();

    AgendaNotificationsManager manager;

    setUp(() {
      when(agendaManager.getAgendaOptions()).thenAnswer((_) async => AgendaNotificationOptions(enabled: false));
      when(agendaManager.getTimetableOptions()).thenAnswer((_) async => TimetableNotificationOptions(enabled: false));
      when(dateTimeProvider.now()).thenAnswer((_) => DateTime.now());

      manager = AgendaNotificationsManager(notifications, agendaBuilder, timetableBuilder, courseManager, dateTimeProvider);
    });

    tearDown(() {
      reset(notifications);
      reset(agendaBuilder);
      reset(timetableBuilder);
      reset(courseManager);
      reset(agendaManager);
      reset(dateTimeProvider);
    });

    test('setupNotifications with disabled notifications', () async {
      when(agendaManager.getAgendaOptions()).thenAnswer((_) async => AgendaNotificationOptions(enabled: false));
      when(agendaManager.getTimetableOptions()).thenAnswer((_) async => TimetableNotificationOptions(enabled: false));

      await manager.setupNotifications(agendaManager);

      verify(notifications.cancelAllOnChannel(NotificationChannel.agenda));
      verify(notifications.cancelAllOnChannel(NotificationChannel.upcomingLessons));
    });

    test('setupNotifications agenda', () async {
      final AddNotification todayNotification = TestAddNotification();
      final Time time = Time(hour: 13, minute: 0);
      final List<Weekday> weekdays = [Weekday.fromDateTime(DateTime.now())];
      final List<AgendaEvent> events = [TestAgendaEvent(), TestAgendaEvent()];

      when(agendaManager.getAgendaOptions()).thenAnswer((_) async => AgendaNotificationOptions(time: time, weekdays: weekdays));
      when(agendaManager.getEvents(DateTime.now().withoutTime(), kinds: AgendaNotificationsManager.agendaKinds))
          .thenAnswer((_) async => events);
      when(agendaBuilder.build(DateTime.now().withTime(time), events.length)).thenReturn(todayNotification);

      await manager.setupNotifications(agendaManager);

      verifyNever(notifications.cancelAllOnChannel(NotificationChannel.agenda));
      verify(notifications.scheduleAllOnChannel(NotificationChannel.agenda, [todayNotification]));
    });

    test('setupNotifications upcoming lessons', () async {
      final DateTime now = DateTime.now().withTime(_time(12, 0));
      final DateTime tomorrow = DateTime.now().add(Duration(days: 1)).withTime(_time(12, 0));
      final TimetableNotificationOptions options = TimetableNotificationOptions(notifyBeforeLessonInMinutes: 15);
      final List<AgendaEventKind> kinds = [AgendaEventKind.timetable];

      final AgendaTimetableEvent pastEvent = _timetableEvent(dateTime: now, timeRange: _timeRange(_time(8, 0), _time(8, 45)));
      final AgendaTimetableEvent currentEvent = _timetableEvent(dateTime: now, timeRange: _timeRange(_time(12, 0), _time(12, 45)));
      final AgendaTimetableEvent todayEvent = _timetableEvent(dateTime: now, timeRange: _timeRange(_time(14, 5), _time(14, 50)));
      final AgendaTimetableEvent tomorrowEvent1 = _timetableEvent(dateTime: tomorrow, timeRange: _timeRange(_time(8, 0), _time(8, 30)));
      final AgendaTimetableEvent tomorrowEvent2 = _timetableEvent(dateTime: tomorrow, timeRange: _timeRange(_time(8, 0), _time(9, 0)));
      final AgendaTimetableEvent futureEvent = _timetableEvent(dateTime: tomorrow, timeRange: _timeRange(_time(10, 15), _time(11, 0)));

      final ScheduledNotification pastNotification = _scheduledNotification(id: 1, date: _notificationDate(pastEvent, options));
      final ScheduledNotification currentNotification = _scheduledNotification(id: 2, date: _notificationDate(currentEvent, options));
      final ScheduledNotification todayNotification = _scheduledNotification(id: 3, date: _notificationDate(todayEvent, options));
      final ScheduledNotification tomorrowNotification = _scheduledNotification(id: 4, date: _notificationDate(tomorrowEvent1, options));
      final ScheduledNotification futureNotification = _scheduledNotification(id: 5, date: _notificationDate(futureEvent, options));

      when(dateTimeProvider.now()).thenReturn(now);
      when(agendaManager.getTimetableOptions()).thenAnswer((_) async => options);

      when(agendaManager.getEvents(now.withoutTime(), kinds: kinds)).thenAnswer((_) async => [
            pastEvent,
            currentEvent,
            todayEvent,
          ]);

      when(agendaManager.getEvents(now.withoutTime().add(Duration(days: 1)), kinds: kinds)).thenAnswer((_) async => [
            tomorrowEvent1,
            tomorrowEvent2,
            futureEvent,
          ]);

      when(notifications.getAllScheduledOn(NotificationChannel.upcomingLessons)).thenAnswer((_) async => [
            pastNotification,
            currentNotification,
            todayNotification,
            tomorrowNotification,
          ]);

      _whenBuildTimetableNotification(timetableBuilder, pastNotification, [pastEvent]);
      _whenBuildTimetableNotification(timetableBuilder, currentNotification, [currentEvent]);
      _whenBuildTimetableNotification(timetableBuilder, todayNotification, [todayEvent]);
      _whenBuildTimetableNotification(timetableBuilder, tomorrowNotification, [tomorrowEvent1, tomorrowEvent2]);
      _whenBuildTimetableNotification(timetableBuilder, futureNotification, [futureEvent]);

      await manager.setupNotifications(agendaManager);

      verify(notifications.cancelAll([pastNotification.id]));
      verify(notifications.scheduleAll([futureNotification.toAddNotification()]));
    });
  });
}

void _whenBuildTimetableNotification(
    TimetableNotificationBuilder builder, ScheduledNotification notification, List<AgendaTimetableEvent> events) {
  when(builder.build(notification.dateTime, events.map((e) => e.raw).toList(), null)).thenReturn(notification.toAddNotification());
}

ScheduledNotification _scheduledNotification({@required int id, @required DateTime date}) {
  return ScheduledNotification(
    id: id,
    dateTime: date,
    title: id.toString(),
    body: id.toString(),
    channel: NotificationChannel.upcomingLessons,
    payload: NotificationPayload(),
  );
}

AgendaTimetableEvent _timetableEvent({@required DateTime dateTime, @required TimeRange timeRange}) {
  dateTime = dateTime.asUtc();

  final SingleTimetableEvent raw = TestSingleTimetableEvent();
  when(raw.dateTime).thenReturn(dateTime.withTime(timeRange.start));
  when(raw.timeRange).thenReturn(timeRange);
  when(raw.reoccursAt).thenReturn(DateTimeRange(
    start: dateTime.withTime(timeRange.start),
    end: dateTime.withTime(timeRange.end),
  ));

  return AgendaTimetableEvent(raw, TimetableType.custom);
}

DateTime _notificationDate(AgendaTimetableEvent event, TimetableNotificationOptions options) {
  return event.raw.dateTime.asLocal().subtract(Duration(minutes: options.notifyBeforeLessonInMinutes));
}

TimeRange _timeRange(Time start, Time end) => TimeRange(start: start, end: end);

Time _time(int hour, int minute) => Time(hour: hour, minute: minute);
