import 'package:mockito/mockito.dart';
import 'package:school_pen/domain/agenda/agenda_manager.dart';
import 'package:school_pen/domain/agenda/model/agenda_event.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/inappsubscription/inapp_subscription_manager.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/rateapp/rate_app_manager.dart';
import 'package:school_pen/domain/smartnote/smart_note.dart';
import 'package:school_pen/domain/smartnote/smart_note_manager_impl.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/user_manager.dart';
import 'package:test/test.dart';

class TestInAppSubscriptionManager extends Mock implements InAppSubscriptionManager {}

class TestRateAppManager extends Mock implements RateAppManager {}

class TestUserManager extends Mock implements UserManager {}

class TestAgendaManager extends Mock implements AgendaManager {}

void main() {
  group('SmartNoteManager', () {
    SmartNoteManagerImpl manager;

    final subscriptionManager = TestInAppSubscriptionManager();
    final rateAppManager = TestRateAppManager();
    final userManager = TestUserManager();
    final slidingWindow = ForcedSlidingWindow();
    final agendaManager = TestAgendaManager();

    void _stubHasPendingPurchases(bool has) {
      when(subscriptionManager.hasPendingPurchases).thenAnswer((_) => Future.value(has));
    }

    void _stubSubscriptionActive(bool active) {
      when(subscriptionManager.isSubscriptionActive).thenReturn(active);
    }

    void _stubPostponedPendingPurchaseWindowHasPassed(bool passed) {
      slidingWindow.forcePassed(key: SmartNoteManagerImpl.keyPostponedBillingWindow, passed: passed);
    }

    void _stubShouldRateApp(bool should) {
      when(rateAppManager.shouldRateNow).thenReturn(should);
    }

    void _stubUserSignedIn(bool signedIn) {
      when(userManager.getCurrentUser()).thenAnswer((_) async => signedIn ? User(id: 'test') : null);
    }

    void _stubPostponedLoginOfferWindowHasPassed(bool passed) {
      slidingWindow.forcePassed(key: SmartNoteManagerImpl.keyPostponedLoginWindow, passed: passed);
    }

    void _stubHasOverdueEvents(bool has) {
      when(agendaManager.getOverdueEvents()).thenAnswer((_) async {
        return [
          if (has)
            AgendaNoteEvent(Note(
              id: Id.random(),
              schoolYearId: Id.random(),
              notificationDate: DateTime.now(),
              items: [],
              updatedAt: DateTime.now(),
              isArchived: false,
            )),
        ];
      });
    }

    setUp(() {
      _stubHasPendingPurchases(false);
      _stubPostponedPendingPurchaseWindowHasPassed(false);
      _stubShouldRateApp(false);
      _stubUserSignedIn(false);
      _stubPostponedLoginOfferWindowHasPassed(false);
      _stubSubscriptionActive(false);
      _stubHasOverdueEvents(false);

      manager = SmartNoteManagerImpl(
        subscriptionManager,
        userManager,
        slidingWindow,
        agendaManager,
      );
    });

    tearDown(() {
      reset(subscriptionManager);
      reset(rateAppManager);
      reset(userManager);
      reset(agendaManager);
    });

    test('.smartNote pending billing', () async {
      _stubHasPendingPurchases(true);
      _stubPostponedPendingPurchaseWindowHasPassed(true);

      expect(await manager.smartNote, equals(SmartNote.pendingBilling));
    });

    test('.smartNote pending billing dismissed', () async {
      _stubHasPendingPurchases(true);
      _stubPostponedPendingPurchaseWindowHasPassed(false);

      expect(await manager.smartNote, isNot(equals(SmartNote.pendingBilling)));
    });

    test('.smartNote login offer', () async {
      _stubUserSignedIn(false);
      _stubPostponedLoginOfferWindowHasPassed(true);

      expect(await manager.smartNote, equals(SmartNote.loginOffer));
    });

    test('.smartNote login offer when user is signed in', () async {
      _stubUserSignedIn(true);
      _stubPostponedLoginOfferWindowHasPassed(true);

      expect(await manager.smartNote, isNot(equals(SmartNote.loginOffer)));
    });

    test('.smartNote login offer dismissed', () async {
      _stubUserSignedIn(false);
      _stubPostponedLoginOfferWindowHasPassed(false);

      expect(await manager.smartNote, isNot(equals(SmartNote.loginOffer)));
    });

    test('.smartNote missed work', () async {
      _stubHasOverdueEvents(true);

      expect(await manager.smartNote, equals(SmartNote.missedWork));
    });

    test('.onPostponePendingBillingNote disables note', () async {
      _stubHasPendingPurchases(true);
      _stubPostponedPendingPurchaseWindowHasPassed(true);
      expect(await manager.smartNote, equals(SmartNote.pendingBilling));

      manager.onPostponePendingBillingNote();
      expect(await manager.smartNote, isNot(equals(SmartNote.pendingBilling)));
    });

    test('.onPostponeLoginOfferNote disables note', () async {
      _stubUserSignedIn(false);
      _stubPostponedLoginOfferWindowHasPassed(true);

      expect(await manager.smartNote, equals(SmartNote.loginOffer));

      manager.onPostponeLoginOfferNote();
      expect(await manager.smartNote, isNot(equals(SmartNote.loginOffer)));
    });
  });
}
