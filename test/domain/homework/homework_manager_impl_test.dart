import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/homework/homework_listener.dart';
import 'package:school_pen/domain/homework/homework_manager_impl.dart';
import 'package:school_pen/domain/homework/model/add_homework.dart';
import 'package:school_pen/domain/homework/model/homework.dart';
import 'package:school_pen/domain/homework/notification/homework_notification_builder.dart';
import 'package:school_pen/domain/homework/repository/homework_repository.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:test/test.dart';

class TestHomeworkRepository extends Mock implements HomeworkRepository {}

class TestNotificationManager extends Mock implements NotificationManager {}

class TestHomeworkNotificationBuilder extends Mock implements HomeworkNotificationBuilder {}

class TestHomeworkListener extends Mock implements HomeworkListener {}

class TestSemesterManager extends Mock implements SemesterManager {}

class TestCourseManager extends Mock implements CourseManager {}

class TestGradeManager extends Mock implements GradeManager {}

void main() {
  group('HomeworkManagerImpl', () {
    HomeworkManagerImpl manager;

    final HomeworkRepository repository = TestHomeworkRepository();
    final NotificationManager notifications = TestNotificationManager();
    final HomeworkNotificationBuilder notificationBuilder = TestHomeworkNotificationBuilder();
    final HomeworkListener homeworkListener = TestHomeworkListener();
    final SemesterManager semesterManager = TestSemesterManager();
    final CourseManager courseManager = TestCourseManager();
    final GradeManager gradeManager = TestGradeManager();
    final PublishSubject<HomeworkRepository> onChange = PublishSubject(sync: true);

    final DateTime now = DateTime.now();
    final String homeworkId = '0721495327';
    final Homework archivedHomework = _createHomework(updatedAt: now, isArchived: true);
    final Homework notArchived = _createHomework(updatedAt: now.minusDays(1), isArchived: false);

    setUp(() {
      when(repository.onChange).thenAnswer((_) => onChange);
      when(repository.getAll()).thenAnswer((_) async => [archivedHomework, notArchived]);
      when(repository.getById(any)).thenAnswer((invocation) async {
        final String id = invocation.positionalArguments.first;
        return [archivedHomework, notArchived].firstWhereOrNull((e) => e.id == id);
      });

      manager = HomeworkManagerImpl(
        repository,
        notifications,
        notificationBuilder,
        semesterManager,
        courseManager,
        gradeManager,
      );

      manager.addHomeworkListener(homeworkListener);
    });

    tearDown(() {
      reset(repository);
      reset(notifications);
      reset(notificationBuilder);
      reset(homeworkListener);
      reset(semesterManager);
      reset(courseManager);
      reset(gradeManager);
    });

    test('getAll() returns sorted homework', () async {
      expect(await manager.getAll(), equals([archivedHomework, notArchived]));
    });

    test('getAll() filters by semesterId', () async {
      final winter = _createHomework(semesterId: 'winter');
      when(repository.getAll(semesterId: winter.semesterId)).thenAnswer((_) async => [winter]);
      expect(await manager.getAll(semesterId: winter.semesterId), equals([winter]));
    });

    test('getById()', () async {
      expect(await manager.getById(archivedHomework.id), equals(archivedHomework));
      expect(await manager.getById('I do not exist'), isNull);
      expect(await manager.getById(null), isNull);
    });

    test('create()', () async {
      var homework = _createAddHomework();

      when(repository.create(homework)).thenAnswer((_) async => homeworkId);

      expect(await manager.create(homework), equals(homeworkId));
      onChange.add(repository);

      verifyInOrder([
        repository.create(homework),
        homeworkListener.onHomeworkChanged(manager),
      ]);
    });

    test('update()', () async {
      var homework = _createAddHomework();

      await manager.update(homeworkId, homework);
      onChange.add(repository);

      verifyInOrder([
        repository.update(homeworkId, homework),
        homeworkListener.onHomeworkChanged(manager),
      ]);
    });

    test('delete()', () async {
      final homework = _createHomework();
      when(repository.getById(homework.id)).thenAnswer((_) async => homework);

      await manager.delete(homework.id);
      verify(repository.delete(homework.id));
      verify(homeworkListener.onHomeworkChanged(manager));
    });

    test('onCleanupSemester', () async {
      final winter = _createHomework(semesterId: 'winter');
      final summer = _createHomework(semesterId: 'summer');

      when(repository.getAll(semesterId: winter.semesterId)).thenAnswer((_) async => [winter]);
      await manager.onCleanupSemester(winter.semesterId);

      verify(repository.delete(winter.id));
      verifyNever(repository.update(summer.id, any));
    });

    test('onCleanupCourse', () async {
      final math = _createHomework(courseId: 'math');
      final english = _createHomework(courseId: 'polish');

      when(repository.getAll(courseId: math.courseId)).thenAnswer((_) async => [math]);
      await manager.onCleanupCourse(math.courseId);

      verify(repository.delete(math.id));
      verifyNever(repository.delete(english.id));
    });

    test('onCleanupGrade', () async {
      final gradeA = _createHomework(gradeId: 'A');
      final gradeB = _createHomework(gradeId: 'B');

      when(repository.getAll(gradeId: gradeA.gradeId)).thenAnswer((_) async => [gradeA]);
      await manager.onCleanupGrade(gradeA.gradeId);

      verify(repository.update(gradeA.id, gradeA.toAddHomework().copyWith(gradeId: Optional(null))));
      verifyNever(repository.update(gradeB.id, gradeB.toAddHomework().copyWith(gradeId: Optional(null))));
    });
  });
}

AddHomework _createAddHomework({String semesterId}) {
  return _createHomework(semesterId: semesterId).toAddHomework();
}

Homework _createHomework({
  String semesterId,
  String courseId,
  String gradeId,
  bool isArchived = false,
  DateTime updatedAt,
}) {
  return Homework(
    id: Id.random(),
    semesterId: semesterId ?? Id.random(),
    courseId: courseId ?? Id.random(),
    gradeId: gradeId ?? Id.random(),
    title: 'title',
    note: 'note',
    dateTime: DateTime.now(),
    isArchived: isArchived,
    updatedAt: updatedAt ?? DateTime.now(),
  );
}
