import 'package:school_pen/domain/note/model/note_item.dart';
import 'package:test/test.dart';

void main() {
  group('NoteItem', () {
    test('trim', () {
      var actual = NoteItem(id: 'id', text: '    text with spaces   \n', isCompleted: false).trim();
      var expected = NoteItem(id: 'id', text: 'text with spaces', isCompleted: false);
      expect(actual, equals(expected));
      expect(expected.trim(), equals(expected));
    });

    test('isEmpty', () {
      expect(NoteItem(id: 'id', text: '', isCompleted: false).isEmpty, isTrue);
      expect(NoteItem(id: 'id', text: '    ', isCompleted: false).isEmpty, isTrue);
      expect(NoteItem(id: 'id', text: 'Test', isCompleted: false).isEmpty, isFalse);
      expect(NoteItem(id: 'id', text: '', isCompleted: true).isEmpty, isTrue);
    });
  });
}
