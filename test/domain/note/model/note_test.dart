import 'package:school_pen/domain/common/date/date_constraints.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/model/note_item.dart';
import 'package:test/test.dart';

void main() {
  group('Note', () {
    final DateTime now = DateTime.now().withoutSeconds();
    final DateTime future = now.add(Duration(minutes: 1));
    final DateTime expired = now.subtract(Duration(minutes: 1));

    test('isNotificationExpired', () {
      var note = _createNote(notificationDate: now);

      expect(note.isNotificationExpired(now), isFalse);
      expect(note.isNotificationExpired(expired), isFalse);
      expect(note.isNotificationExpired(future), isTrue);

      note = _createNote(notificationDate: null);
      expect(note.isNotificationExpired(now.add(Duration(minutes: 1))), isFalse);
    });

    test('suggestedNotificationDate returns notification date if set', () {
      final note = _createNote(notificationDate: now.add(Duration(minutes: 5)));

      expect(note.suggestedNotificationDate(now), equals(note.notificationDate));
    });

    test('suggestedNotificationDate returns date relative to now', () {
      final note = _createNote(notificationDate: null);

      expect(note.suggestedNotificationDate(now), now.plusDays(1).withTime(Time(hour: 12, minute: 0)));
    });

    test('notificationConstraints returns notificationDate as minDate if it is before now', () {
      final note = _createNote(notificationDate: now.minusDays(366));

      expect(
          note.notificationConstraints(now: now, notificationDate: note.notificationDate),
          equals(DateConstraints(
            minimumDate: note.notificationDate,
            maximumDate: DateTime(2029, 12, 31, 23, 59),
          )));
    });

    test('notificationConstraints returns suggested date', () {
      final note = _createNote(notificationDate: now.plusDays(29));

      expect(
          note.notificationConstraints(now: now, notificationDate: note.notificationDate),
          equals(DateConstraints(
            minimumDate: now,
            maximumDate: DateTime(2029, 12, 31, 23, 59),
          )));
    });

    test('sorting order', () {
      final newer = _createNote(updatedAt: DateTime.now());
      final older = _createNote(updatedAt: DateTime.now().minusDays(1));

      final newerOlder = [newer, older];
      newerOlder.sort();
      expect(newerOlder, equals([newer, older]));

      final olderNewer = [older, newer];
      olderNewer.sort();
      expect(olderNewer, equals([newer, older]));
    });
  });
}

Note _createNote({
  String title = '',
  String note = '',
  List<NoteItem> items,
  bool isArchived,
  DateTime notificationDate,
  DateTime updatedAt,
}) {
  return Note(
    id: Id.random(),
    schoolYearId: Id.random(),
    title: title ?? 'title',
    note: note ?? 'note',
    items: items ?? [],
    notificationDate: notificationDate,
    isArchived: isArchived ?? false,
    updatedAt: updatedAt ?? DateTime.now(),
  );
}
