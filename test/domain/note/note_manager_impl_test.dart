import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/note/model/add_note.dart';
import 'package:school_pen/domain/note/model/note.dart';
import 'package:school_pen/domain/note/note_listener.dart';
import 'package:school_pen/domain/note/note_manager_impl.dart';
import 'package:school_pen/domain/note/notification/note_notification_builder.dart';
import 'package:school_pen/domain/note/repository/note_repository.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:test/test.dart';

class TestNoteRepository extends Mock implements NoteRepository {}

class TestNotificationManager extends Mock implements NotificationManager {}

class TestNoteNotificationBuilder extends Mock implements NoteNotificationBuilder {}

class TestNoteListener extends Mock implements NoteListener {}

class TestSchoolYearManager extends Mock implements SchoolYearManager {}

void main() {
  group('NoteManagerImpl', () {
    NoteManagerImpl manager;

    final NoteRepository repository = TestNoteRepository();
    final NotificationManager notifications = TestNotificationManager();
    final NoteNotificationBuilder notificationBuilder = TestNoteNotificationBuilder();
    final NoteListener noteListener = TestNoteListener();
    final SchoolYearManager schoolYearManager = TestSchoolYearManager();
    final PublishSubject<NoteRepository> onChange = PublishSubject(sync: true);

    final now = DateTime.now();
    final noteId = '0721495327';
    final archivedNote = _createNote(updatedAt: now, isArchived: true);
    final notArchived = _createNote(updatedAt: now.minusDays(1), isArchived: false);

    setUp(() {
      when(repository.onChange).thenAnswer((_) => onChange);
      when(repository.getAll()).thenAnswer((_) async => [archivedNote, notArchived]);
      when(repository.getById(any)).thenAnswer((invocation) async {
        final String id = invocation.positionalArguments.first;
        return [archivedNote, notArchived].firstWhereOrNull((e) => e.id == id);
      });

      manager = NoteManagerImpl(
        repository,
        notifications,
        notificationBuilder,
        schoolYearManager,
      );

      manager.addNoteListener(noteListener);
    });

    tearDown(() {
      reset(repository);
      reset(notifications);
      reset(notificationBuilder);
      reset(noteListener);
      reset(schoolYearManager);
    });

    test('getAll() returns reversed notes', () async {
      expect(await manager.getAll(), equals([notArchived, archivedNote]));
    });

    test('getAll() filters by schoolYearId', () async {
      final winter = _createNote(schoolYearId: 'winter');
      when(repository.getAll(winter.schoolYearId)).thenAnswer((_) async => [winter]);
      expect(await manager.getAll(winter.schoolYearId), equals([winter]));
    });

    test('getById()', () async {
      expect(await manager.getById(archivedNote.id), equals(archivedNote));
      expect(await manager.getById('I do not exist'), isNull);
      expect(await manager.getById(null), isNull);
    });

    test('create()', () async {
      var notificationDate = now.add(Duration(hours: 2));
      var note = _createAddNote(notificationDate: notificationDate);

      when(repository.create(note)).thenAnswer((_) async => noteId);

      expect(await manager.create(note), equals(noteId));
      onChange.add(repository);

      verifyInOrder([
        repository.create(note),
        noteListener.onNotesChanged(manager),
      ]);
    });

    test('update()', () async {
      var notificationDate = now.add(Duration(hours: 2));
      var note = _createAddNote(notificationDate: notificationDate);

      await manager.update(noteId, note);
      onChange.add(repository);

      verifyInOrder([
        repository.update(noteId, note),
        noteListener.onNotesChanged(manager),
      ]);
    });

    test('delete()', () async {
      final note = _createNote();
      when(repository.getById(note.id)).thenAnswer((_) async => note);

      await manager.delete(note.id);
      verify(repository.delete(note.id));
      verify(noteListener.onNotesChanged(manager));
    });

    test('onCleanupSchoolYear', () async {
      final winter = _createNote(schoolYearId: 'winter');
      final summer = _createNote(schoolYearId: 'summer');

      when(repository.getAll(winter.schoolYearId)).thenAnswer((_) async => [winter]);
      await manager.onCleanupSchoolYear(winter.schoolYearId);

      verify(repository.delete(winter.id));
      verifyNever(repository.update(summer.id, any));
    });
  });
}

AddNote _createAddNote({
  String schoolYearId,
  DateTime notificationDate,
}) {
  return _createNote(
    schoolYearId: schoolYearId,
    notificationDate: notificationDate,
  ).toAddNote();
}

Note _createNote({
  String schoolYearId,
  DateTime notificationDate,
  bool isArchived = false,
  DateTime updatedAt,
}) {
  return Note(
    id: Id.random(),
    schoolYearId: schoolYearId ?? Id.random(),
    title: 'title',
    note: 'note',
    items: [],
    isArchived: isArchived,
    notificationDate: notificationDate,
    updatedAt: updatedAt ?? DateTime.now(),
  );
}
