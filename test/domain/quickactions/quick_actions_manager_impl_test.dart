import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/preferences/impl/in_memory_preferences.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/quickactions/model/quick_actions.dart';
import 'package:school_pen/domain/quickactions/model/quick_actions_instance.dart';
import 'package:school_pen/domain/quickactions/quick_actions_listener.dart';
import 'package:school_pen/domain/quickactions/quick_actions_manager_impl.dart';
import 'package:school_pen/domain/quickactions/repository/quick_actions_history.dart';
import 'package:school_pen/domain/quickactions/repository/quick_actions_repository.dart';
import 'package:test/test.dart';

class TestLifecycleManager extends Mock implements LifecycleManager {}

class TestQuickActionsRepository extends Mock implements QuickActionsRepository {}

class TestQuickActionsHistory extends Mock implements QuickActionsHistory {}

class TestQuickActionsListener extends Mock implements QuickActionsListener {}

void main() {
  group('QuickActionsManagerImpl', () {
    QuickActionsManagerImpl manager;

    final LifecycleManager lifecycle = TestLifecycleManager();
    final QuickActionsRepository repository = TestQuickActionsRepository();
    final QuickActionsHistory history = TestQuickActionsHistory();
    final Preferences preferences = InMemoryPreferences();
    final QuickActionsListener listener = TestQuickActionsListener();
    final PublishSubject<QuickActionInstance> actionsSubject = PublishSubject();

    setUp(() {
      when(repository.actions).thenAnswer((_) => actionsSubject.stream);

      manager = QuickActionsManagerImpl(lifecycle, repository, history, preferences);
      manager.addQuickActionsListener(listener);
    });

    tearDown(() {
      reset(lifecycle);
      reset(repository);
      reset(history);
      preferences.clearAll();
    });

    test('handleQuickActionInstance() skips already consumed action', () async {
      final QuickActionInstance action = QuickActionInstance(QuickAction.addExam, '1234');
      when(history.consumeQuickAction(action.id)).thenAnswer((_) async => false);

      await manager.handleQuickActionInstance(action);
      verifyNever(listener.onQuickAction(action.type));
    });

    test('handleQuickActionInstance() dispatches not consumed action', () async {
      final QuickActionInstance action = QuickActionInstance(QuickAction.addExam, '5678');
      when(history.consumeQuickAction(action.id)).thenAnswer((_) async => true);

      await manager.handleQuickActionInstance(action);
      verify(listener.onQuickAction(action.type));
    });
  });
}
