import 'package:mockito/mockito.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/preferences/impl/in_memory_preferences.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/rateapp/rate_app_manager_impl.dart';
import 'package:test/test.dart';

class TestDateTimeProvider extends Mock implements DateTimeProvider {}

void main() {
  group('RateAppManager', () {
    RateAppManagerImpl rateAppManager;

    final ForcedSlidingWindow slidingWindow = ForcedSlidingWindow();
    final Preferences preferences = InMemoryPreferences();

    void _stubAppWasRated(bool wasRated) {
      preferences.setBool(RateAppManagerImpl.keyWasAppRated, wasRated);
    }

    void _stubRateAppInitialWindowPassed(bool passed) {
      slidingWindow.forcePassed(
        key: RateAppManagerImpl.keyRateAppInitialWindow,
        passed: passed,
      );
    }

    setUp(() {
      rateAppManager = RateAppManagerImpl(slidingWindow, preferences);
    });

    tearDown(() {
      preferences.clearAll();
    });

    test('shouldRateNow returns false if already rated', () {
      _stubAppWasRated(true);
      _stubRateAppInitialWindowPassed(true);

      expect(rateAppManager.shouldRateNow, isFalse);
    });

    test('shouldRateNow returns false if initial delay did not pass', () {
      _stubAppWasRated(false);
      _stubRateAppInitialWindowPassed(false);

      expect(rateAppManager.shouldRateNow, isFalse);
    });

    test('shouldRateNow returns true', () {
      _stubAppWasRated(false);
      _stubRateAppInitialWindowPassed(true);

      expect(rateAppManager.shouldRateNow, isTrue);
    });

    test('onAppRated() stores flag and disallows more ratings', () {
      _stubAppWasRated(false);
      _stubRateAppInitialWindowPassed(true);
      expect(rateAppManager.shouldRateNow, isTrue);

      rateAppManager.onAppRated();
      expect(rateAppManager.shouldRateNow, isFalse);
    });

    test('shouldRateNow returns false if dismissed', () {
      _stubAppWasRated(false);
      _stubRateAppInitialWindowPassed(true);
      expect(rateAppManager.shouldRateNow, isTrue);

      rateAppManager.onRateAppDismissed();
      expect(rateAppManager.shouldRateNow, isFalse);
    });
  });
}
