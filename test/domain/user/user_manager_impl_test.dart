import 'dart:convert';
import 'dart:typed_data';

import 'package:mockito/mockito.dart';
import 'package:school_pen/domain/common/files.dart';
import 'package:school_pen/domain/common/image_resizer.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/files/model/memory_file.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/preferences/impl/in_memory_preferences.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/user/exception/avatar_size_too_big_exception.dart';
import 'package:school_pen/domain/user/exception/invalid_email_exception.dart';
import 'package:school_pen/domain/user/exception/password_cannot_contain_username_exception.dart';
import 'package:school_pen/domain/user/exception/password_too_short_exception.dart';
import 'package:school_pen/domain/user/exception/username_too_short_exception.dart';
import 'package:school_pen/domain/user/model/auth_provider.dart';
import 'package:school_pen/domain/user/model/auth_result.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/repository/user_repository.dart';
import 'package:school_pen/domain/user/user_login_listener.dart';
import 'package:school_pen/domain/user/user_manager_impl.dart';
import 'package:test/test.dart';

class TestUserRepository extends Mock implements UserRepository {}

class TestUserLoginListener extends Mock implements UserLoginListener {}

class TestLifecycleManager extends Mock implements LifecycleManager {}

class TestImageResizer extends Mock implements ImageResizer {}

void main() {
  group('UserManagerImpl', () {
    UserManagerImpl manager;

    final UserRepository repository = TestUserRepository();
    final Preferences preferences = InMemoryPreferences();
    final LifecycleManager lifecycle = TestLifecycleManager();
    final ForcedSlidingWindow slidingWindow = ForcedSlidingWindow();
    final ImageResizer imageResizer = TestImageResizer();
    final UserLoginListener listener = TestUserLoginListener();

    final String username = 'user';
    final String tooShortUsername = 'A';
    final String email = 'user@domain.com';
    final String invalidEmail = 'invalid email';
    final String password = 'Test1234!';
    final String newPassword = 'new-password';
    final String tooShortPassword = 'test';
    final MemoryFile avatar = MemoryFile(name: 'avatar', bytes: Uint8List.fromList([0, 1, 2]));
    final MemoryFile resizedAvatar = avatar.copyWith(bytes: Optional(Uint8List.fromList([1, 2])));
    final MemoryFile tooBigAvatar = MemoryFile(
      name: 'avatar',
      bytes: Uint8List.fromList(
        List.filled(Files.megabytesToBytes(UserManagerImpl.avatarFileSizeLimitInMB).toInt() + 1, 0),
      ),
    );

    final List<AuthProvider> availableAuthProviders = [AuthProvider.facebook, AuthProvider.apple];
    final AuthProvider authProvider = AuthProvider.apple;
    final dynamic authData = '{token: 1234}';
    final AuthResult authResult = AuthResult(
      provider: authProvider,
      email: email,
      authData: authData,
    );

    final User userNoEmail = User(id: 'test', username: username, email: null);
    final User user = User(id: 'test', username: username, email: email);
    final User linkedUser = User(id: 'test', username: username, email: email, linkedProviders: [authProvider]);

    void _stubSignedUser(User user) {
      when(repository.getCurrentUser()).thenAnswer((_) async => user);
      if (user == null) {
        preferences.clear(UserManagerImpl.key_storedUserMetadata);
      } else {
        preferences.setString(UserManagerImpl.key_storedUserMetadata, json.encode(user.toMetadata().toJson()));
      }
    }

    void _stubRefreshedUser(User user) {
      when(repository.refreshCurrentUser()).thenAnswer((_) async => user);
    }

    void _stubRefreshUserWindowPassed(bool passed) {
      slidingWindow.forcePassed(key: UserManagerImpl.key_refreshUser_slidingWindow, passed: passed);
    }

    setUp(() {
      when(repository.getAvailableAuthProviders()).thenAnswer((_) async => availableAuthProviders);
      when(imageResizer.cropSquareAndResize(avatar, UserManagerImpl.avatarSize)).thenAnswer((_) async => resizedAvatar);

      manager = UserManagerImpl(repository, preferences, lifecycle, slidingWindow, imageResizer);
      manager.addUserLoginListener(listener);
      reset(listener);
    });

    tearDown(() {
      reset(repository);
      reset(listener);
      reset(lifecycle);
      reset(imageResizer);
      preferences.clearAll();
      slidingWindow.reset(key: UserManagerImpl.key_refreshUser_slidingWindow);
    });

    test('getCurrentUser', () async {
      _stubSignedUser(user);
      expect(await manager.getCurrentUser(), equals(user));
    });

    test('refresh', () async {
      _stubSignedUser(user);
      _stubRefreshedUser(linkedUser);

      expect(await manager.refresh(), linkedUser);
    });

    test('signUp', () async {
      _stubSignedUser(null);
      _stubRefreshedUser(user);

      await manager.signUp(username: username, email: email, password: password);
      verify(repository.signUp(username: username, email: email, password: password));
      verify(listener.onUserSignedIn(user.toMetadata()));
    });

    test('signUp too short username', () async {
      expect(
        () async => await manager.signUp(username: tooShortUsername, email: email, password: password),
        throwsA(isA<UsernameTooShortException>()),
      );

      verifyNever(repository.signUp(username: tooShortUsername, email: email, password: password));
    });

    test('signUp invalid email', () async {
      expect(
        () async => await manager.signUp(username: username, email: invalidEmail, password: password),
        throwsA(isA<InvalidEmailException>()),
      );

      verifyNever(repository.signUp(username: username, email: invalidEmail, password: password));
    });

    test('signUp username in password', () async {
      expect(
        () async => await manager.signUp(username: username, email: email, password: username),
        throwsA(isA<PasswordCannotContainUsernameException>()),
      );

      verifyNever(repository.signUp(username: username, email: email, password: username));
    });

    test('signUp too short password', () async {
      expect(
        () async => await manager.signUp(username: username, email: email, password: tooShortPassword),
        throwsA(isA<PasswordTooShortException>()),
      );

      verifyNever(repository.signUp(username: username, email: email, password: tooShortPassword));
    });

    test('signIn', () async {
      _stubSignedUser(null);
      _stubRefreshedUser(user);

      await manager.signIn(email: email, password: password);
      verify(repository.signIn(email: email, password: password));
      verify(listener.onUserSignedIn(user.toMetadata()));
    });

    test('signIn invalid email', () async {
      expect(
        () async => await manager.signIn(email: invalidEmail, password: password),
        throwsA(isA<InvalidEmailException>()),
      );

      verifyNever(repository.signIn(email: invalidEmail, password: password));
      verifyNever(listener.onUserSignedIn(any));
    });

    test('signInWith for the first time', () async {
      _stubSignedUser(null);
      _stubRefreshedUser(userNoEmail);

      await manager.signInWith(authResult);
      verify(repository.signInWith(provider: authResult.provider, authData: authResult.authData));
      verify(repository.editProfile(username: null, email: authResult.email));
    });

    test('signInWith for next time', () async {
      _stubSignedUser(null);
      _stubRefreshedUser(user);

      await manager.signInWith(authResult);
      verify(repository.signInWith(provider: authResult.provider, authData: authResult.authData));
      verifyNever(repository.editProfile(username: null, email: authResult.email));
    });

    test('linkWith', () async {
      _stubSignedUser(user);
      _stubRefreshedUser(linkedUser);

      await manager.linkWith(authResult);
      verify(repository.linkWith(provider: authResult.provider, authData: authResult.authData));
    });

    test('unlinkFrom', () async {
      _stubSignedUser(linkedUser);
      _stubRefreshedUser(user);

      await manager.unlinkFrom(authResult.provider);
      verify(repository.unlinkFrom(authResult.provider));
    });

    test('signOut', () async {
      _stubSignedUser(user);
      _stubRefreshedUser(null);

      await manager.signOut();
      verify(repository.signOut());
      verify(listener.onUserSignedOut(user.toMetadata()));
    });

    test('editProfile', () async {
      _stubSignedUser(user);

      await manager.editProfile(username: username, email: email);
      verify(repository.editProfile(username: username, email: email));
    });

    test('editProfile too short username', () async {
      expect(
        () async => await manager.editProfile(username: tooShortUsername, email: email),
        throwsA(isA<UsernameTooShortException>()),
      );

      verifyNever(repository.editProfile(username: tooShortUsername, email: email));
    });

    test('editProfile invalid email', () async {
      expect(
        () async => await manager.editProfile(username: username, email: invalidEmail),
        throwsA(isA<InvalidEmailException>()),
      );

      verifyNever(repository.editProfile(username: username, email: invalidEmail));
    });

    test('editAvatar', () async {
      await manager.editAvatar(file: avatar);
      verify(repository.editAvatar(file: resizedAvatar));
    });

    test('editAvatar - too big', () async {
      expect(() => manager.editAvatar(file: tooBigAvatar), throwsA(isA<AvatarSizeTooBigException>()));
    });

    test('editAvatar - delete', () async {
      await manager.editAvatar(file: null);
      verify(repository.editAvatar(file: null));
    });

    test('changePassword', () async {
      await manager.changePassword(oldPassword: password, newPassword: newPassword);
      verify(repository.changePassword(oldPassword: password, newPassword: newPassword));
    });

    test('changePassword too short password', () async {
      expect(
        () async => await manager.changePassword(oldPassword: password, newPassword: tooShortPassword),
        throwsA(isA<PasswordTooShortException>()),
      );

      verifyNever(repository.changePassword(oldPassword: password, newPassword: tooShortPassword));
    });

    test('deleteUser', () async {
      _stubSignedUser(user);
      _stubRefreshedUser(null);

      await manager.deleteUser();
      verify(repository.deleteUser());
      verify(listener.onUserSignedOut(user.toMetadata()));
    });

    test('requestPasswordReset', () async {
      await manager.requestPasswordReset(email: email);
      verify(repository.requestPasswordReset(email: email));
    });

    test('requestPasswordReset invalid email', () async {
      expect(
        () async => await manager.requestPasswordReset(email: invalidEmail),
        throwsA(isA<InvalidEmailException>()),
      );
      verifyNever(repository.requestPasswordReset(email: invalidEmail));
    });

    test('getAvailableAuthProviders', () async {
      expect(await manager.getAvailableAuthProviders(), equals(availableAuthProviders));
    });

    test('consumeAuthError', () async {
      dynamic error = Exception('auth');
      when(repository.isSessionInvalidError(error)).thenReturn(true);

      await manager.consumeAuthError(error);
      verify(repository.refreshCurrentUser());
    });

    test('consumeAuthError ignores non-auth errors', () async {
      dynamic error = Exception('io');
      when(repository.isSessionInvalidError(error)).thenReturn(false);

      await manager.consumeAuthError(error);
      verifyNever(repository.refreshCurrentUser());
    });

    test('resume refreshes user', () async {
      _stubRefreshUserWindowPassed(true);

      await manager.onResume();
      verify(repository.refreshCurrentUser());
    });

    test('resume does not refresh user if window did not pass', () async {
      _stubRefreshUserWindowPassed(false);

      await manager.onResume();
      verifyNever(repository.refreshCurrentUser());
    });
  });
}
