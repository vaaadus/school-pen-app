import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/course/course_cleanup_listener.dart';
import 'package:school_pen/domain/course/course_listener.dart';
import 'package:school_pen/domain/course/course_manager_impl.dart';
import 'package:school_pen/domain/course/model/add_course.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/course/repository/course_repository.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/teacher/teacher_manager.dart';
import 'package:test/test.dart';

class TestCourseRepository extends Mock implements CourseRepository {}

class TestSchoolYearManager extends Mock implements SchoolYearManager {}

class TestTeacherManager extends Mock implements TeacherManager {}

class TestCourseListener extends Mock implements CourseListener {}

class TestCourseCleanupListener extends Mock implements CourseCleanupListener {}

void main() {
  group('CourseManagerImpl', () {
    CourseManagerImpl manager;

    final CourseRepository repository = TestCourseRepository();
    final SchoolYearManager schoolYearManager = TestSchoolYearManager();
    final TeacherManager teacherManager = TestTeacherManager();
    final CourseListener listener = TestCourseListener();
    final CourseCleanupListener cleanupListener = TestCourseCleanupListener();
    final PublishSubject<CourseRepository> onChange = PublishSubject(sync: true);
    final PublishSubject<String> onCleanupCourse = PublishSubject(sync: true);

    final String mathTeacher = 'mathTeacher';
    final String otherTeacher = 'otherTeacher';
    final Course math = _createCourse(name: 'math', teachers: [mathTeacher, otherTeacher]);
    final AddCourse mathWithoutTeacher = math.toAddCourse().copyWith(teachersIds: Optional([otherTeacher]));
    final Course english = _createCourse(name: 'english', teachers: [otherTeacher]);

    setUp(() {
      when(repository.onChange).thenAnswer((_) => onChange);
      when(repository.onCleanupCourse).thenAnswer((_) => onCleanupCourse);
      when(repository.getAll()).thenAnswer((_) async => [math, english]);
      when(repository.getById(any)).thenAnswer((invocation) async {
        final String id = invocation.positionalArguments.first;
        return [math, english].firstWhereOrNull((e) => e.id == id);
      });

      manager = CourseManagerImpl(repository, schoolYearManager, teacherManager);
      manager.addCourseListener(listener);
      manager.addCourseCleanupListener(cleanupListener);

      reset(listener);
      reset(cleanupListener);
    });

    tearDown(() {
      reset(repository);
      reset(schoolYearManager);
      reset(teacherManager);
      reset(listener);
      reset(cleanupListener);
    });

    test('getAll()', () async {
      expect(await manager.getAll(), equals([math, english]));
    });

    test('getAllByTeacherId()', () async {
      when(repository.getAll(teachersIds: [mathTeacher])).thenAnswer((_) async => [math]);
      when(repository.getAll(teachersIds: [otherTeacher])).thenAnswer((_) async => [math, english]);
      expect(await manager.getAllByTeacherId(mathTeacher), equals([math]));
      expect(await manager.getAllByTeacherId(otherTeacher), equals([math, english]));
    });

    test('getById()', () async {
      expect(await manager.getById(english.id), equals(english));
    });

    test('create()', () async {
      final AddCourse course = math.toAddCourse();
      await manager.create(course);
      onChange.add(repository);

      verify(repository.create(course));
      verify(listener.onCoursesChanged(manager));
    });

    test('update()', () async {
      final AddCourse course = _createCourse(name: 'edit').toAddCourse();
      await manager.update(english.id, course);
      onChange.add(repository);

      verify(repository.update(english.id, course));
      verify(listener.onCoursesChanged(manager));
    });

    test('delete()', () async {
      await manager.delete(math.id);
      onChange.add(repository);
      onCleanupCourse.add(math.id);

      verify(repository.delete(math.id));
      verify(listener.onCoursesChanged(manager));
      verify(cleanupListener.onCleanupCourse(math.id));
    });

    test('onCleanupSchoolYear()', () async {
      when(repository.getAll(schoolYearId: math.schoolYearId)).thenAnswer((_) async => [math]);
      await manager.onCleanupSchoolYear(math.schoolYearId);
      onChange.add(repository);

      verify(repository.delete(math.id));
      verifyNever(repository.delete(english.id));
      verify(listener.onCoursesChanged(manager));
    });

    test('onCleanupTeacher()', () async {
      when(repository.getAll(teachersIds: [math.teachersIds.first])).thenAnswer((_) async => [math]);
      await manager.onCleanupTeacher(math.teachersIds.first);
      onChange.add(repository);

      verify(repository.update(math.id, mathWithoutTeacher));
      verify(listener.onCoursesChanged(manager));
    });
  });
}

Course _createCourse({String name, List<String> teachers}) {
  return Course(
    id: Id.random(),
    schoolYearId: Id.random(),
    name: name ?? '',
    teachersIds: teachers ?? [],
    updatedAt: DateTime.now(),
  );
}
