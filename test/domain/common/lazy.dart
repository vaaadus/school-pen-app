import 'package:school_pen/domain/common/lazy.dart';
import 'package:test/test.dart';

void main() {
  group('LazyFuture', () {
    LazyFuture<bool> lazy;
    int callCount;

    setUp(() {
      callCount = 0;
      lazy = LazyFuture(() async {
        callCount++;
        return true;
      });
    });

    test('lazy does not initialize eagerly', () async {
      expect(callCount, equals(0));
    });

    test('get called once', () async {
      expect(await lazy.get(), isTrue);
      expect(callCount, equals(1));
    });

    test('get does not call callback twice', () async {
      expect(await lazy.get(), isTrue);
      expect(await lazy.get(), isTrue);

      expect(callCount, equals(1));
    });
  });
}
