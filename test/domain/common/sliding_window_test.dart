import 'package:mockito/mockito.dart';
import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/preferences/impl/in_memory_preferences.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:test/test.dart';

class TestDateTimeProvider extends Mock implements DateTimeProvider {}

void main() {
  group('SlidingWindow', () {
    PersistableSlidingWindow slidingWindow;

    final Preferences preferences = InMemoryPreferences();
    final DateTimeProvider dateTimeProvider = TestDateTimeProvider();

    final String key = 'key';
    final DateTime now = DateTime.now();
    final Duration initialWindow = Duration(hours: 8);
    final Duration nextWindow = Duration(hours: 5);

    void _stubInitialWindowNotPassed() {
      preferences.setDateTime(PersistableSlidingWindow.key_firstLaunch_ts, now);
    }

    void _stubInitialWindowPassed() {
      final Duration duration = Duration(hours: initialWindow.inHours + 1);
      preferences.setDateTime(PersistableSlidingWindow.key_firstLaunch_ts, now.subtract(duration));
    }

    void _stubFirstWindowNeverActivated() {
      preferences.setDateTime(key, null);
    }

    void _stubLastWindowWasRecently() {
      final Duration duration = Duration(hours: nextWindow.inHours - 1);
      preferences.setDateTime(key, now.subtract(duration));
    }

    void _stubLastWindowWasLongTimeAgo() {
      final Duration duration = Duration(hours: nextWindow.inHours + 1);
      preferences.setDateTime(key, now.subtract(duration));
    }

    setUp(() {
      when(dateTimeProvider.now()).thenReturn(now);
      slidingWindow = PersistableSlidingWindow(preferences, dateTimeProvider);
    });

    tearDown(() {
      preferences.clearAll();
      reset(dateTimeProvider);
    });

    test('isPassed when initial window has not passed', () {
      _stubInitialWindowNotPassed();

      final bool actual = slidingWindow.isPassed(key: key, initialWindow: initialWindow);

      expect(actual, isFalse);
    });

    test('isPassed when initial window has passed for the first time returns initial value', () {
      _stubInitialWindowPassed();
      _stubFirstWindowNeverActivated();

      final bool actual = slidingWindow.isPassed(key: key, initialWindow: initialWindow);

      expect(actual, isTrue);
    });

    test('isPassed when last window has not passed', () {
      _stubInitialWindowPassed();
      _stubLastWindowWasRecently();

      final bool actual = slidingWindow.isPassed(key: key, initialWindow: initialWindow, nextWindow: nextWindow);

      expect(actual, isFalse);
    });

    test('isActive when last window has passed', () {
      _stubInitialWindowPassed();
      _stubLastWindowWasLongTimeAgo();

      final bool actual = slidingWindow.isPassed(key: key, initialWindow: initialWindow, nextWindow: nextWindow);

      expect(actual, isTrue);
    });

    test('restart & reset sliding window', () {
      _stubInitialWindowPassed();
      _stubLastWindowWasLongTimeAgo();

      final bool first = slidingWindow.isPassed(key: key, initialWindow: initialWindow, nextWindow: nextWindow);
      expect(first, isTrue);

      slidingWindow.restart(key: key);
      final bool second = slidingWindow.isPassed(key: key, initialWindow: initialWindow, nextWindow: nextWindow);
      expect(second, isFalse);

      slidingWindow.reset(key: key);
      final bool third = slidingWindow.isPassed(key: key, initialWindow: initialWindow, nextWindow: nextWindow);
      expect(third, isTrue);
    });
  });
}
