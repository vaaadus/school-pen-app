import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:test/test.dart';

void main() {
  group('Lists', () {
    test('getOrNull', () {
      expect([1, 2, 3].getOrNull(2), equals(3));
      expect([1, 2, 3].getOrNull(5), isNull);
    });

    test('firstOrNull', () {
      expect([].firstOrNull, isNull);
      expect([1, 2, 3].firstOrNull, equals(1));
    });

    test('firstWhereOrNull', () {
      expect([].firstWhereOrNull((e) => e == 1), isNull);
      expect([0, 1, 2, 3].firstWhereOrNull((e) => e == 1), equals(1));
    });

    test('addIfAbsentAll', () {
      var list = [1, 2, 3];
      expect(list.addIfAbsentAll([4, 5]), isTrue);
      expect(list, equals([1, 2, 3, 4, 5]));

      expect(list.addIfAbsentAll([2, 4, 6]), isTrue);
      expect(list, equals([1, 2, 3, 4, 5, 6]));

      expect(list.addIfAbsentAll([2, 5]), isFalse);
      expect(list, equals([1, 2, 3, 4, 5, 6]));
    });

    test('addIfAbsent', () {
      var list = [1, 2, 3];
      expect(list.addIfAbsent(4), isTrue);
      expect(list, equals([1, 2, 3, 4]));

      expect(list.addIfAbsent(4), isFalse);
      expect(list, equals([1, 2, 3, 4]));
    });

    test('sorted', () {
      expect([].sorted((a, b) => a - b), equals([]));
      expect([1, 2, 3].sorted((a, b) => a - b), equals([1, 2, 3]));
      expect([1, 2, 3].sorted((a, b) => b - a), equals([3, 2, 1]));
    });

    test('mapIndexed', () {
      expect([1, 2, 3].mapIndexed((e, index) => e * index), equals([0, 2, 6]));
    });

    test('none', () {
      expect([1, 2, 3].none((e) => e == 4), isTrue);
      expect([1, 2, 3].none((e) => e == 3), isFalse);
    });

    test('indicesWhere', () {
      expect([1, 2, 3, 4, 5, 6, 7, 8, 9].indicesWhere((e) => e % 2 == 0), equals([1, 3, 5, 7]));
      expect([1, 2, 3, 4, 5, 6, 7, 8, 9].indicesWhere((e) => e % 2 != 0), equals([0, 2, 4, 6, 8]));
    });

    test('plus', () {
      expect([1, 2, 3].plus(4), equals([1, 2, 3, 4]));
    });

    test('minus', () {
      expect([1, 2, 3].minus(3), equals([1, 2]));
      expect([1, 2, 3].minus(5), equals([1, 2, 3]));
    });

    test('minusAll', () {
      expect([1, 2, 3].minusAll([2, 3]), equals([1]));
      expect([1, 2, 3].minusAll([3, 5]), equals([1, 2]));
    });

    test('skipDuplicates', () {
      expect([1, 2, 3].skipDuplicates(), equals([1, 2, 3]));
      expect([1, 2, 3, 3, 5, 5].skipDuplicates(), equals([1, 2, 3, 5]));
    });
  });

  group('ComparableIterable', () {
    test('minOrNull', () {
      final List<num> nums = [1, 2, 3];
      final List<num> emptyNums = [];
      expect(nums.minOrNull(), equals(1));
      expect(emptyNums.minOrNull(), isNull);
    });

    test('maxOrNull', () {
      final List<num> nums = [1, 2, 3];
      final List<num> emptyNums = [];
      expect(nums.maxOrNull(), equals(3));
      expect(emptyNums.minOrNull(), isNull);
    });
  });

  group('Iterables', () {
    test('withoutNulls', () {
      expect([1, 2, 3].withoutNulls(), equals([1, 2, 3]));
      expect([1, 2, null, 3, null].withoutNulls(), equals([1, 2, 3]));
    });
  });
}
