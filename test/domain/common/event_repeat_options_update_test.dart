import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:test/test.dart';

void main() {
  group('EventRepeatOptionsUpdate getAllowedUpdateModes', () {
    final DateTimeRange dateRange = DateTimeRange(
      start: DateTime.utc(2020, 1, 1, 6, 0),
      end: DateTime.utc(2020, 1, 1, 10, 0),
    );

    final EventRepeatOptions single = EventRepeatOptions.single(dateRange);
    final EventRepeatOptions daily = EventRepeatOptions(type: EventRepeatOptionsType.daily, dateRange: dateRange);

    final EventRepeatOptions weekly = EventRepeatOptions(
      type: EventRepeatOptionsType.weekly,
      dateRange: dateRange,
      weeklyRepeatsOn: [Weekday.fromDateTime(dateRange.start)],
    );

    test('single', () {
      expect(single.getAllowedUpdateModes(null, dateRange), equals([EventRepeatOptionsEditMode.allEvents]));
      expect(single.getAllowedUpdateModes(daily, dateRange), equals([EventRepeatOptionsEditMode.allEvents]));
    });

    test('creating a new event', () {
      expect(daily.getAllowedUpdateModes(null, dateRange), equals([EventRepeatOptionsEditMode.allEvents]));
    });

    test('removing recurrence', () {
      expect(daily.getAllowedUpdateModes(single, dateRange), equals([EventRepeatOptionsEditMode.allEvents]));
    });

    test('changing recurrence type', () {
      expect(daily.getAllowedUpdateModes(weekly, dateRange), equals([EventRepeatOptionsEditMode.allEvents]));
    });

    test('updating repeat interval', () {
      expect(
        daily.getAllowedUpdateModes(daily.copyWith(repeatInterval: Optional(2)), dateRange),
        equals([EventRepeatOptionsEditMode.allEvents]),
      );
    });

    test('editing second occurrence of the event', () {
      expect(
        daily.getAllowedUpdateModes(daily, dateRange.plusDays(1)),
        equals([
          EventRepeatOptionsEditMode.thisAndFollowingEvents,
          EventRepeatOptionsEditMode.allEvents,
          EventRepeatOptionsEditMode.thisEvent,
        ]),
      );
    });

    test('editing second occurrence of the event and changing its date range', () {
      expect(
        daily.getAllowedUpdateModes(daily.copyWith(dateRange: Optional(dateRange.plusDays(-1))), dateRange.plusDays(1)),
        equals([
          EventRepeatOptionsEditMode.thisAndFollowingEvents,
          EventRepeatOptionsEditMode.allEvents,
          EventRepeatOptionsEditMode.thisEvent,
        ]),
      );
    });

    test('changing recurrence type at the second event occurrence', () {
      expect(
        daily.getAllowedUpdateModes(weekly, dateRange.plusWeeks(1)),
        equals([
          EventRepeatOptionsEditMode.thisAndFollowingEvents,
          EventRepeatOptionsEditMode.allEvents,
        ]),
      );
    });

    test('changing recurrence type at the last event occurrence', () {
      final EventRepeatOptions pastOptions = weekly.copyWith(
        endsAt: Optional(dateRange.start.plusDays(13).withoutTime()),
      );

      expect(
        daily.getAllowedUpdateModes(pastOptions, dateRange.plusDays(13)),
        equals([EventRepeatOptionsEditMode.allEvents]),
      );
    });
  });

  group('EventRepeatOptionsUpdate getAllowedDeleteModes', () {
    final DateTimeRange dateRange = DateTimeRange(
      start: DateTime.utc(2020, 1, 1, 6, 0),
      end: DateTime.utc(2020, 1, 1, 10, 0),
    );

    final EventRepeatOptions single = EventRepeatOptions.single(dateRange);
    final EventRepeatOptions daily = EventRepeatOptions(type: EventRepeatOptionsType.daily, dateRange: dateRange);

    final EventRepeatOptions weekly = EventRepeatOptions(
      type: EventRepeatOptionsType.weekly,
      dateRange: dateRange,
      weeklyRepeatsOn: [Weekday.fromDateTime(dateRange.start)],
    );

    test('single', () {
      expect(single.getAllowedDeleteModes(dateRange), equals([EventRepeatOptionsEditMode.allEvents]));
    });

    test('delete daily event at first occurrence', () {
      expect(
        daily.getAllowedDeleteModes(dateRange),
        equals([EventRepeatOptionsEditMode.allEvents, EventRepeatOptionsEditMode.thisEvent]),
      );
    });

    test('delete daily event at second occurrence', () {
      expect(
        daily.getAllowedDeleteModes(dateRange.plusDays(1)),
        equals([
          EventRepeatOptionsEditMode.thisAndFollowingEvents,
          EventRepeatOptionsEditMode.allEvents,
          EventRepeatOptionsEditMode.thisEvent,
        ]),
      );
    });

    test('delete daily event at last occurrence (endsAt)', () {
      expect(
        daily
            .copyWith(endsAt: Optional(dateRange.start.plusDays(1).withoutTime()))
            .getAllowedDeleteModes(dateRange.plusDays(1)),
        equals([
          EventRepeatOptionsEditMode.allEvents,
          EventRepeatOptionsEditMode.thisEvent,
        ]),
      );
    });

    test('delete daily event at last occurrence (endsAfterOccurrences)', () {
      expect(
        daily.copyWith(endsAfterOccurrences: Optional(2)).getAllowedDeleteModes(dateRange.plusDays(1)),
        equals([
          EventRepeatOptionsEditMode.allEvents,
          EventRepeatOptionsEditMode.thisEvent,
        ]),
      );
    });

    test('delete weekly event at last occurrence, repeatInterval=2, weeklyRepeatsOn=[wed, fri]', () {
      expect(
        weekly
            .copyWith(
              endsAfterOccurrences: Optional(3),
              repeatInterval: Optional(2),
            )
            .getAllowedDeleteModes(dateRange.plusDays(9)),
        equals([
          EventRepeatOptionsEditMode.allEvents,
          EventRepeatOptionsEditMode.thisEvent,
        ]),
      );
    });
  });
}
