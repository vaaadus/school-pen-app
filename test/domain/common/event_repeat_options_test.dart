import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:test/test.dart';

void main() {
  group('EventRepeatOptions: single', () {
    final EventRepeatOptions singleOptions = EventRepeatOptions.single(DateTimeRange(
      start: DateTime.utc(2020, 11, 15, 13, 15),
      end: DateTime.utc(2020, 11, 17, 14, 30),
    ));

    test('getRepeatRanges: single - couple of days range', () {
      expect(
        EventRepeatOptions.splitManyIntoSingleDays(singleOptions.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 14),
          end: DateTime.utc(2020, 11, 18).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 15, 13, 15), end: DateTime.utc(2020, 11, 15).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 16), end: DateTime.utc(2020, 11, 16).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 17), end: DateTime.utc(2020, 11, 17, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges: single - single day range', () {
      expect(
        EventRepeatOptions.splitManyIntoSingleDays(singleOptions.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 15),
          end: DateTime.utc(2020, 11, 15).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 15, 13, 15), end: DateTime.utc(2020, 11, 15).atEndOfDay()),
        ]),
      );
    });

    test('getRepeatRanges: single - outside range', () {
      expect(
        EventRepeatOptions.splitManyIntoSingleDays(singleOptions.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 14),
          end: DateTime.utc(2020, 11, 14).atEndOfDay(),
        ))),
        unorderedEquals([]),
      );
    });
  });

  group('EventRepeatOptions: daily', () {
    test('getRepeatRanges daily', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.daily,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 15, 13, 15),
          end: DateTime.utc(2020, 11, 15, 14, 30),
        ),
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 14),
          end: DateTime.utc(2020, 11, 18).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 15, 13, 15), end: DateTime.utc(2020, 11, 15, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 16, 13, 15), end: DateTime.utc(2020, 11, 16, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 17, 13, 15), end: DateTime.utc(2020, 11, 17, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 18, 13, 15), end: DateTime.utc(2020, 11, 18, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges daily, daysSpan=3', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.daily,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 10, 13, 15),
          end: DateTime.utc(2020, 11, 12, 14, 30),
        ),
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 8),
          end: DateTime.utc(2020, 11, 15).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 10, 13, 15), end: DateTime.utc(2020, 11, 10).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 11), end: DateTime.utc(2020, 11, 11).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 11, 13, 15), end: DateTime.utc(2020, 11, 11).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 12), end: DateTime.utc(2020, 11, 12, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 12), end: DateTime.utc(2020, 11, 12).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 12, 13, 15), end: DateTime.utc(2020, 11, 12).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 13), end: DateTime.utc(2020, 11, 13, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 13), end: DateTime.utc(2020, 11, 13).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 13, 13, 15), end: DateTime.utc(2020, 11, 13).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 14), end: DateTime.utc(2020, 11, 14, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 14), end: DateTime.utc(2020, 11, 14).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 14, 13, 15), end: DateTime.utc(2020, 11, 14).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 15), end: DateTime.utc(2020, 11, 15, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 15), end: DateTime.utc(2020, 11, 15).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 15, 13, 15), end: DateTime.utc(2020, 11, 15).atEndOfDay()),
        ]),
      );
    });

    test('getRepeatRanges daily with repeatInterval=3', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.daily,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 10, 13, 15),
          end: DateTime.utc(2020, 11, 10, 14, 30),
        ),
        repeatInterval: 3,
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 8),
          end: DateTime.utc(2020, 11, 20).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 10, 13, 15), end: DateTime.utc(2020, 11, 10, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 13, 13, 15), end: DateTime.utc(2020, 11, 13, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 16, 13, 15), end: DateTime.utc(2020, 11, 16, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 19, 13, 15), end: DateTime.utc(2020, 11, 19, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges daily with repeatInterval=3, daysSpan=2', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.daily,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 10, 13, 15),
          end: DateTime.utc(2020, 11, 11, 14, 30),
        ),
        repeatInterval: 3,
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 8),
          end: DateTime.utc(2020, 11, 19).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 10, 13, 15), end: DateTime.utc(2020, 11, 10).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 11), end: DateTime.utc(2020, 11, 11, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 13, 13, 15), end: DateTime.utc(2020, 11, 13).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 14), end: DateTime.utc(2020, 11, 14, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 16, 13, 15), end: DateTime.utc(2020, 11, 16).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 17), end: DateTime.utc(2020, 11, 17, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 19, 13, 15), end: DateTime.utc(2020, 11, 19).atEndOfDay()),
        ]),
      );
    });

    test('getRepeatRanges daily with repeatInterval=3, daysSpan=2, endsAfterOccurrences=2', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.daily,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 10, 13, 15),
          end: DateTime.utc(2020, 11, 11, 14, 30),
        ),
        repeatInterval: 3,
        endsAfterOccurrences: 2,
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 8),
          end: DateTime.utc(2020, 11, 23).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 10, 13, 15), end: DateTime.utc(2020, 11, 10).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 11), end: DateTime.utc(2020, 11, 11, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 13, 13, 15), end: DateTime.utc(2020, 11, 13).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 14), end: DateTime.utc(2020, 11, 14, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges daily with repeatInterval=3, daysSpan=2, endsAt', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.daily,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 10, 13, 15),
          end: DateTime.utc(2020, 11, 11, 14, 30),
        ),
        repeatInterval: 3,
        endsAt: DateTime.utc(2020, 11, 16),
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 8),
          end: DateTime.utc(2020, 11, 24).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 10, 13, 15), end: DateTime.utc(2020, 11, 10).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 11), end: DateTime.utc(2020, 11, 11, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 13, 13, 15), end: DateTime.utc(2020, 11, 13).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 14), end: DateTime.utc(2020, 11, 14, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 16, 13, 15), end: DateTime.utc(2020, 11, 16).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 17), end: DateTime.utc(2020, 11, 17, 14, 30)),
        ]),
      );
    });
  });

  group('EventRepeatOptions: weekly', () {
    test('getRepeatRanges weekly', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.weekly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 15, 13, 15),
          end: DateTime.utc(2020, 11, 15, 14, 30),
        ),
        weeklyRepeatsOn: [Weekday.sunday],
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 14),
          end: DateTime.utc(2020, 11, 30).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 15, 13, 15), end: DateTime.utc(2020, 11, 15, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 22, 13, 15), end: DateTime.utc(2020, 11, 22, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 29, 13, 15), end: DateTime.utc(2020, 11, 29, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges weekly, weeklyRepeatsOn=[Monday, Wednesday]', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.weekly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 16, 13, 15),
          end: DateTime.utc(2020, 11, 16, 14, 30),
        ),
        weeklyRepeatsOn: [Weekday.monday, Weekday.wednesday],
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 18),
          end: DateTime.utc(2020, 11, 27).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 18, 13, 15), end: DateTime.utc(2020, 11, 18, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 23, 13, 15), end: DateTime.utc(2020, 11, 23, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 25, 13, 15), end: DateTime.utc(2020, 11, 25, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges weekly, weeklyRepeatsOn=[Monday, Wednesday], endsAfterOccurrences=3', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.weekly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 16, 13, 15),
          end: DateTime.utc(2020, 11, 16, 14, 30),
        ),
        weeklyRepeatsOn: [Weekday.monday, Weekday.wednesday],
        endsAfterOccurrences: 3,
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 18),
          end: DateTime.utc(2020, 12, 30).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 18, 13, 15), end: DateTime.utc(2020, 11, 18, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 23, 13, 15), end: DateTime.utc(2020, 11, 23, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges weekly, daysSpan=2, weeklyRepeatsOn=[Sunday, Friday], endsAt=22.11', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.weekly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 15, 13, 15),
          end: DateTime.utc(2020, 11, 16, 14, 30),
        ),
        endsAt: DateTime.utc(2020, 11, 22),
        weeklyRepeatsOn: [Weekday.friday, Weekday.sunday],
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 14),
          end: DateTime.utc(2020, 11, 27).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 15, 13, 15), end: DateTime.utc(2020, 11, 15).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 16), end: DateTime.utc(2020, 11, 16, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 20, 13, 15), end: DateTime.utc(2020, 11, 20).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 21), end: DateTime.utc(2020, 11, 21, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 22, 13, 15), end: DateTime.utc(2020, 11, 22).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 23), end: DateTime.utc(2020, 11, 23, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges weekly, repeatInterval=2', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.weekly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 11, 15, 13, 15),
          end: DateTime.utc(2020, 11, 15, 14, 30),
        ),
        repeatInterval: 2,
        weeklyRepeatsOn: [Weekday.sunday],
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 11, 14),
          end: DateTime.utc(2020, 12, 28).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 11, 15, 13, 15), end: DateTime.utc(2020, 11, 15, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 29, 13, 15), end: DateTime.utc(2020, 11, 29, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 12, 13, 13, 15), end: DateTime.utc(2020, 12, 13, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 12, 27, 13, 15), end: DateTime.utc(2020, 12, 27, 14, 30)),
        ]),
      );
    });
  });

  group('EventRepeatOptions: monthly', () {
    test('getRepeatRanges outside range', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.monthly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 10, 31, 13, 15),
          end: DateTime.utc(2020, 10, 31, 14, 30),
        ),
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 5, 14),
          end: DateTime.utc(2020, 5, 21).atEndOfDay(),
        ))),
        unorderedEquals([]),
      );
    });

    test('getRepeatRanges monthly', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.monthly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 10, 31, 13, 15),
          end: DateTime.utc(2020, 10, 31, 14, 30),
        ),
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 5, 14),
          end: DateTime.utc(2020, 12, 30).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 10, 31, 13, 15), end: DateTime.utc(2020, 10, 31, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges single week', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.monthly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 10, 31, 13, 15),
          end: DateTime.utc(2020, 10, 31, 14, 30),
        ),
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 10, 24),
          end: DateTime.utc(2020, 12, 1).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 10, 31, 13, 15), end: DateTime.utc(2020, 10, 31, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges monthly, daysSpan=2', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.monthly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 10, 15, 13, 15),
          end: DateTime.utc(2020, 10, 16, 14, 30),
        ),
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 5, 14),
          end: DateTime.utc(2020, 12, 30).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 10, 15, 13, 15), end: DateTime.utc(2020, 10, 15).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 10, 16), end: DateTime.utc(2020, 10, 16, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 11, 15, 13, 15), end: DateTime.utc(2020, 11, 15).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 11, 16), end: DateTime.utc(2020, 11, 16, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 12, 15, 13, 15), end: DateTime.utc(2020, 12, 15).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 12, 16), end: DateTime.utc(2020, 12, 16, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges monthly, repeatInterval=3', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.monthly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 4, 30, 13, 15),
          end: DateTime.utc(2020, 4, 30, 14, 30),
        ),
        repeatInterval: 3,
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 3, 14),
          end: DateTime.utc(2020, 12, 30).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 4, 30, 13, 15), end: DateTime.utc(2020, 4, 30, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 7, 30, 13, 15), end: DateTime.utc(2020, 7, 30, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 10, 30, 13, 15), end: DateTime.utc(2020, 10, 30, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges monthly, endsAt=30.05.2020', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.monthly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 4, 30, 13, 15),
          end: DateTime.utc(2020, 4, 30, 14, 30),
        ),
        endsAt: DateTime.utc(2020, 5, 30),
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 3, 14),
          end: DateTime.utc(2020, 12, 30).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 4, 30, 13, 15), end: DateTime.utc(2020, 4, 30, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 5, 30, 13, 15), end: DateTime.utc(2020, 5, 30, 14, 30)),
        ]),
      );
    });

    test('getRepeatRanges monthly, endsAfterOccurrences=3', () {
      final EventRepeatOptions options = EventRepeatOptions(
        type: EventRepeatOptionsType.monthly,
        dateRange: DateTimeRange(
          start: DateTime.utc(2020, 4, 30, 13, 15),
          end: DateTime.utc(2020, 4, 30, 14, 30),
        ),
        endsAfterOccurrences: 3,
      );

      expect(
        EventRepeatOptions.splitManyIntoSingleDays(options.getRepeatRanges(DateTimeRange(
          start: DateTime.utc(2020, 3, 14),
          end: DateTime.utc(2020, 12, 30).atEndOfDay(),
        ))),
        unorderedEquals([
          DateTimeRange(start: DateTime.utc(2020, 4, 30, 13, 15), end: DateTime.utc(2020, 4, 30, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 5, 30, 13, 15), end: DateTime.utc(2020, 5, 30, 14, 30)),
          DateTimeRange(start: DateTime.utc(2020, 6, 30, 13, 15), end: DateTime.utc(2020, 6, 30, 14, 30)),
        ]),
      );
    });
  });

  group('EventRepeatOptions: splitIntoSingleDays', () {
    test('splitIntoSingleDays start.time > end.time', () {
      final DateTimeRange dateRange = DateTimeRange(
        start: DateTime.utc(2020, 3, 20, 10, 30),
        end: DateTime.utc(2020, 3, 21, 9, 40),
      );

      expect(
        EventRepeatOptions.splitIntoSingleDays(dateRange),
        equals([
          DateTimeRange(start: DateTime.utc(2020, 3, 20, 10, 30), end: DateTime.utc(2020, 3, 20).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 3, 21).withoutTime(), end: DateTime.utc(2020, 3, 21, 9, 40)),
        ]),
      );
    });

    test('splitIntoSingleDays start.time < end.time', () {
      final DateTimeRange dateRange = DateTimeRange(
        start: DateTime.utc(2020, 3, 20, 9, 30),
        end: DateTime.utc(2020, 3, 21, 10, 40),
      );

      expect(
        EventRepeatOptions.splitIntoSingleDays(dateRange),
        equals([
          DateTimeRange(start: DateTime.utc(2020, 3, 20, 9, 30), end: DateTime.utc(2020, 3, 20).atEndOfDay()),
          DateTimeRange(start: DateTime.utc(2020, 3, 21).withoutTime(), end: DateTime.utc(2020, 3, 21, 10, 40)),
        ]),
      );
    });
  });
}
