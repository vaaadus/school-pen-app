import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:test/test.dart';

void main() {
  group('DateTimes', () {
    final DateTime now = DateTime.now();

    test('isWithoutTime', () {
      expect(now.isWithoutTime(), isFalse);
      expect(now.withoutTime().isWithoutTime(), isTrue);
    });

    test('isSameDayAs', () {
      expect(now.isSameDateAs(now), isTrue);
      expect(DateTime(now.year, now.month, now.day, 12, 0).isSameDateAs(now), isTrue);
      expect(now.isSameDateAs(now.plusDays(1)), isFalse);
      expect(now.isSameDateAs(now.minusDays(1)), isFalse);
    });

    test('isToday', () {
      expect(now.isToday, isTrue);
      expect(now.plusDays(1).isToday, isFalse);
      expect(now.minusDays(1).isToday, isFalse);
    });

    test('isYesterday', () {
      expect(now.isYesterday, isFalse);
      expect(now.minusDays(1).isYesterday, isTrue);
      expect(now.minusDays(2).isYesterday, isFalse);
    });

    test('isTomorrow', () {
      expect(now.isTomorrow, isFalse);
      expect(now.plusDays(1).isTomorrow, isTrue);
      expect(now.plusDays(2).isTomorrow, isFalse);
    });

    test('isAfterOrAt', () {
      expect(now.isAfterOrAt(now), isTrue);
      expect(now.isAfterOrAt(now.subtract(Duration(milliseconds: 1))), isTrue);
      expect(now.isAfterOrAt(now.add(Duration(milliseconds: 1))), isFalse);
    });

    test('isBeforeOrAt', () {
      expect(now.isBeforeOrAt(now), isTrue);
      expect(now.isBeforeOrAt(now.add(Duration(milliseconds: 1))), isTrue);
      expect(now.isBeforeOrAt(now.subtract(Duration(milliseconds: 1))), isFalse);
    });

    test('withTime', () {
      expect(now.withTime(Time(hour: 13, minute: 30)), equals(DateTime(now.year, now.month, now.day, 13, 30)));
    });

    test('withMinutes', () {
      expect(now.withMinutes(40), equals(DateTime(now.year, now.month, now.day, now.hour, 40)));
    });

    test('withoutTime', () {
      expect(now.withoutTime(), equals(DateTime(now.year, now.month, now.day)));
    });

    test('withoutSeconds', () {
      expect(now.withoutSeconds(), equals(DateTime(now.year, now.month, now.day, now.hour, now.minute)));
    });

    test('atEndOfDay', () {
      expect(now.atEndOfDay(), equals(DateTime(now.year, now.month, now.day, 23, 59, 59, 999, 999)));
    });

    test('asUtc', () {
      expect(
          now.asUtc(),
          equals(DateTime.utc(
              now.year, now.month, now.day, now.hour, now.minute, now.second, now.millisecond, now.microsecond)));
    });

    test('asLocal', () {
      expect(now.asUtc().asLocal(), equals(now));
    });

    test('time', () {
      expect(now.time, equals(Time.fromDateTime(now)));
    });

    test('timeOfDay', () {
      expect(now.timeOfDay, equals(TimeOfDay.fromDateTime(now)));
    });

    test('plusDays', () {
      expect(DateTime(2020, 1, 1).plusDays(3), equals(DateTime(2020, 1, 4)));
      expect(DateTime.utc(2020, 1, 1).plusDays(3), equals(DateTime.utc(2020, 1, 4)));

      // DST in Poland: +1h at 29.03.2020, -1h at 25.10.2020
      expect(DateTime(2020, 3, 28).plusDays(2), equals(DateTime(2020, 3, 30)));
      expect(DateTime(2020, 10, 24, 15, 30, 45).plusDays(3), equals(DateTime(2020, 10, 27, 15, 30, 45)));
    });

    test('minusDays', () {
      expect(DateTime(2020, 1, 4).minusDays(3), equals(DateTime(2020, 1, 1)));
      expect(DateTime.utc(2020, 1, 4).minusDays(3), equals(DateTime.utc(2020, 1, 1)));

      // DST in Poland: +1h at 29.03.2020, -1h at 25.10.2020
      expect(DateTime(2020, 3, 30).minusDays(2), equals(DateTime(2020, 3, 28)));
      expect(DateTime(2020, 10, 27, 15, 30, 45).minusDays(3), equals(DateTime(2020, 10, 24, 15, 30, 45)));
    });

    test('plusWeeks', () {
      expect(now.plusWeeks(1), equals(now.plusDays(7)));
      expect(now.plusWeeks(3), equals(now.plusDays(21)));
      expect(now.plusWeeks(3000), equals(now.plusDays(21000)));
    });

    test('minusWeeks', () {
      expect(now.minusWeeks(1), equals(now.minusDays(7)));
      expect(now.minusWeeks(3), equals(now.minusDays(21)));
      expect(now.minusWeeks(3000), equals(now.minusDays(21000)));
    });

    test('plusMonths', () {
      expect(DateTime(2020, 2, 28).plusMonths(1), equals(DateTime(2020, 3, 28)));
      expect(DateTime(2020, 2, 29).plusMonths(12), equals(DateTime(2021, 2, 28)));
      expect(DateTime(2020, 5, 31).plusMonths(1), equals(DateTime(2020, 6, 30)));
    });

    test('minusMonths', () {
      expect(DateTime(2020, 3, 28).minusMonths(1), equals(DateTime(2020, 2, 28)));
      expect(DateTime(2021, 2, 28).minusMonths(12), equals(DateTime(2020, 2, 28)));
      expect(DateTime(2020, 6, 30).minusMonths(1), equals(DateTime(2020, 5, 30)));
    });

    test('adjustToNextWeekday', () {
      for (Weekday weekday in Weekday.values) {
        expect(Weekday.fromDateTime(now.adjustToNextWeekday(weekday)), equals(weekday));
      }

      expect(DateTime(2020, 11, 18).adjustToNextWeekday(Weekday.monday), equals(DateTime(2020, 11, 23)));
    });

    test('adjustToPreviousWeekday', () {
      for (Weekday weekday in Weekday.values) {
        expect(Weekday.fromDateTime(now.adjustToPreviousWeekday(weekday)), equals(weekday));
      }

      expect(DateTime(2020, 11, 18).adjustToPreviousWeekday(Weekday.monday), equals(DateTime(2020, 11, 16)));
    });

    test('adjustToStartOfWeek', () {
      expect(Weekday.fromDateTime(now.adjustToStartOfWeek()), equals(Weekday.firstOfWeek()));
    });

    test('daysInMonth', () {
      expect(DateTimes.daysInMonth(2020, DateTime.january), equals(31));
      expect(DateTimes.daysInMonth(2020, DateTime.february), equals(29));
      expect(DateTimes.daysInMonth(2021, DateTime.february), equals(28));
      expect(DateTimes.daysInMonth(2020, DateTime.june), equals(30));
    });
  });

  group('DateTimeRanges', () {
    test('durationInDays', () {
      final DateTimeRange _withDstInPoland = DateTimeRange(
        start: DateTime(2020, 10, 24, 3, 0),
        end: DateTime(2020, 10, 26, 19, 0),
      );

      expect(_withDstInPoland.durationInDays(), equals(2));

      final DateTimeRange _year = DateTimeRange(start: DateTime(2020, 1, 1), end: DateTime(2021, 1, 1));
      expect(_year.durationInDays(), equals(366));
    });

    test('durationInWeeks', () {
      final DateTimeRange _sixDays = DateTimeRange(start: DateTime(2020, 9, 20), end: DateTime(2020, 9, 26));
      expect(_sixDays.durationInWeeks(), equals(0));

      final DateTimeRange _oneWeek = DateTimeRange(start: DateTime(2020, 6, 15), end: DateTime(2020, 6, 22));
      expect(_oneWeek.durationInWeeks(), equals(1));

      final DateTimeRange _year = DateTimeRange(start: DateTime(2020, 1, 1), end: DateTime(2021, 1, 1));
      expect(_year.durationInWeeks(), equals(52));
    });

    test('durationInMonths', () {
      final DateTimeRange _twoWeeksLeap = DateTimeRange(start: DateTime(2020, 9, 20), end: DateTime(2020, 10, 5));
      expect(_twoWeeksLeap.durationInMonths(), equals(0));

      final DateTimeRange _oneMonth = DateTimeRange(start: DateTime(2020, 6, 15), end: DateTime(2020, 7, 15));
      expect(_oneMonth.durationInMonths(), equals(1));

      final DateTimeRange _twoAndHalfMonths = DateTimeRange(start: DateTime(2020, 3, 1), end: DateTime(2020, 5, 16));
      expect(_twoAndHalfMonths.durationInMonths(), equals(2));

      final DateTimeRange _twelveMonths = DateTimeRange(start: DateTime(2020, 1, 1), end: DateTime(2021, 1, 1));
      expect(_twelveMonths.durationInMonths(), equals(12));
    });

    test('plusDuration', () {
      final DateTimeRange range = DateTimeRange(start: DateTime(2020, 1, 3), end: DateTime(2020, 4, 6));
      expect(
        range.plusDuration(Duration(days: 1)),
        equals(DateTimeRange(start: range.start.plusDays(1), end: range.end.plusDays(1))),
      );
    });

    test('plusDays', () {
      final DateTimeRange range = DateTimeRange(start: DateTime(2020, 1, 3), end: DateTime(2020, 4, 6));
      expect(range.plusDays(11), equals(DateTimeRange(start: range.start.plusDays(11), end: range.end.plusDays(11))));
    });

    test('plusWeeks', () {
      final DateTimeRange range = DateTimeRange(start: DateTime(2020, 1, 3), end: DateTime(2020, 4, 6));
      expect(range.plusWeeks(7), equals(DateTimeRange(start: range.start.plusWeeks(7), end: range.end.plusWeeks(7))));
    });

    test('plusMonths', () {
      final DateTimeRange range = DateTimeRange(start: DateTime(2020, 1, 3), end: DateTime(2020, 4, 6));
      expect(
        range.plusMonths(3),
        equals(DateTimeRange(start: range.start.plusMonths(3), end: range.end.plusMonths(3))),
      );
    });

    test('withoutTime', () {
      final DateTimeRange range = DateTimeRange(start: DateTime(2020, 1, 3, 20, 30), end: DateTime(2020, 4, 6));
      expect(
        range.withoutTime(),
        equals(DateTimeRange(start: range.start.withoutTime(), end: range.end.withoutTime())),
      );
    });

    test('timeRange', () {
      expect(
        DateTimeRange(
          start: DateTime(2020, 1, 3, 11, 30),
          end: DateTime(2020, 4, 6, 15, 49),
        ).timeRange,
        equals(TimeRange(
          start: Time(hour: 11, minute: 30),
          end: Time(hour: 15, minute: 49),
        )),
      );
    });

    test('overlapsOrAdjacentWith', () {
      final DateTimeRange _2020_2021 = DateTimeRange(start: DateTime(2020), end: DateTime(2021));
      final DateTimeRange _2021_2030 = DateTimeRange(start: DateTime(2021), end: DateTime(2030));
      final DateTimeRange _2010_2040 = DateTimeRange(start: DateTime(2010), end: DateTime(2040));

      expect(_2020_2021.overlapsOrAdjacentWith(_2020_2021), isTrue);
      expect(_2020_2021.overlapsWith(_2020_2021), isTrue);

      expect(_2020_2021.overlapsOrAdjacentWith(_2021_2030), isTrue);
      expect(_2020_2021.overlapsWith(_2021_2030), isFalse);

      expect(_2010_2040.overlapsOrAdjacentWith(_2021_2030), isTrue);
      expect(_2010_2040.overlapsWith(_2021_2030), isTrue);
    });

    test('contains', () {
      final DateTimeRange _2020_2021 = DateTimeRange(start: DateTime(2020), end: DateTime(2021));
      expect(_2020_2021.contains(DateTime(2020)), isTrue);
      expect(_2020_2021.contains(DateTime(2021)), isTrue);
      expect(_2020_2021.contains(DateTime(2019)), isFalse);
      expect(_2020_2021.contains(DateTime(2022)), isFalse);
    });

    test('containsRange', () {
      final DateTimeRange _2020_2021 = DateTimeRange(start: DateTime(2020), end: DateTime(2021));
      expect(_2020_2021.containsRange(DateTimeRange(start: DateTime(2020), end: DateTime(2021))), isTrue);
      expect(_2020_2021.containsRange(DateTimeRange(start: DateTime(2020, 5, 3), end: DateTime(2020, 12, 31))), isTrue);
      expect(_2020_2021.containsRange(DateTimeRange(start: DateTime(2019), end: DateTime(2020))), isFalse);
      expect(_2020_2021.containsRange(DateTimeRange(start: DateTime(2021), end: DateTime(2022))), isFalse);
      expect(_2020_2021.containsRange(DateTimeRange(start: DateTime(2019), end: DateTime(2022))), isFalse);
    });

    test('clamp', () {
      final DateTimeRange _2020_2025 = DateTimeRange(start: DateTime(2020), end: DateTime(2025));
      final DateTimeRange _2015_2022 = DateTimeRange(start: DateTime(2015), end: DateTime(2022));
      final DateTimeRange _2010_2040 = DateTimeRange(start: DateTime(2010), end: DateTime(2040));
      final DateTimeRange _2100_2105 = DateTimeRange(start: DateTime(2100), end: DateTime(2105));

      expect(_2020_2025.clamp(_2015_2022), equals(DateTimeRange(start: DateTime(2020), end: DateTime(2022))));
      expect(_2015_2022.clamp(_2020_2025), equals(DateTimeRange(start: DateTime(2020), end: DateTime(2022))));
      expect(_2015_2022.clamp(_2010_2040), equals(_2015_2022));
      expect(_2015_2022.clamp(_2100_2105), isNull);
    });
  });
}
