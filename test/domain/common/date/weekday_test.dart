import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:test/test.dart';

void main() {
  group('Weekday', () {
    test('fromDateTime constructor', () {
      final monday = DateTime.parse('2020-04-27T12:00:00Z');
      final sunday = DateTime.parse('2020-04-26T11:00:00Z');
      expect(Weekday.fromDateTime(monday), equals(Weekday.monday));
      expect(Weekday.fromDateTime(sunday), equals(Weekday.sunday));
    });

    test('fromTag constructor', () {
      expect(Weekday.fromTag(Weekday.monday.tag), equals(Weekday.monday));
      expect(Weekday.fromTag(Weekday.sunday.tag), equals(Weekday.sunday));
    });

    test('firstOfWeek', () {
      expect(Weekday.firstOfWeek(), isNotNull);
    });

    test('earliest', () {
      expect(Weekday.earliest([Weekday.wednesday, Weekday.firstOfWeek()]), equals(Weekday.firstOfWeek()));
      expect(Weekday.earliest([Weekday.tuesday, Weekday.friday]), equals(Weekday.tuesday));
    });

    test('nextDay', () {
      expect(Weekday.monday.nextDay(), equals(Weekday.tuesday));
      expect(Weekday.sunday.nextDay(), equals(Weekday.monday));
    });

    test('previousDay', () {
      expect(Weekday.tuesday.previousDay(), equals(Weekday.monday));
      expect(Weekday.monday.previousDay(), equals(Weekday.sunday));
    });

    test('isBefore', () {
      expect(Weekday.monday.isBefore(Weekday.tuesday), isTrue);
      expect(Weekday.wednesday.isBefore(Weekday.friday), isTrue);
      expect(Weekday.thursday.isBefore(Weekday.saturday), isTrue);
      expect(Weekday.monday.isBefore(Weekday.tuesday), isTrue);

      expect(Weekday.thursday.isBefore(Weekday.thursday), isFalse);
      expect(Weekday.tuesday.isBefore(Weekday.monday), isFalse);
      expect(Weekday.friday.isBefore(Weekday.wednesday), isFalse);
      expect(Weekday.saturday.isBefore(Weekday.thursday), isFalse);
    });

    test('isAfter', () {
      expect(Weekday.tuesday.isAfter(Weekday.monday), isTrue);
      expect(Weekday.friday.isAfter(Weekday.wednesday), isTrue);
      expect(Weekday.saturday.isAfter(Weekday.thursday), isTrue);
      expect(Weekday.tuesday.isAfter(Weekday.monday), isTrue);

      expect(Weekday.thursday.isAfter(Weekday.thursday), isFalse);
      expect(Weekday.monday.isAfter(Weekday.tuesday), isFalse);
      expect(Weekday.wednesday.isAfter(Weekday.friday), isFalse);
      expect(Weekday.thursday.isAfter(Weekday.saturday), isFalse);
    });
  });
}
