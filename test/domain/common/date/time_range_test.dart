import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:test/test.dart';

void main() {
  group('TimeRange', () {
    test('constructor factory assigns start before end', () {
      var earlier = Time(hour: 15, minute: 0);
      var later = Time(hour: 23, minute: 0);

      expect(TimeRange(start: earlier, end: later).start, equals(earlier));
      expect(TimeRange(start: later, end: earlier).start, equals(earlier));
    });

    test('duration()', () {
      var range = TimeRange(
        start: Time(hour: 10, minute: 15),
        end: Time(hour: 20, minute: 45),
      );

      expect(range.duration, equals(Duration(hours: 10, minutes: 30)));
    });

    test('contains()', () {
      var range = TimeRange(
        start: Time(hour: 10, minute: 15),
        end: Time(hour: 20, minute: 45),
      );

      expect(range.contains(Time(hour: 10, minute: 15)), isTrue);
      expect(range.contains(Time(hour: 20, minute: 45)), isTrue);
      expect(range.contains(Time(hour: 15, minute: 32)), isTrue);
      expect(range.contains(Time(hour: 10, minute: 14)), isFalse);
      expect(range.contains(Time(hour: 20, minute: 46)), isFalse);
    });

    test('overlapsWith()', () {
      var left = TimeRange(start: Time(hour: 0, minute: 0), end: Time(hour: 6, minute: 0));
      var middle = TimeRange(start: Time(hour: 5, minute: 0), end: Time(hour: 7, minute: 0));
      var right = TimeRange(start: Time(hour: 6, minute: 0), end: Time(hour: 12, minute: 0));

      expect(left.overlapsWith(middle), isTrue);
      expect(middle.overlapsWith(left), isTrue);
      expect(right.overlapsWith(middle), isTrue);
      expect(middle.overlapsWith(right), isTrue);
      expect(left.overlapsWith(right), isFalse);
      expect(right.overlapsWith(left), isFalse);
    });
  });
}
