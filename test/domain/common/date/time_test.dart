import 'package:school_pen/domain/common/date/time.dart';
import 'package:test/test.dart';

void main() {
  group('Time', () {
    test('.fromDateTime() returns time part', () {
      var datetime = DateTime.now();
      expect(
        Time.fromDateTime(datetime),
        equals(Time(hour: datetime.hour, minute: datetime.minute)),
      );
    });

    test('.fromMinutes()', () {
      expect(Time.fromMinutes(Time.min.toMinutes()), equals(Time.min));
      expect(Time.fromMinutes(Time.max.toMinutes()), equals(Time.max));
      expect(Time.fromMinutes(Time(hour: 10, minute: 37).toMinutes()), equals(Time(hour: 10, minute: 37)));
    });

    test('.fromString()', () {
      expect(Time.fromString('9:07'), equals(Time(hour: 9, minute: 7)));
      expect(Time.fromString('09:17'), equals(Time(hour: 9, minute: 17)));
    });

    test('.isBefore()', () {
      var earlier = Time(hour: 10, minute: 0);
      var later = Time(hour: 20, minute: 0);

      expect(earlier.isBefore(later), isTrue);
      expect(earlier.isBeforeOrAt(later), isTrue);

      expect(earlier.isBefore(earlier), isFalse);
      expect(earlier.isBeforeOrAt(earlier), isTrue);

      expect(later.isBefore(earlier), isFalse);
      expect(later.isBeforeOrAt(earlier), isFalse);
    });

    test('.isAfter()', () {
      var earlier = Time(hour: 10, minute: 0);
      var later = Time(hour: 20, minute: 0);

      expect(later.isAfter(earlier), isTrue);
      expect(later.isAfterOrAt(earlier), isTrue);

      expect(later.isAfter(later), isFalse);
      expect(later.isAfterOrAt(later), isTrue);

      expect(earlier.isAfter(later), isFalse);
      expect(earlier.isAfterOrAt(later), isFalse);
    });

    test('.toMinutes()', () {
      expect(Time(hour: 0, minute: 0).toMinutes(), equals(0));
      expect(Time(hour: 1, minute: 0).toMinutes(), equals(60));
      expect(Time(hour: 24, minute: 0).toMinutes(), equals(1440));
      expect(Time(hour: 12, minute: 59).toMinutes(), equals(779));
    });

    test('.plus()', () {
      expect(
        Time(hour: 20, minute: 0).plus(Duration(minutes: 30)),
        equals(Time(hour: 20, minute: 30)),
      );

      expect(
        Time(hour: 10, minute: 0).plus(Duration()),
        equals(Time(hour: 10, minute: 0)),
      );

      expect(
        Time(hour: 12, minute: 0).plus(Duration(hours: -3)),
        equals(Time(hour: 9, minute: 0)),
      );
    });

    test('.minus()', () {
      expect(
        Time(hour: 20, minute: 30).minus(Time(hour: 20, minute: 0)),
        equals(Duration(minutes: 30)),
      );

      expect(
        Time(hour: 10, minute: 0).minus(Time(hour: 10, minute: 0)),
        equals(Duration()),
      );

      expect(
        Time(hour: 9, minute: 0).minus(Time(hour: 12, minute: 0)),
        equals(Duration(hours: -3)),
      );
    });

    test('.toString()', () {
      expect(Time(hour: 9, minute: 2).toString(), equals('09:02'));
      expect(Time(hour: 23, minute: 59).toString(), equals('23:59'));
    });
  });
}
