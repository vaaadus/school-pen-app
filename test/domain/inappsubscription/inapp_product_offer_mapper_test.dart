// ignore_for_file: must_be_immutable

import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:in_app_purchase/store_kit_wrappers.dart';
import 'package:intl/intl.dart';
import 'package:mockito/mockito.dart';
import 'package:school_pen/domain/inappsubscription/inapp_product_offer_mapper.dart';
import 'package:school_pen/domain/inappsubscription/model/inapp_subscription_offer.dart';
import 'package:school_pen/domain/inappsubscription/model/inapp_subscription_plan.dart';
import 'package:test/test.dart';

class TestProductDetails extends Mock implements ProductDetails {}

class TestSkuDetailsWrapper extends Mock implements SkuDetailsWrapper {}

class TestSKProductWrapper extends Mock implements SKProductWrapper {}

class TestSKPriceLocaleWrapper extends Mock implements SKPriceLocaleWrapper {}

class TestInAppSubscriptionOffer extends Mock implements InAppSubscriptionOffer {}

void main() {
  group('InAppProductOfferMapper', () {
    final mapper = InAppProductOfferMapper();

    String defaultLocale;

    setUp(() {
      defaultLocale = Intl.defaultLocale;
    });

    tearDown(() {
      Intl.defaultLocale = defaultLocale;
    });

    test('.asProduct()', () {
      var product = TestProductDetails();
      var offer = TestInAppSubscriptionOffer();
      when(offer.customData).thenReturn(product);

      expect(mapper.asProduct(offer), equals(product));
    });

    test('.asOffer()', () {
      Intl.defaultLocale = 'pl';

      var skuDetails = TestSkuDetailsWrapper();
      when(skuDetails.priceAmountMicros).thenReturn(24400000);
      when(skuDetails.priceCurrencyCode).thenReturn('PLN');

      var yearlyProduct = ProductDetails(
        id: InAppSubscriptionPlan.premiumYearly.tag,
        title: 'Test title',
        description: 'Test description',
        price: '24,40\u00A0zł',
        skuDetail: skuDetails,
      );

      var expected = InAppSubscriptionOffer(
        plan: InAppSubscriptionPlan.premiumYearly,
        title: yearlyProduct.title,
        description: yearlyProduct.description,
        formattedPrice: yearlyProduct.price,
        formattedMonthlyPrice: '2,03\u00A0zł',
        monthlyPrice: 2.03,
        savings: null,
        customData: yearlyProduct,
      );

      var actual = mapper.asOffer([yearlyProduct], yearlyProduct);

      expect(actual, equals(expected));
    });

    test('.asOffer() with savings', () {
      Intl.defaultLocale = 'en-US';

      // yearly product

      var yearlySKPriceLocale = TestSKPriceLocaleWrapper();
      when(yearlySKPriceLocale.currencyCode).thenReturn('USD');

      var yearlySKProduct = TestSKProductWrapper();
      when(yearlySKProduct.price).thenReturn('24.40');
      when(yearlySKProduct.priceLocale).thenReturn(yearlySKPriceLocale);

      var yearlyProduct = ProductDetails(
        id: InAppSubscriptionPlan.premiumYearly.tag,
        title: 'Yearly test title',
        description: 'Yearly test description',
        price: '\$24.40',
        skProduct: yearlySKProduct,
      );

      // monthly product

      var monthlySKPriceLocale = TestSKPriceLocaleWrapper();
      when(monthlySKPriceLocale.currencyCode).thenReturn('USD');

      var monthlySKProduct = TestSKProductWrapper();
      when(monthlySKProduct.price).thenReturn('2.99');
      when(monthlySKProduct.priceLocale).thenReturn(monthlySKPriceLocale);

      var monthlyProduct = ProductDetails(
        id: InAppSubscriptionPlan.premiumMonthly.tag,
        title: 'Monthly test title',
        description: 'Monthly test description',
        price: '\$2.99',
        skProduct: monthlySKProduct,
      );

      // expected

      var expectedYearlyOffer = InAppSubscriptionOffer(
        plan: InAppSubscriptionPlan.premiumYearly,
        title: yearlyProduct.title,
        description: yearlyProduct.description,
        formattedPrice: yearlyProduct.price,
        formattedMonthlyPrice: '\$2.03',
        monthlyPrice: 2.03,
        savings: 0.32,
        customData: yearlyProduct,
      );

      var expectedMonthlyOffer = InAppSubscriptionOffer(
        plan: InAppSubscriptionPlan.premiumMonthly,
        title: monthlyProduct.title,
        description: monthlyProduct.description,
        formattedPrice: monthlyProduct.price,
        formattedMonthlyPrice: monthlyProduct.price,
        monthlyPrice: 2.99,
        savings: null,
        customData: monthlyProduct,
      );

      var actualYearlyOffer = mapper.asOffer([yearlyProduct, monthlyProduct], yearlyProduct);
      var actualMonthlyOffer = mapper.asOffer([yearlyProduct, monthlyProduct], monthlyProduct);

      expect(actualYearlyOffer, equals(expectedYearlyOffer));
      expect(actualMonthlyOffer, equals(expectedMonthlyOffer));
    });
  });
}
