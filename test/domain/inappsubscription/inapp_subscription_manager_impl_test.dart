// ignore_for_file: must_be_immutable

import 'dart:async';

import 'package:fake_async/fake_async.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/inappsubscription/inapp_product_offer_mapper.dart';
import 'package:school_pen/domain/inappsubscription/inapp_subscription_manager_impl.dart';
import 'package:school_pen/domain/inappsubscription/model/inapp_subscribed_plan.dart';
import 'package:school_pen/domain/inappsubscription/model/inapp_subscription_offer.dart';
import 'package:school_pen/domain/inappsubscription/model/inapp_subscription_plan.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/preferences/impl/in_memory_preferences.dart';
import 'package:test/test.dart';

class TestInAppPurchaseConnection extends Mock implements InAppPurchaseConnection {}

class TestInAppProductOfferMapper extends Mock implements InAppProductOfferMapper {}

class TestPurchaseDetails extends Mock implements PurchaseDetails {}

class TestInAppSubscriptionOffer extends Mock implements InAppSubscriptionOffer {}

class TestProductDetails extends Mock implements ProductDetails {}

class TestIAPError extends Mock implements IAPError {}

class TestLifecycleManager extends Mock implements LifecycleManager {}

void main() {
  group('InAppSubscriptionManager', () {
    InAppSubscriptionManagerImpl manager;
    PublishSubject<List<PurchaseDetails>> purchaseUpdatedStream;

    var store = TestInAppPurchaseConnection();
    var mapper = TestInAppProductOfferMapper();
    var lifecycle = TestLifecycleManager();
    var preferences = InMemoryPreferences();

    void _stubStoreAvailable(bool available) {
      when(store.isAvailable()).thenAnswer((_) => Future.value(available));
    }

    void _stubPastPurchaseWithStatus(PurchaseStatus status) {
      var plan = InAppSubscriptionPlan.premiumMonthly;
      var purchase = TestPurchaseDetails();
      when(purchase.productID).thenReturn(plan.tag);
      when(purchase.status).thenReturn(status);

      var purchaseResponse = QueryPurchaseDetailsResponse(pastPurchases: [purchase]);
      when(store.queryPastPurchases()).thenAnswer((_) => Future.value(purchaseResponse));

      var product = TestProductDetails();
      when(product.title).thenReturn('Product title');
      when(product.description).thenReturn('Product description');

      var productResponse = ProductDetailsResponse(productDetails: [product], notFoundIDs: []);
      when(store.queryProductDetails({plan.tag})).thenAnswer((_) => Future.value(productResponse));
    }

    setUp(() {
      _stubStoreAvailable(true);

      purchaseUpdatedStream = PublishSubject();
      when(store.purchaseUpdatedStream).thenAnswer((_) => purchaseUpdatedStream);

      manager = InAppSubscriptionManagerImpl(store, mapper, lifecycle, preferences);
    });

    tearDown(() {
      reset(store);
      reset(mapper);
      reset(lifecycle);
      preferences.clearAll();
    });

    test('.isAvailable', () async {
      _stubStoreAvailable(true);
      expect(await manager.isAvailable, isTrue);

      _stubStoreAvailable(false);
      expect(await manager.isAvailable, isFalse);
    });

    test('.isSubscriptionActive returns cached result stored from resume', () async {
      _stubPastPurchaseWithStatus(PurchaseStatus.purchased);

      // trigger the flag to be cached
      await manager.onResume();

      expect(manager.isSubscriptionActive, isTrue);
    });

    test('.hasPendingPurchases returns true if status is pending for any', () async {
      _stubPastPurchaseWithStatus(PurchaseStatus.pending);

      expect(await manager.hasPendingPurchases, isTrue);
    });

    test('.hasPendingPurchases returns false if status is not pending for any', () async {
      _stubPastPurchaseWithStatus(PurchaseStatus.purchased);

      expect(await manager.hasPendingPurchases, isFalse);
    });

    test('.subscribedPlan return plan if subscribed', () async {
      var plan = InAppSubscriptionPlan.premiumMonthly;
      var purchase = TestPurchaseDetails();
      when(purchase.productID).thenReturn(plan.tag);
      when(purchase.status).thenReturn(PurchaseStatus.purchased);

      var purchaseResponse = QueryPurchaseDetailsResponse(pastPurchases: [purchase]);
      when(store.queryPastPurchases()).thenAnswer((_) => Future.value(purchaseResponse));

      var product = TestProductDetails();
      when(product.title).thenReturn('Product title');
      when(product.description).thenReturn('Product description');

      var productResponse = ProductDetailsResponse(productDetails: [product], notFoundIDs: []);
      when(store.queryProductDetails({plan.tag})).thenAnswer((_) => Future.value(productResponse));

      var expected = InAppSubscribedPlan(
        plan: plan,
        title: product.title,
        description: product.description,
      );

      expect(await manager.subscribedPlan, equals(expected));
    });

    test('.subscribedPlan returns null for pending purchase', () async {
      var plan = InAppSubscriptionPlan.premiumMonthly;
      var purchase = TestPurchaseDetails();
      when(purchase.productID).thenReturn(plan.tag);
      when(purchase.status).thenReturn(PurchaseStatus.pending);

      var purchaseResponse = QueryPurchaseDetailsResponse(pastPurchases: [purchase]);
      when(store.queryPastPurchases()).thenAnswer((_) => Future.value(purchaseResponse));

      expect(await manager.subscribedPlan, isNull);
    });

    test('.subscribedPlan returns null if nothing subscribed', () async {
      var purchaseResponse = QueryPurchaseDetailsResponse(pastPurchases: []);
      when(store.queryPastPurchases()).thenAnswer((_) => Future.value(purchaseResponse));

      expect(await manager.subscribedPlan, isNull);
    });

    test('.availableSubscriptions', () async {
      var product = TestProductDetails();
      var productDetails = ProductDetailsResponse(productDetails: [product], notFoundIDs: []);
      var expected = TestInAppSubscriptionOffer();

      when(store.queryProductDetails(any)).thenAnswer((_) => Future.value(productDetails));
      when(mapper.asOffer([product], product)).thenReturn(expected);

      expect(await manager.availableSubscriptions, equals([expected]));
    });

    test('.buySubscription() success', () async {
      var offer = TestInAppSubscriptionOffer();
      var purchaseDetails = TestPurchaseDetails();

      when(purchaseDetails.status).thenReturn(PurchaseStatus.purchased);
      when(purchaseDetails.pendingCompletePurchase).thenReturn(true);
      when(store.buyNonConsumable(purchaseParam: anyNamed('purchaseParam'))).thenAnswer((_) => Future.value(true));

      var future = manager.buySubscription(offer);
      purchaseUpdatedStream.add([purchaseDetails]);
      await future;
    });

    test('.buySubscription() error', () async {
      var offer = TestInAppSubscriptionOffer();
      var purchaseDetails = TestPurchaseDetails();
      var error = TestIAPError();

      when(purchaseDetails.status).thenReturn(PurchaseStatus.error);
      when(purchaseDetails.error).thenReturn(error);
      when(purchaseDetails.pendingCompletePurchase).thenReturn(false);
      when(error.message).thenReturn('message');

      when(store.buyNonConsumable(purchaseParam: anyNamed('purchaseParam'))).thenAnswer((_) => Future.value(true));

      var future = manager.buySubscription(offer);
      purchaseUpdatedStream.add([purchaseDetails]);
      expect(() async => await future, throwsException);
    });

    test('.buySubscription() timeout', () {
      fakeAsync((async) {
        var offer = TestInAppSubscriptionOffer();

        when(store.buyNonConsumable(purchaseParam: anyNamed('purchaseParam'))).thenAnswer((_) => Future.value(true));

        var future = manager.buySubscription(offer);

        expect(future, throwsA(isA<TimeoutException>()));
        async.elapse(InAppSubscriptionManagerImpl.buyTimeout);
      });
    });

    test('.onResume() completes any pending purchases', () async {
      var completedPurchase = TestPurchaseDetails();
      when(completedPurchase.pendingCompletePurchase).thenReturn(false);

      var pendingPurchase = TestPurchaseDetails();
      when(pendingPurchase.pendingCompletePurchase).thenReturn(true);

      var response = QueryPurchaseDetailsResponse(pastPurchases: [
        completedPurchase,
        pendingPurchase,
      ]);

      when(store.queryPastPurchases()).thenAnswer((_) => Future.value(response));

      await manager.onResume();

      verifyNever(store.completePurchase(completedPurchase));
      verify(store.completePurchase(pendingPurchase));
    });
  });
}
