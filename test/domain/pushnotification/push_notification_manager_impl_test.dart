// ignore_for_file: must_be_immutable

import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/sliding_window.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/pushnotification/engine/push_notification_engine.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification.dart';
import 'package:school_pen/domain/pushnotification/model/push_notification_type.dart';
import 'package:school_pen/domain/pushnotification/push_notification_listener.dart';
import 'package:school_pen/domain/pushnotification/push_notification_manager_impl.dart';
import 'package:school_pen/domain/pushnotification/repository/push_notification_repository.dart';
import 'package:school_pen/domain/timetable/timetable_manager.dart';
import 'package:school_pen/domain/user/model/user.dart';
import 'package:school_pen/domain/user/user_manager.dart';
import 'package:test/test.dart';

class TestPushNotificationEngine extends Mock implements PushNotificationEngine {}

class TestPushNotificationRepository extends Mock implements PushNotificationRepository {}

class TestTimetableManager extends Mock implements TimetableManager {}

class TestUserManager extends Mock implements UserManager {}

class TestPushNotification extends Mock implements PushNotification {}

class TestLifecycleManager extends Mock implements LifecycleManager {}

class TestPushNotificationListener extends Mock implements PushNotificationListener {}

void main() {
  group('FirebasePushNotificationManager', () {
    PushNotificationManagerImpl manager;

    final PublishSubject<String> onTokenRefresh = PublishSubject(sync: true);
    final PublishSubject<PushNotification> onPushNotification = PublishSubject(sync: true);

    final PushNotificationEngine engine = TestPushNotificationEngine();
    final PushNotificationRepository repository = TestPushNotificationRepository();
    final TimetableManager timetableManager = TestTimetableManager();
    final UserManager userManager = TestUserManager();
    final LifecycleManager lifecycleManager = TestLifecycleManager();
    final SlidingWindow slidingWindow = ForcedSlidingWindow();
    final PushNotification pushNotification = TestPushNotification();
    final PushNotificationListener listener = TestPushNotificationListener();

    final String token = 'push-notification-token';
    final String pushEngine = 'push-engine';
    final User user = User(id: 'id');

    setUp(() async {
      when(engine.onTokenRefresh).thenAnswer((_) => onTokenRefresh);
      when(engine.onPushNotification).thenAnswer((_) => onPushNotification);
      when(engine.token).thenAnswer((_) async => token);
      when(engine.name).thenReturn(pushEngine);
      when(userManager.getCurrentUser()).thenAnswer((_) async => user);

      manager = PushNotificationManagerImpl(
          engine, repository, timetableManager, userManager, lifecycleManager, slidingWindow);

      manager.addPushNotificationListener(listener);
      await manager.initialize();
    });

    tearDown(() {
      reset(engine);
      reset(repository);
      reset(timetableManager);
      reset(userManager);
      reset(lifecycleManager);
      reset(pushNotification);
      reset(listener);
    });

    test('.onPushNotification() skips unknown notification', () {
      when(pushNotification.type).thenReturn(PushNotificationType.unknown);
      onPushNotification.add(pushNotification);
      verifyNever(listener.onPushNotification(pushNotification));
    });

    test('.onPushNotification() notifies listener', () {
      when(pushNotification.type).thenReturn(PushNotificationType.schoolTimetableChanged);
      onPushNotification.add(pushNotification);
      verify(listener.onPushNotification(pushNotification));
    });

    // TODO: implement push notifications
    // test('.onTimetableConfigured() resubscribes topics', () async {
    //   final String nextSchoolId = '531264123129';
    //   final NotificationTopic nextTopic = SchoolTimetableNotificationTopic.forSchoolId(nextSchoolId);
    //   final NotificationTopic previousTopic = SchoolTimetableNotificationTopic.forSchoolId('9473fdsaf34');
    //
    //   when(repository.getSubscribedTopics()).thenAnswer((_) async => [previousTopic]);
    //   await manager.onTimetableConfigured(nextSchoolId, null);
    //
    //   verify(repository.unsubscribeTopic(previousTopic));
    //   verify(repository.subscribeTopic(nextTopic));
    // });

    test('.onUserChanged() sets user', () async {
      await manager.onUserChanged(user);
      verify(repository.updateInstallation(token: token, pushEngine: pushEngine, user: user));
    });

    test('.onTokenChanged() sets token', () async {
      await manager.onTokenChanged(token);
      verify(repository.updateInstallation(token: token, pushEngine: pushEngine, user: user));
    });
  });
}
