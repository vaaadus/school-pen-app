import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/exam/exam_listener.dart';
import 'package:school_pen/domain/exam/exam_manager_impl.dart';
import 'package:school_pen/domain/exam/model/add_exam.dart';
import 'package:school_pen/domain/exam/model/exam.dart';
import 'package:school_pen/domain/exam/notification/exam_notification_builder.dart';
import 'package:school_pen/domain/exam/repository/exam_repository.dart';
import 'package:school_pen/domain/grade/grade_manager.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:test/test.dart';

class TestExamRepository extends Mock implements ExamRepository {}

class TestNotificationManager extends Mock implements NotificationManager {}

class TestExamNotificationBuilder extends Mock implements ExamNotificationBuilder {}

class TestExamListener extends Mock implements ExamListener {}

class TestSemesterManager extends Mock implements SemesterManager {}

class TestCourseManager extends Mock implements CourseManager {}

class TestGradeManager extends Mock implements GradeManager {}

void main() {
  group('ExamManagerImpl', () {
    ExamManagerImpl manager;

    final ExamRepository repository = TestExamRepository();
    final NotificationManager notifications = TestNotificationManager();
    final ExamNotificationBuilder notificationBuilder = TestExamNotificationBuilder();
    final ExamListener examListener = TestExamListener();
    final SemesterManager semesterManager = TestSemesterManager();
    final CourseManager courseManager = TestCourseManager();
    final GradeManager gradeManager = TestGradeManager();
    final PublishSubject<ExamRepository> onChange = PublishSubject(sync: true);

    final DateTime now = DateTime.now();
    final String examId = '0721495327';
    final Exam archivedExam = _createExam(updatedAt: now, isArchived: true);
    final Exam notArchived = _createExam(updatedAt: now.minusDays(1), isArchived: false);

    setUp(() {
      when(repository.onChange).thenAnswer((_) => onChange);
      when(repository.getAll()).thenAnswer((_) async => [archivedExam, notArchived]);
      when(repository.getById(any)).thenAnswer((invocation) async {
        final String id = invocation.positionalArguments.first;
        return [archivedExam, notArchived].firstWhereOrNull((e) => e.id == id);
      });

      manager = ExamManagerImpl(
        repository,
        notifications,
        notificationBuilder,
        semesterManager,
        courseManager,
        gradeManager,
      );

      manager.addExamListener(examListener);
    });

    tearDown(() {
      reset(repository);
      reset(notifications);
      reset(notificationBuilder);
      reset(examListener);
      reset(semesterManager);
      reset(courseManager);
      reset(gradeManager);
    });

    test('getAll() returns sorted exams', () async {
      expect(await manager.getAll(), equals([archivedExam, notArchived]));
    });

    test('getAll() filters by semesterId', () async {
      final winter = _createExam(semesterId: 'winter');
      when(repository.getAll(semesterId: winter.semesterId)).thenAnswer((_) async => [winter]);
      expect(await manager.getAll(semesterId: winter.semesterId), equals([winter]));
    });

    test('getById()', () async {
      expect(await manager.getById(archivedExam.id), equals(archivedExam));
      expect(await manager.getById('I do not exist'), isNull);
      expect(await manager.getById(null), isNull);
    });

    test('create()', () async {
      var exam = _createAddExam();

      when(repository.create(exam)).thenAnswer((_) async => examId);

      expect(await manager.create(exam), equals(examId));
      onChange.add(repository);

      verifyInOrder([
        repository.create(exam),
        examListener.onExamsChanged(manager),
      ]);
    });

    test('update()', () async {
      var exam = _createAddExam();

      await manager.update(examId, exam);
      onChange.add(repository);

      verifyInOrder([
        repository.update(examId, exam),
        examListener.onExamsChanged(manager),
      ]);
    });

    test('delete()', () async {
      final exam = _createExam();
      when(repository.getById(exam.id)).thenAnswer((_) async => exam);

      await manager.delete(exam.id);
      verify(repository.delete(exam.id));
      verify(examListener.onExamsChanged(manager));
    });

    test('onCleanupSemester', () async {
      final winter = _createExam(semesterId: 'winter');
      final summer = _createExam(semesterId: 'summer');

      when(repository.getAll(semesterId: winter.semesterId)).thenAnswer((_) async => [winter]);
      await manager.onCleanupSemester(winter.semesterId);

      verify(repository.delete(winter.id));
      verifyNever(repository.update(summer.id, any));
    });

    test('onCleanupCourse', () async {
      final math = _createExam(courseId: 'math');
      final english = _createExam(courseId: 'polish');

      when(repository.getAll(courseId: math.courseId)).thenAnswer((_) async => [math]);
      await manager.onCleanupCourse(math.courseId);

      verify(repository.delete(math.id));
      verifyNever(repository.delete(english.id));
    });

    test('onCleanupGrade', () async {
      final gradeA = _createExam(gradeId: 'A');
      final gradeB = _createExam(gradeId: 'B');

      when(repository.getAll(gradeId: gradeA.gradeId)).thenAnswer((_) async => [gradeA]);
      await manager.onCleanupGrade(gradeA.gradeId);

      verify(repository.update(gradeA.id, gradeA.toAddExam().copyWith(gradeId: Optional(null))));
      verifyNever(repository.update(gradeB.id, gradeB.toAddExam().copyWith(gradeId: Optional(null))));
    });
  });
}

AddExam _createAddExam({String semesterId}) {
  return _createExam(semesterId: semesterId).toAddExam();
}

Exam _createExam({
  String semesterId,
  String courseId,
  String gradeId,
  bool isArchived = false,
  DateTime updatedAt,
}) {
  return Exam(
    id: Id.random(),
    semesterId: semesterId ?? Id.random(),
    courseId: courseId ?? Id.random(),
    gradeId: gradeId ?? Id.random(),
    title: 'title',
    note: 'note',
    dateTime: DateTime.now(),
    isArchived: isArchived,
    updatedAt: updatedAt ?? DateTime.now(),
  );
}
