import 'package:flutter/foundation.dart';
import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/semester/model/add_semester.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/repository/semester_repository.dart';
import 'package:school_pen/domain/semester/semester_cleanup_listener.dart';
import 'package:school_pen/domain/semester/semester_listener.dart';
import 'package:school_pen/domain/semester/semester_manager_impl.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:test/test.dart';

class TestSemesterRepository extends Mock implements SemesterRepository {}

class TestUserConfigRepository extends Mock implements UserConfigRepository {}

class TestSchoolYearManager extends Mock implements SchoolYearManager {}

class TestDateTimeProvider extends Mock implements DateTimeProvider {}

class TestLifecycleManager extends Mock implements LifecycleManager {}

class TestDefaultSemesterNameGenerator extends Mock implements DefaultSemesterNameGenerator {}

class TestSemesterListener extends Mock implements SemesterListener {}

class TestSemesterCleanupListener extends Mock implements SemesterCleanupListener {}

void main() {
  group('SemesterManagerImpl', () {
    SemesterManagerImpl manager;

    final SemesterRepository repository = TestSemesterRepository();
    final UserConfigRepository userConfigRepository = TestUserConfigRepository();
    final SchoolYearManager schoolYearManager = TestSchoolYearManager();
    final DateTimeProvider dateTimeProvider = TestDateTimeProvider();
    final DefaultSemesterNameGenerator nameGenerator = TestDefaultSemesterNameGenerator();
    final LifecycleManager lifecycle = TestLifecycleManager();
    final SemesterListener listener = TestSemesterListener();
    final SemesterCleanupListener cleanupListener = TestSemesterCleanupListener();
    final PublishSubject<SemesterRepository> onChange = PublishSubject(sync: true);
    final PublishSubject<String> onCleanupSemester = PublishSubject(sync: true);
    final PublishSubject<List<Property>> onChangeUserConfig = PublishSubject(sync: true);

    final DateTime now = DateTime.now();
    final SchoolYear schoolYear = SchoolYear(id: Id.random(), name: 'year', updatedAt: now);
    final SchoolYear nextSchoolYear = SchoolYear(id: Id.random(), name: 'nextYear', updatedAt: now);
    final Semester emptySemester = _createSemester(title: '', schoolYearId: schoolYear.id);
    final AddSemester emptyAddSemester = emptySemester.toAddSemester();
    final Semester defaultSemester = _createSemester(title: now.year.toString(), schoolYearId: schoolYear.id);
    final Semester randomSemester = _createSemester(title: 'random', schoolYearId: nextSchoolYear.id);
    final AddSemester addRandomSemester = randomSemester.toAddSemester();

    void stubSemestersExist(List<Semester> semesters) {
      final Map<String, List<Semester>> semestersByYear = {};
      for (Semester semester in semesters) {
        final List<Semester> group = semestersByYear[semester.schoolYearId] ?? [];
        group.add(semester);
        semestersByYear[semester.schoolYearId] = group;
      }

      when(repository.getAll()).thenAnswer((_) async => semesters);

      semestersByYear.forEach((schoolYearId, semesters) {
        when(repository.getAll(schoolYearId)).thenAnswer((_) async => semesters);
      });

      when(repository.getById(any)).thenAnswer((invocation) async {
        String id = invocation.positionalArguments.first;
        return semesters.firstWhereOrNull((e) => e.id == id);
      });
    }

    void stubNoSemesterExists() {
      when(repository.getAll()).thenAnswer((_) async => []);
      when(repository.getAll(any)).thenAnswer((_) async => []);
      when(repository.getById(any)).thenAnswer((_) async => null);
    }

    void stubActiveSemester(Semester semester) {
      when(userConfigRepository.getSemesterIdPerYear(semester.schoolYearId)).thenAnswer((_) async => semester.id);
      onChangeUserConfig.add([Property.semesterIdPerYear]);
    }

    void stubAutoSemesterChange(bool enabled) {
      when(userConfigRepository.isAutoSemesterChangeEnabled()).thenAnswer((_) async => enabled);
      onChangeUserConfig.add([Property.autoSemesterChange]);
    }

    setUp(() {
      when(repository.onChange).thenAnswer((_) => onChange);
      when(repository.onCleanupSemester).thenAnswer((_) => onCleanupSemester);
      when(userConfigRepository.onChange).thenAnswer((_) => onChangeUserConfig);

      when(repository.getAll(schoolYear.id)).thenAnswer((_) async => []);
      when(repository.getAll(nextSchoolYear.id)).thenAnswer((_) async => []);

      when(schoolYearManager.getAll()).thenAnswer((_) async => [schoolYear, nextSchoolYear]);
      when(schoolYearManager.getActive()).thenAnswer((_) async => schoolYear);
      when(dateTimeProvider.now()).thenReturn(now);
      when(nameGenerator.generateName()).thenAnswer((_) async => defaultSemester.name);

      manager = SemesterManagerImpl(
        repository,
        userConfigRepository,
        schoolYearManager,
        lifecycle,
        dateTimeProvider,
        nameGenerator,
      );

      manager.addSemesterListener(listener);
      manager.addSemesterCleanupListener(cleanupListener);

      reset(listener);
      reset(cleanupListener);
    });

    tearDown(() {
      reset(repository);
      reset(userConfigRepository);
      reset(schoolYearManager);
      reset(dateTimeProvider);
      reset(lifecycle);
      reset(nameGenerator);
      reset(listener);
      reset(cleanupListener);
    });

    test('getAll', () async {
      stubSemestersExist([defaultSemester]);
      expect(await manager.getAll(), equals([defaultSemester]));

      stubNoSemesterExists();
      expect(await manager.getAll(), equals([]));
    });

    test('getById', () async {
      stubSemestersExist([defaultSemester]);
      expect(await manager.getById(defaultSemester.id), equals(defaultSemester));

      stubNoSemesterExists();
      expect(await manager.getById(defaultSemester.id), isNull);
    });

    test('getActive', () async {
      stubSemestersExist([defaultSemester]);
      stubActiveSemester(defaultSemester);
      expect(await manager.getActive(), equals(defaultSemester));
    });

    test('setActive', () async {
      stubSemestersExist([defaultSemester]);
      await manager.setActive(defaultSemester.id);
      verify(userConfigRepository.setSemesterIdPerYear(defaultSemester.schoolYearId, defaultSemester.id));
    });

    test('setActive throws if semester does not exist', () async {
      stubNoSemesterExists();
      expect(() async => await manager.setActive(defaultSemester.id), throwsA(isA<Exception>()));
    });

    test('create', () async {
      when(repository.create(addRandomSemester)).thenAnswer((_) async => randomSemester.id);
      await manager.create(addRandomSemester);
      onChange.add(repository);

      verify(repository.create(addRandomSemester));
      verify(listener.onSemestersChanged(manager));
    });

    test('create throws if semester is empty', () {
      expect(() async => await manager.create(emptyAddSemester), throwsA(isA<ArgumentError>()));
      verifyNever(repository.create(emptyAddSemester));
      verifyNever(listener.onSemestersChanged(manager));
    });

    test('update', () async {
      await manager.update(randomSemester.id, addRandomSemester);
      onChange.add(repository);

      verify(repository.update(randomSemester.id, addRandomSemester));
      verify(listener.onSemestersChanged(manager));
    });

    test('update with empty semester fails', () async {
      expect(() async => await manager.update(emptySemester.id, emptyAddSemester), throwsA(isA<ArgumentError>()));
      verifyNever(repository.update(emptySemester.id, emptyAddSemester));
      verifyNever(listener.onSemestersChanged(manager));
    });

    test('delete when allowed', () async {
      stubSemestersExist([defaultSemester, randomSemester]);
      stubActiveSemester(defaultSemester);
      reset(listener);

      await manager.delete(randomSemester.id);
      onChange.add(repository);
      onCleanupSemester.add(randomSemester.id);

      verify(repository.delete(randomSemester.id));
      verify(listener.onSemestersChanged(manager));
      verify(cleanupListener.onCleanupSemester(randomSemester.id));
    });

    test('delete throws if removing active semester', () async {
      stubSemestersExist([defaultSemester, randomSemester]);
      stubActiveSemester(defaultSemester);
      reset(listener);

      expect(() async => await manager.delete(defaultSemester.id), throwsA(isA<Exception>()));
      verifyNever(repository.delete(any));
      verifyNever(cleanupListener.onCleanupSemester(any));
      verifyNever(listener.onSemestersChanged(manager));
    });

    test('canDelete', () async {
      stubSemestersExist([randomSemester, defaultSemester]);
      stubActiveSemester(defaultSemester);
      expect(await manager.canDelete(randomSemester.id), isTrue);

      stubSemestersExist([randomSemester]);
      stubActiveSemester(randomSemester);
      expect(await manager.canDelete(randomSemester.id), isFalse);
    });

    test('automaticSemesterChange', () async {
      await manager.setAutomaticSemesterChangeEnabled(true);
      stubAutoSemesterChange(true);
      expect(await manager.isAutomaticSemesterChangeEnabled(), isTrue);

      await manager.setAutomaticSemesterChangeEnabled(false);
      stubAutoSemesterChange(false);
      expect(await manager.isAutomaticSemesterChangeEnabled(), isFalse);
    });

    test('onSchoolYearsChanged dispatches change', () {
      manager.onSchoolYearsChanged(schoolYearManager);
      verify(listener.onSemestersChanged(manager));
    });

    test('onCleanupSchoolYear removes related semesters', () async {
      stubSemestersExist([defaultSemester, randomSemester]);
      stubActiveSemester(defaultSemester);
      await manager.onCleanupSchoolYear(randomSemester.schoolYearId);

      verify(repository.delete(randomSemester.id));
      verifyNever(repository.delete(defaultSemester.id));
    });
  });
}

Semester _createSemester({@required String title, String schoolYearId}) {
  return Semester(
    id: Id.random(),
    schoolYearId: schoolYearId ?? Id.random(),
    name: title,
    updatedAt: DateTime.now(),
  );
}
