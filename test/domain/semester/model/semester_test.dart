import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:test/test.dart';

void main() {
  group('Semester', () {
    final Semester earlier = Semester(
      id: Id.random(),
      schoolYearId: Id.random(),
      name: 'earlier',
      startDate: DateTime.now().minusDays(10),
      endDate: DateTime.now().minusDays(5),
      updatedAt: DateTime.now(),
    );

    final Semester later = Semester(
      id: Id.random(),
      schoolYearId: Id.random(),
      name: 'earlier',
      startDate: DateTime.now().plusDays(5),
      endDate: DateTime.now().plusDays(10),
      updatedAt: DateTime.now(),
    );

    test('isActiveAt', () {
      expect(earlier.isActiveAt(earlier.startDate), isTrue);
      expect(earlier.isActiveAt(earlier.startDate.add(Duration(minutes: 1))), isTrue);
      expect(earlier.isActiveAt(earlier.startDate.subtract(Duration(minutes: 1))), isFalse);
      expect(earlier.isActiveAt(earlier.endDate), isTrue);
      expect(earlier.isActiveAt(earlier.endDate.add(Duration(minutes: 1))), isFalse);
      expect(earlier.isActiveAt(earlier.endDate.subtract(Duration(minutes: 1))), isTrue);
    });

    test('isActive without date range', () {
      final DateTime now = DateTime.now();
      final Semester withoutDateRange = Semester(
        id: Id.random(),
        schoolYearId: Id.random(),
        name: 'name',
        updatedAt: now,
      );

      expect(withoutDateRange.isActiveAt(now), isFalse);
    });

    test('isActive without end date', () {
      final DateTime now = DateTime.now();
      final Semester withoutEndDate = Semester(
        id: Id.random(),
        schoolYearId: Id.random(),
        name: 'name',
        startDate: now,
        updatedAt: now,
      );

      expect(withoutEndDate.isActiveAt(now), isTrue);
      expect(withoutEndDate.isActiveAt(now.minusDays(1)), isFalse);
      expect(withoutEndDate.isActiveAt(now.plusDays(10000)), isTrue);
    });

    test('sort order', () {
      final List<Semester> semesters = [later, earlier];
      semesters.sort();
      expect(semesters, equals([earlier, later]));
    });
  });
}
