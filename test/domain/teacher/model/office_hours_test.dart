import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/teacher/model/office_hours.dart';
import 'package:test/test.dart';

void main() {
  group('OfficeHours', () {
    final TimeRange range1 = TimeRange(start: Time(hour: 10, minute: 0), end: Time(hour: 15, minute: 30));
    final TimeRange range2 = TimeRange(start: Time(hour: 16, minute: 0), end: Time(hour: 21, minute: 30));

    test('plus', () {
      final OfficeHours officeHours = OfficeHours({
        Weekday.monday: [range1],
      });

      final OfficeHours actual = officeHours.plus(Weekday.monday, range2).plus(Weekday.tuesday, range2);
      final OfficeHours expected = OfficeHours({
        Weekday.monday: [range1, range2],
        Weekday.tuesday: [range2],
      });

      expect(actual, equals(expected));
    });

    test('minus', () {
      final OfficeHours officeHours = OfficeHours({
        Weekday.monday: [range1, range2],
        Weekday.tuesday: [range2],
      });

      final OfficeHours actual = officeHours.minus(Weekday.monday, range2).minus(Weekday.tuesday, range2);
      final OfficeHours expected = OfficeHours({
        Weekday.monday: [range1],
      });

      expect(actual, equals(expected));
    });

    test('sortedTimes', () {
      final OfficeHours officeHours = OfficeHours({
        Weekday.tuesday: [range2],
        Weekday.monday: [range2, range1],
      });

      final List<OfficeHoursTime> actual = officeHours.sortedTimes();
      final List<OfficeHoursTime> expected = [
        OfficeHoursTime(weekday: Weekday.monday, timeRange: range1),
        OfficeHoursTime(weekday: Weekday.monday, timeRange: range2),
        OfficeHoursTime(weekday: Weekday.tuesday, timeRange: range2),
      ];

      expect(actual, equals(expected));
    });

    test('isEmpty', () {
      final OfficeHours empty1 = OfficeHours.empty();
      final OfficeHours empty2 = OfficeHours({
        Weekday.monday: [],
      });
      final OfficeHours notEmpty = OfficeHours({
        Weekday.monday: [range1],
      });

      expect(empty1.isEmpty, isTrue);
      expect(empty1.isNotEmpty, isFalse);
      expect(empty2.isEmpty, isTrue);
      expect(empty2.isNotEmpty, isFalse);
      expect(notEmpty.isEmpty, isFalse);
      expect(notEmpty.isNotEmpty, isTrue);
    });
  });
}
