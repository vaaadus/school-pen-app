import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/teacher/model/add_teacher.dart';
import 'package:school_pen/domain/teacher/model/office_hours.dart';
import 'package:school_pen/domain/teacher/model/teacher.dart';
import 'package:school_pen/domain/teacher/repository/teacher_repository.dart';
import 'package:school_pen/domain/teacher/teacher_cleanup_listener.dart';
import 'package:school_pen/domain/teacher/teacher_listener.dart';
import 'package:school_pen/domain/teacher/teacher_manager_impl.dart';
import 'package:test/test.dart';

class TestTeacherRepository extends Mock implements TeacherRepository {}

class TestSchoolYearManager extends Mock implements SchoolYearManager {}

class TestTeacherListener extends Mock implements TeacherListener {}

class TestTeacherCleanupListener extends Mock implements TeacherCleanupListener {}

void main() {
  group('TeacherManagerImpl', () {
    TeacherManagerImpl manager;

    final TeacherRepository repository = TestTeacherRepository();
    final SchoolYearManager schoolYearManager = TestSchoolYearManager();
    final TeacherListener listener = TestTeacherListener();
    final TeacherCleanupListener cleanupListener = TestTeacherCleanupListener();
    final PublishSubject<TeacherRepository> onChange = PublishSubject(sync: true);
    final PublishSubject<String> onCleanupTeacher = PublishSubject(sync: true);

    final winterTeacher = _createTeacher(schoolYearUuid: 'winter');
    final summerTeacher = _createTeacher(schoolYearUuid: 'summer');

    setUp(() {
      when(repository.onChange).thenAnswer((_) => onChange);
      when(repository.onCleanupTeacher).thenAnswer((_) => onCleanupTeacher);
      when(repository.getAll(schoolYearId: winterTeacher.schoolYearId)).thenAnswer((_) async => [winterTeacher]);
      when(repository.getAll(schoolYearId: summerTeacher.schoolYearId)).thenAnswer((_) async => [summerTeacher]);
      when(repository.getAll()).thenAnswer((_) async => [winterTeacher, summerTeacher]);
      when(repository.getById(any)).thenAnswer((invocation) async {
        final String id = invocation.positionalArguments.first;
        return [winterTeacher, summerTeacher].firstWhereOrNull((e) => e.id == id);
      });

      manager = TeacherManagerImpl(repository, schoolYearManager);
      manager.addTeacherListener(listener);
      manager.addTeacherCleanupListener(cleanupListener);
    });

    tearDown(() {
      reset(repository);
      reset(listener);
      reset(schoolYearManager);
    });

    test('getAll() returns all', () async {
      expect(await manager.getAll(), equals([winterTeacher, summerTeacher]));
    });

    test('getAll() filters by schoolYearId', () async {
      expect(await manager.getAll(schoolYearId: winterTeacher.schoolYearId), equals([winterTeacher]));
    });

    test('getById()', () async {
      expect(await manager.getById(summerTeacher.id), equals(summerTeacher));
      expect(await manager.getById('I do not exist'), isNull);
      expect(await manager.getById(null), isNull);
    });

    test('create()', () async {
      final teacherId = Id.random();
      final schoolYearId = Id.random();
      final teacher = _createAddTeacher(schoolYearUuid: schoolYearId);

      when(repository.create(teacher)).thenAnswer((_) async => teacherId);

      expect(await manager.create(teacher), equals(teacherId));
      onChange.add(repository);

      verifyInOrder([
        repository.create(teacher),
        listener.onTeachersChanged(manager),
      ]);
    });

    test('update()', () async {
      final teacherId = Id.random();
      final schoolYearId = Id.random();
      final teacher = _createAddTeacher(schoolYearUuid: schoolYearId);

      await manager.update(teacherId, teacher);
      onChange.add(repository);

      verifyInOrder([
        repository.update(teacherId, teacher),
        listener.onTeachersChanged(manager),
      ]);
    });

    test('delete()', () async {
      final teacher = _createTeacher();
      when(repository.getById(teacher.id)).thenAnswer((_) async => teacher);

      await manager.delete(teacher.id);
      onChange.add(repository);
      onCleanupTeacher.add(teacher.id);

      verify(repository.delete(teacher.id));
      verify(listener.onTeachersChanged(manager));
      verify(cleanupListener.onCleanupTeacher(teacher.id));
    });

    test('onCleanupSchoolYear', () async {
      await manager.onCleanupSchoolYear(winterTeacher.schoolYearId);
      verify(repository.delete(winterTeacher.id));
      verifyNever(repository.update(summerTeacher.id, any));
    });
  });
}

AddTeacher _createAddTeacher({String schoolYearUuid}) {
  return _createTeacher(schoolYearUuid: schoolYearUuid).toAddTeacher();
}

Teacher _createTeacher({String schoolYearUuid}) {
  return Teacher(
    id: Id.random(),
    schoolYearId: schoolYearUuid ?? Id.random(),
    name: 'name',
    officeHours: OfficeHours({}),
    updatedAt: DateTime.now(),
  );
}
