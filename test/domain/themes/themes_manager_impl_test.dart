import 'package:mockito/mockito.dart';
import 'package:school_pen/domain/ads/ad_manager.dart';
import 'package:school_pen/domain/ads/model/ad_placement.dart';
import 'package:school_pen/domain/common/exception/no_internet_exception.dart';
import 'package:school_pen/domain/preferences/impl/in_memory_preferences.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/themes/model/custom_theme.dart';
import 'package:school_pen/domain/themes/themes_manager_impl.dart';
import 'package:test/test.dart';

class TestAdManager extends Mock implements AdManager {}

void main() {
  group('ThemesManagerImpl', () {
    ThemesManagerImpl manager;

    final AdManager adManager = TestAdManager();
    final Preferences preferences = InMemoryPreferences();

    void stubSelectedTheme(CustomTheme theme) {
      preferences.setString(ThemesManagerImpl.keySelectedTheme, theme?.tag);
    }

    setUp(() {
      manager = ThemesManagerImpl(adManager, preferences);
    });

    tearDown(() {
      reset(adManager);
      preferences.clearAll();
    });

    test('selectedTheme', () async {
      expect(manager.selectedTheme, equals(CustomTheme.system));

      final CustomTheme selectedTheme = CustomTheme.dark;
      stubSelectedTheme(selectedTheme);
      expect(manager.selectedTheme, equals(selectedTheme));

      await manager.setSelectedTheme(CustomTheme.black);
      expect(CustomTheme.black.isPremium, isTrue);
      expect(manager.unlockedThemes, isNot(contains(CustomTheme.black)));
      expect(manager.selectedTheme, equals(selectedTheme));
    });

    test('playRewardedContentForTheme', () async {
      final CustomTheme theme = CustomTheme.green;
      expect(await manager.lockedThemes, contains(theme));
      expect(await manager.unlockedThemes, isNot(contains(theme)));

      when(adManager.showRewardedAd(RewardedAdPlacement.Themes)).thenAnswer((_) => throw NoInternetException());
      expect(() => manager.playRewardedContentForTheme(theme), throwsA(isA<NoInternetException>()));
      expect(await manager.lockedThemes, contains(theme));
      expect(await manager.unlockedThemes, isNot(contains(theme)));

      when(adManager.showRewardedAd(RewardedAdPlacement.Themes)).thenAnswer((_) async => false);
      expect(await manager.playRewardedContentForTheme(theme), isFalse);
      expect(await manager.lockedThemes, contains(theme));
      expect(await manager.unlockedThemes, isNot(contains(theme)));

      when(adManager.showRewardedAd(RewardedAdPlacement.Themes)).thenAnswer((_) async => true);
      expect(await manager.playRewardedContentForTheme(theme), true);
      expect(await manager.lockedThemes, isNot(contains(theme)));
      expect(await manager.unlockedThemes, contains(theme));
    });
  });
}
