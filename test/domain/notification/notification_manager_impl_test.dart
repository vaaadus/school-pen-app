import 'package:mockito/mockito.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/notification/local_notification_listener.dart';
import 'package:school_pen/domain/notification/model/add_notification.dart';
import 'package:school_pen/domain/notification/model/local_notification.dart';
import 'package:school_pen/domain/notification/model/notification_channel.dart';
import 'package:school_pen/domain/notification/model/notification_payload.dart';
import 'package:school_pen/domain/notification/model/scheduled_notification.dart';
import 'package:school_pen/domain/notification/notification_manager_impl.dart';
import 'package:school_pen/domain/notification/repository/notification_history.dart';
import 'package:school_pen/domain/notification/repository/notification_repository.dart';
import 'package:school_pen/domain/notification/repository/notification_storage.dart';
import 'package:test/test.dart';

class TestNotificationRepository extends Mock implements NotificationRepository {}

class TestNotificationHistory extends Mock implements NotificationHistory {}

class TestNotificationStorage extends Mock implements NotificationStorage {}

class TestLifecycleManager extends Mock implements LifecycleManager {}

class TestLocalNotificationListener extends Mock implements LocalNotificationListener {}

class TestDateTimeProvider extends Mock implements DateTimeProvider {}

void main() {
  group('NotificationManagerImpl', () {
    NotificationManagerImpl manager;

    final LifecycleManager lifecycle = TestLifecycleManager();
    final NotificationRepository repository = TestNotificationRepository();
    final NotificationHistory history = TestNotificationHistory();
    final NotificationStorage storage = TestNotificationStorage();
    final LocalNotificationListener listener = TestLocalNotificationListener();
    final DateTimeProvider dateProvider = TestDateTimeProvider();

    final DateTime now = DateTime.now();
    final int notificationId = 34234;
    final AddNotification addNotification = AddNotification(
      title: 'title',
      body: 'body',
      dateTime: now.add(Duration(days: 1)),
      channel: NotificationChannel.note,
      payload: NotificationPayload(),
    );

    final LocalNotification notification = LocalNotification(
      id: notificationId,
      channel: addNotification.channel,
      payload: addNotification.payload,
    );

    final ScheduledNotification previousNotification = ScheduledNotification(
      id: 83475349,
      title: 'title',
      body: 'body',
      dateTime: now.subtract(Duration(days: 1)),
      channel: NotificationChannel.note,
      payload: NotificationPayload(),
    );

    setUp(() {
      when(dateProvider.now()).thenReturn(now);
      when(repository.scheduleNotification(addNotification)).thenAnswer((_) async => notificationId);
      when(storage.getAll(channel: previousNotification.channel)).thenAnswer((_) async => [previousNotification]);

      manager = NotificationManagerImpl(lifecycle, repository, history, storage, dateProvider);
      manager.addNotificationListener(listener);
    });

    tearDown(() {
      reset(lifecycle);
      reset(repository);
      reset(history);
      reset(storage);
      reset(dateProvider);
      reset(listener);
    });

    test('getAllScheduledOn', () async {
      expect(await manager.getAllScheduledOn(previousNotification.channel), equals([previousNotification]));
    });

    test('scheduleAllOnChannel', () async {
      await manager.scheduleAllOnChannel(NotificationChannel.note, [addNotification]);
      verify(repository.cancelNotification(previousNotification.id));
      verify(repository.scheduleNotification(addNotification));
    });

    test('scheduleAll', () async {
      await manager.scheduleAll([addNotification]);
      verify(repository.scheduleNotification(addNotification));
    });

    test('scheduleNotification', () async {
      expect(await manager.scheduleNotification(addNotification), notificationId);
      verify(repository.scheduleNotification(addNotification));
    });

    test('cancelAllOnChannel', () async {
      await manager.cancelAllOnChannel(previousNotification.channel);
      verify(repository.cancelNotification(previousNotification.id));
    });

    test('cancelAll', () async {
      await manager.cancelAll([previousNotification.id]);
      verify(repository.cancelNotification(previousNotification.id));
    });

    test('cancelNotification', () async {
      await manager.cancelNotification(notificationId);
      verify(repository.cancelNotification(notificationId));
    });

    test('handleLocalNotifications skips already consumed', () async {
      when(history.consumeNotification(notification.id)).thenAnswer((_) async => false);
      await manager.handleLocalNotification(notification);
      verifyNever(listener.onLocalNotification(notification));
    });

    test('handleLocalNotifications dispatches notification', () async {
      when(history.consumeNotification(notification.id)).thenAnswer((_) async => true);
      await manager.handleLocalNotification(notification);
      verify(listener.onLocalNotification(notification));
    });
  });
}
