import 'package:mockito/mockito.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_listener.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager_impl.dart';
import 'package:test/test.dart';

class TestLifecycleListener extends Mock implements LifecycleListener {}

void main() {
  group('LifecycleManager', () {
    test('new manager is paused', () {
      var manager = LifecycleManagerImpl();
      expect(manager.isResumed, isFalse);
    });

    test('adding a listener notifies immediately onResume', () {
      var manager = LifecycleManagerImpl();
      var listener = TestLifecycleListener();

      manager.resume();
      manager.addLifecycleListener(listener);
      verify(listener.onResume());

      manager.removeLifecycleListener(listener);
      reset(listener);

      manager.pause();
      manager.addLifecycleListener(listener);
      verifyNever(listener.onPause());
    });

    test('resume and pause', () {
      var manager = LifecycleManagerImpl();
      var listener = TestLifecycleListener();

      manager.addLifecycleListener(listener);
      reset(listener);

      expect(manager.isResumed, isFalse);

      manager.resume();
      expect(manager.isResumed, isTrue);

      manager.pause();
      expect(manager.isResumed, isFalse);

      verifyInOrder([
        listener.onResume(),
        listener.onPause(),
      ]);
    });

    test('multiple resumes call onResume once', () {
      var manager = LifecycleManagerImpl();
      var listener = TestLifecycleListener();

      manager.addLifecycleListener(listener);
      reset(listener);

      manager.resume();
      manager.resume();

      verify(listener.onResume()).called(1);
    });

    test('multiple pauses call onPause once', () {
      var manager = LifecycleManagerImpl();
      var listener = TestLifecycleListener();

      manager.addLifecycleListener(listener);
      reset(listener);

      manager.resume();
      manager.pause();
      manager.pause();

      verify(listener.onPause()).called(1);
    });
  });
}
