import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:test/test.dart';

void main() {
  group('Timetable', () {
    test('getSingleEventsIn', () {
      final TimetableEvent event = TimetableEvent(
        id: Id.random(),
        timetableId: Id.random(),
        repeatOptions: EventRepeatOptions(
          type: EventRepeatOptionsType.weekly,
          dateRange: DateTimeRange(
            start: DateTime.utc(2020, 3, 20, 10, 30),
            end: DateTime.utc(2020, 3, 21, 9, 30),
          ),
          weeklyRepeatsOn: [Weekday.friday],
        ),
        updatedAt: DateTime.now(),
      );

      final DateTimeRange targetRange = DateTimeRange(
        start: DateTime.utc(2020, 3, 27),
        end: DateTime.utc(2020, 3, 29),
      );

      expect(
        Timetable.getSingleEventsOf(events: [event], range: targetRange),
        unorderedEquals(event.getSingleEventsIn(targetRange)),
      );
    });

    test('getTimeRangeOfEventsOf', () {
      final SingleTimetableEvent event1 = _buildSingleEvent(
        timeRange: TimeRange(
          start: Time(hour: 10, minute: 30),
          end: Time(hour: 15, minute: 55),
        ),
      );

      final SingleTimetableEvent event2 = _buildSingleEvent(
        timeRange: TimeRange(
          start: Time(hour: 6, minute: 30),
          end: Time(hour: 12, minute: 55),
        ),
      );

      expect(
        Timetable.getTimeRangeOfEventsOf(events: [event1, event2]),
        equals(TimeRange(start: event2.timeRange.start, end: event1.timeRange.end)),
      );
    });

    test('getTimeRangeOfEventsOf with minimum', () {
      final SingleTimetableEvent event1 = _buildSingleEvent(
        timeRange: TimeRange(
          start: Time(hour: 10, minute: 30),
          end: Time(hour: 15, minute: 55),
        ),
      );

      final SingleTimetableEvent event2 = _buildSingleEvent(
        timeRange: TimeRange(
          start: Time(hour: 6, minute: 30),
          end: Time(hour: 12, minute: 55),
        ),
      );

      final TimeRange minimumTimeRange = TimeRange(
        start: Time(hour: 1, minute: 30),
        end: Time(hour: 23, minute: 30),
      );

      expect(
        Timetable.getTimeRangeOfEventsOf(events: [event1, event2], minimumRange: minimumTimeRange),
        equals(minimumTimeRange),
      );
    });
  });
}

SingleTimetableEvent _buildSingleEvent({
  String id,
  String timetableId,
  DateTime dateTime,
  TimeRange timeRange,
  DateTimeRange reoccursAt,
  EventRepeatOptions repeatOptions,
  DateTime updatedAt,
}) {
  id ??= Id.random();
  timetableId ??= Id.random();

  dateTime ??= DateTimes.utcNow();
  timeRange ??= TimeRange(start: Time.min, end: Time.max);

  reoccursAt ??= DateTimeRange(
    start: dateTime.withTime(timeRange.start),
    end: dateTime.withTime(timeRange.end),
  );

  repeatOptions ??= EventRepeatOptions.single(reoccursAt);
  updatedAt ??= DateTime.now();

  return SingleTimetableEvent(
    id: id,
    timetableId: timetableId,
    dateTime: dateTime,
    timeRange: timeRange,
    reoccursAt: reoccursAt,
    repeatOptions: repeatOptions,
    updatedAt: updatedAt,
  );
}
