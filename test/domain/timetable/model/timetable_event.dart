import 'package:flutter/material.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/date/time.dart';
import 'package:school_pen/domain/common/date/time_range.dart';
import 'package:school_pen/domain/common/date/weekday.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:test/test.dart';

void main() {
  group('TimetableEvent', () {
    test('getSingleEventsIn', () {
      final TimetableEvent event = TimetableEvent(
        id: Id.random(),
        timetableId: Id.random(),
        repeatOptions: EventRepeatOptions(
          type: EventRepeatOptionsType.weekly,
          dateRange: DateTimeRange(
            start: DateTime.utc(2020, 3, 20, 10, 30),
            end: DateTime.utc(2020, 3, 21, 9, 30),
          ),
          weeklyRepeatsOn: [Weekday.friday],
        ),
        updatedAt: DateTime.now(),
      );

      expect(
        event.getSingleEventsIn(DateTimeRange(
          start: DateTime.utc(2020, 3, 27),
          end: DateTime.utc(2020, 3, 29),
        )),
        unorderedEquals([
          _buildSingleEvent(
            id: event.id,
            timetableId: event.timetableId,
            dateTime: DateTime.utc(2020, 3, 27),
            timeRange: TimeRange(start: Time(hour: 10, minute: 30), end: Time.max),
            reoccursAt: event.repeatOptions.dateRange.plusWeeks(1),
            repeatOptions: event.repeatOptions,
            updatedAt: event.updatedAt,
          ),
          _buildSingleEvent(
            id: event.id,
            timetableId: event.timetableId,
            dateTime: DateTime.utc(2020, 3, 28),
            timeRange: TimeRange(start: Time.min, end: Time(hour: 9, minute: 30)),
            reoccursAt: event.repeatOptions.dateRange.plusWeeks(1),
            repeatOptions: event.repeatOptions,
            updatedAt: event.updatedAt,
          ),
        ]),
      );
    });
  });

  group('SingleTimetableEvent', () {
    test('compareByDate', () {
      final SingleTimetableEvent earlier = _buildSingleEvent(dateTime: DateTimes.utcNow().minusDays(1));
      final SingleTimetableEvent later = _buildSingleEvent(dateTime: DateTimes.utcNow().plusDays(1));

      final List<SingleTimetableEvent> earlierLater = [earlier, later];
      earlierLater.sort(SingleTimetableEvent.compareByDate);
      expect(earlierLater, equals([earlier, later]));

      final List<SingleTimetableEvent> laterEarlier = [later, earlier];
      laterEarlier.sort(SingleTimetableEvent.compareByDate);
      expect(laterEarlier, equals([earlier, later]));
    });
  });
}

SingleTimetableEvent _buildSingleEvent({
  String id,
  String timetableId,
  DateTime dateTime,
  TimeRange timeRange,
  DateTimeRange reoccursAt,
  EventRepeatOptions repeatOptions,
  DateTime updatedAt,
}) {
  id ??= Id.random();
  timetableId ??= Id.random();

  dateTime ??= DateTimes.utcNow();
  timeRange ??= TimeRange(start: Time.min, end: Time.max);

  reoccursAt ??= DateTimeRange(
    start: dateTime.withTime(timeRange.start),
    end: dateTime.withTime(timeRange.end),
  );

  repeatOptions ??= EventRepeatOptions.single(reoccursAt);
  updatedAt ??= DateTime.now();

  return SingleTimetableEvent(
    id: id,
    timetableId: timetableId,
    dateTime: dateTime,
    timeRange: timeRange,
    reoccursAt: reoccursAt,
    repeatOptions: repeatOptions,
    updatedAt: updatedAt,
  );
}
