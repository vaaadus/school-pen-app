import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/event_repeat_options.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/common/optional.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager.dart';
import 'package:school_pen/domain/sync/sync_manager.dart';
import 'package:school_pen/domain/timetable/model/add_timetable.dart';
import 'package:school_pen/domain/timetable/model/add_timetable_event.dart';
import 'package:school_pen/domain/timetable/model/school.dart';
import 'package:school_pen/domain/timetable/model/school_address.dart';
import 'package:school_pen/domain/timetable/model/timetable.dart';
import 'package:school_pen/domain/timetable/model/timetable_event.dart';
import 'package:school_pen/domain/timetable/model/timetable_type.dart';
import 'package:school_pen/domain/timetable/repository/custom/event/custom_timetable_event_repository.dart';
import 'package:school_pen/domain/timetable/repository/custom/timetable/custom_timetable_repository.dart';
import 'package:school_pen/domain/timetable/repository/public/public_timetable_repository.dart';
import 'package:school_pen/domain/timetable/timetable_manager_impl.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:test/test.dart';

class TestPublicTimetableRepository extends Mock implements PublicTimetableRepository {}

class TestCustomTimetableRepository extends Mock implements CustomTimetableRepository {}

class TestCustomTimetableEventRepository extends Mock implements CustomTimetableEventRepository {}

class TestSyncManager extends Mock implements SyncManager {}

class TestSchoolYearManager extends Mock implements SchoolYearManager {}

class TestCourseManager extends Mock implements CourseManager {}

class TestUserConfigRepository extends Mock implements UserConfigRepository {}

void main() {
  group('TimetableManager', () {
    TimetableManagerImpl manager;

    final PublicTimetableRepository publicRepository = TestPublicTimetableRepository();
    final CustomTimetableRepository customRepository = TestCustomTimetableRepository();
    final CustomTimetableEventRepository eventRepository = TestCustomTimetableEventRepository();
    final SyncManager syncManager = TestSyncManager();
    final SchoolYearManager schoolYearManager = TestSchoolYearManager();
    final CourseManager courseManager = TestCourseManager();
    final UserConfigRepository configRepository = TestUserConfigRepository();

    final PublishSubject<CustomTimetableRepository> onChangeCustomRepository = PublishSubject(sync: true);
    final PublishSubject<CustomTimetableEventRepository> onChangeEventRepository = PublishSubject(sync: true);
    final PublishSubject<List<Property>> onChangeConfigRepository = PublishSubject(sync: true);

    final SchoolYear year = SchoolYear(
      id: Id.random(),
      name: 'my-year',
      updatedAt: DateTime.now(),
    );

    final School school = School(
      id: Id.random(),
      name: 'my-school',
      address: SchoolAddress(
        city: 'Ropczyce',
        country: 'PL',
      ),
      updatedAt: DateTime.now(),
    );

    final TimetableInfo roomInfo = TimetableInfo(
      id: Id.random(),
      name: 'room-renamed',
      type: TimetableType.room,
      updatedAt: DateTime.now(),
    );

    final TimetableInfo customInfo = TimetableInfo(
      id: Id.random(),
      name: 'my-renamed',
      type: TimetableType.custom,
      updatedAt: DateTime.now(),
    );

    final TimetableEvent event = TimetableEvent(
      id: Id.random(),
      timetableId: customInfo.id,
      repeatOptions: EventRepeatOptions.single(DateTimeRange(
        start: DateTime.utc(2020, 1, 1),
        end: DateTime.utc(2020, 1, 2),
      )),
      updatedAt: DateTime.now(),
    );

    final Timetable roomTimetable = roomInfo.toTimetable(events: []);
    final Timetable customTimetable = customInfo.toTimetable(events: [event]);

    setUp(() {
      when(customRepository.onChange).thenAnswer((_) => onChangeCustomRepository);
      when(eventRepository.onChange).thenAnswer((_) => onChangeEventRepository);
      when(configRepository.onChange).thenAnswer((_) => onChangeConfigRepository);

      when(schoolYearManager.getActive()).thenAnswer((_) async => year);

      manager = TimetableManagerImpl(
        publicRepository,
        customRepository,
        eventRepository,
        syncManager,
        schoolYearManager,
        courseManager,
        configRepository,
      );
    });

    tearDown(() {
      reset(publicRepository);
      reset(customRepository);
      reset(eventRepository);
      reset(syncManager);
      reset(schoolYearManager);
      reset(courseManager);
      reset(configRepository);
    });

    test('getSchools', () async {
      when(publicRepository.getSchools()).thenAnswer((_) async => [school]);

      expect(await manager.getSchools(), equals([school]));
    });

    test('getTimetablesInfoOfSchool', () async {
      when(publicRepository.getTimetablesInfoOfSchool(school.id)).thenAnswer((_) async => [roomInfo]);

      expect(await manager.getTimetablesInfoOfSchool(school.id), equals([roomInfo]));
    });

    test('getUserTimetablesInfo', () async {
      when(configRepository.getSubscribedTimetableIdsPerYear(year.id)).thenAnswer((_) async => [roomInfo.id]);
      when(publicRepository.getTimetablesInfoOfIds([roomInfo.id])).thenAnswer((_) async => [roomInfo]);
      when(customRepository.getAll(year.id)).thenAnswer((_) async => [customInfo]);

      expect(await manager.getUserTimetablesInfo(), equals([roomInfo, customInfo]));
    });

    test('getUserTimetables', () async {
      when(configRepository.getSubscribedTimetableIdsPerYear(year.id)).thenAnswer((_) async => [roomInfo.id]);
      when(publicRepository.getTimetable(roomInfo.id)).thenAnswer((_) async => roomTimetable);
      when(customRepository.getAll(year.id)).thenAnswer((_) async => [customInfo]);
      when(eventRepository.getAll(timetableId: customInfo.id)).thenAnswer((_) async => [event]);

      expect(await manager.getUserTimetables(), equals([roomTimetable, customTimetable]));
    });

    test('getTimetable', () async {
      when(publicRepository.getTimetable(roomInfo.id)).thenAnswer((_) async => roomTimetable);
      when(customRepository.getInfoById(customInfo.id)).thenAnswer((_) async => customInfo);
      when(eventRepository.getAll(timetableId: customInfo.id)).thenAnswer((_) async => [event]);

      expect(await manager.getTimetable(roomInfo.id), equals(roomTimetable));
      expect(await manager.getTimetable(customInfo.id), equals(customTimetable));
    });

    test('getTimetableInfo', () async {
      when(publicRepository.getTimetableInfo(roomInfo.id)).thenAnswer((_) async => roomInfo);
      when(customRepository.getInfoById(customInfo.id)).thenAnswer((_) async => customInfo);

      expect(await manager.getTimetableInfo(roomInfo.id), equals(roomInfo));
      expect(await manager.getTimetableInfo(customInfo.id), equals(customInfo));
    });

    test('getActiveTimetableId', () async {
      when(configRepository.getTimetableIdPerYear(year.id)).thenAnswer((_) async => roomInfo.id);

      expect(await manager.getActiveTimetableId(), equals(roomInfo.id));
    });

    test('getActiveTimetableInfo', () async {
      when(configRepository.getTimetableIdPerYear(year.id)).thenAnswer((_) async => roomInfo.id);
      when(customRepository.getInfoById(roomInfo.id)).thenAnswer((_) async => roomInfo);

      expect(await manager.getActiveTimetableInfo(), equals(roomInfo));
    });

    test('getActiveTimetable', () async {
      when(configRepository.getTimetableIdPerYear(year.id)).thenAnswer((_) async => roomInfo.id);
      when(customRepository.getInfoById(customInfo.id)).thenAnswer((_) async => customInfo);
      when(publicRepository.getTimetable(roomInfo.id)).thenAnswer((_) async => roomTimetable);

      expect(await manager.getActiveTimetable(), equals(roomTimetable));
    });

    test('setActiveTimetable', () async {
      await manager.setActiveTimetable(roomTimetable.id);

      verify(configRepository.setTimetableIdPerYear(year.id, roomTimetable.id));
    });

    test('unsubscribeAndResolveNewActiveTimetable', () async {
      when(configRepository.getTimetableIdPerYear(year.id)).thenAnswer((_) async => roomTimetable.id);
      when(configRepository.getSubscribedTimetableIdsPerYear(year.id)).thenAnswer((_) async => []);
      when(publicRepository.getTimetablesInfoOfIds([])).thenAnswer((_) async => []);
      when(customRepository.getAll(year.id)).thenAnswer((_) async => [customInfo]);
      when(eventRepository.getAll(timetableId: customInfo.id)).thenAnswer((_) async => [event]);

      await manager.unsubscribeAndResolveNewActiveTimetable();

      verify(configRepository.unsubscribeTimetablePerYear(year.id, roomTimetable.id));
      verify(configRepository.setTimetableIdPerYear(year.id, customTimetable.id));
    });

    test('subscribeToPublicTimetable', () async {
      await manager.subscribeToPublicTimetable(roomTimetable.id);
      verify(configRepository.subscribeTimetablePerYear(year.id, roomTimetable.id));
    });

    test('unsubscribeFromPublicTimetable', () async {
      await manager.unsubscribeFromPublicTimetable(roomTimetable.id);
      verify(configRepository.unsubscribeTimetablePerYear(year.id, roomTimetable.id));
    });

    test('createTimetable', () async {
      await manager.createTimetable(customTimetable.toAddTimetable());
      verify(customRepository.create(customTimetable.toAddTimetable()));
    });

    test('duplicateTimetable', () async {
      final String newName = 'test';
      final DateTime now = DateTime.now();
      final String newTimetableId = Id.random();

      final AddTimetable duplicateTimetable = customTimetable.toAddTimetable().copyWith(
            name: Optional(newName),
            updatedAt: Optional(now),
          );

      final AddTimetableEvent duplicateEvent = event.toAddTimetableEvent().copyWith(
            timetableId: Optional(newTimetableId),
            updatedAt: Optional(now),
          );

      when(customRepository.create(duplicateTimetable)).thenAnswer((_) async => newTimetableId);

      await manager.duplicateTimetable(customTimetable, now, newName: newName);
      verify(customRepository.create(duplicateTimetable));
      verify(eventRepository.createAll([duplicateEvent]));
    });

    test('renameTimetable', () async {
      final String name = 'new-timetable-name';
      final DateTime now = DateTime.now();
      when(customRepository.getInfoById(customInfo.id)).thenAnswer((_) async => customInfo);

      final AddTimetable renamed = customInfo.toAddTimetable().copyWith(
            name: Optional(name),
            updatedAt: Optional(now),
          );

      await manager.renameTimetable(customInfo.id, name, now);
      verify(customRepository.update(customInfo.id, renamed));
    });

    test('deleteAndResolveNewActiveTimetable', () async {
      when(configRepository.getSubscribedTimetableIdsPerYear(year.id)).thenAnswer((_) async => [roomInfo.id]);
      when(publicRepository.getTimetablesInfoOfIds([roomInfo.id])).thenAnswer((_) async => [roomInfo]);
      when(customRepository.getAll(year.id)).thenAnswer((_) async => []);

      await manager.deleteAndResolveNewActiveTimetable(customTimetable.id);
      verify(eventRepository.deleteAll(customTimetable.id));
      verify(customRepository.delete(customTimetable.id));
      verify(configRepository.setTimetableIdPerYear(year.id, roomInfo.id));
    });

    test('getTimetableEventById', () async {
      when(eventRepository.getById(event.id)).thenAnswer((_) async => event);
      expect(await manager.getTimetableEventById(event.id), equals(event));
    });

    test('createTimetableEvent', () async {
      final AddTimetableEvent addEvent = event.toAddTimetableEvent();
      await manager.createTimetableEvent(addEvent);
      verify(eventRepository.create(addEvent));
    });

    test('onCleanupSchoolYear', () async {
      when(configRepository.getSubscribedTimetableIdsPerYear(year.id)).thenAnswer((_) async => [roomInfo.id]);
      when(publicRepository.getTimetablesInfoOfIds([roomInfo.id])).thenAnswer((_) async => [roomInfo]);
      when(customRepository.getAll(year.id)).thenAnswer((_) async => [customInfo]);

      await manager.onCleanupSchoolYear(year.id);

      verify(eventRepository.deleteAll(customTimetable.id));
      verify(customRepository.delete(customTimetable.id));
      verify(configRepository.unsubscribeTimetablePerYear(year.id, roomTimetable.id));
    });

    test('onCleanupCourse', () async {
      final String courseId = 'course-id';
      await manager.onCleanupCourse(courseId);
      verify(eventRepository.deleteAllByCourseId(courseId));
    });
  });
}
