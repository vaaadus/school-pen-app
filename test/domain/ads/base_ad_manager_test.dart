// ignore_for_file: must_be_immutable

import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/ads/base_ad_manager.dart';
import 'package:school_pen/domain/ads/model/ad_placement.dart';
import 'package:school_pen/domain/ads/repository/location_repository.dart';
import 'package:school_pen/domain/preferences/impl/in_memory_preferences.dart';
import 'package:school_pen/domain/preferences/preferences.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:test/test.dart';

class TestUserConfigRepository extends Mock implements UserConfigRepository {}

class TestLocationRepository extends Mock implements LocationRepository {}

class TestBaseAdManager extends BaseAdManager {
  TestBaseAdManager(
    UserConfigRepository userConfig,
    LocationRepository location,
    Preferences preferences,
  ) : super(userConfig, location, preferences);

  @override
  Future<bool> showRewardedAd(RewardedAdPlacement placement) => throw UnimplementedError();

  @override
  Future<void> preloadRewardedAd(RewardedAdPlacement placement) => throw UnimplementedError();
}

void main() {
  group('BaseAdManager', () {
    BaseAdManager adManager;

    final UserConfigRepository userConfig = TestUserConfigRepository();
    final LocationRepository location = TestLocationRepository();
    final Preferences preferences = InMemoryPreferences();
    final PublishSubject<List<Property>> onChangeUserConfig = PublishSubject();

    setUp(() {
      when(userConfig.onChange).thenAnswer((_) => onChangeUserConfig);

      adManager = TestBaseAdManager(userConfig, location, preferences);
    });

    tearDown(() {
      reset(userConfig);
      reset(location);
      preferences.clearAll();
    });

    test('arePersonalizedAdsEnabled', () async {
      when(userConfig.isPersonalizedAdsEnabled()).thenAnswer((_) async => null);
      when(location.isLocatedInEEA()).thenAnswer((_) async => true);
      preferences.clearAll();
      await adManager.initialize();
      expect(adManager.arePersonalizedAdsEnabled, isFalse);

      when(userConfig.isPersonalizedAdsEnabled()).thenAnswer((_) async => false);
      when(location.isLocatedInEEA()).thenAnswer((_) async => false);
      preferences.clearAll();
      await adManager.initialize();
      expect(adManager.arePersonalizedAdsEnabled, isFalse);

      when(userConfig.isPersonalizedAdsEnabled()).thenAnswer((_) async => true);
      when(location.isLocatedInEEA()).thenAnswer((_) async => true);
      preferences.clearAll();
      await adManager.initialize();
      expect(adManager.arePersonalizedAdsEnabled, isTrue);

      when(userConfig.isPersonalizedAdsEnabled()).thenAnswer((_) async => null);
      when(location.isLocatedInEEA()).thenAnswer((_) async => false);
      preferences.clearAll();
      await adManager.initialize();
      expect(adManager.arePersonalizedAdsEnabled, isTrue);
    });
  });
}
