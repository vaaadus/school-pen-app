import 'package:school_pen/domain/ads/repository/adservice_location_repository.dart';
import 'package:test/test.dart';

void main() {
  group('AdServiceLocationRepository', () {
    final AdServiceLocationRepository repository = AdServiceLocationRepository();

    test('isLocatedInEEA', () async {
      expect(await repository.isLocatedInEEA(), isNotNull);
    });
  });
}
