import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/date/datetimes.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/notification/notification_manager.dart';
import 'package:school_pen/domain/reminder/model/add_reminder.dart';
import 'package:school_pen/domain/reminder/model/reminder.dart';
import 'package:school_pen/domain/reminder/notification/reminder_notification_builder.dart';
import 'package:school_pen/domain/reminder/reminder_listener.dart';
import 'package:school_pen/domain/reminder/reminder_manager_impl.dart';
import 'package:school_pen/domain/reminder/repository/reminder_repository.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:test/test.dart';

class TestReminderRepository extends Mock implements ReminderRepository {}

class TestNotificationManager extends Mock implements NotificationManager {}

class TestReminderNotificationBuilder extends Mock implements ReminderNotificationBuilder {}

class TestReminderListener extends Mock implements ReminderListener {}

class TestSemesterManager extends Mock implements SemesterManager {}

class TestCourseManager extends Mock implements CourseManager {}

void main() {
  group('ReminderManagerImpl', () {
    ReminderManagerImpl manager;

    final ReminderRepository repository = TestReminderRepository();
    final NotificationManager notifications = TestNotificationManager();
    final ReminderNotificationBuilder notificationBuilder = TestReminderNotificationBuilder();
    final ReminderListener reminderListener = TestReminderListener();
    final SemesterManager semesterManager = TestSemesterManager();
    final CourseManager courseManager = TestCourseManager();
    final PublishSubject<ReminderRepository> onChange = PublishSubject(sync: true);

    final DateTime now = DateTime.now();
    final String reminderId = '0721495327';
    final Reminder archivedReminder = _createReminder(updatedAt: now, isArchived: true);
    final Reminder notArchived = _createReminder(updatedAt: now.minusDays(1), isArchived: false);

    setUp(() {
      when(repository.onChange).thenAnswer((_) => onChange);
      when(repository.getAll()).thenAnswer((_) async => [archivedReminder, notArchived]);
      when(repository.getById(any)).thenAnswer((invocation) async {
        final String id = invocation.positionalArguments.first;
        return [archivedReminder, notArchived].firstWhereOrNull((e) => e.id == id);
      });

      manager = ReminderManagerImpl(
        repository,
        notifications,
        notificationBuilder,
        semesterManager,
        courseManager,
      );

      manager.addReminderListener(reminderListener);
    });

    tearDown(() {
      reset(repository);
      reset(notifications);
      reset(notificationBuilder);
      reset(reminderListener);
      reset(semesterManager);
      reset(courseManager);
    });

    test('getAll() returns sorted reminders', () async {
      expect(await manager.getAll(), equals([archivedReminder, notArchived]));
    });

    test('getAll() filters by semesterId', () async {
      final winter = _createReminder(semesterId: 'winter');
      when(repository.getAll(semesterId: winter.semesterId)).thenAnswer((_) async => [winter]);
      expect(await manager.getAll(semesterId: winter.semesterId), equals([winter]));
    });

    test('getById()', () async {
      expect(await manager.getById(archivedReminder.id), equals(archivedReminder));
      expect(await manager.getById('I do not exist'), isNull);
      expect(await manager.getById(null), isNull);
    });

    test('create()', () async {
      var reminder = _createAddReminder();

      when(repository.create(reminder)).thenAnswer((_) async => reminderId);

      expect(await manager.create(reminder), equals(reminderId));
      onChange.add(repository);

      verifyInOrder([
        repository.create(reminder),
        reminderListener.onRemindersChanged(manager),
      ]);
    });

    test('update()', () async {
      var reminder = _createAddReminder();

      await manager.update(reminderId, reminder);
      onChange.add(repository);

      verifyInOrder([
        repository.update(reminderId, reminder),
        reminderListener.onRemindersChanged(manager),
      ]);
    });

    test('delete()', () async {
      final reminder = _createReminder();
      when(repository.getById(reminder.id)).thenAnswer((_) async => reminder);

      await manager.delete(reminder.id);
      verify(repository.delete(reminder.id));
      verify(reminderListener.onRemindersChanged(manager));
    });

    test('onCleanupSemester', () async {
      final winter = _createReminder(semesterId: 'winter');
      final summer = _createReminder(semesterId: 'summer');

      when(repository.getAll(semesterId: winter.semesterId)).thenAnswer((_) async => [winter]);
      await manager.onCleanupSemester(winter.semesterId);

      verify(repository.delete(winter.id));
      verifyNever(repository.update(summer.id, any));
    });

    test('onCleanupCourse', () async {
      final math = _createReminder(courseId: 'math');
      final english = _createReminder(courseId: 'polish');

      when(repository.getAll(courseId: math.courseId)).thenAnswer((_) async => [math]);
      await manager.onCleanupCourse(math.courseId);

      verify(repository.delete(math.id));
      verifyNever(repository.delete(english.id));
    });
  });
}

AddReminder _createAddReminder({String semesterId}) {
  return _createReminder(semesterId: semesterId).toAddReminder();
}

Reminder _createReminder({
  String semesterId,
  String courseId,
  bool isArchived = false,
  DateTime updatedAt,
}) {
  return Reminder(
    id: Id.random(),
    semesterId: semesterId ?? Id.random(),
    courseId: courseId ?? Id.random(),
    title: 'title',
    note: 'note',
    dateTime: DateTime.now(),
    isArchived: isArchived,
    updatedAt: updatedAt ?? DateTime.now(),
  );
}
