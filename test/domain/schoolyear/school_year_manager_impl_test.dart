import 'package:flutter/foundation.dart';
import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/date/datetime_provider.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/schoolyear/model/add_school_year.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/schoolyear/repository/school_year_repository.dart';
import 'package:school_pen/domain/schoolyear/school_year_cleanup_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_listener.dart';
import 'package:school_pen/domain/schoolyear/school_year_locator.dart';
import 'package:school_pen/domain/schoolyear/school_year_manager_impl.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:test/test.dart';

class TestSchoolYearRepository extends Mock implements SchoolYearRepository {}

class TestSchoolYearLocator extends Mock implements SchoolYearLocator {}

class TestUserConfigRepository extends Mock implements UserConfigRepository {}

class TestDateTimeProvider extends Mock implements DateTimeProvider {}

class TestSchoolYearListener extends Mock implements SchoolYearListener {}

class TestSchoolYearCleanupListener extends Mock implements SchoolYearCleanupListener {}

void main() {
  group('SchoolYearManagerImpl', () {
    SchoolYearManagerImpl manager;

    final SchoolYearRepository repository = TestSchoolYearRepository();
    final SchoolYearLocator locator = TestSchoolYearLocator();
    final UserConfigRepository userConfig = TestUserConfigRepository();
    final DateTimeProvider dateTimeProvider = TestDateTimeProvider();
    final SchoolYearListener listener = TestSchoolYearListener();
    final SchoolYearCleanupListener cleanupListener = TestSchoolYearCleanupListener();
    final PublishSubject<SchoolYearRepository> onChange = PublishSubject(sync: true);
    final PublishSubject<String> onCleanupSchoolYear = PublishSubject(sync: true);
    final PublishSubject<List<Property>> onChangeUserConfig = PublishSubject(sync: true);

    final DateTime now = DateTime.now();
    final String defaultSchoolYearName = 'default-name';
    final SchoolYear emptySchoolYear = _createSchoolYear(name: '');
    final AddSchoolYear emptyAddSchoolYear = emptySchoolYear.toAddSchoolYear();
    final SchoolYear defaultSchoolYear = _createSchoolYear(name: now.year.toString());
    final SchoolYear randomSchoolYear = _createSchoolYear(name: 'random');
    final AddSchoolYear addRandomSchoolYear = randomSchoolYear.toAddSchoolYear();

    void stubSchoolYearsExist(List<SchoolYear> years) {
      when(repository.getAll()).thenAnswer((_) async => years);
      when(repository.getById(any)).thenAnswer((invocation) async {
        String id = invocation.positionalArguments.first;
        return years.firstWhereOrNull((e) => e.id == id);
      });
    }

    void stubNoSchoolYearsExists() {
      when(repository.getAll()).thenAnswer((_) async => []);
      when(repository.getById(any)).thenAnswer((_) async => null);
    }

    void stubActiveSchoolYear(String id) {
      when(userConfig.getSchoolYearId()).thenAnswer((_) async => id);
      onChangeUserConfig.add([Property.schoolYearId]);
    }

    setUp(() {
      when(repository.onChange).thenAnswer((_) => onChange);
      when(repository.onCleanupSchoolYear).thenAnswer((_) => onCleanupSchoolYear);
      when(locator.buildDefaultName()).thenReturn(defaultSchoolYearName);
      when(userConfig.onChange).thenAnswer((_) => onChangeUserConfig);
      when(dateTimeProvider.now()).thenReturn(now);
      stubSchoolYearsExist([defaultSchoolYear]);

      manager = SchoolYearManagerImpl(repository, locator, userConfig, dateTimeProvider);
      manager.addSchoolYearListener(listener);
      manager.addSchoolYearCleanupListener(cleanupListener);

      reset(listener);
      reset(cleanupListener);
    });

    tearDown(() {
      reset(repository);
      reset(locator);
      reset(userConfig);
      reset(dateTimeProvider);
      reset(listener);
      reset(cleanupListener);
    });

    test('getAll', () async {
      stubSchoolYearsExist([defaultSchoolYear]);
      expect(await manager.getAll(), equals([defaultSchoolYear]));

      stubNoSchoolYearsExists();
      expect(await manager.getAll(), equals([]));
    });

    test('getById', () async {
      stubSchoolYearsExist([defaultSchoolYear]);
      expect(await manager.getById(defaultSchoolYear.id), equals(defaultSchoolYear));

      stubNoSchoolYearsExists();
      expect(await manager.getById(defaultSchoolYear.id), isNull);
    });

    test('getActive', () async {
      stubSchoolYearsExist([defaultSchoolYear]);
      stubActiveSchoolYear(defaultSchoolYear.id);
      expect(await manager.getActive(), equals(defaultSchoolYear));
    });

    test('getActiveOrNull', () async {
      stubSchoolYearsExist([]);
      stubActiveSchoolYear(null);
      expect(await manager.getActiveOrNull(), isNull);
    });

    test('setActive', () async {
      stubSchoolYearsExist([randomSchoolYear]);
      await manager.setActive(randomSchoolYear.id);
      stubActiveSchoolYear(randomSchoolYear.id);

      expect(await manager.getActive(), equals(randomSchoolYear));
      verify(listener.onSchoolYearsChanged(manager));
    });

    test('setActive throws if school year does not exist', () async {
      stubNoSchoolYearsExists();
      expect(() async => await manager.setActive(defaultSchoolYear.id), throwsA(isA<Exception>()));
    });

    test('create', () async {
      when(repository.create(addRandomSchoolYear)).thenAnswer((_) async => randomSchoolYear.id);
      await manager.create(addRandomSchoolYear);
      onChange.add(repository);

      verify(repository.create(addRandomSchoolYear));
      verify(listener.onSchoolYearsChanged(manager));
    });

    test('create throws if year is empty', () {
      expect(() async => await manager.create(emptyAddSchoolYear), throwsA(isA<ArgumentError>()));
      verifyNever(repository.create(emptyAddSchoolYear));
      verifyNever(listener.onSchoolYearsChanged(manager));
    });

    test('update', () async {
      await manager.update(randomSchoolYear.id, addRandomSchoolYear);
      onChange.add(repository);

      verify(repository.update(randomSchoolYear.id, addRandomSchoolYear));
      verify(listener.onSchoolYearsChanged(manager));
    });

    test('update with empty year fails', () async {
      expect(() async => await manager.update(emptySchoolYear.id, emptyAddSchoolYear), throwsA(isA<ArgumentError>()));
      verifyNever(repository.update(emptySchoolYear.id, emptyAddSchoolYear));
      verifyNever(listener.onSchoolYearsChanged(manager));
    });

    test('delete when allowed', () async {
      stubSchoolYearsExist([defaultSchoolYear, randomSchoolYear]);
      stubActiveSchoolYear(defaultSchoolYear.id);
      reset(listener);

      await manager.delete(randomSchoolYear.id);
      onChange.add(repository);
      onCleanupSchoolYear.add(randomSchoolYear.id);

      verify(repository.delete(randomSchoolYear.id));
      verify(listener.onSchoolYearsChanged(manager));
      verify(cleanupListener.onCleanupSchoolYear(randomSchoolYear.id));
    });

    test('delete throws if removing active school year', () async {
      stubSchoolYearsExist([defaultSchoolYear, randomSchoolYear]);
      stubActiveSchoolYear(defaultSchoolYear.id);
      reset(listener);

      expect(() async => await manager.delete(defaultSchoolYear.id), throwsA(isA<Exception>()));
      verifyNever(repository.delete(any));
      verifyNever(cleanupListener.onCleanupSchoolYear(any));
      verifyNever(listener.onSchoolYearsChanged(manager));
    });

    test('canDelete', () async {
      stubSchoolYearsExist([randomSchoolYear, defaultSchoolYear]);
      stubActiveSchoolYear(defaultSchoolYear.id);
      expect(await manager.canDelete(randomSchoolYear.id), isTrue);

      stubSchoolYearsExist([randomSchoolYear]);
      stubActiveSchoolYear(randomSchoolYear.id);
      expect(await manager.canDelete(randomSchoolYear.id), isFalse);
    });
  });
}

SchoolYear _createSchoolYear({@required String name}) {
  return SchoolYear(
    id: Id.random(),
    name: name,
    updatedAt: DateTime.now(),
  );
}
