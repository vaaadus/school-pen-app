import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/diagnosis/diagnosis.dart';
import 'package:school_pen/domain/diagnosis/diagnosis_manager.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:test/test.dart';

class TestUserConfigRepository extends Mock implements UserConfigRepository {}

void main() {
  group('DiagnosisManager', () {
    DiagnosisManager diagnosisManager;

    final UserConfigRepository userConfig = TestUserConfigRepository();
    final PublishSubject<List<Property>> onChangeUserConfig = PublishSubject(sync: true);

    setUp(() {
      when(userConfig.onChange).thenAnswer((_) => onChangeUserConfig);
      diagnosisManager = DiagnosisManager(userConfig);
      Diagnosis.setImplementation(diagnosisManager);
    });

    tearDown(() {
      reset(userConfig);
      Diagnosis.setImplementation(null);
    });

    test('.setDiagnosisEnabled()', () async {
      await Diagnosis.setDiagnosisEnabled(false);
      verify(userConfig.setDiagnosisEnabled(false));
    });
  });
}
