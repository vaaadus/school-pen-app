import 'package:flutter_test/flutter_test.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';

void main() {
  group('GradingSystem', () {
    test('decorate() with percentage', () {
      expect(GradingSystem.of(GradingSystemType.percentage).decorate(1.234), equals('1.2%'));
      expect(GradingSystem.of(GradingSystemType.percentage).decorate(1.25), equals('1.3%'));
      expect(GradingSystem.of(GradingSystemType.percentage).decorate(0), equals('0%'));
      expect(GradingSystem.of(GradingSystemType.percentage).decorate(100), equals('100%'));
    });

    test('decorate() with alphabetic', () {
      expect(GradingSystem.of(GradingSystemType.alphabetic).decorate(0), equals('F'));
      expect(GradingSystem.of(GradingSystemType.alphabetic).decorate(65), equals('D'));
      expect(GradingSystem.of(GradingSystemType.alphabetic).decorate(75), equals('C'));
      expect(GradingSystem.of(GradingSystemType.alphabetic).decorate(85), equals('B'));
      expect(GradingSystem.of(GradingSystemType.alphabetic).decorate(95), equals('A'));
      expect(GradingSystem.of(GradingSystemType.alphabetic).decorate(100), equals('A+'));
    });

    test('decorate() with numeric', () {
      // Polish system
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).decorate(0), equals('1'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).decorate(20), equals('2'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).decorate(40), equals('3'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).decorate(50), equals('3.5'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).decorate(60), equals('4'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).decorate(80), equals('5'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).decorate(100), equals('6'));

      // German system
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).decorate(0), equals('6'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).decorate(20), equals('5'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).decorate(40), equals('4'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).decorate(50), equals('3.5'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).decorate(60), equals('3'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).decorate(80), equals('2'));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).decorate(100), equals('1'));
    });

    test('convert() with percentage', () {
      // valid
      expect(GradingSystem.of(GradingSystemType.percentage).convert('1.2'), equals(1.2));
      expect(GradingSystem.of(GradingSystemType.percentage).convert('1.2%'), equals(1.2));
      expect(GradingSystem.of(GradingSystemType.percentage).convert('1.25'), equals(1.25));
      expect(GradingSystem.of(GradingSystemType.percentage).convert('   0'), equals(0));
      expect(GradingSystem.of(GradingSystemType.percentage).convert('100   '), equals(100));

      // invalid
      expect(GradingSystem.of(GradingSystemType.percentage).convert('1.2 5'), isNull);
      expect(GradingSystem.of(GradingSystemType.percentage).convert('-1'), isNull);
      expect(GradingSystem.of(GradingSystemType.percentage).convert('+1000'), isNull);
    });

    test('convert() with alphabetic', () {
      // valid
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('f'), equals(30));
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('F'), equals(30));
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('D'), equals(66));
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('C'), equals(75));
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('B'), equals(85));
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('A'), equals(95));
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('A+'), equals(100));
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert(' A+'), equals(100));
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('A+ '), equals(100));

      // invalid
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('+FD'), isNull);
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('D-D'), isNull);
      expect(GradingSystem.of(GradingSystemType.alphabetic).convert('X'), isNull);
    });

    test('convert() with numeric', () {
      // Polish system
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('1'), equals(0));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('2'), equals(20));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('3'), equals(40));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('3.5'), equals(50));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('3,5'), equals(50));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('+4'), equals(60));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert(' 5'), equals(80));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('6 '), equals(100));

      // German system
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).convert('6'), equals(0));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).convert('5'), equals(20));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).convert('4'), equals(40));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).convert('3.5'), equals(50));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).convert('3,5'), equals(50));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).convert('+3'), equals(60));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).convert(' 2'), equals(80));
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 6, maxValue: 1).convert('1 '), equals(100));

      // invalid
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('0'), isNull);
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('7'), isNull);
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('3/5'), isNull);
      expect(GradingSystem.of(GradingSystemType.numeric, minValue: 1, maxValue: 6).convert('(3)'), isNull);
    });
  });
}
