import 'package:flutter/foundation.dart';
import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/domain/common/extensions/lists_ext.dart';
import 'package:school_pen/domain/common/id.dart';
import 'package:school_pen/domain/course/course_manager.dart';
import 'package:school_pen/domain/course/model/course.dart';
import 'package:school_pen/domain/grade/grade_listener.dart';
import 'package:school_pen/domain/grade/grade_manager_impl.dart';
import 'package:school_pen/domain/grade/model/add_grade.dart';
import 'package:school_pen/domain/grade/model/add_grade_category.dart';
import 'package:school_pen/domain/grade/model/course_grades_stats.dart';
import 'package:school_pen/domain/grade/model/grade.dart';
import 'package:school_pen/domain/grade/model/grade_category.dart';
import 'package:school_pen/domain/grade/model/grades_stats.dart';
import 'package:school_pen/domain/grade/model/grading_system.dart';
import 'package:school_pen/domain/grade/repository/category/grade_category_repository.dart';
import 'package:school_pen/domain/grade/repository/grade/grade_repository.dart';
import 'package:school_pen/domain/schoolyear/model/school_year.dart';
import 'package:school_pen/domain/semester/model/semester.dart';
import 'package:school_pen/domain/semester/semester_manager.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:test/test.dart';

class TestGradeRepository extends Mock implements GradeRepository {}

class TestGradeCategoryRepository extends Mock implements GradeCategoryRepository {}

class TestSemesterManager extends Mock implements SemesterManager {}

class TestCourseManager extends Mock implements CourseManager {}

class TestUserConfigRepository extends Mock implements UserConfigRepository {}

class TestGradeListener extends Mock implements GradeListener {}

void main() {
  group('GradeManagerImpl', () {
    GradeManagerImpl manager;

    final GradeRepository repository = TestGradeRepository();
    final GradeCategoryRepository categoryRepository = TestGradeCategoryRepository();
    final SemesterManager semesterManager = TestSemesterManager();
    final CourseManager courseManager = TestCourseManager();
    final UserConfigRepository userConfig = TestUserConfigRepository();
    final GradeListener listener = TestGradeListener();
    final PublishSubject<GradeRepository> onChange = PublishSubject(sync: true);
    final PublishSubject<GradeCategoryRepository> onChangeCategory = PublishSubject(sync: true);
    final PublishSubject<String> onCleanupGrade = PublishSubject(sync: true);
    final PublishSubject<String> onCleanupCategory = PublishSubject(sync: true);
    final PublishSubject<List<Property>> onChangeUserConfig = PublishSubject(sync: true);

    final SchoolYear schoolYear = SchoolYear(id: Id.random(), name: 'schoolYear', updatedAt: DateTime.now());

    /// Semesters
    final Semester winterSemester =
        Semester(id: Id.random(), schoolYearId: schoolYear.id, name: 'winter', updatedAt: DateTime.now());

    final Semester summerSemester =
        Semester(id: Id.random(), schoolYearId: schoolYear.id, name: 'summer', updatedAt: DateTime.now());

    /// Categories
    final AddGradeCategory addCategory = AddGradeCategory(updatedAt: DateTime.now(), customName: 'add_category');

    /// Math grades
    final Course math = _createCourse(schoolYearId: schoolYear.id, name: 'math');
    final Grade math5 = _createGrade(semesterId: winterSemester.id, courseId: math.id, value: 5, weight: 0.3);
    final Grade math3 = _createGrade(semesterId: winterSemester.id, courseId: math.id, value: 3, weight: 0.4);
    final Grade math2 = _createGrade(semesterId: summerSemester.id, courseId: math.id, value: 2, weight: 0.5);

    /// Math stats
    final CourseGradesStats mathSummerStats =
        _createStats(courseId: math.id, semesterId: summerSemester.id, average: 1.0 / 0.5, count: 1);

    /// English grades
    final Course english = _createCourse(schoolYearId: schoolYear.id, name: 'english');
    final Grade english1 = _createGrade(semesterId: summerSemester.id, courseId: english.id, value: 1, weight: 0.9);
    final Grade english4 = _createGrade(semesterId: summerSemester.id, courseId: english.id, value: 4, weight: 0.1);

    /// English stats
    final CourseGradesStats englishTotalStats =
        _createStats(courseId: english.id, semesterId: null, average: 1.3 / 1.0, count: 2);

    final CourseGradesStats englishSummerStats =
        _createStats(courseId: english.id, semesterId: summerSemester.id, average: 1.3 / 1.0, count: 2);

    /// Total stats
    final GradesStats summerStats = GradesStats(
      semesterId: summerSemester.id,
      coursesCount: 2,
      averageGrade: (mathSummerStats.averageGrade + englishSummerStats.averageGrade) / 2,
      totalCreditHours: (math.creditHours ?? 0) + (english.creditHours ?? 0),
    );

    /// courses
    final List<Course> courses = [math, english];
    final List<Grade> grades = [math5, math3, math2, english1, english4];
    final List<Grade> summerGrades = grades.where((e) => e.semesterId == summerSemester.id).toList();

    void _stubGradingSystem(GradingSystem system) {
      when(userConfig.getGradingSystem()).thenAnswer((_) async => system);
    }

    setUp(() {
      when(userConfig.onChange).thenAnswer((_) => onChangeUserConfig);
      when(repository.onChange).thenAnswer((_) => onChange);
      when(repository.onCleanupGrade).thenAnswer((_) => onCleanupGrade);
      when(categoryRepository.onChange).thenAnswer((_) => onChangeCategory);
      when(categoryRepository.onCleanupCategory).thenAnswer((_) => onCleanupCategory);

      when(repository.getAll()).thenAnswer((_) async => grades);
      when(repository.getAll(semesterId: summerSemester.id)).thenAnswer((_) async => summerGrades);
      when(repository.getById(any)).thenAnswer((invocation) async {
        final String id = invocation.positionalArguments.first;
        return grades.firstWhereOrNull((e) => e.id == id);
      });

      when(categoryRepository.getAll()).thenAnswer((_) async => []);

      when(courseManager.getAll()).thenAnswer((_) async => courses);
      when(courseManager.getAll(schoolYear.id)).thenAnswer((_) async => courses);
      when(courseManager.getById(any)).thenAnswer((invocation) async {
        final String id = invocation.positionalArguments.first;
        return courses.firstWhereOrNull((e) => e.id == id);
      });

      when(semesterManager.getById(summerSemester.id)).thenAnswer((_) async => summerSemester);

      manager = GradeManagerImpl(repository, categoryRepository, semesterManager, courseManager, userConfig);
      manager.addGradeListener(listener);
      reset(listener);
    });

    tearDown(() {
      reset(repository);
      reset(categoryRepository);
      reset(semesterManager);
      reset(courseManager);
      reset(userConfig);
      reset(listener);
    });

    test('getGradingSystem returns default', () async {
      expect(await manager.getGradingSystem(), equals(GradingSystem.defaultSystem()));
    });

    test('getGradingSystem', () async {
      final GradingSystem system = GradingSystem.of(GradingSystemType.numeric, minValue: 10, maxValue: 50);
      _stubGradingSystem(system);
      expect(await manager.getGradingSystem(), equals(system));
    });

    test('setGradingSystem', () async {
      final GradingSystem system = GradingSystem.of(GradingSystemType.alphabetic);
      await manager.setGradingSystem(system);
      verify(userConfig.setGradingSystem(system));
    });

    test('getCategories', () async {
      expect(await manager.getCategories(), equals(GradeCategory.predefined));
    });

    test('getCategoryById', () async {
      expect(await manager.getCategoryById(GradeCategory.written.id), equals(GradeCategory.written));
    });

    test('createCategory', () async {
      final String id = Id.random();
      when(categoryRepository.create(addCategory)).thenAnswer((_) async => id);

      expect(await manager.createCategory(addCategory), equals(id));
      verify(categoryRepository.create(addCategory));
    });

    test('updateCategory', () async {
      final String id = Id.random();
      await manager.updateCategory(id, addCategory);
      verify(categoryRepository.update(id, addCategory));
    });

    test('updateCategory not allowed for predefined categories', () async {
      expect(() async => await manager.updateCategory(GradeCategory.written.id, addCategory),
          throwsA(isA<ArgumentError>()));
    });

    test('deleteCategory', () async {
      final String id = Id.random();
      await manager.deleteCategory(id);
      verify(categoryRepository.delete(id));
    });

    test('getStats', () async {
      expect(await manager.getStats(semesterId: summerSemester.id), equals(summerStats));
    });

    test('getStatsById filters by courseId', () async {
      when(repository.getAll(courseId: english.id)).thenAnswer((_) async => [english1, english4]);
      expect(await manager.getStatsById(courseId: english.id), equals(englishTotalStats));
    });

    test('getStatsById filters by courseId & semesterId', () async {
      when(repository.getAll(courseId: english.id, semesterId: summerSemester.id))
          .thenAnswer((_) async => [english1, english4]);

      when(repository.getAll(courseId: math.id, semesterId: summerSemester.id)).thenAnswer((_) async => [math2]);

      expect(
          await manager.getStatsById(courseId: english.id, semesterId: summerSemester.id), equals(englishSummerStats));
      expect(await manager.getStatsById(courseId: math.id, semesterId: summerSemester.id), equals(mathSummerStats));
    });

    test('getAll', () async {
      expect(await manager.getAll(), equals(grades));
    });

    test('getAll filters by courseId', () async {
      when(repository.getAll(courseId: math.id)).thenAnswer((_) async => [math2, math3, math5]);
      expect(await manager.getAll(courseId: math.id), equals([math2, math3, math5]));
    });

    test('getAll filters by semesterId', () async {
      when(repository.getAll(semesterId: summerSemester.id)).thenAnswer((_) async => [english4, english1, math2]);
      expect(await manager.getAll(semesterId: summerSemester.id), equals([english4, english1, math2]));
    });

    test('getAll filters by courseId & semesterId', () async {
      when(repository.getAll(courseId: math.id, semesterId: summerSemester.id)).thenAnswer((_) async => [math2]);
      expect(await manager.getAll(courseId: math.id, semesterId: summerSemester.id), equals([math2]));
    });

    test('getById', () async {
      expect(await manager.getById(english4.id), equals(english4));
    });

    test('create', () async {
      final AddGrade grade = math5.toAddGrade();
      await manager.create(grade);
      onChange.add(repository);

      verify(repository.create(grade));
      verify(listener.onGradesChanged(manager));
    });

    test('update', () async {
      final AddGrade grade = _createGrade(title: 'edit', courseId: english.id, value: 0, weight: 0).toAddGrade();
      await manager.update(english4.id, grade);
      onChange.add(repository);

      verify(repository.update(english4.id, grade));
      verify(listener.onGradesChanged(manager));
    });

    test('delete', () async {
      await manager.delete(math3.id);
      onChange.add(repository);

      verify(repository.delete(math3.id));
      verify(listener.onGradesChanged(manager));
    });

    test('onCoursesChanged', () async {
      manager.onCoursesChanged(courseManager);
      verify(listener.onGradesChanged(manager));
    });

    test('onCleanupSemester', () async {
      when(repository.getAll(semesterId: winterSemester.id)).thenAnswer((_) async => [math5, math3]);

      await manager.onCleanupSemester(winterSemester.id);
      verify(repository.delete(math5.id));
      verify(repository.delete(math3.id));
      verifyNever(repository.update(math2.id, any));
      verifyNever(repository.update(english1.id, any));
      verifyNever(repository.update(english4.id, any));
    });

    test('onCleanupCourse', () async {
      when(repository.getAll(courseId: math.id)).thenAnswer((_) async => [math2, math3, math5]);

      await manager.onCleanupCourse(math.id);
      verify(repository.delete(math2.id));
      verify(repository.delete(math3.id));
      verify(repository.delete(math5.id));
      verifyNever(repository.update(english1.id, any));
      verifyNever(repository.update(english4.id, any));
    });
  });
}

Grade _createGrade({String title, String semesterId, String courseId, double value, double weight}) {
  return Grade(
    id: Id.random(),
    semesterId: semesterId ?? Id.random(),
    courseId: courseId ?? Id.random(),
    name: title ?? '',
    value: value ?? 0,
    weight: weight ?? 0,
    dateTime: DateTime.now(),
    updatedAt: DateTime.now(),
  );
}

Course _createCourse({String schoolYearId, String name}) {
  return Course(
    id: Id.random(),
    schoolYearId: schoolYearId ?? Id.random(),
    name: name ?? '',
    teachersIds: [],
    updatedAt: DateTime.now(),
  );
}

CourseGradesStats _createStats({@required String courseId, @required String semesterId, double average, int count}) {
  return CourseGradesStats(
    courseId: courseId,
    semesterId: semesterId,
    gradesCount: count,
    averageGrade: average,
  );
}
