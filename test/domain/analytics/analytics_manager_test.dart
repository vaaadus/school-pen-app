import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';
import 'package:school_pen/app/pages/routes.dart';
import 'package:school_pen/domain/analytics/analytics.dart';
import 'package:school_pen/domain/analytics/analytics_engine.dart';
import 'package:school_pen/domain/analytics/analytics_event.dart';
import 'package:school_pen/domain/analytics/analytics_manager.dart';
import 'package:school_pen/domain/lifecycle/lifecycle_manager.dart';
import 'package:school_pen/domain/userconfig/repository/user_config_repository.dart';
import 'package:test/test.dart';

class TestUserConfigRepository extends Mock implements UserConfigRepository {}

class TestAnalyticsEngine extends Mock implements AnalyticsEngine {}

class TestLifecycleManager extends Mock implements LifecycleManager {}

void main() {
  group('AnalyticsManager', () {
    AnalyticsManager analyticsManager;

    final UserConfigRepository userConfig = TestUserConfigRepository();
    final AnalyticsEngine engine = TestAnalyticsEngine();
    final LifecycleManager lifecycleManager = TestLifecycleManager();
    final PublishSubject<List<Property>> onChangeUserConfig = PublishSubject(sync: true);

    setUp(() {
      when(userConfig.onChange).thenAnswer((_) => onChangeUserConfig);
      analyticsManager = AnalyticsManager(userConfig, [engine], lifecycleManager);
      Analytics.setImplementation(analyticsManager);
    });

    tearDown(() {
      reset(userConfig);
      reset(engine);
      reset(lifecycleManager);
      Analytics.setImplementation(null);
    });

    test('.logEvent() calls engine', () {
      Analytics.logEvent(AnalyticsEvent.dashboardOpenTimetable);
      verify(engine.logEvent(AnalyticsEvent.dashboardOpenTimetable));
    });

    test('.setScreenName() calls engine', () {
      Analytics.setScreenName(R.gateway_home);
      verify(engine.setScreenName(R.gateway_home));
    });

    test('.setAnalyticsEnabled()', () async {
      await Analytics.setAnalyticsEnabled(false);
      verify(userConfig.setAnalyticsEnabled(false));
    });

    test('.onResume() calls logAppActivated()', () {
      analyticsManager.onResume();
      verify(engine.logAppActivated());
    });

    test('.onPause() calls logAppDeactivated()', () {
      analyticsManager.onPause();
      verify(engine.logAppDeactivated());
    });
  });
}
